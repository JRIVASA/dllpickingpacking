VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFlxGd.ocx"
Begin VB.Form FrmDatosUbicacion 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7260
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15090
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   15090
   Begin VB.CheckBox ChkUbicacionPorNiveles 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Mostrar Ubicaci�n por Niveles"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   5880
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   1320
      Value           =   1  'Checked
      Width           =   2850
   End
   Begin VB.CommandButton CmdDepositos 
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Left            =   5040
      Picture         =   "FrmDatosUbicacion.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   720
      Width           =   435
   End
   Begin VB.TextBox txtDeposito 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   390
      Left            =   2640
      TabIndex        =   4
      Top             =   705
      Width           =   2280
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   15090
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12600
         TabIndex        =   3
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmDatosUbicacion.frx":0802
         Top             =   0
         Width           =   480
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Consultar Datos de Ubicaciones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   120
         Width           =   5415
      End
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid Grid 
      Height          =   5055
      Left            =   240
      TabIndex        =   0
      Top             =   1920
      Width           =   14535
      _ExtentX        =   25638
      _ExtentY        =   8916
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      Rows            =   1
      Cols            =   6
      FixedRows       =   0
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      BackColorUnpopulated=   15658734
      GridColor       =   13421772
      GridColorFixed  =   0
      GridColorUnpopulated=   0
      WordWrap        =   -1  'True
      GridLinesFixed  =   0
      ScrollBars      =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   6
   End
   Begin VB.Label lblMascaraDeposito 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   2640
      TabIndex        =   9
      Top             =   1320
      Width           =   2940
   End
   Begin VB.Label lblGUIMascara 
      BackStyle       =   0  'Transparent
      Caption         =   "Plantilla - Mascara:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   480
      TabIndex        =   8
      Top             =   1320
      Width           =   2055
   End
   Begin VB.Label lblDeposito 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   5640
      TabIndex        =   7
      Top             =   720
      Width           =   4500
   End
   Begin VB.Label lblGUIDeposito 
      BackStyle       =   0  'Transparent
      Caption         =   "Dep�sito:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   480
      TabIndex        =   5
      Top             =   720
      Width           =   1215
   End
End
Attribute VB_Name = "FrmDatosUbicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Const NullQuery = "***$#NULL*$#*$$*"

Private Sub Refrescar()
    OldValue = txtDeposito.Text
    txtDeposito.Text = NullQuery
    txtDeposito.Text = OldValue
End Sub

Private Sub ChkUbicacionPorNiveles_Click()
    Refrescar
End Sub

Private Sub CmdDepositos_Click()
    
    With Frm_Super_Consultas
        
        .Inicializar "SELECT c_CodDeposito, c_Descripcion, c_CodLocalidad " & _
        "FROM MA_DEPOSITO" & _
        IIf(Not LcUsuarioGlobal, " WHERE c_CodLocalidad = '" & LcCodLocalidadUser & "'", vbNullString), _
        " D E P � S I T O S ", Ent.BDD
        
        .Add_ItemLabels "Dep�sito", "c_CodDeposito", 2000, 0
        .Add_ItemLabels "Descripci�n", "c_Descripcion", 4500, 0
        .Add_ItemLabels "Localidad", "c_CodLocalidad", 2000, 0
        
        .Add_ItemSearching "Dep�sito", "c_CodDeposito"
        
        .BusquedaInstantanea = True
        .txtDato.Text = "%"
        .StrOrderBy = "c_CodLocalidad, c_Descripcion"
        
        .Show vbModal
        
        Datos = .ArrResultado
        
        If Not IsEmpty(Datos) Then
            If Not Datos(0) = vbNullString Then
                txtDeposito.Text = NullQuery
                txtDeposito.Text = Datos(0)
            End If
        End If
        
    End With
    
End Sub

Private Sub CargarGrid()

    With Grid
        
        .Clear
        .Rows = 1
        .Cols = 0
    
    End With

End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    AjustarPantalla Me
    ChkUbicacionPorNiveles.Value = vbUnchecked
End Sub

Private Sub txtDeposito_Change()
    
    Dim mSQL As String
    Dim mRS As ADODB.Recordset
    
    mSQL = "SELECT * FROM MA_DEPOSITO " & _
    "WHERE c_CodDeposito = '" & QuitarComillasSimples(txtDeposito.Text) & "'" & _
    IIf(Not LcUsuarioGlobal, " AND c_CodLocalidad = '" & LcCodLocalidadUser & "'", vbNullString)
    
    Set mRS = New ADODB.Recordset
    
    mRS.Open mSQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If mRS.EOF Then
        
        'txtDeposito.Text = vbNullString
        lblDeposito.Caption = vbNullString
        lblMascaraDeposito.Caption = vbNullString
        
        CargarGrid
        
    Else
        
        txtDeposito.Text = mRS!c_CodDeposito
        lblDeposito.Caption = mRS!c_Descripcion
        lblMascaraDeposito.Caption = mRS!cu_Mascara
        
        Consultar
        
    End If
    
End Sub

Private Sub Consultar()
    
    Dim mSQL As String
    Dim mRS As ADODB.Recordset
    
    mSQL = "SELECT UBC.cu_Mascara AS Ubicacion, " & GetLines & _
    "SUM(INV.n_Cantidad) AS ExistenciasEnUbicacion, COUNT(*) AS NroArticulosUbicados" & GetLines & _
    "FROM MA_DEPOSITO DEP INNER JOIN MA_UBICACIONxPRODUCTO UBC" & GetLines & _
    "ON DEP.c_CodDeposito = UBC.cu_Deposito" & GetLines & _
    "INNER JOIN MA_PRODUCTOS PRO ON UBC.cu_Producto = PRO.c_Codigo" & GetLines & _
    "INNER JOIN MA_DEPOPROD INV ON INV.c_CodArticulo = PRO.c_Codigo" & GetLines & _
    "AND INV.c_CodDeposito = DEP.c_CodDeposito" & GetLines & _
    "WHERE DEP.c_CodDeposito = '" & txtDeposito.Text & "'" & GetLines & _
    "GROUP BY UBC.cu_Mascara"
    
    Set mRS = New ADODB.Recordset
    
    mRS.Open mSQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    CargarGrid
    
    If Not mRS.EOF Then
        
        Dim TmpMascara, UbcXNivel As Boolean: UbcXNivel = (ChkUbicacionPorNiveles.Value = vbChecked)
        
        TmpMascara = lblMascaraDeposito 'mRS!Ubicacion
        
        CantS1 = (Len(TmpMascara) - Len(Replace(TmpMascara, ",", vbNullString))) / Len(",")
        CantS2 = (Len(TmpMascara) - Len(Replace(TmpMascara, ".", vbNullString))) / Len(".")
        CantS3 = (Len(TmpMascara) - Len(Replace(TmpMascara, "-", vbNullString))) / Len("-")
        CantS4 = (Len(TmpMascara) - Len(Replace(TmpMascara, "/", vbNullString))) / Len("/")
        CantS5 = (Len(TmpMascara) - Len(Replace(TmpMascara, "|", vbNullString))) / Len("|")
        
        CantSeparadores = CantS1 + CantS2 + CantS3 + CantS4 + CantS5
        
        With Grid
            
            .RowHeight(.Row) = 350
            
            .Cols = 1
            
            .Col = 0
            If UbcXNivel Then
                .ColWidth(.Col) = 0
            Else
                .ColWidth(.Col) = 2500
            End If
            .ColAlignment(.Col) = flexAlignCenterCenter
            .Text = "Ubicaci�n"
            
            For i = 1 To CantSeparadores + 1
                .Cols = .Cols + 1
                .Col = .Cols - 1
                If UbcXNivel Then
                    .ColWidth(.Col) = 2250
                Else
                    .ColWidth(.Col) = 0
                End If
                .ColAlignment(.Col) = flexAlignCenterCenter
                .Text = "Nivel " & i
            Next i
            
            .Cols = .Cols + 1
            .Col = .Cols - 1
            .ColWidth(.Col) = 2500
            .ColAlignment(.Col) = flexAlignRightCenter
            .Text = "Cant. Productos"
            
            .Cols = .Cols + 1
            .Col = .Cols - 1
            .ColWidth(.Col) = 0 '2500
            .ColAlignment(.Col) = flexAlignRightCenter
            .Text = "Cant. Existencias"
            
            While Not mRS.EOF
                
                .Rows = .Rows + 1
                .Row = .Rows - 1
                .RowHeight(.Row) = 425
                
                .Col = 0
                .Text = mRS!Ubicacion
                
                '.Col = CantSeparadores + 1
                '.Text = mRS!Ubicacion
                
                TmpUbc = mRS!Ubicacion
                
                For i = 1 To CantSeparadores + 1
                    
                    TmpPosS1 = InStr(1, TmpUbc, ",")
                    TmpPosS2 = InStr(1, TmpUbc, ".")
                    TmpPosS3 = InStr(1, TmpUbc, "-")
                    TmpPosS4 = InStr(1, TmpUbc, "/")
                    TmpPosS5 = InStr(1, TmpUbc, "|")
                    
                    TmpPos = 0
                    
                    If TmpPos = 0 Or (TmpPosS1 < TmpPos And TmpPosS1 > 0) Then TmpPos = TmpPosS1
                    If TmpPos = 0 Or (TmpPosS2 < TmpPos And TmpPosS2 > 0) Then TmpPos = TmpPosS2
                    If TmpPos = 0 Or (TmpPosS3 < TmpPos And TmpPosS3 > 0) Then TmpPos = TmpPosS3
                    If TmpPos = 0 Or (TmpPosS4 < TmpPos And TmpPosS4 > 0) Then TmpPos = TmpPosS4
                    If TmpPos = 0 Or (TmpPosS5 < TmpPos And TmpPosS5 > 0) Then TmpPos = TmpPosS5
                    
                    If TmpPos > 0 Then
                        TmpLvlData = Mid(TmpUbc, 1, TmpPos - 1)
                        TmpUbc = Mid(TmpUbc, TmpPos + 1)
                        .Col = .Col + 1
                        .Text = TmpLvlData
                    Else
                        .Col = .Col + 1
                        TmpLvlData = TmpUbc
                        TmpUbc = vbNullString
                        .Text = TmpLvlData
                    End If
                    
                Next i
                
                .Col = .Col + 1
                .Text = FormatNumber(mRS!NroArticulosUbicados, 2, vbTrue, vbFalse, vbTrue)
                
                .Col = .Col + 1
                .Text = FormatNumber(mRS!ExistenciasEnUbicacion, 2, vbTrue, vbFalse, vbTrue)
                
                mRS.MoveNext
                
            Wend
            
        End With
        
    End If
    
End Sub
