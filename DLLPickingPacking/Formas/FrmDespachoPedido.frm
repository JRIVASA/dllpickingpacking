VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmDespachoPedido 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Caption         =   "Criterios de Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   7200
      TabIndex        =   28
      Top             =   2280
      Width           =   5295
      Begin VB.OptionButton OptDeposito 
         Caption         =   "Deposito"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   240
         TabIndex        =   34
         Top             =   720
         Width           =   1335
      End
      Begin VB.OptionButton OptRIF 
         Caption         =   "R.I.F"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2040
         TabIndex        =   33
         Top             =   795
         Width           =   1695
      End
      Begin VB.OptionButton OptTotal 
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3840
         TabIndex        =   32
         Top             =   800
         Width           =   975
      End
      Begin VB.OptionButton OptCliente 
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2040
         TabIndex        =   31
         Top             =   360
         Width           =   1575
      End
      Begin VB.OptionButton OptFecha 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   3840
         TabIndex        =   30
         Top             =   360
         Width           =   975
      End
      Begin VB.OptionButton OptPedido 
         Caption         =   "Pedido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   240
         TabIndex        =   29
         Top             =   360
         Value           =   -1  'True
         Width           =   1215
      End
   End
   Begin VB.TextBox txtClienteCodigo 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   495
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   27
      Top             =   960
      Visible         =   0   'False
      Width           =   4095
   End
   Begin VB.TextBox txtVendedorCodigo 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   495
      Left            =   1800
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   26
      Top             =   1800
      Visible         =   0   'False
      Width           =   4095
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2655
      Left            =   120
      TabIndex        =   16
      Top             =   720
      Width           =   6975
      Begin VB.TextBox txtOrdenCompra 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   1680
         TabIndex        =   24
         Top             =   1920
         Width           =   4095
      End
      Begin VB.CommandButton cmdOrdenCompra 
         Height          =   615
         Left            =   6000
         Picture         =   "FrmDespachoPedido.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   1880
         Width           =   615
      End
      Begin VB.CommandButton cmdVendedor 
         Height          =   615
         Left            =   6000
         Picture         =   "FrmDespachoPedido.frx":0802
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   1040
         Width           =   615
      End
      Begin VB.TextBox txtVendedor 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   1680
         MultiLine       =   -1  'True
         TabIndex        =   20
         Top             =   1080
         Width           =   4095
      End
      Begin VB.TextBox txtCliente 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   1680
         TabIndex        =   18
         Top             =   240
         Width           =   4095
      End
      Begin VB.CommandButton cmdCliente 
         Height          =   615
         Left            =   6000
         Picture         =   "FrmDespachoPedido.frx":1004
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   200
         Width           =   615
      End
      Begin VB.Label Pedido 
         BackStyle       =   0  'Transparent
         Caption         =   "Pedido WEB"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   25
         Top             =   2040
         Width           =   1935
      End
      Begin VB.Label Vendedor 
         BackStyle       =   0  'Transparent
         Caption         =   "Vendedor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   22
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Cliente 
         BackStyle       =   0  'Transparent
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   19
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.Frame Frame7 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      ForeColor       =   &H80000008&
      Height          =   1455
      Left            =   7200
      TabIndex        =   10
      Top             =   720
      Width           =   5295
      Begin VB.TextBox TextFechaDesde 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   120
         TabIndex        =   12
         Top             =   720
         Width           =   2295
      End
      Begin VB.TextBox TextFechaHasta 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   2640
         TabIndex        =   11
         Top             =   720
         Width           =   2415
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   2640
         TabIndex        =   14
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha de facturación:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   120
         Width           =   4695
      End
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "Buscar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   12600
      Picture         =   "FrmDespachoPedido.frx":1806
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   2520
      Width           =   2055
   End
   Begin VB.Frame TipoPedido 
      Caption         =   "Tipo de Pedido"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   12600
      TabIndex        =   5
      Top             =   720
      Width           =   2055
      Begin VB.OptionButton OptWeb 
         Caption         =   "Web"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Value           =   -1  'True
         Width           =   1575
      End
      Begin VB.OptionButton OptVentas 
         Caption         =   "Ventas ADM"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   240
         TabIndex        =   7
         Top             =   725
         Width           =   1695
      End
      Begin VB.OptionButton OptAmbos 
         Caption         =   "Ambos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   240
         TabIndex        =   6
         Top             =   1200
         Width           =   1695
      End
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   19335
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12600
         TabIndex        =   4
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmDespachoPedido.frx":3588
         Top             =   0
         Width           =   480
      End
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Consola de Despacho"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   120
         Width           =   11535
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   7005
      LargeChange     =   10
      Left            =   14505
      TabIndex        =   0
      Top             =   3720
      Width           =   674
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   7020
      Left            =   150
      TabIndex        =   1
      Top             =   3720
      Width           =   15045
      _ExtentX        =   26538
      _ExtentY        =   12383
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16777215
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image btnInfo 
      Height          =   300
      Left            =   0
      Picture         =   "FrmDespachoPedido.frx":530A
      Top             =   0
      Width           =   900
   End
   Begin VB.Image Image1 
      Height          =   300
      Left            =   0
      Picture         =   "FrmDespachoPedido.frx":5663
      Top             =   0
      Width           =   900
   End
   Begin VB.Image CmdSelect 
      Height          =   300
      Left            =   0
      Picture         =   "FrmDespachoPedido.frx":59BC
      Top             =   0
      Width           =   900
   End
End
Attribute VB_Name = "FrmDespachoPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public DocumentoPublic As String
Public BarBool As Boolean

Private Consulta As String
Private Where As String
Private CantFilas As Long

Private Sub btnInfo_Click()
    
    On Error GoTo Error1
    
    Dim Rs As New ADODB.Recordset
    Dim SQL As String
    
    'If Grid.TextMatrix(Grid.RowSel, 0) <> "" Then
        
        SQL = "SELECT * FROM MA_PEDIDOS_RUTA_PICKING " & _
        "WHERE CodPedido = '" & Grid.TextMatrix(Grid.RowSel, 10) & "' "
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        FrmPackingPedido.Pedido = Grid.TextMatrix(Grid.RowSel, 10) 'Pedido
        FrmPackingPedido.Lote = Rs!CodLote
        FrmPackingPedido.Show vbModal
        Set FrmPackingPedido = Nothing
        'Call ButtonActualizar_Click
        
    'Else
        'Mensaje True, "No hay información adicional ya que la factura no esta relacionada a un pedido."
    'End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al consultar los detalles del pedido, Descripción: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    'Call PrepararGrid
    'Call PrepararDatos
    
End Sub

Private Sub CmdBuscar_Click()
    
    On Error GoTo Error1
    
    Dim Where As String
    
    Where = ""
    
    If txtClienteCodigo.Text <> "" Then
        Where = Where & " AND VEN.c_CODCLIENTE = '" & txtClienteCodigo.Text & "'"
    End If
    
    If txtVendedorCodigo.Text <> "" Then
        Where = Where & " AND VEN.c_CODVENDEDOR = '" & txtVendedorCodigo.Text & "'"
    End If
    
    If TextFechaDesde.Text <> "" And TextFechaHasta.Text <> "" Then
        Where = Where & " AND CAST(VEN.d_FECHA as date) BETWEEN '" & FechaBD(TextFechaDesde.Text) & "' AND '" & FechaBD(TextFechaHasta.Text) & "'"
    End If
    
    Consulta = _
    "SELECT SUBSTRING(VPED.c_Relacion, 5, 9999) AS PedidoWeb, SUBSTRING(VNDE.c_Relacion, 5, 9999) AS PedidoS, " & vbNewLine & _
    "VEN.c_Documento AS Pedido, CAST(VEN.d_Fecha AS DATE) AS Fecha," & vbNewLine & _
    "VEN.c_CodDeposito AS Deposito, VEN.c_CodCliente AS CodCliente, VEN.PackingBoolean AS Packing, " & vbNewLine & _
    "VEN.c_Rif AS RIF, VEN.c_Descripcion AS Nombre, ROUND(VEN.n_Total, 2) AS Total, VPED.c_Relacion AS RelacionPedido FROM " & vbNewLine & _
    "MA_VENTAS VEN INNER JOIN MA_VENTAS VNDE ON VEN.c_Concepto = 'VEN' AND VEN.c_Relacion = 'NDE ' + VNDE.c_Documento AND VNDE.c_Concepto = 'NDE'" & vbNewLine & _
    "INNER JOIN MA_VENTAS VPED ON VNDE.c_Relacion = 'PED ' + VPED.c_Documento AND VPED.c_Concepto = 'PED'" & vbNewLine & _
    "WHERE VEN.c_CONCEPTO = 'VEN' AND VEN.c_Status = 'DCO' AND VEN.PackingBoolean = 0" & vbNewLine & _
    "AND ('VEN ' + VEN.c_Documento) NOT IN (SELECT c_Relacion FROM MA_VENTAS WHERE c_Concepto = 'DEV' AND c_Relacion = 'VEN ' + VEN.c_Documento)" & vbNewLine & _
    "AND (VEN.c_Documento) NOT IN (SELECT Factura FROM MA_ENTREGAS WHERE Factura = VEN.c_Documento)" & vbNewLine & _
    Where & vbNewLine & vbNewLine
    
    Consulta = Consulta & "UNION ALL" & vbNewLine & vbNewLine
    
    Consulta = Consulta & _
    "SELECT SUBSTRING(VPED.c_Relacion, 5, 9999) AS PedidoWeb, SUBSTRING(VEN.c_Relacion, 5, 9999) AS PedidoS, " & vbNewLine & _
    "VEN.c_Documento AS Pedido, CAST(VEN.d_Fecha AS DATE) AS Fecha, " & vbNewLine & _
    "VEN.c_CodDeposito AS Deposito, VEN.c_CodCliente AS CodCliente, VEN.PackingBoolean AS Packing, " & vbNewLine & _
    "VEN.c_Rif AS RIF, VEN.c_Descripcion AS Nombre, ROUND(VEN.n_Total, 2) AS Total, VPED.c_Relacion AS RelacionPedido FROM " & vbNewLine & _
    "MA_VENTAS VEN INNER JOIN MA_VENTAS VPED ON VEN.c_Relacion = VPED.C_CONCEPTO + ' ' + VPED.c_Documento AND NOT (VPED.c_Concepto = 'NDE' AND LEN(VPED.c_Relacion) > 0) " & vbNewLine & _
    "WHERE VEN.c_CONCEPTO = 'VEN' AND VEN.c_Status = 'DCO' AND VEN.PackingBoolean = 0 " & vbNewLine & _
    "AND ('VEN ' + VEN.c_Documento) NOT IN (SELECT c_Relacion FROM MA_VENTAS WHERE c_Concepto = 'DEV' AND c_Relacion = 'VEN ' + VEN.c_Documento) " & vbNewLine & _
    "AND (VEN.c_Documento) NOT IN (SELECT Factura FROM MA_ENTREGAS WHERE Factura = VEN.c_Documento) " & vbNewLine & _
    Where & vbNewLine & vbNewLine
    
    Consulta = Consulta & "UNION ALL" & vbNewLine & vbNewLine
    
    Consulta = Consulta & _
    "SELECT '' AS PedidoWeb, (VEN.c_CONCEPTO + '' + VEN.c_Documento) AS PedidoS, " & vbNewLine & _
    "VEN.c_Documento AS Pedido, CAST(VEN.d_Fecha AS DATE) AS Fecha, " & vbNewLine & _
    "VEN.c_CodDeposito AS Deposito, VEN.c_CodCliente AS CodCliente, VEN.PackingBoolean AS Packing, " & vbNewLine & _
    "VEN.c_Rif AS RIF, VEN.c_Descripcion AS Nombre, ROUND(VEN.n_Total, 2) AS Total, '' AS RelacionPedido FROM " & vbNewLine & _
    "MA_VENTAS VEN " & vbNewLine & _
    "WHERE VEN.c_CONCEPTO = 'VEN' AND VEN.c_Status = 'DCO' AND VEN.PackingBoolean = 0 AND LEN(VEN.c_Relacion) = 0 " & vbNewLine & _
    "AND ('VEN ' + VEN.c_Documento) NOT IN (SELECT c_Relacion FROM MA_VENTAS WHERE c_Concepto = 'DEV' AND c_Relacion = 'VEN ' + VEN.c_Documento) " & vbNewLine & _
    "AND (VEN.c_Documento) NOT IN (SELECT Factura FROM MA_ENTREGAS WHERE Factura = VEN.c_Documento) " & vbNewLine & _
    Where & vbNewLine & vbNewLine
    
    Dim Rs As New ADODB.Recordset, RsFila As New ADODB.Recordset
    Dim Concepto As String, Documento As String
    Dim Vacio As Boolean, WEB As Boolean
    
    Select Case True
        
        Case OptWeb.Value
            
            Rs.CursorLocation = adUseClient
            Rs.Open Consulta, Ent.BDD, adOpenStatic, adLockOptimistic
            Set Rs.ActiveConnection = Nothing
            
            While Not Rs.EOF
                
                WEB = (UCase(Left(Rs!RelacionPedido, 3)) = UCase("Web"))
                
                If Not WEB Then Rs.Delete
                
                Rs.MoveNext
                
            Wend
            
        Case OptVentas.Value
            
            Rs.CursorLocation = adUseClient
            Rs.Open Consulta, Ent.BDD, adOpenStatic, adLockOptimistic
            Set Rs.ActiveConnection = Nothing
            
            While Not Rs.EOF
                
                WEB = (UCase(Left(Rs!RelacionPedido, 3)) = UCase("Web"))
                
                If WEB Then Rs.Delete
                
                Rs.MoveNext
                
            Wend
            
        Case OptAmbos.Value
            
            Rs.Open Consulta, Ent.BDD, adOpenStatic, adLockOptimistic
            
    End Select
    
    Call PrepararGrid
    
    If Rs.RecordCount > 0 Then
        
        Rs.MoveFirst
        
        Select Case True
            Case OptPedido.Value
                Rs.Sort = "Pedido"
            Case OptCliente.Value
                Rs.Sort = "Nombre"
            Case OptFecha.Value
                Rs.Sort = "Fecha"
            Case OptDeposito.Value
                Rs.Sort = "Deposito"
            Case OptRIF.Value
                Rs.Sort = "RIF"
            Case OptTotal.Value
                Rs.Sort = "Total"
        End Select
        
        Call PrepararDatos(Rs)
        
    End If
    
    Call LimpiarControlesBusqueda
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al realizar la consulta de los pedido datos, Descripción: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Private Sub Grid_EnterCell()

    If Grid.Rows = 1 Then
        'FrameSelect.Visible = False
        Exit Sub
    End If

    If Not Band Then

        If Fila <> Grid.Row Then

            If Fila > 0 Then
                Grid.RowHeight(Fila) = 600
            End If

            Grid.RowHeight(Grid.Row) = 720
            Fila = Grid.Row

        End If

        MostrarEditorTexto2 Me, Grid, btnInfo
        'Grid.ColSel = 0

        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    Else
        Band = False
    End If

End Sub

Public Sub MostrarEditorTexto2(pFrm As Form, pGrd As Object, ByRef txteditor As Object)
    
    On Error Resume Next
    
    With Grid

        .RowSel = .Row
        
        If .Col <> 6 Then Band = True
        '.Col = 6
        '580
        If BarBool Then
            FrameSelect.BackColor = pGrd.BackColorSel
            FrameSelect.Move .Left + (13635 - 1220), .Top + .CellTop, 1290, .CellHeight
            btnInfo.Move ((FrameSelect.Width / 2) - (btnInfo.Width / 2)), ((FrameSelect.Height / 2) - (btnInfo.Height / 2))
            FrameSelect.Visible = True: btnInfo.Visible = True
            FrameSelect.ZOrder
        Else
            FrameSelect.BackColor = pGrd.BackColorSel
            FrameSelect.Move .Left + (13635 - 560), .Top + .CellTop, 1290, .CellHeight
            btnInfo.Move ((FrameSelect.Width / 2) - (btnInfo.Width / 2)), ((FrameSelect.Height / 2) - (btnInfo.Height / 2))
            FrameSelect.Visible = True: btnInfo.Visible = True
            FrameSelect.ZOrder
        End If
        
        
        cellRow = .Row
        cellCol = .Col

     End With
     
End Sub

Private Sub Form_Load()
    
    AjustarPantalla Me
    
    Call PrepararGrid
    ScrollGrid.Min = 0
    ScrollGrid.Max = 0
    ScrollGrid.Value = 0
    Grid.ScrollBars = flexScrollBarNone
    ScrollGrid.Visible = False
    
End Sub

Private Sub Form_Activate()
    
    Dim Rs As New ADODB.Recordset
    
    Consulta = "SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS " & _
    "WHERE TABLE_NAME = 'MA_VENTAS' " & _
    "AND COLUMN_NAME = 'PackingBoolean'"
    
    Rs.Open Consulta, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        Rs.Close
        Unload Me
        Exit Sub
    End If
    
    Rs.Close
    
    Consulta = Empty
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionFree
                     
        .Rows = 2
             
        .FixedCols = 0
        .FixedRows = 1
                
        .Rows = 1
        .Cols = 11
        
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        .FormatString = "WEB" & "|" & "Fecha" & "|" & "Deposito" & "|" & "Cod. Cliente" & "|" & "R.I.F" & "|" & "Nombre del Cliente" & "|" & "Total"
        
        .ColWidth(0) = 1450
        .ColAlignment(0) = flexAlignCenterCenter
        
        .ColWidth(1) = 1600
        .ColAlignment(1) = flexAlignCenterCenter
        
        .ColWidth(2) = 1200
        .ColAlignment(2) = flexAlignCenterCenter
        
        .ColWidth(3) = 1600
        .ColAlignment(3) = flexAlignCenterCenter
                
        .ColWidth(4) = 1650
        .ColAlignment(4) = flexAlignCenterCenter
        
        .ColWidth(5) = 4200 '5150
        .ColAlignment(5) = flexAlignLeftCenter
        
        .ColWidth(6) = 2000
        .ColAlignment(6) = flexAlignRightCenter
        
        .ColWidth(7) = 0
        
        .ColWidth(8) = 1300
        .ColAlignment(8) = flexAlignCenterCenter
        
        .ColWidth(9) = 0
        
        .ColWidth(10) = 0
        
        .Width = 15050

        .ScrollTrack = True
        
        .Row = 0
        .Col = 9
        .ColSel = 6
        
    End With
    
    For i = 0 To Grid.Cols - 1
        Grid.Col = i
        Grid.CellAlignment = flexAlignCenterCenter
    Next
    
End Sub

Private Sub PrepararDatos(Rs As ADODB.Recordset)
    
    On Error GoTo Error1
    
    Grid.Visible = False
    
    If Not Rs.EOF Then
        
        ScrollGrid.Min = 0
        ScrollGrid.Max = Rs.RecordCount
        ScrollGrid.Value = 0
        
        CantFilas = Rs.RecordCount
        
        BarBool = False
        
        If CantFilas > 10 Then
            BarBool = True
        End If
        
        While Not Rs.EOF
            
            Grid.Rows = Grid.Rows + 1
            
            Grid.TextMatrix(Grid.Rows - 1, 0) = Rs!PedidoWeb
            Grid.TextMatrix(Grid.Rows - 1, 1) = Rs!FECHA
            Grid.TextMatrix(Grid.Rows - 1, 2) = Rs!Deposito
            Grid.TextMatrix(Grid.Rows - 1, 3) = Rs!CodCliente
            Grid.TextMatrix(Grid.Rows - 1, 4) = Rs!Rif
            Grid.TextMatrix(Grid.Rows - 1, 5) = Rs!Nombre
            Grid.TextMatrix(Grid.Rows - 1, 6) = Rs!Total
            Grid.TextMatrix(Grid.Rows - 1, 9) = Rs!Pedido
            Grid.TextMatrix(Grid.Rows - 1, 10) = Rs!Pedidos
            
            Grid.Row = Grid.Rows - 1
            Grid.Col = Grid.Cols - 3
            
            Set Grid.CellPicture = Me.btnInfo.Picture
            Grid.CellPictureAlignment = flexAlignCenterCenter
            
            Dim i As Integer
            
            If Rs!Packing Then
                Grid.TextMatrix(Grid.Rows - 1, 7) = 1
                For i = 0 To Grid.Cols - 1
                    Grid.Row = Grid.Rows - 1
                    Grid.Col = i
                    Grid.CellBackColor = &HB4EDB9    'verde
                Next i
            Else
                Grid.TextMatrix(Grid.Rows - 1, 7) = 0
            End If
            
            Rs.MoveNext
            
        Wend
        
        Rs.Close
        
        If Grid.Row > 1 Then
            Grid.Row = 1
            'Grid.ColSel = 6
        End If
        
        Grid.Col = 0
        Grid.Row = 1
        
        If Grid.Rows > 10 Then
            ScrollGrid.Visible = True
            Grid.ScrollBars = flexScrollBarBoth
            Grid.ColWidth(5) = 4200 - 560
        Else
            Grid.ScrollBars = flexScrollBarNone
            ScrollGrid.Visible = False
        End If
    End If
    
Finally:
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, Descripción: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    
    GoTo Finally
    
End Sub

Private Sub LimpiarControlesBusqueda()
    txtCliente.Text = ""
    txtClienteCodigo.Text = ""
    txtVendedor.Text = ""
    txtVendedorCodigo.Text = ""
    txtOrdenCompra.Text = ""
    TextFechaDesde.Text = ""
    TextFechaHasta.Text = ""
End Sub

Private Sub Grid_Click()
    If Grid.CellPicture = Me.btnInfo.Picture Then
        btnInfo_Click
    End If
End Sub

Private Sub grid_DblClick()
    
    On Error GoTo Error1
    
    If SelectRow <> 0 Then
        Grid.RowSel = SelectRow
    End If
    
    'If Grid.TextMatrix(Grid.RowSel, 0) <> "" And Grid.RowSel > 0 Then
    If Grid.RowSel > 0 Then
        
        If Grid.TextMatrix(Grid.RowSel, 7) = "0" Then
            
            FrmDespachoEntrega.PedidoWeb = Grid.TextMatrix(Grid.RowSel, 0) ' El 0 es el PedidoWeb y el 9 el Pedido Stellar.
            FrmDespachoEntrega.PedidoStellar = Grid.TextMatrix(Grid.RowSel, 10)
            FrmDespachoEntrega.Factura = Grid.TextMatrix(Grid.RowSel, 9)
            FrmDespachoEntrega.Comprador = Grid.TextMatrix(Grid.RowSel, 3)
            
            FrmDespachoEntrega.Show vbModal
            
            If FrmDespachoEntrega.Entregado Then
                Set FrmDespachoEntrega = Nothing
                Call LimpiarControlesBusqueda
                Call PrepararGrid
            End If
            
            Set FrmDespachoEntrega = Nothing
            
        Else
            Mensaje True, "El pedido ya fue despachado."
        End If
        
    Else
        Mensaje True, "Seleccione un pedido."
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al finalizar el despachado de este pedido, Descripción: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    Call PrepararGrid
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call grid_DblClick
    End If
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub TextFechaDesde_Click()
    
    Dim mCls As clsFechaSeleccion
    
    Set mCls = New clsFechaSeleccion
    mCls.MostrarInterfazFechaHora 1
    
    If mCls.Selecciono Then
        TextFechaDesde.Text = mCls.FECHA
        TextFechaDesde.Tag = CDbl(mCls.FECHA)
    End If
    
End Sub

Private Sub TextFechaDesdeEmp_Click()
    
    Dim mCls As clsFechaSeleccion
    
    Set mCls = New clsFechaSeleccion
    mCls.MostrarInterfazFechaHora 1
    
    If mCls.Selecciono Then
        TextFechaDesdeEmp.Text = mCls.FECHA
        TextFechaDesdeEmp.Tag = CDbl(mCls.FECHA)
        TextFechaDesde.Enabled = False
        TextFechaHasta.Enabled = False
    End If
    
End Sub

Private Sub TextFechaHasta_Click()
    
    Dim mCls As clsFechaSeleccion
    
    Set mCls = New clsFechaSeleccion
    mCls.MostrarInterfazFechaHora 1
    
    If mCls.Selecciono Then
        TextFechaHasta.Text = mCls.FECHA
        TextFechaHasta.Tag = CDbl(mCls.FECHA)
    End If

End Sub

Private Sub TextFechaHastaEmp_Click()
    
    Dim mCls As clsFechaSeleccion
    
    Set mCls = New clsFechaSeleccion
    mCls.MostrarInterfazFechaHora 1
    
    If mCls.Selecciono Then
        TextFechaHastaEmp.Text = mCls.FECHA
        TextFechaHastaEmp.Tag = CDbl(mCls.FECHA)
        TextFechaDesde.Enabled = False
        TextFechaHasta.Enabled = False
    End If
    
End Sub

Private Sub TxtCliente_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
       cmdCliente_Click
    'ElseIf KeyCode = vbKeyReturn Then
        'BuscarCliente (txtCliente.Text)
    End If
End Sub

Private Sub txtCliente_KeyPress(KeyAscii As Integer)
    'KeyAscii = IIf(KeyAscii = Asc("'"), 0, KeyAscii)
    KeyAscii = 0
    cmdCliente_Click
End Sub

Private Sub BuscarCliente(Codigo As String)
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset
    
    SQL = "SELECT c_CodCliente, c_Descripcio " & _
    "FROM MA_CLIENTES " & _
    "WHERE c_CodCliente = '" & Codigo & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        Mensaje True, "No existe ningun cliente asociado al código: " & Codigo
        txtCliente.Text = ""
    Else
        txtCliente.Text = Rs!c_Descripcio
        txtClienteCodigo = Rs!c_CodCliente
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al buscar un cliente, Descripción: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    Call PrepararGrid
    
End Sub

Private Sub cmdCliente_Click()
    
    On Error GoTo Error1
    
    Dim SQL As String
    
    SQL = "SELECT TOP 30 c_CodCliente, c_Descripcio FROM MA_CLIENTES WHERE n_Activo = 1"
    
    With Frm_Super_Consultas
        .Inicializar SQL, "C L I E N T E S", Ent.BDD
        .Add_ItemLabels "Codigo", "c_CODCLIENTE", 2610, 0
        .Add_ItemLabels "Nombre", "c_DESCRIPCIO", 8500, 0
        .Add_ItemSearching "Nombre", "c_DESCRIPCIO"
        .Add_ItemSearching "Codigo", "c_CODCLIENTE"
        .txtDato.Text = "%"
        '.BusquedaInstantanea = True
        .Show vbModal
        Resultado = .ArrResultado
    End With
    
    Set Frm_Super_Consultas = Nothing
    
    If Not IsNull(Resultado) Then
        If Resultado(0) <> "" Then
            txtCliente.Text = Resultado(1)
            txtClienteCodigo = Resultado(0)
        End If
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al buscar un cliente en el form de superconsultas, Descripción: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    Call PrepararGrid
    
End Sub

Private Sub txtVendedor_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
       cmdVendedor_Click
    'ElseIf KeyCode = vbKeyReturn Then
        'BuscarVendidor (txtVendedor.Text)
    End If
End Sub

Private Sub txtVendedor_KeyPress(KeyAscii As Integer)
    'KeyAscii = IIf(KeyAscii = Asc("'"), 0, KeyAscii)
    KeyAscii = 0
    cmdVendedor_Click
End Sub

Private Sub BuscarVendidor(Codigo As String)
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset
    
    SQL = "SELECT CodUsuario, Descripcion FROM MA_USUARIOS WHERE CodUsuario = '" & Codigo & "'"
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        Mensaje True, "No existe ningun vendedor asocionado al codigo: " & Codigo
        txtVendedor.Text = ""
    Else
        txtVendedor.Text = Rs!Descripcion
        txtVendedorCodigo = Rs!CodUsuario
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al buscar un vendedor, Descripción: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    Call PrepararGrid
    
End Sub

Private Sub cmdVendedor_Click()
    
    On Error GoTo Error1
    
    Dim SQL As String
    
    SQL = "SELECT TOP 30 CodUsuario, Descripcion FROM MA_USUARIOS WHERE bs_Activo = 1"
    
    With Frm_Super_Consultas
        .Inicializar SQL, "U S U A R I O S", Ent.BDD
        .Add_ItemLabels "Codigo", "codusuario", 2610, 0
        .Add_ItemLabels "Nombre", "descripcion", 8500, 0
        .Add_ItemSearching "Nombre", "descripcion"
        .Add_ItemSearching "Codigo", "codusuario"
        .txtDato.Text = "%"
        '.BusquedaInstantanea = True
        .Show vbModal
        Resultado = .ArrResultado
    End With
    
    Set Frm_Super_Consultas = Nothing
    
    If Not IsNull(Resultado) Then
        If Resultado(0) <> "" Then
            txtVendedor.Text = Resultado(1)
            txtVendedorCodigo = Resultado(0)
        End If
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al buscar un cliente en el form de superconsultas, Descripción: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    Call PrepararGrid
            
End Sub

Private Sub txtOrdenCompra_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
       cmdOrdenCompra_Click
    ElseIf KeyCode = vbKeyReturn Then
        BuscarOrdenCompra txtOrdenCompra.Text
    End If
End Sub

Private Sub txtOrdenCompra_KeyPress(KeyAscii As Integer)
    KeyAscii = IIf(KeyAscii = Asc("'"), 0, KeyAscii)
End Sub

Private Sub BuscarOrdenCompra(Codigo As String)
    
    On Error GoTo Error1
    
    Dim Rs As New ADODB.Recordset
    
    Call LimpiarControlesBusqueda
    Call PrepararGrid
    
    Consulta = "SELECT SUBSTRING(MA_VENTAS.c_Relacion, 5, 9999) AS Codigo, MA_VENTAS.c_DESCRIPCION as Cliente" & vbNewLine & _
    "FROM MA_VENTAS INNER JOIN MA_VENTAS VNDE ON VNDE.c_RELACION = 'PED ' + MA_VENTAS.c_DOCUMENTO" & vbNewLine & _
    "INNER JOIN MA_VENTAS VVEN ON VVEN.c_Concepto ='VEN' AND VVEN.c_RELACION = 'NDE ' + VNDE.c_DOCUMENTO" & vbNewLine & _
    "WHERE LEFT(MA_VENTAS.c_Relacion, 3) = 'WEB' AND VVEN.c_Status = 'DCO'" & vbNewLine & _
    "AND VVEN.PackingBoolean = 0" & vbNewLine & _
    "AND ('VEN ' + VVEN.c_Documento) NOT IN (SELECT c_Relacion FROM MA_VENTAS WHERE c_Concepto = 'DEV' AND c_Relacion = 'VEN ' + VVEN.c_Documento)" & vbNewLine & _
    "AND (VVEN.c_Documento) NOT IN (SELECT Factura FROM MA_ENTREGAS WHERE Factura = VVEN.c_Documento)" & vbNewLine & _
    "AND SUBSTRING(MA_VENTAS.c_Relacion, 5, 9999) = '" & Codigo & "'"
    
    Rs.Open Consulta, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Rs.EOF Then
        Mensaje True, "No existe ningún pedido web asociado al código: " & Codigo
        txtOrdenCompra.Text = Empty
    Else
        
        Consulta = "SELECT '" & Rs!Codigo & "' AS PedidoWeb, SUBSTRING(VNDE.c_Relacion, 5, 9999) AS PedidoS, VEN.c_Documento AS Pedido, CAST(VEN.d_FECHA as date) as Fecha," & vbNewLine & _
        "VEN.C_CODDEPOSITO AS Deposito, VEN.c_CODCLIENTE as CodCliente, VEN.PackingBoolean AS Packing," & vbNewLine & _
        "VEN.c_Rif AS RIF, VEN.c_DESCRIPCION AS Nombre, ROUND(VEN.n_Total, 2) AS Total FROM MA_VENTAS VEN INNER JOIN MA_VENTAS VNDE ON VEN.c_Relacion = 'NDE ' + VNDE.c_Documento AND VNDE.c_Concepto = 'NDE'" & vbNewLine & _
        "WHERE VEN.c_Observacion LIKE '%Web [[]" & Rs!Codigo & "]%' AND VEN.c_CONCEPTO = 'VEN'"
        
        Rs.Close
        
        Rs.Open Consulta, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        Call PrepararDatos(Rs)
        
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al buscar un Pedido WEB, Descripción: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    Call PrepararGrid
    
End Sub

Private Sub cmdOrdenCompra_Click()
    
    On Error GoTo Error1
    
    Dim SQL As String
    
    SQL = "SELECT SUBSTRING(MA_VENTAS.c_Relacion, 5, 9999) AS Codigo, MA_VENTAS.c_DESCRIPCION as Cliente" & vbNewLine & _
    "FROM MA_VENTAS INNER JOIN MA_VENTAS VNDE ON VNDE.c_RELACION = 'PED ' + MA_VENTAS.c_DOCUMENTO" & vbNewLine & _
    "INNER JOIN MA_VENTAS VVEN ON VVEN.c_RELACION = 'NDE ' + VNDE.c_DOCUMENTO" & vbNewLine & _
    "WHERE LEFT(MA_VENTAS.c_Relacion, 3) = 'WEB' AND VVEN.c_status = 'DCO'" & vbNewLine & _
    "AND VVEN.PackingBoolean = 0" & vbNewLine & _
    "AND ('VEN ' + VVEN.c_Documento) NOT IN (SELECT c_Relacion FROM MA_VENTAS WHERE c_Concepto = 'DEV')" & vbNewLine & _
    "AND VVEN.c_Documento NOT IN (SELECT Factura FROM MA_ENTREGAS WHERE Factura = VVEN.c_Documento)"
    
    With Frm_Super_Consultas
        .Inicializar SQL, "PEDIDOS WEB", Ent.BDD
        .Add_ItemLabels "Codigo", "Codigo", 2610, 0
        .Add_ItemLabels "Nombre", "Cliente", 8500, 0
        .Add_ItemSearching "Codigo", "Codigo"
        .Add_ItemSearching "Nombre", "Cliente"
        .txtDato.Text = "%"
        '.BusquedaInstantanea = True
        .Show vbModal
        Resultado = .ArrResultado
    End With
    
    Set Frm_Super_Consultas = Nothing
    
    If Not IsEmpty(Resultado) Then
        
        If Trim(Resultado(0)) <> vbNullString Then
            
            Call LimpiarControlesBusqueda
            Call PrepararGrid
            
            If Resultado(0) <> "" Then
                        
                Dim Rs As New ADODB.Recordset
                
                Consulta = "SELECT '" & Resultado(0) & "' AS PedidoWeb, SUBSTRING(VNDE.c_Relacion, 5, 9999) AS PedidoS, VEN.c_Documento AS Pedido, CAST(VEN.d_FECHA as date) as Fecha," & vbNewLine & _
                "VEN.C_CODDEPOSITO AS Deposito, VEN.c_CODCLIENTE as CodCliente, VEN.PackingBoolean AS Packing," & vbNewLine & _
                "VEN.c_Rif AS RIF, VEN.c_DESCRIPCION AS Nombre, ROUND(VEN.n_Total, 2) AS Total FROM MA_VENTAS VEN INNER JOIN MA_VENTAS VNDE ON VEN.c_Relacion = 'NDE ' + VNDE.c_Documento AND VNDE.c_Concepto = 'NDE'" & vbNewLine & _
                "WHERE VEN.c_OBSERVACION LIKE '%Web [[]" & Resultado(0) & "]%' AND VEN.c_CONCEPTO = 'VEN'"
                
                Rs.Open Consulta, Ent.BDD, adOpenForwardOnly, adLockReadOnly
                
                Call PrepararDatos(Rs)
                
            End If
            
        End If
        
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al buscar un cliente en el form de superconsultas, Descripción: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    Call PrepararGrid
    
End Sub
