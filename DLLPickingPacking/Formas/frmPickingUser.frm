VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmPickingUser 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FramePickAll 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      ForeColor       =   &H0000C000&
      Height          =   615
      Left            =   13260
      TabIndex        =   18
      Top             =   9180
      Width           =   1755
      Begin VB.Label CmdPickAll 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "     Recoger    Todo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   555
         Left            =   60
         TabIndex        =   19
         Top             =   20
         Width           =   1635
      End
   End
   Begin VB.Frame FrameCheckAll 
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   13260
      TabIndex        =   16
      Top             =   10080
      Width           =   1755
      Begin VB.Label CmdCheckAll 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "     Finalizar    Todo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   555
         Left            =   60
         TabIndex        =   17
         Top             =   30
         Width           =   1635
      End
   End
   Begin VB.Frame buscar 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   " C�digo a Buscar: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   1275
      Left            =   5040
      TabIndex        =   15
      Top             =   9300
      Width           =   6165
      Begin VB.TextBox Cod_Buscar 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   120
         MaxLength       =   15
         TabIndex        =   1
         Top             =   480
         Width           =   5805
      End
   End
   Begin VB.CommandButton ButtonLeyenda 
      Caption         =   "Leyenda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   11520
      Picture         =   "frmPickingUser.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   14
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9060
      Width           =   1455
   End
   Begin VB.CheckBox Chk_Pedido 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Pedido"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   2880
      TabIndex        =   13
      Top             =   10200
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.CheckBox Chk_NombreProducto 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Art�culo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   2880
      TabIndex        =   12
      Top             =   9840
      Width           =   1305
   End
   Begin VB.CheckBox Chk_Ubicacion 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Ubicaci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   420
      Left            =   840
      TabIndex        =   11
      Top             =   9720
      Value           =   1  'Checked
      Width           =   1425
   End
   Begin VB.CheckBox Chk_Lote 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Lote"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   840
      TabIndex        =   8
      Top             =   10200
      Visible         =   0   'False
      Width           =   2505
   End
   Begin VB.Frame FrameSelect 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   13680
      TabIndex        =   7
      Top             =   7860
      Visible         =   0   'False
      Width           =   720
      Begin VB.Image CmdSelect 
         Height          =   480
         Left            =   120
         Picture         =   "frmPickingUser.frx":0CCA
         Top             =   120
         Width           =   480
      End
   End
   Begin VB.CommandButton ButtonActualizar 
      Caption         =   "Actualizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   11520
      Picture         =   "frmPickingUser.frx":2A4C
      Style           =   1  'Graphical
      TabIndex        =   6
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   10020
      Width           =   1455
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   8370
      LargeChange     =   10
      Left            =   14530
      TabIndex        =   3
      Top             =   600
      Width           =   674
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   8370
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   14764
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   19335
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Consola de Recolecci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   360
         TabIndex        =   5
         Top             =   120
         Width           =   11535
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "frmPickingUser.frx":47CE
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12600
         TabIndex        =   2
         Top             =   120
         Width           =   1815
      End
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00AE5B00&
      X1              =   3360
      X2              =   4800
      Y1              =   9510
      Y2              =   9510
   End
   Begin VB.Label C�digo 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Criterios de Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Index           =   71
      Left            =   480
      TabIndex        =   9
      Top             =   9360
      Width           =   2805
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   1215
      Index           =   1
      Left            =   360
      TabIndex        =   10
      Top             =   9300
      Width           =   4455
   End
End
Attribute VB_Name = "FrmPickingUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Band As Boolean
Private OrderBy As String
Private Fila As Integer
Private Salir As Variant
Private CheckAll As Boolean

Const AnchoCampoDescripcion = 6790 '7450

Private Sub Form_Activate()
    
    If Salir Then
        
        Mensaje True, "No hay art�culos pendientes por recolectar."
        
        Unload Me
        
        Exit Sub
        
    End If
    
    If gRutinas.PuedeObtenerFoco(Cod_Buscar) Then Cod_Buscar.SetFocus
    
End Sub

Private Sub Form_Load()
    
    AjustarPantalla Me
    
    'OrderBy = "order by isNull(MA_UBICACIONxPRODUCTO.CU_MASCARA,'N/A')"
    OrderBy = "ORDER BY isNULL(MAX(Ubicacion),'N/A')"
    
    Salir = False
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionByRow
        
        .Rows = 2
        
        .FixedCols = 0
        .FixedRows = 1
        
        .Rows = 1
        .Cols = 11
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        .FormatString = "Lote" & "|" & "Pedido" & "|" & "Articulo" & "|" & _
        "Nombre del Producto" & "|" & "Ubicaci�n" & "|" & "Cant. Sol" & "|" & "Cant. Rec"
        
        .ColWidth(0) = 0 ' Valor anterior 1450
        .ColAlignment(0) = flexAlignCenterCenter
        
        .ColWidth(1) = 0 ' Valor anterior 1450
        .ColAlignment(1) = flexAlignCenterCenter
        
        .ColWidth(2) = 2500 ' Valor anterior 2000
        .ColAlignment(2) = flexAlignCenterCenter
        
        .ColWidth(3) = AnchoCampoDescripcion ' Valor anterior 5500
        .ColAlignment(3) = flexAlignLeftCenter
        
        .ColWidth(4) = 1860 ' Valor anterior 1200
        .ColAlignment(4) = flexAlignCenterCenter
        
        .ColWidth(5) = 1200 ' Valor anterior 1100
        .ColAlignment(5) = flexAlignCenterCenter
        
        .ColWidth(6) = 1400 ' Valor anterior 1400
        .ColAlignment(6) = flexAlignCenterCenter
        
        .ColWidth(7) = 1250
        
        .ColWidth(8) = 0
        
        .ColWidth(9) = 0
        
        .ColWidth(10) = 0
        
        .Width = 15050
        
        .ScrollTrack = True
        
        .Row = 0
        Fila = 0
        .Col = 10
        .ColSel = 9
        
    End With
    
    For I = 0 To Grid.Cols - 1
        Grid.Col = I
        Grid.CellAlignment = flexAlignCenterCenter
    Next
    
End Sub

Private Sub PrepararDatos()
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset, I As Integer
    
    Grid.Visible = False
    
'    SQL = "select isNull(MA_Codigos.C_Codigo,MA_PRODUCTOS.C_Codigo) as CodEDI, " & vbNewLine & _
'    "MA_PEDIDOS_RUTA_PICKING.CodProducto, MA_PRODUCTOS.C_DESCRI as Nombre, " & vbNewLine & _
'    "isNull(MA_UBICACIONxPRODUCTO.CU_MASCARA,'N/A') as Ubicacion, " & vbNewLine & _
'    "MA_PEDIDOS_RUTA_PICKING.CodLote, MA_PEDIDOS_RUTA_PICKING.CantSolicitada, " & vbNewLine & _
'    "MA_PEDIDOS_RUTA_PICKING.CantRecolectada, MA_PEDIDOS_RUTA_PICKING.CodPedido " & vbNewLine & _
'    "from MA_PEDIDOS_RUTA_PICKING inner join MA_PRODUCTOS on MA_PRODUCTOS.C_CODIGO " & vbNewLine & _
'    "= MA_PEDIDOS_RUTA_PICKING.CodProducto left join MA_UBICACIONxPRODUCTO on " & vbNewLine & _
'    "MA_UBICACIONxPRODUCTO.CU_PRODUCTO = MA_PEDIDOS_RUTA_PICKING.CodProducto " & vbNewLine & _
'    "left join MA_Codigos on MA_Codigos.c_codnasa = MA_PRODUCTOS.C_Codigo and  " & vbNewLine & _
'    "MA_Codigos.nu_intercambio = 1 where MA_PEDIDOS_RUTA_PICKING.CodRecolector = '" & lccodusuario & "'" & vbNewLine & _
'    "and MA_PEDIDOS_RUTA_PICKING.Picking = 0 " & vbNewLine & _
'    OrderBy
    
'    SQL = "select isNull(MA_Codigos.C_Codigo,MA_PRODUCTOS.C_Codigo) as CodEDI, " & vbNewLine & _
'    "MA_PEDIDOS_RUTA_PICKING.CodProducto, MA_PRODUCTOS.C_DESCRI as Nombre, " & vbNewLine & _
'    "isNull(MA_UBICACIONxPRODUCTO.CU_MASCARA,'N/A') as Ubicacion, " & vbNewLine & _
'    "MA_PEDIDOS_RUTA_PICKING.CodLote, SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada) as CantSolicitada, " & vbNewLine & _
'    "SUM(MA_PEDIDOS_RUTA_PICKING.CantRecolectada) as CantRecolectada " & vbNewLine & _
'    "from MA_PEDIDOS_RUTA_PICKING inner join MA_PRODUCTOS on MA_PRODUCTOS.C_CODIGO " & vbNewLine & _
'    "= MA_PEDIDOS_RUTA_PICKING.CodProducto left join MA_UBICACIONxPRODUCTO on " & vbNewLine & _
'    "MA_UBICACIONxPRODUCTO.CU_PRODUCTO = MA_PEDIDOS_RUTA_PICKING.CodProducto " & vbNewLine & _
'    "left join MA_Codigos on MA_Codigos.c_codnasa = MA_PRODUCTOS.C_Codigo and  " & vbNewLine & _
'    "MA_Codigos.nu_intercambio = 1 where MA_PEDIDOS_RUTA_PICKING.CodRecolector = '" & lccodusuario & "'" & vbNewLine & _
'    "and MA_PEDIDOS_RUTA_PICKING.Picking = 0 " & vbNewLine & _
'    "group by MA_PRODUCTOS.C_CODIGO,MA_Codigos.c_codigo,MA_PEDIDOS_RUTA_PICKING.CodProducto,MA_PRODUCTOS.C_DESCRI,MA_UBICACIONxPRODUCTO.CU_MASCARA,MA_PEDIDOS_RUTA_PICKING.CodLote " & vbNewLine & _
'    OrderBy
    
'    SQL = "select  MAX(CodEdi) as CodEdi, CodProducto, MAX(Nombre) as Nombre, MAX(Ubicacion) as Ubicacion, MAX(CodLote) as codLote, SUM(CantSolicitada) as CantSolicitada," & vbNewLine & _
'    "SUM(CantRecolectada) as CantRecolectada, SUM(CantPedidos) as CantPedidos from ( " & vbNewLine & _
'    "select isNull(MA_Codigos.C_Codigo,MA_PRODUCTOS.C_Codigo) as CodEDI, " & vbNewLine & _
'    "MA_PEDIDOS_RUTA_PICKING.CodProducto, MA_PRODUCTOS.C_DESCRI as Nombre, " & vbNewLine & _
'    "isNull(MA_UBICACIONxPRODUCTO.CU_MASCARA,'N/A') as Ubicacion, " & vbNewLine & _
'    "MA_PEDIDOS_RUTA_PICKING.CodLote, SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada) as CantSolicitada, " & vbNewLine & _
'    "SUM(MA_PEDIDOS_RUTA_PICKING.CantRecolectada) as CantRecolectada, 0 as cantPedidos " & vbNewLine & _
'    "from MA_PEDIDOS_RUTA_PICKING inner join MA_PRODUCTOS on MA_PRODUCTOS.C_CODIGO " & vbNewLine & _
'    "= MA_PEDIDOS_RUTA_PICKING.CodProducto left join MA_UBICACIONxPRODUCTO on " & vbNewLine & _
'    "MA_UBICACIONxPRODUCTO.CU_PRODUCTO = MA_PEDIDOS_RUTA_PICKING.CodProducto " & vbNewLine & _
'    "left join MA_Codigos on MA_Codigos.c_codnasa = MA_PRODUCTOS.C_Codigo and  " & vbNewLine & _
'    "MA_Codigos.nu_intercambio = 1 where MA_PEDIDOS_RUTA_PICKING.CodRecolector = '" & lccodusuario & "'" & vbNewLine & _
'    "and MA_PEDIDOS_RUTA_PICKING.Picking = 0 " & vbNewLine & _
'    "group by MA_PRODUCTOS.C_CODIGO,MA_Codigos.c_codigo,MA_PEDIDOS_RUTA_PICKING.CodProducto,MA_PRODUCTOS.C_DESCRI,MA_UBICACIONxPRODUCTO.CU_MASCARA,MA_PEDIDOS_RUTA_PICKING.CodLote,CodRecolector, Picking " & vbNewLine & _
'    " UNION " & vbNewLine & _
'    "(select '' as CodEdi, CodProducto, '' as Nombre, '' as Ubicacion, '' as CodLote, 0 as CantSolicitada, 0 as CantRecolectada " & vbNewLine & _
'    ",count(*) as cantPedidos from MA_PEDIDOS_RUTA_PICKING M where M.CodRecolector = '9999999999'" & vbNewLine & _
'    "and M.Picking = 0 group by CodProducto) ) as tb group by CodProducto" & vbNewLine & _
'    OrderBy
    
    mCampoUbicaciones = "REPLACE(isNULL(REVERSE(SUBSTRING(REVERSE((SELECT UBC2.cu_Mascara + '[VBNEWLINE]' FROM MA_UBICACIONxPRODUCTO UBC2 WHERE UBC2.cu_Deposito + UBC2.cu_Producto = c_CodDeposito + CodProducto FOR XML PATH(''))), 1 + LEN('[VBNEWLINE]'), 9999)), 'N/A'), '&#x20', '') AS Ubicacion"
    
    SQL = "SELECT MAX(CodEdi) AS CodEdi, CodProducto, MAX(Nombre) AS Nombre, MAX(Ubicacion) AS Ubicacion, MAX(CodLote) AS codLote, SUM(CantSolicitada) AS CantSolicitada," & vbNewLine & _
    "SUM(CantRecolectada) as CantRecolectada, SUM(CantPedidos) as CantPedidos, CantDecimales FROM (" & vbNewLine & _
    "SELECT CodEDI,CodProducto, Nombre, " & mCampoUbicaciones & ", CodLote, CantSolicitada, CantRecolectada, CantPedidos, CantDecimales FROM (" & vbNewLine & _
    "SELECT isNull(MA_Codigos.C_Codigo, MA_PRODUCTOS.C_Codigo) as CodEDI, " & vbNewLine & _
    "MA_PEDIDOS_RUTA_PICKING.CodProducto, MA_PRODUCTOS.C_DESCRI as Nombre, " & vbNewLine & _
    "MA_PEDIDOS_RUTA_PICKING.CodLote, SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada) AS CantSolicitada, " & vbNewLine & _
    "SUM(MA_PEDIDOS_RUTA_PICKING.CantRecolectada) as CantRecolectada, 0 AS CantPedidos," & vbNewLine & _
    "MA_PRODUCTOS.Cant_Decimales AS CantDecimales, c_CodDeposito " & vbNewLine & _
    "FROM MA_PEDIDOS_RUTA_PICKING INNER JOIN MA_VENTAS " & vbNewLine & _
    "ON MA_PEDIDOS_RUTA_PICKING.CodPedido = MA_VENTAS.c_DOCUMENTO AND MA_VENTAS.c_CONCEPTO = 'PED'" & vbNewLine & _
    "inner join MA_PRODUCTOS on MA_PRODUCTOS.C_CODIGO " & vbNewLine & _
    "= MA_PEDIDOS_RUTA_PICKING.CodProducto " & vbNewLine & _
    "left join MA_Codigos on MA_Codigos.c_codnasa = MA_PRODUCTOS.C_Codigo and  " & vbNewLine & _
    "MA_Codigos.nu_intercambio = 1 where MA_PEDIDOS_RUTA_PICKING.CodRecolector = '" & LcCodUsuario & "'" & vbNewLine & _
    "AND MA_PEDIDOS_RUTA_PICKING.Picking = 0 " & vbNewLine & _
    "GROUP BY MA_PRODUCTOS.C_CODIGO, MA_Codigos.c_Codigo, MA_PEDIDOS_RUTA_PICKING.CodProducto, MA_PRODUCTOS.C_DESCRI, MA_PEDIDOS_RUTA_PICKING.CodLote, CodRecolector, Picking, MA_PRODUCTOS.Cant_Decimales, c_CodDeposito " & vbNewLine & _
    ") AS Todo GROUP BY Nombre, CodEDI, CantSolicitada, CantRecolectada, CodProducto, CodLote, CantPedidos, CantDecimales, c_CodDeposito " & vbNewLine & _
    " UNION " & vbNewLine & _
    "(SELECT '' AS CodEdi, CodProducto, '' AS Nombre, '' AS Ubicacion, '' AS CodLote, 0 AS CantSolicitada, 0 AS CantRecolectada " & vbNewLine & _
    ", COUNT(*) AS CantPedidos, P.Cant_Decimales AS CantDecimales FROM MA_PEDIDOS_RUTA_PICKING M INNER JOIN MA_PRODUCTOS P ON M.CodProducto = P.c_Codigo WHERE M.CodRecolector = '" & LcCodUsuario & "'" & vbNewLine & _
    "AND M.Picking = 0 GROUP BY CodProducto, P.Cant_Decimales) ) AS TB GROUP BY CodProducto, CantDecimales" & vbNewLine & _
    OrderBy
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        Salir = True
    End If
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    While Not Rs.EOF
        
        Grid.Rows = Grid.Rows + 1
        'GRID.TextMatrix(GRID.Rows - 1, 0) = Rs!CodLote
        'GRID.TextMatrix(GRID.Rows - 1, 1) = Rs!CodPedido
        Grid.TextMatrix(Grid.Rows - 1, 2) = Rs!CodEDI
        Grid.TextMatrix(Grid.Rows - 1, 3) = Rs!Nombre
        
        TmpUbc = Rs!Ubicacion
        Ubc = vbNullString
        
        Do While (TmpUbc Like "*[[]VBNEWLINE[]]*")
            
            TmpNumChars = InStr(1, TmpUbc, "[VBNEWLINE]")
            TmpNumChars = TmpNumChars - 1
            TmpItmUbc = Mid(TmpUbc, 1, TmpNumChars)
            TmpUbc = Mid(TmpUbc, TmpNumChars + Len("[VBNEWLINE]") + 1)
            
            TmpMaxLen = 12
            
            If Len(TmpItmUbc) <= TmpMaxLen Then
                RemChar = TmpMaxLen - Len(TmpItmUbc)
                TmpItmUbc = String(Int(RemChar / 2), " ") & TmpItmUbc & String(Int(RemChar / 2), " ")
                'TmpItmUbc = 'Rellenar_SpaceR(TmpItmUbc, 15, " ")
            End If
            
            Ubc = Ubc & TmpItmUbc
            
        Loop
        
        Ubc = Ubc & TmpUbc
        
        TmpCantLn = (Len(Rs!Ubicacion) - _
        Len(Replace(Rs!Ubicacion, "[VBNEWLINE]", vbNullString))) / Len("[VBNEWLINE]")
        
        If TmpCantLn >= 2 Then
            Grid.RowHeight(Grid.Rows - 1) = (Grid.RowHeight(Grid.Rows - 1) * ((TmpCantLn + 1) / 2))
        End If
        
        Grid.TextMatrix(Grid.Rows - 1, 4) = Ubc 'Rs!Ubicacion
        Grid.RowData(Grid.Rows - 1) = Grid.RowHeight(Grid.Rows - 1)
        
        Grid.TextMatrix(Grid.Rows - 1, 5) = FormatNumber(Rs!CantSolicitada, Rs!CantDecimales, vbTrue, vbFalse, True)
        Grid.TextMatrix(Grid.Rows - 1, 6) = FormatNumber(Rs!CantRecolectada, Rs!CantDecimales, vbTrue, vbFalse, True)
        Grid.TextMatrix(Grid.Rows - 1, 8) = Rs!CodProducto
        Grid.TextMatrix(Grid.Rows - 1, 9) = Rs!CantPedidos
        Grid.TextMatrix(Grid.Rows - 1, 10) = Rs!CantDecimales
        
        If Rs!CantRecolectada > 0 Then
            For I = 0 To Grid.Cols - 1
                Grid.Row = Grid.Rows - 1
                Grid.Col = I
                Grid.CellBackColor = -2147483624   'Amarillo
            Next I
        End If
        
        Rs.MoveNext
        
    Wend
    
    Rs.Close
    
    If Grid.Row > 1 Then
        Grid.Row = 1
        Fila = 1
        Grid.ColSel = 6
    End If
    
    If Grid.Rows > 13 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(3) = AnchoCampoDescripcion - 675
    Else
        Grid.ScrollBars = flexScrollBarNone
        ScrollGrid.Visible = False
    End If
    
    SQL = "SELECT Descripcion " & _
    "FROM MA_USUARIOS " & _
    "WHERE CodUsuario = '" & LcCodUsuario & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    LabelTitulo.Caption = "Consola de Recolecci�n - Usuario: " & Rs!Descripcion
    
    Grid_EnterCell
    
    If gRutinas.PuedeObtenerFoco(Cod_Buscar) Then Cod_Buscar.SetFocus
    
Finally:
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub FramePickAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    CmdPickAll_MouseUp Button, Shift, X, Y
End Sub

Private Sub CmdPickAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_KeyDown vbKeyF5, 0
End Sub

Private Sub FrameCheckAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    CmdCheckAll_MouseUp Button, Shift, X, Y
End Sub

Private Sub CmdCheckAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_KeyDown vbKeyF6, 0
End Sub

Private Sub Grid_DblClick()
    
    On Error GoTo Error1
        
    Dim Resultado As Variant, NewCant As Double
    
    Dim CantSolicitada As Double, CantRecolectada As Double
    
    CantSolicitada = CDbl(Grid.TextMatrix(Grid.RowSel, 5))
    CantRecolectada = CDbl(Grid.TextMatrix(Grid.RowSel, 6))
    
    If CheckAll Then
        NewCant = CantSolicitada
    Else
        FrmNumPadGeneral.ValorOriginal = CantRecolectada
        FrmNumPadGeneral.Show vbModal
        NewCant = FrmNumPadGeneral.ValorNumerico
        Set FrmNumPadGeneral = Nothing
    End If
    
    'If NewCant = 0 Then ' Comentado... y si en verdad no hay...
        'Mensaje True, "La cantidad recolectada no puede ser cero (0)."
        'Exit Sub
    'End If
    
    'If NewCant > 0 Then
    
        Dim SQL As String
        Dim CantDisponible As Double
        
        Dim CantDecimalesProducto As Long
        
        CantDecimalesProducto = SVal(Grid.TextMatrix(Grid.RowSel, 10))
        
        CantDisponible = Round(NewCant, CantDecimalesProducto)
        
        Dim SQLRecolector As String
        Dim Rs As Recordset
        
        'Validaciones
        
        If NewCant > CantSolicitada Then
            If Not CheckAll Then
                Mensaje True, "La cantidad recolectada no puede ser mayor a la cantidad solicitada."
            End If
            Exit Sub
        End If
        
        If (CantRecolectada > NewCant And Not CheckAll) Then
            
            Mensaje False, "La cantidad actual de productos recolectados es de " & _
            CantRecolectada & " �Seguro que desea disminuirla a " & CantDisponible & "?"
            
            If Not Retorno Then Exit Sub
            
        End If
            
        'Grabar
        
        Dim Trans As Boolean
        
        Ent.BDD.BeginTrans
        Trans = True
        
        Set Rs = New Recordset
        
        SQLRecolector = "SELECT * FROM MA_PEDIDOS_RUTA_PICKING " & _
        "WHERE CodProducto = '" & Grid.TextMatrix(Grid.RowSel, 8) & "'" & vbNewLine & _
        "AND CodRecolector = '" & LcCodUsuario & "' AND Picking = 0 ORDER BY FechaAsignacion ASC"
        'consulta
        
        Rs.Open SQLRecolector, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        Do While (Not Rs.EOF)
            
            'If (CantDisponible = 0) Then ' Comentado en caso de que haya que cambiar la cantidad hacia abajo...
                'Exit Do
            'End If
            
            If (CantDisponible >= Rs!CantSolicitada) Then
                
                SQL = "UPDATE MA_PEDIDOS_RUTA_PICKING SET " & _
                "CantRecolectada = " & CDec(Rs!CantSolicitada) & " " & vbNewLine & _
                "WHERE CodLote = '" & Rs!CodLote & "' " & _
                "AND CodPedido = '" & Rs!CodPedido & "' " & vbNewLine & _
                "AND CodProducto = '" & Grid.TextMatrix(Grid.RowSel, 8) & "' "
                
                Ent.BDD.Execute SQL
                
                CantDisponible = Round((CantDisponible - Rs!CantSolicitada), CantDecimalesProducto)
                
            Else
                
                SQL = "UPDATE MA_PEDIDOS_RUTA_PICKING SET " & _
                "CantRecolectada = " & CDec(CantDisponible) & " " & vbNewLine & _
                "WHERE CodLote = '" & Rs!CodLote & "' " & _
                "AND CodPedido = '" & Rs!CodPedido & "' " & vbNewLine & _
                "AND CodProducto = '" & Grid.TextMatrix(Grid.RowSel, 8) & "' "
                
                Ent.BDD.Execute SQL
                
                CantDisponible = 0
                
            End If
            
            Rs.MoveNext
            
        Loop
        
        If Trans Then
            Ent.BDD.CommitTrans
            Trans = False
        End If
        
        If Not CheckAll Then
            Call ButtonActualizar_Click
        End If
        
    'End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If Trans Then
        Ent.BDD.RollbackTrans
        Trans = False
    End If
    
    If Not CheckAll Then
        
        Mensaje True, "Error al actualizar cantidad recolectada, " & _
        "Descripci�n " & mErrorDesc & " (" & mErrorNumber & ")"
        
        Call PrepararGrid
        Call PrepararDatos
        
    End If
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Grid_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyF5 And Shift = 0 Then
        
        If Grid.Rows > 1 Then
            
            Mensaje False, "Atenci�n! �Est� seguro de establecer " & _
            "la cantidad m�xima como recolectada para todos los productos?" & _
            GetLines & "En tal caso, presione Aceptar para continuar"
            
            If Retorno Then
                
                CheckAll = True
                
                For I = 1 To Grid.Rows - 1
                    Grid.Row = I
                    Grid.RowSel = Grid.Row
                    Grid_DblClick
                Next I
                
                CheckAll = False
                
                ButtonActualizar_Click
                
            End If
            
        End If
        
    ElseIf KeyCode = vbKeyF6 And Shift = 0 Then
        
        If Grid.Rows > 1 Then
            
            Mensaje False, "Atenci�n! �Est� seguro de finalizar la recolecci�n " & _
            "de todos los productos? En tal caso, presione Aceptar para continuar " & _
            "o Cancelar si desea verificar los datos."
            
            If Retorno Then
                
                CheckAll = True
                
                For I = 1 To Grid.Rows - 1
                    Grid.Row = I
                    Grid.RowSel = Grid.Row
                    CmdSelect_Click
                Next I
                
                CheckAll = False
                
                ButtonActualizar_Click
                
            End If
            
        End If
        
    End If
    
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call Grid_DblClick
    End If
End Sub

Private Sub ButtonActualizar_Click()
    Call PrepararGrid
    Call PrepararDatos
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub Grid_EnterCell()
        
    If Grid.Rows = 1 Then
        FrameSelect.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = Grid.RowData(Fila) '600
            End If
            
            Grid.RowHeight(Grid.Row) = Grid.RowData(Grid.Row) + 120 '720
            Fila = Grid.Row
            
        End If
        
        MostrarEditorTexto2 Me, Grid, CmdSelect
        Grid.ColSel = 0
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        
    Else
        Band = False
    End If
    
End Sub

Public Sub MostrarEditorTexto2(pFrm As Form, pGrd As Object, ByRef txteditor As Object)
    
    On Error Resume Next
    
    With Grid
        
        .RowSel = .Row
        
        If .Col <> 7 Then Band = True
        .Col = 7
        
        FrameSelect.BackColor = pGrd.BackColorSel
        FrameSelect.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
        FrameSelect.Visible = True: CmdSelect.Visible = True
        FrameSelect.ZOrder
        
        cellRow = .Row
        cellCol = .Col
        
     End With
     
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Private Sub CmdSelect_Click()
    
    On Error GoTo Error1
    
    Dim Cant As Double
    Dim FinalizarCantidadCero As Boolean
    Dim CantPedidos As Double
    Dim CantSolicitada As Double
    Dim Trans As Boolean
    
    Dim CantDecimalesProducto As Long
    
    CantDecimalesProducto = SVal(Grid.TextMatrix(Grid.RowSel, 10))
    
    Ent.BDD.BeginTrans: Trans = True
    
    'CantPedidos = GRID.TextMatrix(GRID.RowSel, 9)
    CantPedidos = Grid.TextMatrix(Grid.RowSel, 6)
    
    'Grid.TextMatrix(Grid.RowSel, 5) Solicitada
    'Grid.TextMatrix(Grid.RowSel, 6) Recogida
    
    CantSolicitada = CDbl(Grid.TextMatrix(Grid.RowSel, 5))
    
    Cant = Round((CantSolicitada - CantPedidos), CantDecimalesProducto)
    
    If CantPedidos = 0 Then
        
        If CheckAll Then
            Retorno = True
        Else
            Mensaje False, "�Est� seguro de finalizar la recolecci�n " & _
            "de este producto con una cantidad igual a cero (0)?"
        End If
        
        If Retorno Then
            FinalizarCantidadCero = True
        End If
        
    End If
    
    If (CantPedidos < CantSolicitada) _
    And Not FinalizarCantidadCero Then
        
        If CheckAll Then
            Retorno = True
        Else
            Mensaje False, "�Est� seguro de finalizar la recolecci�n " & _
            "de este producto con una cantidad faltante de productos (Falta(n) " & Cant & ") ?"
        End If
        
        If Not Retorno Then
            Ent.BDD.RollbackTrans
            Exit Sub
        End If
        
    End If
    
    Dim mRs As Recordset
    
    Set mRs = New Recordset
    
    SQL = "SELECT * FROM MA_PEDIDOS_RUTA_PICKING " & _
    "WHERE CodProducto = '" & Grid.TextMatrix(Grid.RowSel, 8) & "' " & vbNewLine & _
    "AND CodRecolector = '" & LcCodUsuario & "' " & _
    "AND Picking = 0 " & _
    "ORDER BY FechaAsignacion ASC "
    
    mRs.CursorLocation = adUseClient
    mRs.Open SQL, Ent.BDD, adOpenStatic, adLockOptimistic
    
    Set mRs.ActiveConnection = Nothing
    
    If Not FinalizarCantidadCero Then
        
        mRs.Filter = "CantRecolectada = 0"
        
        If Not mRs.EOF Then
            
            While Not mRs.EOF
                
                mRs.Delete
                mRs.MoveNext
                
            Wend
            
        End If
        
    End If
    
    mRs.Filter = "" 'vbNullString
    
    If mRs.RecordCount > 0 Then
        mRs.MoveFirst
    End If
    
    While Not mRs.EOF
        
        If CantPedidos = 0 Then
            
            SQL = "UPDATE MA_PEDIDOS_RUTA_PICKING SET " & _
            "CantRecolectada = 0 " & vbNewLine & _
            "WHERE CodLote = '" & mRs!CodLote & "' " & _
            "AND CodPedido = '" & mRs!CodPedido & "' " & vbNewLine & _
            "AND CodProducto = '" & Grid.TextMatrix(Grid.RowSel, 8) & "' "
            
            Ent.BDD.Execute SQL
            'Ent.BDD.RollbackTrans
            'Exit Sub
            
        End If
        
        SQL = "UPDATE MA_PEDIDOS_RUTA_PICKING SET " & _
        "Picking = 1 " & vbNewLine & _
        "WHERE CodLote = '" & mRs!CodLote & "' " & _
        "AND CodPedido = '" & mRs!CodPedido & "' " & vbNewLine & _
        "AND CodProducto = '" & Grid.TextMatrix(Grid.RowSel, 8) & "' "
        
        Ent.BDD.Execute SQL
        
        'CantPedidos = (CantPedidos - 1)
        mRs.MoveNext
        
    Wend
    
    If Trans Then
        Ent.BDD.CommitTrans
        Trans = False
    End If
    
    If Not CheckAll Then
        Call ButtonActualizar_Click
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If Trans Then
        Ent.BDD.RollbackTrans
        Trans = False
    End If
    
    If Not CheckAll Then
        
        Mensaje True, "Error al finalizar esta recolecci�n, " & _
        "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
        
        Call PrepararGrid
        Call PrepararDatos
        
    End If
    
End Sub

Private Sub Chk_Ubicacion_Click()
    If Chk_Ubicacion.Value = vbChecked Then
        OrderBy = "ORDER BY isNull(MAX(Ubicacion), 'N/A')"
        Chk_Pedido.Value = vbUnchecked
        Chk_Lote.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Pedido_Click()
    If Chk_Pedido.Value = vbChecked Then
        OrderBy = "ORDER BY MA_PEDIDOS_RUTA_PICKING.CodPedido"
        Chk_Lote.Value = vbUnchecked
        Chk_Ubicacion.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Lote_Click()
    If Chk_Lote.Value = vbChecked Then
        OrderBy = "ORDER BY MA_PEDIDOS_RUTA_PICKING.CodLote"
        Chk_Pedido.Value = vbUnchecked
        Chk_Ubicacion.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_NombreProducto_Click()
    If Chk_NombreProducto.Value = vbChecked Then
        OrderBy = "ORDER BY isNull(MAX(Nombre), 'N/A')"
        Chk_Pedido.Value = vbUnchecked
        Chk_Ubicacion.Value = vbUnchecked
        Chk_Lote.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Cod_Buscar_GotFocus()
    Cod_Buscar.SelStart = 0
    Cod_Buscar.SelLength = Len(Cod_Buscar)
End Sub

Private Sub Cod_Buscar_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        
        Dim SQL As String, Rs As New ADODB.Recordset, I As Integer, Band As Integer
        
        SQL = "SELECT c_CodNasa " & _
        "FROM MA_CODIGOS " & _
        "WHERE c_Codigo = '" & FixTSQL(Trim(Cod_Buscar.Text)) & "' "
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        If Not Rs.EOF Then
            
            Cod_Buscar = Rs!c_CodNasa
            
            Band = 0
            
            For I = 1 To Grid.Rows - 1
                
                If UCase(Grid.TextMatrix(I, 8)) = UCase(Cod_Buscar) Then
                    
                    Band = I
                    
                    If Grid.TextMatrix(I, 5) <> Grid.TextMatrix(I, 6) Then
                        
                        Grid.Row = Band
                        
                        Cod_Buscar = Empty
                        
                        Call Grid_DblClick
                        
                        Exit Sub
                        
                    End If
                
                End If
                
            Next I
            
            If Band > 0 Then
                Call Mensaje(True, "El producto ya fue recolectado.")
            Else
                Call Mensaje(True, "El producto no se encontr�.")
            End If
            
        Else
            Call Mensaje(True, "C�digo no encontrado, verifique el c�digo a buscar.")
                If gRutinas.PuedeObtenerFoco(Cod_Buscar) Then Cod_Buscar.SetFocus
            Exit Sub
        End If
        
        Cod_Buscar = Empty
        
    End If
    
End Sub

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute( _
    "SELECT c_Proceso " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE c_Proceso = '008'")
    
    If Not TempRs.EOF Then frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub
