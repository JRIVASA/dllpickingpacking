VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmPackingUser 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   7260
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   7260
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdReimprimir 
      Caption         =   "Reimprimir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   4440
      Picture         =   "FrmPackingUser.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   17
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9480
      Width           =   1335
   End
   Begin VB.CheckBox Chk_Pedido 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Pedido"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   360
      TabIndex        =   14
      Top             =   8880
      Value           =   1  'Checked
      Width           =   1545
   End
   Begin VB.CheckBox Chk_NombreProducto 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Art�culos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   5160
      TabIndex        =   13
      Top             =   8880
      Width           =   1425
   End
   Begin VB.CheckBox Chk_Fecha 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Fecha"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   420
      Left            =   2640
      TabIndex        =   12
      Top             =   8760
      Width           =   1425
   End
   Begin VB.CommandButton ButtonActualizar 
      Caption         =   "Actualizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   5880
      Picture         =   "FrmPackingUser.frx":4F18
      Style           =   1  'Graphical
      TabIndex        =   5
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9480
      Width           =   1215
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   7050
      LargeChange     =   10
      Left            =   6480
      TabIndex        =   4
      Top             =   600
      Width           =   675
   End
   Begin VB.Frame FrameTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9720
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   4680
         TabIndex        =   3
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   6720
         Picture         =   "FrmPackingUser.frx":6C9A
         Top             =   0
         Width           =   480
      End
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Consola de Empacado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   120
         Width           =   4575
      End
   End
   Begin VB.TextBox txtminutos 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   2760
      TabIndex        =   0
      Text            =   "1"
      Top             =   9960
      Width           =   275
   End
   Begin VB.Timer Timer1 
      Interval        =   500
      Left            =   4200
      Top             =   8640
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   7065
      Left            =   120
      TabIndex        =   6
      Top             =   600
      Width           =   7005
      _ExtentX        =   12356
      _ExtentY        =   12462
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ProgressBar bar 
      Height          =   255
      Left            =   3600
      TabIndex        =   7
      Top             =   7920
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.Label C�digo 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Criterios de Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Index           =   71
      Left            =   240
      TabIndex        =   15
      Top             =   8400
      Width           =   2805
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00AE5B00&
      X1              =   3240
      X2              =   6840
      Y1              =   8520
      Y2              =   8520
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "minutos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3240
      TabIndex        =   10
      Top             =   9960
      Width           =   855
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Intervalo de actualizar: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   9960
      Width           =   2535
   End
   Begin VB.Shape Shape1 
      Height          =   240
      Index           =   4
      Left            =   600
      Top             =   7920
      Visible         =   0   'False
      Width           =   6255
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Pr�xima actualizaci�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   600
      TabIndex        =   8
      Top             =   7920
      Width           =   2775
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   735
      Index           =   0
      Left            =   120
      TabIndex        =   11
      Top             =   9720
      Width           =   4215
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   975
      Index           =   1
      Left            =   120
      TabIndex        =   16
      Top             =   8280
      Width           =   7005
   End
End
Attribute VB_Name = "FrmPackingUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public PackingMobile As Boolean

Private Salir As Variant
Private OrderBy As String

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub LabelTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub CmdReimprimir_Click()
    
    Set Frm_Super_Consultas = Nothing
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    mFrm_Super_Consultas.Inicializar _
    "SELECT TOP 200 Pack.*, CONVERT(NVARCHAR(30), " & _
    "CAST(NDE.n_Total AS Money), 1) AS TotalNDE, NDE.c_Documento AS DocNDE, " & _
    "CONVERT(NVARCHAR(MAX), Pack.FechaEmpacado, 112) AS FechaISO " & _
    "FROM MA_PEDIDOS_RUTA_PACKING Pack " & _
    "INNER JOIN MA_VENTAS NDE " & _
    "ON NDE.c_Relacion = 'PED ' + CodPedido " & _
    "AND NDE.c_Concepto = 'NDE' ", _
    " N O T A S  D E  E N T R E G A ", Ent.BDD
    
    mFrm_Super_Consultas.Add_ItemLabels "FechaISO", "FechaISO", 0, AlignmentConstants.vbLeftJustify
    mFrm_Super_Consultas.Add_ItemLabels "DocNDE", "DocNDE", 0, AlignmentConstants.vbLeftJustify
    mFrm_Super_Consultas.Add_ItemLabels "Lote", "CodLote", 2000, AlignmentConstants.vbCenter
    mFrm_Super_Consultas.Add_ItemLabels "Pedido", "CodPedido", 2000, AlignmentConstants.vbCenter
    mFrm_Super_Consultas.Add_ItemLabels "Fecha", "FechaEmpacado", 3480, _
    AlignmentConstants.vbCenter, , Array("Fecha", "VbGeneralDate")
    mFrm_Super_Consultas.Add_ItemLabels "Total NDE", "TotalNDE", 2040, _
    AlignmentConstants.vbRightJustify, , Array("Numerico", 2, 20)
    mFrm_Super_Consultas.Add_ItemLabels "Empaques", "CantEmpaques", 1590, _
    AlignmentConstants.vbRightJustify, , Array("Numerico", 0, 20)
    
    mFrm_Super_Consultas.Add_ItemSearching "Lote", "CodLote"
    mFrm_Super_Consultas.Add_ItemSearching "Pedido", "CodPedido"
    mFrm_Super_Consultas.Add_ItemSearching "Empaques", "CantEmpaques"
    mFrm_Super_Consultas.Add_ItemSearching "YYYYMMDD", "CONVERT(NVARCHAR(MAX), FechaEmpacado, 112)"
    
    mFrm_Super_Consultas.StrOrderBy = "FechaEmpacado Desc"
    mFrm_Super_Consultas.BusquedaInstantanea = True
    mFrm_Super_Consultas.txtDato.Text = "%"
    
    mFrm_Super_Consultas.Show vbModal
    
    mArr = mFrm_Super_Consultas.ArrResultado
    
    Set mFrm_Super_Consultas = Nothing
    
    If Not IsEmpty(mArr) Then
        
        If Trim(mArr(0)) <> Empty Then
            
            FrmPackingUserDetails.PedidoNDE = mArr(1)
            FrmPackingUserDetails.Lote = mArr(2)
            FrmPackingUserDetails.Pedido = mArr(3)
            
            Call FrmPackingUserDetails.ImprimirNotaDeEntrega(mArr(6), True)
            
            Set FrmPackingUserDetails = Nothing
            
            Call ButtonActualizar_Click
            
        End If
        
    End If
    
End Sub

Private Sub Form_Activate()
    
    If Salir Then
        
        Mensaje True, "No hay pedidos pendientes por empacar."
        
        Unload Me
        
        Exit Sub
        
    End If
    
End Sub

Private Sub Form_Load()
    
    AjustarPantalla Me
    
    Salir = False
    
    'OrderBy = "ORDER BY CodPedido"
    'Call PrepararGrid
    'Call PrepararDatos
    
    ' El ordenamiento por defecto deber�a ser por fecha...
    
    If Chk_Fecha.Value = vbUnchecked Then
        Chk_Fecha.Value = vbChecked
    Else
        Chk_Fecha_Click
    End If
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionByRow
        
        .Rows = 2
        .FixedCols = 0
        .FixedRows = 1
        
        .Rows = 1
        .Cols = 6
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        .FormatString = "Pedido" & "|" & "Fecha" & "|" & _
        "Articulos" & "||" & "CodEmpacador" & "|" & _
        "CantEmpaquesMobile"
        
        .ColWidth(0) = 2200
        .ColAlignment(0) = flexAlignCenterCenter
        
        .ColWidth(1) = 3300
        .ColAlignment(1) = flexAlignCenterCenter
        
        .ColWidth(2) = 1500
        .ColAlignment(2) = flexAlignCenterCenter
        
        .ColWidth(3) = 0
        .ColWidth(4) = 0
        .ColWidth(5) = 0
        
        .Width = 7000
        
        .ScrollTrack = True
        
        .Row = 0
        .Col = 2
        .ColSel = 3
        
    End With
    
    For I = 0 To Grid.Cols - 1
        Grid.Col = I
        Grid.CellAlignment = flexAlignCenterCenter
    Next
    
End Sub

Private Sub PrepararDatos()
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset
    
    If PackingMobile Then
        
        SQL = "SELECT CodLote, CodPedido, Min(FechaAsignacion) as FechaAsignacion, " & vbNewLine & _
        "SUM(CantRecolectada) AS Articulos, MAX(CodEmpacador) AS CodEmpacador, " & vbNewLine & _
        "MAX(PackingMobile_CantEmpaques) AS EmpaquesMobile " & vbNewLine & _
        "FROM MA_PEDIDOS_RUTA_PICKING " & vbNewLine & _
        "WHERE Picking = 1 " & _
        "AND (PackingMobile_CantEmpaques > 0) " & _
        "AND Packing = 0 " & vbNewLine & _
        "GROUP BY CodPedido, CodLote " & OrderBy
        
    Else
        
        SQL = "SELECT CodLote, CodPedido, Min(FechaAsignacion) as FechaAsignacion, " & vbNewLine & _
        "SUM(CantRecolectada) AS Articulos, CodEmpacador, 0 AS EmpaquesMobile " & vbNewLine & _
        "FROM MA_PEDIDOS_RUTA_PICKING " & vbNewLine & _
        "WHERE CodEmpacador = '" & LcCodUsuario & "' " & _
        "AND Picking = 1 " & _
        "AND Packing = 0 " & vbNewLine & _
        "GROUP BY CodPedido, CodLote, CodEmpacador " & OrderBy
        
    End If
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        'jesus comentado para que no se salga
        'Salir = True
    End If
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    Grid.Visible = False
    
    While Not Rs.EOF
        
        Grid.Rows = Grid.Rows + 1
        
        Grid.TextMatrix(Grid.Rows - 1, 0) = Rs!CodPedido
        Grid.TextMatrix(Grid.Rows - 1, 1) = Rs!FechaAsignacion
        Grid.TextMatrix(Grid.Rows - 1, 2) = Rs!Articulos
        Grid.TextMatrix(Grid.Rows - 1, 3) = Rs!CodLote
        Grid.TextMatrix(Grid.Rows - 1, 4) = Rs!CodEmpacador
        Grid.TextMatrix(Grid.Rows - 1, 5) = Rs!EmpaquesMobile
        
        Rs.MoveNext
        
    Wend
    
    Rs.Close
    
    If Grid.Rows > 12 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(1) = 4000 - 675
    Else
        Grid.ScrollBars = flexScrollBarNone
        ScrollGrid.Visible = False
    End If
    
    SQL = "SELECT Descripcion " & _
    "FROM MA_USUARIOS " & _
    "WHERE CodUsuario = '" & LcCodUsuario & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If PackingMobile Then
        LabelTitulo.Caption = "Consola de Empacado - Pedidos M�viles. " & Rs!Descripcion
    Else
        LabelTitulo.Caption = "Consola de Empacado de " & Rs!Descripcion
    End If
    
    Rs.Close
    
Finally:
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub Grid_DblClick()
    
    If Grid.TextMatrix(Grid.RowSel, 0) <> Empty Then
        
        FrmPackingUserDetails.Pedido = Grid.TextMatrix(Grid.RowSel, 0)
        FrmPackingUserDetails.Lote = Grid.TextMatrix(Grid.RowSel, 3)
        
        If PackingMobile Then
            FrmPackingUserDetails.EmpaquesMobile = SVal(Grid.TextMatrix(Grid.RowSel, 5))
            FrmPackingUserDetails.CodEmpacadorMobile = Grid.TextMatrix(Grid.RowSel, 4)
        End If
        
        FrmPackingUserDetails.Show vbModal
        
        Set FrmPackingUserDetails = Nothing
        
        Call ButtonActualizar_Click
        
    Else
        Mensaje True, "Seleccione un Pedido."
    End If
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub ButtonActualizar_Click()
    Call PrepararGrid
    Call PrepararDatos
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub Timer1_Timer()
    DoEvents
    bar.Value = bar.Value + 1
    If bar.Value = bar.Max Then
        Call ButtonActualizar_Click
        Call txtminutos_LostFocus
    End If
End Sub

Private Sub txtminutos_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyReturn
            Call txtminutos_LostFocus
    End Select
End Sub

Private Sub txtminutos_LostFocus()
    
    If Not IsNumeric(Me.txtminutos) Then
        
        Me.txtminutos.Text = "1"
        
        bar.Max = 1 * 100
        bar.Min = 0
        bar.Value = 0
        
        Timer1.Enabled = False
        Timer1.Enabled = True
        
    Else
        
        If txtminutos < 1 Then
            txtminutos = "1"
        End If
        
        bar.Max = Me.txtminutos * 100
        bar.Min = 0
        bar.Value = 0
        
        Timer1.Enabled = False
        Timer1.Enabled = True
        
    End If
    
End Sub

Private Sub Chk_Fecha_Click()
    If Chk_Fecha.Value = vbChecked Then
        OrderBy = "ORDER BY MIN(FechaAsignacion)"
        Chk_Pedido.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Pedido_Click()
    If Chk_Pedido.Value = vbChecked Then
        OrderBy = "ORDER BY CodPedido"
        Chk_Fecha.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_NombreProducto_Click()
    If Chk_NombreProducto.Value = vbChecked Then
        OrderBy = "ORDER BY SUM(CantRecolectada) DESC"
        Chk_Pedido.Value = vbUnchecked
        Chk_Fecha.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub
