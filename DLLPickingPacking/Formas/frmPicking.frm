VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form FrmPicking 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer TimerAlert 
      Interval        =   1000
      Left            =   13800
      Top             =   1200
   End
   Begin VB.Timer GlobalHotKeyTimer 
      Interval        =   25
      Left            =   0
      Top             =   1200
   End
   Begin VB.Frame FrameBusqueda 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   480
      TabIndex        =   23
      Top             =   580
      Width           =   8895
      Begin VB.CommandButton CmdTopRegistros 
         Caption         =   "Top 100"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   885
         Left            =   7680
         Picture         =   "frmPicking.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   120
         Width           =   1100
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   885
         Left            =   6480
         Picture         =   "frmPicking.frx":1D82
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   120
         Width           =   1095
      End
      Begin VB.TextBox TextFechaDesde 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   120
         TabIndex        =   25
         Top             =   600
         Width           =   3000
      End
      Begin VB.TextBox TextFechaHasta 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   3360
         TabIndex        =   24
         Top             =   600
         Width           =   3000
      End
      Begin MSComCtl2.DTPicker desde 
         Height          =   315
         Left            =   120
         TabIndex        =   26
         Top             =   600
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarForeColor=   5790296
         CalendarTitleForeColor=   5790296
         Format          =   133103617
         CurrentDate     =   40709
      End
      Begin MSComCtl2.DTPicker hasta 
         Height          =   315
         Left            =   3360
         TabIndex        =   27
         Top             =   600
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarForeColor=   5790296
         CalendarTitleForeColor=   5790296
         Format          =   133103617
         CurrentDate     =   40709
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   260
         Width           =   1215
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   3360
         TabIndex        =   29
         Top             =   255
         Width           =   1455
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha de Creaci�n de Lote."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   0
         Width           =   4935
      End
   End
   Begin VB.Frame TipoPedido 
      Caption         =   "Tipo de Lote"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   9480
      TabIndex        =   20
      Top             =   600
      Width           =   4335
      Begin VB.OptionButton OptEnproceso 
         Caption         =   "En Proceso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   22
         Top             =   480
         Value           =   -1  'True
         Width           =   1575
      End
      Begin VB.OptionButton OptProcesados 
         Caption         =   "Procesados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   21
         Top             =   480
         Width           =   1815
      End
   End
   Begin VB.CommandButton ButtonLeyenda 
      Caption         =   "Leyenda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   12360
      Picture         =   "frmPicking.frx":3B04
      Style           =   1  'Graphical
      TabIndex        =   19
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9960
      Width           =   1095
   End
   Begin VB.Timer Timer1 
      Interval        =   500
      Left            =   9720
      Top             =   10200
   End
   Begin VB.TextBox txtminutos 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   10200
      TabIndex        =   5
      Text            =   "1"
      Top             =   10200
      Width           =   375
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   15360
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Consola Asignaci�n para Recolecci�n y Empacado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   120
         Width           =   5415
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "frmPicking.frx":47CE
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12600
         TabIndex        =   4
         Top             =   120
         Width           =   1815
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   7650
      LargeChange     =   10
      Left            =   14520
      TabIndex        =   1
      Top             =   1800
      Width           =   675
   End
   Begin VB.CommandButton ButtonActualizar 
      Caption         =   "Actualizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   13800
      Picture         =   "frmPicking.frx":6550
      Style           =   1  'Graphical
      TabIndex        =   0
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9960
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   7665
      Left            =   210
      TabIndex        =   2
      Top             =   1800
      Width           =   15000
      _ExtentX        =   26458
      _ExtentY        =   13520
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ProgressBar bar 
      Height          =   255
      Left            =   11760
      TabIndex        =   9
      Top             =   9600
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.CheckBox Chk_Pedido 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Pedidos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   2880
      TabIndex        =   13
      Top             =   10440
      Width           =   2385
   End
   Begin VB.CheckBox Chk_NombreProducto 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Art�culos Solicitados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   2880
      TabIndex        =   14
      Top             =   10080
      Width           =   3105
   End
   Begin VB.CheckBox Chk_Proceso 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Proceso"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   420
      Left            =   840
      TabIndex        =   15
      Top             =   10320
      Width           =   1425
   End
   Begin VB.CheckBox Chk_Lote 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Lote"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   840
      TabIndex        =   16
      Top             =   10080
      Value           =   1  'Checked
      Width           =   1065
   End
   Begin VB.Label lblPackingMobile 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Mobile"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002F39C0&
      Height          =   240
      Left            =   14040
      TabIndex        =   33
      Top             =   1200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Image ImgAlertaPackingMobile 
      Height          =   480
      Left            =   14280
      Picture         =   "frmPicking.frx":82D2
      Top             =   720
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label LabelTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FAFAFA&
      Height          =   255
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   5295
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Pr�xima actualizaci�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   10
      Top             =   9600
      Width           =   2415
   End
   Begin VB.Shape Shape1 
      Height          =   240
      Index           =   4
      Left            =   9240
      Top             =   9600
      Visible         =   0   'False
      Width           =   5775
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Intervalo de actualizar: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   7080
      TabIndex        =   7
      Top             =   10200
      Width           =   2535
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "minutos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   10680
      TabIndex        =   6
      Top             =   10200
      Width           =   855
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   735
      Index           =   0
      Left            =   6600
      TabIndex        =   8
      Top             =   10020
      Width           =   5535
   End
   Begin VB.Label C�digo 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Criterios de Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Index           =   71
      Left            =   480
      TabIndex        =   17
      Top             =   9600
      Width           =   2805
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00AE5B00&
      X1              =   3480
      X2              =   6120
      Y1              =   9720
      Y2              =   9720
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   1335
      Index           =   1
      Left            =   240
      TabIndex        =   18
      Top             =   9480
      Width           =   6255
   End
   Begin VB.Menu TrayMenu 
      Caption         =   "Opciones de Bandeja"
      Visible         =   0   'False
      Begin VB.Menu mShow 
         Caption         =   "Mostrar"
      End
      Begin VB.Menu mExit 
         Caption         =   "Cerrar"
      End
   End
End
Attribute VB_Name = "FrmPicking"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private OrderBy As String
Private SQL As String
Private Proceso As Boolean

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute( _
    "SELECT c_Proceso " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE c_Proceso = '005' ")
    
    If Not TempRs.EOF Then
        frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    End If
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub

Private Sub CmdBuscar_Click()
    
    If TextFechaDesde <> Empty And TextFechaHasta <> Empty Then
        
        SQL = "SELECT MA_PEDIDOS_RUTA_PICKING.CodLote as Lote, count(distinct(MA_PEDIDOS_RUTA_PICKING.CodPedido)) as Pedidos," & vbNewLine & _
        "SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada) as CantProductos," & vbNewLine & _
        "MAX(MA_PEDIDOS_RUTA_PICKING.FechaAsignacion) as Fecha, SUM(MA_PEDIDOS_RUTA_PICKING.CantEmpacada) as CantEmpacada," & vbNewLine & _
        "SUM(MA_PEDIDOS_RUTA_PICKING.CantRecolectada) as CantRecolectada," & vbNewLine & _
        "((SUM(MA_PEDIDOS_RUTA_PICKING.CantEmpacada)/(SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada)))*100) as Proceso" & vbNewLine & _
        "FROM TR_PEDIDOS_RUTA INNER JOIN MA_PEDIDOS_RUTA_PICKING on tr_Pedidos_Ruta.cs_Corrida = MA_PEDIDOS_RUTA_PICKING.CodLote" & vbNewLine & _
        "WHERE c_Documento_NDE <> '' OR c_Documento_Ven <> '' group by MA_PEDIDOS_RUTA_PICKING.CodLote" & vbNewLine & _
        "HAVING CAST(MAX(MA_PEDIDOS_RUTA_PICKING.FechaAsignacion) AS DATE) BETWEEN '" & FechaBD(TextFechaDesde.Text) & "' AND '" & FechaBD(TextFechaHasta.Text) & "'" & vbNewLine & _
        OrderBy
        
        Call PrepararGrid
        Call PrepararDatos(SQL)
        
    End If
    
End Sub

Private Sub CmdTopRegistros_Click()
    
    SQL = "SELECT TOP 100 MA_PEDIDOS_RUTA_PICKING.CodLote as Lote, count(distinct(MA_PEDIDOS_RUTA_PICKING.CodPedido)) as Pedidos," & vbNewLine & _
    "SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada) as CantProductos," & vbNewLine & _
    "MAX(MA_PEDIDOS_RUTA_PICKING.FechaAsignacion) as Fecha, SUM(MA_PEDIDOS_RUTA_PICKING.CantEmpacada) as CantEmpacada," & vbNewLine & _
    "SUM(MA_PEDIDOS_RUTA_PICKING.CantRecolectada) as CantRecolectada," & vbNewLine & _
    "((SUM(MA_PEDIDOS_RUTA_PICKING.CantEmpacada)/(SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada)))*100) as Proceso" & vbNewLine & _
    "from tr_Pedidos_Ruta inner join MA_PEDIDOS_RUTA_PICKING on tr_Pedidos_Ruta.cs_Corrida = MA_PEDIDOS_RUTA_PICKING.CodLote" & vbNewLine & _
    "where c_Documento_NDE <> '' OR c_Documento_Ven <> '' group by MA_PEDIDOS_RUTA_PICKING.CodLote" & vbNewLine & _
    OrderBy
    
    Call PrepararGrid
    Call PrepararDatos(SQL)
    
End Sub

Private Sub Form_Load()
    
    AjustarPantalla Me
    
    FrameBusqueda.Enabled = False
    
    Chk_Lote_Click
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionByRow
        
        .FixedCols = 0
        
        .Rows = 1
        .Cols = 8
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        .FormatString = "Lote" & "|" & "Fecha" & "|" & "Pedidos" & "|" & _
        "Art. Sol" & "|" & "Art. Rec" & "|" & "Art. Emp" & "|" & "Proceso" & "|"
        
        .ColWidth(0) = 2000
        .ColAlignment(0) = flexAlignCenterCenter
        
        .ColWidth(1) = 5200
        .ColAlignment(1) = flexAlignCenterCenter
        
        .ColWidth(2) = 1500
        .ColAlignment(2) = flexAlignCenterCenter
        
        .ColWidth(3) = 1500
        .ColAlignment(3) = flexAlignCenterCenter
        
        .ColWidth(4) = 1500
        .ColAlignment(4) = flexAlignCenterCenter
        
        .ColWidth(5) = 1500
        .ColAlignment(5) = flexAlignCenterCenter
        
        .ColWidth(6) = 1500
        .ColAlignment(6) = flexAlignCenterCenter
        
        .ColWidth(7) = 0
        
        .Width = 15000
        
        .ScrollTrack = True
        
        .Row = 0
        .Col = 7
        .ColSel = 6
        
    End With
    
    For I = 0 To Grid.Cols - 1
        Grid.Col = I
        Grid.CellAlignment = flexAlignCenterCenter
    Next
    
End Sub

Private Sub PrepararDatos(SQL As String)
    
    'On Error GoTo Error1
    
    Dim Rs As New ADODB.Recordset
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    Grid.Visible = False
    
    While Not Rs.EOF
        
        Grid.Rows = Grid.Rows + 1
        
        Grid.TextMatrix(Grid.Rows - 1, 0) = Rs!Lote
        Grid.TextMatrix(Grid.Rows - 1, 1) = Rs!FECHA
        Grid.TextMatrix(Grid.Rows - 1, 2) = Rs!Pedidos
        Grid.TextMatrix(Grid.Rows - 1, 3) = Rs!CantProductos
        Grid.TextMatrix(Grid.Rows - 1, 4) = Rs!CantRecolectada
        Grid.TextMatrix(Grid.Rows - 1, 5) = Rs!CantEmpacada
        Grid.TextMatrix(Grid.Rows - 1, 6) = Round(Rs!Proceso, 2) & " %"
        
        If Proceso Then
            If Rs!Picking Then
                Grid.TextMatrix(Grid.Rows - 1, 7) = "1"
                For I = 0 To Grid.Cols - 1
                    Grid.Row = Grid.Rows - 1
                    Grid.Col = I
                    Grid.CellBackColor = &HC0E0FF      'naranja 12640511
                Next I
            Else
                Grid.TextMatrix(Grid.Rows - 1, 7) = "0"
                If Rs!CantRecolectada = Rs!CantProductos Then
                    For I = 0 To Grid.Cols - 1
                        Grid.Row = Grid.Rows - 1
                        Grid.Col = I
                        Grid.CellBackColor = &HB4EDB9    'verde 11857337
                    Next I
                ElseIf Rs!CantRecolectada > 0 Then
                    For I = 0 To Grid.Cols - 1
                        Grid.Row = Grid.Rows - 1
                        Grid.Col = I
                        Grid.CellBackColor = -2147483624 'Amarillo
                    Next I
                End If
            End If
        End If
        
        Rs.MoveNext
        
    Wend
    
    Rs.Close
    
    If Grid.Rows > 12 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(1) = 5200 - 675
    Else
        Grid.ScrollBars = flexScrollBarNone
        ScrollGrid.Visible = False
    End If
    
    ImgAlertaPackingMobile.Visible = False
    lblPackingMobile.Visible = False
    
    TimerAlert.Enabled = (CDec( _
    BuscarValorBD("PackingMobile", _
    "SELECT isNULL(COUNT(*), 0) AS PackingMobile " & _
    "FROM MA_PEDIDOS_RUTA_PICKING " & _
    "WHERE Picking = 1 " & _
    "AND (PackingMobile_CantEmpaques > 0) " & _
    "AND Packing = 0", "0", _
    Ent.BDD)) > 0)
    
Finally:
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub GlobalHotKeyTimer_Timer()
    If GetAsyncKeyState(VK_CONTROL) < 0 Then
        If GetAsyncKeyState(VK_SHIFT) < 0 Then
            If GetAsyncKeyState(VK_DOWN) < 0 Then
                GoTo ToTheTray
            ElseIf GetAsyncKeyState(VK_UP) < 0 Then
                GoTo RestoreFromTray
            End If
        End If
        If GetAsyncKeyState(VK_SHIFT) < 0 Then
            If GetAsyncKeyState(VK_F10) < 0 Then
ToTheTray:
                mExit.Enabled = (Screen.ActiveForm Is Me _
                Or Screen.ActiveForm Is FrmPickingLote _
                Or Screen.ActiveForm Is FrmPacking)
                TrayApp Me
            End If
            If GetAsyncKeyState(VK_F11) < 0 Then
RestoreFromTray:
                UnTrayApp
            End If
        End If
    End If
End Sub

Private Sub Grid_DblClick()
    
    If Proceso Then
        
        If Grid.TextMatrix(Grid.RowSel, 0) <> Empty Then
            
            If Grid.TextMatrix(Grid.RowSel, 7) = "1" Then
                FrmPacking.Lote = Grid.TextMatrix(Grid.RowSel, 0)
                FrmPacking.Show vbModal
                Set FrmPacking = Nothing
            Else
                FrmPickingLote.Lote = Grid.TextMatrix(Grid.RowSel, 0)
                FrmPickingLote.Show vbModal
                Set FrmPickingLote = Nothing
            End If
            
            Call ButtonActualizar_Click
            
        Else
            Mensaje True, "El Lote Esta Vac�o."
        End If
        
    End If
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub ButtonActualizar_Click()
    
    Select Case True
        
        Case OptEnproceso.Value
            
            SQL = "select Lote,MIN(Fecha) as Fecha,count(distinct(Pedido)) as Pedidos, SUM(Articulos) as CantProductos," & vbNewLine & _
            "ROUND(isNull(SUM(CantRecolectada), 0), MAX(Pro.Cant_Decimales), 0) AS CantRecolectada, ROUND(isNull(SUM(CantEmpacada), 0), MAX(Pro.Cant_Decimales), 0) AS CantEmpacada, TB.Picking," & vbNewLine & _
            "((ROUND(isNull(SUM(CantRecolectada), 0), MAX(Pro.Cant_Decimales), 0) + ROUND(isNull(SUM(CantEmpacada), 0), MAX(Pro.Cant_Decimales), 0)) / CONVERT(Real, SUM(Articulos)*2) * 100) AS Proceso " & vbNewLine & _
            "FROM ( " & vbNewLine & _
                "select tr_Pedidos_Ruta.cs_Corrida as Lote, tr_Pedidos_Ruta.c_Documento as Pedido, " & vbNewLine & _
                "tr_VENTAS.c_CODARTICULO as Art, SUM(tr_VENTAS.n_CANTIDAD) as Articulos, " & vbNewLine & _
                "MA_Pedidos_Ruta.d_FechaCorrida as Fecha, MA_Pedidos_Ruta.Picking as Picking " & vbNewLine & _
                "from tr_Pedidos_Ruta inner join tr_VENTAS on tr_Pedidos_Ruta.c_DOCUMENTO = " & vbNewLine & _
                "TR_VENTAS.c_DOCUMENTO and TR_VENTAS.c_CONCEPTO = 'PED' inner join Ma_VENTAS on tr_Pedidos_Ruta.c_DOCUMENTO = " & vbNewLine & _
                "Ma_VENTAS.c_DOCUMENTO inner join MA_Pedidos_Ruta on tr_Pedidos_Ruta.cs_Corrida = " & vbNewLine & _
                "MA_Pedidos_Ruta.cs_Corrida where MA_VENTAS.c_CONCEPTO = 'PED' and " & vbNewLine & _
                "MA_Pedidos_Ruta.Packing = 0 GROUP BY TR_PEDIDOS_RUTA.cs_Corrida, TR_PEDIDOS_RUTA.c_Documento, c_CodArticulo, d_FechaCorrida, Picking" & vbNewLine & _
            ") TB INNER JOIN MA_PRODUCTOS Pro ON TB.Art = Pro.C_CODIGO " & vbNewLine & _
            "left JOIN MA_PEDIDOS_RUTA_PICKING ON MA_PEDIDOS_RUTA_PICKING.CodLote = Lote " & vbNewLine & _
            "and  MA_PEDIDOS_RUTA_PICKING.CodPedido = Pedido " & vbNewLine & _
            "and MA_PEDIDOS_RUTA_PICKING.CodProducto = art " & vbNewLine & _
            "group by Lote,TB.Picking " & vbNewLine & _
            OrderBy
            
            Proceso = True
            FrameBusqueda.Enabled = False
            
        Case OptProcesados.Value
            
            SQL = "select top 30 MA_PEDIDOS_RUTA_PICKING.CodLote as Lote, count(distinct(MA_PEDIDOS_RUTA_PICKING.CodPedido)) as Pedidos," & vbNewLine & _
            "SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada) as CantProductos," & vbNewLine & _
            "MAX(MA_PEDIDOS_RUTA_PICKING.FechaAsignacion) as Fecha, SUM(MA_PEDIDOS_RUTA_PICKING.CantEmpacada) as CantEmpacada," & vbNewLine & _
            "SUM(MA_PEDIDOS_RUTA_PICKING.CantRecolectada) as CantRecolectada," & vbNewLine & _
            "((SUM(MA_PEDIDOS_RUTA_PICKING.CantEmpacada)/(SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada)))*100) as Proceso" & vbNewLine & _
            "from tr_Pedidos_Ruta inner join MA_PEDIDOS_RUTA_PICKING on tr_Pedidos_Ruta.cs_Corrida = MA_PEDIDOS_RUTA_PICKING.CodLote" & vbNewLine & _
            "where c_Documento_NDE <> '' OR c_Documento_Ven <> '' group by MA_PEDIDOS_RUTA_PICKING.CodLote" & vbNewLine & _
            OrderBy
            
            Proceso = False
            FrameBusqueda.Enabled = True
            
    End Select
    
    Call PrepararGrid
    Call PrepararDatos(SQL)
    
End Sub

Private Sub ShowPackingMobile()
    FrmPackingUser.PackingMobile = True
    FrmPackingUser.Show vbModal
    Set FrmPackingUser = Nothing
    ButtonActualizar_Click
End Sub

Private Sub ImgAlertaPackingMobile_Click()
    ShowPackingMobile
End Sub

Private Sub lblPackingMobile_Click()
    ShowPackingMobile
End Sub

Private Sub OptEnproceso_Click()
    If Chk_Lote.Value = vbChecked Then Chk_Lote_Click
    Call ButtonActualizar_Click
End Sub

Private Sub OptProcesados_Click()
    If Chk_Lote.Value = vbChecked Then Chk_Lote_Click
    Call ButtonActualizar_Click
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub TextFechaDesde_Click()
    
    Dim mCls As clsFechaSeleccion
    
    Set mCls = New clsFechaSeleccion
    
    mCls.MostrarInterfazFechaHora DateTypes.JustDate
    
    If mCls.Selecciono Then
        TextFechaDesde.Text = mCls.FECHA
        TextFechaDesde.Tag = CDbl(mCls.FECHA)
    End If
    
End Sub

Private Sub TextFechaHasta_Click()
    
    Dim mCls As clsFechaSeleccion
    
    Set mCls = New clsFechaSeleccion
    
    mCls.MostrarInterfazFechaHora DateTypes.JustDate
    
    If mCls.Selecciono Then
        TextFechaHasta.Text = mCls.FECHA
        TextFechaHasta.Tag = CDbl(mCls.FECHA)
    End If

End Sub

Private Sub Timer1_Timer()
    DoEvents
    bar.Value = bar.Value + 1
    If bar.Value = bar.Max Then
        Call ButtonActualizar_Click
        Call txtminutos_LostFocus
    End If
End Sub

Private Sub TimerAlert_Timer()
    ImgAlertaPackingMobile.Visible = Not ImgAlertaPackingMobile.Visible
    lblPackingMobile.Visible = ImgAlertaPackingMobile.Visible
End Sub

Private Sub txtminutos_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyReturn
            Call txtminutos_LostFocus
    End Select
End Sub

Private Sub txtminutos_LostFocus()
    
    If Not IsNumeric(Me.txtminutos) Then
        
        Me.txtminutos.Text = "1"
        
        bar.Max = 1 * 100
        bar.Min = 0
        bar.Value = 0
        
        Timer1.Enabled = False
        Timer1.Enabled = True
        
    Else
        
        If txtminutos < 1 Then
            txtminutos = "1"
        End If
        
        bar.Max = Me.txtminutos * 100
        bar.Min = 0
        bar.Value = 0
        
        Timer1.Enabled = False
        Timer1.Enabled = True
        
    End If
    
End Sub

Private Sub Chk_Proceso_Click()
    If Chk_Proceso.Value = vbChecked Then
        OrderBy = "ORDER BY Proceso DESC"
        Chk_Pedido.Value = vbUnchecked
        Chk_Lote.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Pedido_Click()
    If Chk_Pedido.Value = vbChecked Then
        OrderBy = "ORDER BY Pedidos DESC"
        Chk_Lote.Value = vbUnchecked
        Chk_Proceso.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Lote_Click()
    If Chk_Lote.Value = vbChecked Then
        OrderBy = "ORDER BY Lote" & IIf(OptProcesados.Value, " DESC", vbNullString)
        Chk_Pedido.Value = vbUnchecked
        Chk_Proceso.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_NombreProducto_Click()
    If Chk_NombreProducto.Value = vbChecked Then
        OrderBy = "ORDER BY CantProductos DESC"
        Chk_Pedido.Value = vbUnchecked
        Chk_Proceso.Value = vbUnchecked
        Chk_Lote.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Dim Msg As Long
    
    Msg = X / Screen.TwipsPerPixelX
    
    Select Case Msg
        
        Case WM_LBUTTONDOWN
        Case WM_LBUTTONUP
        Case WM_LBUTTONDBLCLK
            
            UnTrayApp
            
        Case WM_RBUTTONDOWN
            
            PopupMenu TrayMenu, , , , mShow
            
        Case WM_RBUTTONUP
        Case WM_RBUTTONDBLCLK
        
    End Select
    
End Sub

Private Sub mExit_Click()
    mShow_Click
    CerrarDesdeBandeja = True
    ForcedExit
End Sub

Private Sub mShow_Click()
    Form_MouseMove vbLeftButton, 0, _
    WM_LBUTTONDBLCLK * Screen.TwipsPerPixelX, 0
End Sub
