VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form FrmDateSelector 
   Appearance      =   0  'Flat
   BackColor       =   &H00F4F4F4&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7245
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   13665
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7245
   ScaleWidth      =   13665
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame_Second 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   500
      Left            =   12525
      TabIndex        =   34
      Top             =   1320
      Width           =   500
      Begin VB.Label lbl_Second 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "S"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   15
         TabIndex        =   35
         Top             =   0
         Width           =   450
      End
   End
   Begin VB.Frame Frame_Minute 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   500
      Left            =   10975
      TabIndex        =   32
      Top             =   1320
      Width           =   500
      Begin VB.Label lbl_Minute 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "M"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   25
         TabIndex        =   33
         Top             =   0
         Width           =   450
      End
   End
   Begin VB.Frame Frame_Hour 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   500
      Left            =   9400
      TabIndex        =   30
      Top             =   1320
      Width           =   500
      Begin VB.Label lbl_Hour 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "H"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   15
         TabIndex        =   31
         Top             =   0
         Width           =   450
      End
   End
   Begin VB.Frame FramePantalla3 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   135
      Left            =   120
      TabIndex        =   29
      Top             =   480
      Visible         =   0   'False
      Width           =   13575
   End
   Begin VB.Frame FramePantalla2 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1335
      Left            =   8520
      TabIndex        =   28
      Top             =   540
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Frame FramePantalla1 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1455
      Left            =   0
      TabIndex        =   27
      Top             =   420
      Visible         =   0   'False
      Width           =   135
   End
   Begin VB.PictureBox PicSecond 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2535
      Left            =   13155
      ScaleHeight     =   2535
      ScaleWidth      =   495
      TabIndex        =   25
      Top             =   7080
      Width           =   495
      Begin MSComctlLib.Slider sl_second 
         Height          =   2655
         Left            =   0
         TabIndex        =   26
         Top             =   120
         Visible         =   0   'False
         Width           =   630
         _ExtentX        =   1111
         _ExtentY        =   4683
         _Version        =   393216
         Orientation     =   1
         LargeChange     =   6
         Max             =   59
         SelectRange     =   -1  'True
      End
   End
   Begin VB.PictureBox PicMinute 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2535
      Left            =   12540
      ScaleHeight     =   2535
      ScaleWidth      =   495
      TabIndex        =   23
      Top             =   6960
      Width           =   495
      Begin MSComctlLib.Slider sl_minute 
         Height          =   2655
         Left            =   0
         TabIndex        =   24
         Top             =   0
         Visible         =   0   'False
         Width           =   630
         _ExtentX        =   1111
         _ExtentY        =   4683
         _Version        =   393216
         Orientation     =   1
         LargeChange     =   6
         Max             =   59
         SelectRange     =   -1  'True
      End
   End
   Begin VB.PictureBox PicHour 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2535
      Left            =   8880
      ScaleHeight     =   2535
      ScaleWidth      =   495
      TabIndex        =   21
      Top             =   7200
      Width           =   495
      Begin MSComctlLib.Slider sl_hour 
         Height          =   2655
         Left            =   0
         TabIndex        =   22
         Top             =   360
         Visible         =   0   'False
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   4683
         _Version        =   393216
         Orientation     =   1
         LargeChange     =   3
         Max             =   23
         SelectRange     =   -1  'True
      End
   End
   Begin VB.Frame FrameAceptar 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   975
      Left            =   10200
      TabIndex        =   19
      Top             =   6000
      Width           =   2055
      Begin VB.Label Aceptar 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   615
         Left            =   0
         TabIndex        =   20
         Top             =   225
         Width           =   2055
      End
   End
   Begin VB.Frame Framecmd_SecondDown 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   12120
      TabIndex        =   17
      Top             =   3720
      Width           =   1335
      Begin VB.Label cmd_SecondDown 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   120
         TabIndex        =   18
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Framecmd_MinuteDown 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   10560
      TabIndex        =   15
      Top             =   3720
      Width           =   1335
      Begin VB.Label cmd_MinuteDown 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   120
         TabIndex        =   16
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Framecmd_HourDown 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   9000
      TabIndex        =   13
      Top             =   3720
      Width           =   1335
      Begin VB.Label cmd_HourDown 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   120
         TabIndex        =   14
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Framecmd_SecondUp 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   12120
      TabIndex        =   11
      Top             =   2040
      Width           =   1335
      Begin VB.Label cmd_SecondUp 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   120
         TabIndex        =   12
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Framecmd_MinuteUp 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   10560
      TabIndex        =   9
      Top             =   2040
      Width           =   1335
      Begin VB.Label cmd_MinuteUp 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   120
         TabIndex        =   10
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Framecmd_HourUp 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   9000
      TabIndex        =   7
      Top             =   2040
      Width           =   1335
      Begin VB.Label cmd_HourUp 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   120
         TabIndex        =   8
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   13680
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   10755
         TabIndex        =   6
         Top             =   105
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   75
         Width           =   3135
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   12960
         Picture         =   "FormDateSelector.frx":0000
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.TextBox txt_Second 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   39.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00828282&
      Height          =   1215
      Left            =   12120
      TabIndex        =   2
      Text            =   "00"
      Top             =   2520
      Width           =   1335
   End
   Begin VB.TextBox txt_Minute 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   39.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00828282&
      Height          =   1215
      Left            =   10560
      TabIndex        =   1
      Text            =   "00"
      Top             =   2520
      Width           =   1335
   End
   Begin VB.TextBox txt_Hour 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   39.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00828282&
      Height          =   1215
      Left            =   9000
      TabIndex        =   0
      Text            =   "00"
      Top             =   2520
      Width           =   1335
   End
   Begin MSComCtl2.MonthView mv_Month 
      Height          =   7560
      Left            =   120
      TabIndex        =   3
      Top             =   360
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   13335
      _Version        =   393216
      ForeColor       =   5790296
      BackColor       =   12632256
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   30
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MonthBackColor  =   16053492
      ShowToday       =   0   'False
      StartOfWeek     =   134807553
      TitleBackColor  =   11426560
      TitleForeColor  =   16053492
      TrailingForeColor=   -2147483635
      CurrentDate     =   42509
   End
End
Attribute VB_Name = "FrmDateSelector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public FECHA As Date
Public SelectedDateType As DateTypes
Public MinDate As Date

Private FormColor As Long

Public Enum DateTypes
    Full
    JustDate
    JustHour
End Enum

Private Declare Function GetWindowLong Lib "user32" _
    Alias "GetWindowLongA" ( _
    ByVal hWnd As Long, _
    ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" _
    Alias "SetWindowLongA" ( _
    ByVal hWnd As Long, _
    ByVal nIndex As Long, _
    ByVal dwNewLong As Long) As Long

Private Const GWL_STYLE As Long = -16
Private Const MCS_NOTODAYCIRCLE As Long = 8
Public fCls As clsFechaSeleccion

Private Declare Function SendMessage Lib "user32" _
                        Alias "SendMessageA" (ByVal hWnd As Long, _
                                              ByVal wMsg As Long, _
                                              ByVal wParam As Long, _
                                              lParam As Any) As Long

Private Declare Sub ReleaseCapture Lib "user32" ()
Const WM_NCLBUTTONDOWN = &HA1
Const HTCAPTION = 2

Private Sub Frame5_MouseMove(Button As Integer, Shift As Integer, _
                           X As Single, Y As Single)
    On Error Resume Next
    Dim lngReturnValue As Long
    
    If Button = 1 Then
      Call ReleaseCapture
      lngReturnValue = SendMessage(Me.hWnd, WM_NCLBUTTONDOWN, _
                                   HTCAPTION, 0&)
    End If
End Sub

Private Sub PaintSlider(pCtl, pColor As Long)
    
    'pCtl.Move -10, -10, Picture1.Width + 25
    
    pCtl.Container.BackColor = pColor
    
    'SetStyle pCtl.hWnd
    'SetSolidColor pCtl.hWnd, pColor
    
    'SSTabSubclass pCtl.hWnd
        
    'RedrawWindow pCtl.hWnd, ByVal 0&, ByVal 0&, &H1
    
End Sub

Private Sub Aceptar_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then
        Select Case SelectedDateType
            Case Full
                fCls.Selecciono = True
                If txt_Hour = "" Or txt_Minute = "" Or txt_Second = "" Then
                    Call Mensaje(False, "Debe ingresar un valor valido para la hora")
                    Exit Sub
                Else
                    fCls.FECHA = CDate(mv_Month.Value & " " & txt_Hour & ":" & txt_Minute & ":" & txt_Second)
                End If
            Case JustDate
                fCls.Selecciono = True
                fCls.FECHA = CDate(mv_Month.Value)
            Case JustHour
                fCls.Selecciono = True
                fCls.FECHA = CDate(txt_Hour & ":" & txt_Minute & ":" & txt_Second)
        End Select
        If MinDate > FECHA Then
            'MsgBox Stellar_Mensaje(105)
        Else
            Unload Me
        End If
    End If
End Sub

Private Sub cmd_HourUp_Click()
    If sl_hour.Value + 1 <= sl_hour.Max Then sl_hour.Value = sl_hour.Value + 1
End Sub

Private Sub cmd_HourDown_Click()
    If sl_hour.Value - 1 >= sl_hour.Min Then sl_hour.Value = sl_hour.Value - 1
End Sub

Private Sub cmd_MinuteUp_Click()
    If sl_minute.Value + 1 <= sl_minute.Max Then sl_minute.Value = sl_minute.Value + 1
End Sub

Private Sub cmd_MinuteDown_Click()
    If sl_minute.Value - 1 >= sl_minute.Min Then sl_minute.Value = sl_minute.Value - 1
End Sub

Private Sub cmd_SecondUp_Click()
    If sl_second.Value + 1 <= sl_second.Max Then sl_second.Value = sl_second.Value + 1
End Sub

Private Sub cmd_SecondDown_Click()
    If sl_second.Value - 1 >= sl_second.Min Then sl_second.Value = sl_second.Value - 1
End Sub

Private Sub Exit_Click()
    FECHA = CDbl(0)
    Unload Me
End Sub

Private Sub Form_Activate()
    
    mv_Month.Value = FormatDateTime(Now, vbShortDate)
    
    If MinDate <> CDbl(0) Then mv_Month.MinDate = FormatDateTime(MinDate, vbShortDate)
    
    Select Case SelectedDateType
        Case 0
            
        Case 1
            Me.Width = 8715
            Me.lbl_website.Left = 5835
            Me.Exit.Left = 7920
            Me.FramePantalla1.Visible = True
            Me.FramePantalla2.Visible = True
            Me.FramePantalla3.Visible = True
            
        Case DateTypes.JustHour
        
            Me.Width = 4910
            
            Me.mv_Month.Visible = False
            Me.FramePantalla1.Visible = False
            Me.FramePantalla2.Visible = False
            Me.FramePantalla3.Visible = False
            
            Me.Frame_Hour.Left = 630
            Me.Framecmd_HourUp.Left = 230
            Me.txt_Hour.Left = 230
            Me.Framecmd_HourDown.Left = 230
            
            Me.Frame_Minute.Left = 2190
            Me.Framecmd_MinuteUp.Left = 1790
            Me.txt_Minute.Left = 1790
            Me.Framecmd_MinuteDown.Left = 1790
            
            Me.Frame_Second.Left = 3750
            Me.Framecmd_SecondUp.Left = 3350
            Me.txt_Second.Left = 3350
            Me.Framecmd_SecondDown.Left = 3350
            
            FrameAceptar.Left = 1430
            
    End Select
    
    Call AjustarPantalla(Me)
    
End Sub

Private Sub Form_Load()
    
    If Me.fCls.TiempoNulo Then
        
        sl_hour.Value = 0
        sl_minute.Value = 0
        sl_second.Value = 0
        
    Else
        
        sl_hour.Value = Hour(Now)
        sl_minute.Value = Minute(Now)
        sl_second.Value = Second(Now)
        
    End If
    
    FormColor = Me.BackColor
    
    PaintSlider sl_hour, FormColor
    PaintSlider sl_minute, FormColor
    PaintSlider sl_second, FormColor
    
    aceptar.Caption = Replace(Stellar_Mensaje(5), "&", "")
    
    With Me.mv_Month
        SetWindowLong .hWnd, GWL_STYLE, GetWindowLong(.hWnd, GWL_STYLE) Or MCS_NOTODAYCIRCLE
    End With

End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Frame5_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Frame5_MouseMove Button, Shift, X, Y
End Sub

Private Sub mv_Month_DateClick(ByVal DateClicked As Date)
    If SelectedDateType = 1 Then
        fCls.Selecciono = True
        fCls.FECHA = DateClicked
        fCls.Selecciono = True
        Unload Me
    End If
End Sub

Private Sub sl_hour_Change()
    txt_Hour.Text = Format(sl_hour.Value, "00")
End Sub

Private Sub sl_minute_Change()
    txt_Minute.Text = Format(sl_minute.Value, "00")
End Sub

Private Sub sl_second_Change()
    txt_Second.Text = Format(sl_second.Value, "00")
End Sub

Private Sub txt_Hour_GotFocus()
    SeleccionarTexto txt_Hour
End Sub

Private Sub txt_Hour_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 48 To 57
'        Case Asc("."), vbKeyDelete
'            KeyAscii = IIf(InStr(1, txt_Hour.Text, ".", vbTextCompare) > 0, 0, KeyAscii)
        Case 8 ' vbBack
        Case vbKeyReturn
            If txt_Hour.Text <> "" Then
               SendKeys (vbTab)
            End If
        Case vbKeyF12, vbKeyEscape
            'Exit_Click
        Case Else
            KeyAscii = 0
    End Select
End Sub

Private Sub txt_Hour_LostFocus()
    
    If txt_Hour.Text = Empty Then
        Call Mensaje(False, "El valor de la hora no es valida")
        If gRutinas.PuedeObtenerFoco(txt_Hour) Then txt_Hour.SetFocus: txt_Hour.Text = "00"
    Else
        If SVal(txt_Hour.Text) < 0 Or txt_Hour.Text > 23 Then
            Call Mensaje(False, "El valor de la hora no es valida")
            If gRutinas.PuedeObtenerFoco(txt_Hour) Then txt_Hour.SetFocus: txt_Hour.Text = "00"
        End If
    End If
    
    sl_hour.Value = SVal(txt_Hour.Text)
    
End Sub

Private Sub txt_Minute_GotFocus()
    SeleccionarTexto txt_Minute
End Sub

Private Sub txt_Minute_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 48 To 57
'        Case Asc("."), vbKeyDelete
'            KeyAscii = IIf(InStr(1, txt_Minute.Text, ".", vbTextCompare) > 0, 0, KeyAscii)
        Case 8 ' vbBack
        Case vbKeyReturn
            If txt_Minute.Text <> "" Then
               SendKeys (vbTab)
            End If
        Case vbKeyF12, vbKeyEscape
            'Exit_Click
        Case Else
            KeyAscii = 0
    End Select
End Sub

Private Sub txt_Minute_LostFocus()
    If txt_Minute.Text = "" Then
        Call Mensaje(False, "El valor de la hora no es valida")
        If gRutinas.PuedeObtenerFoco(txt_Minute) Then txt_Minute.SetFocus: txt_Minute.Text = "00"
    Else
        If SVal(txt_Minute.Text) < 0 Or txt_Minute.Text > 59 Then
            Call Mensaje(False, "El valor de los minutos no es valida")
            If gRutinas.PuedeObtenerFoco(txt_Minute) Then txt_Minute.SetFocus: txt_Minute.Text = "00"
        End If
    End If
    
    sl_minute.Value = SVal(txt_Minute.Text)
End Sub

Private Sub txt_Second_GotFocus()
    SeleccionarTexto txt_Second
End Sub

Private Sub txt_Second_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 48 To 57
'        Case Asc("."), vbKeyDelete
'            KeyAscii = IIf(InStr(1, txt_Minute.Text, ".", vbTextCompare) > 0, 0, KeyAscii)
        Case 8 ' vbBack
        Case vbKeyReturn
            If txt_Second.Text <> "" Then
               SendKeys (vbTab)
            End If
        Case vbKeyF12, vbKeyEscape
            'Exit_Click
        Case Else
            KeyAscii = 0
    End Select
End Sub

Private Sub txt_Second_LostFocus()
    If txt_Second.Text = "" Then
        Call Mensaje(False, "El valor de la hora no es valida")
        If gRutinas.PuedeObtenerFoco(txt_Second) Then txt_Second.SetFocus: txt_Second.Text = "00"
    Else
        If SVal(txt_Second.Text) < 0 Or txt_Second.Text > 59 Then
            Call Mensaje(False, "El valor de la hora no es valida")
            If gRutinas.PuedeObtenerFoco(txt_Second) Then txt_Second.SetFocus: txt_Second.Text = "00"
        End If
    End If
    
    sl_second.Value = SVal(txt_Second.Text)
End Sub

Public Sub SeleccionarTexto(pControl As Object)
    On Error Resume Next
    pControl.SelStart = 0
    pControl.SelLength = Len(pControl.Text)
End Sub

