VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form frmFechaSel 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7185
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   8565
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7185
   ScaleWidth      =   8565
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   -120
      TabIndex        =   4
      Top             =   600
      Width           =   255
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   8400
      TabIndex        =   3
      Top             =   600
      Width           =   255
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   600
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8670
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   7920
         Picture         =   "frmFechaSel.frx":0000
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   3315
         TabIndex        =   2
         Top             =   75
         Width           =   1815
      End
   End
   Begin MSComCtl2.MonthView mv 
      Height          =   6660
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   7260
      _ExtentX        =   12806
      _ExtentY        =   11748
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      StartOfWeek     =   112197633
      TitleBackColor  =   11426560
      TitleForeColor  =   16777215
      TrailingForeColor=   12632256
      CurrentDate     =   42507
   End
End
Attribute VB_Name = "frmFechaSel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function GetWindowLong Lib "user32" _
    Alias "GetWindowLongA" ( _
    ByVal hWnd As Long, _
    ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" _
    Alias "SetWindowLongA" ( _
    ByVal hWnd As Long, _
    ByVal nIndex As Long, _
    ByVal dwNewLong As Long) As Long

Private Const GWL_STYLE As Long = -16
Private Const MCS_NOTODAYCIRCLE As Long = 8

Public fCls As clsFechaSeleccion

Private Sub Exit_Click()
    fCls.Selecciono = False
    Unload Me
End Sub

Private Sub Form_Initialize()
    With Me.mv
        SetWindowLong .hWnd, GWL_STYLE, GetWindowLong(.hWnd, GWL_STYLE) Or MCS_NOTODAYCIRCLE
    End With
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF12, vbKeyEscape
            Exit_Click
    End Select
End Sub

Private Sub Form_Load()
    mv.Value = Now
End Sub

Private Sub mv_DateClick(ByVal DateClicked As Date)
    fCls.Selecciono = True
    fCls.FECHA = DateClicked
    Unload Me
End Sub
