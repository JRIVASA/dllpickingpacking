VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmPackingPedido 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmd_salir 
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   13920
      Picture         =   "FrmPackingPedido.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   9650
      Width           =   1215
   End
   Begin VB.CommandButton ButtonLeyenda 
      Caption         =   "Leyenda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   12480
      Picture         =   "FrmPackingPedido.frx":1D82
      Style           =   1  'Graphical
      TabIndex        =   1
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9650
      Width           =   1215
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   8850
      LargeChange     =   10
      Left            =   14480
      TabIndex        =   6
      Top             =   600
      Width           =   674
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   19320
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Pedido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   480
         TabIndex        =   5
         Top             =   120
         Width           =   7575
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12480
         TabIndex        =   4
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14520
         Picture         =   "FrmPackingPedido.frx":2A4C
         Top             =   0
         Width           =   480
      End
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   8865
      Left            =   160
      TabIndex        =   0
      Top             =   600
      Width           =   15000
      _ExtentX        =   26458
      _ExtentY        =   15637
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "FrmPackingPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Lote As String
Public Pedido As String

Private Enum GrdPack
    ColNull
    ColCodigoAlterno
    ColDesPro
    ColCantSolicitada
    ColCantRecolectada
    ColCantEmpacada
    ColHidden1
    ColCount
End Enum

Private AnchoCampoDescripcion As Long

Private Band As Boolean
Private Fila As Integer

Private Salir As Variant

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute( _
    "SELECT c_Proceso " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE c_Proceso = '007' ")
    
    If Not TempRs.EOF Then frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub

Private Sub cmd_salir_Click()
    Exit_Click
End Sub

Private Sub Form_Activate()
    
    If Salir Then
        
        Mensaje True, "No hay productos para mostrar."
        
        Unload Me
        
        Exit Sub
        
    End If
    
End Sub

Private Sub Form_Load()
    
    AjustarPantalla Me
    
    Salir = False
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionByRow
        
        .FixedCols = 0
        .FixedRows = 1
        
        .Rows = 1
        .Cols = ColCount
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        AnchoCampoDescripcion = 7500
        
        .Row = 0
        
        .Col = ColNull
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColCodigoAlterno
        .ColWidth(.Col) = 3000
        .Text = "Art�culo"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesPro
        .ColWidth(.Col) = AnchoCampoDescripcion
        .Text = "Nombre del Producto"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantSolicitada
        .ColWidth(.Col) = 1500
        .Text = "Cant. Sol"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantRecolectada
        .ColWidth(.Col) = 1500
        .Text = "Cant. Rec"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantEmpacada
        .ColWidth(.Col) = 1500
        .Text = "Cant. Emp"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColHidden1
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Width = 15000
        
        .ScrollTrack = True
        
        Fila = .Row
        
        .Col = ColHidden1
        .ColSel = ColCantEmpacada
        
    End With
    
    'For I = 0 To Grid.Cols - 1
        'Grid.Col = I
        'Grid.CellAlignment = flexAlignCenterCenter
    'Next
    
End Sub

Private Sub PrepararDatos()
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset, I As Integer, Deposito As String
    
    SQL = "SELECT PRO.c_Descri AS Nombre, PP.CantSolicitada, " & vbNewLine & _
    "PP.CantRecolectada, isNULL(COD.c_Codigo, PRO.c_Codigo) AS CodEDI, " & vbNewLine & _
    "PP.CantEmpacada, PP.Packing AS Packing, PP.Picking AS Picking, " & vbNewLine & _
    "PRO.Cant_Decimales, PRO.n_Peso, PRO.n_Volumen, PRO.n_PesoBul, PRO.n_VolBul " & vbNewLine & _
    "FROM MA_PEDIDOS_RUTA_PICKING PP " & _
    "INNER JOIN MA_PRODUCTOS PRO " & _
    "ON PRO.c_Codigo = PP.CodProducto " & _
    "LEFT JOIN MA_Codigos COD " & _
    "ON COD.c_CodNasa = PRO.c_Codigo " & _
    "AND COD.nu_Intercambio = 1 " & vbNewLine & _
    "WHERE PP.CodLote = '" & Lote & "' " & vbNewLine & _
    "AND PP.CodPedido = '" & Pedido & "' " & _
    "ORDER BY PRO.c_Descri "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Rs.EOF Then
        Salir = True
        Exit Sub
    End If
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    Grid.Visible = False
    
    While Not Rs.EOF
        
        Grid.Rows = Grid.Rows + 1
        Linea = Grid.Rows - 1
        
        Grid.RowData(Linea) = Grid.RowHeight(Linea)
        
        Grid.TextMatrix(Linea, ColCodigoAlterno) = Rs!CodEDI
        Grid.TextMatrix(Linea, ColDesPro) = Rs!Nombre
        Grid.TextMatrix(Linea, ColCantSolicitada) = FormatNumber(Rs!CantSolicitada, Rs!Cant_Decimales)
        Grid.TextMatrix(Linea, ColCantRecolectada) = FormatNumber(Rs!CantRecolectada, Rs!Cant_Decimales)
        Grid.TextMatrix(Linea, ColCantEmpacada) = FormatNumber(Rs!CantEmpacada, Rs!Cant_Decimales)
        
        If Rs!Packing And Rs!Picking Then
            
            If CDec(Rs!CantSolicitada) = CDec(Rs!CantEmpacada) Then
                For I = 0 To Grid.Cols - 1
                    Grid.Row = Grid.Rows - 1
                    Grid.Col = I
                    Grid.CellBackColor = &HB4EDB9 'verde
                Next I
            Else
                For I = 0 To Grid.Cols - 1
                    Grid.Row = Grid.Rows - 1
                    Grid.Col = I
                    Grid.CellBackColor = 12632319 'rojo
                Next I
            End If
            
        ElseIf (Rs!CantEmpacada > 0) Then
            For I = 0 To Grid.Cols - 1
                Grid.Row = Grid.Rows - 1
                Grid.Col = I
                Grid.CellBackColor = -2147483624 'Amarillo
            Next I
        End If
        
        Rs.MoveNext
        
    Wend
    
    Rs.Close
    
    If Grid.Row > 1 Then
        Grid.Row = 1
        Fila = 1 ' Para que se cambie el tama�o en el EnterCell
        Grid.ColSel = ColCantEmpacada
    End If
    
    If Grid.Rows > 14 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(ColDesPro) = AnchoCampoDescripcion - 675
    Else
        Grid.ScrollBars = flexScrollBarNone
        ScrollGrid.Visible = False
    End If
    
    Grid_EnterCell
    
Finally:
    
    LabelTitulo.Caption = "Detalles del Pedido " & Pedido
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub Grid_EnterCell()
        
    If Grid.Rows = 1 Then
        'FrameSelect.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = Grid.RowData(Fila) '600
            End If
            
            Grid.RowHeight(Grid.Row) = Grid.RowData(Grid.Row) + 300 '900
            Fila = Grid.Row
            
        End If
        
        'MostrarEditorTexto2 Me, Grid, CmdSelect
        Grid.ColSel = ColNull
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        
    Else
        Band = False
    End If
    
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub
