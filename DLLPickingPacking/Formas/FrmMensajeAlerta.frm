VERSION 5.00
Begin VB.Form FrmMensajeAlerta 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3435
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   6945
   ControlBox      =   0   'False
   Icon            =   "FrmMensajeAlerta.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3435
   ScaleWidth      =   6945
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Mensaje 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1200
      Left            =   270
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   635
      Width           =   6525
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&SI"
      CausesValidation=   0   'False
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   4080
      Picture         =   "FrmMensajeAlerta.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2160
      Width           =   1095
   End
   Begin VB.CommandButton Cancelar 
      Caption         =   "&No"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   5400
      Picture         =   "FrmMensajeAlerta.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2160
      Width           =   1095
   End
   Begin VB.Frame Frame6 
      BackColor       =   &H000000C0&
      BorderStyle     =   0  'None
      Height          =   500
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   120
         Width           =   4695
      End
      Begin VB.Label lbl_Website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   4800
         TabIndex        =   1
         Top             =   120
         Width           =   1815
      End
   End
   Begin VB.Image Icono 
      Appearance      =   0  'Flat
      Height          =   1740
      Left            =   960
      Picture         =   "FrmMensajeAlerta.frx":9D8E
      Top             =   2000
      Width           =   1740
   End
   Begin VB.Label lblMarginTop 
      BackColor       =   &H80000005&
      Height          =   1395
      Left            =   165
      TabIndex        =   5
      Top             =   540
      Width           =   6630
   End
End
Attribute VB_Name = "FrmMensajeAlerta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function SendMessage Lib "user32" _
                        Alias "SendMessageA" (ByVal hWnd As Long, _
                                              ByVal wMsg As Long, _
                                              ByVal wParam As Long, _
                                              lParam As Any) As Long

Private Declare Sub ReleaseCapture Lib "user32" ()
Const WM_NCLBUTTONDOWN = &HA1
Const HTCAPTION = 2

Private Sub Frame6_MouseMove(Button As Integer, Shift As Integer, _
X As Single, Y As Single)
    
    On Error Resume Next
    
    Dim lngReturnValue As Long
    
    If Button = 1 Then
        Call ReleaseCapture
        lngReturnValue = SendMessage( _
        Me.hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0&)
    End If
    
End Sub

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    If OrMode Then
        PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
    Else
        PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
    End If
End Function

Private Sub aceptar_Click()
    Tecla_pulsada = False
    Retorno = True
    Unload Me
End Sub

Private Sub cancelar_Click()
    Form_KeyDown vbKeyF12, 0
End Sub

Private Sub Exit_Click()
    Form_KeyDown vbKeyF12, 0
End Sub

Private Sub Form_Activate()
    
    ModalDisponible = False
    
    ' C�digo para imponer el mensaje al usuario, ya que a veces los mensajes
    ' Quedan detr�s de otras ventanas cuando se produce un status inactivo en la aplicaci�n.
    
    DoEvents
    Me.Refresh
    
    'Traslado la funci�n de gRutinas para que este formulario sea portable y no requiera componentes externos.
    
    If PuedeObtenerFoco(Me) Then Me.SetFocus
    If PuedeObtenerFoco(Aceptar) Then Aceptar.SetFocus
    
    If Uno = True Then
        Cancelar.Enabled = False
    Else
        Cancelar.Enabled = True
    End If
    
    DoEvents
    
    On Error Resume Next
    SetWindowPos Me.hWnd, -1, 0, 0, 0, 0, (&H2 + &H1)
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF12
            Retorno = False
            Unload Me
    End Select
End Sub

Private Sub Form_Load()
    
    AjustarPantalla Me
    
    Screen.MousePointer = vbDefault
    Me.lbl_Organizacion.Caption = Stellar_Mensaje(11005) '"Informaci�n Stellar"
    
    Aceptar.Caption = Stellar_Mensaje(5) ' Aceptar
    Cancelar.Caption = Stellar_Mensaje(6) ' Cancelar
    
    If Uno = True Then
        Cancelar.Enabled = False
        'cancelar.Visible = False
        'frm_Mensajeria.Width = 7605
    Else
        Cancelar.Enabled = True
        
        If txtMensaje1 <> "" Then Aceptar.Caption = txtMensaje1
        If txtMensaje2 <> "" Then Cancelar.Caption = txtMensaje2
        
        'cancelar.Visible = True
        'frm_Mensajeria.Width = 8730
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frm_Mensajeria = Nothing
    ModalDisponible = True
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Frame6_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Frame6_MouseMove Button, Shift, X, Y
End Sub
