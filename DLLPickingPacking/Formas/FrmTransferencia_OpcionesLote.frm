VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form FrmTransferencia_OpcionesLote 
   BackColor       =   &H8000000B&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3615
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   8445
   ControlBox      =   0   'False
   Icon            =   "FrmTransferencia_OpcionesLote.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3615
   ScaleWidth      =   8445
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox lblCmdNum 
      BackColor       =   &H00AE5B00&
      Caption         =   "Ver Lote"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FAFAFA&
      Height          =   750
      Index           =   3
      Left            =   5760
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   2400
      Width           =   2175
   End
   Begin VB.CheckBox lblCmdNum 
      BackColor       =   &H00AE5B00&
      Caption         =   "Modificar / Crear Nuevo Lote"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FAFAFA&
      Height          =   750
      Index           =   2
      Left            =   3120
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   2400
      Width           =   2175
   End
   Begin VB.CheckBox lblCmdNum 
      BackColor       =   &H00AE5B00&
      Caption         =   "Cambiar Fase"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FAFAFA&
      Height          =   750
      Index           =   1
      Left            =   480
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   2400
      Width           =   2175
   End
   Begin VB.Timer ClickHandler 
      Enabled         =   0   'False
      Left            =   5400
      Top             =   7320
   End
   Begin VB.CheckBox CmdNum 
      BackColor       =   &H00AE5B00&
      Caption         =   "3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FAFAFA&
      Height          =   1800
      Index           =   3
      Left            =   5760
      Style           =   1  'Graphical
      TabIndex        =   5
      Tag             =   "C"
      Top             =   840
      Width           =   2175
   End
   Begin VB.CheckBox CmdNum 
      BackColor       =   &H00AE5B00&
      Caption         =   "2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FAFAFA&
      Height          =   1800
      Index           =   2
      Left            =   3120
      Style           =   1  'Graphical
      TabIndex        =   4
      Tag             =   "B"
      Top             =   840
      Width           =   2175
   End
   Begin VB.CheckBox CmdNum 
      BackColor       =   &H00AE5B00&
      Caption         =   "1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FAFAFA&
      Height          =   1800
      Index           =   1
      Left            =   480
      Style           =   1  'Graphical
      TabIndex        =   3
      Tag             =   "A"
      Top             =   840
      Width           =   2175
   End
   Begin MSFlexGridLib.MSFlexGrid GridEvitarFoco 
      Height          =   30
      Left            =   2160
      TabIndex        =   0
      Top             =   720
      Width           =   135
      _ExtentX        =   238
      _ExtentY        =   53
      _Version        =   393216
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   500
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8520
      Begin VB.Image CmdClose 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   7875
         Picture         =   "FrmTransferencia_OpcionesLote.frx":628A
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione una Opci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   195
         TabIndex        =   2
         Top             =   105
         Width           =   8040
      End
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   30
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   53
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog RutaArchivo 
      Left            =   7800
      Top             =   5640
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "FrmTransferencia_OpcionesLote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private FormaCargada As Boolean

'Public Valor As Double
Public ValorCaption As String
Public LblDescValor As String

Const DoubleClickTimeThreshold = 450
Private InitialClickTime As Date
Private DoubleClickAttemptTime As Date

'Private CHDelegate As New ClsDelegate

'Private Sub ClickHandler_Timer()
'    ClickHandler.Enabled = False
'    ClickTask
'End Sub
'
'Private Sub ClickTask()
'
'    Select Case CHDelegate.Name
'
'        Case "SelectNumber"
'
'            On Error Resume Next
'
'            Select Case CHDelegate.Param("Index")
'
'                Case 0 To 9
'
'                    CmdValor.Caption = CHDelegate.Param("Index")
'                    CmdValor_Click
'
'                Case 10
'
'                    CmdValor.Caption = CStr(Val(CmdValor.Caption) + 1)
'
'                Case 11
'
'                    CmdValor.Caption = CStr(Val(CmdValor.Caption) - 1)
'
'            End Select
'
'    End Select
'
'End Sub

Private Sub EventosNumClick(Index As Integer)
    
    'If (InitialClickTime = 0) Then
        'CmdNumberClick Index
    'Else
        
        'On Error Resume Next
        
        'DoubleClickAttemptTime = GetSysTime(True)
        
        'Dim TimeTillLastClick As Variant
        'TimeTillLastClick = (DoubleClickAttemptTime - InitialClickTime)
        
        'If (Fix(TimeTillLastClick * 100000000) <= DoubleClickTimeThreshold) Then
            'ClickHandler.Enabled = False ' Avoid ClickTask Because this is a double click and should continue with the Following Code
            'InitialClickTime = 0
            
            'CmdNumberDoubleClick Index
        'Else
            CmdNumberClick Index
        'End If
        
    'End If
    
End Sub

Private Sub CmdNumberClick(Index As Integer)
    
    On Error GoTo Error
    
    'InitialClickTime = GetSysTime(True)
    'DoubleClickAttemptTime = 0
    
    'CHDelegate.Initialize
    'CHDelegate.Name = "SelectNumber"
    'CHDelegate.SetParam "Index", "Long", Index
    
    'ClickHandler.Interval = DoubleClickTimeThreshold
    'ClickHandler.Enabled = True
    
    ValorCaption = CmdNum(Index).Tag
    LblDescValor = lblCmdNum(Index).Caption
    
    Unload Me
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If mErrorNumber <> 32755 Then
        MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(FrmTransferencia_OpcionesLote_Click)"
    End If
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
End Sub

Private Sub CmdClose_Click()
    
    If Not CmdClose.Visible Then
        Exit Sub
    End If
    
    ValorCaption = Empty
    LblDescValor = Empty
    
    Unload Me
    
End Sub

'Private Sub CmdNumberDoubleClick(Index As Integer)

'End Sub

Private Sub CmdNum_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If CmdNum(Index).Value = vbUnchecked And lblCmdNum(Index).Value = vbUnchecked Then
        Exit Sub
    End If
    
    Call EventosNumClick(Index)
    
    If gRutinas.PuedeObtenerFoco(GridEvitarFoco) Then
        GridEvitarFoco.SetFocus
    End If
    
    CmdNum(Index).Value = vbUnchecked
    lblCmdNum(Index).Value = vbUnchecked
    
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        If PuedeObtenerFoco(GridEvitarFoco) Then
            GridEvitarFoco.SetFocus
        End If
    
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF1, vbKey1
            CmdNum(1).Value = vbChecked
            CmdNum_MouseUp 1, 0, 0, 0, 0
        Case vbKeyF2, vbKey2
            CmdNum(2).Value = vbChecked
            CmdNum_MouseUp 2, 0, 0, 0, 0
        Case vbKeyF3, vbKey3
            CmdNum(3).Value = vbChecked
            CmdNum_MouseUp 3, 0, 0, 0, 0
        Case vbKeyF4, vbKey4
            CmdNum(4).Value = vbChecked
            CmdNum_MouseUp 4, 0, 0, 0, 0
        Case vbKeyF5, vbKey5
            CmdNum(5).Value = vbChecked
            CmdNum_MouseUp 5, 0, 0, 0, 0
        Case vbKeyF12, vbKeyEscape
            CmdClose_Click
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Form_KeyDown KeyAscii, 0
End Sub

Private Sub Form_Load()

    FormaCargada = False
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If Me.Visible Then
        Me.Hide
    Else
        Cancel = 0
        'Unload
    End If
    
End Sub

Private Sub lblCmdNum_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call CmdNum_MouseUp(Index, Button, Shift, X, Y)
End Sub
