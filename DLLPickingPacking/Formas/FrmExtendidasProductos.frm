VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmExtendidasProductos 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7005
   ClientLeft      =   15
   ClientTop       =   -45
   ClientWidth     =   12375
   ControlBox      =   0   'False
   Icon            =   "FrmExtendidasProductos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7005
   ScaleWidth      =   12375
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   12564
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   11880
         Picture         =   "FrmExtendidasProductos.frx":628A
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Productos con Caracteristicas Extendidas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   252
         Left            =   9912
         TabIndex        =   3
         Top             =   72
         Width           =   1812
      End
   End
   Begin VB.Frame FrameSalida 
      BorderStyle     =   0  'None
      Caption         =   " Productos Con CaracterÝsticas Extendidas"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6300
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   12216
      Begin VB.CheckBox chk_SinExistencia 
         Appearance      =   0  'Flat
         Caption         =   "Incluir Productos Sin Existencia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   0
         MaskColor       =   &H00585A58&
         TabIndex        =   7
         Top             =   5280
         Width           =   3012
      End
      Begin VB.CommandButton btn_Agregar 
         Caption         =   "Agregar"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Left            =   10800
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Importa los productos a su planilla de trabajo"
         Top             =   5280
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.TextBox TxtEdit 
         Appearance      =   0  'Flat
         BackColor       =   &H00404040&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   135
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   375
         Visible         =   0   'False
         Width           =   945
      End
      Begin MSFlexGridLib.MSFlexGrid Fg1 
         CausesValidation=   0   'False
         Height          =   5112
         Left            =   0
         TabIndex        =   5
         Top             =   0
         Width           =   12000
         _ExtentX        =   21167
         _ExtentY        =   9022
         _Version        =   393216
         Cols            =   1
         FixedCols       =   0
         RowHeightMin    =   350
         ForeColor       =   5790296
         BackColorFixed  =   11426560
         ForeColorFixed  =   16448250
         BackColorBkg    =   15198440
         GridColor       =   8421504
         AllowBigSelection=   0   'False
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "FrmExtendidasProductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public StrBtnAntPre As String
Public StrBtnActPre As String
Public blnPantallaActivada As Boolean
Dim Std_Decm As Integer

Public CodPlantilla As String
Public CodProducto As String
Public TipoPrecio As String
Public Forma As String
Public CodDeposito As String

Public Codigo As String

Private Sub cmdCancel_Click()
    Exit_Click
End Sub

Private Sub Btn_Agregar_Click()
    
'    Dim RsTemp As New ADODB.Recordset
'
'    Call Apertura_RecordsetC(RsTemp)
'
'    RsTemp.Open "SELECT * From MA_PLANTILLAS WHERE c_Cod_Plantilla = '" & Trim(CodPlantilla) & "'  ORDER BY RELACION, PRIORIDAD ", _
'    Ent.BDD, adOpenStatic, adLockReadOnly, adCmdText
'
'    If Not RsTemp.EOF Then
'
'        Dim CampoClave
'
'        CampoClave = RsTemp!clave
'
'        Dim RsPlantillaProd As New ADODB.Recordset
'
'        Call Apertura_RecordsetC(RsPlantillaProd)
'
'        RsPlantillaProd.Open "Select * from MA_PLANTILLAS WHERE RELACION = '" & CampoClave & "'  ORDER BY RELACION, PRIORIDAD", _
'        Ent.BDD, adOpenStatic, adLockReadOnly, adCmdText
'
'        If RsPlantillaProd.EOF Then
'            Mensaje True, "La Plantilla no Posee Caracteristicas Extendidas"
'            Exit Sub
'        End If
'
'        While Not RsPlantillaProd.EOF
'
'            Dim Caracteristica
'            Dim StrCadena As String
'
'            Dim Date1 As Date
'            Dim vText1, vText2, vText3, vText4, vText5, vText6, vText7, vText8 As String
'
'            If UCase(RsPlantillaProd!Campo) = UCase("Date1") Then
'
'                Dim ClsFecha As clsFechaSeleccion
'
'                Set ClsFecha = New clsFechaSeleccion
'
'                ClsFecha.MostrarInterfazFechaHora JustDate, "Ingrese " & RsPlantillaProd!texto & ":"
'
'                If ClsFecha.Selecciono Then
'                    Date1 = ClsFecha.FECHA
'                    Caracteristica = Format(Date1, "YYYYMMDD")
'                Else
'                    'Manejar codigo para echar todo pa atras y no grabar el producto.
'''                    txtedit.SelText = ""
'''                    txtedit.Text = txtedit.SelText
'''                    habilitar_grupos (True)
'''                    If fg2.Enabled And fg2.Visible Then fg2.SetFocus
'''                    fg2.Col = 1
'                    Exit Sub
'                End If
'
'            Else
'
'                Caracteristica = QuickInputRequest("Ingrese " & RsPlantillaProd!texto & ":", , , , _
'                , , , , , , ModoTouch, True)
'
'                If Trim(Caracteristica) <> "" Then
'
'                    Select Case UCase(RsPlantillaProd!Campo)
'
'                        Case UCase("Text1")
'                            vText1 = Caracteristica
'
'                        Case UCase("Text2")
'                            vText2 = Caracteristica
'
'                        Case UCase("Text3")
'                            vText3 = Caracteristica
'
'                        Case UCase("Text4")
'                            vText4 = Caracteristica
'
'                        Case UCase("Text5")
'                            vText5 = Caracteristica
'
'                        Case UCase("Text6")
'                            vText6 = Caracteristica
'
'                        Case UCase("Text7")
'                            vText7 = Caracteristica
'
'                        Case UCase("Text8")
'                            vText8 = Caracteristica
'
'                    End Select
'
'                Else
'
'''                    txtedit.SelText = ""
'''                    txtedit.Text = txtedit.SelText
'''                    habilitar_grupos (True)
'''                    If fg2.Enabled And fg2.Visible Then fg2.SetFocus
'''                    fg2.Col = 1
'                    Exit Sub
'
'                End If
'
'            End If
'
'            StrCadena = StrCadena & RTrim(Caracteristica) & "-"
'
'            RsPlantillaProd.MoveNext
'
'        Wend
'
'        StrCadena = Mid(StrCadena, 1, Len(StrCadena) - Len("-"))
'        StrCadena = "(" & StrCadena & ")"
'
'    End If
'
'    Call Apertura_RecordsetC(RsProductos)
'
'    RsProductos.Open "SELECT * from MA_PRODUCTOS WHERE c_Codigo = '" & CodProducto & "'", _
'    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
'
'    Set RsProductos.ActiveConnection = Nothing
'
'    Dim RsProductoExist As New Recordset
'
'    Call Apertura_RecordsetC(RsProductoExist)
'
'    RsProductoExist.Open "select * from MA_productos where c_Descri = '" & Trim(RsProductos!c_Descri) & Trim(StrCadena) & "'", _
'    Ent.BDD, adOpenDynamic, adLockReadOnly
'
'    Dim Cod As String
'
'    If Not RsProductoExist.EOF Then
'
'        Call Apertura_RecordsetC(RsProductos)
'
'        RsProductos.Open "select * from MA_productos where c_Codigo = '" & RsProductoExist!c_Codigo & "'", _
'        Ent.BDD, adOpenDynamic, adLockReadOnly
'
'        Set RsProductos.ActiveConnection = Nothing
'
'        Codigo = RsProductoExist!c_Codigo
'
'    Else
'
'        Dim RsProductosExt As New ADODB.Recordset
'        Dim RsMa_Codigos As New ADODB.Recordset
'
'        RsProductosExt.Open "SELECT * FROM MA_PRODUCTOS WHERE 1 = 2 ", _
'        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
'
'        RsProductosExt.AddNew
'        RsProductosExt!c_Codigo = "7" & MyExte.intProximoConsecutivoMA_CO(True, "n_Prod_Ext") 'Me.intProximoConsecutivo(Me.strc_Codigo_Ext)
'
'        Cod = RsProductosExt!c_Codigo
'
'        RsProductosExt!c_Descri = Trim(RsProductos!c_Descri) & Trim(StrCadena)
'
'        RsProductosExt!n_Activo = 1
'
'        RsProductosExt!c_Codigo_Base = RsProductos!c_Codigo
'        RsProductosExt!c_Descri_Base = RsProductos!c_Descri
'
'        RsProductosExt!c_Departamento = RsProductos!c_Departamento
'        RsProductosExt!c_Grupo = RsProductos!c_Grupo
'        RsProductosExt!c_Subgrupo = RsProductos!c_Subgrupo
'
'        RsProductosExt!c_Modelo = Trim(StrCadena)
'
'        RsProductosExt!n_CostoAct = RsProductos!n_CostoAct
'        RsProductosExt!n_CostoAnt = RsProductos!n_CostoAnt
'        RsProductosExt!n_CostoPro = RsProductos!n_CostoPro
'        RsProductosExt!n_CostoRep = RsProductos!n_CostoRep
'
'        RsProductosExt!n_Precio1 = RsProductos!n_Precio1
'        RsProductosExt!n_Precio2 = RsProductos!n_Precio2
'        RsProductosExt!n_Precio3 = RsProductos!n_Precio3
'
'        RsProductosExt!n_Impuesto1 = RsProductos!n_Impuesto1
'        RsProductosExt!n_Impuesto2 = RsProductos!n_Impuesto2
'        RsProductosExt!n_Impuesto3 = RsProductos!n_Impuesto3
'
'        RsProductosExt!c_CodMoneda = RsProductos!c_CodMoneda
'
'        If Date1 <> 0 Then
'            RsProductosExt!Date1 = FechaBD(Date1, FBd_Fecha, True)
'        End If
'
'        If Trim(vText1) <> "" Then
'            RsProductosExt!Text1 = vText1
'        End If
'
'        If Trim(vText2) <> "" Then
'            RsProductosExt!Text2 = vText2
'        End If
'
'        If Trim(vText3) <> "" Then
'            RsProductosExt!Text3 = vText3
'        End If
'
'        If Trim(vText4) <> "" Then
'            RsProductosExt!Text4 = vText4
'        End If
'
'        If Trim(vText5) <> "" Then
'            RsProductosExt!Text5 = vText5
'        End If
'
'        If Trim(vText6) <> "" Then
'            RsProductosExt!text6 = vText6
'        End If
'
'        If Trim(vText7) <> "" Then
'            RsProductosExt!Text7 = vText7
'        End If
'
'        If Trim(vText8) <> "" Then
'            RsProductosExt!Text8 = vText8
'        End If
'
'        RsProductosExt.UpdateBatch
'
'        RsMa_Codigos.Open "SELECT * FROM MA_CODIGOS WHERE 1 = 2", _
'        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
'
'        RsMa_Codigos.AddNew
'            RsMa_Codigos!c_Codigo = Cod
'            RsMa_Codigos!c_CodNasa = Cod
'            RsMa_Codigos!C_DESCRIPCION = "CODIGO MAESTRO"
'            If ExisteCampoTabla("nu_Intercambio", RsMa_Codigos) Then RsMa_Codigos!nu_Intercambio = 1
'            RsMa_Codigos!n_CANTIDAD = 1
'        RsMa_Codigos.UpdateBatch
'
'        Call Apertura_RecordsetC(RsProductos)
'
'        RsProductos.Open "select * from MA_productos where c_Codigo = '" & Cod & "'", _
'        Ent.BDD, adOpenDynamic, adLockReadOnly
'
'        Set RsProductos.ActiveConnection = Nothing
'
'        Codigo = Cod
'
'    End If
'
'    If Not RsProductos.EOF Then
'
'        Linea = Fg1.Rows
'
'        Fg1.Rows = Linea + 1
'
'        Fg1.TextMatrix(Linea, 0) = RsProductos!c_Codigo
'
'        Fg1.TextMatrix(Linea, 1) = Trim(RsProductos!c_Descri)
'
'        Select Case TipoPrecio
'
'            Case Is = 1
'                Fg1.TextMatrix(Linea, 2) = FormatNumber(RsProductos!n_Precio1, Std_Decm)
'
'            Case Is = 2
'                Fg1.TextMatrix(Linea, 2) = FormatNumber(RsProductos!n_Precio2, Std_Decm)
'
'            Case Is = 3
'                Fg1.TextMatrix(Linea, 2) = FormatNumber(RsProductos!n_Precio3, Std_Decm)
'
'            Case Else
'                Fg1.TextMatrix(Linea, 2) = FormatNumber(RsProductos!n_Precio1, Std_Decm)
'
'        End Select
'
'        Fg1.TextMatrix(Linea, 3) = "0"
'
'    End If
    
End Sub

Private Sub chk_SinExistencia_Click()
    Call CargarGrid
End Sub

Private Sub Exit_Click()
    
    Codigo = Empty
    
    Unload Me
    
End Sub

Private Sub Fg1_DblClick()
    
    If Fg1.Rows <= 1 Then Exit Sub
    
    Dim TmpResult
    
    TmpResult = Fg1.TextMatrix(Fg1.Row, 0)
    
    Codigo = TmpResult
    
'    ReDim TmpResult(0 To (Grid.Cols - 1))
'
'    For i = 0 To UBound(TmpResult)
'        TmpResult(i) = Grid.TextMatrix(Grid.Row, 0)
'    Next i
'
'    ArrResultado = TmpResult
'
    Unload Me
    
End Sub

Private Sub fg1_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call Fg1_DblClick
    End If
End Sub

Private Sub Form_Load()
    
    Call CargarGrid
    
End Sub

Private Sub CargarGrid()
    
    'Set MyExte = New ClsExtendidas
    
    Fg1.Clear
    Fg1.SelectionMode = flexSelectionByRow
    
    'Fg1.Cols = 4
    
    Select Case Forma
        
        Case "Factura"
            
            Fg1.Cols = 4
            
            Call MSGridAsign(Fg1, 0, 0, Stellar_Mensaje(142), 2000, flexAlignCenterCenter) 'Codigo
            Call MSGridAsign(Fg1, 0, 1, Stellar_Mensaje(143), 5000, flexAlignCenterCenter) 'Descripcion
            Call MSGridAsign(Fg1, 0, 2, "Precio", 2000, flexAlignCenterCenter) 'Precio
            Call MSGridAsign(Fg1, 0, 3, "Existencia", 2000, flexAlignCenterCenter) 'Existencia
            
            btn_Agregar.Visible = False
            
        Case "InvFisico", "Ajuste"
            
            Fg1.Cols = 4
            
            Call MSGridAsign(Fg1, 0, 0, Stellar_Mensaje(142), 3000, flexAlignCenterCenter) 'Codigo
            Call MSGridAsign(Fg1, 0, 1, Stellar_Mensaje(143), 6000, flexAlignCenterCenter) 'Descripcion
            Call MSGridAsign(Fg1, 0, 2, "Precio", 2000, flexAlignCenterCenter) 'Precio
            Call MSGridAsign(Fg1, 0, 3, "Existencia", 0, flexAlignCenterCenter) 'Existencia
            
            btn_Agregar.Visible = True
            
        Case "Pedido"
            
            Fg1.Cols = 5
            
            Call MSGridAsign(Fg1, 0, 0, Stellar_Mensaje(142), 2000, flexAlignCenterCenter) 'Codigo
            Call MSGridAsign(Fg1, 0, 1, Stellar_Mensaje(143), 4500, flexAlignCenterCenter) 'Descripcion
            Call MSGridAsign(Fg1, 0, 2, "Precio", 1700, flexAlignCenterCenter) 'Precio
            Call MSGridAsign(Fg1, 0, 3, "Existencia", 1700, flexAlignCenterCenter) 'Existencia
            Call MSGridAsign(Fg1, 0, 4, "Disponible", 1700, flexAlignCenterCenter) 'Disponible
            
            btn_Agregar.Visible = False
            
        Case "Packing"
            
            Fg1.Cols = 5
            
            Call MSGridAsign(Fg1, 0, 0, Stellar_Mensaje(142), 2000, flexAlignCenterCenter) 'Codigo
            Call MSGridAsign(Fg1, 0, 1, Stellar_Mensaje(143), 4500, flexAlignCenterCenter) 'Descripcion
            Call MSGridAsign(Fg1, 0, 2, "Precio", 1700, flexAlignCenterCenter) 'Precio
            Call MSGridAsign(Fg1, 0, 3, "Existencia", 1700, flexAlignCenterCenter) 'Existencia
            Call MSGridAsign(Fg1, 0, 4, "Disponible", 1700, flexAlignCenterCenter) 'Disponible
            
            btn_Agregar.Visible = False
            
            DPS_Local = CodDeposito
            
        Case Else
            
            Fg1.Cols = 4
            
            Call MSGridAsign(Fg1, 0, 0, Stellar_Mensaje(142), 2000, flexAlignCenterCenter) 'Codigo
            Call MSGridAsign(Fg1, 0, 1, Stellar_Mensaje(143), 5000, flexAlignCenterCenter) 'Descripcion
            Call MSGridAsign(Fg1, 0, 2, "Precio", 2000, flexAlignCenterCenter) 'Precio
            Call MSGridAsign(Fg1, 0, 3, "Existencia", 2000, flexAlignCenterCenter) 'Existencia
            'Call MSGridAsign(Fg1, 0, 4, "Disponible", 2000, flexAlignCenterCenter) 'Disponible
            
            btn_Agregar.Visible = False
            
    End Select
    
    Dim RsProductosExt As New ADODB.Recordset, StrSQL As String
    
    StrSQL = Empty
    
    If chk_SinExistencia.Value = 0 Then
        
        StrSQL = "SELECT *, isnull((SELECT n_cantidad  FROM MA_DEPOPROD WHERE c_codarticulo= c_Codigo AND c_coddeposito = '" & Trim(DPS_Local) & "'),0) AS existencia " & _
        ",isnull((SELECT n_cantidad-n_cant_comprometida  FROM MA_DEPOPROD WHERE c_codarticulo= c_Codigo AND c_coddeposito = '" & Trim(DPS_Local) & "'),0) as disponible " & _
        "FROM MA_PRODUCTOS WHERE c_Codigo_Base = '" & Trim(CodProducto) & "' AND n_Activo = 1 " & _
        "AND isnull((SELECT n_cantidad  FROM MA_DEPOPROD WHERE c_codarticulo= c_Codigo AND c_coddeposito = '" & Trim(DPS_Local) & "'),0)>0 " & _
        "order by date1 asc"
        
    Else
        
        StrSQL = "SELECT *, isnull((SELECT n_cantidad  FROM MA_DEPOPROD WHERE c_codarticulo= c_Codigo AND c_coddeposito = '" & Trim(DPS_Local) & "'),0) AS existencia, " & _
        "isnull((SELECT n_cantidad-n_cant_comprometida  FROM MA_DEPOPROD WHERE c_codarticulo= c_Codigo AND c_coddeposito = '" & Trim(DPS_Local) & "'),0) as disponible " & _
        "FROM MA_PRODUCTOS WHERE c_Codigo_Base = '" & Trim(CodProducto) & "' AND n_Activo = 1 " & _
        "order by date1 asc"
        
    End If
    
'    If Me.StrOrderBy <> "" Then
'        StrSQL = StrSQL + " ORDER BY " & Me.StrOrderBy
'    Else
'        Call Mensaje(True, "Esta plantilla no se encuentra correctamente configurada.")
'    End If
    
    RsProductosExt.Open StrSQL, Ent.BDD, adOpenStatic, adLockReadOnly, adCmdText
    
    Fg1.Row = Fg1.Rows - 1
    
    Fila = 1
    
    Do While Not RsProductosExt.EOF
        
        Fg1.Rows = Fila + 1
        
        Fg1.TextMatrix(Fila, 0) = RsProductosExt!c_Codigo
        Fg1.TextMatrix(Fila, 1) = Trim(RsProductosExt!c_Descri)
        
        Select Case TipoPrecio
            
            Case Is = 1
                Fg1.TextMatrix(Fila, 2) = FormatNumber(RsProductosExt!n_Precio1, Std_Decm)
                
            Case Is = 2
                Fg1.TextMatrix(Fila, 2) = FormatNumber(RsProductosExt!n_Precio2, Std_Decm)
                
            Case Is = 3
                Fg1.TextMatrix(Fila, 2) = FormatNumber(RsProductosExt!n_Precio3, Std_Decm)
                
            Case Else
                Fg1.TextMatrix(Fila, 2) = FormatNumber(RsProductosExt!n_Precio1, Std_Decm)
               
        End Select
        
        Fg1.TextMatrix(Fila, 3) = Trim(RsProductosExt!Existencia)
        
        If Fg1.Col > 3 Then
            Fg1.TextMatrix(Fila, 4) = Trim(RsProductosExt!Disponible)
        End If
        
'        Fg1.Col = 0
'        Fg1.Text = rsProductosExt!c_Codigo
'
'        Fg1.Col = 1
'        Fg1.Text = Trim(rsProductosExt!c_Descri)
'
'        Fg1.Rows = Fg1.Rows + 1
'        Fg1.Row = Fg1.Rows - 1
        
        Fila = Fila + 1
        RsProductosExt.MoveNext
        
    Loop
    
    RsProductosExt.Close
    
    If Fg1.Rows > 1 Then
        Fg1.RowSel = Fg1.Row
        Fg1.Col = 0
        Fg1.ColSel = Fg1.Cols - 1
        'Fg1.SetFocus
    End If
    
End Sub
