VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frmLeyendaGridStellar 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6675
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12405
   ControlBox      =   0   'False
   DrawStyle       =   5  'Transparent
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLeyendaGridStellar.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6675
   ScaleWidth      =   12405
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   12480
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   10035
         TabIndex        =   5
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.ComboBox CmbProceso 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   390
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   720
      Width           =   5500
   End
   Begin VB.CommandButton Regresar 
      Caption         =   "Salir"
      CausesValidation=   0   'False
      Height          =   1065
      Left            =   10880
      Picture         =   "frmLeyendaGridStellar.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   5450
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid GridAlertas 
      Height          =   3930
      Left            =   270
      TabIndex        =   2
      Top             =   1320
      Width           =   11835
      _ExtentX        =   20876
      _ExtentY        =   6932
      _Version        =   393216
      Rows            =   1
      Cols            =   3
      FixedRows       =   0
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      AllowBigSelection=   0   'False
      GridLinesFixed  =   0
      ScrollBars      =   2
      AllowUserResizing=   2
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmLeyendaGridStellar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Const ColorSobrePasaStock As Long = &HC0C0C0 'Color cuando la Existencia + Pedidos est� sobre el Stock M�ximo (Al hacer ODC y Lista de Precios).
Private Const ColorBajoStockMinimo As Long = &HFFC0C0 'Color cuando la Existencia est� por debajo del Stock M�nimo (Al Cargar ProdxProv).
Private Const ColorVariacionCostoActual As Long = &HC0FFFF 'Color cuando var�a el Costo Actual (Al hacer registro de Facturas)(Y Solo si PV_Estimacion = 0 [CostoActual])
Private Const ColorExistenciaNegativa As Long = &HC0C0FF 'Color cuando el Producto posee Existencia Negativa (Al Cargar ProdxProv).
Private Const ColorAumentaCostoRep As Long = &HFFC0C0 'Color cuando Aumenta el Costo de Reposici�n (Al Hacer Lista de Precios Manual)(Y Solo si PV_Estimacion = 3 [CostoReposicion])
Private Const ColorDisminuyeCostoRep As Long = &HC0C0FF 'Color cuando Disminuye el Costo de Reposici�n (Al Hacer Lista de Precios Manual)(Y Solo si PV_Estimacion = 3 [CostoReposicion])
Private Const ColorNormalLineaGrid As Long = &H80000005 'Color Blanco - Por defecto en la mayor�a de los Grid de Stellar. No Indica ningun aviso particular.

Public CodigoProcesoInicial As String

Private Sub CmbProceso_Click()
    
    If CmbProceso.ListIndex <> -1 Then
        
        Dim CodigoProcesoSeleccionado As String
        
        CodigoProcesoSeleccionado = CStr(CmbProceso.ItemData(CmbProceso.ListIndex))
        
        CargarGrid
        
        CargarAlertas CodigoProcesoSeleccionado
        
    Else
        
        CargarGrid
        
    End If
    
End Sub

Private Sub CargarAlertas(pCodProceso As String)
    
    Dim mRs As ADODB.Recordset, TempReg As Long
    
    Dim mSQL As String
    
    mSQL = "SELECT A.c_Alerta, A.c_Descripcion_Evento, A.n_ColorLineaGrid, PxA.c_Proceso " & _
    "FROM MA_ALERTAGRID_ALERTAS A " & _
    "INNER JOIN MA_ALERTAGRID_PROCESOxALERTA PxA " & _
    "ON A.c_Alerta = PxA.c_CodAlerta " & _
    "AND CASE " & _
    "WHEN ISNUMERIC(PxA.c_Proceso) = 1 THEN " & _
    "CAST(PxA.c_Proceso AS NUMERIC(18, 0)) " & _
    "ELSE " & _
    "0 " & _
    "END = " & CLng(SVal(pCodProceso)) & " "
    
    'Debug.Print mSQL
    
    Set mRs = Ent.BDD.Execute(mSQL, TempReg)
    
    'Debug.Print TempReg
    
    If Not mRs.EOF Then
        
        I = 1
        
        While Not mRs.EOF
            
            With GridAlertas
                
                .Rows = .Rows + 1
                .Row = .Rows - 1
                .GridLines = flexGridInset
                .TextMatrix(.Row, 0) = mRs!c_Descripcion_Evento
                .TextMatrix(.Row, 1) = "Texto Gen�rico"
                .RowHeight(.Row) = 550 'IIf(Len(mRs!c_Descripcion_Evento) > 45, 600, 350)
                
                gRutinas.CambiarColorFila GridAlertas, .Row, CLng(mRs!n_ColorLineaGrid)
            
            End With
            
            mRs.MoveNext
            
        Wend
        
    End If
    
End Sub

Private Sub Form_Load()
    
    CargarGrid
    
    Me.lbl_Organizacion.Caption = "Leyenda de Alertas Visuales en Grid Stellar"
    
    Dim mRs As ADODB.Recordset, TempReg As Long
    
    Set mRs = Ent.BDD.Execute( _
    "SELECT c_Proceso, c_Descripcion " & _
    "FROM MA_ALERTAGRID_PROCESOS ", TempReg)
    
    Debug.Print TempReg
    
    Dim I As Long
    
    CmbProceso.Clear
    
    While Not mRs.EOF
        
        CmbProceso.AddItem mRs!c_Descripcion, I
        CmbProceso.ItemData(CmbProceso.NewIndex) = SVal(mRs!c_Proceso)
        
        mRs.MoveNext
        
    Wend
    
    If CodigoProcesoInicial <> Empty Then
        ConseguirIndex SVal(CodigoProcesoInicial) 'If ConseguirIndex.... Then CmbProceso_Click NO HACE FALTA
    End If
    
    Regresar.Enabled = True
    Regresar.Visible = True
    
End Sub

Private Sub CargarGrid()
    
    With GridAlertas
        
        .Rows = 1
        .Cols = 2
        .Clear
        
        .GridLines = flexGridInset
        .ColWidth(0) = 7000
        .ColWidth(1) = 4875
        .ColAlignment(0) = 1
        .ColAlignment(1) = 1
        .TextMatrix(0, 0) = "Descripci�n del Evento de Alerta"
        .TextMatrix(0, 1) = "Ejemplo Texto de Otras Columnas"
        .Col = 0
        .CellFontName = "Tahoma"
        .CellFontSize = 10
        .CellFontBold = True
        .CellForeColor = &H80000002
        '.CellBackColor = Gray
        .Col = 1
        .CellFontName = "Tahoma"
        .CellFontSize = 10
        .CellFontBold = True
        .CellForeColor = &H80000002
        .RowHeight(0) = 400
        
        .WordWrap = True
        .AllowUserResizing = flexResizeBoth
        
    End With
    
End Sub

Private Function ConseguirIndex(ByVal Codigo As String) As Boolean
    
    For I = 0 To CmbProceso.ListCount - 1
        
        If CLng(SVal(Codigo)) = CmbProceso.ItemData(I) Then
            
            ConseguirIndex = True
            CmbProceso.ListIndex = I
            
            Exit Function
            
        End If
        
    Next I
    
End Function

Private Sub Regresar_Click()
    Unload Me
End Sub
