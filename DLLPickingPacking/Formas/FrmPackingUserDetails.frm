VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmPackingUserDetails 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FramePickAll 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      ForeColor       =   &H0000C000&
      Height          =   855
      Left            =   13560
      TabIndex        =   13
      Top             =   9780
      Width           =   1635
      Begin VB.Label CmdPickAll 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "   Empacar   Todo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   555
         Left            =   0
         TabIndex        =   14
         Top             =   165
         Width           =   1635
      End
   End
   Begin VB.CommandButton Cmd_Listo 
      Caption         =   "Finalizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   12360
      Picture         =   "FrmPackingUserDetails.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   9780
      Width           =   1095
   End
   Begin VB.Frame buscar 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   " C�digo a Buscar: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   915
      Left            =   6360
      TabIndex        =   10
      Top             =   9720
      Width           =   4605
      Begin VB.TextBox cod_buscar 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   240
         MaxLength       =   15
         TabIndex        =   11
         Top             =   240
         Width           =   4125
      End
   End
   Begin VB.CommandButton ButtonLeyenda 
      Caption         =   "Leyenda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   11100
      Picture         =   "FrmPackingUserDetails.frx":1D82
      Style           =   1  'Graphical
      TabIndex        =   9
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9780
      Width           =   1095
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   19335
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12480
         TabIndex        =   5
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14520
         Picture         =   "FrmPackingUserDetails.frx":2A4C
         Top             =   0
         Width           =   480
      End
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Pedido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   120
         Width           =   8895
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   8850
      LargeChange     =   10
      Left            =   14480
      TabIndex        =   1
      Top             =   600
      Width           =   674
   End
   Begin VB.CheckBox Chk_NombreProducto 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Art�culo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   285
      Left            =   1080
      TabIndex        =   0
      Top             =   10200
      Value           =   1  'Checked
      Width           =   1785
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   8865
      Left            =   160
      TabIndex        =   2
      Top             =   600
      Width           =   15000
      _ExtentX        =   26458
      _ExtentY        =   15637
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox Chk_Lote 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Cantidad Solicitada"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   285
      Left            =   3240
      TabIndex        =   8
      Top             =   10200
      Width           =   2505
   End
   Begin VB.Label C�digo 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Criterios de Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Index           =   71
      Left            =   840
      TabIndex        =   6
      Top             =   9840
      Width           =   2805
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00AE5B00&
      X1              =   3720
      X2              =   5750
      Y1              =   9990
      Y2              =   9990
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   855
      Index           =   1
      Left            =   480
      TabIndex        =   7
      Top             =   9720
      Width           =   5655
   End
End
Attribute VB_Name = "FrmPackingUserDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Pedido As String
Public Lote As String

Private OrderBy As String
Private Salir As Variant
Private CambioCantidad As Boolean

Public PedidoNDE As String
Public PedidoWeb As String
Public EmpaquesMobile As Long
Public CodEmpacadorMobile As String

Public ModoGestorPreautorizacion        As Long
Public FormaDeTrabajo_Precios           As Long

Private FormaCargada As Boolean
Private CheckAll As Boolean

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        'If ExisteTabla("MA_VENTAS_PREAUTORIZACION_WEB", Ent.BDD) Then
            
            ModoGestorPreautorizacion = Val(BuscarReglaNegocioStr("ADM_WEB_ModoGestorPreautorizacion"))
            
        'End If
        
        FormaDeTrabajo_Precios = CLng(Val(BuscarReglaNegocioStr("ADM_WEB_FormaDeTrabajo_Precios", "0")))
        
        TR_INV_n_FactorMonedaProd = ExisteCampoTabla(Empty, , _
        "SELECT n_FactorMonedaProd FROM TR_INVENTARIO WHERE 1 = 2", , Ent.BDD)
        
        Moneda_Dec = FrmAppLink.DecMonedaPref 'BuscarValorBD("Cant_Decimales", _
        "SELECT Cant_Decimales FROM MA_MONEDAS WHERE b_Preferencia = 1", "2", Ent.BDD)
        
    End If
    
    If Salir Then
        
        Mensaje True, "No hay productos para empacar."
        
        Unload Me
        
        Exit Sub
        
    End If
    
    If gRutinas.PuedeObtenerFoco(Cod_Buscar) Then Cod_Buscar.SetFocus
    
End Sub

Private Sub Form_Load()
    
    AjustarPantalla Me
    
    OrderBy = "ORDER BY MA_PRODUCTOS.c_Descri"
    Salir = False
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionByRow
        
        .Rows = 2
        
        .FixedCols = 0
        .FixedRows = 1
        
        .Rows = 1
        .Cols = 7
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        .FormatString = "Articulo" & "|" & "Nombre del Producto" & "|" & _
        "Cant. Sol" & "|" & "Cant. Rec" & "|" & "Cant. Emp" & "|" & "Cant. Dec"
        
        .ColWidth(0) = 2500
        .ColAlignment(0) = flexAlignCenterCenter
        
        .ColWidth(1) = 8000
        .ColAlignment(1) = flexAlignLeftCenter
        
        .ColWidth(2) = 1500
        .ColAlignment(2) = flexAlignCenterCenter
        
        .ColWidth(3) = 1500
        .ColAlignment(3) = flexAlignCenterCenter
        
        .ColWidth(4) = 1500
        .ColAlignment(4) = flexAlignCenterCenter
        
        .ColWidth(5) = 0
        
        .ColWidth(6) = 0
        
        .Width = 15000
        
        .ScrollTrack = True
        
        .Row = 0
        .Col = 6
        .ColSel = 5
        
    End With
    
    For I = 0 To Grid.Cols - 1
        Grid.Col = I
        Grid.CellAlignment = flexAlignCenterCenter
    Next
    
End Sub

Private Sub PrepararDatos()
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset, I As Integer
    
'    SQL = "select MA_PEDIDOS_RUTA_PICKING.CodProducto, MA_PRODUCTOS.C_DESCRI as Nombre, " & vbNewLine & _
'    "MA_PEDIDOS_RUTA_PICKING.CantSolicitada, MA_PEDIDOS_RUTA_PICKING.CantEmpacada, " & vbNewLine & _
'    "MA_PEDIDOS_RUTA_PICKING.CantRecolectada, isNull(MA_Codigos.C_Codigo,MA_PRODUCTOS.C_Codigo) " & vbNewLine & _
'    "as CodEDI from MA_PEDIDOS_RUTA_PICKING inner join MA_PRODUCTOS on MA_PRODUCTOS.C_CODIGO " & vbNewLine & _
'    "= MA_PEDIDOS_RUTA_PICKING.CodProducto inner join MA_UBICACIONxPRODUCTO on " & vbNewLine & _
'    "MA_UBICACIONxPRODUCTO.CU_PRODUCTO = MA_PEDIDOS_RUTA_PICKING.CodProducto " & vbNewLine & _
'    "left join MA_Codigos on MA_Codigos.c_codnasa = MA_PRODUCTOS.C_Codigo and " & vbNewLine & _
'    "MA_Codigos.nu_intercambio = 1 where MA_PEDIDOS_RUTA_PICKING.CodEmpacador = '" & lccodusuario & "' and " & vbNewLine & _
'    "MA_PEDIDOS_RUTA_PICKING.CodLote = '" & Lote & "' and MA_PEDIDOS_RUTA_PICKING.Picking = 1 " & vbNewLine & _
'    "and MA_PEDIDOS_RUTA_PICKING.CodPedido = '" & Pedido & "' and MA_PEDIDOS_RUTA_PICKING.CantRecolectada > 0" & vbNewLine & _
'    OrderBy
    
    SQL = "SELECT MA_PEDIDOS_RUTA_PICKING.CodProducto, MA_PRODUCTOS.C_DESCRI as Nombre, " & vbNewLine & _
    "MA_PEDIDOS_RUTA_PICKING.CantSolicitada, MA_PEDIDOS_RUTA_PICKING.CantEmpacada, " & vbNewLine & _
    "MA_PEDIDOS_RUTA_PICKING.CantRecolectada, isNull(MA_Codigos.C_Codigo, MA_PRODUCTOS.C_Codigo) " & vbNewLine & _
    "AS CodEDI, MA_PRODUCTOS.Cant_Decimales AS CantDecimales FROM " & vbNewLine & _
    "MA_PEDIDOS_RUTA_PICKING INNER JOIN MA_PRODUCTOS ON MA_PRODUCTOS.C_CODIGO " & vbNewLine & _
    "= MA_PEDIDOS_RUTA_PICKING.CodProducto  " & vbNewLine & _
    "LEFT JOIN MA_Codigos on MA_Codigos.c_codnasa = MA_PRODUCTOS.C_Codigo and " & vbNewLine & _
    "MA_Codigos.nu_Intercambio = 1 WHERE MA_PEDIDOS_RUTA_PICKING.CodEmpacador = '" & _
    IIf(FrmPackingUser.PackingMobile, CodEmpacadorMobile, LcCodUsuario) & "' AND " & vbNewLine & _
    "MA_PEDIDOS_RUTA_PICKING.CodLote = '" & Lote & "' AND MA_PEDIDOS_RUTA_PICKING.Picking = 1 " & vbNewLine & _
    "AND MA_PEDIDOS_RUTA_PICKING.CodPedido = '" & Pedido & "'-- AND MA_PEDIDOS_RUTA_PICKING.CantRecolectada > 0" & vbNewLine & _
    OrderBy
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        Salir = True
    End If
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    Grid.Visible = False
    
    While Not Rs.EOF
        
        Grid.Rows = Grid.Rows + 1
        
        Grid.TextMatrix(Grid.Rows - 1, 0) = Rs!CodEDI
        Grid.TextMatrix(Grid.Rows - 1, 1) = Rs!Nombre
        Grid.TextMatrix(Grid.Rows - 1, 2) = FormatNumber(Rs!CantSolicitada, Rs!CantDecimales, vbTrue, vbFalse, True)
        Grid.TextMatrix(Grid.Rows - 1, 3) = FormatNumber(Rs!CantRecolectada, Rs!CantDecimales, vbTrue, vbFalse, True)
        Grid.TextMatrix(Grid.Rows - 1, 4) = FormatNumber(Rs!CantEmpacada, Rs!CantDecimales, vbTrue, vbFalse, True)
        Grid.TextMatrix(Grid.Rows - 1, 5) = Rs!CodProducto
        Grid.TextMatrix(Grid.Rows - 1, 6) = Rs!CantDecimales
        
        If Rs!CantEmpacada > 0 Then
            If Rs!CantRecolectada = Rs!CantEmpacada Then
                For I = 0 To Grid.Cols - 1
                    Grid.Row = Grid.Rows - 1
                    Grid.Col = I
                    Grid.CellBackColor = &HB4EDB9    ' Verde.
                Next I
            Else
                For I = 0 To Grid.Cols - 1
                    Grid.Row = Grid.Rows - 1
                    Grid.Col = I
                    Grid.CellBackColor = -2147483624   ' Amarillo.
                Next I
            End If
        End If
        
        Rs.MoveNext
        
    Wend
    
    Rs.Close
    
    If Grid.Rows > 14 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(1) = 8000 - 675
    Else
        Grid.ScrollBars = flexScrollBarNone
        ScrollGrid.Visible = False
    End If
        
Finally:
    
    Grid.Visible = True
    
    LabelTitulo.Caption = "Detalles del Pedido  " & Pedido
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub Grid_DblClick()
    
    On Error GoTo Error1
    
    Dim ActiveTrans As Boolean
    
    If BuscarReglaNegocioBoolean("Packing_DesglosarProductoPadre", 1) Then
        DesglosarProductoPadre = True
    Else
        DesglosarProductoPadre = False
    End If
    
    ProductoNuevo = False
    
    If Grid.TextMatrix(Grid.RowSel, 5) <> Empty Then
        
        Dim Resultado As Variant, NewCant As Double
        Dim CantSolicitada As Double, CantRecolectada As Double, _
        CantEmpacada As Double, CantDecimalesProducto As Long
        
        Retorno = True
        
        CantSolicitada = CDbl(Grid.TextMatrix(Grid.RowSel, 2))
        CantRecolectada = CDbl(Grid.TextMatrix(Grid.RowSel, 3))
        CantEmpacada = CDbl(Grid.TextMatrix(Grid.RowSel, 4))
        CantDecimalesProducto = SVal(Grid.TextMatrix(Grid.RowSel, 6))
        
        If CantRecolectada = CantEmpacada Then
            If CheckAll Then
                Retorno = True
            Else
                Retorno = Mensaje(False, "Ha empacado la cantidad recolectada " & _
                "para este producto �Seguro que desea cambiarla?")
            End If
        End If
        
        If Retorno Then
            
            If CheckAll Then
                NewCant = CantRecolectada
            Else
                FrmNumPadGeneral.ValorOriginal = CantEmpacada
                FrmNumPadGeneral.Show vbModal
                NewCant = Round(FrmNumPadGeneral.ValorNumerico, CantDecimalesProducto)
                Set FrmNumPadGeneral = Nothing
            End If
            
            If NewCant >= 0 Then
                
                Dim SQL As String, Rs As New ADODB.Recordset
                
                'DesglosarProductoPadre = True ' Debug
                
                If DesglosarProductoPadre = True Then
                    
                    Dim CodProducto As String
                    
                    Dim cCodPlan As String
                    Dim cCodProExt As String
                    
                    Dim RsProductos As New ADODB.Recordset
                    
                    CodProducto = Grid.TextMatrix(Grid.RowSel, 5)
                    
                    Call Apertura_RecordsetC(RsProductos)
                    
                    RsProductos.Open _
                    "SELECT * FROM MA_PRODUCTOS " & _
                    "WHERE c_Codigo = '" & FixTSQL(CodProducto) & "' ", _
                    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                    
                    Set RsProductos.ActiveConnection = Nothing
                    
                    cCodPlan = RsProductos!c_Cod_Plantilla
                    cCodProExt = RsProductos!c_Codigo
                    
                    If Trim(cCodPlan) <> Empty Then
                        
                        If NewCant = 0 Then Exit Sub
                        
                        Dim Query As String
                        Dim RsTemp As ADODB.Recordset
                        
                        Dim Deposito As String
                        Dim Localidad As String
                        Dim CodCliente As String
                        
                        Query = "SELECT c_CodDeposito AS Deposito, c_CodLocalidad AS Localidad, " & _
                        "c_CodCliente AS Cliente " & _
                        "FROM MA_VENTAS WITH (NOLOCK) " & _
                        "WHERE c_Documento = '" & Pedido & "' " & _
                        "AND c_Concepto = 'PED' "
                        
                        Set RsTemp = New ADODB.Recordset
                        
                        RsTemp.Open Query, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                        
                        Deposito = RsTemp!Deposito
                        'Localidad = Rs!Localidad
                        'CodCliente = Rs!Cliente
                        
                        RsTemp.Close
                        
                        FrmExtendidasProductos.CodPlantilla = cCodPlan
                        FrmExtendidasProductos.CodProducto = cCodProExt
                        'FrmExtendidasProductos.TipoPrecio = 3
                        FrmExtendidasProductos.CodDeposito = Deposito
                        FrmExtendidasProductos.Forma = "Packing"
                        
                        FrmExtendidasProductos.Show vbModal
                        
                        If Trim(FrmExtendidasProductos.Codigo) <> Empty Then
                            
                            Dim RsTrVentas As New ADODB.Recordset
                            Dim SqlTrVen As String
                            
                            Call Apertura_RecordsetC(RsTrVentas)
                            
                            SqlTrVen = "SELECT * FROM TR_VENTAS " & _
                            "WHERE c_Documento = '" & Pedido & "' " & _
                            "AND c_CodArticulo = '" & CodProducto & "' " & _
                            "AND c_Concepto = 'PED' "
                            
                            RsTrVentas.Open SqlTrVen, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                            
                            RsTrVentas.ActiveConnection = Nothing
                            
                            Ent.BDD.BeginTrans
                            ActiveTrans = True
                            
                            Dim RsTrVentasNew As New ADODB.Recordset
                            
                            RsTrVentasNew.Open "SELECT * FROM TR_VENTAS WHERE 1 = 2 ", _
                            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                            
                            RsTrVentasNew.AddNew
                            
                            RsTrVentasNew!c_Documento = RsTrVentas!c_Documento
                            RsTrVentasNew!c_Concepto = RsTrVentas!c_Concepto
                            RsTrVentasNew!c_CodArticulo = FrmExtendidasProductos.Codigo
                            RsTrVentasNew!n_Cantidad = NewCant
                            RsTrVentasNew!n_Precio = RsTrVentas!n_Precio
                            RsTrVentasNew!n_Subtotal = RsTrVentas!n_Subtotal
                            RsTrVentasNew!n_Impuesto = RsTrVentas!n_Impuesto
                            RsTrVentasNew!n_Total = RsTrVentas!n_Total
                            RsTrVentasNew!n_Cant_Vendida = NewCant
                            RsTrVentasNew!n_Precio_Original = RsTrVentas!n_Precio_Original
                            'RsTrVentasNew!ID
                            RsTrVentasNew!c_Descripcion = RsTrVentas!c_Descripcion
                            RsTrVentasNew!c_Compuesto = RsTrVentas!c_Compuesto
                            RsTrVentasNew!CodConcepto = RsTrVentas!CodConcepto
                            RsTrVentasNew!n_DescuentoGeneral = RsTrVentas!n_DescuentoGeneral
                            RsTrVentasNew!n_DescuentoEspecifico = RsTrVentas!n_DescuentoEspecifico
                            RsTrVentasNew!cs_CodLocalidad = RsTrVentas!cs_CodLocalidad
                            
                            RsTrVentasNew.UpdateBatch
                            
                            RsTrVentasNew.Close
                            
                            Dim RsPedRutaPick As New ADODB.Recordset
                            Dim Sql2 As String
                            
                            Sql2 = "SELECT * FROM MA_PEDIDOS_RUTA_PICKING " & vbNewLine & _
                            "WHERE CodLote = '" & Lote & "' " & _
                            "AND CodPedido = '" & Pedido & "' " & _
                            "AND CodProducto = '" & FixTSQL(Grid.TextMatrix(Grid.RowSel, 5)) & "' "
                            
                            Call Apertura_RecordsetC(RsPedRutaPick)
                            
                            RsPedRutaPick.Open Sql2, Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
                            
                            RsPedRutaPick.ActiveConnection = Nothing
                            
                            Dim RsPedRutaPickNew As New ADODB.Recordset
                            
                            RsPedRutaPickNew.Open "SELECT * FROM MA_PEDIDOS_RUTA_PICKING WHERE 1 = 2 ", _
                            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                            
                            RsPedRutaPickNew.AddNew
                            
                            RsPedRutaPickNew!CodLote = RsPedRutaPick!CodLote
                            RsPedRutaPickNew!CodPedido = RsPedRutaPick!CodPedido
                            RsPedRutaPickNew!CodProducto = FrmExtendidasProductos.Codigo
                            RsPedRutaPickNew!CodRecolector = RsPedRutaPick!CodRecolector
                            RsPedRutaPickNew!CodSupervisorPicking = RsPedRutaPick!CodSupervisorPicking
                            RsPedRutaPickNew!CodEmpacador = RsPedRutaPick!CodEmpacador
                            RsPedRutaPickNew!CodSupervisorPacking = RsPedRutaPick!CodSupervisorPacking
                            RsPedRutaPickNew!CantSolicitada = NewCant
                            RsPedRutaPickNew!CantRecolectada = NewCant
                            RsPedRutaPickNew!CantEmpacada = NewCant
                            RsPedRutaPickNew!Picking = RsPedRutaPick!Picking
                            RsPedRutaPickNew!Packing = RsPedRutaPick!Packing
                            RsPedRutaPickNew!FechaAsignacion = RsPedRutaPick!FechaAsignacion
                            RsPedRutaPickNew!PackingMobile_CantEmpaques = RsPedRutaPick!PackingMobile_CantEmpaques
                            
                            RsPedRutaPickNew.UpdateBatch
                            RsPedRutaPickNew.Close
                            
                            ProductoNuevo = True
                            
                        Else
                            
                            Exit Sub
                            
                        End If
                        
                    Else
                        
                        GoTo ProductoNormal1
                        
                    End If
                    
                Else
                    
ProductoNormal1:
                    
                    SQL = "UPDATE MA_PEDIDOS_RUTA_PICKING SET " & _
                    "CantEmpacada = " & NewCant & " " & vbNewLine & _
                    "WHERE CodLote = '" & Lote & "' " & _
                    "AND CodPedido = '" & Pedido & "' " & _
                    "AND CodProducto = '" & Grid.TextMatrix(Grid.RowSel, 5) & "' "
                    
                End If
                
                If CantEmpacada > NewCant Then
                    
                    Retorno = True
                    
                    If Not CheckAll Then
                        Retorno = Mensaje(False, "La cantidad actual de productos empacados es de " & _
                        CantEmpacada & " �Seguro que desea disminuirla a " & NewCant & "?")
                    End If
                    
                    If Retorno Then
                        
                        If Not ProductoNuevo Then
                            Ent.BDD.Execute SQL
                        Else
                            Ent.BDD.CommitTrans
                            ActiveTrans = False
                        End If
                        
                        CambioCantidad = True
                        
                        If Not CheckAll Then
                            Call PrepararGrid
                            Call PrepararDatos
                        End If
                        
                    End If
                    
                ElseIf CantRecolectada = NewCant Then
                    
                    If Not ProductoNuevo Then
                        Ent.BDD.Execute SQL
                    Else
                        Ent.BDD.CommitTrans
                        ActiveTrans = False
                    End If
                    
                    CambioCantidad = True
                    
                    If Not CheckAll Then
                        Call PrepararGrid
                        Call PrepararDatos
                    End If
                    
                ElseIf CantEmpacada < NewCant Then
                    
                    If NewCant > CantRecolectada Then
                        Mensaje True, "La cantidad empacada no puede ser mayor a la cantidad recolectada."
                    Else
                        
                        If Not ProductoNuevo Then
                            Ent.BDD.Execute SQL
                        Else
                            Ent.BDD.CommitTrans
                            ActiveTrans = False
                        End If
                        
                        CambioCantidad = True
                        
                        If Not CheckAll Then
                            Call PrepararGrid
                            Call PrepararDatos
                        End If
                        
                    End If
                    
                End If
            
            End If
            
        End If
        
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If ActiveTrans Then
        Ent.BDD.RollbackTrans
        ActiveTrans = False
    End If
    
    If Not CheckAll Then
        
        Mensaje True, "Error al actualizar cantidad empacada, " & _
        "Informaci�n: " & mErrorDesc & " (" & mErrorNumber & ")."
        
        Call PrepararGrid
        Call PrepararDatos
        
    End If
    
End Sub

Private Sub FramePickAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    CmdPickAll_MouseUp Button, Shift, X, Y
End Sub

Private Sub CmdPickAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_KeyDown vbKeyF6, 0
End Sub

Private Sub RestaurarComprometidos(pCn As ADODB.Connection, _
ByVal pPedido As String, ByVal pDeposito As String)
    
    ' Update en n_Cant_Comprometida y n_Cantidad disminuyendo la cantidad anulada.
    
    Dim SQL As String, HowManyAffected
    
    SQL = "UPDATE MA_DEPOPROD SET " & GetLines & _
    "n_Cant_Comprometida = (INV.n_Cant_Comprometida - Restante) " & GetLines & _
    "FROM MA_DEPOPROD INV " & _
    "INNER JOIN ( " & GetLines & _
    "SELECT c_CodArticulo AS ArticuloPedido, SUM(n_Cant_Vendida) AS Restante " & _
    "FROM TR_VENTAS " & GetLines & _
    "WHERE c_Documento = '" & pPedido & "' " & _
    "AND c_Concepto = 'PED' " & _
    "GROUP BY c_CodArticulo " & GetLines & _
    ") PED " & GetLines & _
    "ON INV.c_CodArticulo = PED.ArticuloPedido " & GetLines & _
    "AND INV.c_CodDeposito = '" & pDeposito & "' " & GetLines & _
    "AND Restante > 0 "
    
    pCn.Execute SQL, HowManyAffected
    
    SQL = "UPDATE TR_VENTAS WITH (ROWLOCK) SET " & _
    "n_Cant_Vendida = 0 " & GetLines & _
    "WHERE c_Documento = '" & pPedido & "' " & _
    "AND c_Concepto = 'PED' "
    
    pCn.Execute SQL, HowManyAffected
    
End Sub

Private Function AnularPedido(pCn As ADODB.Connection, _
ByVal pLote As String, ByVal pPedido As String, _
Optional ByVal pTransInterna As Boolean = True) As Boolean
    
    On Error GoTo Error
    
    Dim SQL As String
    Dim Rs As ADODB.Recordset
    
    Dim Deposito As String
    Dim Localidad As String
    Dim CodCliente As String
    
    If pTransInterna Then
        pCn.BeginTrans
        Trans = True
    End If
    
    SQL = "SELECT c_CodDeposito AS Deposito, c_CodLocalidad AS Localidad, " & _
    "c_CodCliente AS Cliente " & _
    "FROM MA_VENTAS WITH (NOLOCK) " & _
    "WHERE c_Documento = '" & pPedido & "' " & _
    "AND c_Concepto = 'PED' "
    
    Set Rs = New ADODB.Recordset
    
    Rs.Open SQL, pCn, adOpenForwardOnly, adLockReadOnly
    
    Deposito = Rs!Deposito
    Localidad = Rs!Localidad
    CodCliente = Rs!Cliente
    
    Rs.Close
    
    ' Finalizar el Packing del pedido.
    
    SQL = "UPDATE MA_PEDIDOS_RUTA_PICKING WITH (ROWLOCK) SET " & _
    "Packing = 1 " & vbNewLine & _
    "WHERE CodLote = '" & pLote & "' " & _
    "AND CodPedido = '" & pPedido & "' "
    
    pCn.Execute SQL
    
    RestaurarComprometidos pCn, pPedido, Deposito
    
    'Colocar el pedido con status = ANU
    
    SQL = "UPDATE MA_VENTAS WITH (ROWLOCK) SET " & _
    "c_Status = 'ANU' " & GetLines & _
    "WHERE c_Documento = '" & pPedido & "' " & _
    "AND c_Concepto = 'PED' "
    
    pCn.Execute SQL
    
    If pTransInterna Then
        pCn.CommitTrans
        Trans = False
    End If
    
    AnularPedido = True
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If pTransInterna Then
        If Trans Then
            pCn.RollbackTrans
            Trans = False
        End If
    End If
    
    MsjErrorRapido mErrorDesc & " (" & mErrorNumber & ")", _
    "Ocurri� un error al anular el pedido, reporte lo siguiente: "
    
End Function

Private Function PrevenirDuplicados(pCn As Object) As Boolean
    
    Dim SQL As String, ExisteDcto As Boolean, ExistePacking As Boolean
    
    SQL = "SELECT TOP 1 c_Documento " & vbNewLine & _
    "FROM MA_VENTAS " & vbNewLine & _
    "WHERE c_Concepto = 'NDE' " & vbNewLine & _
    "AND c_Relacion = 'PED " & Pedido & "' " & vbNewLine
    
    ExisteDcto = (UCase(BuscarValorBD("c_Documento", SQL, "NULL", pCn)) <> UCase("NULL"))
    
    SQL = "SELECT TOP 1 CodPedido " & vbNewLine & _
    "FROM MA_PEDIDOS_RUTA_PACKING " & vbNewLine & _
    "WHERE CodLote = '" & Lote & "' " & vbNewLine & _
    "AND CodPedido = '" & Pedido & "' " & vbNewLine
    
    ExistePacking = (UCase(BuscarValorBD("CodPedido", SQL, "NULL", pCn)) <> UCase("NULL"))
    
    PrevenirDuplicados = (ExisteDcto And ExistePacking)
    
    ' Si las 2 condiciones se cumplen entonces hay que _
    evitar grabar pues se crear�a un duplicado.
    
End Function

' Empacar Pedido

Private Sub Cmd_Listo_Click()
    
    On Error GoTo Error1
    
    Dim Trans As Boolean, SumCantRec As Double, SumCantEmp As Double, Anular As Boolean
    Dim I As Integer, Resultado As Variant, CantEmpaques As Long
    
    Dim mCampoMonedaProd As String, mFactorMonedaProd As Variant, mCodMonedaProd As String
    Dim CostoUsar As Double, PrecioUsar As Double, TmpPrecioDoc As Double
    
    Dim mMantenerMonedaOriginal As Boolean
    Dim mMantenerTasaOrigen As Boolean
    
    'aca buscar la regla de negocio para mantener el documento en moneda original
    mMantenerMonedaOriginal = BuscarReglaNegocioBoolean("Packing_MantenerMonedaOrigen", 1)
    mMantenerTasaOrigen = BuscarReglaNegocioBoolean("Packing_MantenerTasaOrigen", 1)
    
    If PrevenirDuplicados(Ent.BDD) Then
        Mensaje True, "Atenci�n, este pedido ya ha sido empacado, probablemente " & _
        "desde otro equipo. Verifique con los otros operadores por que ya la nota " & _
        "de entrega ha sido generada."
        Exit Sub
    End If
    
    Resultado = False
    
    'Verificar Que todos los articulos recolectados sean iguales a los empacados.
    
    For I = 1 To Grid.Rows - 1
        
        If CDbl(Grid.TextMatrix(I, 3)) <> CDbl(Grid.TextMatrix(I, 4)) Then
            Resultado = True
        End If
        
        SumCantRec = SumCantRec + CDbl(Grid.TextMatrix(I, 3))
        SumCantEmp = SumCantEmp + CDbl(Grid.TextMatrix(I, 4))
        
    Next I
    
    If Resultado And IIf(FrmPackingUser.PackingMobile, CambioCantidad, True) Then
        Resultado = Mensaje(False, _
        "�Est� Seguro que desea finalizar el empacado con productos faltantes?")
    Else
        Resultado = True
    End If
    
    If Not Resultado Then Exit Sub
    
    If SumCantRec = 0 Or SumCantEmp = 0 Then
        
        Mensaje False, "No se ha establecido ninguna cantidad para los productos, " & _
        "en ese caso el pedido ser� anulado, dicha acci�n no se podr� reversar. " & _
        "�Est� realmente seguro de anular el pedido? "
        
        If Not Retorno Then
            Exit Sub
        End If
        
        Anular = True
        
    End If
    
    Dim Rs As New ADODB.Recordset, SQL As String, Deposito As String, Localidad As String
    
    If Anular Then
        
        If AnularPedido(Ent.BDD, Lote, Pedido) Then
            Unload Me
        End If
        
        Exit Sub
        
    End If
    
    If Resultado Then
        
        If Not FrmPackingUser.PackingMobile Then
            Mensaje True, "Ingrese la cantidad de empaques usados para este pedido."
            Retorno = False
        Else
            Mensaje False, "El empacador indic� " & EmpaquesMobile & " empaques para este pedido. " & _
            "Si es correcto presione aceptar, o cancelar para cambiar la cantidad de empaques."
        End If
        
        If Not Retorno Then
            
            FrmNumPadGeneral.ValorOriginal = 1
            FrmNumPadGeneral.Show vbModal
            CantEmpaques = FrmNumPadGeneral.ValorNumerico
            Set FrmNumPadGeneral = Nothing
            
        Else
            CantEmpaques = EmpaquesMobile
        End If
        
        Retorno = False
        
        If CantEmpaques > 0 Then
            
            Uno = False
            
            FrmMensajeAlerta.Mensaje.Text = "La cantidad de empaques indicada para este " & _
            "pedido es: " & CantEmpaques & ", por favor confirme la finalizaci�n " & _
            "del proceso de empacado para este pedido."
            
            FrmMensajeAlerta.Show vbModal
            
            frm_Mensajeria.aceptar.Enabled = False
            
            Set frm_Mensajeria = Nothing
            
        End If
        
        If Retorno Then
            
            Dim Cont As Long
            
            CampoC = Ent.BDD.Execute( _
            "SELECT Estimacion_INV " & _
            "FROM REGLAS_COMERCIALES")!Estimacion_Inv
            
            Select Case CampoC
                Case 0
TipoCostoDefault:
                    CampoC = "n_CostoAct"
                Case 1
                    CampoC = "n_CostoAnt"
                Case 2
                    CampoC = "n_CostoPro"
                'Case 3
                    'CampoC = "n_CostoRep"
                Case Else
                    GoTo TipoCostoDefault
            End Select
            
            Ent.BDD.BeginTrans
            Trans = True
            
            Dim mCampoIGTF As String, mMontoIGTF_Original As Double, mMontoIGTF_Final As Double
            Dim mBaseIGTF As Double, mPorcIGTF As Double
            
            If ManejaIGTF_Ventas Then
                mCampoIGTF = ", n_Imp_IGTF, n_Base_IGTF"
            End If
            
            SQL = "SELECT c_CodDeposito AS Deposito, c_CodLocalidad AS Localidad, " & _
            "c_CodCliente AS Cliente, c_Rif AS Rif, " & _
            "c_CodMoneda, n_FactorCambio" & mCampoIGTF & " " & _
            "FROM MA_VENTAS WITH (NOLOCK) " & _
            "WHERE c_Documento = '" & Pedido & "' " & _
            "AND c_Concepto = 'PED' "
            
            Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
            
            Deposito = Rs!Deposito
            Localidad = Rs!Localidad
            CodCliente = Rs!Cliente
            RifCliente = Rs!Rif
            CodMonedaDoc = Rs!c_CodMoneda
            
            'posiblemente buscar aca el factor de la cabecera
            Set MonedaTmp = FrmAppLink.ClaseMonedaTemp
            
            If Not MonedaTmp.BuscarMonedas(, CodMonedaDoc) Then
                FrmAppLink.ValidarConexion Ent.BDD, , True
                If Not MonedaTmp.BuscarMonedas(, CodMonedaDoc) Then
                    Err.Raise 999, , "No se pudo cargar datos de la moneda del documento. "
                End If
            End If
            
            Dim FacMonedaDoc As Double
            Dim DecMonedaDoc As Integer
            
            If mMantenerMonedaOriginal Then
                If Not mMantenerTasaOrigen Then
                    FacMonedaDoc = MonedaTmp.FacMoneda
                Else
                    FacMonedaDoc = Rs!n_FactorCambio
                End If
                
                DecMonedaDoc = MonedaTmp.DecMoneda
            Else
                FacMonedaDoc = 1
                DecMonedaDoc = FrmAppLink.DecMonedaPref
            End If
            
            If ManejaIGTF_Ventas _
            And ExisteCampoTabla("n_Imp_IGTF", Rs) Then
                
                mMontoIGTF_Original = Rs!n_Imp_IGTF
                
                If mMontoIGTF_Original > 0 Then
                    
                    If Rs!n_Base_IGTF > 0 Then
                        
                        mBaseIGTF = Rs!n_Base_IGTF
                        
                    Else
                        
                        'isNULL(SUM((DP.n_Monto - DP.n_Vuelto) * DP.n_Factor), 0) AS TotalBaseIGTF
                        'isNULL(SUM(DP.n_Base_IGTF * DP.n_Factor), 0) AS TotalBaseIGTF
                        
                        mBaseIGTF = CDec(BuscarValorBD("TotalBaseIGTF", _
                        "SELECT isNULL(SUM(DP.n_Base_IGTF * DP.n_Factor), 0) AS TotalBaseIGTF " & _
                        "FROM MA_VENTAS_WEB_DETPAG VDP " & _
                        "INNER JOIN MA_CXC CXC " & _
                        "ON VDP.CorrelativoAnticipoCxC = CXC.c_Documento " & _
                        "AND CXC.c_Codigo = '" & CodCliente & "' " & _
                        "AND CXC.c_Concepto = 'A_P' " & _
                        "INNER JOIN MA_CXC_DETPAG DP " & _
                        "ON CXC.c_Concepto = DP.c_Concepto " & _
                        "AND CXC.c_Documento = DP.c_Documento " & _
                        "WHERE VDP.Code = '" & Pedido & "' " & _
                        "AND LTRIM(RTRIM(VDP.CorrelativoAnticipoCxC)) <> '' " & _
                        "AND DP.n_Porc_IGTF > 0 " & _
                        "AND DP.n_Imp_IGTF > 0 ", 0, Ent.BDD)) / CDec(FacMonedaDoc)
                        
                    End If
                    
                End If
                
                If mBaseIGTF <> 0 Then
                    mPorcIGTF = Round(mMontoIGTF_Original / mBaseIGTF * 100, 2)
                Else
                    mMontoIGTF_Original = 0
                End If
                
            End If
            
            Rs.Close
            
            SQL = "DELETE FROM MA_PEDIDOS_RUTA_PACKING " & vbNewLine & _
            "WHERE CodLote = '" & Lote & "' " & _
            "AND CodPedido = '" & Pedido & "' " & vbNewLine
            
            Ent.BDD.Execute SQL
            
            'Insertar el Nuevo Pedido al Packing
            
            SQL = "INSERT MA_PEDIDOS_RUTA_PACKING " & _
            "(CodLote, CodPedido, CantEmpaques) " & GetLines & _
            "VALUES ('" & Lote & "', '" & Pedido & "', (" & CDec(CantEmpaques) & ")) "
            
            Ent.BDD.Execute SQL
            
            ' Finalizar el Packing del pedido.
            SQL = "UPDATE MA_PEDIDOS_RUTA_PICKING WITH (ROWLOCK) SET " & _
            "Packing = 1 " & vbNewLine & _
            "WHERE CodLote = '" & Lote & "' " & _
            "AND CodPedido = '" & Pedido & "' "
            
            Ent.BDD.Execute SQL
            
            PedidoNDE = Format(NO_CONSECUTIVO("nde", True), "00000000#")
            
            Cont = 1 ' Control del campo C_Linea
            
            SQL = "SELECT * " & _
            "FROM TR_VENTAS WITH (NOLOCK) " & _
            "WHERE c_Documento = '" & Pedido & "' " & _
            "AND c_Concepto = 'PED' " & _
            "ORDER BY ID "
            
            Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
            
            Dim RsProducto As Recordset
            
            Faltantes = False 'Bandera usada para saber La cantidad de productos empacados es diferente a los solicitados.
            
            While Not Rs.EOF
                
                For I = 1 To Grid.Rows - 1
                    
                    If UCase(Grid.TextMatrix(I, 5)) = UCase(Rs!c_CodArticulo) Then
                        
                        Set RsProducto = New Recordset
                        
                        If FormaDeTrabajo_Precios = 0 Then
                            
FormaDeTrabajo_Precios_Default_1:
                            
                            ' Precios autorizados en �rea de caja (habladores).
                            ' Se inici� as� para KROMI.
                            
                            RsProducto.Open _
                            "SELECT * FROM " & FrmAppLink.Srv_Remote_BD_POS & ".DBO.MA_PRODUCTOS " & _
                            "WHERE c_Codigo = '" & Rs!c_CodArticulo & "' ", _
                            Ent.POS, adOpenKeyset, adLockReadOnly, adCmdText
                            
                        ElseIf FormaDeTrabajo_Precios = 1 Then
                            
                            ' Precios en �rea administrativa. Por si alg�n d�a surge
                            ' venta web para un comercio que no maneja POS.
                            
                            RsProducto.Open _
                            "SELECT * FROM MA_PRODUCTOS " & _
                            "WHERE c_Codigo = '" & Rs!c_CodArticulo & "' ", _
                            Ent.BDD, adOpenKeyset, adLockReadOnly, adCmdText
                            
                        ElseIf FormaDeTrabajo_Precios = 2 Then
                            
                            ' Sacar detalles del producto de VAD20 o sino de VAD10.
                            ' Pero los precios e impuestos se tomar�n de los Pedidos
                            ' Para no manejar diferencias pago contra el pago web.
                            ' Caso Plazas.
                            
                            RsProducto.Open _
                            "SELECT * FROM " & FrmAppLink.Srv_Remote_BD_POS & ".DBO.MA_PRODUCTOS " & _
                            "WHERE c_Codigo = '" & Rs!c_CodArticulo & "' ", _
                            Ent.POS, adOpenKeyset, adLockReadOnly, adCmdText
                            
                            If RsProducto.EOF Then
                                
                                RsProducto.Close
                                
                                RsProducto.Open "SELECT * FROM MA_PRODUCTOS WITH (NOLOCK) " & _
                                "WHERE c_Codigo = '" & Rs!c_CodArticulo & "' ", _
                                Ent.BDD, adOpenKeyset, adLockReadOnly, adCmdText
                                
                            End If
                            
                        Else
                            GoTo FormaDeTrabajo_Precios_Default_1
                        End If
                        
                        If RsProducto.EOF Then
                            Err.Raise 999, , "El Producto [" & Rs!c_CodArticulo & "] no existe."
                        End If
                        
                        mCodMonedaProd = RsProducto!c_CodMoneda
                        
                        Set MonedaProd = FrmAppLink.ClaseMonedaProducto
                        
                        If Not MonedaProd.BuscarMonedas(, mCodMonedaProd) Then
                            FrmAppLink.ValidarConexion Ent.BDD, , True
                            If Not MonedaProd.BuscarMonedas(, mCodMonedaProd) Then
                                Err.Raise 999, , "No se pudo cargar datos de la moneda para el producto " & Rs!c_CodArticulo & ""
                            End If
                        End If
                        
                        mFactorMonedaProd = MonedaProd.FacMoneda 'FactorMonedaProducto(RsProducto!c_Codigo, Ent.BDD) 'Mejor no, Existe el riesgo de mezclar datos de ficha de VAD10 con VAD20 en la transacci�n.
                        
                        'CostoUsar = RsProducto.Fields(CampoC).Value 'CMM1O
                        If mMantenerMonedaOriginal Then
                            CostoUsar = Round(RsProducto.Fields(CampoC).Value * (mFactorMonedaProd / FacMonedaDoc), DecMonedaDoc) 'FrmAppLink.DecMonedaPref)  'CMM1N
                        Else
                            CostoUsar = Round(RsProducto.Fields(CampoC).Value * mFactorMonedaProd, DecMonedaDoc) 'FrmAppLink.DecMonedaPref) 'CMM1N
                        End If
                        
                        ' Esto es para verificar si por casualidad hubiera una oferta vigente y estuviera mas barato que en el pedido.
                        If RsProducto!n_PrecioO <> 0 _
                        And Now >= CDate(SDate(RsProducto!f_Inicial) & " " & Format(RsProducto!h_Inicial, "HH:mm:ss")) _
                        And Now <= CDate(SDate(RsProducto!f_Final) & " " & Format(RsProducto!h_Final, "HH:mm:ss")) Then
                            PrecioUsar = IIf(RsProducto.Fields("n_PrecioO").Value < RsProducto.Fields("n_Precio1").Value, _
                            RsProducto.Fields("n_PrecioO").Value, RsProducto.Fields("n_Precio1").Value)
                        Else
                            PrecioUsar = RsProducto.Fields("n_Precio1").Value
                        End If
                        
                        If mMantenerMonedaOriginal Then
                            PrecioUsar = Round(PrecioUsar * (mFactorMonedaProd / FacMonedaDoc), DecMonedaDoc) 'FrmAppLink.DecMonedaPref)    'CMM1N
                        Else
                            PrecioUsar = Round(PrecioUsar * mFactorMonedaProd, DecMonedaDoc) 'CMM1N
                        End If
                        
                        If mMantenerMonedaOriginal Then
                            TmpPrecioDoc = (Rs!n_Precio)
                        Else
                            TmpPrecioDoc = (Rs!n_Precio * FacMonedaDoc)
                        End If
                        
                        Dim Cantidad As Double, Precio As Double, _
                        Subtotal As Double, Impuesto As Double, Total As Double
                        
                        Cantidad = CDbl(Grid.TextMatrix(I, 4)) ' Cantidad Restante.
                        
                        Dim PrecioInvalido As Boolean: PrecioInvalido = False
                        Dim ImpuestoInvalido As Boolean: ImpuestoInvalido = False
                        Dim TmpImp As Double
                        Dim TuvoOferta As Boolean
                        
                        If FormaDeTrabajo_Precios = 0 Or FormaDeTrabajo_Precios = 1 Then
                            
FormaDeTrabajo_Precios_Default_2:
                            
                            If Rs!n_Subtotal <> 0 Then
                                TmpImp = Round(((Rs!n_Impuesto / Rs!n_Subtotal) * 100), 6) ' Round para evitar basura decimal
                            Else
                                TmpImp = 0
                            End If
                            
                            If Round(Abs(TmpImp - RsProducto!n_Impuesto1), 4) >= 0.01 Then
                                ImpuestoInvalido = True
                            End If
                            
                            TuvoOferta = (Rs!n_Precio_Original <> 0)
                            TuvoOferta = TuvoOferta And (Round(Rs!n_Precio_Original - Rs!n_Precio, 8) >= 0.01)
                            
                            If RsProducto!n_TipoPeso <> 4 Then
                                '20191127. A�adido evitar comparacion de precios cuando son monedas alternas, _
                                ya que las fluctuaciones de tasas al alza no permitir�an facturar los productos _
                                debido a la validaci�n. Que el mismo solo aplique a los productos solo en moneda _
                                predeterminada, que es donde se puede corregir la situaci�n actualizando el precio...
                                '20200123. A�adido tambi�n evitar esta comparaci�n cuando el producto este mas caro _
                                en la tienda debido a que el producto haya tenido alguna oferta o descuento. _
                                En este caso es obvio que se debe tomar el precio del pedido dado que para eso se le _
                                hizo una oferta en primer lugar.
                                If (UCase(mCodMonedaProd) = UCase(FrmAppLink.CodMonedaPref)) _
                                And (Not TuvoOferta) _
                                And (PrecioUsar - TmpPrecioDoc) >= 0.01 Then
                                    ' Si el precio del producto est� mas caro en la tienda que en la web.
                                    PrecioInvalido = True
                                End If
                            End If
                            
                        ElseIf FormaDeTrabajo_Precios = 2 Then
                            
                            ' No manejar diferencias de precios.
                            
                            PrecioInvalido = False
                            ImpuestoInvalido = False
                            
                            ' Si se est� trabajando con pagos de tarjeta preautorizados
                            ' No se pueden hacer facturas con precios potencialmente mayores
                            ' si llegan a estar mas caros que en la web por cambio de precios.
                            ' Por que sino no se puede completar el pago preautorizado y se tranca
                            ' el lote.
                            
                        Else
                            GoTo FormaDeTrabajo_Precios_Default_2
                        End If
                        
                        If PrecioInvalido Then
                            
                            ' Rechazar el producto por que est� mas caro que en el pedido.
                            
                            ActivarMensajeGrande 80
                            
                            Mensaje False, "Atenci�n, el producto [" & RsProducto!c_Codigo & _
                            "][" & RsProducto!c_Descri & "] posee un precio mayor en la sucursal que " & _
                            "en el pedido original. No es posible empacar el producto. " & _
                            "Presione Aceptar si desea empacar el pedido sin el producto, " & _
                            "o Cancelar para no empacar el pedido a�n. Verifique los datos del producto en las cajas." & GetLines(2) & _
                            "Precio en tienda: " & Format(PrecioUsar, "Standard") & GetLines & _
                            "Precio en pedido: " & Format(TmpPrecioDoc, "Standard")
                            
                            If Retorno Then
                                Cantidad = 0
                                Faltantes = True
                            Else
                                GoTo Error1
                            End If
                            
                        ElseIf ImpuestoInvalido Then
                            
                            ' Rechazar el producto por que est� mas caro que en el pedido.
                            
                            ActivarMensajeGrande 80
                            Mensaje False, "Atenci�n, el producto [" & RsProducto!c_Codigo & _
                            "][" & RsProducto!c_Descri & "] posee un porcentaje de impuesto distinto en la sucursal que " & _
                            "en el pedido original. No es posible empacar el producto. " & _
                            "Presione Aceptar si desea empacar el pedido sin el producto, " & _
                            "o Cancelar para no empacar el pedido a�n. Verifique los datos del producto en las cajas." & GetLines(2) & _
                            "Impuesto en tienda: " & Format(RsProducto!n_Impuesto1, "Standard") & GetLines & _
                            "Impuesto en pedido: " & Format(TmpImp, "Standard")
                            
                            If Retorno Then
                                Cantidad = 0
                                Faltantes = True
                            Else
                                GoTo Error1
                            End If
                            
                        ElseIf Cantidad >= Rs!n_Cantidad Then ' Si hay suficiente, se toma la del pedido.
                            
                            Grid.TextMatrix(I, 4) = Round((Cantidad - Rs!n_Cantidad), _
                            RsProducto!Cant_Decimales)
                            
                            Cantidad = Rs!n_Cantidad
                            Resultado = 0
                            
                        Else ' En caso contrario, se da la restante...
                            
                            Faltantes = True
                            
                            Resultado = Round((Rs!n_Cantidad - Cantidad), _
                            RsProducto!Cant_Decimales)
                            
                        End If
                        
                        If FormaDeTrabajo_Precios = 0 Or FormaDeTrabajo_Precios = 1 Then
                            
FormaDeTrabajo_Precios_Default_3:
                            
                            If RsProducto!n_TipoPeso <> 4 Then
                                '20191127. A�adido que cuando el producto este anclado a una moneda distinta a la predeterminada _
                                obligatoriamente tome el precio del pedido si este es mas barato, debido a que si no no se podr�a _
                                honrar el compromiso del pago que haya sido efectuado previamente por el cliente, las fluctuaciones _
                                al alza podr�an desestabilizar el negocio y muchos pedidos quedar�an sin entregar. Para el comercio _
                                esta situaci�n ser�a immanejable.
                                'If UCase(mCodMonedaProd) <> UCase(FrmAppLink.CodMonedaPref) Then
                                    '20200123 'Mejor tomar siempre el precio mas barato que este disponible _
                                    para dar soporte al caso de ofertas.
                                    Precio = IIf(TmpPrecioDoc < PrecioUsar, TmpPrecioDoc, PrecioUsar)
                                'Else
                                    'Precio = PrecioUsar
                                'End If
                            Else
                                ' Para los productos informativos siempre se toma el precio de la transacci�n.
                                Precio = TmpPrecioDoc
                            End If
                            
                            Subtotal = (Cantidad * Precio)
                            Impuesto = (Subtotal * (RsProducto!n_Impuesto1 / 100))
                            Total = (Subtotal + Impuesto)
                            
                        ElseIf FormaDeTrabajo_Precios = 2 Then
                            
                            ' Calcular con la informaci�n del Pedido y no de la
                            ' Ficha del producto, donde la informaci�n var�a.
                            
                            Precio = TmpPrecioDoc
                            Subtotal = (Cantidad * Precio)
                            
                            If Rs!n_Subtotal <> 0 Then
                                TmpImp = Round(((Rs!n_Impuesto / Rs!n_Subtotal) * 100), 6) ' Round para evitar basura decimal
                            Else
                                TmpImp = 0
                            End If
                            
                            Impuesto = (Subtotal * (TmpImp / 100))
                            
                            Total = (Subtotal + Impuesto)
                            
                        Else
                            GoTo FormaDeTrabajo_Precios_Default_3
                        End If
                        
                        If Cantidad > 0 Then
                            
                            If TR_INV_n_FactorMonedaProd Then
                                mCampoMonedaProd = ", n_FactorMonedaProd"
                                mFactorMonedaProd = ", " & mFactorMonedaProd & ""
                            Else
                                mCampoMonedaProd = Empty
                                mFactorMonedaProd = Empty
                            End If
                            
                            SQL = "INSERT INTO TR_INVENTARIO " & _
                            "(c_LINEA, c_CONCEPTO, c_DOCUMENTO, c_DEPOSITO, c_CODARTICULO, " & _
                            "n_CANTIDAD, n_COSTO, n_SUBTOTAL, n_IMPUESTO, n_TOTAL, " & _
                            "c_TIPOMOV, n_cant_teorica, n_cant_diferencia, N_PRECIO, " & _
                            "N_PRECIO_ORIGINAL, f_fecha, c_codlocalidad, n_factorcambio, " & _
                            "C_DESCRIPCION, C_COMPUESTO, codconcepto, n_descuentogeneral, " & _
                            "n_descuentoespecifico, c_documento_origen, c_tipodoc_origen, " & _
                            "N_CANTIDADFAC, ns_descuento, cs_comprobanteContable, cs_codlocalidad, " & _
                            "ns_CantidadEmpaque, IMPUESTO" & mCampoMonedaProd & ") " & vbNewLine & _
                            "VALUES ( " & _
                            "" & Cont & ", 'NDE', '" & PedidoNDE & "', '" & Deposito & "', " & _
                            "'" & FixTSQL(Rs!c_CodArticulo) & "', " & CDec(Cantidad) & ", " & CDec(CostoUsar) & ", " & _
                            "" & CDec(Subtotal) & ", " & CDec(Impuesto) & ", " & CDec(Total) & ", 'Descargo', " & _
                            "" & CDec(Cantidad) & ", 0, " & CDec(Precio) & ", " & CDec(TmpPrecioDoc) & ", " & _
                            "CAST(GETDATE() AS DATE), '" & Localidad & "', " & CDec(FacMonedaDoc) & ", " & _
                            "'" & FixTSQL(Rs!c_Descripcion) & "', '', 0, " & 0 & ", " & 0 & ", '" & Pedido & "', " & _
                            "'PED', 0, 0, '', '" & Localidad & "', " & CDec(RsProducto!n_CantiBul) & ", " & _
                            "0" & mFactorMonedaProd & ") "
                            
                            Ent.BDD.Execute SQL
                            
                            'update en n_cant_ventidad con la cantidad de articulos faltantes para terminar con el empacado de este articulo
                            SQL = "UPDATE TR_VENTAS SET " & _
                            "n_Cant_Vendida = " & Resultado & " " & _
                            "WHERE c_Documento = '" & Pedido & "' " & _
                            "AND c_Concepto = 'PED' " & _
                            "AND c_CodArticulo = '" & Rs!c_CodArticulo & "' "
                            
                            Ent.BDD.Execute SQL
                            
                            'update en n_cant_comprometida y n_cantidad disminuyendo la cantidad empacada
                            SQL = "UPDATE MA_DEPOPROD SET " & _
                            "n_Cant_Comprometida = ROUND(n_Cant_Comprometida - " & Cantidad & ", 8, 0), " & _
                            "n_Cantidad = ROUND(n_Cantidad - " & Cantidad & ", 8, 0) " & _
                            "WHERE c_CodArticulo = '" & Rs!c_CodArticulo & "' " & _
                            "AND c_CodDeposito = '" & Deposito & "' "
                            
                            Ent.BDD.Execute SQL
                            
                        End If
                        
                        Exit For
                        
                    End If
                    
                Next I
                
                Cont = Cont + 1
                Rs.MoveNext
                
            Wend
            
            Rs.Close
            
            VE_DECRETO_2602.VE_DECRETO_2602_AplicaTransaccion = True
            
Totalizar:
            
            SQL = "SELECT COUNT(*) AS CantProd, (ROUND(CASE WHEN n_Subtotal <> 0 " & _
            "THEN (n_Impuesto / n_Subtotal) ELSE 0 END, 2) * 100) AS Porc, " & _
            "SUM(n_Subtotal) AS Base, SUM(n_Impuesto) AS Imp " & _
            "FROM TR_INVENTARIO WITH (NOLOCK) " & _
            "WHERE c_Concepto = 'NDE' " & _
            "AND c_Documento = '" & PedidoNDE & "' " & _
            "GROUP BY (ROUND(CASE WHEN n_Subtotal <> 0 " & _
            "THEN (n_Impuesto / n_Subtotal) ELSE 0 END, 2) * 100) "
            
            Rs.Open SQL, Ent.BDD, adOpenKeyset, adLockReadOnly, adCmdText
            
            Dim Exento As Double, BaseImp As Double, Impuestos As Double
            
            If Rs.EOF Then
                
                Ent.BDD.RollbackTrans
                Trans = False
                
                If AnularPedido(Ent.BDD, Lote, Pedido) Then
                    Unload Me
                End If
                
                Exit Sub
                
            End If
            
            Subtotal = 0
            Impuestos = 0
            BaseImp = 0
            Total = 0
            
            While Not Rs.EOF
                
                If Rs!Porc = 0 Then
                    Exento = Rs!Base
                Else
                    
                    BaseImp = (BaseImp + Rs!Base)
                    Impuestos = (Impuestos + Rs!Imp)
                    
                    ' Actualizaci�n: Parece que la Nota de Entrega Normal de Stellar
                    ' tampoco graba MA_VENTAS_IMPUESTOS. Entonces no es necesario.
                    ' Sin embargo se deja la consulta y el resto de este c�digo para
                    ' la totalizaci�n del MA_VENTAS.
                    
                    'SQL = "INSERT INTO MA_VENTAS_IMPUESTOS " & _
                    "(cs_Documento, cs_TipoDoc, ns_BaseImpuesto, " & _
                    "ns_MontoImpuesto, ns_PorcentajeImpuesto, cs_CodProveedor, cs_CodLocalidad) " & _
                    "VALUES ('" & PedidoNDE & "', 'NDE', " & Rs!Base & ", " & Rs!Imp & ", " & _
                    "" & Rs!Porc & ", '" & CodCliente & "', '" & Localidad & "')"
                    
                    'Ent.BDD.Execute SQL
                    
                End If
                
                Rs.MoveNext
                
            Wend
            
            Rs.Close
            
            Subtotal = (BaseImp + Exento)
            Total = (Subtotal + Impuestos)
            
            If ManejaIGTF_Ventas And mMontoIGTF_Original > 0 Then
                mMontoIGTF_Final = (Total * mPorcIGTF / 100)
                'Total = (Total + mMontoIGTF_Final)
            End If
            
            ' -------
            
            If VE_DECRETO_2602.VE_DECRETO_2602_Activo _
            And VE_DECRETO_2602.VE_DECRETO_2602_AplicaTransaccion Then
                
                Select Case UCase(Mid(RifCliente, 1, 1))
                    
                    Case "V", "E"
                        
                        Dim Expresion As RegExp
                        
                        Set Expresion = New RegExp
                        
                        Expresion.Pattern = "[^0-9]"
                        Expresion.Global = True
                        
                        VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = Expresion.Replace( _
                        RifCliente, vbNullString)
                        
                        VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = _
                        (Len(VE_DECRETO_2602.VE_DECRETO_2602_TmpObj) < 9)
                        
                        If VE_DECRETO_2602.VE_DECRETO_2602_TmpObj Then
                            VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = _
                            VE_DECRETO_2602.VE_DECRETO_2602_PermitirNaturales
                        Else
                            VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = _
                            VE_DECRETO_2602.VE_DECRETO_2602_PermitirJuridicos
                        End If
                        
                    Case "P"
                        
                        VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = _
                        VE_DECRETO_2602.VE_DECRETO_2602_PermitirNaturales
                        
                    Case "J", "G"
                        
                        VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = _
                        VE_DECRETO_2602.VE_DECRETO_2602_PermitirJuridicos
                        
                    Case Else
                        
                        VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = _
                        VE_DECRETO_2602.VE_DECRETO_2602_PermitirNaturales _
                        And VE_DECRETO_2602.VE_DECRETO_2602_PermitirJuridicos
                        
                End Select
                
                If Not VE_DECRETO_2602.VE_DECRETO_2602_TmpObj Then GoTo VE_DECRETO_2602_FIN
                
                Dim MontoComparar As Double
                
                Select Case VE_DECRETO_2602.VE_DECRETO_2602_CampoMontoComparar
                    Case 0
Default:
                        MontoComparar = BaseImp
                    Case 1
                        MontoComparar = Total
                    Case 2
                        MontoComparar = Subtotal
                    Case Else
                        GoTo Default
                End Select
                
                If Not VE_DECRETO_2602.VE_DECRETO_2602_AlicuotasSegunMonto Is Nothing Then
                    For I = 1 To VE_DECRETO_2602.VE_DECRETO_2602_AlicuotasSegunMonto.Count
                        VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = VE_DECRETO_2602.VE_DECRETO_2602_AlicuotasSegunMonto(I)
                        If I = VE_DECRETO_2602.VE_DECRETO_2602_AlicuotasSegunMonto.Count Then
                            VE_DECRETO_2602.VE_DECRETO_2602_AlicuotaAplicar = CDbl(Split(VE_DECRETO_2602.VE_DECRETO_2602_TmpObj, ";")(1))
                        Else
                            VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = CDbl(Split(VE_DECRETO_2602.VE_DECRETO_2602_TmpObj, ";")(0))
                            If MontoComparar <= CDbl(VE_DECRETO_2602.VE_DECRETO_2602_TmpObj) Then
                                VE_DECRETO_2602.VE_DECRETO_2602_AlicuotaAplicar = CDbl(Split(VE_DECRETO_2602.VE_DECRETO_2602_AlicuotasSegunMonto(I), ";")(1))
                                Exit For
                            Else
                                VE_DECRETO_2602.VE_DECRETO_2602_MontoLimite = CDbl(VE_DECRETO_2602.VE_DECRETO_2602_TmpObj)
                            End If
                        End If
                    Next
                Else
                    VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = vbNullString
                    GoTo VE_DECRETO_2602_FIN
                End If
                
                SQL = "UPDATE TR_INVENTARIO SET " & GetLines & _
                "n_Precio = TBFinal.Precio, " & GetLines & _
                "n_Subtotal = TBFinal.Subtotal, " & GetLines & _
                "n_Impuesto = TBFinal.Impuesto, " & GetLines & _
                "n_Total = TBFinal.Total " & GetLines & _
                "FROM TR_INVENTARIO I " & _
                "INNER JOIN (" & GetLines & _
                "SELECT *, ROUND(Subtotal + Impuesto, 8, 1) AS Total FROM ( " & GetLines & _
                "SELECT *, ROUND((Subtotal * PorcImp), 4, 1) AS Impuesto FROM ( " & GetLines & _
                "SELECT c_Linea, c_Concepto, c_Documento, c_CodArticulo, " & _
                "ID, n_Precio AS Precio, " & GetLines & _
                "ROUND(n_Cantidad * n_Precio, 8, 1) AS Subtotal, " & GetLines
                
                SQL = SQL & _
                "CAST((" & VE_DECRETO_2602.VE_DECRETO_2602_AlicuotaAplicar & "/100.0) AS NUMERIC(20,4)) AS PorcImp " & GetLines & _
                "FROM TR_INVENTARIO INV " & _
                "WHERE C_CONCEPTO = 'NDE' " & _
                "AND C_DOCUMENTO = '" & PedidoNDE & "' " & GetLines & _
                "AND (ROUND(CASE WHEN n_Subtotal <> 0 THEN " & _
                "(n_Impuesto / n_Subtotal) ELSE 0 END, 2) * 100) = " & _
                "(" & VE_DECRETO_2602.VE_DECRETO_2602_AlicuotaCambiar & ") " & GetLines & _
                ") TB1 " & GetLines & _
                ") TB2 " & GetLines & _
                ") TBFinal " & GetLines & _
                "ON TBFinal.c_Documento = I.c_Documento " & GetLines & _
                "AND TBFinal.c_Concepto = I.c_Concepto " & GetLines & _
                "AND TBFinal.c_CodArticulo = I.c_CodArticulo " & GetLines & _
                "AND TBFinal.c_Linea = I.c_Linea " & GetLines & _
                "AND TBFinal.c_CodArticulo = I.c_CodArticulo " & GetLines & _
                "AND TBFinal.ID = I.ID "
                
                Ent.BDD.Execute SQL, HowManyAffected
                
                VE_DECRETO_2602.VE_DECRETO_2602_AplicaTransaccion = False
                
                GoTo Totalizar ' Recalcular...
                
VE_DECRETO_2602_FIN:
                
            End If
            
            If PrevenirDuplicados(Ent.BDD) Then
                
                Mensaje True, "Atenci�n, este pedido ya ha sido empacado, probablemente " & _
                "desde otro equipo. Verifique con los otros operadores por que ya la nota " & _
                "de entrega ha sido generada."
                
                Ent.BDD.RollbackTrans
                Trans = False
                
                Exit Sub
                
            End If
            
            If ModoGestorPreautorizacion = 1 And Total > 0 Then ' Completar Antes de Empacar.
                
                ' AQUI PROCESAR PAGO PREAUTORIZADO. _
                TRANCAR EJECUCI�N. VOLVER A CHEQUEAR ROW DE PREAUTORIZACION _
                Y HACER UPDATE A ESTATUS = 1 SI ES NECESARIO.
                
                If ExisteTabla("MA_VENTAS_PREAUTORIZACION_WEB", Ent.BDD) Then
                    
                    EstatusPago = BuscarValorBD("Estatus", "" & _
                    "SELECT TOP 1 Estatus " & GetLines & _
                    "FROM MA_VENTAS_PREAUTORIZACION_WEB WITH (NOLOCK) " & GetLines & _
                    "WHERE Pedido = '" & Pedido & "' " & _
                    "ORDER BY ID DESC ", -1, Ent.BDD)
                    
                    If EstatusPago <> -1 Then
                        
                        'Total = 100 ' Debug
                        
                        ShellAndWait App.Path & "\GestorPreAutorizacion.exe " & _
                        "1 " & Pedido & " " & PedidoNDE & " " & Round((Total + mMontoIGTF_Final), 2), vbNormalFocus
                        
                        EstatusPago = BuscarValorBD("Estatus", "" & _
                        "SELECT TOP 1 Estatus " & GetLines & _
                        "FROM MA_VENTAS_PREAUTORIZACION_WEB WITH (NOLOCK) " & GetLines & _
                        "WHERE Pedido = '" & Pedido & "' " & _
                        "ORDER BY ID DESC ", -1, Ent.BDD)
                        
                        If EstatusPago <= 1 Then
                            Err.Raise 999, "GestorPreautorizacion", _
                            "No se pudo finalizar el pago. Intente de nuevo mas tarde."
                        End If
                        
                    End If
                    
                End If
                
            End If
            
            Dim mDirEnt1 As String ', mDirEnt2 As String
            
            If ExisteCampoTabla("c_DireccionEntrega", , _
            "SELECT c_DireccionEntrega FROM MA_VENTAS WHERE 1 = 2", , Ent.BDD) Then
                mDirEnt1 = " c_DireccionEntrega,"
            End If
            
            Dim mCampoIGTF1, mValorIGTF1
            
            If mMontoIGTF_Final > 0 Then
                mCampoIGTF1 = ", n_Imp_IGTF, n_Base_IGTF"
                mValorIGTF1 = ", (" & CDec(mMontoIGTF_Final) & "), (" & CDec(Total) & ")"
            End If
            
            'FrmAppLink.CodMonedaPref se cambia para guardar la moneda del documento
            'se cambia el 1 por el factor de la moneda al momento
            
            SQL = "INSERT INTO MA_VENTAS " & _
            "(cs_ORGANIZACION, c_DOCUMENTO, c_CONCEPTO, d_FECHA, c_DESCRIPCION, " & _
            "c_direccion, c_rif, c_nit, c_telefono, c_status, c_CODCLIENTE, " & _
            "c_CODLOCALIDAD, c_CODMONEDA, n_FACTORCAMBIO, n_DESCUENTO, c_OBSERVACION, " & _
            "c_RELACION, c_CODVENDEDOR, d_fecha_recepcion, NS_BASE_RETENCION, NS_RETENCION, " & _
            "N_SUBTOTAL, N_IMPUESTO, NU_MONTO_SERVICIO, N_TOTAL, NU_MONTO_CANCELADO, " & _
            "C_CODDEPOSITO, n_baseimp, c_orden_compra, codconcepto, cu_vendedor_cod, " & _
            "cu_codafiliado, N_VUELTO, NU_MONTO_VUELTO, cs_comprobanteContable, " & _
            "cs_codlocalidad, CS_NUMERO_TRANSFERENCIA," & mDirEnt1 & " " & _
            "CS_CODUNIDAD, bu_impresa, CS_NUMTRANSF_DESTINO" & mCampoIGTF1 & ") " & vbNewLine & _
            "SELECT cs_ORGANIZACION, '" & PedidoNDE & "', 'NDE', CAST(GETDATE() AS DATE), " & _
            "c_DESCRIPCION, c_direccion, c_rif, c_nit, c_telefono, 'DPE', c_CODCLIENTE, " & _
            "c_CODLOCALIDAD, '" & CodMonedaDoc & "' AS c_CodMoneda, " & FacMonedaDoc & " AS n_FACTORCAMBIO, " & _
            "0, ('Seg�n Pedido [' + c_DOCUMENTO + ']' + CHAR(13) + CAST(c_OBSERVACION AS nvarchar(MAX))), " & _
            "(c_Concepto + ' ' + c_Documento), c_CODVENDEDOR, CAST(GETDATE() AS DATE), 0, 0, " & _
            "" & Subtotal & ", " & Impuestos & ", 0, " & Total & ", " & Total & ", C_CODDEPOSITO, " & _
            "" & BaseImp & ", c_orden_compra, codconcepto, cu_vendedor_cod, cu_codafiliado, " & _
            "0, 0, '', cs_codlocalidad, 'N/A'," & mDirEnt1 & " '', 1, ''" & mValorIGTF1 & _
            " " & vbNewLine & _
            "FROM MA_VENTAS WITH (NOLOCK) " & _
            "WHERE c_Concepto = 'PED' " & _
            "AND c_Documento = '" & Pedido & "' "
            
            Ent.BDD.Execute SQL
            
            'If Faltantes Then 'Si es true eso quiere decir que el pedido NO fue totalmente completado c_status = DPE
                
                'Colocar el pedido con status = DPE
                'SQL = "UPDATE MA_VENTAS WITH (ROWLOCK) SET c_Status = 'DPE' WHERE c_Documento = '" & Pedido & "' AND c_Concepto = 'PED'"
                'Ent.BDD.Execute SQL
                
            'Else 'Si es false eso quiere decir que el pedido fue totalmente completado c_status = DCO
                
                'Finalizar el pedido, status = DCO
                
                SQL = "UPDATE MA_VENTAS WITH (ROWLOCK) " & _
                "SET c_Status = 'DCO' " & _
                "WHERE c_Documento = '" & Pedido & "' " & _
                "AND c_Concepto = 'PED' "
                
                Ent.BDD.Execute SQL
                
                ' Mejor para estos pedidos de Picking & Packing, colocar siempre DCO.
                ' Sin embargo, si hubo faltantes, hay que liberar la cantidad restante de la
                ' cantidad comprometida al instante. De esta manera esas cantidades ya no quedan
                ' en el aire, y no hay que preocuparse de que el cliente (la organizaci�n)
                ' tenga que anular el pedido manualmente o que al hacer la facturaci�n masiva
                ' del lote haya que liberar las cantidades en ese momento.
                
                If Faltantes Then
                    RestaurarComprometidos Ent.BDD, Pedido, Deposito
                End If
                
            'End If
            
            Ent.BDD.CommitTrans
            Trans = False
            
            ' AQUI PROCESAR PAGO PREAUTORIZADO. _
            TRANCAR EJECUCI�N. VOLVER A CHEQUEAR ROW DE PREAUTORIZACION _
            Y HACER UPDATE A ESTATUS = 1 SI ES NECESARIO.
            
            If ModoGestorPreautorizacion = 0 And Total > 0 Then  ' Completar
                
                If ExisteTabla("MA_VENTAS_PREAUTORIZACION_WEB", Ent.BDD) Then
                    
                    EstatusPago = BuscarValorBD("Estatus", "" & _
                    "SELECT TOP 1 Estatus " & GetLines & _
                    "FROM MA_VENTAS_PREAUTORIZACION_WEB " & GetLines & _
                    "WHERE Pedido = '" & Pedido & "' " & _
                    "ORDER BY ID DESC ", -1, Ent.BDD)
                    
                    If EstatusPago <> -1 Then
                        
                        'Total = 100 ' Debug
                        ShellAndWait App.Path & "\GestorPreAutorizacion.exe " & _
                        "1 " & Pedido & " " & PedidoNDE & " " & Round((Total + mMontoIGTF_Final), 2), vbNormalFocus
                        
                        EstatusPago = BuscarValorBD("Estatus", "" & _
                        "SELECT TOP 1 Estatus " & GetLines & _
                        "FROM MA_VENTAS_PREAUTORIZACION_WEB " & GetLines & _
                        "WHERE Pedido = '" & Pedido & "' " & _
                        "ORDER BY ID DESC ", -1, Ent.BDD)
                        
                        If EstatusPago <= 0 Then
                            
                            Ent.BDD.Execute _
                            "UPDATE MA_VENTAS_PREAUTORIZACION_WEB SET " & GetLines & _
                            "Estatus = 1, NotaDeEntrega = '" & PedidoNDE & "', " & _
                            "MontoFinal = (" & Round((Total + mMontoIGTF_Final), 2) & ") " & GetLines & _
                            "WHERE ID IN ( " & GetLines & _
                            "SELECT TOP 1 ID FROM MA_VENTAS_PREAUTORIZACION_WEB " & GetLines & _
                            "WHERE Pedido = '" & Pedido & "' " & _
                            "ORDER BY ID DESC " & GetLines & _
                            ") "
                            
                        End If
                        
                    End If
                    
                End If
                
            End If
            
            Call ImprimirNotaDeEntrega(CantEmpaques)
            
            Unload Me
            
        End If
        
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If Trans Then
        Ent.BDD.RollbackTrans
        Trans = False
    End If
    
    If mErrorNumber <> 0 Then
        Mensaje True, "Error al finalizar el empaque del pedido, " & _
        "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    End If
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Public Sub ImprimirNotaDeEntrega(ByVal CantEmpaques As Long, _
Optional ByVal pShowRTF As Boolean = False)
    If CantEmpaques = 1 Then
        Call LlenarRTF(1, CantEmpaques, pShowRTF)
        'Call RPT_DOC_VENTAS(lcConsecu, Find_Concept, Find_Status, IIf(mImpres = 0, 5, Index), TipoImpresion)
    Else
        Dim I As Long
        For I = 1 To CantEmpaques
            Call LlenarRTF(I, CantEmpaques, pShowRTF)
        Next I
    End If
End Sub

Private Sub LlenarRTF(NumTique As Long, TotalTique As Long, _
Optional ByVal pShowRTF As Boolean = False)
    
    On Error GoTo Error1
    
    Dim Rs As New ADODB.Recordset, Rtf As recsun.Obj_rtf, SQL As String
    Set Rtf = New recsun.Obj_rtf
    
    Dim mMontoIGTF As Variant, mBaseIGTF As Variant, mPorcIGTF As Variant, _
    mTotalConIGTF As Variant
    
    With Rtf
        
        .RutayArchivo = NDEArchivo '"C:\rtf\NDEPacking.rtf"
        
        .IdentifidicadorMA = "H1"
        .IdentifidicadorTR = "D1"
        
        'N� confirmacion de la WEB
        SQL = "SELECT SUBSTRING(c_Relacion, 5, 9999) AS PedidoWeb, * " & _
        "FROM MA_VENTAS " & _
        "WHERE c_Concepto = 'PED' " & _
        "AND c_Documento = '" & Pedido & "' "
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        If Not Rs.EOF Then
            
            Call .LLenarCamposMA("H101W", CStr(Rs!PedidoWeb))
            
            PedidoWeb = CStr(Rs!PedidoWeb)
            
            If ExisteCampoTabla("c_DireccionEntrega", Rs) Then
                
                Dim mDireccionEntrega As String: mDireccionEntrega = Rs!c_DireccionEntrega
                
                mLongitud = SVal(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "50"))
                If mLongitud = 0 Then mLongitud = 50
                
                mCadenaAux = mDireccionEntrega & " "
                
                Call .LLenarCamposMA("H1062", Replace(mCadenaAux, vbNewLine, " "))
                
                mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, mCadenaAux, False), vbNewLine)
                
                For I = 0 To 5
                    
                    mVariable = "H1062-" & Format(I + 1, "#")
                    
                    If I <= UBound(mCadena) Then
                        Call .LLenarCamposMA(CStr(mVariable), CStr(mCadena(I)))
                    Else
                        Call .LLenarCamposMA(CStr(mVariable), vbNullString)
                    End If
                    
                Next I
                
            End If
            
        End If
        
        Rs.Close
        
        'Cantidad de Empaques y fecha
        
        SQL = "SELECT CantEmpaques, FechaEmpacado " & _
        "FROM MA_PEDIDOS_RUTA_PACKING " & _
        "WHERE CodPedido = '" & Pedido & "' "
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        Call .LLenarCamposMA("H101", CStr(NumTique))
        Call .LLenarCamposMA("H102", CStr(TotalTique))
        Call .LLenarCamposMA("H1017", Trim(Rs!FechaEmpacado))
        
        Rs.Close
        
        'Datos de la Empresa
        
        SQL = "SELECT Nom_Org AS Nombre, Dir_Org AS Direccion, " & _
        "Tlf_Org AS Telefono, Rif_Org AS RIF " & _
        "FROM ESTRUC_SIS"
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        Call .LLenarCamposMA("H1001", Trim(Rs!Nombre))
        
        Call .LLenarCamposMA("H1002", Trim(Rs!Direccion))
        
        mLongitud = SVal(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "50"))
        
        If mLongitud = 0 Then mLongitud = 50
        
        mCadenaAux = Rs!Direccion & " "
        
        mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, mCadenaAux, False), vbNewLine)
        
        For I = 0 To 5
            
            mVariable = "H1002-" & Format(I + 1, "#")
            
            If I <= UBound(mCadena) Then
                Call .LLenarCamposMA(CStr(mVariable), CStr(mCadena(I)))
            Else
                Call .LLenarCamposMA(CStr(mVariable), vbNullString)
            End If
            
        Next I
        
        Call .LLenarCamposMA("H1003", Trim(Rs!Telefono))
        Call .LLenarCamposMA("H1018", Trim(Rs!Rif))
        
        Rs.Close
        
        'Nombre de Usuario
        
        SQL = "SELECT USR.Descripcion AS Nombre " & _
        "FROM MA_PEDIDOS_RUTA_PICKING PP " & _
        "INNER JOIN MA_USUARIOS USR " & vbNewLine & _
        "ON PP.CodEmpacador = USR.CodUsuario " & vbNewLine & _
        "WHERE PP.CodPedido =  '" & Pedido & "' "
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        Call .LLenarCamposMA("H1014", Trim(Rs!Nombre))
        
        Rs.Close
        
        'C�digo de Documento
        
        Call .LLenarCamposMA("H1005", Trim(PedidoNDE)) 'Pedido
        Call .LLenarCamposMA("H1005P", Trim(Pedido)) 'Pedido
        
        'Sucursal
        Call .LLenarCamposMA("H1006", Trim(FrmAppLink.GetCodLocalidadSistema))
        Call .LLenarCamposMA("H1006-1", Trim(FrmAppLink.GetNomLocalidadSistema))
        
        'Datos de la NDE
        
        SQL = "SELECT n_Impuesto AS mImpuesto, " & _
        "n_Subtotal AS mSubtotal, n_Total AS mTotal, * " & vbNewLine & _
        "FROM MA_VENTAS " & _
        "WHERE c_Documento = '" & PedidoNDE & "' " & _
        "AND c_Concepto = 'NDE' "
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        Call .LLenarCamposMA("H1009", CStr(Rs!mSubtotal), True, 2)
        Call .LLenarCamposMA("H1011", CStr(Rs!mImpuesto), True, 2)
        Call .LLenarCamposMA("H1012", CStr(Rs!mTotal), True, 2)
        
        If ManejaIGTF_Ventas Then
            
            mMontoIGTF = CDec(Rs!n_Imp_IGTF)
            mBaseIGTF = CDec(Rs!n_Base_IGTF)
            
            If mBaseIGTF <> 0 Then
                mPorcIGTF = Round(mMontoIGTF / mBaseIGTF * 100, 2)
            Else
                mPorcIGTF = 0
            End If
            
            mTotalConIGTF = CDec(Rs!mTotal) + mMontoIGTF
            
        Else
            
            mMontoIGTF = 0
            mBaseIGTF = 0
            mPorcIGTF = 0
            
            mTotalConIGTF = Rs!mTotal
            
        End If
        
        Rs.Close
        
        'Datos del Cliente
        
        SQL = "SELECT MA_CLIENTES.c_DESCRIPCIO AS Nombre, " & _
        "MA_CLIENTES.c_RIF AS RIF, MA_CLIENTES.c_DIRECCION AS Direccion, " & vbNewLine & _
        "MA_CLIENTES.c_Telefono AS Telefono, MA_CLIENTES.c_Email AS Email " & _
        "FROM MA_CLIENTES " & _
        "INNER JOIN MA_VENTAS " & _
        "ON MA_CLIENTES.c_CODCLIENTE = MA_VENTAS.c_CODCLIENTE " & vbNewLine & _
        "AND MA_VENTAS.c_CONCEPTO = 'PED' " & _
        "WHERE MA_VENTAS.c_DOCUMENTO = '" & Pedido & "' "
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        Call .LLenarCamposMA("H1004", Trim(Rs!Nombre))
        Call .LLenarCamposMA("H1016", Trim(Rs!Email))
        Call .LLenarCamposMA("H1019", Trim(Rs!Rif))
        Call .LLenarCamposMA("H1020", Rs!Direccion)
        
        mLongitud = SVal(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "50"))
        If mLongitud = 0 Then mLongitud = 50
        
        mCadenaAux = Rs!Direccion & " "
        
        mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, mCadenaAux, False), vbNewLine)
        
        For I = 0 To 5
            
            mVariable = "H1020-" & Format(I + 1, "#")
            
            If I <= UBound(mCadena) Then
                Call .LLenarCamposMA(CStr(mVariable), CStr(mCadena(I)))
            Else
                Call .LLenarCamposMA(CStr(mVariable), vbNullString)
            End If
            
        Next I
        
        Call .LLenarCamposMA("H1021", Rs!Telefono)
        
        Rs.Close
        
        'LLenando el Tr Con la Informacion de los productos empacados
        SQL = "SELECT PRO.C_CODIGO as CodProducto, DET.n_CANTIDAD AS Cantidad, " & vbNewLine & _
        "PRO.C_DESCRI as Descripcion, DET.N_PRECIO AS PrecioUnitario, " & vbNewLine & _
        "DET.n_TOTAL AS Total, DET.n_SUBTOTAL AS Subtotal, " & vbNewLine & _
        "DET.n_IMPUESTO AS Impuesto, isNULL(COD.c_Codigo, 'N/A') AS CodEDI " & vbNewLine & _
        "FROM TR_INVENTARIO DET " & vbNewLine & _
        "INNER JOIN MA_PRODUCTOS PRO " & vbNewLine & _
        "ON DET.c_CODARTICULO = PRO.C_CODIGO " & vbNewLine & _
        "LEFT JOIN MA_CODIGOS COD " & vbNewLine & _
        "ON DET.c_CODARTICULO = COD.c_CodNasa " & vbNewLine & _
        "AND COD.nu_Intercambio = 1 " & vbNewLine & _
        "WHERE DET.c_DOCUMENTO = '" & PedidoNDE & "' " & vbNewLine & _
        "AND DET.c_CONCEPTO = 'NDE' " & vbNewLine & _
        "ORDER BY DET.c_LINEA, DET.ID "
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        While Not Rs.EOF
            
            .LLenarCamposTr_add
            
            .LLenarCamposTr "D1001", Rs!CodProducto
            .LLenarCamposTr "D1002", Rs!Cantidad
            .LLenarCamposTr "D1003", Rs!Descripcion
            .LLenarCamposTr "D1004", Rs!PrecioUnitario, True, 2
            .LLenarCamposTr "D1005", Rs!Total, True, 2
            .LLenarCamposTr "D1018", Rs!CodEDI
            .LLenarCamposTr "D1019", Rs!Subtotal, True, 2
            .LLenarCamposTr "D1020", Rs!Impuesto, True, 2
            
            Rs.MoveNext
            
        Wend
        
        Rs.Close
        
        Dim mTipoDespacho As String, mCostoEnvio As Double
        
        Set TmpRs = ExecuteSafeSQL( _
        "SELECT * FROM MA_ORDERS_DELIVERYMETHOD " & _
        "WHERE Code = '" & Pedido & "' ", Ent.BDD, , True)
        
        If TmpRs Is Nothing Then
            
            mTipoDespacho = "Retiro en Sitio"
            
        Else
            
            If TmpRs.EOF Then
                mTipoDespacho = "Retiro en Sitio"
            Else
                
                Select Case TmpRs!Type
                    Case 2
                        mTipoDespacho = "Delivery"
                        mDireccionDespacho = mDireccionEntrega
                    Case 3
                        mTipoDespacho = "Entrega en Punto de Retiro"
                        mDireccionDespacho = mDireccionEntrega
                    Case Else
                        mTipoDespacho = "Retiro en Sitio"
                End Select
                
                If ExisteCampoTabla("ShippingCost", TmpRs) Then
                    mCostoEnvio = TmpRs!ShippingCost
                End If
                
            End If
            
        End If
        
        ' Numeros Reservados para datos de Entrega.
        
        Call .LLenarCamposMA("H1022", mTipoDespacho)
        Call .LLenarCamposMA("H1023", CStr(mCostoEnvio), True, 2)
        Call .LLenarCamposMA("H1024", Empty)
        Call .LLenarCamposMA("H1025", Empty)
        
        'If ManejaIGTF_Ventas Then
            
            Call .LLenarCamposMA("H1026", CStr(mBaseIGTF), True, 2)
            Call .LLenarCamposMA("H1027", CStr(mPorcIGTF), True, 2)
            Call .LLenarCamposMA("H1028", CStr(mMontoIGTF), True, 2)
            Call .LLenarCamposMA("H1029", CStr(mTotalConIGTF), True, 2)
            
        'End If
        
        If Not TmpRs Is Nothing Then
            TmpRs.Close
        End If
        
        'jesus
        
        If (Trim(mvarNDEPrinter) <> Empty) Then
            .Impresora = mvarNDEPrinter
        End If
        
        .MostrarSelecionImpresora = pShowRTF ' para imprimir directo con la impresora por defecto
        .ModoImpresion = IIf(pShowRTF, s_RtfPantalla, s_RtfImpresora) ' para imprimir directo con la impresora por defecto
        
        .Ejecutar
        
    End With
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al generar el RTF de la Nota de Entrega, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Grid_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyF6 And Shift = 0 Then
        
        If Grid.Rows > 1 Then
            
            Mensaje False, "Atenci�n! �Est� seguro de que no hay " & _
            "cantidad faltante de ning�n producto en este pedido? " & _
            GetLines & _
            "En tal caso, presione Aceptar para continuar"
            
            If Retorno Then
                
                CheckAll = True
                
                For I = 1 To Grid.Rows - 1
                    Grid.Row = I
                    Grid.RowSel = Grid.Row
                    Grid_DblClick
                Next I
                
                CheckAll = False
                
                PrepararGrid
                PrepararDatos
                
            End If
            
        End If
        
    End If
    
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call Grid_DblClick
    End If
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub Chk_Lote_Click()
    If Chk_Lote.Value = vbChecked Then
        OrderBy = "ORDER BY MA_PEDIDOS_RUTA_PICKING.CantSolicitada DESC"
        Chk_NombreProducto.Value = vbUnchecked
        Call PrepararGrid
        Call PrepararDatos
    End If
End Sub

Private Sub Chk_NombreProducto_Click()
    If Chk_NombreProducto.Value = vbChecked Then
        OrderBy = "ORDER BY MA_PRODUCTOS.c_Descri"
        Chk_Lote.Value = vbUnchecked
        Call PrepararGrid
        Call PrepararDatos
    End If
End Sub

Private Sub Cod_Buscar_GotFocus()
    Cod_Buscar.SelStart = 0
    Cod_Buscar.SelLength = Len(Cod_Buscar)
End Sub

Private Sub Cod_Buscar_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        
        Dim SQL As String, Rs As New ADODB.Recordset, Band As Integer
        
        SQL = "SELECT c_CodNasa " & _
        "FROM MA_CODIGOS " & _
        "WHERE c_Codigo = '" & FixTSQL(Trim(Cod_Buscar.Text)) & "' "
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        If Not Rs.EOF Then
            
            Cod_Buscar = Rs!c_CodNasa
            
            Band = 0
            
            For I = 1 To Grid.Rows - 1
                
                If UCase(Grid.TextMatrix(I, 5)) = UCase(Cod_Buscar) Then
                    
                    Band = I
                    
                    If Grid.TextMatrix(I, 3) <> Grid.TextMatrix(I, 4) Then
                        
                        Grid.Row = I
                        
                        Call Grid_DblClick
                        
                        Cod_Buscar = Empty
                        
                        Exit Sub
                        
                    End If
                    
                End If
                
            Next I
            
            If Band > 0 Then
                Call Mensaje(True, "El producto ya fue recolectado.")
            Else
                Call Mensaje(True, "El producto no esta dentro de su pedido.")
            End If
            
        Else
            
            Call Mensaje(True, "C�digo no encontrado, verifique el c�digo a buscar.")
            
            If gRutinas.PuedeObtenerFoco(Cod_Buscar) Then Cod_Buscar.SetFocus
            
            Exit Sub
            
        End If
        
        Cod_Buscar = Empty
        
    End If
    
End Sub

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute( _
    "SELECT c_Proceso " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE c_Proceso = '009' ")
    
    If Not TempRs.EOF Then frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub
