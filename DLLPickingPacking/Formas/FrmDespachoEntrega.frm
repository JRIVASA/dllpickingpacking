VERSION 5.00
Begin VB.Form FrmDespachoEntrega 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8760
   ClientLeft      =   -45
   ClientTop       =   -45
   ClientWidth     =   6735
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8760
   ScaleWidth      =   6735
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame TipoPedido 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   4
      Top             =   720
      Width           =   6015
      Begin VB.OptionButton OptPersona 
         Caption         =   "Autorizado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   3240
         TabIndex        =   6
         Top             =   360
         Width           =   2175
      End
      Begin VB.OptionButton OptComprador 
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Value           =   -1  'True
         Width           =   2175
      End
   End
   Begin VB.CommandButton cmd_listo 
      Caption         =   "Finalizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   5040
      Picture         =   "FrmDespachoEntrega.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   7020
      Width           =   1335
   End
   Begin VB.Frame FrameTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   19335
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Control de Entrega del Pedido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   120
         Width           =   3615
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   6000
         Picture         =   "FrmDespachoEntrega.frx":1D82
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   3840
         TabIndex        =   1
         Top             =   120
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4695
      Left            =   360
      TabIndex        =   7
      Top             =   1920
      Width           =   6015
      Begin VB.TextBox txtTarjetaHabiente 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   735
         Left            =   2160
         MultiLine       =   -1  'True
         TabIndex        =   16
         Top             =   3600
         Width           =   3615
      End
      Begin VB.TextBox txtTelefono 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   1680
         TabIndex        =   15
         Top             =   2760
         Width           =   4095
      End
      Begin VB.TextBox txtPedido 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   1680
         TabIndex        =   10
         Top             =   240
         Width           =   4095
      End
      Begin VB.TextBox txtNombre 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   1680
         MultiLine       =   -1  'True
         TabIndex        =   9
         Top             =   1080
         Width           =   4095
      End
      Begin VB.TextBox txtID 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   1680
         TabIndex        =   8
         Top             =   1920
         Width           =   4095
      End
      Begin VB.Label lblTarjetaHabiente 
         BackStyle       =   0  'Transparent
         Caption         =   "Tarjetahabiente (si aplica)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   735
         Left            =   240
         TabIndex        =   17
         Top             =   3720
         Width           =   1815
      End
      Begin VB.Label Telefono 
         BackStyle       =   0  'Transparent
         Caption         =   "Tel�fono"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   14
         Top             =   2880
         Width           =   1335
      End
      Begin VB.Label lblPedido 
         BackStyle       =   0  'Transparent
         Caption         =   "Pedido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Nombre 
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   12
         Top             =   1200
         Width           =   1335
      End
      Begin VB.Label ID 
         BackStyle       =   0  'Transparent
         Caption         =   "C.I / ID"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   11
         Top             =   2040
         Width           =   1335
      End
   End
End
Attribute VB_Name = "FrmDespachoEntrega"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public PedidoWeb As String
Public PedidoStellar As String
Public Factura As String
Public Comprador As String

Public Entregado As Boolean

Private FormaCargada As Boolean

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        ' Actualizacion 2017 11 29: No se comprobar� el estatus de los pagos desde esta ventana.
        ' Se hicieron modificaciones en la C�nsola Gesti�n de Pedidos para no permitir finalizar
        ' Lotes de pedidos si no est�n pagados, o en caso de que no se puedan pagar se deben anular
        ' Por lo cual efectivamente ya no hay que hacer este control en este punto.
        ' Se asume que todos los pedidos que se van a entregar ya est�n pagados.
        
        'If ExisteTabla("MA_VENTAS_PREAUTORIZACION_WEB", Ent.BDD) Then
            
            'Dim HayPagoPre As Long
            
            'HayPagoPre = BuscarValorBD("Estatus", "" & _
            "SELECT Estatus" & GetLines & _
            "FROM MA_VENTAS_PREAUTORIZACION_WEB" & GetLines & _
            "WHERE Pedido = '" & PedidoStellar & "' ORDER BY ID DESC", -1, Ent.BDD)
            
            'If HayPagoPre <> -1 Then
                
                'Select Case HayPagoPre
                    
                    'Case 0, 1, 3
                        
                        'ActivarMensajeGrande 25
                        
                        'Mensaje False, "Atenci�n, este pedido posee un pago preautorizado pendiente" & _
                        " que no ha sido a�n cancelado. Se debe solventar dicha situaci�n antes" & _
                        " de poder marcar el pedido como entregado. " & vbNewLine & vbNewLine & _
                        "Presione aceptar para intentar completar el pago."
                        
                        'If Retorno Then
                            
                            'Dim NDE As String, MontoPago As Double, PagoRealizado As Boolean
                            
                            'If DetallesPreautorizacion(PedidoStellar, NDE, MontoPago) Then
                                
                                'ShellAndWait App.Path & "\GestorPreAutorizacion.exe " & _
                                "1 " & PedidoStellar & " " & NDE & " " & Round(MontoPago, 2), vbNormalFocus
                                
                                'EstatusPago = BuscarValorBD("Estatus", "" & _
                                "SELECT TOP 1 Estatus" & GetLines & _
                                "FROM MA_VENTAS_PREAUTORIZACION_WEB" & GetLines & _
                                "WHERE Pedido = '" & PedidoStellar & "' ORDER BY ID Desc", -1, Ent.BDD)
                                
                                'If EstatusPago = 2 Or EstatusPago = 4 Or EstatusPago = 6 Then
                                    'PagoRealizado = True
                                'End If
                                
                            'End If
                            
                            'If PagoRealizado Then
                                'Mensaje True, "Pago completado exitosamente."
                            'Else
                                'Mensaje True, "Intento fallido."
                            'End If
                            
                        'End If
                        
                        'If Not PagoRealizado Then Unload Me
                        
                        'Exit Sub
                        
                    'Case 5
                        
                        'ActivarMensajeGrande 15
                        
                        'Mensaje True, "Atenci�n, este pedido posee un pago preautorizado que" & _
                        " ha sido anulado. No es posible entregar el pedido." & _
                        vbNewLine & vbNewLine & _
                        "Se deber� hacer la devoluci�n de la factura" & _
                        " o solventar el pago administrativamente."
                        
                        'Unload Me
                        
                        'Exit Sub
                        
                'End Select
                
            'End If
            
        'End If
        
    End If
    
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    
    AjustarPantalla Me
    
    Dim SQL As String, Rs As New ADODB.Recordset
    
    SQL = "SELECT c_Descripcio AS Nombre, c_Rif AS ID, c_Telefono AS Telefono " & _
    "FROM MA_CLIENTES WHERE c_CodCliente = '" & Comprador & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Not Rs.EOF Then
        txtNombre.Text = Rs!Nombre
        txtID.Text = Rs!ID
        txtTelefono.Text = Rs!Telefono
    End If
    
    txtPedido.Text = IIf(Trim(PedidoWeb) <> vbNullString, PedidoWeb, PedidoStellar)
    txtPedido.Enabled = False
    
    Call OptComprador_Click
    
    lbl_Organizacion.Caption = "Pedido N� " & IIf(Trim(PedidoWeb) <> vbNullString, PedidoWeb, PedidoStellar)
    
    Rs.Close
    
    ' EL False HardCoded es por que temporalmente estar� deshabilitado hasta que se defina bien el proceso
    ' o haya una manera de cumplir con las restricciones de ley que no permiten a terceros recibir
    ' los pedidos. Se pretende en el futuro hacer unas "Cartas de Autorizaci�n firmadas" que permitir�an
    ' Determinar quien puede tener esta opci�n habilitada pero eso ser�a un desarrollo adicional
    ' que involucrar�a una nueva tabla que no es MA_ORDERS_DELIVERYMETHOD.
    ' Por lo cual ahorita mismo no estar� disponible seg�n requerimiento del cliente.
    
    OptPersona.Enabled = False 'ExisteTabla("MA_ORDERS_DELIVERYMETHOD", Ent.BDD)
    
End Sub

Private Sub cmd_listo_Click()
    
    On Error GoTo Error
    
    Dim SQL As String
    
    If txtNombre <> "" And txtID <> "" Then
        
        SQL = "UPDATE MA_VENTAS " & _
        "SET PackingBoolean = 1 " & _
        "WHERE c_Concepto = 'VEN' " & _
        "AND c_Documento = '" & Factura & "'"
        
        Ent.BDD.Execute SQL
        
        'SQL = "insert into MA_ENTREGAS (Documento, Descripcion, RIF, Fecha, Telefono)" & vbNewLine & _
        "values ('" & Pedido & "','" & txtNombre & "','" & txtID & "',getdate(),'" & txtTelefono & "')"
        'Ent.BDD.Execute SQL
        
        Dim mRS As Recordset
        Set mRS = New Recordset
        
        With mRS
            
            .Open "SELECT * FROM MA_ENTREGAS WHERE 1 = 2", _
            Ent.BDD, adOpenKeyset, adLockOptimistic, adCmdText
            
            .AddNew
            
            !Documento = IIf(Trim(PedidoWeb) <> vbNullString, PedidoWeb, PedidoStellar)
            !Descripcion = txtNombre.Text
            !RIF = txtID.Text
            'Fecha = ' DEFAULT GetDate()
            !Telefono = txtTelefono.Text
            !PedidoRelacion = PedidoStellar
            !Factura = Factura
            
            If ExisteCampoTabla("Tarjetahabiente", mRS) Then
                !Tarjetahabiente = txtTarjetaHabiente.Text
            End If
            
            .Update
            
            .Close
            
        End With
        
        Entregado = True
        
        Mensaje True, "Proceso finalizado satisfactoriamente."
        
        Unload Me
        
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Cmd_Listo_Click)"
    
End Sub

Private Sub OptComprador_Click()
    
    txtNombre.Enabled = False
    txtID.Enabled = False
    
    If Trim(txtTelefono) = vbNullString Then
        txtTelefono.Enabled = True
    Else
        txtTelefono.Enabled = False
    End If
    
    Dim SQL As String, Rs As New ADODB.Recordset
    
    SQL = "SELECT c_Descripcio AS Nombre, c_Rif AS ID, c_Telefono AS Telefono " & _
    "FROM MA_CLIENTES WHERE c_CodCliente = '" & Comprador & "'"
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Not Rs.EOF Then
        txtNombre.Text = Rs!Nombre
        txtID.Text = Rs!ID
        txtTelefono.Text = Rs!Telefono
    End If
    
    Rs.Close
    
    If Trim(PedidoWeb) <> vbNullString Then
        
        SQL = "SELECT TOP 1 * FROM MA_VENTAS_WEB_DETPAG WHERE Code = '" & PedidoWeb & "'"
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not Rs.EOF Then
            txtTarjetaHabiente.Text = IIf(Trim(Rs!PersonName & " " & Rs!PersonLastName) _
            <> vbNullString, Rs!PersonName & " " & Rs!PersonLastName, "N/A")
        Else
            txtTarjetaHabiente.Text = "N/A"
        End If
        
    End If
    
    txtTarjetaHabiente.Locked = True
    
End Sub

Private Sub OptPersona_Click()
    
    txtNombre = Empty
    txtID = Empty
    txtTelefono = Empty
    txtTarjetaHabiente = Empty
    
    txtNombre.Enabled = True
    txtID.Enabled = True
    txtTelefono.Enabled = True
    txtTarjetaHabiente.Enabled = True
    
    Dim SQL As String, Rs As New ADODB.Recordset
    
    SQL = "SELECT * FROM MA_ORDERS_DELIVERYMETHOD DM " & vbNewLine & _
    "INNER JOIN MA_VENTAS PED " & vbNewLine & _
    "ON PED.c_Concepto = 'PED' " & vbNewLine & _
    "AND PED.c_Documento = DM.Code " & vbNewLine & _
    "WHERE PED.c_Documento = '" & PedidoStellar & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Not Rs.EOF Then
        txtNombre = Rs!PersonName & " " & Rs!PersonLastName
        txtID = Rs!PersonCedula
        txtTelefono = Rs!PersonPhone '& " / " & Rs!PersonAdditionalPhone
    End If
    
    Rs.Close
    
    If Trim(PedidoWeb) <> vbNullString Then
        
        SQL = "SELECT TOP 1 * FROM MA_VENTAS_WEB_DETPAG WHERE Code = '" & PedidoWeb & "'"
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not Rs.EOF Then
            txtTarjetaHabiente.Text = IIf(Trim(Rs!PersonName & " " & Rs!PersonLastName) _
            <> vbNullString, Rs!PersonName & " " & Rs!PersonLastName, "N/A")
        Else
            txtTarjetaHabiente.Text = "N/A"
        End If
        
    End If
    
    txtTarjetaHabiente.Locked = True
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Function DetallesPreautorizacion(ByVal pPedido As String, _
ByRef oNDE As String, ByRef oMonto As Double) As Boolean
    
    On Error GoTo Error
    
    Dim mRS As Recordset, mSQL As String
    
    Set mRS = Ent.BDD.Execute("SELECT * FROM MA_VENTAS " & _
    "WHERE c_Concepto = 'NDE' " & _
    "AND c_Relacion = 'PED " & pPedido & "' ")
    
    If Not mRS.EOF Then
        oNDE = mRS!c_Documento
        oMonto = mRS!n_Total
        DetallesPreautorizacion = True
    End If
    
    Exit Function
    
Error:
    
End Function
