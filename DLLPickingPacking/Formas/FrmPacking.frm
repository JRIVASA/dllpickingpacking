VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmPacking 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btn_asignarm 
      Caption         =   "Asignar m"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   9960
      Picture         =   "FrmPacking.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   23
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9960
      Width           =   1215
   End
   Begin VB.PictureBox Picture2 
      Height          =   255
      Left            =   0
      Picture         =   "FrmPacking.frx":0CCA
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   22
      Top             =   240
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture1 
      Height          =   255
      Left            =   0
      Picture         =   "FrmPacking.frx":2114
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   21
      Top             =   0
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Frame FrameSelect 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   6240
      TabIndex        =   20
      Top             =   9600
      Visible         =   0   'False
      Width           =   1120
      Begin VB.Image CmdSelect 
         Height          =   300
         Left            =   120
         Picture         =   "FrmPacking.frx":355E
         Top             =   120
         Width           =   900
      End
   End
   Begin VB.CommandButton Cmd_Listo 
      Caption         =   "Finalizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   14040
      Picture         =   "FrmPacking.frx":38B7
      Style           =   1  'Graphical
      TabIndex        =   19
      ToolTipText     =   "Empacar Lote"
      Top             =   9960
      Width           =   1095
   End
   Begin VB.CommandButton ButtonLeyenda 
      Caption         =   "Leyenda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   11280
      Picture         =   "FrmPacking.frx":5639
      Style           =   1  'Graphical
      TabIndex        =   18
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9960
      Width           =   1095
   End
   Begin VB.CheckBox Chk_Pedido 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Pedido"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   480
      TabIndex        =   15
      Top             =   10440
      Width           =   1065
   End
   Begin VB.CheckBox Chk_Lote 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Empacador"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   2760
      TabIndex        =   14
      Top             =   10440
      Width           =   1545
   End
   Begin VB.CheckBox Chk_NombreProducto 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Cant. Solicitada"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   2760
      TabIndex        =   13
      Top             =   10080
      Width           =   2265
   End
   Begin VB.CheckBox Chk_Fecha 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Fecha"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   420
      Left            =   480
      TabIndex        =   12
      Top             =   9960
      Value           =   1  'Checked
      Width           =   1065
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   19320
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmPacking.frx":6303
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12600
         TabIndex        =   5
         Top             =   120
         Width           =   1815
      End
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Consola Asignaci�n para Empacado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   480
         TabIndex        =   4
         Top             =   120
         Width           =   12135
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   8850
      LargeChange     =   10
      Left            =   14490
      TabIndex        =   2
      Top             =   600
      Width           =   674
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   5400
      Top             =   10200
   End
   Begin VB.TextBox txtminutos 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   8160
      TabIndex        =   1
      Text            =   "5"
      Top             =   10320
      Width           =   375
   End
   Begin VB.CommandButton ButtonActualizar 
      Caption         =   "Actualizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   12600
      Picture         =   "FrmPacking.frx":8085
      Style           =   1  'Graphical
      TabIndex        =   0
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9960
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   8865
      Left            =   140
      TabIndex        =   6
      Top             =   600
      Width           =   15000
      _ExtentX        =   26458
      _ExtentY        =   15637
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ProgressBar bar 
      Height          =   255
      Left            =   10080
      TabIndex        =   7
      Top             =   9525
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.Label C�digo 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Criterios de Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Index           =   71
      Left            =   240
      TabIndex        =   16
      Top             =   9720
      Width           =   2805
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00AE5B00&
      X1              =   3135
      X2              =   5075
      Y1              =   9885
      Y2              =   9885
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Pr�xima actualizaci�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   7560
      TabIndex        =   10
      Top             =   9525
      Width           =   2535
   End
   Begin VB.Shape Shape1 
      Height          =   240
      Index           =   4
      Left            =   7560
      Top             =   9525
      Visible         =   0   'False
      Width           =   6015
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Intervalo de actualizar: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   5520
      TabIndex        =   9
      Top             =   10320
      Width           =   2535
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "minutos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   8640
      TabIndex        =   8
      Top             =   10320
      Width           =   855
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   735
      Index           =   0
      Left            =   5280
      TabIndex        =   11
      Top             =   10080
      Width           =   4455
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   1215
      Index           =   2
      Left            =   120
      TabIndex        =   17
      Top             =   9600
      Width           =   5055
   End
End
Attribute VB_Name = "FrmPacking"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Lote As String

Private Salir As Variant
Private OrderBy As String

Public BarBool As Boolean

Private CantFilas As Long

Private Sub btn_asignarm_Click()
    Call AsignarPedidosMas
    Call ButtonActualizar_Click
End Sub

Private Sub Form_Activate()
    
    If Salir Then
        
        Mensaje True, "No hay pedidos pendientes por empacar."
        
        Unload Me
        
        Exit Sub
        
    End If
    
End Sub

Private Sub Form_Load()
    
    Salir = False
    
    OrderBy = "ORDER BY MIN(MA_PEDIDOS_RUTA_PICKING.FechaAsignacion) "
    
    AjustarPantalla Me
    
    Call PrepararGrid
    Call PrepararDatos
    Call txtminutos_LostFocus
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionFree
        
        .FixedCols = 0
        .FixedRows = 1
        
        .Rows = 1
        .Cols = 9
        
        .RowHeight(0) = 600
        .RowHeightMin = 600
        
        .FormatString = "Fecha" & "|" & "Pedido" & "|" & "Empacador" & "|" & _
        "Art. Sol" & "|" & "Art. Rec" & "|" & "Art. Emp"
        
        .ColWidth(0) = 3850
        .ColAlignment(0) = flexAlignCenterCenter
        
        .ColWidth(1) = 2000
        .ColAlignment(1) = flexAlignCenterCenter
        
        .ColWidth(2) = 3850
        .ColAlignment(2) = flexAlignLeftCenter
        
        .ColWidth(3) = 1300
        .ColAlignment(3) = flexAlignCenterCenter
        
        .ColWidth(4) = 1300
        .ColAlignment(4) = flexAlignCenterCenter
        
        .ColWidth(5) = 1300
        .ColAlignment(5) = flexAlignCenterCenter
        
        .ColWidth(6) = 1300
        
        .ColWidth(7) = 0
        
        .ColWidth(8) = 600
        
        .Width = 15000
                
        .ScrollTrack = True
        
        For I = 0 To .Cols - 1
            .Col = I
            .CellAlignment = flexAlignCenterCenter
        Next
        
        .Row = 0
        .Col = 8
        .ColSel = 7
        
    End With
    
End Sub

Private Sub PrepararDatos()
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset
    
    SQL = "SELECT MA_PEDIDOS_RUTA_PICKING.CodPedido, " & vbNewLine & _
    "ROUND(SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada), MAX(Pro.Cant_Decimales), 0) as CantSolicitada, " & vbNewLine & _
    "ROUND(SUM(MA_PEDIDOS_RUTA_PICKING.CantRecolectada), MAX(Pro.Cant_Decimales), 0) as CantRecolectada, " & vbNewLine & _
    "MIN(MA_PEDIDOS_RUTA_PICKING.FechaAsignacion) as FechaAsignacion, " & vbNewLine & _
    "ROUND(SUM(MA_PEDIDOS_RUTA_PICKING.CantEmpacada), MAX(Pro.Cant_Decimales), 0) as CantEmpacada, " & vbNewLine & _
    "isNull(MA_USUARIOS.descripcion, '') as NombreEmpacador " & vbNewLine & _
    "FROM MA_PEDIDOS_RUTA_PICKING " & vbNewLine & _
    "INNER JOIN MA_PRODUCTOS Pro ON MA_PEDIDOS_RUTA_PICKING.CodProducto = PRO.C_CODIGO" & vbNewLine & _
    "INNER JOIN MA_Pedidos_Ruta " & vbNewLine & _
    "on MA_PEDIDOS_RUTA_PICKING.CodLote = MA_Pedidos_Ruta.cs_Corrida " & vbNewLine & _
    "left join MA_USUARIOS on MA_PEDIDOS_RUTA_PICKING.CodEmpacador = " & vbNewLine & _
    "MA_USUARIOS.codusuario where MA_PEDIDOS_RUTA_PICKING.CodLote = '" & Lote & "' " & vbNewLine & _
    "and MA_Pedidos_Ruta.Picking = 1" & vbNewLine & _
    "group by MA_PEDIDOS_RUTA_PICKING.CodLote,  MA_PEDIDOS_RUTA_PICKING.CodPedido, " & vbNewLine & _
    "MA_USUARIOS.descripcion-- HAVING SUM(MA_PEDIDOS_RUTA_PICKING.CantRecolectada) > 0" & vbNewLine & _
    OrderBy
    
    Rs.Open SQL, Ent.BDD, adOpenStatic, adLockReadOnly, adCmdText
    
    If Rs.EOF Then
        Salir = True
        Exit Sub
    End If
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    CantFilas = Rs.RecordCount
    
    BarBool = False
    
    If CantFilas > 12 Then
        BarBool = True
    End If
    
    Grid.Visible = False
    
    While Not Rs.EOF
        
        Grid.Rows = Grid.Rows + 1
        
        Grid.TextMatrix(Grid.Rows - 1, 0) = Rs!FechaAsignacion
        Grid.TextMatrix(Grid.Rows - 1, 1) = Rs!CodPedido
        Grid.TextMatrix(Grid.Rows - 1, 2) = Rs!NombreEmpacador
        Grid.TextMatrix(Grid.Rows - 1, 3) = Rs!CantSolicitada
        Grid.TextMatrix(Grid.Rows - 1, 4) = Rs!CantRecolectada
        Grid.TextMatrix(Grid.Rows - 1, 5) = Rs!CantEmpacada
        
        'If GRID.TextMatrix(GRID.Rows - 1, 2) <> "" Then
            'GRID.Row = GRID.Rows - 1
            'Set GRID.CellPicture = Me.Picture1.Picture
        'Else
            Grid.Row = Grid.Rows - 1
            Set Grid.CellPicture = Me.Picture2.Picture
        'End If
        
        If Rs!CantEmpacada = Rs!CantRecolectada Then
            For I = 0 To Grid.Cols - 1
                Grid.Row = Grid.Rows - 1
                Grid.Col = I
                Grid.CellBackColor = &HB4EDB9 'verde
            Next I
        ElseIf (Rs!NombreEmpacador <> Empty) And (Rs!CantEmpacada > 0) Then
            For I = 0 To Grid.Cols - 1
                Grid.Row = Grid.Rows - 1
                Grid.Col = I
                Grid.CellBackColor = -2147483624 'Amarillo
            Next I
        End If
        
        Rs.MoveNext
        
    Wend
    
    Rs.Close
    
    Grid.Col = 0
    Grid.Row = 1
    
    If Grid.Rows > 12 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(0) = 3350
        Grid.ColWidth(2) = 3350
    Else
        Grid.ScrollBars = flexScrollBarNone
        Grid.ColWidth(0) = 3350
        ScrollGrid.Visible = False
    End If
    
    LabelTitulo.Caption = "Consola Asignaci�n para Empacado del Lote N� " & Lote
    
Finally:
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub AsignarPedidosMas()
    
    On Error GoTo Error1
    
    If Grid.TextMatrix(Grid.RowSel, 1) <> Empty Then
        
        Dim SQL As String
        Dim Resultado As Variant
        
        Dim mTipoPre As String
        
        If Packing_RequiereEmpacadores Then
            mTipoPre = "PAC"
        Else
            mTipoPre = "PIC"
        End If
        
        SQL = "SELECT USR.CodUsuario, USR.Descripcion " & _
        "FROM MA_USUARIOS USR " & vbNewLine & _
        "INNER JOIN MA_VENDEDORES VEN " & _
        "ON '" & mTipoPre & "_' + USR.CodUsuario = VEN.cu_Vendedor_Cod " & vbNewLine & _
        "WHERE USR.bs_Activo = 1 " & _
        "AND VEN.cs_Tipo = '" & mTipoPre & "' "
        
        Retorno = True
    
        'If Grid.TextMatrix(Grid.RowSel, 5) = 0 Then
            
            If Grid.TextMatrix(Grid.RowSel, 2) <> Empty Then
                
                Retorno = Mensaje(False, "Este Pedido ya tiene " & _
                "un empacador �Seguro que desea cambiarlo?")
                
            End If
            
            If Retorno Then
                
                With Frm_Super_Consultas
                    
                    .Inicializar SQL, "U S U A R I O S", Ent.BDD
                    
                    .Add_ItemLabels "Codigo", "codusuario", 2610, 0
                    .Add_ItemLabels "Nombre", "descripcion", 8500, 0
                    .Add_ItemSearching "Nombre", "descripcion"
                    .Add_ItemSearching "Codigo", "codusuario"
                    
                    .txtDato.Text = "%"
                    
                    '.BusquedaInstantanea = True
                    
                    .Show vbModal
                    
                    Resultado = .ArrResultado
                    
                End With
                
                Set Frm_Super_Consultas = Nothing
                
                If Not IsEmpty(Resultado) Then
                    
                    If Trim(Resultado(0)) <> Empty Then
                        
                        For I = 1 To Grid.Rows - 1
                            
                            Grid.Col = 8
                            Grid.Row = I
                            
                            If SDec(Grid.TextMatrix(I, 5)) = "0" _
                            And Grid.CellPicture = Me.Picture1.Picture Then
                                
                                SQL = "UPDATE MA_PEDIDOS_RUTA_PICKING SET " & _
                                "CodSupervisorPacking = '" & LcCodUsuario & "', " & vbNewLine & _
                                "CodEmpacador = '" & Resultado(0) & "' " & _
                                "WHERE CodLote = '" & Lote & "' " & vbNewLine & _
                                "AND CodPedido = '" & Grid.TextMatrix(Grid.RowSel, 1) & "' "
                                
                                Ent.BDD.Execute SQL
                                
                            End If
                            
                        Next I
                        
                        Grid.TextMatrix(Grid.RowSel, 2) = Resultado(1)
                        
                        Call ButtonActualizar_Click
                        
                    End If
                    
                End If
                
            End If
            
        'Else
            'Mensaje True, "No puede ser cambiado el empacador de este pedido, ya que el proceso de empacado ha culminado."
        'End If
        
    Else
        Mensaje True, "Seleccione un pedido."
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al asignar el empacador, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub AsignarPedidos()
    
    On Error GoTo Error1
    
    If Grid.TextMatrix(Grid.RowSel, 1) <> Empty Then
        
        Dim SQL As String
        Dim Resultado As Variant
        
        Dim mTipoPre As String
        
        If Packing_RequiereEmpacadores Then
            mTipoPre = "PAC"
        Else
            mTipoPre = "PIC"
        End If
        
        SQL = "SELECT USR.CodUsuario, USR.Descripcion " & _
        "FROM MA_USUARIOS USR" & vbNewLine & _
        "INNER JOIN MA_VENDEDORES VEN " & _
        "ON '" & mTipoPre & "_' + USR.CodUsuario = VEN.cu_Vendedor_Cod" & vbNewLine & _
        "WHERE USR.bs_Activo = 1 " & _
        "AND VEN.cs_Tipo = '" & mTipoPre & "' "
        
        Retorno = True
        
        'If Grid.TextMatrix(Grid.RowSel, 5) = 0 Then
            
            If Grid.TextMatrix(Grid.RowSel, 2) <> Empty _
            And Grid.TextMatrix(Grid.RowSel, 5) = 0 Then
                
                Retorno = Mensaje(False, "Este Pedido ya tiene " & _
                "un empacador �Seguro que desea cambiarlo?")
                
            ElseIf Grid.TextMatrix(Grid.RowSel, 2) <> Empty _
            And Grid.TextMatrix(Grid.RowSel, 5) <> 0 Then
                
                Retorno = Mensaje(False, "Este Pedido ya tiene un empacador " & _
                "y el proceso de empacado est� en curso (indic� cantidades), " & _
                "�Est� seguro que desea cambiarlo? En caso afirmativo " & _
                "tambi�n se reasignar� el pedido en Stellar MOBILE.")
                
            End If
            
            If Retorno Then
                
                With Frm_Super_Consultas
                    
                    .Inicializar SQL, "U S U A R I O S", Ent.BDD
                    
                    .Add_ItemLabels "Codigo", "codusuario", 2610, 0
                    .Add_ItemLabels "Nombre", "descripcion", 8500, 0
                    .Add_ItemSearching "Nombre", "descripcion"
                    .Add_ItemSearching "Codigo", "codusuario"
                    
                    .txtDato.Text = "%"
                    
                    '.BusquedaInstantanea = True
                    
                    .Show vbModal
                    
                    Resultado = .ArrResultado
                    
                End With
                
                Set Frm_Super_Consultas = Nothing
                
                If Not IsEmpty(Resultado) Then
                    
                    If Trim(Resultado(0)) <> Empty Then
                         
'                         For i = 1 To Grid.Rows - 1
'
'                             Grid.Col = 8
'                             Grid.Row = i
'
'                            If Grid.TextMatrix(i, 5) = "0" Then
                                
                                SQL = "UPDATE MA_PEDIDOS_RUTA_PICKING SET " & _
                                "PackingMobile_CantEmpaques = 0, " & _
                                "CodSupervisorPacking = '" & LcCodUsuario & "', " & vbNewLine & _
                                "CodEmpacador = '" & Resultado(0) & "' " & _
                                "WHERE CodLote = '" & Lote & "' " & vbNewLine & _
                                "AND CodPedido = '" & Grid.TextMatrix(Grid.RowSel, 1) & "' "
                                
                                Ent.BDD.Execute SQL
                                
'                            End If
                             
'                         Next i
                         
                         Grid.TextMatrix(Grid.RowSel, 2) = Resultado(1)
                         
                         Call ButtonActualizar_Click
                         
                    End If
                    
                End If
                
            End If
            
        'Else
            'Mensaje True, "No puede ser cambiado empacador de este pedido."
        'End If
        
    Else
        Mensaje True, "Seleccione un pedido."
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al asignar el empacador, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub Grid_Click()
    
    Select Case Grid.Col
        
        Case Is = 8
            
            Call Grid_DblClick
            
    End Select
    
End Sub

Private Sub Grid_DblClick()
    
    Select Case Grid.Col
        
        Case 0, 1, 2, 3, 4, 5
            
            AsignarPedidos
            
        Case Is = 8
            
            Grid.Col = Grid.Col
            
            If Grid.CellPicture = Me.Picture2.Picture Then
               Set Grid.CellPicture = Me.Picture1.Picture
               'AsignarPedidosMas
            ElseIf Grid.TextMatrix(Grid.RowSel, 2) <> Empty _
            And Grid.CellPicture = Me.Picture1.Picture Then
                Mensaje True, "Ya hay un empacador asignado."
                Set Grid.CellPicture = Me.Picture2.Picture
            Else
                Set Grid.CellPicture = Me.Picture2.Picture
            End If
            
        Case Else
            
            AsignarPedidosMas
            
            Call ButtonActualizar_Click
            
    End Select
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub ButtonActualizar_Click()
    Call PrepararGrid
    Call PrepararDatos
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub Timer1_Timer()
    DoEvents
    bar.Value = bar.Value + 1
    If bar.Value = bar.Max Then
        Call ButtonActualizar_Click
        Call txtminutos_LostFocus
    End If
End Sub

Private Sub txtminutos_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyReturn
            Call txtminutos_LostFocus
    End Select
End Sub

Private Sub txtminutos_LostFocus()
    
    If Not IsNumeric(Me.txtminutos) Then
        
        Me.txtminutos.Text = "1"
        
        bar.Max = 1 * 60
        bar.Min = 0
        bar.Value = 0
        
        Timer1.Enabled = False
        Timer1.Enabled = True
        
    Else
        
        If txtminutos < 1 Then
            txtminutos = "1"
        End If
        
        bar.Max = Me.txtminutos * 60
        bar.Min = 0
        bar.Value = 0
        
        Timer1.Enabled = False
        Timer1.Enabled = True
        
    End If
    
End Sub

Private Sub Chk_Fecha_Click()
    If Chk_Fecha.Value = vbChecked Then
        OrderBy = "ORDER BY MIN(MA_PEDIDOS_RUTA_PICKING.FechaAsignacion) "
        Chk_Pedido.Value = vbUnchecked
        Chk_Lote.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Pedido_Click()
    If Chk_Pedido.Value = vbChecked Then
        OrderBy = "ORDER BY MA_PEDIDOS_RUTA_PICKING.CodPedido "
        Chk_Lote.Value = vbUnchecked
        Chk_Fecha.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Lote_Click()
    If Chk_Lote.Value = vbChecked Then
        OrderBy = "ORDER BY isNULL(MA_USUARIOS.Descripcion, '') "
        Chk_Pedido.Value = vbUnchecked
        Chk_Fecha.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_NombreProducto_Click()
    If Chk_NombreProducto.Value = vbChecked Then
        OrderBy = "ORDER BY SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada) DESC "
        Chk_Pedido.Value = vbUnchecked
        Chk_Fecha.Value = vbUnchecked
        Chk_Lote.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Cmd_Listo_Click()
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset, Finalizar As Variant
    
    Finalizar = True
    
    SQL = "SELECT CantRecolectada, CantEmpacada, Packing " & _
    "FROM MA_PEDIDOS_RUTA_PICKING " & _
    "WHERE CodLote = '" & Lote & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
   
    While Not Rs.EOF
        
        'If Rs!CantRecolectada <> Rs!CantEmpacada Then
        ' Esto estaba MALO. No importa que se hayan recogido las cantidades o no.
        ' Pero todos los pedidos deben estar Finalizados (Rs!Packing = 1) sino no
        ' se puede cerrar el Lote!.
            If Not Rs!Packing Then
                Finalizar = False
            End If
        'End If
        
        Rs.MoveNext
        
    Wend
    
    Rs.Close
    
    If Finalizar Then
        
        SQL = "UPDATE MA_PEDIDOS_RUTA SET " & _
        "Packing = 1 " & _
        "WHERE cs_Corrida = '" & Lote & "' "
        
        Ent.BDD.Execute SQL
        
        Unload Me
        
        Exit Sub
        
    Else
        Mensaje True, "El empacado del lote esta en proceso."
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
End Sub

Private Sub Grid_EnterCell()
    
    If Grid.Rows = 1 Then
        FrameSelect.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = 600
            End If
            
            Grid.RowHeight(Grid.Row) = 720
            Fila = Grid.Row
            
        End If
        
        MostrarEditorTexto2 Me, Grid, CmdSelect
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        
    Else
        Band = False
    End If
    
End Sub

Public Sub MostrarEditorTexto2(pFrm As Form, pGrd As Object, ByRef txteditor As Object)
    
    On Error Resume Next
    
    With Grid
        
        .RowSel = .Row
        
        If .Col <> 6 Then Band = True
        '.Col = 6
        
        If BarBool Then
            FrameSelect.BackColor = pGrd.BackColorSel
            FrameSelect.Move .Left + (13635 - 1220), .Top + .CellTop, 1290, .CellHeight
            CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
            FrameSelect.Visible = True: CmdSelect.Visible = True
            FrameSelect.ZOrder
        Else
            FrameSelect.BackColor = pGrd.BackColorSel
            FrameSelect.Move .Left + (13635 - 560), .Top + .CellTop, 1290, .CellHeight
            CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
            FrameSelect.Visible = True: CmdSelect.Visible = True
            FrameSelect.ZOrder
        End If
        
        cellRow = .Row
        cellCol = .Col
        
    End With
    
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Private Sub CmdSelect_Click()
    
    On Error GoTo Error1
    
    If Grid.TextMatrix(Grid.RowSel, 1) <> Empty Then
        
        FrmPackingPedido.Pedido = Grid.TextMatrix(Grid.RowSel, 1)
        FrmPackingPedido.Lote = Lote
        
        FrmPackingPedido.Show vbModal
        
        Set FrmPackingPedido = Nothing
        
        Call ButtonActualizar_Click
        
    Else
        Mensaje True, "Seleccione un Pedido."
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al consultar los detalles del pedido, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute( _
    "SELECT c_Proceso " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE c_Proceso = '007'")
    
    If Not TempRs.EOF Then frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub
