Attribute VB_Name = "Stellar_RecursosV2"
Public Enum Alienacion
    flexDefault = -1
    flexAlignLeftTop = 0
    flexAlignLeftCenter = 1
    flexAlignLeftBottom = 2
    flexAlignCenterTop = 3
    flexAlignCenterCenter = 4
    flexAlignCenterBottom = 5
    flexAlignRightTop = 6
    flexAlignRightCenter = 7
    flexAlignRightBottom = 8
    flexAlignGeneral = 9
End Enum

Public Enum Peso
    GRM = 0
    KGS = 1
    TON = 2
End Enum

Public Enum VOL
    MLT = 0
    LTS = 1
End Enum

Public Sub MSGridAsign(ByRef GridObj As Object, Fila As Integer, Columna As Integer, texto As Variant, Optional Tamano As Long = -1, Optional Alinear As Alienacion, Optional Formato As String = "")
    GridObj.Row = Fila
    GridObj.Col = Columna
    If Tamano >= 0 Then
        GridObj.ColWidth(GridObj.ColSel) = Tamano
    End If
    If Alinear > -1 Then
        GridObj.CellAlignment = Alinear
    End If
    If Formato <> "" Then
        texto = Format(texto, Formato)
    End If
    GridObj.Text = IIf(IsNull(texto), "", texto)
End Sub

Sub SetDefMSGrid(ByRef GridObj As Object, Fila As Integer, Columna As Integer)
    GridObj.Col = Columna
    GridObj.Row = Fila
End Sub

Function MSGridRecover(ByRef GridObj As Object, ByVal Fila As Integer, ByVal Columna As Integer) As Variant
    Dim Tcol As Integer, Trow As Integer
    Tcol = GridObj.Col
    Trow = GridObj.Row
    GridObj.Row = Fila
    GridObj.Col = Columna
    MSGridRecover = GridObj.Text
    SetDefMSGrid GridObj, Trow, Tcol
End Function

Sub FillGridRS(ByRef GridObj As Object, ByVal Registros As ADODB.Recordset, ColumnsAlign() As Integer, Formatos() As String)
    GridObj.Rows = 1
    If Not Registros.EOF Then
        Registros.MoveFirst
        While Not Registros.EOF
            GridObj.Rows = GridObj.Rows + 1
            GridObj.Row = GridObj.Rows - 1
            For Cont = 0 To GridObj.Cols - 1
                If UBound(ColumnsAlign) = 0 Then
                    If UBound(Formatos) = 0 Then
                        MSGridAsign GridObj, GridObj.Row, (Cont), Registros.Fields(Cont).value
                    Else
                        MSGridAsign GridObj, GridObj.Row, (Cont), Registros.Fields(Cont).value, , , (Formatos(Cont))
                    End If
                Else
                    If UBound(Formatos) = 0 Then
                        MSGridAsign GridObj, GridObj.Row, (Cont), Registros.Fields(Cont).value, , (ColumnsAlign(Cont))
                    Else
                        MSGridAsign GridObj, GridObj.Row, (Cont), Registros.Fields(Cont).value, , (ColumnsAlign(Cont)), (Formatos(Cont))
                    End If
                End If
            Next Cont
            Registros.MoveNext
        Wend
        GridObj.Rows = GridObj.Rows + 1
    End If
End Sub

Sub DelFilaGrid(GridObj As Object, ByVal Linea As Integer)
    Dim Fila As Integer, FilaT As Integer, Columna As Integer, ColumnaT As Integer
    For Fila = Linea To GridObj.Rows - 2
        For Columna = 1 To GridObj.Cols - 1
            MSGridAsign GridObj, Fila, Columna, MSGridRecover(GridObj, Fila + 1, Columna)
        Next Columna
    Next Fila
    If Linea < GridObj.Rows - 1 Then
        GridObj.Rows = GridObj.Rows - 1
    End If
    SetDefMSGrid GridObj, 1, 1
End Sub
