Attribute VB_Name = "WinApi"
Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, _
ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, _
ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Public Declare Sub GetSystemTime Lib "KERNEL32.dll" (lpSystemTime As SYSTEMTIME)
Public Declare Sub GetLocalTime Lib "KERNEL32.dll" (lpSystemTime As SYSTEMTIME)

Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, _
ByVal bInheritHandle As Long, ByVal dwProcessID As Long) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Private Const NORMAL_PRIORITY_CLASS = &H20
Private Const IDLE_PRIORITY_CLASS = &H40
Private Const HIGH_PRIORITY_CLASS = &H80
Private Const REALTIME_PRIORITY_CLASS = &H100
Private Const PROCESS_DUP_HANDLE = &H40

Private Const SYNCHRONIZE = &H100000
Private Const INFINITE = -1&

Public Type NotifyIconData
    cbSize As Long
    hWnd As Long
    uId As Long
    uFlags As Long
    uCallBackMessage As Long
    hIcon As Long
    szTip As String * 64
End Type

Public Const NIM_ADD = &H0
Public Const NIM_MODIFY = &H1
Public Const NIM_DELETE = &H2
Public Const WM_MOUSEMOVE = &H200
Public Const NIF_MESSAGE = &H1
Public Const NIF_ICON = &H2
Public Const NIF_TIP = &H4
Public Const WM_LBUTTONDBLCLK = &H203 'Double-click
Public Const WM_LBUTTONDOWN = &H201 'Button down
Public Const WM_LBUTTONUP = &H202 'Button up
Public Const WM_RBUTTONDBLCLK = &H206 'Double-click
Public Const WM_RBUTTONDOWN = &H204 'Button down
Public Const WM_RBUTTONUP = &H205 'Button up

Public Declare Function Shell_NotifyIcon Lib "shell32" Alias "Shell_NotifyIconA" (ByVal dwMessage As Long, pnid As NotifyIconData) As Boolean

Public TrayInfo As NotifyIconData

Public Declare Function GetAsyncKeyState Lib "user32.dll" (ByVal vbKey As Long) As Integer

Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

Public Const GWL_EXSTYLE = (-20)
Public Const WS_EX_APPWINDOW = &H40000

Public Declare Function ShowWindow Lib "user32" _
(ByVal hWnd As Long, ByVal nCmdShow As Long) As Long

Const FLASHW_STOP = 0 'Stop flashing. The system restores the window to its original state.
Const FLASHW_CAPTION = &H1 'Flash the window caption.
Const FLASHW_TRAY = &H2 'Flash the taskbar button.
Const FLASHW_ALL = (FLASHW_CAPTION Or FLASHW_TRAY) 'Flash both the window caption and taskbar button. This is equivalent to setting the FLASHW_CAPTION Or FLASHW_TRAY flags.
Const FLASHW_TIMER = &H4 'Flash continuously, until the FLASHW_STOP flag is set.
Const FLASHW_TIMERNOFG = &HC 'Flash continuously until the window comes to the foreground.

Private Type FlashWInfo
    cbSize As Long
    hWnd As Long
    dwFlags As Long
    uCount As Long
    dwTimeout As Long
End Type

Private Declare Function FlashWindowEx Lib "user32" (pfwi As FlashWInfo) As Boolean
Private Declare Function FlashWindow Lib "user32" (ByVal hWnd As Long, ByVal bInvert As Long) As Long
Private Declare Function GetActiveWindow Lib "user32" () As Long

Public TrayedForms          As Collection
Public CallerAppForms       As Collection
Public CallerAppIcon        As Object
Public CallerAppCaption     As String
Public FrmAppLink           As Object

Public CerrarDesdeBandeja   As Boolean

Const WM_NCLBUTTONDOWN = &HA1
Const HTCAPTION = 2

Public Declare Sub ReleaseCapture Lib "user32" ()

Private Declare Function SendMessage Lib "user32.dll" _
Alias "SendMessageA" ( _
ByVal hWnd As Long, _
ByVal wMsg As Long, _
ByVal wParam As Long, _
ByVal lParam As Any) As Long

' Start the indicated program and wait for it
' to finish, hiding while we wait.

Public Function ShellAndWait(ByVal PathName As String, _
Optional ByVal WindowStyle As VbAppWinStyle = vbMinimizedFocus) As Double
    
    Dim Process_Id As Long
    Dim Process_Handle As Long
    
    Process_Id = Shell(PathName, WindowStyle)

    DoEvents

    ' Wait for the program to finish.
    ' Get the process handle.
    
    Process_Handle = OpenProcess(SYNCHRONIZE, 0, Process_Id)
    
    If Process_Handle <> 0 Then
        WaitForSingleObject Process_Handle, INFINITE
        CloseHandle Process_Handle
    End If
    
End Function

Public Function AllAppForms() As Collection
    
    Set AllAppForms = New Collection
    
    Dim i As Long
    
    For i = Forms.Count - 1 To 0 Step -1 ' Getting This App Forms in Call Stack Order
        AllAppForms.Add Forms(i)
    Next i
    
    If Not CallerAppForms Is Nothing Then
        For i = 1 To CallerAppForms.Count ' Getting Caller App Forms. They should be in Call Stack Order already if supplied.
            AllAppForms.Add CallerAppForms(i)
        Next i
    End If
    
End Function

Function TrayName() As String
    TrayName = App.ProductName & " v" & App.Major & "." & App.Minor & "." & App.Revision & vbCrLf & App.CompanyName & vbNullChar
End Function

Sub ToggleTaskbarVisibility(pForm As Form, ByVal pSetVisible As Boolean)
    
    Dim Style As Long
    
    Style = GetWindowLong(pForm.hWnd, GWL_EXSTYLE)
    
    If pSetVisible Then
        Style = Style Or WS_EX_APPWINDOW
    Else
        If Style And WS_EX_APPWINDOW Then
            Style = Style - WS_EX_APPWINDOW
        End If
    End If
    
    ShowWindow pForm.hWnd, 0
    SetWindowLong pForm.hWnd, GWL_EXSTYLE, Style
    ShowWindow pForm.hWnd, 4
    
End Sub

Public Sub TrayApp(Optional Trigger As Form)
    
    If Trigger Is Nothing Then Set Trigger = Screen.ActiveForm
    If Trigger Is Nothing Then Exit Sub
    
    Dim TmpForm As Form, TriggerHandle As Long, AllForms As Collection
    
    TriggerHandle = Trigger.hWnd
    
    If TrayedForms Is Nothing Then
        
        Set TrayedForms = New Collection
        
        Dim PrevLeft, PrevTop, _
        PrevWidth, PrevHeight, _
        PrevShownInTaskbar, i As Long
        
        Set AllForms = AllAppForms
        
        For i = 1 To AllForms.Count
            
            Set TmpForm = AllForms(i)
            
            PrevLeft = TmpForm.Left
            PrevTop = TmpForm.Top
            PrevWidth = TmpForm.Width
            PrevHeight = TmpForm.Height
            PrevShownInTaskbar = False
            
            Dim Style As Long
            
            Style = GetWindowLong(TmpForm.hWnd, GWL_EXSTYLE)
            
            If Style And WS_EX_APPWINDOW Then
                Style = Style - WS_EX_APPWINDOW
                PrevShownInTaskbar = True
            End If
            
            TrayedForms.Add Array(TmpForm, PrevLeft, PrevWidth, _
            PrevTop, PrevHeight, PrevShownInTaskbar), _
            TmpForm.Name & i
            
            'TmpForm.Visible = False ' No se puede en una App con ventanas modales.
            
            'TmpForm.Width = 1 ' Podr�a causar estragos en forms con eventos resize.
            'TmpForm.Height = 1
            
            TmpForm.Left = -100000
            TmpForm.Top = -100000 ' Esto es todo lo que necesitamos para "Ocultar" el app.
            
            If PrevShownInTaskbar Then
                ToggleTaskbarVisibility TmpForm, False
            End If
            
        Next i
        
    End If
    
    '------------------------
    '--- Create Tray Icon ---
    '------------------------
    
    TrayInfo.cbSize = Len(TrayInfo)
    TrayInfo.hWnd = TriggerHandle
    TrayInfo.uId = vbNull
    TrayInfo.uFlags = NIF_ICON Or NIF_TIP Or NIF_MESSAGE
    TrayInfo.uCallBackMessage = WM_MOUSEMOVE
    
    If Not CallerAppIcon Is Nothing Then
        TrayInfo.hIcon = CallerAppIcon
    Else
        TrayInfo.hIcon = FrmPicking.Icon
    End If
    
    ' Tip Text
    
    If CallerAppCaption <> vbNullString Then
        TrayInfo.szTip = CallerAppCaption
    Else
        TrayInfo.szTip = TrayName
    End If
    
    ' Adds the icon to the taskbar area.
    Shell_NotifyIcon NIM_ADD, TrayInfo
    
End Sub

Public Sub UnTrayApp()
    
    If TrayedForms Is Nothing Then Exit Sub
    
    Dim TmpForm As Form, i As Long

    For i = 1 To TrayedForms.Count
        
        Set TmpForm = TrayedForms(i)(0)
        
        If TrayedForms(i)(5) Then
            ToggleTaskbarVisibility TmpForm, True
            WindowFlashA TmpForm.hWnd, , 250, 2
        End If
        
        TmpForm.Left = TrayedForms(i)(1)
        
        TmpForm.Top = TrayedForms(i)(3)
        
    Next
    
    Set TrayedForms = Nothing
    
    'Deletes the tray icon when the form is shown
    Shell_NotifyIcon NIM_DELETE, TrayInfo ' del tray icon
    
End Sub


'Purpose     :  Causes the specified window to "flash".
'Inputs      :  lhwndWindow         The handle to the window to flash.
'               [lFlags]            One or a combinate of the "FLASHW_" flags listed in the declaration section above
'               [lFlashRate]        If specified is the rate at which the screen flashes in ms,
'                                   else uses the default.
'               [lNumFlashes]       The number of times to flash the window. The default, zero, causes it to flash indefinitely.
'Outputs     :  Returns True if the window was active before the call else returns False.
'OS          :  Windows 2000; Windows 98
'Notes       :  Flashing a window means changing the appearance of its caption bar as if the window were changing from
'               inactive to active status, or vice versa. (An inactive caption bar changes to an active caption bar;
'               an active caption bar changes to an inactive caption bar.)

Function WindowFlashA(lhwndWindow As Long, _
Optional lFlags As Long = FLASHW_ALL Or FLASHW_TIMER, _
Optional lFlashRate As Long = 0, _
Optional lNumFlashes As Long = 0) As Boolean
    
    Dim tFlash As FlashWInfo
    
    On Error GoTo ErrFailed
    
    tFlash.cbSize = Len(tFlash)
    'Specifies the flash status
    tFlash.dwFlags = lFlags
    'Specifies the rate, in milliseconds, at which the window will be flashed.
    'If dwTimeout is zero, the function uses the default cursor blink rate.
    tFlash.dwTimeout = lFlashRate
    'Handle to the window to be flashed. The window can be either opened or minimized.
    tFlash.hWnd = lhwndWindow
    'Specifies the number of times to flash the window. Zero will cause it to flash indefinately.
    tFlash.uCount = lNumFlashes
    'Call the API to flash the window
    WindowFlashA = FlashWindowEx(tFlash)
    
    Exit Function
    
ErrFailed:
    
    Debug.Print "Error in WindowFlashA: " & Err.Description
    WindowFlashA = False
    
End Function

Public Sub MoverVentana(hWnd As Long)
    
    On Error Resume Next
    
    Dim lngReturnValue As Long
    
    Call ReleaseCapture
    
    lngReturnValue = SendMessage( _
    hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0&)
    
End Sub
