Attribute VB_Name = "Funciones"
'Para obtener el tama�o de la pantalla
Private Const ABM_GETTASKBARPOS = &H5

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type AppBarData
    cbSize As Long
    hWnd As Long
    uCallBackMessage As Long
    uEdge As Long
    RC As RECT
    lParam As Long
End Type

Private Declare Function SHAppBarMessage Lib "shell32.dll" (ByVal dwMessage As Long, pData As AppBarData) As Long

Public Function GetTaskBarHeight() As Long
    
    On Error Resume Next
    
    Dim ABD As AppBarData
    
    SHAppBarMessage ABM_GETTASKBARPOS, ABD
    
    GetTaskBarHeight = ABD.RC.Bottom - ABD.RC.Top
    
    If ABD.uEdge = 3 Then ' Anclada al fondo... lo com�n.
        GetTaskBarHeight = ABD.RC.Bottom - ABD.RC.Top
    Else ' Anclada a alg�n lado o arriba...
        GetTaskBarHeight = 0
    End If
    
End Function

Public Function GetTaskBarInfo() As AppBarData
    
    On Error Resume Next
    
    Dim ABD As AppBarData
    
    SHAppBarMessage ABM_GETTASKBARPOS, ABD
    
    GetTaskBarInfo = ABD
    
End Function

Public Sub AjustarPantalla(ByRef Forma As Form, _
Optional EsFormNavegador As Boolean = False)
    
    On Error GoTo Error
    
    Dim TbInfo As AppBarData
    
    TbInfo = GetTaskBarInfo
    
    If TbInfo.RC.Bottom = 0 And TbInfo.RC.Top = 0 And TbInfo.RC.Right = 0 And TbInfo.RC.Left = 0 Then
        TbInfo.uEdge = -1
    End If
    
    Select Case TbInfo.uEdge
        
        Case 3
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2)) - (((TbInfo.RC.Bottom - TbInfo.RC.Top) * Screen.TwipsPerPixelY) / 2)
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2))

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If Forma.Top < 0 Then
                Forma.Top = 0
            End If
            
            If (Screen.Width - Forma.Width - Forma.Left) < 0 Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case 2
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2))
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2)) - (((TbInfo.RC.Right - TbInfo.RC.Left) * Screen.TwipsPerPixelX) / 2)

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If Forma.Left < 0 Then
                Forma.Left = 0
            End If
            
        Case 1
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2)) + (((TbInfo.RC.Bottom - TbInfo.RC.Top) * Screen.TwipsPerPixelY) / 2)
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2))
            
            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If (Screen.Width - Forma.Width - Forma.Left < 0) Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case 0
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2))
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2)) + (((TbInfo.RC.Right - TbInfo.RC.Left) * Screen.TwipsPerPixelX) / 2)

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If Forma.Left < ((TbInfo.RC.Right - TbInfo.RC.Left) * Screen.TwipsPerPixelX) Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case -1
            
            Forma.Top = (Screen.Height / 2) - (Forma.Height / 2)
            Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
            
            If Forma.Left < 0 Then Forma.Left = 0
            If Forma.Top < 0 Then Forma.Top = 0
            
    End Select
    
    ' El Form Navegador tiene un footer que siempre debe quedar en bottom.
    
    If EsFormNavegador Then
        Forma.Frame1.Top = (Forma.Height - Forma.Frame1.Height)
    End If
    
    Exit Sub
    
Error:
    
    Debug.Print Err.Description
    
End Sub

Public Function Mensaje(Activo As Boolean, Texto As String, Optional pVbmodal = True, _
Optional txtBoton1 As Variant, Optional txtBoton2 As Variant) As Boolean
    
    Uno = Activo
    
    On Error GoTo Falla_Local
    
    If Not IsMissing(txtBoton1) Then txtMensaje1 = CStr(txtBoton1) Else txtMensaje1 = ""
    If Not IsMissing(txtBoton2) Then txtMensaje2 = CStr(txtBoton2) Else txtMensaje2 = ""
    
    If Not frm_Mensajeria.Visible Then
        
        frm_Mensajeria.Mensaje.Text = frm_Mensajeria.Mensaje.Text & IIf(frm_Mensajeria.Mensaje.Text <> "", " ", "") & Texto
        
        If pVbmodal Then
            If Not frm_Mensajeria.Visible Then
                Retorno = False
                
                frm_Mensajeria.Show vbModal
                
                Set frm_Mensajeria = Nothing
            End If
        Else
            Retorno = False
            
            frm_Mensajeria.Show
            
            frm_Mensajeria.aceptar.Enabled = False
            
            Set frm_Mensajeria = Nothing
        End If
        
    End If
    
    Mensaje = Retorno
    
Falla_Local:

End Function

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
            
            Case FBD_Fecha
                
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
                
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
                
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
                
                FechaBD = Format(Expression, "HH:mm")
                
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

Public Function EndOfDay(Optional ByVal pFecha) As Date
    If IsMissing(pFecha) Then pFecha = Now
    pFecha = SDate(CDate(pFecha))
    EndOfDay = DateAdd("h", 23, DateAdd("n", 59, DateAdd("s", 59, pFecha)))
End Function

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    If Not Objeto Is Nothing Then
        If OrMode Then
            PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
        Else
            PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
        End If
    End If
End Function

Public Function StellarMensaje(pResourceID As Long) As String
    StellarMensaje = Stellar_Mensaje(pResourceID, True)
End Function

Public Function Stellar_Mensaje(Mensaje As Long, Optional pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    Texto = LoadResString(Mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje = Texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje = "CHK_RES"
    
End Function

Public Function NO_CONSECUTIVO(Campo_Cons As String, Optional pAvance As Boolean = True, Optional pConexion As Object = Nothing) As String
    
    On Error GoTo Falla_Local
    
    'Debug.Print pConexion
            
    'If pConexion Is Nothing Then
        'NO_CONSECUTIVO = gClsDatos.NO_CONSECUTIVO(gCodProducto, Ent.BDD, Campo_Cons, pAvance)
    'Else
        'NO_CONSECUTIVO = gClsDatos.NO_CONSECUTIVO(gCodProducto, pConexion, Campo_Cons, pAvance)
    'End If
    
    ' Cambiar Validaci�n del Recsun? Aprobado.
    
    If pConexion Is Nothing Then
        NO_CONSECUTIVO = ClsDatos_NO_CONSECUTIVO(gCodProducto, Ent.BDD, Campo_Cons, pAvance)
    Else
        NO_CONSECUTIVO = ClsDatos_NO_CONSECUTIVO(gCodProducto, pConexion, Campo_Cons, pAvance)
    End If
    
    If Trim(NO_CONSECUTIVO) = Empty Then
        GoTo Falla_Local
    End If
    
    Exit Function
    
Falla_Local:
    
    Mensaje True, "Ocurri� un error en los correlativos. Se cerrar� la aplicaci�n. Reporte lo siguiente: " & Err.Description
    
    'End
    
End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function ClsDatos_NO_CONSECUTIVO(ByVal pCodProducto, pConexion As Object, ByVal pCampo As String, ByVal pAvance As Boolean) As String
    
    On Error GoTo Error
    
    Dim mSQL, mRs, SetDemo, ObjPers
    
    mSQL = "SELECT TOP(1) Nom_Org, Ser_Sof, Key_Sof FROM ESTRUC_SIS"
    
    Set mRs = pConexion.Execute(mSQL)
    
    If Not mRs.EOF Then
        
        Dim mOrganizacion As String, mSerial As String, mKey As String, mRegEx As New RegExp
        
        Set DLLReg = SafeCreateObject("DLLEstatusProducto.ClsReg")
        
        mOrganizacion = Trim((mRs!nom_org))
        mSerial = Trim((mRs!Ser_Sof))
        mKey = Trim((mRs!Key_Sof))
        
        'mOrganizacion = "BLABLAbla, C.A.;   (COMPANY&ASSOCIATED@ONLINE.COM)_ALGO! $$$, :��� 2+3=1-2 | []" ' Testing RegExp
        
        mRegEx.Global = True: mRegEx.Pattern = "[^a-zA-Z0-9������������\.,'()\-_& ]": mOrganizacion = mRegEx.Replace(mOrganizacion, vbNullString)
        mRegEx.Global = True: mRegEx.Pattern = "[^a-zA-Z0-9������������\.,'()\-_& ]": mSerial = mRegEx.Replace(mSerial, vbNullString)
        mRegEx.Global = True: mRegEx.Pattern = "[^a-zA-Z0-9������������\.,'()\-_& ]": mKey = mRegEx.Replace(mKey, vbNullString)
        
        If Not DLLReg Is Nothing Then
            SetDemo = DLLReg.VerificarEstatusProducto( _
            mOrganizacion, mSerial, mKey, CLng(pCodProducto)) = False
        Else
            'ClsDatos_NO_CONSECUTIVO = gClsDatos.NO_CONSECUTIVO(gCodProducto, pConexion, pCampo, pAvance)
            Exit Function
        End If
        
    Else
        SetDemo = True
    End If
    
    If SetDemo And UCase(mRs!Ser_Sof) <> UCase("Demo") Then
        
        ' LOG OLD VALUES
            
        pConexion.Execute "IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ESTRUC_SIS_TMP_LOG') BEGIN CREATE TABLE [dbo].[ESTRUC_SIS_TMP_LOG]([ID] [int] IDENTITY(1,1) NOT NULL, [INFO] [nvarchar](max) NOT NULL, [FECHA] DATETIME NOT NULL DEFAULT (GETDATE()), CONSTRAINT [PK_ESTRUC_SIS_TMP_LOG] PRIMARY KEY CLUSTERED ( [ID] ASC ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] END; INSERT INTO ESTRUC_SIS_TMP_LOG (INFO) (SELECT client_net_address + '|' + HOST_NAME() + '|' + APP_NAME() + '|' + '" & mOrganizacion & "|" & mSerial & "|" & mKey & "|" & pCodProducto & "|PreCorrelativo|ORIGINAL:" & CStr(mRs!nom_org) & CStr(mRs!Ser_Sof) & CStr(mRs!Key_Sof) & "' From sys.dm_exec_connections WHERE Session_id = @@SPID)"
        
        IniciarDEMO pConexion
        
        Set mRs = pConexion.Execute(mSQL)
        
        pConexion.Execute "IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ESTRUC_SIS_TMP_LOG') BEGIN CREATE TABLE [dbo].[ESTRUC_SIS_TMP_LOG]([ID] [int] IDENTITY(1,1) NOT NULL, [INFO] [nvarchar](max) NOT NULL, [FECHA] DATETIME NOT NULL DEFAULT (GETDATE()), CONSTRAINT [PK_ESTRUC_SIS_TMP_LOG] PRIMARY KEY CLUSTERED ( [ID] ASC ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] END; INSERT INTO ESTRUC_SIS_TMP_LOG (INFO) (SELECT client_net_address + '|' + HOST_NAME() + '|' + APP_NAME() + '|' + '" & mOrganizacion & "|" & mSerial & "|" & mKey & "|" & pCodProducto & "|PostCorrelativo|ORIGINAL:" & CStr(mRs!nom_org) & CStr(mRs!Ser_Sof) & CStr(mRs!Key_Sof) & "' From sys.dm_exec_connections WHERE Session_id = @@SPID)"
        
    End If
    
    If UCase(mRs!Ser_Sof) = UCase("Demo") Or UCase(mRs!Key_Sof) = UCase("Demo") Then
        
        mSQL = "SELECT SUM(ABS(nu_Valor)) AS nOperaciones FROM MA_CORRELATIVOS" & GetLines & _
        "WHERE CU_Campo IN (" & vbNewLine & _
        "'FACTURA'," & vbNewLine & _
        "'ESPERA_FAC'," & vbNewLine & _
        "'NDE'," & vbNewLine & _
        "'ESPERA_NDE'," & vbNewLine & _
        "'PRESUPUESTO'," & vbNewLine & _
        "'ESPERA_PRE'," & vbNewLine & _
        "'PEDIDO'," & vbNewLine & _
        "'ESPERA_PED'," & vbNewLine & _
        "'FacturaxCierre'," & vbNewLine & _
        "'DevolucionesxCierre'," & vbNewLine & _
        "'COMPRAS'," & vbNewLine & _
        "'ESPERA_COM'," & vbNewLine & _
        "'ORDEN_COMPRA'," & vbNewLine & _
        "'ESPERA_ODC'," & vbNewLine

        mSQL = mSQL & _
        "'CxP_A_P'," & vbNewLine & _
        "'CxP_NotaC'," & vbNewLine & _
        "'CxP_NotaD'," & vbNewLine & _
        "'CxP_Pagos'," & vbNewLine & _
        "'CxC_A_P'," & vbNewLine & _
        "'CxC_NotaC'," & vbNewLine & _
        "'CxC_NotaD'," & vbNewLine & _
        "'CxC_Pagos'," & vbNewLine & _
        "'INVENTARIO'," & vbNewLine & _
        "'INV_FISICO'," & vbNewLine & _
        "'AJUSTES'," & vbNewLine & _
        "'TRASLADOS'," & vbNewLine & _
        "'TRANSFERENCIA'," & vbNewLine & _
        "'RECEPCION'," & vbNewLine & _
        "'BANCO_DEPOSITO'," & vbNewLine & _
        "'BANCO_CREDITO'," & vbNewLine & _
        "'BANCO_DEBITO'," & vbNewLine & _
        "'BANCO_EGRESO'," & vbNewLine & _
        "'BANCO_OPERACION'," & vbNewLine & _
        "'BANCO_ANULADO'," & vbNewLine & _
        "'BANCO_DEBITO'," & vbNewLine & _
        "'BANCO_TRANSFERENCIA'" & vbNewLine & _
        ")"
        
        Set mRs = pConexion.Execute(mSQL)
        
        If mRs!nOperaciones > 1000 Then
            ClsDatos_NO_CONSECUTIVO = "DEMO"
            Exit Function
        End If
        
    End If
    
    mSQL = "SELECT * FROM MA_CORRELATIVOS WHERE cu_Campo = '" & pCampo & "'"
    Set mRs = New Recordset
    
    mRs.Open mSQL, pConexion, adOpenStatic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
    
        If pAvance Then
            'pConexion.Execute ("UPDATE MA_CORRELATIVOS SET nu_Valor = nu_Valor + 1 WHERE cu_Campo = '" & Campo_Cons & "'")
            mRs!nu_valor = mRs!nu_valor + 1
            mRs.Update
        End If
        
        ClsDatos_NO_CONSECUTIVO = Format(mRs!nu_valor, mRs!cu_formato)
        
    End If
    
    Exit Function
    
Error:
    
    Grabar_Errores Err.Number, Err.Description, "9999999999", "Funciones", "ClsDatos_NO_CONSECUTIVO"
    ClsDatos_NO_CONSECUTIVO = vbNullString
    
End Function

Sub Grabar_Errores(ByVal Num As String, ByVal Desc As String, ByVal Usuario As String, ByVal Forma_Name As String, ByVal Quien As String)
    
    Dim lRutinas As New cls_Rutinas
    
    On Error GoTo ERROR_ERROR
    
    'Call Mensaje(True, "Disculpe, por favor comun�quese con soporte t�cnico." & vbNewLine & "REPORTE ESTO: " & vbNewLine & Desc)
    Call Mensaje(True, Replace(Replace(StellarMensaje(405), "$(Line)", GetLines), "$(Msg)", Desc))
    
    Dim RsError As New ADODB.Recordset
    
    Call Apertura_Recordset(RsError)
    
    'RSERROR.Open "ma_errores", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdTable
    
    RsError.Open "SELECT * FROM MA_ERRORES WHERE 1=2", Ent.BDD, adOpenDynamic, adLockBatchOptimistic
    
    With RsError
        .AddNew
        !Numero = Num
        !Descripcion = Desc & " PCNombre: " & lRutinas.NombreDelComputador & " IdProcesador: " & lRutinas.SerialDelProcesador
        !Hora = Time
        !Usuario = Usuario
        !Modulo = path_menu
        !Forma = Forma_Name
        !FECHA = Date
        !Obj_Pro_Fun = Quien
        .UpdateBatch
    End With
    
    Call Cerrar_Recordset(RsError)
    
    Exit Sub
    
ERROR_ERROR:
    
    'Debug.Print Err.Description
    'Call mensaje(True, "Verificar rutina de grabar errores.")
    
End Sub

Private Sub IniciarDEMO(pConexion As Object)

    Dim mRsOrg As New ADODB.Recordset
    Dim mRs As New ADODB.Recordset
    
    SQL = "SELECT * From ESTRUC_SIS"
    
    mRs.Open SQL, pConexion, adOpenKeyset, adLockBatchOptimistic, adCmdText
    
    If Not mRs.EOF Then
        
        ' ESTRUC_SIS
        
        With mRs
            !nom_org = "STELLAR PLATFORM"
            !dir_org = "DEMO"
            !tlf_org = ""
            !loc_org = "DEMO"
            !rif_org = "DEMO"
            !Ser_Sof = "DEMO"
            !nit_org = "DEMO"
            !Key_Sof = vbNullString
            .UpdateBatch
        End With
        
        ' ESTRUC_ORG
        
        SQL = "SELECT TOP (1) Texto FROM ESTRUC_ORG ORDER BY ID"
        
        mRsOrg.Open SQL, pConexion, adOpenKeyset, adLockPessimistic, adCmdText
            mRsOrg!Texto = CStr(mRs!nom_org)
        mRsOrg.UpdateBatch
        
        ' SUCURSALES
        
        pConexion.Execute "UPDATE MA_SUCURSALES SET c_Descripcion = '" & CStr(mRs!nom_org) & "'"
            
    End If
    
    mRs.Close
    
End Sub

Public Sub Apertura_Recordset(Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Rec.CursorLocation = adUseServer
End Sub

Public Sub Cerrar_Recordset(Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Set Rec = Nothing
End Sub

Public Sub Apertura_RecordsetC(ByRef Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Rec.CursorLocation = adUseClient
End Sub

Public Function MsjErrorRapido(ByVal pDescripcion As String, _
Optional ByVal pMsjIntro As String = "[StellarMensaje]")
    
    If pMsjIntro = "[StellarMensaje]" Then
        pMsjIntro = Replace(pMsjIntro, "[StellarMensaje]", StellarMensaje(286)) & GetLines(2) '"Ha ocurrido un error, por favor reporte lo siguiente: " & vbNewLine & vbNewLine
    End If
    
    Mensaje True, pMsjIntro & pDescripcion
    
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
   
   Dim HowManyLines As Integer
   
   HowManyLines = HowMany
   
   For I = 1 To HowManyLines
       GetLines = GetLines & vbNewLine
   Next I
   
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
   
   Dim HowManyTabs As Integer
   
   HowManyTabs = HowMany
   
   For I = 1 To HowManyTabs
       GetTab = GetTab & vbTab
   Next I
   
End Function

Public Function ValidarNumeroIntervalo(ByVal pValor, _
Optional pMax As Variant, _
Optional pMin As Variant) As Variant

    On Error GoTo Err

    If Not IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor > pMax Then
            pValor = pMax
        ElseIf pValor < pMin Then
            pValor = pMin
        End If
    ElseIf Not IsMissing(pMax) And IsMissing(pMin) Then
        If pValor > pMax Then pValor = pMax
    ElseIf IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor < pMin Then pValor = pMin
    End If
    
    ValidarNumeroIntervalo = pValor
    
    Exit Function
    
Err:

    ValidarNumeroIntervalo = pValor

End Function

Public Function GetSysTime(ByVal bReturnUTC As Boolean) As Date
    
    Dim oSYS As SYSTEMTIME
    
    If bReturnUTC Then
       GetSystemTime oSYS
    Else
       GetLocalTime oSYS
    End If
    
    With oSYS
        GetSysTime = DateSerial(.wYear, .wMonth, .wDay) + _
        TimeSerial(.wHour, .wMinute, .wSecond) + _
        .wMilliseconds / 86400000
    End With
        
End Function

Public Function Collection_ExisteKey(pCollection As Collection, pKey As String, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pKey)
    Else
        Set tmpValorObj = pCollection.Item(pKey)
    End If
     
    Collection_ExisteKey = True
     
    Exit Function
        
Err:
    
    'Debug.Print Err.Description
    
    Collection_ExisteKey = False
    
End Function

Public Sub SeleccionarTexto(pControl As Object)
    On Error Resume Next
    pControl.SelStart = 0
    pControl.SelLength = Len(pControl.Text)
End Sub

Public Function ExisteTabla(ByVal pTabla As String, _
pCn As ADODB.Connection, _
Optional ByVal pBD As String = "") As Boolean
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTabla = Not (pRs.EOF And pRs.BOF)
End Function

Public Function BuscarValorBD(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    On Error GoTo Err_Campo
    
    Dim mRs As ADODB.Recordset: Set mRs = New ADODB.Recordset
    
    mRs.Open SQL, IIf(pCn Is Nothing, Ent.BDD, pCn), adOpenStatic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarValorBD = mRs.Fields(Campo).Value
    Else
        BuscarValorBD = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Err_Campo:
    
    'Debug.Print Err.Description
    
    Err.Clear
    BuscarValorBD = pDefault
    
End Function

' Version Estricta de BuscarValorBD. Para cuando el valor a buscar es muy importante _
y no se pueda dar el lujo de reemplazar el dato con valor default por un fallo de conexi�n _
o de otra indole t�cnica. Si se usa esta versi�n sin control de errores, el manejo de errores _
lo debe tener el proceso que llam� a esta funci�n:

Public Function BuscarValorBD_Strict(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    Dim mRs As ADODB.Recordset: Set mRs = New ADODB.Recordset
    
    mRs.Open SQL, IIf(pCn Is Nothing, Ent.BDD, pCn), adOpenStatic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarValorBD_Strict = mRs.Fields(Campo).Value
    Else
        BuscarValorBD_Strict = pDefault
    End If
    
    mRs.Close
    
End Function

Public Sub ActivarMensajeGrande(Optional ByVal pPorcentaje As Integer = 20)
    
    On Error GoTo Falla_Local
    
    Dim PorcWidth As Double, PorcHeight As Double
    
    If pPorcentaje <= 0 Then Exit Sub
    
    If pPorcentaje > 100 Then pPorcentaje = 100
    
    PorcWidth = (1 + (CDbl(pPorcentaje) / 2 / 100))
    PorcHeight = (1 + (CDbl(pPorcentaje) / 100))
    
    With frm_Mensajeria
    
        .Width = .Width * PorcWidth
        .Height = .Height * (1 + (CDbl(pPorcentaje) / 1.4 / 100))
        
        .Frame6.Width = .Frame6.Width * PorcWidth
        .lbl_Organizacion.Width = .Width
        
        .lbl_website.Left = .lbl_website.Left * PorcWidth
        .Exit.Left = .Exit.Left * PorcWidth
        
        .Mensaje.Width = .Mensaje.Width * PorcWidth
        .Mensaje.Height = .Mensaje.Height * PorcHeight
        
        .lblMarginTop.Width = .lblMarginTop.Width * PorcWidth
        .lblMarginTop.Height = .lblMarginTop.Height * PorcHeight
        
        '.btnCopy.Left = .btnCopy.Left * PorcWidth
        .btnCopy.Top = .btnCopy.Top * (1 + (CDbl(pPorcentaje) / 1.25 / 100))
        .btnCopy.Width = .btnCopy.Width * PorcWidth
        .btnCopy.Height = .btnCopy.Height * PorcWidth
        
        .Cancelar.Left = .Cancelar.Left * (1 + (CDbl(pPorcentaje) / 2.25 / 100))
        .Cancelar.Top = .btnCopy.Top
        .Cancelar.Width = .Cancelar.Width * PorcWidth
        .Cancelar.Height = .Cancelar.Height * PorcWidth
        
        .aceptar.Left = .aceptar.Left * (1 + (CDbl(pPorcentaje) / 2.25 / 100))
        .aceptar.Top = .btnCopy.Top
        .aceptar.Width = .aceptar.Width * PorcWidth
        .aceptar.Height = .aceptar.Height * PorcWidth
        
    End With
    
Falla_Local:

End Sub

Public Function BuscarReglaNegocioStr(ByVal pCampo, _
Optional ByVal pDefault As String = "") As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * FROM MA_REGLASDENEGOCIO WHERE Campo = '" & pCampo & "' "
    
    mRs.Open mSQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarReglaNegocioStr = CStr(mRs!Valor)
    Else
        BuscarReglaNegocioStr = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Function BuscarReglaNegocioBoolean(pCampo, Optional pIgualA) As Boolean
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * FROM MA_REGLASDENEGOCIO WHERE Campo = '" & pCampo & "'"
    mRs.Open mSQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarReglaNegocioBoolean = CStr(mRs!Valor) = IIf(Not IsMissing(pIgualA), CStr(pIgualA), CStr(1))
    End If
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Function Rellenar_SpaceR(intValue, intDigits, car)
    '*** ESPACIO A LA DERECHA
    mValorLon = intDigits - Len(intValue)
    Rellenar_SpaceR = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), car)
End Function

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

' Imprime un recordset o el Record Actual utilizando el m�todo original de ADODB.

' El Original de ADODB pero incluyendo los headers.
' Es m�s r�pido pero menos ordenado.
' NO Deja el recordset en su posici�n del cursor original:
' Lo lleva al EOF si es el Recordset, o avanza el cursor una posici�n si es solo el Record Actual.
' Por lo cual habr�a que hacer MoveFirst o MovePrevious respectivamente
' Dependiendo del tipo de recorset para poder volver a recorrerlo.

Public Function QuickPrintRecordset(ByVal pRs As ADODB.Recordset, Optional OnlyCurrentRow As Boolean = False) As String
    
    On Error Resume Next
    
    Dim Data As Boolean
    Dim RsPreview As String
    Dim RowData As String
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    For I = 0 To pRs.Fields.Count - 1
        RsPreview = RsPreview + IIf(I = 0, vbNullString, "|") + pRs.Fields(I).Name
    Next I
    
    RsPreview = RsPreview & vbNewLine
    
    If Data Then
        If OnlyCurrentRow Then
            RowData = pRs.GetString(, 1, "|", vbNewLine, "NULL")
        Else
            RowData = pRs.GetString(, , "|", vbNewLine, "NULL")
        End If
    End If
    
    RsPreview = RsPreview & RowData
    
    QuickPrintRecordset = RsPreview
    
End Function

Public Function QuickPrintRecord(ByVal pRs As ADODB.Recordset) As String
    QuickPrintRecord = QuickPrintRecordset(pRs, True)
End Function

'' Imprime un Recordset o el Record Actual de manera ordenada. Deja al recordset en su posici�n (Record) Original.
'
'' Parametros:
'
'' 1) pRs: Recordset
'' 2) pRowNumAlias: Alias para una columna que enumera los rows. Si no se especifica o OnlyCurrentRow no se imprime
'' 3) OnlyCurrentRow: Imprimir solo el record actual o todo.
'' 4) EncloseHeaders: Encierra los cabeceros en Corchetes para mayor separaci�n.
'' 5) LineBeforeData: Agrega una Linea de separaci�n entre Header y Records.
'' 6) pOnValueError: Que se imprime si da un error a la hora de recuperar un campo.
'' 7) Uso interno: Dejar siempre el valor default. Es para poder imprimir los Recordsets anidados (Tipo Consulta Shape).
'
'' Recomendaci�n al obtener el String devuelto: Copiar y Pegar en TextPad o un Bloc de Notas sin Line Wrap para analizar.
'
'Public Function PrintRecordset(ByVal pRs As ADODB.Recordset, Optional ByVal pRowNumAlias As String = vbNullString, _
'Optional OnlyCurrentRow As Boolean = False, Optional EncloseHeaders As Boolean = True, _
'Optional LineBeforeData As Boolean = False, Optional ByVal pOnValueError As String = "Error|N/A", _
'Optional NestLevel As Byte = 0) As String
'
'    On Error GoTo FuncError
'
'    Dim RsPreview As String, bRowNumAlias As Boolean, RowNumAlias As String, RowNumCount As Double
'    Dim OriginalRsPosition, TmpRsValue, mColLength() As Double, mColArrIndex, pRsValues As ADODB.Recordset
'    Dim Data As Boolean
'
'    If OnlyCurrentRow Then pRowNumAlias = vbNullString
'
'    bRowNumAlias = (pRowNumAlias <> vbNullString)
'
'    Data = Not (pRs.BOF And pRs.EOF)
'
'    ReDim mColLength(pRs.Fields.Count - 1 + IIf(bRowNumAlias, 1, 0))
'
'    If bRowNumAlias Then
'        RowNumAlias = IIf(EncloseHeaders, "[", vbNullString) & CStr(pRowNumAlias) & IIf(EncloseHeaders, "]", vbNullString)
'        RowNumCount = 0
'        RsPreview = RsPreview & GetTab(NestLevel) & "$(mCol[0])"
'        mColLength(0) = Len(RowNumAlias)
'    End If
'
'    For I = 0 To pRs.Fields.Count - 1
'        'RsPreview = RsPreview + pRs.Fields(i).Name + vbTab
'        mColArrIndex = IIf(bRowNumAlias, I + 1, I)
'        RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & "$(mCol[" & mColArrIndex & "])"
'        mColLength(mColArrIndex) = Len(IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(I).Name & IIf(EncloseHeaders, "]", vbNullString))
'    Next I
'
'    If LineBeforeData Then RsPreview = RsPreview & vbNewLine
'
'    If Data And Not OnlyCurrentRow Then
'        OriginalRsPosition = SafeProp(pRs, "Bookmark", -1)
'        If pRs.CursorType <> adOpenForwardOnly Then
'            pRs.MoveFirst
'        Else
'            pRs.Requery
'        End If
'        If OriginalRsPosition <> -1 Then Set pRsValues = pRs.Clone(adLockReadOnly)
'    Else
'        Set pRsValues = pRs
'    End If
'
'    RowNumCount = 0
'
'    Do While Not pRs.EOF
'
'        'RsPreview = RsPreview & vbNewLine
'        'If bRowNumAlias Then RsPreview = RsPreview & Rellenar_SpaceR(RowNumCount, Len(RowNumAlias), " ")
'
'        If bRowNumAlias Then
'            TmpRsValue = RowNumCount
'            If Len(TmpRsValue) > mColLength(0) Then mColLength(0) = Len(TmpRsValue)
'            'RsPreview = RsPreview & TmpRsValue
'        End If
'
'        For I = 0 To pRs.Fields.Count - 1
'            TmpRsValue = isDBNull(SafeItem(pRs, pRs.Fields(I).Name, pOnValueError), "NULL")
'            mColArrIndex = IIf(bRowNumAlias, I + 1, I)
'
'            If UCase(TypeName(TmpRsValue)) <> UCase("Recordset") Then
'                If Len(TmpRsValue) > mColLength(mColArrIndex) Then mColLength(mColArrIndex) = Len(TmpRsValue)
'            End If
'            'RsPreview = RsPreview & TmpRsValue
'        Next I
'
'        RowNumCount = RowNumCount + 1
'
'        If OnlyCurrentRow Then Exit Do
'
'        pRs.MoveNext
'
'    Loop
'
'    If bRowNumAlias Then RsPreview = Replace(RsPreview, "$(mCol[0])", Rellenar_SpaceR(RowNumAlias, mColLength(0), " ") & vbTab, , 1)
'
'    For I = 0 To pRs.Fields.Count - 1
'        mColArrIndex = IIf(bRowNumAlias, I + 1, I)
'        RsPreview = Replace(RsPreview, "$(mCol[" & mColArrIndex & "])", Rellenar_SpaceR( _
'        IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(I).Name & IIf(EncloseHeaders, "]", vbNullString), _
'        mColLength(mColArrIndex), " ") & vbTab, , 1)
'    Next I
'
'    If OriginalRsPosition = -1 And Not OnlyCurrentRow Then
'        Set pRsValues = pRs
'        If Data Then
'            If pRsValues.CursorType <> adOpenForwardOnly Then
'                pRsValues.MoveFirst
'            Else
'                pRsValues.Requery
'            End If
'        End If
'    End If
'
'    RowNumCount = 0
'
'    Do While Not pRsValues.EOF
'
'        RsPreview = RsPreview & GetTab(NestLevel) & vbNewLine
'
'        If bRowNumAlias Then
'            TmpRsValue = RowNumCount
'            RsPreview = RsPreview & GetTab(NestLevel) & Rellenar_SpaceR(TmpRsValue, mColLength(0), " ") & vbTab
'        End If
'
'        For I = 0 To pRsValues.Fields.Count - 1
'            TmpRsValue = isDBNull(SafeItem(pRsValues, pRsValues.Fields(I).Name, pOnValueError), "NULL")
'            mColArrIndex = IIf(bRowNumAlias, I + 1, I)
'            If Not UCase(TypeName(TmpRsValue)) = UCase("Recordset") Then
'                RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & Rellenar_SpaceR(TmpRsValue, mColLength(mColArrIndex), " ") & vbTab
'            Else
'                RsPreview = RsPreview & vbNewLine & PrintRecordset(TmpRsValue, pRowNumAlias, OnlyCurrentRow, EncloseHeaders, LineBeforeData, pOnValueError, NestLevel + 1)
'            End If
'        Next I
'
'        RowNumCount = RowNumCount + 1
'
'        If OnlyCurrentRow Then Exit Do
'
'        pRsValues.MoveNext
'
'    Loop
'
'    PrintRecordset = RsPreview
'
'Finally:
'
'    On Error Resume Next
'
'    If Data And Not OnlyCurrentRow Then
'        If OriginalRsPosition <> -1 Then
'            If pRs.CursorType = adOpenForwardOnly Then
'                pRs.Requery
'                pRs.Move OriginalRsPosition, 0
'            Else
'                SafePropAssign pRs, "Bookmark", OriginalRsPosition
'            End If
'        Else
'            If pRsValues.CursorType <> adOpenForwardOnly Then
'                pRsValues.MoveFirst
'            Else
'                pRsValues.Requery
'            End If
'        End If
'    End If
'
'    Exit Function
'
'FuncError:
'
'    'Resume ' Debug
'
'    PrintRecordset = vbNullString
'
'    Resume Finally
'
'End Function

'' Imprime el Record Actual de un Recordset.

'Public Function PrintRecord(ByVal pRs As ADODB.Recordset, _
Optional EncloseHeaders As Boolean = True, Optional LineBeforeData As Boolean = False, _
Optional ByVal pOnValueError As String = "Error|N/A") As String
    'PrintRecord = PrintRecordset(pRs, , True, EncloseHeaders, LineBeforeData, pOnValueError)
'End Function

' Copia en el portapapeles y Devuelve el string.

Public Function CtrlC(ByVal Text): On Error Resume Next: CtrlC = Text: Clipboard.Clear: Clipboard.SetText CtrlC: End Function

' Devuelve lo que haya en el portapapeles.

Public Function CtrlV() As String: On Error Resume Next: CtrlV = Clipboard.GetText: End Function

Public Function GDate(ByVal pDate) As String: On Error Resume Next: GDate = FormatDateTime(pDate, vbGeneralDate): End Function

' Formatea Fecha Corta seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function SDate(ByVal pDate) As String: On Error Resume Next: SDate = FormatDateTime(pDate, vbShortDate): End Function

' Formatea Hora Corta o Larga si incluye hora, seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function GTime(ByVal pDate) As String: On Error Resume Next: GTime = FormatDateTime(pDate, vbLongTime): End Function

' Formatea Fecha Corta seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function STime(ByVal pDate) As String: On Error Resume Next: STime = FormatDateTime(pDate, vbShortTime): End Function


Public Function QuitarComillasDobles(ByVal pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(ByVal pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

Public Function LimitarComillasSimples(ByVal pCadena As String) As String
    
    Dim Cad As String
    
    Cad = pCadena
    
    Do While (Cad Like "*''*")
        Cad = Replace(Cad, "''", "'")
    Loop
    
    LimitarComillasSimples = Cad
    
End Function

Public Sub ForcedExit()
    On Error GoTo Error
    While (Not Screen.ActiveForm Is Nothing)
        Unload Screen.ActiveForm
    Wend
Error:
End Sub

Public Function ExisteCampoTabla(pCampo As String, Optional pRs, _
Optional pSql As String = "", _
Optional pValidarFecha As Boolean = False, _
Optional pCn As Variant) As Boolean
    
    Dim mAux As Variant
    Dim mRs As New ADODB.Recordset
    
    On Error GoTo Error
    
    If Not IsMissing(pRs) Then
        
        mAux = pRs.Fields(pCampo).Name
        
        If pValidarFecha Then
            mAux = pRs.Fields(pCampo).Value
            ExisteCampoTabla = mAux <> "01/01/1900"
        Else
            ExisteCampoTabla = True
        End If
        
    Else
        
        If IsMissing(pCn) Then
            mRs.Open pSql, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        Else
            mRs.Open pSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
        End If
        
        ExisteCampoTabla = True
        
        mRs.Close
        
    End If
    
    Exit Function
    
Error:
    
    'MsgBox Err.Description
    'Debug.Print Err.Description
    
    Err.Clear
    ExisteCampoTabla = False
    
End Function

Public Function FactorMonedaProducto(ByVal pCodigo As String, pCn As Object) As Double
    FactorMonedaProducto = FrmAppLink.BuscarValorBD_Strict("n_Factor", _
    "SELECT * FROM MA_MONEDAS " & _
    "WHERE c_CodMoneda IN (SELECT TOP 1 c_CodMoneda " & _
    "FROM MA_PRODUCTOS " & _
    "WHERE c_Codigo = '" & QuitarComillasSimples(pCodigo) & "') ", 1, pCn)
End Function

Public Function SplitStringIntoFixedLengthLines(ByVal MaxLineChars As Long, _
ByVal pFullString As String, _
Optional ByVal RemoveLf As Boolean = True)
    
    Dim I As Long, Length As Long, Tmp As String, OverRun As String, RemainingCharsNum As Long, FoundLine As Boolean
    
    I = 1
    Tmp = vbNullString
    OverRun = vbNullString
    Length = Len(pFullString)
    
    While (I < Length)
        
        RemainingCharsNum = Length - I
        
        If RemainingCharsNum > MaxLineChars Then
            Tmp = Mid(pFullString, I, MaxLineChars)
            OverRun = WordWrapLineBySpace(Tmp, MaxLineChars, FoundLine)
        Else
            Tmp = Mid(pFullString, Length - RemainingCharsNum)
            OverRun = vbNullString
        End If
        
        SplitStringIntoFixedLengthLines = SplitStringIntoFixedLengthLines & IIf(SplitStringIntoFixedLengthLines = 0, vbNullString, vbNewLine) & (IIf(FoundLine, Mid(Tmp, 1, Len(Tmp) - 1), Tmp))
        
        I = I + MaxLineChars - Len(OverRun)
        
    Wend
    
    If RemoveLf Then SplitStringIntoFixedLengthLines = Replace(SplitStringIntoFixedLengthLines, vbLf, vbNullString)
    
End Function

Public Function WordWrapLineBySpace(ByRef LineStr As String, MaxLineChars As Long, ByRef FoundLine As Boolean)
    
    Dim WhatIsLeft As String, Index As Long, CalculatedString As String
    
    WhatIsLeft = vbNullString
    
    Index = InStr(LineStr, vbNewLine)
    
    If Index > 0 Then
        
        CalculatedString = Mid(LineStr, 1, Index)
        WhatIsLeft = Mid(LineStr, Index + 1)
        FoundLine = True
        
    Else
        
        FoundLine = False
        
        Index = InStrRev(LineStr, " ")
        
        If Index > 0 Then
            CalculatedString = Mid(LineStr, 1, Index)
            WhatIsLeft = Mid(LineStr, Index + 1)
        Else
            CalculatedString = Mid(LineStr, 1, MaxLineChars)
            WhatIsLeft = Mid(LineStr, MaxLineChars + 1)
        End If
        
    End If
    
    LineStr = CalculatedString
    WordWrapLineBySpace = WhatIsLeft
    
End Function

'Public Function ExecuteSafeSQL(ByVal SQL As String, _
'pCn As ADODB.Connection, _
'Optional ByRef Records = 0, _
'Optional ByVal ReturnsResultSet As Boolean = False) As ADODB.Recordset
'
'    On Error GoTo Error
'
'    If ReturnsResultSet Then
'        Set ExecuteSafeSQL = pCn.Execute(SQL, Records)
'    Else
'        pCn.Execute SQL, Records
'        Set ExecuteSafeSQL = Nothing
'    End If
'
'    Exit Function
'
'Error:
'
'    Records = -1
'
'End Function

Public Function FixTSQL(ByVal pCadena As String) As String
    FixTSQL = Replace(pCadena, "'", "''", , , vbTextCompare)
End Function

Public Function RoundUp(ByVal pNum As Variant, Optional ByVal pDecimals As Long) As Double
    RoundUp = CDbl(FormatNumber(pNum, pDecimals, vbTrue, vbFalse, vbFalse))
End Function

Public Function RoundDecimalUp(ByVal pNum As Variant, Optional ByVal pDecimals As Long) As Variant ' Para Max. Precisi�n.
    RoundDecimalUp = CDec(FormatNumber(pNum, pDecimals, vbTrue, vbFalse, vbFalse))
End Function

Public Function BuscarImpresora(ByVal pImpresora As String) As Boolean
    
    Dim mCont As Integer, mPrinter As Printer, mLastPrinter As Printer
    
    BuscarImpresora = False
    
    Set mLastPrinter = Printer
    
    For Each mPrinter In Printers
        
        If UCase(mPrinter.DeviceName) = UCase(pImpresora) Then
            
            BuscarImpresora = True
            
            Set Printer = mPrinter
            
            Exit Function
            
        End If
        
    Next
    
End Function

Public Function PrinterSecureKillDoc() As Boolean
    On Error GoTo ErrPrinter
    Printer.KillDoc
    PrinterSecureKillDoc = True
    Exit Function
ErrPrinter:
    Err.Clear
End Function
