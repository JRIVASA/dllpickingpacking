Attribute VB_Name = "EnumeracionesVarias"
Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Public Enum TipoTecladoStellar
    TecladoInterno
    DLLTeclado
    WindowsOSK
    WindowsTabletInputPanel
    [TipoTeclado_Count]
End Enum

Public Type SYSTEMTIME
   wYear                As Integer
   wMonth               As Integer
   wDayOfWeek           As Integer
   wDay                 As Integer
   wHour                As Integer
   wMinute              As Integer
   wSecond              As Integer
   wMilliseconds        As Integer
End Type

Public Enum FrmBuscarProducto_Vista
    VistaPrecioCliente
    VistaMarca
End Enum

Public Enum StringContains_TipoBusqueda
    ComienzaCon_Search
    Contiene_Search
    TerminaCon_Search
End Enum

Public Enum Enum_FieldType
    Numerico
    Alfanumerico
End Enum
