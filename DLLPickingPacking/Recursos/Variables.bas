Attribute VB_Name = "Variables"
Global Uno As Boolean
Global Retorno As Boolean
Global Const gCodProducto = 859
Global LcCodUsuario As String
Global LcCodLocalidadUser As String
Global LcUsuarioGlobal As Boolean

Global gRutinas As New cls_Rutinas

Public Forma As Object
Public Titulo As String
Public Tabla As String
Public CampoT As Object
Global Campo_Txt As Object
Global Campo_Lbl As Object

Public Type Entorno
    BDD As Object
    POS As Object
    BDDShape As Object
    POSShape As Object
End Type

Public Ent As Entorno
Public mvarNDEPrinter As String
Public NDEArchivo As String
Public mvarNDELabelPrinter As String
Public NDEArchivoEtiqueta As String

'Public frm_Mensajeria As Object
'Public FrmDateSelector As Object
'Public frmFechaSel As Object
'
'Public frmLeyendaGridStellar  As Object
'Public Frm_Super_Consultas As Object

Public Type VE_DECRETO_2602_ErrObject
    Number                                                  As Variant
    Description                                             As Variant
    Source                                                  As Variant
End Type

Public Type VE_DECRETO_2602_DATA
    VE_DECRETO_2602_Totales                                 As Dictionary
    VE_DECRETO_2602_Productos                               As Dictionary
    VE_DECRETO_2602_Producto_Tmp                            As Dictionary
    VE_DECRETO_2602_TmpObj                                  As Variant
    VE_DECRETO_2602_TmpCodigoCliente                        As String
    VE_DECRETO_2602_PorcDescGeneral                         As Double
    VE_DECRETO_2602_Activo                                  As Boolean
    VE_DECRETO_2602_AlicuotaAplicar                         As Double
    VE_DECRETO_2602_NumeroActual                            As String
    VE_DECRETO_2602_PermitirNaturales                       As Boolean
    VE_DECRETO_2602_PermitirJuridicos                       As Boolean
    VE_DECRETO_2602_AplicaTransaccion                       As Boolean
    VE_DECRETO_2602_ErrorTransaccion                        As VE_DECRETO_2602_ErrObject
    VE_DECRETO_2602_MontoLimite                             As Double
    VE_DECRETO_2602_AlicuotasSegunMonto                     As Collection
    VE_DECRETO_2602_DenominacionesElectronicas              As String
    VE_DECRETO_2602_DenominacionesClausulaIN                As String
    VE_DECRETO_2602_AlicuotaCambiar                         As Double
    VE_DECRETO_2602_CampoMontoComparar                      As Integer
End Type

Global VE_DECRETO_2602 As VE_DECRETO_2602_DATA

Global TR_INV_n_FactorMonedaProd                            As Boolean
'Global Moneda_Cod                                           As String
'Global Moneda_Dec                                           As Integer
'Global Moneda_Fac                                           As Double
'Comentadas. Usar variables de FrmAppLink para acceder a los datos del Business
'FrmAppLink.CodMonedaPref
'FrmAppLink.DesMonedaPref
'FrmAppLink.DecMonedaPref
'FrmAppLink.FacMonedaPref
'FrmAppLink.SimMonedaPref

Global MonedaProd                                           As Object ' Cls_Monedas
Global MonedaDoc                                            As Object
Global MonedaTmp                                            As Object

Global ManejaIGTF_Ventas                                    As Boolean

Global Packing_RequiereEmpacadores                          As Boolean
Global TRA_Consola_RequiereEmpacadores                      As Boolean
