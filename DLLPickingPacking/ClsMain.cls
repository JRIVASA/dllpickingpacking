VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Property Let VE_DECRETO_2602_Activo(ByVal pValor As Boolean)
    VE_DECRETO_2602.VE_DECRETO_2602_Activo = pValor
End Property

Public Property Let VE_DECRETO_2602_AlicuotasSegunMonto(ByRef pValor As Collection)
    Set VE_DECRETO_2602.VE_DECRETO_2602_AlicuotasSegunMonto = pValor
End Property

Public Property Let VE_DECRETO_2602_DenominacionesClausulaIN(ByVal pValor As String)
    VE_DECRETO_2602.VE_DECRETO_2602_DenominacionesClausulaIN = pValor
End Property

Public Property Let VE_DECRETO_2602_DenominacionesElectronicas(ByVal pValor As String)
    VE_DECRETO_2602.VE_DECRETO_2602_DenominacionesElectronicas = pValor
End Property

Public Property Let VE_DECRETO_2602_NumeroActual(ByVal pValor As String)
    VE_DECRETO_2602.VE_DECRETO_2602_NumeroActual = pValor
End Property

Public Property Let VE_DECRETO_2602_PermitirJuridicos(ByVal pValor As Boolean)
    VE_DECRETO_2602.VE_DECRETO_2602_PermitirJuridicos = pValor
End Property

Public Property Let VE_DECRETO_2602_PermitirNaturales(ByVal pValor As Boolean)
    VE_DECRETO_2602.VE_DECRETO_2602_PermitirNaturales = pValor
End Property

Public Property Let VE_DECRETO_2602_AlicuotaCambiar(ByVal pValor As Double)
    VE_DECRETO_2602.VE_DECRETO_2602_AlicuotaCambiar = pValor
End Property

Public Property Let VE_DECRETO_2602_CampoMontoComparar(ByVal pValor As Integer)
    VE_DECRETO_2602.VE_DECRETO_2602_CampoMontoComparar = pValor
End Property

' FrmAppLink Ser� el punto de entrada nuevo para todo lo que la DLL requiera _
utilizar del sistema. Y servira de puente para el intercambio de par�metros Input / Output

Public Property Let Prop_FrmAppLink(ByVal pValue As Object)
    Set FrmAppLink = pValue
End Property

Public Property Let Prop_CallerAppForms(ByVal pValue As Object)
    Set CallerAppForms = pValue
End Property

Public Property Let Prop_CallerAppIcon(ByVal pValue As Object)
    Set CallerAppIcon = pValue
End Property

Public Property Let Prop_CallerAppCaption(ByVal pValue As String)
    CallerAppCaption = pValue
End Property

Public Property Get Prop_TrayClose() As Boolean
    Prop_TrayClose = CerrarDesdeBandeja
End Property

' --------------------------------------------------------------------------------

Public Sub Inicializar(pBDD As Object, pPOS As Object, pBDDShape As Object, _
pPOSShape As Object, pLcCodUsuario As String)
    
    Set Ent.BDD = pBDD
    Set Ent.POS = pPOS
    Set Ent.BDDShape = pBDDShape
    Set Ent.POSShape = pPOSShape
    LcCodUsuario = pLcCodUsuario
    
    ManejaIGTF_Ventas = Val(BuscarReglaNegocioStr( _
    "ManejaImpuesto_IGTF_Ventas", 0)) = 1
    
    Packing_RequiereEmpacadores = Val(BuscarReglaNegocioStr( _
    "Packing_RequiereEmpacadores", 0)) = 1
    
    TRA_Consola_RequiereEmpacadores = Val(BuscarReglaNegocioStr( _
    "TRA_Consola_RequiereEmpacadores", 0)) = 1
    
End Sub

Public Sub PickingPacking(pBDD As Object, pPOS As Object, pBDDShape As Object, _
pPOSShape As Object, pLcCodUsuario As String, _
Optional NDEPrinter As String, _
Optional pNDEArchivo As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    mvarNDEPrinter = NDEPrinter
    NDEArchivo = pNDEArchivo
    FrmPicking.Show vbModal
    Set FrmPicking = Nothing
    
End Sub

Public Sub PickingUser(pBDD As Object, pPOS As Object, pBDDShape As Object, _
pPOSShape As Object, pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    FrmPickingUser.Show vbModal
    Set FrmPickingUser = Nothing
    
End Sub

Public Sub DespachoPedido(pBDD As Object, pPOS As Object, pBDDShape As Object, _
pPOSShape As Object, pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    FrmDespachoPedido.Show vbModal
    Set FrmDespachoPedido = Nothing
    
End Sub

Public Sub PackingUser(pBDD As Object, pPOS As Object, pBDDShape As Object, _
pPOSShape As Object, pLcCodUsuario As String, _
Optional NDEPrinter As String, _
Optional pNDEArchivo As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    'jesus
    mvarNDEPrinter = NDEPrinter
    NDEArchivo = pNDEArchivo
    FrmPackingUser.Show vbModal
    Set FrmPackingUser = Nothing
    
End Sub

Public Sub ConsultarUbicaciones(pBDD As Object, pPOS As Object, pBDDShape As Object, _
pPOSShape As Object, ByVal pLcCodUsuario As String, ByVal pLcLocalidadUser As String, _
ByVal pLcUsuarioGlobal As Boolean)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    LcCodLocalidadUser = pLcLocalidadUser
    LcUsuarioGlobal = pLcUsuarioGlobal
    
    FrmDatosUbicacion.Show vbModal
    Set FrmDatosUbicacion = Nothing
    
End Sub

Public Sub ConsolaGestionTransferencia( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    Dim mCls As New ClsTrans_Gestion
    
    mCls.IniciarInterfazDocumento Ent.BDD, FrmAppLink.GetCodUsuario, _
    FrmAppLink.GetNomUsuario, FrmAppLink.GetNivelUsuario, FrmAppLink.GetCodLocalidadSistema
    
    Set mCls = Nothing
    
End Sub

Public Sub TransferenciaPickingPacking( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    mvarNDEPrinter = FrmAppLink.sGetIni(FrmAppLink.GetSetup, _
    "OPCION_IMP", "ImpresoraPackingNDT", Empty)
    NDEArchivo = FrmAppLink.sGetIni(FrmAppLink.GetSetup, _
    "OPCION_IMP", "RutaArchivoPackingNDT", Empty)
    
    If mvarNDEPrinter = Empty Then
        mvarNDEPrinter = BuscarReglaNegocioStr( _
        "TRA_Consola_ImpresoraPackingNDT", Empty)
    End If
    
    If NDEArchivo = Empty Then
        NDEArchivo = BuscarReglaNegocioStr( _
        "TRA_Consola_RutaArchivoPackingNDT", "C:\RTF\PackingNDT.rtf")
    End If
    
    '
    
    mvarNDELabelPrinter = FrmAppLink.sGetIni(FrmAppLink.GetSetup, _
    "OPCION_IMP", "ImpresoraEtiquetaPackingNDT", Empty)
    NDEArchivoEtiqueta = FrmAppLink.sGetIni(FrmAppLink.GetSetup, _
    "OPCION_IMP", "RutaEtiquetaPackingNDT", Empty)
    
    If mvarNDELabelPrinter = Empty Then
        mvarNDELabelPrinter = BuscarReglaNegocioStr( _
        "TRA_Consola_ImpresoraEtiquetaPackingNDT", Empty)
    End If
    
    If NDEArchivoEtiqueta = Empty Then
        NDEArchivoEtiqueta = BuscarReglaNegocioStr( _
        "TRA_Consola_RutaEtiquetaPackingNDT", "C:\RTF\PackingNDT.txt")
    End If
    
    '
    
    FrmTransferencia_PickingPacking.Show vbModal
    
    Set FrmTransferencia_PickingPacking = Nothing
    
End Sub

Public Sub TransferenciaPickingUser( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    FrmTransferencia_PickingUser.Show vbModal
    
    Set FrmTransferencia_PickingUser = Nothing
    
End Sub

Public Sub TransferenciaPackingUser( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    mvarNDEPrinter = FrmAppLink.sGetIni(FrmAppLink.GetSetup, _
    "OPCION_IMP", "ImpresoraPackingNDT", Empty)
    NDEArchivo = FrmAppLink.sGetIni(FrmAppLink.GetSetup, _
    "OPCION_IMP", "RutaArchivoPackingNDT", Empty)
    
    If mvarNDEPrinter = Empty Then
        mvarNDEPrinter = BuscarReglaNegocioStr( _
        "TRA_Consola_ImpresoraPackingNDT", Empty)
    End If
    
    If NDEArchivo = Empty Then
        NDEArchivo = BuscarReglaNegocioStr( _
        "TRA_Consola_RutaArchivoPackingNDT", "C:\RTF\PackingNDT.rtf")
    End If
    
    '
    
    mvarNDELabelPrinter = FrmAppLink.sGetIni(FrmAppLink.GetSetup, _
    "OPCION_IMP", "ImpresoraEtiquetaPackingNDT", Empty)
    NDEArchivoEtiqueta = FrmAppLink.sGetIni(FrmAppLink.GetSetup, _
    "OPCION_IMP", "RutaEtiquetaPackingNDT", Empty)
    
    If mvarNDELabelPrinter = Empty Then
        mvarNDELabelPrinter = BuscarReglaNegocioStr( _
        "TRA_Consola_ImpresoraEtiquetaPackingNDT", Empty)
    End If
    
    If NDEArchivoEtiqueta = Empty Then
        NDEArchivoEtiqueta = BuscarReglaNegocioStr( _
        "TRA_Consola_RutaEtiquetaPackingNDT", "C:\RTF\PackingNDT.txt")
    End If
    
    '
    
    FrmTransferencia_PackingUser.Show vbModal
    
    Set FrmTransferencia_PackingUser = Nothing
    
End Sub

Public Sub TransferenciaEntregaPedidos( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    FrmTransferencia_EntregaPedido.Show vbModal
    
    Set FrmTransferencia_EntregaPedido = Nothing
    
End Sub

Private Sub Class_Initialize()
    CerrarDesdeBandeja = False
End Sub

Public Sub Rep_SolTra_Picking_Res( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    Dim mCls As New ClsTrans_Gestion
    
    mCls.IniciarInterfazDocumento Ent.BDD, FrmAppLink.GetCodUsuario, _
    FrmAppLink.GetNomUsuario, FrmAppLink.GetNivelUsuario, FrmAppLink.GetCodLocalidadSistema, False
    
    FrmTransferencia_ConsolaGestion.Toolbar1_ButtonMenuClick _
    FrmTransferencia_ConsolaGestion.Toolbar1.Buttons("Reimprimir").ButtonMenus("RFac")
    
    Set FrmTransferencia_ConsolaGestion = Nothing
    
    Set mCls = Nothing
    
End Sub

Public Sub Rep_SolTra_Picking_Det( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    Dim mCls As New ClsTrans_Gestion
    
    mCls.IniciarInterfazDocumento Ent.BDD, FrmAppLink.GetCodUsuario, _
    FrmAppLink.GetNomUsuario, FrmAppLink.GetNivelUsuario, FrmAppLink.GetCodLocalidadSistema, False
    
    FrmTransferencia_ConsolaGestion.Toolbar1_ButtonMenuClick _
    FrmTransferencia_ConsolaGestion.Toolbar1.Buttons("Reimprimir").ButtonMenus("RDet")
    
    Set FrmTransferencia_ConsolaGestion = Nothing
    
    Set mCls = Nothing
    
End Sub

Public Sub Rep_SolTra_Picking_Acum( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    Dim mCls As New ClsTrans_Gestion
    
    mCls.IniciarInterfazDocumento Ent.BDD, FrmAppLink.GetCodUsuario, _
    FrmAppLink.GetNomUsuario, FrmAppLink.GetNivelUsuario, FrmAppLink.GetCodLocalidadSistema, False
    
    FrmTransferencia_ConsolaGestion.Toolbar1_ButtonMenuClick _
    FrmTransferencia_ConsolaGestion.Toolbar1.Buttons("Reimprimir").ButtonMenus("RPicking")
    
    Set FrmTransferencia_ConsolaGestion = Nothing
    
    Set mCls = Nothing
    
End Sub

Public Sub Rep_Tra_Picking_Res( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    Dim mCls As New ClsTrans_Gestion
    
    mCls.IniciarInterfazDocumento Ent.BDD, FrmAppLink.GetCodUsuario, _
    FrmAppLink.GetNomUsuario, FrmAppLink.GetNivelUsuario, FrmAppLink.GetCodLocalidadSistema, False
    
    FrmTransferencia_ConsolaGestion.Toolbar1_ButtonMenuClick _
    FrmTransferencia_ConsolaGestion.Toolbar1.Buttons("Reimprimir").ButtonMenus("PickingResFac")
    
    Set FrmTransferencia_ConsolaGestion = Nothing
    
    Set mCls = Nothing
    
End Sub

Public Sub Rep_Tra_Picking_Det( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    Dim mCls As New ClsTrans_Gestion
    
    mCls.IniciarInterfazDocumento Ent.BDD, FrmAppLink.GetCodUsuario, _
    FrmAppLink.GetNomUsuario, FrmAppLink.GetNivelUsuario, FrmAppLink.GetCodLocalidadSistema, False
    
    FrmTransferencia_ConsolaGestion.Toolbar1_ButtonMenuClick _
    FrmTransferencia_ConsolaGestion.Toolbar1.Buttons("Reimprimir").ButtonMenus("PickingDetFac")
    
    Set FrmTransferencia_ConsolaGestion = Nothing
    
    Set mCls = Nothing
    
End Sub

Public Sub Rep_Tra_Picking_Agrupado( _
pBDD As Object, pPOS As Object, _
pBDDShape As Object, pPOSShape As Object, _
pLcCodUsuario As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    Dim mCls As New ClsTrans_Gestion
    
    mCls.IniciarInterfazDocumento Ent.BDD, FrmAppLink.GetCodUsuario, _
    FrmAppLink.GetNomUsuario, FrmAppLink.GetNivelUsuario, FrmAppLink.GetCodLocalidadSistema, False
    
    FrmTransferencia_ConsolaGestion.Toolbar1_ButtonMenuClick _
    FrmTransferencia_ConsolaGestion.Toolbar1.Buttons("Reimprimir").ButtonMenus("LT_CON_TRA_X_LOC")
    
    Set FrmTransferencia_ConsolaGestion = Nothing
    
    Set mCls = Nothing
    
End Sub

Public Sub Imprimir_Lote_Consola_Transferencia( _
pBDD As Object, pPOS As Object, pBDDShape As Object, _
pPOSShape As Object, pLcCodUsuario As String, _
Documento As String, CodLocalidadOrigen As String, _
Estatus As String, Dispositivo As Integer, _
Optional TipoImpresion As String)
    
    Call Inicializar(pBDD, pPOS, pBDDShape, pPOSShape, pLcCodUsuario)
    
    On Error GoTo Error
    
    Dim RsEmpresa As New ADODB.Recordset
    Dim RsCompras As New ADODB.Recordset
    
    Dim mRsMaRequisicion As New ADODB.Recordset, _
    mRsTrRequisicion As New ADODB.Recordset, _
    RsDocumento As New ADODB.Recordset
    
    Dim mClsRutinas As New cls_Rutinas
    
    'Dim fSeguridadReglasdeNegocio As New cls_Seg_RegdeNegocio
    
    Dim mFormaTrabajo As Integer
    
    mFormaTrabajo = SVal(BuscarReglaNegocioStr("Trf_FormaDeTrabajo")) 'fSeguridadReglasdeNegocio.Req_FormadeTrabajo
    
    Call mClsRutinas.Apertura_Recordset(mRsMaRequisicion, adUseClient)
    Call mClsRutinas.Apertura_Recordset(mRsTrRequisicion, adUseClient)
    
    mRsMaRequisicion.Open _
    "SELECT MA.*, isNULL(ORIGEN.c_Descripcion, 'N/A') AS OrigenDes, " & _
    "isNULL(DESTINO.c_Descripcion, 'N/A') AS DestinoDes, " & _
    "isNULL(DESTINO.c_Direccion, '') AS DireccionDes, " & _
    "isNULL(DESTINO.c_Telefono, '') AS TelefonoDes, " & _
    "isNULL(DESTINO.c_Ciudad, '') AS CiudadDes, " & _
    "isNULL(DESTINO.c_Rif, '') AS RifDes, " & _
    "isNULL(DESTINO.c_Nombre_Cheque, '') AS RazonSocialDes, " & _
    "isNULL(DESTINO.c_Gerente, '') AS GerenteDes, " & _
    "isNULL(DESTINO.c_SubGerebte, '') AS SubGerenteDes, " & _
    "isNULL(TRP.c_Descripcion, 'N/A') AS Transporte, " & _
    "'" & FrmAppLink.GetCodLocalidadSistema & "' AS CodLocalidadDestino " & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA_DESTINO MA " & _
    "LEFT JOIN MA_SUCURSALES AS ORIGEN " & _
    "ON ORIGEN.c_Codigo = MA.cs_CodLocalidad " & _
    "LEFT JOIN MA_SUCURSALES AS DESTINO " & _
    "ON DESTINO.c_Codigo = '" & FrmAppLink.GetCodLocalidadSistema & "' " & _
    "LEFT JOIN MA_TRANSPORTE TRP " & _
    "ON TRP.c_CodTransporte = MA.c_Transporte " & _
    "WHERE MA.cs_Corrida = '" & Documento & "' " & _
    "AND MA.cs_CodLocalidad = '" & CodLocalidadOrigen & "' " & _
    "AND MA.c_Status = '" & Estatus & "' ", _
    Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
    
    Dim mCampoCantiBul As String
    
    mCampoCantiBul = "(CASE WHEN TR.CantXEmp > 0 THEN TR.CantXEmp " & _
    "ELSE CASE WHEN PRO.n_CantiBul > 0 THEN PRO.n_CantiBul ELSE 1 END END)"
    
    Dim mSQL As String
    
    mSQL = "SELECT " & mCampoCantiBul & " AS CantiBul, " & _
    "ROUND(TR.CantSolicitada / " & mCampoCantiBul & ", 0, 1) AS EmpSol, " & _
    "ROUND(((TR.CantSolicitada / " & mCampoCantiBul & ") - " & _
    "ROUND(TR.CantSolicitada / " & mCampoCantiBul & ", 0, 1)) * " & mCampoCantiBul & ", PRO.Cant_Decimales, 0) AS UndSol, " & _
    "ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1) AS EmpTra, " & _
    "ROUND(((TR.CantTransferencia / " & mCampoCantiBul & ") - " & _
    "ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1)) * " & mCampoCantiBul & ", PRO.Cant_Decimales, 0) AS UndTra, " & _
    "TR.*, PRO.Cant_Decimales, PRO.n_Peso, PRO.n_Volumen, PRO.n_PesoBul, PRO.n_VolBul, " & _
    "PRO.n_CostoAct, PRO.n_CostoRep, PRO.n_CostoPro, PRO.n_CostoAnt, " & _
    "((ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1) * PRO.n_PesoBul) + " & _
    "((ROUND(((TR.CantTransferencia / " & mCampoCantiBul & ") - " & _
    "ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1)) * " & mCampoCantiBul & ", PRO.Cant_Decimales, 0)) * PRO.n_Peso)) AS Peso, " & _
    "((ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1) * PRO.n_VolBul) + " & _
    "((ROUND(((TR.CantTransferencia / " & mCampoCantiBul & ") - " & _
    "ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1)) * " & mCampoCantiBul & ", PRO.Cant_Decimales, 0)) * PRO.n_Volumen)) AS Volumen, " & _
    "(TR.CantTransferencia * PRO.n_CostoAct * MON.n_Factor) AS Total, " & _
    "(PRO.n_CostoAct * MON.n_Factor) As CostoActFactorizado, " & _
    "PRO.c_Modelo AS c_Modelo, PRO.c_Marca AS c_Marca, PRO.c_Descri, " & _
    "isNULL(EDI.c_Codigo, TR.CodProducto) AS cs_CodEDI "
    
    mSQL = mSQL & _
    "FROM TR_LOTE_GESTION_TRANSFERENCIA_DESTINO_DETALLE TR " & _
    "INNER JOIN MA_PRODUCTOS PRO " & _
    "ON PRO.c_Codigo = TR.CodProducto " & _
    "LEFT JOIN MA_MONEDAS MON " & _
    "ON PRO.c_CodMoneda = MON.c_CodMoneda " & _
    "LEFT JOIN MA_CODIGOS EDI " & _
    "ON EDI.c_CodNasa = TR.CodProducto " & _
    "AND EDI.nu_Intercambio = 1 " & _
    "WHERE TR.cs_Corrida = '" & Documento & "' " & _
    "AND TR.cs_CodLocalidad = '" & CodLocalidadOrigen & "' " & _
    "ORDER BY TR.Linea "
    
    mRsTrRequisicion.Open mSQL, Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
    
    'Debug.Print mRsTrRequisicion.Source
    
    If mRsMaRequisicion.EOF Then
        mRsMaRequisicion.Close
        Exit Sub
    End If
    
    'Nombre_Usuario = FrmAppLink.Buscar_Usuario(mRsMaRequisicion!c_Usuario)
    
    'Titulo = "* R E Q U I S I C I O N *"
    Titulo = FrmAppLink.StellarMensaje(4065)
    
    SQL = "SELECT Clave, Texto, Ruta, SeImprime " & _
    "FROM ESTRUC_REPORT WHERE Clave = 'LT_TRA' "
    
    Call Apertura_RecordsetC(RsDocumento)
    RsDocumento.Open SQL, Ent.BDD, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not RsDocumento.EOF Then
        ImpXPlantilla = RsDocumento.Fields!SeImprime
        'ImpXPlantilla = True ' Debug
    Else
        ImpXPlantilla = False
    End If
    
    Dim Rep As Object
    
    If ImpXPlantilla Then
        Set Rep = FrmAppLink.ClaseReporteRTF 'New Reporte
        Rep.Configurar_Documentos "LT_CON_TRA"
        Rep.AddNewMaster
    Else
        AnuladaRegladeJuego = FrmAppLink.RsReglasComerciales!Factura_Encabezado
    End If
    
    If Not mRsMaRequisicion.EOF Then
        
        '*************************************
        '*** DATOS DE LA EMPRESA
        '*************************************
        
        Call Apertura_Recordset(RsEmpresa)
        
        RsEmpresa.Open "ESTRUC_SIS", Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdTable
        
        If ImpXPlantilla Then
            
            Rep.FillMaster "H1077", TipoImpresion
            
            Rep.FillMaster "H1001", IIf(Len(RsEmpresa!Nom_Org) > 1, RsEmpresa!Nom_Org, Empty)
            Rep.FillMaster "H1002", IIf(Len(RsEmpresa!Dir_Org) > 1, RsEmpresa!Dir_Org, Empty)
            Rep.FillMaster "H1003", IIf(Len(RsEmpresa!Tlf_Org) > 1, RsEmpresa!Tlf_Org, Empty)
            
        Else
            
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("lblTipoImpresion").Caption = TipoImpresion
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_EMPRESA").Caption = RsEmpresa!Nom_Org
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_DIRECCION").Caption = RsEmpresa!Dir_Org
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_TELEFONOS").Caption = "TELEFONOS:" & RsEmpresa!Tlf_Org
            
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("Etiqueta1").Caption = "LOCALIDAD ORIGEN: "
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("Etiqueta4").Caption = "LOCALIDAD A DESPACHAR: "
            
            '"LOTE DE CONSOLA DE TRANSFERENCIAS No "
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("QUE_DOCUMENTO").Caption = FrmAppLink.StellarMensaje(4065) & " No. "
            Documento_Lote_Consola_Transferencia.Caption = FrmAppLink.StellarMensaje(4065)
            
        End If
        
        Call Cerrar_Recordset(RsEmpresa)
        
        Call Apertura_RecordsetC(RsCompras)
        
        RsCompras.Open _
        "SELECT * FROM TR_LOTE_GESTION_TRANSFERENCIA_DESTINO " & _
        "WHERE cs_Corrida = '" & Documento & "' " & _
        "AND cs_CodLocalidad = '" & CodLocalidadOrigen & "' ", _
        Ent.BDD, adOpenStatic, adLockReadOnly, adCmdText
        
        Dim mSolicitudes As String
        
        mSolicitudes = Empty
        
        Do While Not RsCompras.EOF
            
            mSolicitudes = mSolicitudes & ", " & RsCompras!cs_Solicitud
            
            RsCompras.MoveNext
            
        Loop
        
        Call Cerrar_Recordset(RsCompras)
        
        mSolicitudes = Mid(mSolicitudes, 3)
        
        '*************************************
        '*** DATOS DEL DOCUMENTO
        '*************************************
        
        If ImpXPlantilla Then
            
            Rep.FillMaster "H1010", mRsMaRequisicion!cs_Corrida
            Rep.FillMaster "H1011-1", mRsMaRequisicion!cs_CodLocalidad
            Rep.FillMaster "H1011-2", mRsMaRequisicion!OrigenDes
            Rep.FillMaster "H1012-1", GDate(mRsMaRequisicion!d_FechaCorrida)
            Rep.FillMaster "H1012-2", SDate(mRsMaRequisicion!d_FechaCorrida)
            Rep.FillMaster "H1012-3", GTime(mRsMaRequisicion!d_FechaCorrida)
            Rep.FillMaster "H1012-4", STime(mRsMaRequisicion!d_FechaCorrida)
            Rep.FillMaster "H1013-1", mRsMaRequisicion!c_Transporte
            Rep.FillMaster "H1013-2", mRsMaRequisicion!Transporte
            
            Rep.FillMaster "H1014", mRsMaRequisicion!c_Observacion
            
            mLongitud = 0 'Val(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "40"))
            If mLongitud = 0 Then mLongitud = 40
            
            mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, _
            mRsMaRequisicion!c_Observacion & " ", False), vbNewLine)
            
            For i = 0 To 4
                
                mVariable = "H1014-" & Format(i + 1, "#")
                
                If i <= UBound(mCadena) Then
                    Rep.FillMaster CStr(mVariable), CStr(mCadena(i))
                Else
                    Rep.FillMaster CStr(mVariable), vbNullString
                End If
                
            Next i
            
            Rep.FillMaster "H1015", mRsMaRequisicion!c_Clasificacion
            
            mLongitud = 0 'Val(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "40"))
            If mLongitud = 0 Then mLongitud = 40
            
            mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, _
            mRsMaRequisicion!c_Clasificacion & " ", False), vbNewLine)
            
            For i = 0 To 4
                
                mVariable = "H1015-" & Format(i + 1, "#")
                
                If i <= UBound(mCadena) Then
                    Rep.FillMaster CStr(mVariable), CStr(mCadena(i))
                Else
                    Rep.FillMaster CStr(mVariable), vbNullString
                End If
                
            Next i
            
            Rep.FillMaster "H1016", mSolicitudes
            
            mLongitud = 0 'Val(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "40"))
            If mLongitud = 0 Then mLongitud = 40
            
            mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, _
            mSolicitudes & " ", False), vbNewLine)
            
            For i = 0 To 4
                
                mVariable = "H1016-" & Format(i + 1, "#")
                
                If i <= UBound(mCadena) Then
                    Rep.FillMaster CStr(mVariable), CStr(mCadena(i))
                Else
                    Rep.FillMaster CStr(mVariable), vbNullString
                End If
                
            Next i
            
            Rep.FillMaster "H1017-1", mRsMaRequisicion!CodLocalidadDestino
            Rep.FillMaster "H1017-2", mRsMaRequisicion!DestinoDes
            Rep.FillMaster "H1017-3", mRsMaRequisicion!DireccionDes
            Rep.FillMaster "H1017-4", mRsMaRequisicion!TelefonoDes
            Rep.FillMaster "H1017-5", mRsMaRequisicion!CiudadDes
            Rep.FillMaster "H1017-6", mRsMaRequisicion!RifDes
            Rep.FillMaster "H1017-7", mRsMaRequisicion!RazonSocialDes
            Rep.FillMaster "H1017-8", mRsMaRequisicion!GerenteDes
            Rep.FillMaster "H1017-9", mRsMaRequisicion!SubGerenteDes
            
            ' H1018
            
            mLongitud = 0 'Val(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "40"))
            If mLongitud = 0 Then mLongitud = 40
            
            mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, _
            mRsMaRequisicion!DireccionDes & " ", False), vbNewLine)
            
            For i = 0 To 5
                
                mVariable = "H1018-" & Format(i + 1, "#")
                
                If i <= UBound(mCadena) Then
                    Rep.FillMaster CStr(mVariable), CStr(mCadena(i))
                Else
                    Rep.FillMaster CStr(mVariable), vbNullString
                End If
                
            Next i
            
        Else
            
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_DOCUMENTO").Caption = Trim(mRsMaRequisicion!cs_Corrida)
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_FECHA").Caption = SDate(mRsMaRequisicion!d_FechaCorrida)
            
            '*************************************
            '*** DATOS DE LA LOCALIDAD
            '*************************************
            
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_DIRECCION_ORIGEN").Caption = mRsMaRequisicion!OrigenDes
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_DIRECCION_DESTINO").Caption = mRsMaRequisicion!DestinoDes
            
            '*************************************
            '*** DATOS DEL PIE DE PAGINA
            '*************************************
            
            Documento_Lote_Consola_Transferencia.Sections("PIE_INF").Controls("lbl_Solicitudes").Caption = mSolicitudes
            Documento_Lote_Consola_Transferencia.Sections("PIE_INF").Controls("LBL_OBSERVACION").Caption = _
            IIf(IsNull(mRsMaRequisicion!c_Observacion), Empty, mRsMaRequisicion!c_Observacion)
            
            '********** usuario *******************
            Documento_Lote_Consola_Transferencia.Sections("PIE_PAG").Controls("usuario").Caption = Empty 'Nombre_Usuario
            '********** ***************************
            
        End If
        
'        Select Case mFormaTrabajo
'            Case 0
'                Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("lblCantibul").Visible = False
'                Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("lblEmpq").Visible = False
'                Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("lblDescri").Width = 4150
'                Documento_Lote_Consola_Transferencia.Sections("DETALLE").Controls("txtCantibul").Visible = False
'                Documento_Lote_Consola_Transferencia.Sections("DETALLE").Controls("txtEmpq").Visible = False
'                Documento_Lote_Consola_Transferencia.Sections("DETALLE").Controls("txtUnid").DataField = "ns_Cantidad"
'        End Select
        
        '*************************************
        '*** DETALLE DE LA ORDEN
        '*************************************
        
        Dim Total_CantSol As Double, Total_CantTra As Double, Total_DifCant As Double
        Dim Total_EmpSol As Double, Total_UndSol As Double, Total_EmpTra As Double, _
        Total_UndTra As Double, Total_DifEmp As Double, Total_DifUnd As Double
        Dim Total_Peso As Double, Total_Volumen As Double, Total_Costo As Double, CantLineas As Long
        
        If Not mRsTrRequisicion.EOF Then
            
            If ImpXPlantilla Then
                
                While Not mRsTrRequisicion.EOF
                    
                    If ImpXPlantilla Then
                        
                        CantLineas = CantLineas + 1
                        
                        Rep.AddNewDetail
                        
                        Dim CantSol As Double, CantTra As Double, DifCant As Double
                        Dim EmpSol As Double, UndSol As Double, EmpTra As Double, _
                        UndTra As Double, DifEmp As Double, DifUnd As Double
                        Dim Peso As Double, Volumen As Double, Costo As Double
                        
                        CantSol = mRsTrRequisicion!CantSolicitada
                        CantTra = mRsTrRequisicion!CantTransferencia
                        DifCant = CDec(CantTra) - CDec(CantSol)
                        EmpSol = mRsTrRequisicion!EmpSol
                        UndSol = mRsTrRequisicion!UndSol
                        EmpTra = mRsTrRequisicion!EmpTra
                        UndTra = mRsTrRequisicion!UndTra
                        DifEmp = CDec(EmpTra) - CDec(EmpSol)
                        DifUnd = CDec(UndTra) - CDec(UndSol)
                        Peso = mRsTrRequisicion!Peso
                        Volumen = mRsTrRequisicion!Volumen
                        Costo = mRsTrRequisicion!Total
                        
                        Total_CantSol = Total_CantSol + CantSol
                        Total_CantTra = Total_CantTra + CantTra
                        Total_DifCant = CDec(Total_DifCant) + CDec(DifCant)
                        Total_EmpSol = Total_EmpSol + EmpSol
                        Total_UndSol = Total_UndSol + UndSol
                        Total_EmpTra = Total_EmpTra + EmpTra
                        Total_UndTra = Total_UndTra + UndTra
                        Total_DifEmp = CDec(Total_DifEmp) + CDec(DifEmp)
                        Total_DifUnd = CDec(Total_DifUnd) + CDec(DifUnd)
                        Total_Peso = Total_Peso + Peso
                        Total_Volumen = Total_Volumen + Volumen
                        Total_Costo = Total_Costo + Costo
                        
                        Rep.FillDetails "D1001", mRsTrRequisicion!CodProducto
                        Rep.FillDetails "D1002", mRsTrRequisicion!c_Descri
                        Rep.FillDetails "D1003", FormatoDecimalesDinamicos(CantSol)
                        Rep.FillDetails "D1004", FormatoDecimalesDinamicos(CantTra)
                        Rep.FillDetails "D1005", FormatoDecimalesDinamicos(DifCant)
                        Rep.FillDetails "D1006", mRsTrRequisicion!c_Marca
                        Rep.FillDetails "D1007", mRsTrRequisicion!c_Modelo
                        Rep.FillDetails "D1008", mRsTrRequisicion!cs_CodEdi
                        Rep.FillDetails "D1009", FormatoDecimalesDinamicos(EmpSol)
                        Rep.FillDetails "D1010", FormatoDecimalesDinamicos(UndSol)
                        Rep.FillDetails "D1011", FormatoDecimalesDinamicos(EmpTra)
                        Rep.FillDetails "D1012", FormatoDecimalesDinamicos(UndTra)
                        Rep.FillDetails "D1013", mRsTrRequisicion!CantiBul
                        Rep.FillDetails "D1014", FormatoDecimalesDinamicos(DifEmp)
                        Rep.FillDetails "D1015", FormatoDecimalesDinamicos(DifUnd)
                        Rep.FillDetails "D1016", FormatoDecimalesDinamicos(Peso)
                        Rep.FillDetails "D1017", FormatoDecimalesDinamicos(Volumen)
                        Rep.FillDetails "D1018", FormatoDecimalesDinamicos(mRsTrRequisicion!n_Peso)
                        Rep.FillDetails "D1019", FormatoDecimalesDinamicos(mRsTrRequisicion!n_Volumen)
                        Rep.FillDetails "D1020", FormatoDecimalesDinamicos(mRsTrRequisicion!n_PesoBul)
                        Rep.FillDetails "D1021", FormatoDecimalesDinamicos(mRsTrRequisicion!n_VolBul)
                        Rep.FillDetails "D1022", FormatoDecimalesDinamicos(mRsTrRequisicion!Cant_Decimales)
                        Rep.FillDetails "D1023", FormatoDecimalesDinamicos(mRsTrRequisicion!CostoActFactorizado)
                        Rep.FillDetails "D1024", FormatoDecimalesDinamicos(Costo)
                        Rep.FillDetails "D1025", IIf(DifCant <> 0, "(**)", Empty) ' Indicador de Diferencia
                        Rep.FillDetails "D1026", FormatoDecimalesDinamicos(CantLineas)
                        
                    End If
                    
                    mRsTrRequisicion.MoveNext
                    
                Wend
                
                Rep.FillMaster "H1020", FormatoDecimalesDinamicos(Total_CantSol, 0, 4)
                Rep.FillMaster "H1021", FormatoDecimalesDinamicos(Total_CantTra, 0, 4)
                Rep.FillMaster "H1022", FormatoDecimalesDinamicos(Total_DifCant, 0, 4)
                
                Rep.FillMaster "H1023", FormatoDecimalesDinamicos(Total_EmpSol, 0, 4)
                Rep.FillMaster "H1024", FormatoDecimalesDinamicos(Total_UndSol, 0, 4)
                Rep.FillMaster "H1025", FormatoDecimalesDinamicos(Total_EmpTra, 0, 4)
                Rep.FillMaster "H1026", FormatoDecimalesDinamicos(Total_UndTra, 0, 4)
                
                Rep.FillMaster "H1027", FormatoDecimalesDinamicos(Total_DifEmp, 0, 4)
                Rep.FillMaster "H1028", FormatoDecimalesDinamicos(Total_DifUnd, 0, 4)
                
                Rep.FillMaster "H1029", FormatoDecimalesDinamicos(Total_Peso, 0, 4)
                Rep.FillMaster "H1030", FormatoDecimalesDinamicos(Total_Volumen, 0, 4)
                Rep.FillMaster "H1031", FormatoDecimalesDinamicos(Total_Costo, 0, 2)
                
                Rep.FillMaster "H1032", FormatoDecimalesDinamicos(CantLineas)
                
            End If
            
            If ImpXPlantilla Then
                
                Set ShowRTF = FrmAppLink.GetFrmShowRTF
                FrmAppLink.SetRepObjRTF Rep
                
                If Dispositivo = 0 Then
                    ShowRTF.LoadFile RsDocumento.Fields!Ruta
                    ShowRTF.Show vbModal
                Else
                    ShowRTF.LoadFile RsDocumento.Fields!Ruta
                    ShowRTF.PrintAllPage
                End If
                
                FrmAppLink.SetRepObjRTF FrmAppLink.ClaseReporteRTF
                Set Rep = Nothing
                Set ShowRTF = Nothing
                
            Else
                
                Set Documento_Lote_Consola_Transferencia.DataSource = mRsTrRequisicion
                
                'If SolicitudTransferencia_ReporteCodigoEDI Then
                    Documento_Lote_Consola_Transferencia.Sections("Detalle").Controls("Texto2").DataField = "cs_CodEdi"
                'End If
                
                If Dispositivo = 0 Then
                    Documento_Lote_Consola_Transferencia.Show vbModal
                Else
                    Documento_Lote_Consola_Transferencia.PrintReport True
                End If
                
                Set Documento_Lote_Consola_Transferencia = Nothing
                
            End If
            
        Else
            Call Mensaje(True, "No existen datos relacionados en el sistema.")
        End If
        
    Else
        Call Cerrar_Recordset(mRsMaRequisicion)
    End If
    
    Call Cerrar_Recordset(mRsTrRequisicion)
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Imprimir_Lote_Consola_Transferencia)"
    
    FrmAppLink.SetRepObjRTF FrmAppLink.ClaseReporteRTF
    Set Rep = Nothing
    Set ShowRTF = Nothing
    
    Set Documento_Lote_Consola_Transferencia = Nothing
    
End Sub
