VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsDelegate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private Delegate As Collection

Public Sub Initialize()
    Set Delegate = New Collection
End Sub

Property Get Raw()
    Raw = Delegate
End Property

Property Get Name() As String
    Name = Delegate("ProcName")
End Property

Property Let Name(pName As String)
    If Not Collection_ExisteKey(Delegate, "ProcName") Then
        Delegate.Add pName, "ProcName"
    Else
        Delegate.Remove ("ProcName")
        Delegate.Add pName, "ProcName"
    End If
End Property

Private Sub InitializeParams()
    If Not Collection_ExisteKey(Delegate, "Params", True) Then Delegate.Add New Collection, "Params"
End Sub

Property Get Param(pName As String)
    On Error Resume Next
    Set Param = Delegate("Params")(pName)("Value")
    If Err.Number = 0 Then Exit Sub
    Param = Delegate("Params")(pName)("Value")
End Property

Public Sub SetParam(pName As String, pType As String, pVal)
    
    InitializeParams
    
    If Not Collection_ExisteKey(Delegate("Params"), pName) Then
        Delegate("Params").Add New Collection, pName
        Delegate("Params")(pName).Add pName, "Name"
        Delegate("Params")(pName).Add pType, "Type"
        Delegate("Params")(pName).Add pVal, "Value"
    End If
    
End Sub

Property Get Output()
    On Error Resume Next
    Set Output = Delegate("Output")
    If Err.Number = 0 Then Exit Sub
    Output = Delegate("Output")
End Property

Property Let Output(pVal)
    On Error Resume Next
    Delegate.Remove ("Output")
    Delegate.Add pVal, "Output"
End Property

Public Function IsMissing(ParamName As String) As Boolean
    If Not Collection_ExisteKey(Delegate("Params"), ParamName, False) _
    And Not Collection_ExisteKey(Delegate("Params"), ParamName, True) Then
        IsMissing = True
    End If
End Function
