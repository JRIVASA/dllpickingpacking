VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFechaSeleccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarFecha                                                                   As Date
Private mvarSelecciono                                                              As Boolean
Private mvarTiempoNulo                                                              As Boolean

Property Get FECHA() As Date
    FECHA = mvarFecha
End Property

Property Let FECHA(ByVal Data As Date)
    mvarFecha = Data
End Property

Property Let Selecciono(ByVal Data As Boolean)
    mvarSelecciono = Data
End Property

Property Get Selecciono() As Boolean
    Selecciono = mvarSelecciono
End Property

Property Let TiempoNulo(ByVal Data As Boolean)
    mvarTiempoNulo = Data
End Property

Property Get TiempoNulo() As Boolean
    TiempoNulo = mvarTiempoNulo
End Property

Public Sub MostrarInterfaz()
    Set frmFechaSel.fCls = Me
    frmFechaSel.Show vbModal
    Set frmFechaSel = Nothing
End Sub

Public Sub MostrarInterfazFechaHora(Optional tipo As Integer)
    'MostrarInterfazFechaHora = Now
    Set FrmDateSelector.fCls = Me
    FrmDateSelector.SelectedDateType = tipo
    FrmDateSelector.Show vbModal
    Set FrmDateSelector = Nothing
End Sub

