VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsTrans_Det_InvxDoc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarDocumento                                           As String
Private mvarDisponibleAnt                                       As String
Private mvarCantidad                                            As Double

Property Get DocumentoSolicitud() As String
    DocumentoSolicitud = mvarDocumento
End Property

Property Get DisponibleAnterior() As Double
    DisponibleAnterior = mvarDisponibleAnt
End Property

Property Get CantidadSolicitud() As Double
    CantidadSolicitud = mvarCantidad
End Property

Public Function AddDocumentoInvxDoc(pDoc As String, _
pInv As Double, pCant As Double) As ClsTrans_Det_InvxDoc
    
    mvarDocumento = pDoc
    mvarDisponibleAnt = pInv
    mvarCantidad = pCant
    
    Set AddDocumentoInvxDoc = Me
    
End Function

