VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FrmTransferencia_AsignarPickingLote 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleMode       =   0  'User
   ScaleWidth      =   15360
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btn_asignarm 
      Caption         =   "Asignar m"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   9360
      Picture         =   "FrmTransferencia_AsignarPickingLote.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   23
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9960
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Height          =   255
      Left            =   0
      Picture         =   "FrmTransferencia_AsignarPickingLote.frx":0CCA
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   22
      Top             =   720
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture2 
      Height          =   255
      Left            =   0
      Picture         =   "FrmTransferencia_AsignarPickingLote.frx":2114
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   21
      Top             =   960
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.CommandButton ButtonUbicacion 
      Caption         =   "Asignar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   5040
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   20
      Top             =   9520
      Width           =   3735
   End
   Begin VB.CommandButton ButtonLeyenda 
      Caption         =   "Leyenda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   10800
      Picture         =   "FrmTransferencia_AsignarPickingLote.frx":355E
      Style           =   1  'Graphical
      TabIndex        =   19
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9960
      Width           =   1215
   End
   Begin VB.CommandButton cmd_listo 
      Caption         =   "Empacar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   13920
      Picture         =   "FrmTransferencia_AsignarPickingLote.frx":4228
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Empacar Lote"
      Top             =   9960
      Width           =   1095
   End
   Begin VB.CommandButton ButtonActualizar 
      Caption         =   "Actualizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   12360
      Picture         =   "FrmTransferencia_AsignarPickingLote.frx":5FAA
      Style           =   1  'Graphical
      TabIndex        =   7
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9960
      Width           =   1215
   End
   Begin VB.TextBox txtminutos 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   7680
      TabIndex        =   6
      Text            =   "5"
      Top             =   10320
      Width           =   375
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   5160
      Top             =   10080
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   8850
      LargeChange     =   10
      Left            =   14520
      TabIndex        =   1
      Top             =   600
      Width           =   674
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   -240
      TabIndex        =   0
      Top             =   0
      Width           =   19320
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Consola Asignaci�n para Recolecci�n de Lote de Transferencia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   480
         TabIndex        =   12
         Top             =   120
         Width           =   12375
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12840
         TabIndex        =   3
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14880
         Picture         =   "FrmTransferencia_AsignarPickingLote.frx":7D2C
         Top             =   0
         Width           =   480
      End
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   8865
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   15075
      _ExtentX        =   26591
      _ExtentY        =   15637
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      Enabled         =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ProgressBar bar 
      Height          =   255
      Left            =   11640
      TabIndex        =   4
      Top             =   9525
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.CheckBox Chk_Ubicacion 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Ubicaci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   420
      Left            =   600
      TabIndex        =   13
      Top             =   9960
      Value           =   1  'Checked
      Width           =   1425
   End
   Begin VB.CheckBox Chk_NombreProducto 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Art�culo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   2520
      TabIndex        =   14
      Top             =   10080
      Width           =   1305
   End
   Begin VB.CheckBox Chk_FechaAsignacion 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Recolector"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   2520
      TabIndex        =   15
      Top             =   10440
      Width           =   1425
   End
   Begin VB.CheckBox Chk_Lote 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Solicitud"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   210
      Left            =   600
      TabIndex        =   16
      Top             =   10440
      Width           =   1665
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "minutos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   8160
      TabIndex        =   9
      Top             =   10320
      Width           =   855
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Intervalo de actualizar: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   5160
      TabIndex        =   8
      Top             =   10320
      Width           =   2535
   End
   Begin VB.Shape Shape1 
      Height          =   240
      Index           =   4
      Left            =   8880
      Top             =   9525
      Visible         =   0   'False
      Width           =   6255
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Pr�xima actualizaci�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9000
      TabIndex        =   5
      Top             =   9525
      Width           =   2535
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   735
      Index           =   0
      Left            =   5040
      TabIndex        =   10
      Top             =   10080
      Width           =   4095
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00AE5B00&
      X1              =   3246.341
      X2              =   4689.159
      Y1              =   9800
      Y2              =   9800
   End
   Begin VB.Label C�digo 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Criterios de Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Index           =   71
      Left            =   360
      TabIndex        =   17
      Top             =   9650
      Width           =   2805
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   1215
      Index           =   2
      Left            =   240
      TabIndex        =   18
      Top             =   9600
      Width           =   4455
   End
End
Attribute VB_Name = "FrmTransferencia_AsignarPickingLote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Lote As String
Private Empacar As Variant
Private OrderBy As String
Private Salir As Variant

Private CodDepositoOrigen As String

Private Sub btn_asignarm_Click()
    Call AsignarPedidosMas
    Call ButtonActualizar_Click
End Sub

Private Sub Form_Activate()
    If Salir Then
        Mensaje True, "No hay art�culos pendientes por recolectar."
        Unload Me
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    
    CodDepositoOrigen = BuscarReglaNegocioStr("TRA_Consola_CodDepositoOrigen", Empty)
    
    If Trim(mvarCodigoDepositoOrigen) = Empty Then
        CodDepositoOrigen = FrmAppLink.GetCodDepositoPredeterminado
    End If
    
    AjustarPantalla Me
    
    Salir = False
    
    OrderBy = "ORDER BY Ubicacion, CodPedido"
    
    Call PrepararGrid
    Call PrepararDatos
    
    ButtonUbicacion.Enabled = False
    
    Call txtminutos_LostFocus
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionFree
        
        .FixedCols = 0
        .FixedRows = 1
        
        .Rows = 1
        .Cols = 12
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        .FormatString = "Solicitud" & "|" & "Articulo" & "|" & _
        "Nombre del Producto" & "|" & "Ubicacion" & "|" & _
        "Recolector" & "|" & "Cant. Sol" & "|" & "Cant. Rec"
                
        .ColWidth(0) = 1900
        .ColAlignment(0) = flexAlignCenterCenter
        
        .ColWidth(1) = 1900
        .ColAlignment(1) = flexAlignCenterCenter
        
        .ColWidth(2) = 3600 '5160
        .ColAlignment(2) = flexAlignLeftCenter
        
        .ColWidth(3) = 1860
        .ColAlignment(3) = flexAlignCenterCenter
        
        .ColWidth(4) = 2500 '3100
        .ColAlignment(4) = flexAlignLeftCenter
        
        .ColWidth(5) = 1100 '1100
        .ColAlignment(5) = flexAlignCenterCenter
        
        .ColWidth(6) = 1100 '1100
        .ColAlignment(6) = flexAlignCenterCenter
        
        .ColWidth(7) = 0
        .ColWidth(8) = 0
        
        .ColWidth(9) = 600
        
        .ColWidth(10) = 0
        .ColWidth(11) = 0
        
        .Width = 15100
        
        .ScrollTrack = True
        
        .Row = 0
        .Col = 7
        .ColSel = 6
        
    End With
    
    For I = 0 To Grid.Cols - 1
        Grid.Col = I
        Grid.CellAlignment = flexAlignCenterCenter
    Next
    
End Sub

Private Sub PrepararDatos()
    
    On Error GoTo Error1
    
    Empacar = True
    
    Dim SQL As String, Rs As New ADODB.Recordset
    
    mCampoUbicaciones = "REPLACE(isNULL(REVERSE(SUBSTRING(REVERSE((SELECT UBC2.cu_Mascara + '[VBNEWLINE]' " & _
    "FROM MA_UBICACIONxPRODUCTO UBC2 " & _
    "WHERE UBC2.cu_Deposito + UBC2.cu_Producto = '" & FixTSQL(CodDepositoOrigen) & "' + CodArticulo " & _
    "FOR XML PATH(''))), 1 + LEN('[VBNEWLINE]'), 9999)), 'N/A'), '&#x20', '') AS Ubicacion"
    
    SQL = "SELECT CodEDI, Picking, CodArticulo, Nombre, SUM(CantSolicitada) AS CantSolicitada, " & _
    "SUM(CantRecolectada) / Count(CodArticulo) AS CantRecolectada, NombreRecolector, " & _
    "CodPedido, CodArticulo, c_CodDeposito, cs_CodLocalidad_Solicitud, CostoLinea, " & vbNewLine & _
    mCampoUbicaciones & " FROM ( " & vbNewLine & _
    "SELECT isNULL(COD.c_Codigo, MA_PRODUCTOS.c_Codigo) AS CodEDI," & vbNewLine & _
    "isNULL(PP.Picking, 0) AS Picking, " & _
    "MA_PRODUCTOS.c_Codigo AS CodArticulo, " & vbNewLine & _
    "MA_PRODUCTOS.c_Descri AS Nombre, DET.ns_Cantidad AS CantSolicitada, " & vbNewLine & _
    "isNULL(PP.CantRecolectada, 0) AS CantRecolectada, " & vbNewLine & _
    "isNULL(USR.Descripcion, '') AS NombreRecolector, TR.c_Documento AS CodPedido, " & _
    "TR.cs_CodLocalidad_Solicitud, DET.ns_Costo AS CostoLinea, '' AS c_CodDeposito " & vbNewLine & _
    "FROM TR_LOTE_GESTION_TRANSFERENCIA TR " & vbNewLine
    
    SQL = SQL & _
    "INNER JOIN MA_REQUISICIONES DOC " & _
    "ON TR.c_Documento = DOC.cs_Documento " & vbNewLine & _
    "AND TR.cs_CodLocalidad_Solicitud = DOC.cs_CodLocalidad " & vbNewLine & _
    "INNER JOIN TR_REQUISICIONES DET " & _
    "ON TR.c_Documento = DET.cs_Documento" & vbNewLine & _
    "AND TR.cs_CodLocalidad_Solicitud = DET.cs_CodLocalidad " & vbNewLine & _
    "INNER JOIN MA_PRODUCTOS " & _
    "ON MA_PRODUCTOS.c_Codigo = DET.cs_CodArticulo " & vbNewLine & _
    "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & _
    "ON PP.CodLote = TR.cs_Corrida " & vbNewLine & _
    "AND PP.CodProducto = DET.cs_CodArticulo " & vbNewLine & _
    "AND PP.CodPedido = TR.c_Documento " & vbNewLine & _
    "AND PP.cs_CodLocalidad_Solicitud = TR.cs_CodLocalidad_Solicitud " & vbNewLine & _
    "LEFT JOIN MA_USUARIOS USR " & _
    "ON PP.CodRecolector = USR.CodUsuario " & vbNewLine & _
    "LEFT JOIN MA_Codigos COD " & _
    "ON COD.c_CodNasa = MA_PRODUCTOS.C_Codigo " & _
    "AND COD.nu_Intercambio = 1 " & _
    "WHERE TR.cs_Corrida = '" & Lote & "' " & vbNewLine & _
    " ) AS Todo " & _
    "GROUP BY Nombre, CodEDI, Picking, CodArticulo, " & _
    "NombreRecolector, CodPedido, CodArticulo, c_CodDeposito, " & _
    "cs_CodLocalidad_Solicitud, CostoLinea " & vbNewLine & _
    OrderBy
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        Salir = True
        Exit Sub
    End If
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    Grid.Visible = False
    
    While Not Rs.EOF
        
        Grid.Rows = Grid.Rows + 1
        
        Grid.TextMatrix(Grid.Rows - 1, 0) = Rs!CodPedido
        Grid.TextMatrix(Grid.Rows - 1, 1) = Rs!CodEDI
        Grid.TextMatrix(Grid.Rows - 1, 2) = Rs!Nombre
        
        TmpUbc = Rs!Ubicacion
        Ubc = vbNullString
        
        Do While (TmpUbc Like "*[[]VBNEWLINE[]]*")
            
            TmpNumChars = InStr(1, TmpUbc, "[VBNEWLINE]")
            TmpNumChars = TmpNumChars - 1
            TmpItmUbc = Mid(TmpUbc, 1, TmpNumChars)
            TmpUbc = Mid(TmpUbc, TmpNumChars + Len("[VBNEWLINE]") + 1)
            
            TmpMaxLen = 12
            
            If Len(TmpItmUbc) <= TmpMaxLen Then
                RemChar = TmpMaxLen - Len(TmpItmUbc)
                TmpItmUbc = String(Int(RemChar / 2), " ") & TmpItmUbc & String(Int(RemChar / 2), " ")
                'TmpItmUbc = 'Rellenar_SpaceR(TmpItmUbc, 15, " ")
            End If
            
            Ubc = Ubc & TmpItmUbc
            
        Loop
        
        Ubc = Ubc & TmpUbc
        
        TmpCantLn = (Len(Rs!Ubicacion) - _
        Len(Replace(Rs!Ubicacion, "[VBNEWLINE]", vbNullString))) / Len("[VBNEWLINE]")
        
        If TmpCantLn >= 2 Then
            Grid.RowHeight(Grid.Rows - 1) = (Grid.RowHeight(Grid.Rows - 1) * ((TmpCantLn + 1) / 2))
        End If
        
        Grid.TextMatrix(Grid.Rows - 1, 3) = Ubc 'Rs!Ubicacion
        
        Grid.TextMatrix(Grid.Rows - 1, 4) = Rs!NombreRecolector
        Grid.TextMatrix(Grid.Rows - 1, 5) = Rs!CantSolicitada
        Grid.TextMatrix(Grid.Rows - 1, 6) = Rs!CantRecolectada
        Grid.TextMatrix(Grid.Rows - 1, 8) = Rs!CodArticulo
        
        Grid.TextMatrix(Grid.Rows - 1, 10) = Rs!cs_CodLocalidad_Solicitud
        Grid.TextMatrix(Grid.Rows - 1, 11) = CDec(Rs!CostoLinea)
        
        Grid.Col = 9
        
'        If GRID.TextMatrix(GRID.Rows - 1, 4) <> "" Then
'            GRID.Row = GRID.Rows - 1
'            Set GRID.CellPicture = Me.Picture1.Picture
'        Else
            Grid.Row = Grid.Rows - 1
            Set Grid.CellPicture = Me.Picture2.Picture
'        End If
        
        Dim I As Integer
        
        Grid.TextMatrix(Grid.Rows - 1, 7) = "0"
        
        If Not IsNull(Rs!Picking) Then
            If Rs!Picking Then
                Grid.TextMatrix(Grid.Rows - 1, 7) = "1"
                If CDec(Rs!CantRecolectada) = CDec(Rs!CantSolicitada) Then
                    For I = 0 To Grid.Cols - 1
                        Grid.Row = Grid.Rows - 1
                        Grid.Col = I
                        Grid.CellBackColor = &HB4EDB9    ' Verde
                    Next I
                Else
                    For I = 0 To Grid.Cols - 1
                        Grid.Row = Grid.Rows - 1
                        Grid.Col = I
                        Grid.CellBackColor = 12632319 ' Rojo
                    Next I
                End If
            Else
                Empacar = False
                If Rs!NombreRecolector <> Empty Then
                    For I = 0 To Grid.Cols - 1
                        Grid.Row = Grid.Rows - 1
                        Grid.Col = I
                        Grid.CellBackColor = -2147483624   'Amarillo
                    Next I
                End If
            End If
        Else
            Empacar = False
        End If
        
        Rs.MoveNext
        
    Wend
    
    Rs.Close
    
    Grid.Col = 0
    Grid.Row = 1
    
    If Grid.Rows > 15 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(2) = 4160 - 675
    Else
        Grid.ScrollBars = flexScrollBarNone
        Grid.ColWidth(2) = 4160
        ScrollGrid.Visible = False
    End If
    
    LabelTitulo.Caption = "Consola Asignaci�n para Recolecci�n de Lote de Transferencia N� " & Lote
    
Finally:
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos. " & _
    "Informaci�n Adicional: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub Grid_Click()
    
    ' La mayoria de estas acciones estan comentadas para hacerlas en el Doble Click
    
    Select Case Grid.ColSel
        
        'Case Is = 0
            
            'AsignacionMultiple
            
        'Case Is = 1
            
            'AsignarProducto
            
        'Case Is = 2
            
            'AsignacionMultiple
            
        'Case Is = 3
            
            'AsignacionMultiple
            
        'Case Is = 4
            
            'If Grid.TextMatrix(Grid.RowSel, Grid.ColSel) <> Empty Then
                'AsignacionMultiple
            'End If
            
        'Case Is = 5
            
            'AsignarProducto
            
        'Case Is = 6
            
            'AsignarProducto
            
        Case Is = 9
            
            Call Grid_DblClick
            
        'Case Else
            
            'AsignarProducto
            
    End Select
    
    'Call ButtonActualizar_Click
    
End Sub

Private Sub Grid_DblClick()
    
    Select Case Grid.ColSel
        
        Case Is = 0
            
            AsignacionMultiple
            
            Call ButtonActualizar_Click
            
        Case Is = 1
            
            AsignarProducto
            
            Call ButtonActualizar_Click
            
        Case Is = 2
            
            AsignacionMultiple
            
            Call ButtonActualizar_Click
            
        Case Is = 3
            
            AsignacionMultiple
            
            Call ButtonActualizar_Click
            
        Case Is = 4
            
            'If Grid.TextMatrix(Grid.RowSel, Grid.ColSel) <> Empty Then
                ' Comentado para permitir asignar todos los que no tengan recolector asignado.
                AsignacionMultiple
                Call ButtonActualizar_Click
            'End If
            
        Case Is = 5
            
            AsignarProducto
            
            Call ButtonActualizar_Click
            
        Case Is = 6
            
            AsignarProducto
            
            Call ButtonActualizar_Click
            
        Case Is = 9
            
            Grid.Col = Grid.ColSel
            
            If Grid.CellPicture = Me.Picture2.Picture Then
               Set Grid.CellPicture = Me.Picture1.Picture
            ElseIf Grid.TextMatrix(Grid.RowSel, 4) <> Empty _
            And Grid.CellPicture = Me.Picture1.Picture Then
                Set Grid.CellPicture = Me.Picture2.Picture
            Else
                Set Grid.CellPicture = Me.Picture2.Picture
            End If
            
        Case Else
            
            AsignarProducto
            
            Call ButtonActualizar_Click
            
    End Select
    
End Sub

Private Sub AsignacionMultiple()
    
    On Error GoTo Error1
    
    Dim RecFinalizada As Variant, Campo As String
    Dim Update As Boolean
    Dim Rs As New ADODB.Recordset
    Dim Trans As Boolean, AffectedRecords
    Dim LineaForzar As Long
    
    Update = False
    
    Campo = Grid.TextMatrix(Grid.RowSel, Grid.ColSel)
    
    RecFinalizada = True
    
    For I = 1 To Grid.Rows - 1
        
        If (Grid.TextMatrix(I, 7) <> "0" _
        Or Grid.TextMatrix(I, 6) <> 0) _
        And Grid.TextMatrix(I, Grid.ColSel) = Campo Then
            
            RecFinalizada = False
            
        End If
        
    Next I
    
    If RecFinalizada Then
        
ReasignarRecoleccion:
        
        Dim SQL As String
        
        SQL = "SELECT USR.CodUsuario, USR.Descripcion " & _
        "FROM MA_USUARIOS USR" & vbNewLine & _
        "INNER JOIN MA_VENDEDORES VEN " & _
        "ON 'PIC_' + USR.CodUsuario = VEN.cu_Vendedor_Cod" & vbNewLine & _
        "WHERE USR.bs_Activo = 1 " & _
        "AND VEN.cs_Tipo = 'PIC' "
        
        Retorno = True
        
        If Trim(Grid.TextMatrix(Grid.RowSel, 4)) <> Empty Then
            Retorno = Mensaje(False, "El producto " & Grid.TextMatrix(Grid.RowSel, 0) & " " & _
            "ya tiene un recolector �Seguro que desea cambiarlo?")
        End If
        
        If Retorno Then
            
            With Frm_Super_Consultas
                
                .Inicializar SQL, "U S U A R I O S", Ent.BDD
                
                .Add_ItemLabels "Codigo", "codusuario", 2610, 0
                .Add_ItemLabels "Nombre", "descripcion", 8500, 0
                .Add_ItemSearching "Nombre", "descripcion"
                .Add_ItemSearching "Codigo", "codusuario"
                
                .txtDato.Text = "%"
                
                '.BusquedaInstantanea = True
                
                .Show vbModal
                
                Resultado = .ArrResultado
                
            End With
            
            Set Frm_Super_Consultas = Nothing
            
            If Not IsNull(Resultado) Then
                
                If Resultado(0) <> Empty Then
                    
                    Ent.BDD.BeginTrans
                    Trans = True
                    
                    For I = 1 To Grid.Rows - 1
                        
                        If ((Grid.TextMatrix(I, 6) = "0" _
                        And Grid.TextMatrix(I, 7) = "0") _
                        Or (I = LineaForzar)) _
                        And Grid.TextMatrix(I, Grid.ColSel) = Campo Then
                            
                            If Grid.TextMatrix(I, 4) = Empty Then
                                
                                SQL = "SELECT * FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                                "WHERE CodLote = '" & Lote & "' " & _
                                "AND CodPedido = '" & Grid.TextMatrix(I, 0) & "' " & _
                                "AND CodProducto = '" & Grid.TextMatrix(I, 8) & "' " & _
                                "AND cs_CodLocalidad_Solicitud = '" & Grid.TextMatrix(I, 10) & "' " & _
                                "AND CostoLinea = (" & CDec(Grid.TextMatrix(I, 11)) & ") "
                                
                                Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
                                
                                While Not Rs.EOF
                                    
                                    Update = True
                                    
                                    Rs.MoveNext
                                    
                                Wend
                                
                                Rs.Close
                                
                                If Not Update Then
                                    
                                    SQL = "INSERT INTO MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                                    "(CodLote, CodPedido, CodProducto, " & vbNewLine & _
                                    "CodSupervisorPicking, CodRecolector, CantSolicitada, " & vbNewLine & _
                                    "cs_CodLocalidad_Solicitud, CostoLinea) " & vbNewLine & _
                                    "VALUES ('" & Lote & "', '" & Grid.TextMatrix(I, 0) & "', " & _
                                    "'" & Grid.TextMatrix(I, 8) & "', " & _
                                    "'" & LcCodUsuario & "', '" & Resultado(0) & "', " & _
                                    "(" & Grid.TextMatrix(I, 5) & "), '" & Grid.TextMatrix(I, 10) & "', " & _
                                    "(" & CDec(Grid.TextMatrix(I, 11)) & ")) "
                                    
                                Else
                                    
                                    SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                                    "CodRecolector = '" & Resultado(0) & "', " & _
                                    "CantRecolectada = 0, Picking = 0, " & vbNewLine & _
                                    "CodSupervisorPicking = '" & LcCodUsuario & "', " & vbNewLine & _
                                    "FechaAsignacion = CASE WHEN FechaAsignacion = '19000101' THEN " & _
                                    "GetDate() ELSE FechaAsignacion END " & _
                                    "WHERE CodLote = '" & Lote & "' " & _
                                    "AND CodPedido = '" & Grid.TextMatrix(I, 0) & "' " & vbNewLine & _
                                    "AND CodProducto = '" & Grid.TextMatrix(I, 8) & "' " & _
                                    "AND cs_CodLocalidad_Solicitud = '" & Grid.TextMatrix(I, 10) & "' " & _
                                    "AND CostoLinea = (" & CDec(Grid.TextMatrix(I, 11)) & ") "
                                    
                                End If
                                
                            Else
                                
                                SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                                "CodRecolector = '" & Resultado(0) & "', " & _
                                "CantRecolectada = 0, Picking = 0, " & vbNewLine & _
                                "CodSupervisorPicking = '" & LcCodUsuario & "', " & vbNewLine & _
                                "FechaAsignacion = CASE WHEN FechaAsignacion = '19000101' THEN " & _
                                "GetDate() ELSE FechaAsignacion END " & _
                                "WHERE CodLote = '" & Lote & "' " & _
                                "AND CodPedido = '" & Grid.TextMatrix(I, 0) & "'" & vbNewLine & _
                                "AND CodProducto = '" & Grid.TextMatrix(I, 8) & "' " & _
                                "AND cs_CodLocalidad_Solicitud = '" & Grid.TextMatrix(I, 10) & "' " & _
                                "AND CostoLinea = (" & CDec(Grid.TextMatrix(I, 11)) & ") "
                                
                            End If
                            
                            Ent.BDD.Execute SQL, AffectedRecords
                            
                            If Not Update _
                            And Grid.TextMatrix(I, 7) = "1" Then ' Si ya estaba con Picking Finalizado, Descomprometer.
                                
                                SQL = "IF NOT EXISTS( " & _
                                "SELECT * FROM MA_DEPOPROD " & _
                                "WHERE c_CodDeposito = '" & CodDepositoOrigen & "' " & _
                                "AND c_CodArticulo = '" & FixTSQL(Grid.TextMatrix(I, 8)) & "' " & _
                                ") " & _
                                "INSERT INTO MA_DEPOPROD (c_CodDeposito, c_CodArticulo, " & _
                                "c_Descripcion, n_Cantidad, n_Cant_Ordenada, n_Cant_Comprometida) " & _
                                "SELECT " & _
                                "'" & CodDepositoOrigen & "', '" & FixTSQL(Grid.TextMatrix(I, 8)) & "', " & _
                                "'', 0, 0, 0"
                                
                                Ent.BDD.Execute SQL, AffectedRecords
                                
                                SQL = "UPDATE MA_DEPOPROD SET " & _
                                "n_Cant_Comprometida = ROUND(n_Cant_Comprometida - " & _
                                "(" & SDec(Grid.TextMatrix(I, 6)) & "), 8, 0) " & _
                                "WHERE c_CodDeposito = '" & CodDepositoOrigen & "' " & _
                                "AND c_CodArticulo = '" & FixTSQL(Grid.TextMatrix(I, 8)) & "' "
                                
                                Ent.BDD.Execute SQL, AffectedRecords
                                
                            End If
                            
                        End If
                        
                        Update = False
                        
                    Next I
                    
                    If Trans Then
                        Ent.BDD.CommitTrans
                        Trans = False
                    End If
                    
                    PrepararGrid
                    PrepararDatos
                    
                End If
                
            End If
            
        End If
        
    Else
        
        Select Case Grid.ColSel
            Case Is = 0
                Mensaje True, "La recolecci�n algunos productos de esta solicitud " & _
                "�sta en proceso o finalizada. por lo cual no puede ser cambiado el recolector"
            Case Is = 2
                Mensaje True, "La recolecci�n algunos de estos productos " & _
                "�sta en proceso o finalizada. por lo cual no puede ser cambiado el recolector"
            Case Is = 3
                Mensaje True, "La recolecci�n algunos productos de esta ubicaci�n " & _
                "�sta en proceso o finalizada. por lo cual no puede ser cambiado el recolector"
            Case Is = 4
                'Mensaje True, "Este recolector ya ha realizado algunas recolecciones. por lo cual no puede ser cambiado el recolector"
                Mensaje False, "Este recolector ya ha iniciado el proceso. " & _
                "Si desea cambiar el recolector, en tal caso se reiniciar� el proceso " & _
                "de recolecci�n de los productos seleccionados. �Est� seguro de proceder?"
                If Retorno Then
                    'Grid.TextMatrix(Grid.Row, Grid.ColSel) = vbNullString
                    'Grid.TextMatrix(Grid.Row, 6) = "0"
                    'Grid.TextMatrix(Grid.Row, 7) = "0"
                    LineaForzar = Grid.Row
                    'Campo = vbNullString
                    GoTo ReasignarRecoleccion
                End If
        End Select
        
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If Trans Then
        Ent.BDD.RollbackTrans
        Trans = False
    End If
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error en la asignaci�n m�ltiple de recolector, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub AsignarProducto()
    
    On Error GoTo Error1
    
    Dim Update As Boolean
    Dim Rs As New ADODB.Recordset
    
    Update = False
    
    If Grid.TextMatrix(Grid.RowSel, 1) <> Empty Then
        
        If Grid.TextMatrix(Grid.RowSel, 7) = "0" Then
            
            Dim SQL As String
            Dim Resultado As Variant
            
            SQL = "SELECT USR.CodUsuario, USR.Descripcion " & _
            "FROM MA_USUARIOS USR" & vbNewLine & _
            "INNER JOIN MA_VENDEDORES VEN " & _
            "ON 'PIC_' + USR.CodUsuario = VEN.cu_Vendedor_Cod" & vbNewLine & _
            "WHERE USR.bs_Activo = 1 " & _
            "AND VEN.cs_Tipo = 'PIC' "
            
            Retorno = True
            
            If Grid.TextMatrix(Grid.RowSel, 6) = 0 Then
                
                If Trim(Grid.TextMatrix(Grid.RowSel, 4)) <> Empty Then
                    Retorno = Mensaje(False, "El producto " & Grid.TextMatrix(Grid.RowSel, 0) & " " & _
                    "ya tiene un recolector �Seguro que desea cambiarlo?")
                End If
                
                If Retorno Then
                    
                    With Frm_Super_Consultas
                        
                        .Inicializar SQL, "U S U A R I O S", Ent.BDD
                        
                        .Add_ItemLabels "Codigo", "codusuario", 2610, 0
                        .Add_ItemLabels "Nombre", "descripcion", 8500, 0
                        .Add_ItemSearching "Nombre", "descripcion"
                        .Add_ItemSearching "Codigo", "codusuario"
                        
                        .txtDato.Text = "%"
                        
                        '.BusquedaInstantanea = True
                        
                        .Show vbModal
                        
                        Resultado = .ArrResultado
                        
                    End With
                    
                    Set Frm_Super_Consultas = Nothing
                    
                    If Not IsNull(Resultado) Then
                        
                        If Resultado(0) <> Empty Then
                            
                            If Grid.TextMatrix(Grid.RowSel, 4) = Empty Then
                                
                                SQL = "SELECT * FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                                "WHERE CodLote = '" & Lote & "' " & _
                                "AND CodPedido = '" & Grid.TextMatrix(Grid.Row, 0) & "' " & _
                                "AND CodProducto = '" & Grid.TextMatrix(Grid.Row, 8) & "' " & _
                                "AND cs_CodLocalidad_Solicitud = '" & Grid.TextMatrix(Grid.Row, 10) & "' " & _
                                "AND CostoLinea = (" & CDec(Grid.TextMatrix(Grid.Row, 11)) & ") "
                                
                                Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
                                
                                While Not Rs.EOF
                                    
                                    Update = True
                                    
                                    Rs.MoveNext
                                    
                                Wend
                                
                                Rs.Close
                                
                                If Not Update Then
                                    
                                    SQL = "INSERT INTO MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                                    "(CodLote, CodPedido, CodProducto, " & vbNewLine & _
                                    "CodSupervisorPicking, CodRecolector, CantSolicitada, " & vbNewLine & _
                                    "cs_CodLocalidad_Solicitud, CostoLinea) " & vbNewLine & _
                                    "VALUES ('" & Lote & "', '" & Grid.TextMatrix(Grid.Row, 0) & "', " & _
                                    "'" & Grid.TextMatrix(Grid.Row, 8) & "', " & _
                                    "'" & LcCodUsuario & "', '" & Resultado(0) & "', " & _
                                    "" & Grid.TextMatrix(Grid.Row, 5) & ", '" & Grid.TextMatrix(Grid.Row, 10) & "', " & _
                                    "(" & CDec(Grid.TextMatrix(Grid.Row, 11)) & ")) "
                                    
                                Else
                                    
                                    SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                                    "CodRecolector = '" & Resultado(0) & "', " & vbNewLine & _
                                    "CodSupervisorPicking = '" & LcCodUsuario & "', " & vbNewLine & _
                                    "FechaAsignacion = CASE WHEN FechaAsignacion = '19000101' THEN " & _
                                    "GetDate() ELSE FechaAsignacion END " & _
                                    "WHERE CodLote = '" & Lote & "' " & _
                                    "AND CodPedido = '" & Grid.TextMatrix(Grid.Row, 0) & "' " & vbNewLine & _
                                    "AND CodProducto = '" & Grid.TextMatrix(Grid.Row, 8) & "' " & _
                                    "AND cs_CodLocalidad_Solicitud = '" & Grid.TextMatrix(Grid.Row, 10) & "' " & _
                                    "AND CostoLinea = (" & CDec(Grid.TextMatrix(Grid.Row, 11)) & ") "
                                    
                                End If
                                
                            Else
                                
                                SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                                "CodRecolector = '" & Resultado(0) & "', " & vbNewLine & _
                                "CodSupervisorPicking = '" & LcCodUsuario & "', " & vbNewLine & _
                                "FechaAsignacion = CASE WHEN FechaAsignacion = '19000101' THEN " & _
                                "GetDate() ELSE FechaAsignacion END " & _
                                "WHERE CodLote = '" & Lote & "' " & _
                                "AND CodPedido = '" & Grid.TextMatrix(Grid.Row, 0) & "' " & vbNewLine & _
                                "AND CodProducto = '" & Grid.TextMatrix(Grid.Row, 8) & "' " & _
                                "AND cs_CodLocalidad_Solicitud = '" & Grid.TextMatrix(Grid.Row, 10) & "' " & _
                                "AND CostoLinea = (" & CDec(Grid.TextMatrix(Grid.Row, 11)) & ") "
                                
                            End If
                            
                            Ent.BDD.Execute SQL
                            
                            Call ButtonActualizar_Click
                            
                            Grid.TextMatrix(Grid.RowSel, 4) = Resultado(1)
                            
                        End If
                        
                    End If
                    
                End If
                
            Else
                Mensaje True, "No puede ser cambiado el recolector de este producto."
            End If
            
        Else
            Mensaje True, "La recolecci�n de este producto ya finalizo."
        End If
        
    Else
        Mensaje True, "Seleccione un art�culo."
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al asignar el recolector, Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub ButtonActualizar_Click()
    
    Call PrepararGrid
    Call PrepararDatos
    
    ButtonUbicacion.Enabled = False
    
End Sub

Private Sub Grid_EnterCell()
    
    Select Case Grid.ColSel
        Case Is = 0
            ButtonUbicacion.Enabled = True
            ButtonUbicacion.Caption = "Asignar Solicitud."
        Case Is = 1
            ButtonUbicacion.Enabled = True
            ButtonUbicacion.Caption = "Asignar este Articulo."
        Case Is = 2
            ButtonUbicacion.Enabled = True
            ButtonUbicacion.Caption = "Asignar estos Articulos."
        Case Is = 3
            ButtonUbicacion.Enabled = True
            ButtonUbicacion.Caption = "Asignar Ubicaci�n."
        Case Is = 4
            If Grid.TextMatrix(Grid.RowSel, Grid.ColSel) <> Empty Then
                ButtonUbicacion.Enabled = True
                ButtonUbicacion.Caption = "Cambiar El Recolector."
            Else
                ButtonUbicacion.Enabled = False
            End If
        Case Is = 5
            ButtonUbicacion.Enabled = True
            ButtonUbicacion.Caption = "Asignar este Articulo."
        Case Is = 6
            ButtonUbicacion.Enabled = True
            ButtonUbicacion.Caption = "Asignar este Articulo."
        Case Else
            ButtonUbicacion.Enabled = False
    End Select
    
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call Grid_DblClick
    End If
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub Timer1_Timer()
    DoEvents
    bar.Value = bar.Value + 1
    If bar.Value = bar.Max Then
        Call ButtonActualizar_Click
        Call txtminutos_LostFocus
    End If
End Sub

Private Sub txtminutos_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyReturn
            Call txtminutos_LostFocus
    End Select
End Sub

Private Sub txtminutos_LostFocus()
    If Not IsNumeric(Me.txtminutos) Then
        Me.txtminutos.Text = "1"
        
        bar.Max = 1 * 60
        bar.Min = 0
        bar.Value = 0
        
        Timer1.Enabled = False
        Timer1.Enabled = True
    Else
        If txtminutos < 1 Then
            txtminutos = "1"
        End If
        
        bar.Max = Me.txtminutos * 60
        bar.Min = 0
        bar.Value = 0
        
        Timer1.Enabled = False
        Timer1.Enabled = True
    End If
End Sub

Private Sub Cmd_Listo_Click()
    
    On Error GoTo Error1
    
    If Empacar Then
        
        Dim SQL As String
        
        SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA SET " & _
        "b_PickingFinalizado = 1 " & _
        "WHERE cs_Corrida = '" & Lote & "' "
        
        Ent.BDD.Execute SQL
        
        FrmTransferencia_AsignarPackingLote.Lote = Lote
        FrmTransferencia_AsignarPackingLote.Show vbModal
        
        Set FrmTransferencia_AsignarPackingLote = Nothing
        
        Unload Me
        
    Else
        Mensaje True, "La recolecci�n de productos no ha finalizado."
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al finalizar la recolecci�n del lote, Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub Chk_Ubicacion_Click()
    If Chk_Ubicacion.Value = vbChecked Then
        OrderBy = "ORDER BY Ubicacion, CodPedido"
        Chk_FechaAsignacion.Value = vbUnchecked
        Chk_Lote.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_FechaAsignacion_Click()
    If Chk_FechaAsignacion.Value = vbChecked Then
        OrderBy = "ORDER BY NombreRecolector, CodPedido"
        Chk_Lote.Value = vbUnchecked
        Chk_Ubicacion.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Lote_Click()
    If Chk_Lote.Value = vbChecked Then
        OrderBy = "ORDER BY CodPedido"
        Chk_FechaAsignacion.Value = vbUnchecked
        Chk_Ubicacion.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_NombreProducto_Click()
    If Chk_NombreProducto.Value = vbChecked Then
        OrderBy = "ORDER BY Nombre, CodPedido"
        Chk_FechaAsignacion.Value = vbUnchecked
        Chk_Ubicacion.Value = vbUnchecked
        Chk_Lote.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub ButtonUbicacion_Click()
    Call Grid_DblClick
    Call ButtonActualizar_Click
End Sub

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute("SELECT C_PROCESO " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE C_PROCESO = '006'")
    
    If Not TempRs.EOF Then
        frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    End If
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub

Private Function AsignarPedidosMas()
    
    On Error GoTo Error1
    
    Dim RecFinalizada As Variant, Campo As String
    
    Dim Update As Boolean
    Dim Rs As New ADODB.Recordset
    
    Update = False
    
    Campo = Grid.TextMatrix(Grid.RowSel, Grid.ColSel)
    
    If Campo = Empty Then
        Campo = "N/A"
    End If
    
    Dim SQL As String
    
    SQL = "SELECT USR.CodUsuario, USR.Descripcion " & _
    "FROM MA_USUARIOS USR" & vbNewLine & _
    "INNER JOIN MA_VENDEDORES VEN " & _
    "ON 'PIC_' + USR.CodUsuario = VEN.cu_Vendedor_Cod" & vbNewLine & _
    "WHERE USR.bs_Activo = 1 " & _
    "AND VEN.cs_Tipo = 'PIC' "
    
    Retorno = True
    
    If Trim(Grid.TextMatrix(Grid.Row, 4)) <> Empty Then
        Retorno = Mensaje(False, "La Solicitud " & Grid.TextMatrix(Grid.RowSel, 0) & " " & _
        "ya tiene un recolector �Seguro que desea cambiarlo?")
    End If
    
    If Retorno Then
        
        With Frm_Super_Consultas
            
            .Inicializar SQL, "U S U A R I O S", Ent.BDD
            
            .Add_ItemLabels "Codigo", "codusuario", 2610, 0
            .Add_ItemLabels "Nombre", "descripcion", 8500, 0
            .Add_ItemSearching "Nombre", "descripcion"
            .Add_ItemSearching "Codigo", "codusuario"
            
            .txtDato.Text = "%"
            
            '.BusquedaInstantanea = True
            
            .Show vbModal
            
            Resultado = .ArrResultado
            
        End With
        
        Set Frm_Super_Consultas = Nothing
        
        If Not IsNull(Resultado) Then
            
            If Resultado(0) <> Empty Then
                
                For I = 1 To Grid.Rows - 1
                    
                    Grid.Col = 9
                    Grid.Row = I
                    
                    If Grid.TextMatrix(I, 6) = "0" _
                    And Grid.TextMatrix(I, 7) = "0" _
                    And Grid.CellPicture = Me.Picture1.Picture Then
                        
                        If Trim(Grid.TextMatrix(I, 4)) = Empty Then
                            
                            SQL = "SELECT * FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                            "WHERE CodLote = '" & Lote & "' " & _
                            "AND CodPedido = '" & Grid.TextMatrix(I, 0) & "' " & _
                            "AND CodProducto = '" & Grid.TextMatrix(I, 8) & "' " & _
                            "AND cs_CodLocalidad_Solicitud = '" & Grid.TextMatrix(I, 10) & "' " & _
                            "AND CostoLinea = (" & CDec(Grid.TextMatrix(I, 11)) & ") "
                            
                            Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
                            
                            While Not Rs.EOF
                                
                                Update = True
                                
                                Rs.MoveNext
                                
                            Wend
                            
                            Rs.Close
                            
                            If Not Update Then
                                
                                SQL = "INSERT INTO MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                                "(CodLote, CodPedido, CodProducto," & vbNewLine & _
                                "CodSupervisorPicking, CodRecolector, CantSolicitada, " & vbNewLine & _
                                "cs_CodLocalidad_Solicitud, CostoLinea) " & vbNewLine & _
                                "VALUES ('" & Lote & "', '" & Grid.TextMatrix(I, 0) & "', " & _
                                "'" & Grid.TextMatrix(I, 8) & "', " & _
                                "'" & LcCodUsuario & "', '" & Resultado(0) & "', " & _
                                "(" & Grid.TextMatrix(I, 5) & ", '" & Grid.TextMatrix(I, 10) & "', " & _
                                "(" & CDec(Grid.TextMatrix(I, 11)) & ")) "
                                
                            Else
                                
                                SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                                "CodRecolector = '" & Resultado(0) & "', " & vbNewLine & _
                                "CodSupervisorPicking = '" & LcCodUsuario & "', " & vbNewLine & _
                                "FechaAsignacion = CASE WHEN FechaAsignacion = '19000101' THEN " & _
                                "GetDate() ELSE FechaAsignacion END " & _
                                "WHERE CodLote = '" & Lote & "' " & _
                                "AND CodPedido = '" & Grid.TextMatrix(I, 0) & "' " & vbNewLine & _
                                "AND CodProducto = '" & Grid.TextMatrix(I, 8) & "' " & _
                                "AND cs_CodLocalidad_Solicitud = '" & Grid.TextMatrix(I, 10) & "' " & _
                                "AND CostoLinea = (" & CDec(Grid.TextMatrix(I, 11)) & ") "
                                
                            End If
                            
                        Else
                            
                            SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                            "CodRecolector = '" & Resultado(0) & "', " & vbNewLine & _
                            "CodSupervisorPicking = '" & LcCodUsuario & "', " & vbNewLine & _
                            "FechaAsignacion = CASE WHEN FechaAsignacion = '19000101' THEN " & _
                            "GetDate() ELSE FechaAsignacion END " & _
                            "WHERE CodLote = '" & Lote & "' " & _
                            "AND CodPedido = '" & Grid.TextMatrix(I, 0) & "' " & vbNewLine & _
                            "AND CodProducto = '" & Grid.TextMatrix(I, 8) & "' " & _
                            "AND cs_CodLocalidad_Solicitud = '" & Grid.TextMatrix(I, 10) & "' " & _
                            "AND CostoLinea = (" & CDec(Grid.TextMatrix(I, 11)) & ") "
                            
                        End If
                        
                        Ent.BDD.Execute SQL
                        
                    End If
                    
                    Update = False
                    
                Next I
                
            End If
            
        End If
        
    End If
    
    Exit Function
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(AsignarPedidosMas)"
    
End Function
