VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FrmTransferencia_ModificarLote 
   Appearance      =   0  'Flat
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleMode       =   0  'User
   ScaleWidth      =   15360
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox CmdOrdenar 
      BackColor       =   &H00AE5B00&
      Caption         =   "Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FAFAFA&
      Height          =   750
      Left            =   11400
      Style           =   1  'Graphical
      TabIndex        =   54
      Top             =   7800
      Width           =   2175
   End
   Begin VB.Frame FrameGrid 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      ForeColor       =   &H80000008&
      Height          =   4575
      Left            =   120
      TabIndex        =   49
      Top             =   3000
      Width           =   15135
      Begin VB.VScrollBar ScrollGrid 
         Height          =   4530
         LargeChange     =   10
         Left            =   14400
         TabIndex        =   50
         Top             =   0
         Width           =   674
      End
      Begin MSFlexGridLib.MSFlexGrid Grid 
         Height          =   4545
         Left            =   0
         TabIndex        =   51
         Top             =   0
         Width           =   15075
         _ExtentX        =   26591
         _ExtentY        =   8017
         _Version        =   393216
         BackColor       =   16448250
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         GridColor       =   13421772
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         Enabled         =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         SelectionMode   =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame FrameFiltros 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      ForeColor       =   &H80000008&
      Height          =   2295
      Left            =   120
      TabIndex        =   9
      Top             =   480
      Width           =   15015
      Begin VB.CommandButton CmdLocalidades 
         CausesValidation=   0   'False
         Height          =   345
         Left            =   14580
         Picture         =   "FrmTransferencia_ModificarLote.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   120
         Width           =   435
      End
      Begin VB.TextBox txtLocalidades 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10680
         Locked          =   -1  'True
         TabIndex        =   23
         Top             =   120
         Width           =   3855
      End
      Begin VB.ComboBox CmbClasificacion 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10680
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   1170
         Width           =   4335
      End
      Begin VB.TextBox Departamento 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2250
         MaxLength       =   10
         TabIndex        =   21
         ToolTipText     =   "Ingrese el Departamento donde se encuentra el producto"
         Top             =   600
         Width           =   1755
      End
      Begin VB.TextBox Grupo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2250
         MaxLength       =   10
         TabIndex        =   20
         ToolTipText     =   "Ingrese el Grupo al que Pertenece el producto"
         Top             =   1005
         Width           =   1755
      End
      Begin VB.TextBox Subgrupo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2250
         MaxLength       =   10
         TabIndex        =   19
         ToolTipText     =   "Ingrese el Subgrupo al que Pertenece el producto"
         Top             =   1410
         Width           =   1755
      End
      Begin VB.CommandButton CmdDepartamento 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   345
         Left            =   4050
         Picture         =   "FrmTransferencia_ModificarLote.frx":0802
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   600
         Width           =   315
      End
      Begin VB.CommandButton CmdSubgrupo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   345
         Left            =   4050
         Picture         =   "FrmTransferencia_ModificarLote.frx":1004
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   1410
         Width           =   315
      End
      Begin VB.CommandButton CmdGrupo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   345
         Left            =   4050
         Picture         =   "FrmTransferencia_ModificarLote.frx":1806
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   1005
         Width           =   315
      End
      Begin VB.CommandButton CmdProducto 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   345
         Left            =   4050
         Picture         =   "FrmTransferencia_ModificarLote.frx":2008
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   1800
         Width           =   315
      End
      Begin VB.TextBox txt_Producto 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2250
         TabIndex        =   14
         ToolTipText     =   "Ingrese el C�digo del Producto a Buscar."
         Top             =   1800
         Width           =   1755
      End
      Begin VB.CommandButton btnLote 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   345
         Left            =   14580
         Picture         =   "FrmTransferencia_ModificarLote.frx":280A
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   645
         Width           =   435
      End
      Begin VB.TextBox txtLote 
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10680
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   645
         Width           =   3855
      End
      Begin VB.Frame FrameBuscar 
         Appearance      =   0  'Flat
         BackColor       =   &H00AE5B00&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H0000C000&
         Height          =   495
         Left            =   9360
         TabIndex        =   10
         Top             =   1680
         Width           =   5655
         Begin VB.Label lblBuscarSolicitudes 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Buscar Lotes / Art�culos en Proceso"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   255
            Left            =   240
            TabIndex        =   11
            Top             =   120
            Width           =   5055
         End
      End
      Begin VB.Label lblLocalidad 
         BackStyle       =   0  'Transparent
         Caption         =   "Localidades"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   9360
         TabIndex        =   36
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label lblClasificacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Clasificaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   9360
         TabIndex        =   35
         Top             =   1170
         Width           =   1170
      End
      Begin VB.Label lblDepartamento 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   34
         Top             =   600
         Width           =   1530
      End
      Begin VB.Label lblGrupo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   33
         Top             =   1005
         Width           =   825
      End
      Begin VB.Label lblSubgrupo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Subgrupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   32
         Top             =   1410
         Width           =   1140
      End
      Begin VB.Label lbl_Departamento 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4410
         TabIndex        =   31
         Top             =   600
         Width           =   4650
      End
      Begin VB.Label lbl_Grupo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4410
         TabIndex        =   30
         Top             =   1005
         Width           =   4650
      End
      Begin VB.Label lbl_Subgrupo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4410
         TabIndex        =   29
         Top             =   1410
         Width           =   4650
      End
      Begin VB.Label lbl_Producto 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4410
         TabIndex        =   28
         Top             =   1800
         Width           =   4650
      End
      Begin VB.Label lblProducto 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo del Producto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   27
         Top             =   1830
         Width           =   2025
      End
      Begin VB.Label lblLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Lote:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   9360
         TabIndex        =   26
         Top             =   645
         Width           =   555
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Filtros de B�squeda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   25
         Top             =   120
         Width           =   2070
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   2400
         X2              =   9112
         Y1              =   270
         Y2              =   270
      End
   End
   Begin VB.Frame FrameFooter 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      ForeColor       =   &H80000008&
      Height          =   3135
      Left            =   120
      TabIndex        =   8
      Top             =   7680
      Width           =   15015
      Begin VB.CommandButton CmdVerDiferenciaTransporte 
         Caption         =   "Detalle Transporte"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   7680
         Picture         =   "FrmTransferencia_ModificarLote.frx":300C
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   1560
         Width           =   2175
      End
      Begin VB.CheckBox CmdLimpiar 
         BackColor       =   &H00AE5B00&
         Caption         =   "Limpiar Memoria (Desmarcar Todo)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   750
         Left            =   8880
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   120
         Width           =   2175
      End
      Begin VB.CommandButton cmdTransporte 
         Height          =   390
         Left            =   2100
         Picture         =   "FrmTransferencia_ModificarLote.frx":4D8E
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   1560
         Width           =   390
      End
      Begin VB.TextBox txtTransporte 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   42
         Top             =   1560
         Width           =   1815
      End
      Begin VB.CheckBox CmdAlternarSeleccion 
         BackColor       =   &H00AE5B00&
         Caption         =   "Marcar | Desmarcar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   750
         Left            =   6480
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   120
         Width           =   2175
      End
      Begin VB.CommandButton cmd_listo 
         Caption         =   "Generar Lote"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   12720
         Picture         =   "FrmTransferencia_ModificarLote.frx":5590
         Style           =   1  'Graphical
         TabIndex        =   40
         ToolTipText     =   "Empacar Lote"
         Top             =   1560
         Width           =   2175
      End
      Begin VB.CommandButton ButtonLeyenda 
         Caption         =   "Leyenda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   10320
         Picture         =   "FrmTransferencia_ModificarLote.frx":7312
         Style           =   1  'Graphical
         TabIndex        =   39
         Tag             =   "Actualizar"
         ToolTipText     =   "Actualizar Movimientos"
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CheckBox CmdBuscarFila 
         BackColor       =   &H00AE5B00&
         Caption         =   "Ubicar Fila"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   750
         Left            =   4080
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   120
         Width           =   2175
      End
      Begin VB.CheckBox ChkViewSel 
         BackColor       =   &H00AE5B00&
         Caption         =   "Ver Seleccionados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   750
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   120
         Width           =   2175
      End
      Begin VB.Label lblDescri 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   240
         TabIndex        =   48
         Top             =   2400
         Width           =   6975
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Descripcion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   47
         Top             =   2040
         Width           =   1455
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Responsable"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   2520
         TabIndex        =   46
         Top             =   1200
         Width           =   1695
      End
      Begin VB.Label lblResponsable 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2520
         TabIndex        =   45
         Top             =   1560
         Width           =   4695
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Transporte"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   240
         TabIndex        =   44
         Top             =   1200
         Width           =   1455
      End
   End
   Begin VB.VScrollBar ScrollGridSelOnly 
      Height          =   450
      LargeChange     =   10
      Left            =   3840
      TabIndex        =   7
      Top             =   10680
      Visible         =   0   'False
      Width           =   674
   End
   Begin VB.PictureBox Picture1 
      Height          =   255
      Left            =   2520
      Picture         =   "FrmTransferencia_ModificarLote.frx":7FDC
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   5
      Top             =   10680
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture2 
      Height          =   255
      Left            =   2880
      Picture         =   "FrmTransferencia_ModificarLote.frx":9426
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   4
      Top             =   10680
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   -240
      TabIndex        =   0
      Top             =   0
      Width           =   19320
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Modificar / Crear / Transformar Lotes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   480
         TabIndex        =   3
         Top             =   120
         Width           =   12375
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12840
         TabIndex        =   1
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14880
         Picture         =   "FrmTransferencia_ModificarLote.frx":A870
         Top             =   0
         Width           =   480
      End
   End
   Begin MSComctlLib.ProgressBar bar 
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   7725
      Visible         =   0   'False
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin MSFlexGridLib.MSFlexGrid GridSelOnly 
      Height          =   465
      Left            =   3240
      TabIndex        =   6
      Top             =   10680
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   820
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      Enabled         =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00AE5B00&
      X1              =   120.235
      X2              =   15100.49
      Y1              =   2880
      Y2              =   2880
   End
End
Attribute VB_Name = "FrmTransferencia_ModificarLote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public GeneroLote As Boolean
Public LoteNuevo As String

Public Enum GridCNL ' Crear Nuevo Lote
    ColNull
    ColRowID
    ColLote
    ColSolicitud
    ColLocalidadSolicitud
    ColCodigoItem
    ColDescItem
    ColCantDisponible
    ColCantAsignar
    ColAsignar
    ColAsignado
    ColPesoAsignado
    ColVolumenAsignado
    Col_PP_ID
    ColCodPrincipal
    ColCantDec
    ColTipoItem
    ColCantiBul
    ColPesoUni
    ColPesoEmp
    ColVolUni
    ColVolEmp
    ColCodRecolector
    ColNomRecolector
    ColCodEmpacador
    ColNomEmpacador
    ColEstatusLinea
    ColFaseLote
    ColFechaEmision
    ColFechaDespacho
    ColCodLocalidadDestino
    ColDesLocalidadDestino
    ColCantRecolectada
    ColCantEmpacada
    ColCostoLinea
    ColCount
End Enum

Private NomCol() As String
Private VarColWidth As Long

Public mOrderByGrid_ColAnt
Public mOrderByGrid_ColAct
Public mOrderByGrid_Mode ' 0 ASC, 1 DESC
Private mOrderByGrid_ManualColSel As GridCNL

Private PreservarTmp As Boolean

Private FormaCargada As Boolean

Private Empacar As Variant
Private OrderBy As String
Private Salir As Variant

Private CodDepositoOrigen As String

Private EventoProgramado As Boolean

Private mClsGrupos As Object 'New cls_grupos

Private Arr_Localidades As Object

Private mTransporte As ClsTransporte

Private mvarFormaTrabajoTransferencias As Long

Private Grid_Data_Temp1 As Dictionary
Private Grid_Data_Temp2 As Dictionary

Private Sub ChkViewSel_Click()
    
    If EventoProgramado Then
        EventoProgramado = False
        Exit Sub
    End If
    
    If ChkViewSel.Value = vbUnchecked Then
        Exit Sub
    End If
    
    If ChkViewSel.Tag = "0" Then
        ChkViewSel.Caption = "Ver Todos"
    Else
        ChkViewSel.Caption = "Ver Seleccionados"
    End If
    
    AlternarGridView
    
    ChkViewSel.Value = vbUnchecked
    
    SafeFocus Grid
    
End Sub

Private Sub AlternarGridView()
    
    On Error GoTo Error
    
    Dim TmpListItem As ListItem
    Dim Item, Item2, I, K
    Dim TmpRowData() As String
    
    If ChkViewSel.Tag = "0" Then  ' Ver solo elementos seleccionados
        
        Grid.Visible = False
        
        Set Grid_Data_Temp1 = New Dictionary
        
        For I = 1 To Grid.Rows - 1
            
            ReDim TmpRowData(ColCount) As String
            
            For K = 0 To ColCount - 1
                
                TmpRowData(K) = Grid.TextMatrix(I, K)
                
            Next K
            
            Grid_Data_Temp1.Add Grid.TextMatrix(I, Col_PP_ID), TmpRowData
            
        Next I
        
        Set Grid_Data_Temp2 = New Dictionary
        
        Grid.Rows = 1
        
        For Each Item In AsEnumerable(Grid_Data_Temp1.Items())
            
            If SBool(Item(ColAsignado)) Then
                
                Grid.Rows = Grid.Rows + 1
                Grid.Row = Grid.Rows - 1
                
                For K = 0 To ColCount - 1
                    
                    Grid.TextMatrix(Grid.Row, K) = Item(K)
                    
                Next
                
                Grid.Col = ColAsignar
                
                'If Grid.TextMatrix(Grid.Row, ColAsignar) = "1" Then
                'If SBool(Grid.ColData(ColAsignar)) Then
                If SBool(Grid.TextMatrix(Grid.Row, ColAsignado)) Then
                    Set Grid.CellPicture = Me.Picture1.Picture
                    Grid.ColData(ColAsignar) = 1
                    Grid.TextMatrix(Grid.Row, ColAsignado) = "1"
                Else
                    Set Grid.CellPicture = Me.Picture2.Picture
                    Grid.ColData(ColAsignar) = 0
                    Grid.TextMatrix(Grid.Row, ColAsignado) = "0"
                End If
                
                Grid.TextMatrix(Grid.Row, ColAsignar) = Empty
                
                Call PintarFilaGrid(Grid.Row)
                
            End If
            
        Next
        
        If Grid.Rows = 1 Then
            Grid.Rows = 2
            Grid.Row = 1
        End If
        
        Grid.Visible = True
        
        'Toolbar1.Buttons("opciones").ButtonMenus("View_Sel_Only").Text = _
        StellarMensaje(4088)  '"Ver todas las filas"
        
        EventoProgramado = True
        ChkViewSel.Tag = "1"
        DoEvents
        EventoProgramado = False
        
    Else
        
        Grid.Visible = False
        
        Set Grid_Data_Temp2 = New Dictionary
        
        For I = 1 To Grid.Rows - 1
            
            ReDim TmpRowData(ColCount) As String
            
            For K = 0 To ColCount - 1
                
                TmpRowData(K) = Grid.TextMatrix(I, K)
                
            Next K
            
            Grid_Data_Temp2.Add Grid.TextMatrix(I, Col_PP_ID), TmpRowData
            
        Next I
        
        Grid.Rows = 1
        
        '
        
        For Each Item In AsEnumerable(Grid_Data_Temp1.Items())
            
            If Grid_Data_Temp2.Exists(CStr(Item(Col_PP_ID))) Then
                
                Item2 = Grid_Data_Temp2(CStr(Item(Col_PP_ID)))
                
                Grid.Rows = Grid.Rows + 1
                Grid.Row = Grid.Rows - 1
                
                For K = 0 To ColCount - 1
                    
                    Grid.TextMatrix(Grid.Row, K) = Item2(K)
                    
                Next
                
                Grid.Col = ColAsignar
                
                If SBool(Item2(ColAsignado)) Then
                    Set Grid.CellPicture = Me.Picture1.Picture
                    Grid.ColData(ColAsignar) = 1
                    Grid.TextMatrix(Grid.Row, ColAsignado) = "1"
                Else
                    Set Grid.CellPicture = Me.Picture2.Picture
                    Grid.ColData(ColAsignar) = 0
                    Grid.TextMatrix(Grid.Row, ColAsignado) = "0"
                End If
                
                Grid.TextMatrix(Grid.Row, ColAsignar) = Empty
                
            Else
                
                Grid.Rows = Grid.Rows + 1
                Grid.Row = Grid.Rows - 1
                
                For K = 0 To ColCount - 1
                    
                    Grid.TextMatrix(Grid.Row, K) = Item(K)
                    
                Next
                
                Grid.Col = ColAsignar
                Set Grid.CellPicture = Me.Picture2.Picture
                Grid.ColData(ColAsignar) = 0
                Grid.TextMatrix(Grid.Row, ColAsignar) = Empty
                Grid.TextMatrix(Grid.Row, ColAsignado) = "0"
                
            End If
            
            Call PintarFilaGrid(Grid.Row)
            
        Next
        
        Grid.Visible = True
        
        'Toolbar1.Buttons("opciones").ButtonMenus("View_Sel_Only").Text = StellarMensaje(4087)  ' Ver solo elementos seleccionados
        
        EventoProgramado = True
        ChkViewSel.Tag = "0"
        DoEvents
        EventoProgramado = False
        
    End If
    
    Exit Sub
    
Error:
    
    'Resume ' Debug
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(MostrarFilasSeleccionadas)"
    
    Grid.Visible = True
    
    EventoProgramado = False
    
End Sub

Private Sub UbicarFila()
    
    On Error GoTo Error
    
    Dim mSql1 As String
    
    ValidarConexion Ent.BDD
    
    If Grid.Rows <= 0 Then
        Exit Sub
    End If
    
    EventoProgramado = True
    
    Grid.Visible = False
    
    Dim CualquierCampo As String
    
    For I = 1 To Grid.Rows - 1
        
        If I > 1 Then
            mSql1 = mSql1 & vbNewLine & _
            "UNION ALL " & vbNewLine
        End If
        
        'Grid.Row = I
        
        mSql1 = mSql1 & _
        "SELECT CAST('" & FormatoDecimalesDinamicos(CDec( _
        Grid.TextMatrix(I, ColRowID)), 0, 0) & "' AS NVARCHAR(500)) AS ColRowID, " & _
        "'" & FixTSQL(Grid.TextMatrix(I, ColLote)) & "' AS ColLote, " & _
        "'" & FixTSQL(Grid.TextMatrix(I, ColSolicitud)) & "' AS ColSolicitud, " & _
        "'" & FixTSQL(Grid.TextMatrix(I, ColCodigoItem)) & "' AS ColCodigoItem, " & _
        "'" & FixTSQL(Grid.TextMatrix(I, ColDescItem)) & "' AS ColDescItem, " & _
        "CAST('" & FormatoDecimalesDinamicos(CDec( _
        Grid.TextMatrix(I, ColCantDisponible)), 0, 3) & "' AS NVARCHAR(500)) AS ColCantDisponible, " & _
        "CAST('" & FormatoDecimalesDinamicos(CDec( _
        Grid.TextMatrix(I, ColCantAsignar)), 0, 3) & "' AS NVARCHAR(500)) AS ColCantAsignar, " & _
        "CASE " & SDec(Grid.TextMatrix(I, ColAsignado)) & " WHEN 1 THEN 'SI' ELSE 'NO' END AS ColAsignado, " & _
        "CAST('" & FormatoDecimalesDinamicos(CDec( _
        Grid.TextMatrix(I, ColPesoAsignado)), 0, 3) & "' AS NVARCHAR(500)) AS ColPesoAsignado, " & _
        "CAST('" & FormatoDecimalesDinamicos(CDec( _
        Grid.TextMatrix(I, ColVolumenAsignado)), 0, 3) & "' AS NVARCHAR(500)) AS ColVolumenAsignado, " & _
        "'" & FixTSQL(Grid.TextMatrix(I, ColCodPrincipal)) & "' AS ColCodPrincipal, " & _
        "'" & MinDec0(Grid.TextMatrix(I, ColCantDec)) & "' AS ColCantDec, " & _
        "'" & MinDec0(Grid.TextMatrix(I, ColCantiBul)) & "' AS ColCantiBul, " & _
        "CAST('" & FormatoDecimalesDinamicos(CDec( _
        Grid.TextMatrix(I, ColPesoUni)), 0, 3) & "' AS NVARCHAR(500)) AS ColPesoUni, " & _
        "CAST('" & FormatoDecimalesDinamicos(CDec( _
        Grid.TextMatrix(I, ColPesoEmp)), 0, 3) & "' AS NVARCHAR(500)) AS ColPesoEmp, "
        
        mSql1 = mSql1 & _
        "CAST('" & FormatoDecimalesDinamicos(CDec( _
        Grid.TextMatrix(I, ColVolUni)), 0, 3) & "' AS NVARCHAR(500)) AS ColVolUni, " & _
        "CAST('" & FormatoDecimalesDinamicos(CDec( _
        Grid.TextMatrix(I, ColVolEmp)), 0, 3) & "' AS NVARCHAR(500)) AS ColVolEmp, " & _
        "'" & FixTSQL(Grid.TextMatrix(I, ColNomRecolector)) & "' AS ColNomRecolector, " & _
        "'" & FixTSQL(Grid.TextMatrix(I, ColNomEmpacador)) & "' AS ColNomEmpacador, " & _
        "CASE " & Grid.TextMatrix(I, ColEstatusLinea) & " " & _
        "WHEN 2 THEN 'Empacado' " & _
        "WHEN 1 THEN 'Recolectado' " & _
        "ELSE 'Solicitado' " & _
        "END AS ColEstatusLinea, " & _
        "CASE " & Grid.TextMatrix(I, ColFaseLote) & " " & _
        "WHEN 2 THEN 'Pendiente por Finalizar' " & _
        "WHEN 1 THEN 'Empacando' " & _
        "ELSE 'Recolectando' " & _
        "END AS ColFaseLote, " & _
        "CAST('" & FechaBD(CDate(Grid.TextMatrix(I, ColFechaEmision))) & "' AS DATE) AS ColFechaEmision, " & _
        "CAST('" & FechaBD(CDate(Grid.TextMatrix(I, ColFechaDespacho))) & "' AS DATE) AS ColFechaDespacho, " & _
        "'" & FixTSQL(Grid.TextMatrix(I, ColDesLocalidadDestino)) & "' AS ColDesLocalidadDestino, "
        
        CualquierCampo = FormatoDecimalesDinamicos(CDec( _
        Grid.TextMatrix(I, ColRowID)), 0, 0) & vbTab & FixTSQL(Grid.TextMatrix(I, ColLote)) & vbTab & _
        FixTSQL(Grid.TextMatrix(I, ColSolicitud)) & vbTab & FixTSQL(Grid.TextMatrix(I, ColCodigoItem)) & vbTab & _
        FixTSQL(Grid.TextMatrix(I, ColDescItem)) & vbTab & _
        FormatoDecimalesDinamicos(CDec(Grid.TextMatrix(I, ColCantDisponible)), 0, 3) & vbTab & _
        FormatoDecimalesDinamicos(CDec(Grid.TextMatrix(I, ColCantAsignar)), 0, 3) & vbTab & _
        IIf(SBool(Grid.TextMatrix(I, ColAsignado)), "SI", "NO") & vbTab & _
        FormatoDecimalesDinamicos(CDec(Grid.TextMatrix(I, ColPesoAsignado)), 0, 3) & vbTab & _
        FormatoDecimalesDinamicos(CDec(Grid.TextMatrix(I, ColVolumenAsignado)), 0, 3) & vbTab & _
        FixTSQL(Grid.TextMatrix(I, ColCodigoItem)) & vbTab & _
        MinDec0(Grid.TextMatrix(I, ColCantDec)) & vbTab & MinDec0(Grid.TextMatrix(I, ColCantiBul)) & vbTab & _
        FormatoDecimalesDinamicos(CDec(Grid.TextMatrix(I, ColPesoUni)), 0, 3) & vbTab & _
        FormatoDecimalesDinamicos(CDec(Grid.TextMatrix(I, ColPesoEmp)), 0, 3) & vbTab & _
        FormatoDecimalesDinamicos(CDec(Grid.TextMatrix(I, ColVolUni)), 0, 3) & vbTab & _
        FormatoDecimalesDinamicos(CDec(Grid.TextMatrix(I, ColVolEmp)), 0, 3) & vbTab & _
        FixTSQL(Grid.TextMatrix(I, ColNomRecolector)) & vbTab & FixTSQL(Grid.TextMatrix(I, ColNomEmpacador)) & vbTab & _
        IIf(Grid.TextMatrix(I, ColEstatusLinea) = "2", "Empacado", IIf(Grid.TextMatrix(I, ColEstatusLinea) = "1", _
        "Recolectado", "Solicitado")) & vbTab & _
        IIf(Grid.TextMatrix(I, ColFaseLote) = "2", "Pendiente por Finalizar", _
        IIf(Grid.TextMatrix(I, ColEstatusLinea) = "1", "Empacando", "Recolectando")) & vbTab & _
        FixTSQL(Grid.TextMatrix(I, ColFechaEmision)) & vbTab & FixTSQL(Grid.TextMatrix(I, ColFechaDespacho)) & vbTab & _
        FixTSQL(Grid.TextMatrix(I, ColDesLocalidadDestino)) & vbTab & CStr(CDec(I)) & vbTab
        
        mSql1 = mSql1 & _
        "CualquierCampo = '" & CualquierCampo & "', "
        
        mSql1 = mSql1 & _
        "" & CDec(I) & " AS NumeroFila "
        
    Next I
    
    Set mFrmSuperConsultas = FrmAppLink.GetFrmSuperConsultas
    
    mSql1 = "SELECT * FROM (" & mSql1 & ") TB WHERE 1 = 1 "
    
    mFrmSuperConsultas.Inicializar mSql1, "B�squeda de Filas", Ent.BDD
    
    '
    
    mFrmSuperConsultas.Add_ItemLabels Empty, "NumeroFila", 0, 0
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColRowID), "ColRowID", _
    700, ListColumnAlignmentConstants.lvwColumnCenter, , Array("Numerico", 0, 0)
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColLote), "ColLote", _
    1500, ListColumnAlignmentConstants.lvwColumnCenter
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColSolicitud), "ColSolicitud", _
    1500, ListColumnAlignmentConstants.lvwColumnCenter
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColCodigoItem), "ColCodigoItem", _
    1500, ListColumnAlignmentConstants.lvwColumnLeft
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColDescItem), "ColDescItem", _
    4000, ListColumnAlignmentConstants.lvwColumnLeft
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColCantDisponible), "ColCantDisponible", _
    1400, ListColumnAlignmentConstants.lvwColumnRight, , Array("Numerico", 0, 3)
    
    mFrmSuperConsultas.Add_ItemLabels "Cantidad", "ColCantAsignar", _
    1400, ListColumnAlignmentConstants.lvwColumnRight, , Array("Numerico", 0, 3)
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColAsignado), "ColAsignado", _
    1150, ListColumnAlignmentConstants.lvwColumnCenter
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColPesoAsignado), "ColPesoAsignado", _
    1200, ListColumnAlignmentConstants.lvwColumnRight, , Array("Numerico", 0, 3)
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColVolumenAsignado), "ColVolumenAsignado", _
    1200, ListColumnAlignmentConstants.lvwColumnRight, , Array("Numerico", 0, 3)
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColCodPrincipal), "ColCodPrincipal", _
    1925, ListColumnAlignmentConstants.lvwColumnLeft
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColCantDec), "ColCantDec", _
    1200, ListColumnAlignmentConstants.lvwColumnRight, , Array("Numerico", 0, 0)
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColCantiBul), "ColCantiBul", _
    1500, ListColumnAlignmentConstants.lvwColumnRight, , Array("Numerico", 0, 0)
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColPesoUni), "ColPesoUni", _
    1250, ListColumnAlignmentConstants.lvwColumnRight, , Array("Numerico", 0, 3)
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColPesoEmp), "ColPesoEmp", _
    1250, ListColumnAlignmentConstants.lvwColumnRight, , Array("Numerico", 0, 3)
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColVolUni), "ColVolUni", _
    1250, ListColumnAlignmentConstants.lvwColumnRight, , Array("Numerico", 0, 3)
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColVolEmp), "ColVolEmp", _
    1250, ListColumnAlignmentConstants.lvwColumnRight, , Array("Numerico", 0, 3)
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColNomRecolector), "ColNomRecolector", _
    2000, ListColumnAlignmentConstants.lvwColumnLeft
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColNomEmpacador), "ColNomEmpacador", _
    2000, ListColumnAlignmentConstants.lvwColumnLeft
    
    'mFrmSuperConsultas.Add_ItemLabels NomCol(ColEstatusLinea), "ColEstatusLinea", _
    3000, ListColumnAlignmentConstants.lvwColumnLeft
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColFaseLote), "ColFaseLote", _
    2250, ListColumnAlignmentConstants.lvwColumnLeft
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColEstatusLinea), "ColEstatusLinea", _
    2250, ListColumnAlignmentConstants.lvwColumnLeft
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColFechaEmision), "ColFechaEmision", _
    1925, ListColumnAlignmentConstants.lvwColumnCenter, , Array("Fecha", "VbShortDate")
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColFechaDespacho), "ColFechaDespacho", _
    1925, ListColumnAlignmentConstants.lvwColumnCenter, , Array("Fecha", "VbShortDate")
    
    mFrmSuperConsultas.Add_ItemLabels NomCol(ColDesLocalidadDestino), "ColDesLocalidadDestino", _
    4000, ListColumnAlignmentConstants.lvwColumnLeft
    
    mFrmSuperConsultas.Add_ItemLabels "NumeroFila", "NumeroFila", 0, 0
    
    '
    
    mFrmSuperConsultas.Add_ItemSearching "Cualquier Campo", "CualquierCampo"
    
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColLote), "ColLote"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColSolicitud), "ColSolicitud"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColCodigoItem), "ColCodigoItem"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColDescItem), "ColDescItem"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColCantDisponible), "ColCantDisponible"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColCantAsignar), "ColCantAsignar"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColAsignado), "ColAsignado"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColPesoAsignado), "ColPesoAsignado"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColVolumenAsignado), "ColVolumenAsignado"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColCodPrincipal), "ColCodPrincipal"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColCantDec), "ColCantDec"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColCantiBul), "ColCantiBul"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColPesoUni), "ColPesoUni"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColPesoEmp), "ColPesoEmp"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColVolUni), "ColVolUni"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColVolEmp), "ColVolEmp"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColNomRecolector), "ColNomRecolector"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColNomEmpacador), "ColNomEmpacador"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColEstatusLinea), "ColEstatusLinea"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColFaseLote), "ColFaseLote"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColFechaEmision) & " (YYYY-MM-DD)", "ColFechaEmision"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColFechaDespacho) & " (YYYY-MM-DD)", "ColFechaDespacho"
    mFrmSuperConsultas.Add_ItemSearching NomCol(ColDesLocalidadDestino), "ColDesLocalidadDestino"
    
    mFrmSuperConsultas.Add_ItemSearching "NumeroFila", "NumeroFila"
    
    mFrmSuperConsultas.txtDato.Text = "%"
    mFrmSuperConsultas.StrOrderBy = "NumeroFila ASC "
    mFrmSuperConsultas.BusquedaInstantanea = (Grid.Rows <= 300)
    mFrmSuperConsultas.CustomFontSize = 10
    
    mFrmSuperConsultas.Show vbModal
    
    EventoProgramado = True
    
    mResult = mFrmSuperConsultas.ArrResultado
    
    Set mFrmSuperConsultas = Nothing
    
    Grid.Visible = True
    
    If Not IsEmpty(mResult) Then
        
        If Trim(mResult(0)) <> Empty Then
            
            Grid.Row = CLng(mResult(0))
            Grid.TopRow = Grid.Row
            Grid.Refresh
            Grid.RowSel = Grid.Row
            Grid.Col = ColRowID
            Grid.ColSel = ColCount - 1
            
            SafeFocus Grid
            
        End If
        
    End If
    
    DoEvents
    EventoProgramado = False
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Grid.Visible = True
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(UbicarFila)"
    
    SafeFocus Grid
    
    DoEvents
    
    EventoProgramado = False
    
End Sub

Private Sub CmdBuscarFila_Click()
    
    If CmdBuscarFila.Value = vbUnchecked Then
        Exit Sub
    End If
    
    Call UbicarFila
    
    SafeFocus Grid
    
    CmdBuscarFila.Value = vbUnchecked
    
End Sub

Private Sub AlternarSeleccion()
    
    On Error GoTo Error
    
    Grid.Visible = False
    
    Dim TmpMostrar, TmpMostrarAnt
    
    Grid.Col = ColAsignar
    
    For I = 1 To Grid.Rows - 1
        
        If Grid.TextMatrix(I, ColAsignado) <> Empty Then
            
            TmpMostrarAnt = SBool(Grid.TextMatrix(I, ColAsignado))
            TmpMostrar = Not TmpMostrarAnt
            
            Grid.Row = I
            
            If TmpMostrar Then
                
                Grid.TextMatrix(I, ColAsignado) = "1"
                Grid.ColData(I) = 1
                Set Grid.CellPicture = Me.Picture1.Picture
                
            Else
                
                Grid.TextMatrix(I, ColAsignado) = "0"
                Grid.ColData(I) = 0
                Set Grid.CellPicture = Me.Picture2.Picture
                
            End If
            
            mSQL_TMP = "UPDATE TMP_LOTE_GESTION_TRANSFERENCIA SET " & _
            "Asignado = " & Grid.TextMatrix(I, ColAsignado) & " " & _
            "WHERE CodUsuario = '" & FixTSQL(FrmAppLink.GetCodUsuario) & "' " & _
            "AND LAN_DeviceName = '" & FixTSQL(FrmAppLink.GetPCName) & "' " & _
            "AND PP_ID = " & SDec(Grid.TextMatrix(Grid.Row, Col_PP_ID)) & " "
            
            Ent.BDD.Execute mSQL_TMP, RowsAffected
            
        End If
        
    Next
    
    Grid.Visible = True
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Grid.Visible = True
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(AlternarSeleccion)"
    
    Grid.Visible = True
    
End Sub

Private Sub CmdAlternarSeleccion_Click()
    
    If CmdAlternarSeleccion.Value = vbUnchecked Then
        Exit Sub
    End If
    
    Call AlternarSeleccion
    
    SafeFocus Grid
    
    CmdAlternarSeleccion.Value = vbUnchecked
    
End Sub

Private Sub LimpiarTmp()
    
    ActivarMensajeGrande 50
    
    If Mensaje(False, "Atenci�n, esta opci�n remover� el estatus de seleccionado " & _
    "de todas las filas, y restaurar� todas las cantidades asignadas a cero. " & _
    "Si esta seguro de continuar, confirme presione Aceptar. ") Then
        
        PreservarTmp = False ' Limpiar
        
        lblBuscarSolicitudes_Click ' Recargar Todo
        
        PreservarTmp = True ' Habilitar Memoria
        
    End If
    
End Sub

Private Sub CmdLimpiar_Click()
    
    If CmdLimpiar.Value = vbUnchecked Then
        Exit Sub
    End If
    
    Call LimpiarTmp
    
    SafeFocus Grid
    
    CmdLimpiar.Value = vbUnchecked
    
End Sub

Private Sub OrdenarFilas()
    
    On Error GoTo Error
    
    Dim mSql1 As String
    
    ValidarConexion Ent.BDD
    
    If Grid.Rows <= 0 Then
        Exit Sub
    End If
    
    EventoProgramado = True
    
    Grid.Visible = False
    
    Dim CualquierCampo As String
    
    mSql1 = Empty
    mSql1 = mSql1 & "SELECT " & Col_PP_ID & " AS ColSel, '" & NomCol(Col_PP_ID) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColLote & " AS ColSel, '" & NomCol(ColLote) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColSolicitud & " AS ColSel, '" & NomCol(ColSolicitud) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColCodigoItem & " AS ColSel, '" & NomCol(ColCodigoItem) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColDescItem & " AS ColSel, '" & NomCol(ColDescItem) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColCantDisponible & " AS ColSel, '" & NomCol(ColCantDisponible) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColCantAsignar & " AS ColSel, '" & NomCol(ColCantAsignar) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColAsignado & " AS ColSel, '" & NomCol(ColAsignado) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColPesoAsignado & " AS ColSel, '" & NomCol(ColPesoAsignado) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColVolumenAsignado & " AS ColSel, '" & NomCol(ColVolumenAsignado) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColCodPrincipal & " AS ColSel, '" & NomCol(ColCodPrincipal) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColCantDec & " AS ColSel, '" & NomCol(ColCantDec) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColCantiBul & " AS ColSel, '" & NomCol(ColCantiBul) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColTipoItem & " AS ColSel, '" & NomCol(ColTipoItem) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColPesoUni & " AS ColSel, '" & NomCol(ColPesoUni) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColPesoEmp & " AS ColSel, '" & NomCol(ColPesoEmp) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColVolUni & " AS ColSel, '" & NomCol(ColVolUni) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColVolEmp & " AS ColSel, '" & NomCol(ColVolEmp) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColNomRecolector & " AS ColSel, '" & NomCol(ColNomRecolector) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColNomEmpacador & " AS ColSel, '" & NomCol(ColNomEmpacador) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColFaseLote & " AS ColSel, '" & NomCol(ColFaseLote) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColEstatusLinea & " AS ColSel, '" & NomCol(ColEstatusLinea) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColFechaEmision & " AS ColSel, '" & NomCol(ColFechaEmision) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColFechaDespacho & " AS ColSel, '" & NomCol(ColFechaDespacho) & "' AS NomCol "
    mSql1 = mSql1 & vbNewLine & "UNION ALL "
    mSql1 = mSql1 & "SELECT " & ColDesLocalidadDestino & " AS ColSel, '" & NomCol(ColDesLocalidadDestino) & "' AS NomCol "
    
    Set mFrmSuperConsultas = FrmAppLink.GetFrmSuperConsultas
    
    mSql1 = "SELECT * FROM (" & mSql1 & ") TB WHERE 1 = 1 "
    
    mFrmSuperConsultas.Inicializar mSql1, "Campo Orden", Ent.BDD
    
    '
    
    mFrmSuperConsultas.Add_ItemLabels Empty, "ColSel", 0, 0
    
    mFrmSuperConsultas.Add_ItemLabels "Columna", "NomCol", _
    9000, ListColumnAlignmentConstants.lvwColumnLeft
    
    '
    
    mFrmSuperConsultas.Add_ItemSearching "Columna", "NomCol"
    
    mFrmSuperConsultas.txtDato.Text = "%"
    mFrmSuperConsultas.StrOrderBy = "ColSel ASC "
    mFrmSuperConsultas.BusquedaInstantanea = True
    mFrmSuperConsultas.CustomFontSize = 18
    
    mFrmSuperConsultas.Show vbModal
    
    EventoProgramado = True
    
    mResult = mFrmSuperConsultas.ArrResultado
    
    Set mFrmSuperConsultas = Nothing
    
    Grid.Visible = True
    
    mOrderByGrid_ManualColSel = ColNull
    
    If Not IsEmpty(mResult) Then
        
        If Trim(mResult(0)) <> Empty Then
            
            mOrderByGrid_ManualColSel = CDec(mResult(0))
            
            Grid_Click
            
            mOrderByGrid_ManualColSel = ColNull
            
        End If
        
    End If
    
    DoEvents
    EventoProgramado = False
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Grid.Visible = True
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(OrdenarFilas)"
    
    SafeFocus Grid
    
    DoEvents
    
    EventoProgramado = False
    
    mOrderByGrid_ManualColSel = ColNull
    
End Sub

Private Sub CmdOrdenar_Click()
    
    If CmdOrdenar.Value = vbUnchecked Then
        Exit Sub
    End If
    
    Call OrdenarFilas
    
    SafeFocus Grid
    
    CmdOrdenar.Value = vbUnchecked
    
End Sub

Private Sub CmdVerDiferenciaTransporte_Click()
    
    If Trim(txtTransporte.Text) = Empty Then
        Mensaje True, "Debe seleccionar un Transporte."
        Exit Sub
    End If
    
    Dim TmpPK As String, TmpCod As String, TmpRs As ADODB.Recordset
    Dim TmpDoc As Variant, TmpItm As Variant
    Dim DocumentoNDT As String, mSubtotal As Variant
    
    Dim mCantDisp As Variant, mCantAsignar As Variant
    Dim SumaCantDispSel As Variant, SumaCantAsignar As Variant
    Dim SumaCantEmp As Variant, SumaCantUnd As Variant
    Dim TotalPeso As Variant, TotalVolumen As Variant
    
    SumaCantDispSel = CDec(0)
    SumaCantAsignar = CDec(0)
    
    SumaCantEmp = CDec(0)
    SumaCantUnd = CDec(0)
    
    TotalPeso = CDec(0)
    TotalVolumen = CDec(0)
    
    Dim mSeleccionados As Long
    
    mSeleccionados = 0
    
    Dim mProductosSel As Dictionary
    
    Set mProductosSel = New Dictionary
    
    For I = 1 To Grid.Rows - 1
        
        If SBool(Grid.TextMatrix(I, ColAsignado)) _
        And (SDec(Grid.TextMatrix(I, ColCantAsignar)) > 0 _
        Or SDec(Grid.TextMatrix(I, ColCantDisponible)) = 0) Then
            
            mSeleccionados = mSeleccionados + 1
            
            mCantDisp = SDec(Grid.TextMatrix(I, ColCantDisponible))
            mCantAsignar = SDec(Grid.TextMatrix(I, ColCantAsignar))
            
            SumaCantDispSel = SumaCantDispSel + mCantDisp
            SumaCantAsignar = SumaCantAsignar + mCantAsignar
            
            TmpCod = Grid.TextMatrix(I, ColCodPrincipal)
            
            If Not mProductosSel.Exists(TmpCod) Then
                
                Set mProductosSel(TmpCod) = New Dictionary
                
                mProductosSel(TmpCod)("CodProducto") = TmpCod
                
                mProductosSel(TmpCod)("ColCantDec") = SDec(Grid.TextMatrix(I, ColCantDec))
                mProductosSel(TmpCod)("ColCantiBul") = MinDec0(Grid.TextMatrix(I, ColCantiBul))
                mProductosSel(TmpCod)("ColPesoUni") = SDec(Grid.TextMatrix(I, ColPesoUni))
                mProductosSel(TmpCod)("ColPesoEmp") = SDec(Grid.TextMatrix(I, ColPesoEmp))
                mProductosSel(TmpCod)("ColVolUni") = SDec(Grid.TextMatrix(I, ColVolUni))
                mProductosSel(TmpCod)("ColVolEmp") = SDec(Grid.TextMatrix(I, ColVolEmp))
                
                mProductosSel(TmpCod)("CantRecolectada") = CDec(0)
                mProductosSel(TmpCod)("CantEmpacada") = CDec(0)
                
                mProductosSel(TmpCod)("CantDisponible") = CDec(0)
                mProductosSel(TmpCod)("CantAsignar") = CDec(0)
                
            End If
            
            mProductosSel(TmpCod)("CantRecolectada") = _
            mProductosSel(TmpCod)("CantRecolectada") + _
            SDec(Grid.TextMatrix(I, ColCantRecolectada))
            
            mProductosSel(TmpCod)("CantEmpacada") = _
            mProductosSel(TmpCod)("CantEmpacada") + _
            SDec(Grid.TextMatrix(I, ColCantEmpacada))
            
            mProductosSel(TmpCod)("CantDisponible") = _
            mProductosSel(TmpCod)("CantDisponible") + _
            mCantDisp
            
            mProductosSel(TmpCod)("CantAsignar") = _
            mProductosSel(TmpCod)("CantAsignar") + _
            mCantAsignar
            
            If mvarFormaTrabajoTransferencias = 0 Then
                
                mProductosSel(TmpCod)("CantEmpaquesAsignar") = CDec(0)
                mProductosSel(TmpCod)("CantUnidadesRestantesAsignar") = _
                mProductosSel(TmpCod)("CantAsignar")
                
            Else
                
                mProductosSel(TmpCod)("CantEmpaquesAsignar") = _
                Fix(mProductosSel(TmpCod)("CantAsignar") / mProductosSel(TmpCod)("ColCantiBul"))
                
                mProductosSel(TmpCod)("CantUnidadesRestantesAsignar") = RoundDecimalUp( _
                mProductosSel(TmpCod)("CantAsignar") - _
                (mProductosSel(TmpCod)("CantEmpaquesAsignar") * mProductosSel(TmpCod)("ColCantiBul")), _
                mProductosSel(TmpCod)("ColCantDec"))
                
            End If
            
        End If
        
        ' FIN DE ESTRUCTURA DE MODIFICACION DE DETALLE DE LOTE
        
    Next
    
    For Each TmpItm In AsEnumerable(mProductosSel.Items())
        
        SumaCantEmp = SumaCantEmp + TmpItm("CantEmpaquesAsignar")
        SumaCantUnd = SumaCantUnd + TmpItm("CantUnidadesRestantesAsignar")
        
        If mvarFormaTrabajoTransferencias = 0 Then
            
            TotalPeso = TotalPeso + _
            (TmpItm("CantAsignar") * TmpItm("ColPesoUni"))
            
            TotalVolumen = TotalVolumen + _
            (TmpItm("CantAsignar") * TmpItm("ColVolUni"))
            
        Else
            
            TotalPeso = TotalPeso + _
            (TmpItm("CantEmpaquesAsignar") * TmpItm("ColPesoEmp")) + _
            (TmpItm("CantUnidadesRestantesAsignar") * TmpItm("ColPesoUni"))
            
            TotalVolumen = TotalVolumen + _
            (TmpItm("CantEmpaquesAsignar") * TmpItm("ColVolEmp")) + _
            (TmpItm("CantUnidadesRestantesAsignar") * TmpItm("ColVolUni"))
            
        End If
        
    Next
    
    With FrmTransferencia_ModificarLote_DetalleTransporte
        
        .CodigoTransporte = txtTransporte.Text
        
        .CantFilasSeleccionadas = mSeleccionados
        .CantProductos = mProductosSel.Count
        .CantEmpaques = SumaCantEmp
        .CantUnidades = SumaCantUnd
        .CantTotalUnidades = SumaCantAsignar
        .PesoAsignado = TotalPeso
        .VolumenAsignado = TotalVolumen
        
        .Show vbModal
        
        If UCase(.CodTransporteFinal) <> UCase(.CodigoTransporte) Then
            txtTransporte.Text = .CodTransporteFinal
            txtTransporte_LostFocus
        End If
        
        Set FrmTransferencia_ModificarLote_DetalleTransporte = Nothing
        
    End With
    
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    mOrderByGrid_ManualColSel = ColNull
    
    'CodDepositoOrigen = BuscarReglaNegocioStr("TRA_Consola_CodDepositoOrigen", Empty)
    
    Departamento.MaxLength = CampoLength("c_Departamento", "MA_PRODUCTOS", Ent.BDD, Alfanumerico, 10)
    Grupo.MaxLength = CampoLength("c_Grupo", "MA_PRODUCTOS", Ent.BDD, Alfanumerico, 10)
    Subgrupo.MaxLength = CampoLength("c_Subgrupo", "MA_PRODUCTOS", Ent.BDD, Alfanumerico, 10)
    
    lblClasificacion.Caption = FrmAppLink.StellarMensaje(3168) 'Clasificacion
    
    ChkViewSel.Value = vbUnchecked
    ChkViewSel.Caption = "Ver Seleccionados"
    
    EventoProgramado = True
    Set mClsGrupos = FrmAppLink.GetClassGrupo
    mClsGrupos.cTipoGrupo = "TRA_PICKING_CLS"
    mClsGrupos.CargarComboGrupos Ent.BDD, CmbClasificacion
    FrmAppLink.ListRemoveItems CmbClasificacion, "Ninguno" ' Quitar dichos elementos unicamente aqu� sin alterar la funci�n anterior.
    EventoProgramado = False
    
    FrmAppLink.ListSafeIndexSelection CmbClasificacion, 0
    
    mvarFormaTrabajoTransferencias = Val(BuscarReglaNegocioStr("Req_FormaDeTrabajo", "0"))
    
    CodDepositoOrigen = BuscarReglaNegocioStr("TRA_Consola_CodDepositoOrigen", Empty)
    
    If Trim(CodDepositoOrigen) = Empty Then
        CodDepositoOrigen = FrmAppLink.GetCodDepositoPredeterminado
    End If
    
    AjustarPantalla Me
    
    'Salir = False
    
    Call PrepararGrid
    'Call PrepararDatos
    
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        ChkViewSel.Tag = "0"
        
        PreservarTmp = True
        
        PrepararDatos
        
        'PreservarTmp = False ' Vamos a trabajar con memoria por defecto, y colocaremos una opci�n de limpiar.
        
    End If
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionFree
        .WordWrap = True
        .ScrollTrack = True
        .AllowUserResizing = flexResizeColumns
        
        .Rows = 2
        
        .FixedCols = 0
        .FixedRows = 1
        
        .Cols = GridCNL.ColCount
        ReDim NomCol(GridCNL.ColCount)
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        .Row = 0
        
        .Col = ColNull
        NomCol(.Col) = Empty
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColRowID
        NomCol(.Col) = "L�nea"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(.Col) = 800
        
        .Col = ColLote
        NomCol(.Col) = "Lote"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(.Col) = 1365
        
        .Col = ColSolicitud
        NomCol(.Col) = "Solicitud"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(.Col) = 1410
        
        .Col = ColCodigoItem
        NomCol(.Col) = "C�digo"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignLeftCenter
        .ColWidth(.Col) = 1770
        
        .Col = ColDescItem
        NomCol(.Col) = "Descripci�n"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignLeftCenter
        .ColWidth(.Col) = 2715
        
        VarColWidth = .ColWidth(.Col)
        
        .Col = ColCantDisponible
        NomCol(.Col) = "Disponible"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 1600
        
        .Col = ColCantAsignar
        NomCol(.Col) = "Cant. Asignar"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 1600
        
        .Col = ColAsignar
        NomCol(.Col) = Empty
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 600
        
        .Col = ColAsignado
        NomCol(.Col) = "Asignado"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColPesoAsignado
        NomCol(.Col) = "Peso"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 1500
        
        .Col = ColVolumenAsignado
        NomCol(.Col) = "Volumen"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 1500
        
        ' AQUI EMPIEZAN LAS OCULTAS / SOLO PARA USO EXCLUSIVO DE B�SQUEDA O USO INTERNO.
        
        .Col = Col_PP_ID
        NomCol(.Col) = "Orden de Entrada / Creacion de Lote"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColCodPrincipal
        NomCol(.Col) = "C�digo Principal"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColTipoItem
        NomCol(.Col) = "Tipo Item (Unitario - Pesado - Pesable - Informativo - Compuesto)"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColCantiBul
        NomCol(.Col) = "Cant. X Emp."
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColCantDec
        NomCol(.Col) = "Cant. Dec"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColPesoUni
        NomCol(.Col) = "Peso Uni."
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColPesoEmp
        NomCol(.Col) = "Peso Emp."
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColVolUni
        NomCol(.Col) = "Vol. Uni."
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColVolEmp
        NomCol(.Col) = "Vol. Emp."
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColCodRecolector
        NomCol(.Col) = "Cod. Rec."
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColNomRecolector
        NomCol(.Col) = "Recolector"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColCodEmpacador
        NomCol(.Col) = "Cod. Emp."
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColNomEmpacador
        NomCol(.Col) = "Empacador"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColEstatusLinea
        NomCol(.Col) = "Estatus Item"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColFaseLote
        NomCol(.Col) = "Estatus Lote"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColFechaEmision
        NomCol(.Col) = "Fecha Solicitud"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColFechaDespacho
        NomCol(.Col) = "Fecha Despacho"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColCodLocalidadDestino
        NomCol(.Col) = "Cod. Loc."
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColDesLocalidadDestino
        NomCol(.Col) = "Localidad Destino"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColCantRecolectada
        NomCol(.Col) = "Recolectados"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColCantEmpacada
        NomCol(.Col) = "Empacados"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColLocalidadSolicitud
        NomCol(.Col) = "Localidad Solicitud"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColCostoLinea
        NomCol(.Col) = "Costo"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        ' FIN COLUMNAS. GESTIONAR ORDENAMIENTO
        
        If mOrderByGrid_ColAnt = Empty _
        And mOrderByGrid_ColAct = Empty Then
            mOrderByGrid_ColAnt = Col_PP_ID
            mOrderByGrid_ColAct = Col_PP_ID
            mOrderByGrid_Mode = 0 ' Ascendente
        End If
        
        .Col = mOrderByGrid_ColAct
        .Text = .Text & _
        IIf(mOrderByGrid_Mode = 0, " (�)", " (�)")
        
        .Width = 15100
        
        .Row = 1
        
        .Col = ColRowID
        
    End With
    
End Sub

Private Sub PintarFilaGrid(ByVal pFila As Integer, _
Optional pForzarPosicion As Boolean)
    
    If pForzarPosicion Then
        Grid.Row = pFila
    End If
    
    If SDec(Grid.TextMatrix(pFila, ColFaseLote)) = 2 _
    Or SDec(Grid.TextMatrix(pFila, ColEstatusLinea)) = 2 Then
        For I = 0 To ColCount - 1
            Grid.Col = I
            Grid.CellBackColor = &HC0E0FF      'naranja 12640511
        Next I
    ElseIf SDec(Grid.TextMatrix(pFila, ColFaseLote)) = 1 _
    Or SDec(Grid.TextMatrix(pFila, ColEstatusLinea)) = 1 Then
        For I = 0 To Grid.Cols - 1
            Grid.Col = I
            Grid.CellBackColor = &HB4EDB9    'verde 11857337
        Next I
    Else
        If SDec(Grid.TextMatrix(pFila, ColEstatusLinea)) = 1 Then
            For I = 0 To Grid.Cols - 1
                Grid.Col = I
                Grid.CellBackColor = &HB4EDB9    'verde 11857337
            Next I
        Else
            'For I = 0 To Grid.Cols - 1
                'Grid.Col = I
                'Grid.CellBackColor = -2147483624 'Amarillo
            'Next I
        End If
    End If
    
End Sub

Private Sub PrepararDatos()
    
    On Error GoTo Error
    
    If ChkViewSel.Tag = "1" Then
        ChkViewSel.Value = vbChecked
    End If
    
    Dim SQL As String, mCriterio As String, Rs As New ADODB.Recordset, RowsAffected
    
    Dim mCantidad As String, mCantidadActual As String
    
    mCantidadActual = "CASE " & _
    "WHEN EstatusLinea = 2 THEN CantEmpacada " & _
    "WHEN EstatusLinea = 1 THEN CantRecolectada " & _
    "ELSE CantSolicitada " & _
    "END"
    
    mCantidad = "CASE WHEN TmpCantAsignar IS NULL THEN " & _
    mCantidadActual & " ELSE " & _
    "CASE WHEN TmpCantAsignar > " & mCantidadActual & " " & _
    "THEN " & mCantidadActual & " ELSE TmpCantAsignar END END"
    
    SQL = "SELECT *, " & _
    mCantidadActual & " AS CantDisponible, " & _
    "(" & mCantidadActual & " * PesoUni) AS PesoDisponible, " & _
    "(" & mCantidadActual & " * VolUni) AS VolumenDisponible, " & _
    mCantidad & " AS CantAsignar, " & _
    "(" & mCantidad & " * PesoUni) AS PesoAsignado, " & _
    "(" & mCantidad & " * VolUni) AS VolumenAsignado " & _
    "FROM ( " & vbNewLine & _
    vbNewLine & _
    Empty
    
    SQL = SQL & _
    "SELECT PP.CodLote, PP.CodPedido, PP.cs_CodLocalidad_Solicitud, isNULL(EDI.c_Codigo, PP.CodProducto) AS CodigoItem, " & _
    "PRO.c_Descri AS DescItem, PP.CodProducto, PRO.Cant_Decimales, PRO.n_TipoPeso, PRO.n_CantiBul, " & _
    "PRO.n_Peso AS PesoUni, PRO.n_PesoBul AS PesoEmp, PRO.n_Volumen AS VolUni, PRO.n_VolBul AS VolEmp, " & _
    "PP.CodRecolector, isNULL(PIC.Descripcion, 'N/A') AS NomRecolector, " & _
    "PP.CodEmpacador, isNULL(PAC.Descripcion, 'N/A') AS NomEmpacador, " & _
    "CASE WHEN MA.b_PackingFinalizado = 1 THEN 2 WHEN MA.b_PickingFinalizado = 1 THEN 1 ELSE 0 END AS FaseLote, " & _
    "CASE WHEN PP.Packing = 1 THEN 2 WHEN Picking = 1 THEN 1 ELSE 0 END AS EstatusLinea, " & _
    "PP.FechaAsignacion, PED.ds_Fecha AS FechaEmision, PED.d_Fecha_Recepcion AS FechaDespacho, " & _
    "TR.c_CodLocalidad_Destino AS CodLocalidadDestino, isNULL(SUC.c_Descripcion, 'N/A') AS DesLocalidadDestino, " & _
    "PP.CantEmpacada, PP.CantRecolectada, PP.CantSolicitada, PP.ID AS PP_ID, PP.CostoLinea, " & _
    "TMP.CantAsignar AS TmpCantAsignar, isNULL(TMP.Asignado, 0) AS TmpAsignado " & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & _
    "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA MA " & _
    "ON PP.CodLote = MA.cs_Corrida " & _
    "INNER JOIN MA_PRODUCTOS PRO " & _
    "ON PRO.c_Codigo = PP.CodProducto " & _
    "LEFT JOIN MA_CODIGOS EDI " & _
    "ON PRO.c_Codigo = EDI.c_CodNasa " & _
    "AND EDI.nu_Intercambio = 1 " & _
    "LEFT JOIN MA_USUARIOS PIC " & _
    "ON PP.CodRecolector = PIC.CodUsuario " & _
    "LEFT JOIN MA_USUARIOS PAC " & _
    "ON PP.CodRecolector = PAC.CodUsuario "
    
    SQL = SQL & _
    "INNER JOIN TR_LOTE_GESTION_TRANSFERENCIA TR " & _
    "ON TR.c_Documento = PP.CodPedido " & _
    "AND TR.cs_CodLocalidad_Solicitud = PP.cs_CodLocalidad_Solicitud " & _
    "AND TR.cs_Corrida = PP.CodLote " & _
    "INNER JOIN MA_REQUISICIONES PED " & _
    "ON PED.cs_Documento = PP.CodPedido " & _
    "AND TR.cs_CodLocalidad_Solicitud = PED.cs_CodLocalidad " & _
    "LEFT JOIN MA_SUCURSALES SUC " & _
    "ON TR.c_CodLocalidad_Destino = SUC.c_Codigo " & _
    "LEFT JOIN TMP_LOTE_GESTION_TRANSFERENCIA TMP " & _
    "ON TMP.PP_ID = PP.ID " & _
    "AND TMP.CodUsuario = '" & FixTSQL(FrmAppLink.GetCodUsuario) & "' " & _
    "AND TMP.LAN_DeviceName = '" & FixTSQL(FrmAppLink.GetPCName) & "' " & _
    "WHERE MA.b_Finalizada = 0 " & _
    "AND MA.b_Anulada = 0 "
    
    mCriterio = Empty
    
    If Trim(txt_Producto.Text) <> Empty Then
        mCriterio = mCriterio & _
        "AND PRO.c_Codigo = '" & FixTSQL(txt_Producto.Text) & "' "
    End If
    
    If Trim(Departamento.Text) <> Empty Then
        mCriterio = mCriterio & _
        "AND PRO.c_Departamento = '" & FixTSQL(Departamento.Text) & "' "
    End If
    
    If Trim(Grupo.Text) <> Empty Then
        mCriterio = mCriterio & _
        "AND PRO.c_Grupo = '" & FixTSQL(Grupo.Text) & "' "
    End If
    
    If Trim(Subgrupo.Text) <> Empty Then
        mCriterio = mCriterio & _
        "AND PRO.c_Subgrupo = '" & FixTSQL(Subgrupo.Text) & "' "
    End If
    
    If Not Arr_Localidades Is Nothing Then
        
        Dim ListaLocalidades As Variant, ListaLocalidadesIn As String
        
        ListaLocalidadesIn = Empty
        'ListaLocalidades = Split(txtLocalidades.Text, ",")
        
        For Each Item In Arr_Localidades
            ListaLocalidadesIn = ListaLocalidadesIn & "," & "'" & Item(0) & "'"
        Next
        
        If Len(ListaLocalidadesIn) > 0 Then
            
            mCriterio = mCriterio & _
            "AND TR.c_CodLocalidad_Destino IN (" & Mid(ListaLocalidadesIn, 2) & ") "
            
        End If
        
    End If
    
    If Trim(txtLote.Text) <> Empty Then
        mCriterio = mCriterio & _
        "AND PP.CodLote = '" & FixTSQL(txtLote.Text) & "' "
    End If
    
    If Trim(CmbClasificacion.Text) <> Empty Then
        mCriterio = mCriterio & _
        "AND MA.c_Clasificacion = '" & FixTSQL(CmbClasificacion.Text) & "' "
    End If
    
    SQL = SQL & _
    mCriterio & _
    vbNewLine & _
    ") TB1 "
    
    If Not PreservarTmp Then
        
        PrepararGrid
        
        mSQL_TMP = "DELETE TMP FROM TMP_LOTE_GESTION_TRANSFERENCIA TMP " & _
        "WHERE TMP.CodUsuario = '" & FixTSQL(FrmAppLink.GetCodUsuario) & "' " & _
        "AND TMP.LAN_DeviceName = '" & FixTSQL(FrmAppLink.GetPCName) & "' "
        
        Ent.BDD.Execute mSQL_TMP, RowsAffected
        
        If GeneroLote And LoteNuevo <> Empty Then
            Exit Sub
        End If
        
        mSQL_TMP = "INSERT INTO TMP_LOTE_GESTION_TRANSFERENCIA " & _
        "([LAN_DeviceName], [LAN_IP], [CodUsuario], [PP_ID], [CantAsignar], [Asignado]) " & _
        "SELECT '" & FixTSQL(FrmAppLink.GetPCName) & "', '" & FixTSQL(FrmAppLink.GetLAN_IP) & "', " & _
        "'" & FixTSQL(FrmAppLink.GetCodUsuario) & "', [PP_ID], [CantDisponible], 0 AS Asignado " & _
        "FROM ( " & vbNewLine & _
        vbNewLine & _
        SQL & vbNewLine & _
        vbNewLine & _
        ") TB2 "
        
        Ent.BDD.Execute mSQL_TMP, RowsAffected
        
    Else
        
        ' AGREGAR AL TMP DEL USUARIO LOS REGISTROS NUEVOS
        
        mSQL_TMP = "INSERT INTO TMP_LOTE_GESTION_TRANSFERENCIA " & _
        "([LAN_DeviceName], [LAN_IP], [CodUsuario], [PP_ID], [CantAsignar], [Asignado]) " & _
        "SELECT '" & FixTSQL(FrmAppLink.GetPCName) & "', '" & FixTSQL(FrmAppLink.GetLAN_IP) & "', " & _
        "'" & FixTSQL(FrmAppLink.GetCodUsuario) & "', TB.[PP_ID], [CantDisponible], 0 AS Asignado " & _
        "FROM ( " & vbNewLine & _
        vbNewLine & _
        SQL & vbNewLine & _
        vbNewLine & _
        ") TB " & _
        "LEFT JOIN TMP_LOTE_GESTION_TRANSFERENCIA TMP " & _
        "ON TMP.PP_ID = TB.PP_ID " & _
        "AND TMP.LAN_DeviceName = '" & FixTSQL(FrmAppLink.GetPCName) & "' " & _
        "AND TMP.CodUsuario = '" & FixTSQL(FrmAppLink.GetCodUsuario) & "' " & _
        "WHERE TMP.CantAsignar IS NULL "
        
        ' ACTUALIZAR EL TMP DEL USUARIO TODOS LOS REGISTROS QUE HAYA CON ESTE CRITERIO
        ' EN CASO DE QUE HAYAN CAMBIADO Y HAYAN FILAS O CANTIDADES QUE YA NO ESTEN DISPONIBLES.
        
        Ent.BDD.Execute mSQL_TMP, RowsAffected
        
        mSQL_TMP = "UPDATE TMP SET " & _
        "Asignado = TB.TmpAsignado, " & _
        "CantAsignar = TB.CantAsignar " & _
        "FROM ( " & vbNewLine & _
        vbNewLine & _
        SQL & vbNewLine & _
        vbNewLine & _
        ") TB " & _
        "INNER JOIN TMP_LOTE_GESTION_TRANSFERENCIA TMP " & _
        "ON TMP.PP_ID = TB.PP_ID " & _
        "AND TMP.LAN_DeviceName = '" & FixTSQL(FrmAppLink.GetPCName) & "' " & _
        "AND TMP.CodUsuario = '" & FixTSQL(FrmAppLink.GetCodUsuario) & "' " & _
        "WHERE 1 = 1 "
        
        Ent.BDD.Execute mSQL_TMP, RowsAffected
        
    End If
    
    Set Rs = ExecuteSafeSQL(SQL, Ent.BDD, RowsAffected, True, True)
    
    If Rs.RecordCount <= 0 Then
        Rs.Close
        Mensaje True, "No se encontraron datos con los criterios seleccionados."
        PrepararGrid
        Exit Sub
    End If
    
    TmpOrderByMode = IIf(mOrderByGrid_Mode = 0, " ASC", " DESC")
    
    Select Case mOrderByGrid_ColAct
        Case ColLote
            Rs.Sort = "CodLote" & TmpOrderByMode
        Case ColSolicitud
            Rs.Sort = "CodPedido" & TmpOrderByMode
        Case ColCodigoItem
            Rs.Sort = "CodigoItem" & TmpOrderByMode
        Case ColDescItem
            Rs.Sort = "DescItem" & TmpOrderByMode
        Case ColCantDisponible
            Rs.Sort = "CantDisponible" & TmpOrderByMode
        Case ColCantAsignar
            Rs.Sort = "CantAsignar" & TmpOrderByMode
        Case ColAsignar, ColAsignado
            Rs.Sort = "TmpAsignado" & TmpOrderByMode
        Case ColPesoAsignado
            Rs.Sort = "PesoAsignado" & TmpOrderByMode
        Case ColVolumenAsignado
            Rs.Sort = "VolumenAsignado" & TmpOrderByMode
        Case ColCodPrincipal
            Rs.Sort = "CodProducto" & TmpOrderByMode
        Case ColCantDec
            Rs.Sort = "Cant_Decimales" & TmpOrderByMode
        Case ColTipoItem
            Rs.Sort = "n_TipoPeso" & TmpOrderByMode
        Case ColCantiBul
            Rs.Sort = "n_CantiBul" & TmpOrderByMode
        Case ColPesoUni
            Rs.Sort = "PesoUni" & TmpOrderByMode
        Case ColPesoEmp
            Rs.Sort = "PesoEmp" & TmpOrderByMode
        Case ColVolUni
            Rs.Sort = "VolUni" & TmpOrderByMode
        Case ColVolEmp
            Rs.Sort = "VolEmp" & TmpOrderByMode
        Case ColCodRecolector
            Rs.Sort = "CodRecolector" & TmpOrderByMode
        Case ColNomRecolector
            Rs.Sort = "NomRecolector" & TmpOrderByMode
        Case ColCodEmpacador
            Rs.Sort = "CodEmpacador" & TmpOrderByMode
        Case ColNomEmpacador
            Rs.Sort = "NomEmpacador" & TmpOrderByMode
        Case ColEstatusLinea
            Rs.Sort = "EstatusLinea" & TmpOrderByMode
        Case ColFaseLote
            Rs.Sort = "FaseLote" & TmpOrderByMode
        Case ColFechaEmision
            Rs.Sort = "FechaEmision" & TmpOrderByMode
        Case ColFechaDespacho
            Rs.Sort = "FechaDespacho" & TmpOrderByMode
        Case ColCodLocalidadDestino
            Rs.Sort = "CodLocalidadDestino" & TmpOrderByMode
        Case ColDesLocalidadDestino
            Rs.Sort = "DesLocalidadDestino" & TmpOrderByMode
        Case ColCostoLinea
            Rs.Sort = "CostoLinea" & TmpOrderByMode
        Case Else ' Col_PP_ID
            Rs.Sort = "PP_ID" & TmpOrderByMode
    End Select
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    bar.Max = Rs.RecordCount
    bar.Min = 0
    bar.Value = 0
    bar.Visible = True
    
    Grid.Visible = False
    
    Dim Linea As Long
    
    Linea = 0
    
    While Not Rs.EOF
        
        Linea = Linea + 1
        
        bar.Value = Linea
        
        Grid.Rows = Linea + 1
        Grid.Row = Linea
        
        Grid.TextMatrix(Linea, ColNull) = Empty
        Grid.TextMatrix(Linea, ColRowID) = FormatoDecimalesDinamicos(Linea)
        
        Grid.TextMatrix(Linea, ColLote) = Rs!CodLote
        Grid.TextMatrix(Linea, ColSolicitud) = Rs!CodPedido
        Grid.TextMatrix(Linea, ColCodigoItem) = Rs!CodigoItem
        Grid.TextMatrix(Linea, ColDescItem) = Rs!DescItem
        
        Grid.TextMatrix(Linea, ColCantDisponible) = FormatoDecimalesDinamicos( _
        Rs!CantDisponible, Rs!Cant_Decimales)
        Grid.TextMatrix(Linea, ColCantAsignar) = FormatoDecimalesDinamicos( _
        Rs!CantAsignar, Rs!Cant_Decimales)
        Grid.TextMatrix(Linea, ColPesoAsignado) = FormatoDecimalesDinamicos( _
        CDec(Grid.TextMatrix(Linea, ColCantAsignar)) * CDec(Rs!PesoUni))
        Grid.TextMatrix(Linea, ColVolumenAsignado) = FormatoDecimalesDinamicos( _
        CDec(Grid.TextMatrix(Linea, ColCantAsignar)) * CDec(Rs!VolUni))
        
        Grid.TextMatrix(Linea, ColAsignar) = Empty
        
        Grid.Col = ColAsignar
        
        If CBool(Rs!TmpAsignado) Then
            Set Grid.CellPicture = Me.Picture1.Picture
            Grid.ColData(ColAsignar) = 1
            Grid.TextMatrix(Linea, ColAsignado) = "1"
        Else
            Set Grid.CellPicture = Me.Picture2.Picture
            Grid.ColData(ColAsignar) = 0
            Grid.TextMatrix(Linea, ColAsignado) = "0"
        End If
        
        Grid.TextMatrix(Linea, Col_PP_ID) = CDec(Rs!PP_ID)
        
        Grid.TextMatrix(Linea, ColCodPrincipal) = Rs!CodProducto
        Grid.TextMatrix(Linea, ColTipoItem) = CDec(Rs!n_TipoPeso)
        Grid.TextMatrix(Linea, ColCantiBul) = MinDec0(Rs!n_CantiBul)
        Grid.TextMatrix(Linea, ColCantDec) = CDec(Rs!Cant_Decimales)
        
        Grid.TextMatrix(Linea, ColPesoUni) = CDec(Rs!PesoUni)
        Grid.TextMatrix(Linea, ColPesoEmp) = CDec(Rs!PesoEmp)
        Grid.TextMatrix(Linea, ColVolUni) = CDec(Rs!VolUni)
        Grid.TextMatrix(Linea, ColVolEmp) = CDec(Rs!VolEmp)
        
        Grid.TextMatrix(Linea, ColCodRecolector) = Rs!CodRecolector
        Grid.TextMatrix(Linea, ColNomRecolector) = Rs!NomRecolector
        Grid.TextMatrix(Linea, ColCodEmpacador) = Rs!CodEmpacador
        Grid.TextMatrix(Linea, ColNomEmpacador) = Rs!NomEmpacador
        
        Grid.TextMatrix(Linea, ColEstatusLinea) = Rs!EstatusLinea
        Grid.TextMatrix(Linea, ColFaseLote) = Rs!FaseLote
        
        Grid.TextMatrix(Linea, ColFechaEmision) = Format(Rs!FechaEmision, "YYYY-MM-DD")
        Grid.TextMatrix(Linea, ColFechaDespacho) = Format(Rs!FechaDespacho, "YYYY-MM-DD")
        Grid.TextMatrix(Linea, ColCodLocalidadDestino) = Rs!CodLocalidadDestino
        Grid.TextMatrix(Linea, ColDesLocalidadDestino) = Rs!DesLocalidadDestino
        
        Grid.TextMatrix(Linea, ColCantRecolectada) = CDec(Rs!CantRecolectada)
        Grid.TextMatrix(Linea, ColCantEmpacada) = CDec(Rs!CantEmpacada)
        
        Grid.TextMatrix(Linea, ColLocalidadSolicitud) = Rs!cs_CodLocalidad_Solicitud
        Grid.TextMatrix(Linea, ColCostoLinea) = CDec(Rs!CostoLinea)
        
        Call PintarFilaGrid(Linea)
        
        Rs.MoveNext
        
        DoEvents
        
    Wend
    
    Rs.Close
    
    Grid.Col = ColAsignar
    Grid.Row = 1
    
    If Grid.Rows > 5 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(ColDescItem) = VarColWidth - ScrollGrid.Width
    Else
        Grid.ScrollBars = flexScrollBarNone
        Grid.ColWidth(ColDescItem) = VarColWidth
        ScrollGrid.Visible = False
    End If
    
    'LabelTitulo.Caption = "Consola Asignaci�n para Recolecci�n de Lote de Transferencia N� " & Lote
    
Finally:
    
    Grid.Visible = True
    bar.Visible = False
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    'Salir = True
    
    GoTo Finally
    
End Sub

Private Sub Grid_Click()
    
    Dim mTmpRowSel As Integer
    Dim mTmpColSel As Integer
    
    If mOrderByGrid_ManualColSel = ColNull Then
        mTmpRowSel = Grid.MouseRow
        mTmpColSel = Grid.Col
    Else
        mTmpRowSel = 0
        mTmpColSel = mOrderByGrid_ManualColSel
    End If
    
    If mTmpRowSel = 0 Then
        
        Select Case True 'Grid.MouseCol
            
            Case True 'ColLote, ColSolicitud, ColCodigoItem, ColDescItem, colcant
                
                If mTmpColSel = ColRowID Then
                    Exit Sub
                End If
                
                TmpCol = mTmpColSel
                
                mOrderByGrid_ManualColSel = ColNull
                
                If TmpCol <> mOrderByGrid_ColAct Then
                    mOrderByGrid_Mode = 0
                Else
                    mOrderByGrid_Mode = IIf(mOrderByGrid_Mode = 0, 1, 0)
                End If
                
                mOrderByGrid_ColAnt = mOrderByGrid_ColAct
                mOrderByGrid_ColAct = TmpCol
                
                Grid.TextMatrix(0, mOrderByGrid_ColAct) = NomCol(mOrderByGrid_ColAct) & _
                IIf(mOrderByGrid_Mode = 0, " (�)", " (�)")
                
                If mOrderByGrid_ColAct <> mOrderByGrid_ColAnt Then
                    Grid.TextMatrix(0, mOrderByGrid_ColAnt) = NomCol(mOrderByGrid_ColAnt)
                    'Grid.Col = mOrderByGrid_ColAct
                End If
                
                PreservarTmp = True
                lblBuscarSolicitudes_Click
                'PreservarTmp = False' Vamos a trabajar con memoria por defecto, y colocaremos una opci�n de limpiar.
                
        End Select
        
        Exit Sub
        
    End If
    
    Select Case mTmpColSel
        
        Case ColCantAsignar, ColAsignar
            
            Call Grid_DblClick
            
    End Select
    
End Sub

Private Sub Grid_DblClick()
    
    Select Case Grid.Col
        
        Case ColCantAsignar
            
            Dim RowsAffected
            Dim CantDisp As Variant, NewCant As Variant 'Decimal
            
            CantDisp = SDec(Grid.TextMatrix(Grid.Row, ColCantDisponible))
            
            FrmNumPadGeneral.ValorOriginal = SDec(Grid.TextMatrix(Grid.Row, ColCantAsignar))
            FrmNumPadGeneral.Show vbModal
            NewCant = SDec(FrmNumPadGeneral.ValorNumerico)
            Set FrmNumPadGeneral = Nothing
            
            If NewCant < 0 Or NewCant > CantDisp Then
                
                Mensaje True, "Cantidad inv�lida."
                
            Else
                
                Grid.TextMatrix(Grid.Row, ColCantAsignar) = FormatoDecimalesDinamicos( _
                NewCant, SDec(Grid.TextMatrix(Grid.Row, ColCantDec)))
                
                mSQL_TMP = "UPDATE TMP_LOTE_GESTION_TRANSFERENCIA SET " & _
                "CantAsignar = " & CDec(NewCant) & " " & _
                "WHERE CodUsuario = '" & FixTSQL(FrmAppLink.GetCodUsuario) & "' " & _
                "AND LAN_DeviceName = '" & FixTSQL(FrmAppLink.GetPCName) & "' " & _
                "AND PP_ID = " & SDec(Grid.TextMatrix(Grid.Row, Col_PP_ID)) & " "
                
                Ent.BDD.Execute mSQL_TMP, RowsAffected
                
                If Grid.Row < Grid.Rows - 1 Then
                    Grid.Row = Grid.Row + 1
                End If
                
            End If
            
        Case ColAsignar
            
            If SBool(Grid.TextMatrix(Grid.Row, ColAsignado)) Then
                Set Grid.CellPicture = Me.Picture2.Picture
                Grid.ColData(ColAsignar) = 0
                Grid.TextMatrix(Grid.Row, ColAsignado) = "0"
            Else
                Set Grid.CellPicture = Me.Picture1.Picture
                Grid.ColData(ColAsignar) = 1
                Grid.TextMatrix(Grid.Row, ColAsignado) = "1"
            End If
            
            mSQL_TMP = "UPDATE TMP_LOTE_GESTION_TRANSFERENCIA SET " & _
            "Asignado = " & Grid.TextMatrix(Grid.Row, ColAsignado) & " " & _
            "WHERE CodUsuario = '" & FixTSQL(FrmAppLink.GetCodUsuario) & "' " & _
            "AND LAN_DeviceName = '" & FixTSQL(FrmAppLink.GetPCName) & "' " & _
            "AND PP_ID = " & SDec(Grid.TextMatrix(Grid.Row, Col_PP_ID)) & " "
            
            Ent.BDD.Execute mSQL_TMP, RowsAffected
            
        Case Else
            
    End Select
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn, vbKeySpace, _
        vbKey0 To vbKey9, vbKeyNumpad0 To vbKeyNumpad9
            Call Grid_DblClick
    End Select
End Sub

Private Sub FrameBuscar_Click()
    Call lblBuscarSolicitudes_Click
End Sub

Private Sub lblBuscarSolicitudes_Click()
    PrepararDatos
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub Cmd_Listo_Click()
    
    On Error GoTo Error
    
    Dim ActiveTrans As Boolean
    Dim SQL As String, RowsAffected
    
    Dim TmpPK As String, TmpCod As String, TmpRs As ADODB.Recordset
    Dim TmpDoc As Variant, TmpItm As Variant
    Dim DocumentoNDT As String, mSubtotal As Variant
    
    Dim mCantDisp As Variant, mCantAsignar As Variant
    Dim SumaCantDispSel As Variant, SumaCantAsignar As Variant
    Dim SumaCantEmp As Variant, SumaCantUnd As Variant
    Dim TotalPeso As Variant, TotalVolumen As Variant
    
    Dim MinFaseLoteNuevo As Long, mObservacion As String, _
    mSolicitudesSel As String, mLotesAfectados As String
    
    MinFaseLoteNuevo = 2 ' -> Fase M�xima previo al cierre '999999
    
    SumaCantDispSel = CDec(0)
    SumaCantAsignar = CDec(0)
    
    SumaCantEmp = CDec(0)
    SumaCantUnd = CDec(0)
    
    TotalPeso = CDec(0)
    TotalVolumen = CDec(0)
    
    Dim mSeleccionados As Long
    
    mSeleccionados = 0
    
    For I = 1 To Grid.Rows - 1
        
        If SBool(Grid.TextMatrix(I, ColAsignado)) _
        And (SDec(Grid.TextMatrix(I, ColCantAsignar)) > 0 _
        Or SDec(Grid.TextMatrix(I, ColCantDisponible)) = 0) Then
            
            mSeleccionados = mSeleccionados + 1
            
            mCantDisp = SDec(Grid.TextMatrix(I, ColCantDisponible))
            mCantAsignar = SDec(Grid.TextMatrix(I, ColCantAsignar))
            
            SumaCantDispSel = SumaCantDispSel + mCantDisp
            SumaCantAsignar = SumaCantAsignar + mCantAsignar
            
        End If
        
    Next
    
    If mSeleccionados > 0 _
    And SumaCantAsignar > 0 Then
        
        mObservacion = Trim(QuickInputRequest( _
        "(Opcional) Indique alguna observaci�n para el Nuevo Lote " & _
        "si la hay, en caso contrario deje vac�o y pulse Enter", True, _
        "[**CANCEL**]", , "Escriba aqu�", , , , , , , True, , GetFont("Tahoma", 9)))
        
        If UCase(mObservacion) = "[**CANCEL**]" Then
            Exit Sub
        End If
        
        Dim mNivelTransformarLote As Long, mAutorizadoPor As String
        
        mNivelTransformarLote = Val(BuscarReglaNegocioStr("TRA_Consola_NivelTransformarLotes", 5))
        
        If mNivelTransformarLote > FrmAppLink.GetNivelUsuario Then
            
            If Not gRutinas.ShowAutorizaciones(CInt(mNivelTransformarLote), Ent.BDD) Then
                Mensaje True, FrmAppLink.Stellar_Mensaje(11010) 'Usted no posee el nivel de usuario permitido para ejecutar esta Operaci�n.
                Exit Sub
            End If
            
            mAutorizadoPor = ". Autorizado por " & _
            "[" & gRutinas.UsuarioAutorizo & "][" & gRutinas.DescriUsuarioAutorizo & "]"
            
        End If
        
        bar.Max = Grid.Rows - 1
        bar.Min = 0
        bar.Value = 0
        bar.Visible = True
        
        Ent.BDD.BeginTrans
        ActiveTrans = True
        
        Dim CodLoteNew As String
        
        Dim fCls As ClsTrans_Gestion
        
        Set fCls = New ClsTrans_Gestion
        
        CodLoteNew = Format(NO_CONSECUTIVO("Lote_Picking_Transferencia", True), "00000000#")
        
        Dim mPackingAfec As Dictionary, mPedidosAfec As Dictionary, mLotesAfec As Dictionary
        
        Set mPackingAfec = New Dictionary
        Set mPedidosAfec = New Dictionary
        Set mLotesAfec = New Dictionary
        
        ' Inicio Grid
        
        For I = 1 To Grid.Rows - 1
            
            If SBool(Grid.TextMatrix(I, ColAsignado)) _
            And (SDec(Grid.TextMatrix(I, ColCantAsignar)) > 0 _
            Or SDec(Grid.TextMatrix(I, ColCantDisponible)) = 0) Then
                
                mCantDisp = SDec(Grid.TextMatrix(I, ColCantDisponible))
                mCantAsignar = SDec(Grid.TextMatrix(I, ColCantAsignar))
                
                ' INICIO PICKING & PACKING DEL LOTE
                
                If SDec(Grid.TextMatrix(I, ColCantDisponible)) <= 0 Then
                    
                    ' Me llevo todo el Row del Lote tal y como est�
                    
                    SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                    "CodLote = '" & CodLoteNew & "' " & _
                    "WHERE CodLote = '" & Grid.TextMatrix(I, ColLote) & "' " & _
                    "AND ID = " & SDec(Grid.TextMatrix(I, Col_PP_ID)) & " "
                    
                    Ent.BDD.Execute SQL, RowsAffected
                    
                Else
                    
                    If Grid.TextMatrix(I, ColFaseLote) = 0 Then ' En Recoleccion
                        
                        ' EL INSERT ES POR CANTSOLICITADA = CANTASIGNAR (SE SUPONE QUE ES MENOR A CANT DISPONIBLE).
                        ' SI CANT ASIGNAR > RECOLECTADO, EN EL INSERT PONGO EN CANTRECOLECTADA
                        ' TODO LO RECOLECTADO DEL LOTE VIEJO Y DEJO AL LOTE VIEJO SIN RECOLECTADOS
                        ' SI CANT ASIGNAR <= LO RECOLECTADO, CANT RECOLECTADO INSERT = CANT ASIGNAR.
                        
                        SQL = "INSERT INTO MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                        "([CodLote], [CodPedido], [CodProducto], [CodRecolector], " & _
                        "[CodSupervisorPicking], [CodEmpacador], [CodSupervisorPacking], " & _
                        "[CantSolicitada], [CantRecolectada], [CantEmpacada], " & _
                        "[Picking], [Packing], [FechaAsignacion], [PackingMobile_CantEmpaques], " & _
                        "[cs_CodLocalidad_Solicitud], [CostoLinea]) " & _
                        "SELECT '" & CodLoteNew & "', [CodPedido], [CodProducto], [CodRecolector], " & _
                        "[CodSupervisorPicking], [CodEmpacador], [CodSupervisorPacking], " & _
                        "(" & CDec(mCantAsignar) & ") AS CantSolicitada, CASE WHEN " & _
                        "ROUND(CantRecolectada, (" & SDec(Grid.TextMatrix(I, ColCantDec)) & "), 0) " & _
                        "< (" & CDec(mCantAsignar) & ") THEN CantRecolectada " & _
                        "ELSE (" & CDec(mCantAsignar) & ") END AS CantRecolectada, CantEmpacada, " & _
                        "[Picking], [Packing], [FechaAsignacion], [PackingMobile_CantEmpaques], " & _
                        "[cs_CodLocalidad_Solicitud], [CostoLinea] " & _
                        "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                        "WHERE CodLote = '" & Grid.TextMatrix(I, ColLote) & "' " & _
                        "AND ID = " & SDec(Grid.TextMatrix(I, Col_PP_ID)) & " "
                        
                        Ent.BDD.Execute SQL, RowsAffected
                        
                        ' Restar Cantidad al registro en el Lote Viejo.
                        
                        SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                        "CantSolicitada = ROUND(CantSolicitada - (" & mCantAsignar & "), " & _
                        Grid.TextMatrix(I, ColCantDec) & ", 0), " & _
                        "CantRecolectada = CASE WHEN " & _
                        "ROUND(CantRecolectada - (" & mCantAsignar & "), " & _
                        Grid.TextMatrix(I, ColCantDec) & ", 0)>= 0 THEN " & _
                        "ROUND(CantRecolectada - (" & mCantAsignar & "), " & _
                        Grid.TextMatrix(I, ColCantDec) & ", 0) ELSE 0 END " & _
                        "WHERE CodLote = '" & Grid.TextMatrix(I, ColLote) & "' " & _
                        "AND ID = " & SDec(Grid.TextMatrix(I, Col_PP_ID)) & " "
                        
                        Ent.BDD.Execute SQL, RowsAffected
                        
                    ElseIf Grid.TextMatrix(I, ColFaseLote) = 1 Then ' En Empacado
                        
                        ' EL INSERT ES POR CANTSOLICITADA = CANTASIGNAR (SE SUPONE QUE ES MENOR A CANT DISPONIBLE).
                        ' CANT RECOLECTADA = CANT ASIGNAR (SE SUPONE QUE ES MENOR A CANT DISPONIBLE).
                        ' SI CANT ASIGNAR > EMPACADO, EN EL INSERT PONGO EN CANTEMPACADA
                        ' TODO LO EMPACADO DEL LOTE VIEJO Y DEJO AL LOTE VIEJO SIN EMPACADOS
                        ' SI CANT ASIGNAR <= LO EMPACADO, CANT EMPACADO INSERT = CANT ASIGNAR.
                        
                        SQL = "INSERT INTO MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                        "([CodLote], [CodPedido], [CodProducto], [CodRecolector], " & _
                        "[CodSupervisorPicking], [CodEmpacador], [CodSupervisorPacking], " & _
                        "[CantSolicitada], [CantRecolectada], [CantEmpacada], " & _
                        "[Picking], [Packing], [FechaAsignacion], [PackingMobile_CantEmpaques], " & _
                        "[cs_CodLocalidad_Solicitud], [CostoLinea]) " & _
                        "SELECT '" & CodLoteNew & "', [CodPedido], [CodProducto], [CodRecolector], " & _
                        "[CodSupervisorPicking], [CodEmpacador], [CodSupervisorPacking], " & _
                        "(" & CDec(mCantAsignar) & ") AS CantSolicitada, " & _
                        "(" & CDec(mCantAsignar) & ") AS CantRecolectada, CASE WHEN " & _
                        "ROUND(CantEmpacada, " & SDec(Grid.TextMatrix(I, ColCantDec)) & ", 0) " & _
                        "< (" & CDec(mCantAsignar) & ") THEN CantEmpacada " & _
                        "ELSE (" & CDec(mCantAsignar) & ") END AS CantEmpacada, " & _
                        "[Picking], [Packing], [FechaAsignacion], [PackingMobile_CantEmpaques], " & _
                        "[cs_CodLocalidad_Solicitud], [CostoLinea] " & _
                        "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                        "WHERE CodLote = '" & Grid.TextMatrix(I, ColLote) & "' " & _
                        "AND ID = " & SDec(Grid.TextMatrix(I, Col_PP_ID)) & " "
                        
                        Ent.BDD.Execute SQL, RowsAffected
                        
                        ' Restar Cantidad al registro en el Lote Viejo.
                        
                        SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                        "CantSolicitada = ROUND(CantSolicitada - (" & mCantAsignar & "), " & _
                        Grid.TextMatrix(I, ColCantDec) & ", 0), " & _
                        "CantRecolectada = CASE WHEN " & _
                        "ROUND(CantRecolectada - (" & mCantAsignar & "), " & _
                        Grid.TextMatrix(I, ColCantDec) & ", 0) >= 0 THEN " & _
                        "ROUND(CantRecolectada - (" & mCantAsignar & "), " & _
                        Grid.TextMatrix(I, ColCantDec) & ", 0) ELSE 0 END, " & _
                        "CantEmpacada = CASE WHEN " & _
                        "ROUND(CantEmpacada - (" & mCantAsignar & "), " & _
                        Grid.TextMatrix(I, ColCantDec) & ", 0) >= 0 THEN " & _
                        "ROUND(CantEmpacada - (" & mCantAsignar & "), " & _
                        Grid.TextMatrix(I, ColCantDec) & ", 0) ELSE 0 END " & _
                        "WHERE CodLote = '" & Grid.TextMatrix(I, ColLote) & "' " & _
                        "AND ID = " & SDec(Grid.TextMatrix(I, Col_PP_ID)) & " "
                        
                        Ent.BDD.Execute SQL, RowsAffected
                        
                    'ElseIf Grid.TextMatrix(I, ColFaseLote) = 2 Then ' Post Empacado - Pendiente por Finalizar.
                        
                        ' Igual que en Fase 1
                        
                    End If
                    
                End If
                
                ' FIN PICKING & PACKING DEL LOTE
                
                ' INICIO DE ESTRUCTURA DE MODIFICACION DE DETALLE DE LOTE
                
                TmpPK = Grid.TextMatrix(I, ColSolicitud) & ";" & _
                Grid.TextMatrix(I, ColLocalidadSolicitud)
                
                If Not mPedidosAfec.Exists(TmpPK) Then
                    
                    Set mPedidosAfec(TmpPK) = New Dictionary
                    
                    mPedidosAfec(TmpPK)("TmpPK") = TmpPK
                    mPedidosAfec(TmpPK)("LoteAnt") = Grid.TextMatrix(I, ColLote)
                    mPedidosAfec(TmpPK)("Doc") = Grid.TextMatrix(I, ColSolicitud)
                    mPedidosAfec(TmpPK)("LocalidadSolicitud") = Grid.TextMatrix(I, ColLocalidadSolicitud)
                    mPedidosAfec(TmpPK)("LoteNew") = CodLoteNew
                    
                    mPedidosAfec(TmpPK)("SumaDisponible") = CDec(0)
                    mPedidosAfec(TmpPK)("SumaAsignar") = CDec(0)
                    
                    Set mPedidosAfec(TmpPK)("Items") = New Dictionary
                    
                End If
                
                TmpCod = Grid.TextMatrix(I, ColCodPrincipal)
                
                If Not mPedidosAfec(TmpPK)("Items").Exists(TmpCod) Then
                    
                    Set mPedidosAfec(TmpPK)("Items")(TmpCod) = New Dictionary
                    
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("CodProducto") = TmpCod
                    
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("ColCantDec") = _
                    SDec(Grid.TextMatrix(I, ColCantDec))
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("ColCantiBul") = _
                    SDec(Grid.TextMatrix(I, ColCantiBul))
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("ColPesoUni") = _
                    SDec(Grid.TextMatrix(I, ColPesoUni))
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("ColPesoEmp") = _
                    SDec(Grid.TextMatrix(I, ColPesoEmp))
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("ColVolUni") = _
                    SDec(Grid.TextMatrix(I, ColVolUni))
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("ColVolEmp") = _
                    SDec(Grid.TextMatrix(I, ColVolEmp))
                    
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("CantRecolectada") = CDec(0)
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("CantEmpacada") = CDec(0)
                    
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("CantDisponible") = CDec(0)
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("CantAsignar") = CDec(0)
                    
                    mPedidosAfec(TmpPK)("Items")(TmpCod)("CostoLinea") = _
                    SDec(Grid.TextMatrix(I, ColCostoLinea)) 'CDec( _
                    BuscarValorBD_Strict("Costo", _
                    "SELECT TOP 1 ns_Costo AS Costo " & _
                    "FROM TR_REQUISICIONES TR " & _
                    "WHERE cs_Documento = '" & mPedidosAfec(TmpPK)("Doc") & "' " & _
                    "AND cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' " & _
                    "AND cs_CodArticulo = '" & TmpCod & "' ", 0))
                    
                End If
                
                '
                
                mPedidosAfec(TmpPK)("Items")(TmpCod)("CantRecolectada") = _
                mPedidosAfec(TmpPK)("Items")(TmpCod)("CantRecolectada") + _
                SDec(Grid.TextMatrix(I, ColCantRecolectada))
                
                mPedidosAfec(TmpPK)("Items")(TmpCod)("CantEmpacada") = _
                mPedidosAfec(TmpPK)("Items")(TmpCod)("CantEmpacada") + _
                SDec(Grid.TextMatrix(I, ColCantEmpacada))
                
                mPedidosAfec(TmpPK)("Items")(TmpCod)("CantDisponible") = _
                mPedidosAfec(TmpPK)("Items")(TmpCod)("CantDisponible") + _
                mCantDisp
                
                mPedidosAfec(TmpPK)("Items")(TmpCod)("CantAsignar") = _
                mPedidosAfec(TmpPK)("Items")(TmpCod)("CantAsignar") + _
                mCantAsignar
                
                ' FIN DE ESTRUCTURA DE MODIFICACION DE DETALLE DE LOTE
                
                TmpPK = Grid.TextMatrix(I, ColLote)
                
                If Not mLotesAfec.Exists(TmpPK) Then
                    
                    Set mLotesAfec(TmpPK) = New Dictionary
                    
                    mLotesAfec(TmpPK)("LoteAnt") = Grid.TextMatrix(I, ColLote)
                    
                    mLotesAfectados = mLotesAfectados & ", " & mLotesAfec(TmpPK)("LoteAnt")
                    
                End If
                
                ' INICIO DE ESTRUCTURA MODIFICACION DE CONSTANCIAS DE EMPAQUE
                
                If SDec(Grid.TextMatrix(I, ColCantEmpacada)) > 0 _
                And Grid.TextMatrix(I, ColEstatusLinea) = "2" Then ' Ya empacado, con constancia de empaque emitida.
                    
                    TmpPK = Grid.TextMatrix(I, ColLote) & ";" & Grid.TextMatrix(I, ColSolicitud) & ";" & _
                    Grid.TextMatrix(I, ColLocalidadSolicitud)
                    
                    If Not mPackingAfec.Exists(TmpPK) Then
                        
                        Set mPackingAfec(TmpPK) = New Dictionary
                        
                        mPackingAfec(TmpPK)("TmpPK") = TmpPK
                        mPackingAfec(TmpPK)("LoteAnt") = Grid.TextMatrix(I, ColLote)
                        mPackingAfec(TmpPK)("Doc") = Grid.TextMatrix(I, ColSolicitud)
                        mPackingAfec(TmpPK)("LocalidadSolicitud") = Grid.TextMatrix(I, ColLocalidadSolicitud)
                        mPackingAfec(TmpPK)("Packing") = BuscarValorBD_Strict("DocPacking", _
                        "SELECT DocPacking FROM MA_LOTE_GESTION_TRANSFERENCIA_PACKING " & _
                        "WHERE (CodLote + ';' + CodPedido + ';' + cs_CodLocalidad_Solicitud) " & _
                        "= '" & TmpPK & "' ", vbNullString)
                        
                        Set mPackingAfec(TmpPK)("Items") = New Dictionary
                        
                    End If
                    
                    TmpCod = Grid.TextMatrix(I, ColCodPrincipal)
                    
                    If Not mPackingAfec(TmpPK)("Items").Exists(TmpCod) Then
                        
                        Set mPackingAfec(TmpPK)("Items")(TmpCod) = New Dictionary
                        
                        mPackingAfec(TmpPK)("Items")(TmpCod)("CodProducto") = TmpCod
                        
                        mPackingAfec(TmpPK)("Items")(TmpCod)("ColCantDec") = _
                        SDec(Grid.TextMatrix(I, ColCantDec))
                        mPackingAfec(TmpPK)("Items")(TmpCod)("ColCantiBul") = _
                        SDec(Grid.TextMatrix(I, ColCantiBul))
                        mPackingAfec(TmpPK)("Items")(TmpCod)("ColPesoUni") = _
                        SDec(Grid.TextMatrix(I, ColPesoUni))
                        mPackingAfec(TmpPK)("Items")(TmpCod)("ColPesoEmp") = _
                        SDec(Grid.TextMatrix(I, ColPesoEmp))
                        mPackingAfec(TmpPK)("Items")(TmpCod)("ColVolUni") = _
                        SDec(Grid.TextMatrix(I, ColVolUni))
                        mPackingAfec(TmpPK)("Items")(TmpCod)("ColVolEmp") = _
                        SDec(Grid.TextMatrix(I, ColVolEmp))
                        
                        mPackingAfec(TmpPK)("Items")(TmpCod)("CantRecolectada") = CDec(0)
                        mPackingAfec(TmpPK)("Items")(TmpCod)("CantEmpacada") = CDec(0)
                        
                    End If
                    
                    'mPackingAfec(TmpPK)("Items")(TmpCod)("CantRecolectada") = _
                    mPackingAfec(TmpPK)("Items")(TmpCod)("CantRecolectada") + _
                    SDec(Grid.TextMatrix(I, ColCantRecolectada))
                    
                    mPackingAfec(TmpPK)("Items")(TmpCod)("CantEmpacada") = _
                    mPackingAfec(TmpPK)("Items")(TmpCod)("CantEmpacada") + _
                    SDec(Grid.TextMatrix(I, ColCantEmpacada))
                    
                End If
                
                ' FIN DE ESTRUCTURA DE MODIFICACION DE CONSTANCIAS DE EMPAQUE
                
                If SDec(Grid.TextMatrix(I, ColFaseLote)) < MinFaseLoteNuevo Then
                    MinFaseLoteNuevo = SDec(Grid.TextMatrix(I, ColFaseLote))
                End If
                
                ' Borrar cualquier registro que haya sido asignado por completo
                ' a un nuevo lote, del lote viejo.
                
                SQL = "DELETE FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                "WHERE CodLote = '" & Grid.TextMatrix(I, ColLote) & "' " & _
                "AND CantSolicitada <= 0 "
                
                Ent.BDD.Execute SQL, RowsAffected
                
                ' Anular el Lote Viejo en caso de que no le haya quedado nada.
                
                SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA " & _
                "SET b_Anulada = 1 " & _
                "WHERE cs_Corrida = '" & Grid.TextMatrix(I, ColLote) & "' " & _
                "AND cs_Corrida NOT IN ( " & _
                "SELECT DISTINCT CodLote " & _
                "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
                "WHERE CantSolicitada >= 0 " & _
                "AND CodLote = '" & Grid.TextMatrix(I, ColLote) & "' " & _
                ") "
                
                Ent.BDD.Execute SQL, RowsAffected
                
                'Debug.Assert True
                
                ' Pendiente verificar
                ' CON RESPECTO AL TR_LOTE.
                ' SI ME LLEVO UNA SOLICITUD COMPLETA, SE LA QUITO AL LOTE VIEJO
                ' SI ME LLEVO UNA SOLICITUD DE MANERA PARCIAL (VOY LLEVANDO EL CONTROL EN OTRO DICTIONARY)
                ' ENTONCES DEBO RECALCULAR SU N_SUBTOTAL EN BASE AL DETALLE
                ' (AL IGUAL QUE SUS CANTIDADES, EMPAQUES Y PESOS)
                
                ' OJO PENDIENTE VALIDAR QUE PODRIA PASAR EN CONSOLA AL CERRAR UN LOTE
                ' ANTE LA PRESENCIA DE UNA MISMA SOLICITUD EN MAS DE UN LOTE. DE IGUAL MANERA
                ' EN REPORTES, EVITAR DUPLICACIONES EXTRA�AS POR JOINS FRAGILES.
                
                ' FINALMENTE, PEDIR CLASIFICACION PARA EL NUEVO LOTE.
                
            End If
            
        Next
        
        ' Fin Grid
        
        ' Inicio Packing
        
        For Each TmpDoc In AsEnumerable(mPackingAfec.Items())
            
            DocumentoNDT = Format(NO_CONSECUTIVO("Packing_Transferencia", True), "00000000#")
            
            TmpDoc("NewPacking") = DocumentoNDT
            TmpDoc("ItemCount") = CLng(0)
            mSubtotal = CDec(0)
            
            For Each TmpItm In AsEnumerable(TmpDoc("Items").Items())
                
                TmpDoc("ItemCount") = TmpDoc("ItemCount") + 1
                
                SQL = "INSERT INTO TR_PACKING_TRANSFERENCIA " & _
                "([c_Documento], [n_Linea], [c_CodArticulo], [c_Descripcion], " & _
                "[n_Cantidad], [n_Costo], [ns_CantidadEmpaque], [cs_CodLocalidad]) " & _
                "SELECT " & _
                "'" & DocumentoNDT & "', (" & CDec(TmpDoc("ItemCount")) & "), c_CodArticulo, " & _
                "c_Descripcion, (" & TmpItm("CantEmpacada") & ") AS n_Cantidad, n_Costo, " & _
                "ns_CantidadEmpaque, cs_CodLocalidad " & _
                "FROM TR_PACKING_TRANSFERENCIA " & _
                "WHERE c_Documento = '" & TmpDoc("Packing") & "' " & _
                "AND cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' " & _
                "AND c_CodArticulo = '" & TmpItm("CodProducto") & "' "
                
                Ent.BDD.Execute SQL, RowsAffected
                
                ' Restar Cantidad al packing viejo.
                
                Set TmpRs = ExecuteSafeSQL( _
                "SELECT * FROM TR_PACKING_TRANSFERENCIA TMP " & _
                "WHERE c_Documento = '" & TmpDoc("Packing") & "' " & _
                "AND cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' " & _
                "AND c_CodArticulo = '" & TmpItm("CodProducto") & "' " & _
                "AND n_Cantidad > 0 ", _
                Ent.BDD, RowsAffected, True, False, True)
                
                mCantDisp = TmpItm("CantEmpacada")
                
                If TmpRs.RecordCount > 0 Then
                    
                    Do While (Not TmpRs.EOF) And (mCantDisp > 0)
                        
                        mCantAsignar = RoundDecimalUp(IIf( _
                        CDec(TmpRs!n_Cantidad) > TmpItm("CantEmpacada"), _
                        TmpItm("CantEmpacada"), CDec(TmpRs!n_Cantidad)), TmpItm("ColCantDec"))
                        
                        mCantDisp = RoundDecimalUp(mCantDisp - mCantAsignar, TmpItm("ColCantDec"))
                        
                        TmpRs!n_Cantidad = RoundDecimalUp( _
                        CDec(TmpRs!n_Cantidad) - mCantAsignar, TmpItm("ColCantDec"))
                        
                        TmpRs.MoveNext
                        
                    Loop
                    
                    TmpRs.UpdateBatch
                    
                End If
                
                TmpRs.Close
                
                ' Borrar cualquier row del packing viejo que haya quedado con cantidad cero.
                
                SQL = "DELETE FROM TR_PACKING_TRANSFERENCIA " & _
                "WHERE c_Documento = '" & TmpDoc("Packing") & "' " & _
                "AND cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' " & _
                "AND n_Cantidad <= 0 "
                
                Ent.BDD.Execute SQL, RowsAffected
                
            Next
            
            ' Calcular Subtotal del Nuevo Packing
            
            mSubtotal = CDec(BuscarValorBD_Strict("Subtotal", _
            "SELECT isNULL(SUM(n_Cantidad * n_Costo), 0) AS Subtotal " & _
            "FROM TR_PACKING_TRANSFERENCIA " & _
            "WHERE c_Documento = '" & DocumentoNDT & "' " & _
            "AND cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' ", 0))
            
            SQL = "INSERT INTO MA_PACKING_TRANSFERENCIA " & _
            "([c_Documento], [d_Fecha], [c_Status], [c_CodLocalidad_Destino], " & _
            "[c_CodDeposito_Destino], [cs_Observacion], [c_Relacion], [cs_CodLocalidad_Relacion], " & _
            "[c_Usuario], [n_CantEmpaques], [n_Subtotal], [c_CodMoneda], [n_FactorCambio], " & _
            "[cs_CodLocalidad], [cs_Numero_Transferencia], [cs_NumTransf_Destino]) " & _
            "SELECT " & _
            "'" & DocumentoNDT & "', GETDATE(), 'DPE', c_CodLocalidad_Destino, " & _
            "c_CodDeposito_Destino, cs_Observacion + '-> Lote Transformado N� [" & CodLoteNew & "]', " & _
            "c_Relacion, cs_CodLocalidad_Relacion, '" & FrmAppLink.GetCodUsuario & "', n_CantEmpaques, " & _
            "(" & mSubtotal & "), c_CodMoneda, n_FactorCambio, cs_CodLocalidad, '', '' " & _
            "FROM MA_PACKING_TRANSFERENCIA " & _
            "WHERE c_Documento = '" & TmpDoc("Packing") & "' " & _
            "AND cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' "
            
            Ent.BDD.Execute SQL, RowsAffected
            
            ' Por si acaso, borrar cualquier constancia de empaque que haya quedado
            ' sin items.
            
            SQL = "DELETE FROM MA_PACKING_TRANSFERENCIA " & _
            "WHERE c_Documento = '" & TmpDoc("Packing") & "' " & _
            "AND cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' " & _
            "AND (c_Documento + ';' + cs_CodLocalidad) NOT IN ( " & _
            "SELECT (c_Documento + ';' + cs_CodLocalidad) " & _
            "FROM TR_PACKING_TRANSFERENCIA " & _
            "WHERE c_Documento = '" & TmpDoc("Packing") & "' " & _
            "AND cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' " & _
            ") "
            
            Ent.BDD.Execute SQL, RowsAffected
            
            mSubtotal = CDec(BuscarValorBD_Strict("Subtotal", _
            "SELECT isNULL(SUM(n_Cantidad * n_Costo), 0) AS Subtotal " & _
            "FROM TR_PACKING_TRANSFERENCIA " & _
            "WHERE c_Documento = '" & TmpDoc("Packing") & "' " & _
            "AND cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' ", 0))
            
            'If mSubtotal <> 0 Then
                
                SQL = "UPDATE MA_PACKING_TRANSFERENCIA SET " & _
                "n_Subtotal = (" & mSubtotal & ") " & _
                "WHERE c_Documento = '" & TmpDoc("Packing") & "' " & _
                "AND cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' "
                
                Ent.BDD.Execute SQL, RowsAffected
                
            'End If
            
        Next
        
        ' Fin Packing
        
        ' Inicio Solicitudes
        
        SumaCantEmp = CDec(0)
        SumaCantUnd = CDec(0)
        
        TotalPeso = CDec(0)
        TotalVolumen = CDec(0)
        
        mSolicitudesSel = Empty
        
        For Each TmpDoc In AsEnumerable(mPedidosAfec.Items())
            
            TmpDoc("Rows") = CLng(0)
            
            TmpDoc("Subtotal") = CDec(0)
            TmpDoc("CantEmpaquesAsignar") = CDec(0)
            TmpDoc("CantUnidadesRestantesAsignar") = CDec(0)
            TmpDoc("CantTotalUnidades") = CDec(0)
            TmpDoc("TotalPeso") = CDec(0)
            TmpDoc("TotalVolumen") = CDec(0)
            
            mSubtotal = CDec(0)
            
            For Each TmpItm In AsEnumerable(TmpDoc("Items").Items())
                
                TmpDoc("Rows") = TmpDoc("Rows") + 1
                
                TmpDoc("Subtotal") = TmpDoc("Subtotal") + _
                (TmpItm("CostoLinea") * TmpItm("CantAsignar"))
                
                If mvarFormaTrabajoTransferencias = 0 Then
                    
                    TmpItm("CantEmpaquesAsignar") = CDec(0)
                    TmpItm("CantUnidadesRestantesAsignar") = TmpItm("CantAsignar")
                    
                Else
                    
                    TmpItm("CantEmpaquesAsignar") = _
                    Fix(TmpItm("CantAsignar") / TmpItm("ColCantiBul"))
                    
                    TmpItm("CantUnidadesRestantesAsignar") = RoundDecimalUp( _
                    TmpItm("CantAsignar") - _
                    (TmpItm("CantEmpaquesAsignar") * TmpItm("ColCantiBul")), TmpItm("ColCantDec"))
                    
                End If
                
                TmpDoc("CantEmpaquesAsignar") = TmpDoc("CantEmpaquesAsignar") + _
                TmpItm("CantEmpaquesAsignar")
                
                TmpDoc("CantUnidadesRestantesAsignar") = TmpDoc("CantUnidadesRestantesAsignar") + _
                TmpItm("CantUnidadesRestantesAsignar")
                
                TmpDoc("CantTotalUnidades") = TmpDoc("CantTotalUnidades") + _
                TmpItm("CantAsignar")
                
                If mvarFormaTrabajoTransferencias = 0 Then
                    
                    TmpDoc("TotalPeso") = TmpDoc("TotalPeso") + _
                    (TmpItm("CantAsignar") * TmpItm("ColPesoUni"))
                    
                    TmpDoc("TotalVolumen") = TmpDoc("TotalVolumen") + _
                    (TmpItm("CantAsignar") * TmpItm("ColVolUni"))
                    
                Else
                    
                    TmpDoc("TotalPeso") = TmpDoc("TotalPeso") + _
                    (TmpItm("CantEmpaquesAsignar") * TmpItm("ColPesoEmp")) + _
                    (TmpItm("CantUnidadesRestantesAsignar") * TmpItm("ColPesoUni"))
                    
                    TmpDoc("TotalVolumen") = TmpDoc("TotalVolumen") + _
                    (TmpItm("CantEmpaquesAsignar") * TmpItm("ColVolEmp")) + _
                    (TmpItm("CantUnidadesRestantesAsignar") * TmpItm("ColVolUni"))
                    
                End If
                
            Next
            
            SQL = "INSERT INTO TR_LOTE_GESTION_TRANSFERENCIA " & _
            "([cs_Corrida], [cs_CodLocalidad], [c_Documento], [cs_CodLocalidad_Solicitud], " & _
            "[c_CodLocalidad_Destino], [d_Fecha], [c_Documento_Nota], [c_Documento_Trans], " & _
            "[n_Subtotal], [n_CantEmpaques], [n_CantUnidades], [n_TotalUnidades], " & _
            "[n_TotalPeso], [n_TotalVolumen]) " & _
            "SELECT " & _
            "'" & CodLoteNew & "', '" & FrmAppLink.GetCodLocalidadSistema & "', '" & TmpDoc("Doc") & "', " & _
            "'" & TmpDoc("LocalidadSolicitud") & "', c_CodLocalidad_Destino, GetDate(), " & _
            "'', '', (" & TmpDoc("Subtotal") & "), (" & TmpDoc("CantEmpaquesAsignar") & "), " & _
            "(" & TmpDoc("CantUnidadesRestantesAsignar") & "), (" & TmpDoc("CantTotalUnidades") & "), " & _
            "(" & TmpDoc("TotalPeso") & "), (" & TmpDoc("TotalVolumen") & ") " & _
            "FROM TR_LOTE_GESTION_TRANSFERENCIA " & _
            "WHERE cs_Corrida = '" & TmpDoc("LoteAnt") & "' " & _
            "AND c_Documento = '" & TmpDoc("Doc") & "' " & _
            "AND cs_CodLocalidad_Solicitud = '" & TmpDoc("LocalidadSolicitud") & "' "
            
            Ent.BDD.Execute SQL, RowsAffected
            
            SumaCantEmp = SumaCantEmp + TmpDoc("CantEmpaquesAsignar")
            SumaCantUnd = SumaCantUnd + TmpDoc("CantUnidadesRestantesAsignar")
            TotalPeso = TotalPeso + TmpDoc("TotalPeso")
            TotalVolumen = TotalVolumen + TmpDoc("TotalVolumen")
            
            mSolicitudesSel = mSolicitudesSel & ", [" & TmpDoc("Doc") & "]"
            
        Next
        
        ' Fin Solicitudes
        
        ' AGREGAR A ESTA FUNCIONALIDAD REGLA DE NEGOCIO POR NIVEL (QUIEN PUEDE CREAR UN NUEVO LOTE)
        ' GRABAR UNA AUDITORIA POR CADA NUEVO LOTE Y QUIEN LO HIZO
        ' GRABAR EN LA OBSERVACION DEL NUEVO LOTE, UNA REFERENCIA A LOS LOTES VIEJOS (AFECTADOS)
        ' POR CADA DISTINCT LOTE AFECTADO.
        
        ' AL GRABAR EL NUEVO LOTE, HAY QUE DETERMINAR POR UN QUERY, EN QUE FASE VA A QUEDAR,
        ' EN BASE A LOS REGISTROS RECOGIDOS DE LOS LOTES VIEJOS. PARA QUE EL LOTE NUEVO QUEDE
        ' EN UNA FASE AVANZADA, TODOS SUS DETALLES DEBEN TENER DICHA FASE FINALIZADA. EN CASO
        ' CONTRARIO, EL LOTE NUEVO QUEDA EN LA MENOR FASE EN LA CUAL ESTEN SUS DETALLES
        
        SQL = "INSERT INTO MA_LOTE_GESTION_TRANSFERENCIA " & _
        "([cs_Corrida], [d_Fecha], [d_FechaCorrida], [c_Usuario], [c_Transporte], " & _
        "[b_Finalizada], [b_Anulada], [b_PickingFinalizado], [b_PackingFinalizado], " & _
        "[n_TipoSeparacionDetalle], [c_Observacion], [c_Clasificacion]) " & _
        "SELECT '" & CodLoteNew & "', GetDate(), GetDate(), '" & FrmAppLink.GetCodUsuario & "', " & _
        "'" & FixTSQL(txtTransporte.Text) & "', 0, 0, " & IIf(MinFaseLoteNuevo >= 1, 1, 0) & ", " & _
        IIf(MinFaseLoteNuevo >= 2, 1, 0) & ", 1, 'Lote Transformado N� [" & CodLoteNew & "]. " & _
        FixTSQL(mObservacion) & "', '" & FixTSQL(CmbClasificacion.Text) & "' "
        
        Ent.BDD.Execute SQL, RowsAffected
        
        mSolicitudesSel = Mid(mSolicitudesSel, 3)
        mLotesAfectados = Mid(mLotesAfectados, 3)
        
        Call InsertarAuditoria(470, "Consola de Transferencia - Transformaci�n de Lotes", _
        "Se cre� lote transformado N� [" & CodLoteNew & "]" & mAutorizadoPor & ". " & _
        "Solicitudes Incluidas: " & mSolicitudesSel & ". Lotes Modificados: " & mLotesAfectados & ".", _
        "FrmTransferencia_ModificarLote", "Lote de Transferencias", CodLoteNew, Ent.BDD)
        
        Ent.BDD.CommitTrans
        ActiveTrans = False
        
        GeneroLote = True
        LoteNuevo = CodLoteNew
        
        PreservarTmp = False ' Limpiar la memoria de este usuario para que al crear un nuevo lote no esten marcadas las mismas cosas que antes.
        lblBuscarSolicitudes_Click
        
        Unload Me
        
        Exit Sub
        
    Else
        
        Mensaje True, "Debe seleccionar las filas que van a formar parte del lote, y " & _
        "la cantidad de art�culos a asignar debe ser mayor a cero (0). "
        
    End If
    
Finally:
    
    bar.Visible = False
    
    Exit Sub
    
Error:
    
    'Resume ' Debug
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If ActiveTrans Then
        
        Ent.BDD.RollbackTrans
        ActiveTrans = False
        
    End If
    
    ActivarMensajeGrande 80
    Mensaje True, "Error al generar nuevo lote. Informaci�n Adicional: " & _
    mErrorDesc & " (" & mErrorNumber & ")"
    
    GoTo Finally
    
End Sub

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute( _
    "SELECT C_PROCESO " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE c_Proceso = '005'")
    
    If Not TempRs.EOF Then
        frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    End If
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub

Private Sub Departamento_Change()
    If Departamento.Text = Empty Then
        Grupo.Text = vbNullString
        Grupo.Enabled = False
        Subgrupo.Text = vbNullString
        Subgrupo.Enabled = False
    Else
        Grupo.Enabled = True
    End If
End Sub

Private Sub Departamento_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case vbKeyF2
            
            Tecla_Pulsada = True
            
            Set Forma = Me
            Tabla = "MA_DEPARTAMENTOS"
            Titulo = UCase(FrmAppLink.StellarMensaje(361)) '"D E P A R T A M E N T O S"
            Call DGS(Forma, Titulo, Tabla)
            
            Tecla_Pulsada = False
            
        Case vbKeyBack, vbKeyDelete, vbKeySpace
            
            LcDepartamento = vbNullString
            Departamento.Text = vbNullString
            lbl_Departamento.Caption = vbNullString
            LcGrupo = vbNullString
            Grupo.Text = vbNullString
            lbl_Grupo.Caption = vbNullString
            LcSubGrupo = vbNullString
            Subgrupo.Text = vbNullString
            lbl_Subgrupo.Caption = vbNullString
            
    End Select
    
End Sub

Private Sub Departamento_LostFocus()
    If Departamento.Text <> Empty Then
        Set Forma = Me
        Tabla = "MA_DEPARTAMENTOS"
        Titulo = UCase(FrmAppLink.StellarMensaje(361)) '"D E P A R T A M E N T O S"
        Call PrepararDGS(Forma, Titulo, Tabla)
        Call Carga_DGS(Departamento, "MA_DEPARTAMENTOS", 0)
    Else
        Call Departamento_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub Grupo_Change()
    If Grupo.Text = Empty Then
        Subgrupo.Text = Empty
        Subgrupo.Enabled = False
    Else
        Subgrupo.Enabled = True
    End If
End Sub

Private Sub Grupo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case vbKeyF2
            
            Set Forma = Me
            Tabla = "MA_GRUPOS"
            Titulo = UCase(FrmAppLink.StellarMensaje(362))
            
            Call DGS(Forma, Titulo, Tabla)
            
        Case vbKeyBack, vbKeyDelete, vbKeySpace
            
            LcGrupo = vbNullString
            Grupo.Text = vbNullString
            lbl_Grupo = vbNullString
            Subgrupo.Text = vbNullString
            LcSubGrupo = vbNullString
            lbl_Subgrupo = vbNullString
            
    End Select
    
End Sub

Private Sub Grupo_LostFocus()
    If Grupo.Text <> Empty Then
        Set Forma = Me
        Tabla = "MA_GRUPOS"
        Titulo = UCase(FrmAppLink.StellarMensaje(362)) 'G R U P O S"
        Call PrepararDGS(Forma, Titulo, Tabla)
        Call Carga_DGS(Grupo, "MA_GRUPOS", 1)
    Else
        Call Grupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub Subgrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case vbKeyF2
            
            Set Forma = Me
            Tabla = "MA_SUBGRUPOS"
            Titulo = UCase(FrmAppLink.StellarMensaje(363))
            Call DGS(Forma, Titulo, Tabla)
            
        Case vbKeyBack, vbKeyDelete, vbKeySpace
            
            LcSubGrupo = Empty
            Subgrupo.Text = Empty
            lbl_Subgrupo = Empty
            
    End Select
    
End Sub

Private Sub Subgrupo_LostFocus()
    If Subgrupo.Text <> Empty Then
        Set Forma = Me
        Tabla = "MA_SUBGRUPOS"
        Titulo = UCase(FrmAppLink.StellarMensaje(363)) 'S U B - G R U P O S"
        Call PrepararDGS(Forma, Titulo, Tabla)
        Call Carga_DGS(Subgrupo, "MA_SUBGRUPOS", 2)
    Else
        Call Subgrupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub CmdDepartamento_Click()
    Departamento.SetFocus
    Call Departamento_KeyDown(vbKeyF2, 0)
End Sub

Private Sub CmdGrupo_Click()
    If Grupo.Enabled Then
        Grupo.SetFocus
        Call Grupo_KeyDown(vbKeyF2, 0)
    End If
End Sub

Private Sub CmdSubgrupo_Click()
    If Subgrupo.Enabled Then
        Subgrupo.SetFocus
        Call Subgrupo_KeyDown(vbKeyF2, 0)
    End If
End Sub

Private Sub txt_Producto_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim mArrProducto As Variant
    
    Select Case KeyCode
        
        Case vbKeyF2
            
            Tecla_Pulsada = True
            
            mArrProducto = BuscarInfoProducto_Basica(, , , , , VistaMarca, , False)
            
            If Not IsEmpty(mArrProducto) Then
                txt_Producto.Text = mArrProducto(0)
                'lbl_producto.Caption = mArrProducto(1)
                txt_Producto_LostFocus
            Else
                txt_Producto.Text = Empty
                lbl_Producto.Caption = Empty
            End If
            
        Case vbKeyReturn
            
            txt_Producto_LostFocus
            
        Case vbKeyBack, vbKeyDelete, vbKeySpace
            
            txt_Producto.Text = Empty
            lbl_Producto.Caption = Empty
            
    End Select
    
End Sub

Private Sub txt_Producto_LostFocus()
    
    Dim cProducto As String, RsProducto As New ADODB.Recordset
    Dim cDepGruSub As String
    
    If Trim(txt_Producto.Text) = Empty Then
        lbl_Producto.Caption = Empty
        Exit Sub
    End If
    
    cProducto = _
    "SELECT ma_productos.c_Codigo, c_Descri " & _
    "from ma_productos " & _
    "inner join ma_codigos " & _
    "on ma_codigos.c_CodNasa = ma_productos.c_Codigo " & _
    "where ma_codigos.c_Codigo = '" & FixTSQL(Trim(txt_Producto.Text)) & "' "
    
    Call Apertura_Recordset(RsProducto)
    
    RsProducto.Open cProducto, Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If Not RsProducto.EOF Then
        txt_Producto.Text = RsProducto!c_Codigo
        lbl_Producto.Caption = RsProducto!c_Descri
    Else
        lbl_Producto.Caption = Empty
        txt_Producto.Text = Empty
    End If
    
    Call Cerrar_Recordset(RsProducto)
    
End Sub

Private Sub CmdProducto_Click()
    Tecla_Pulsada = True
    Call txt_Producto_KeyDown(vbKeyF2, 0)
    Tecla_Pulsada = False
End Sub

Private Sub txtLocalidades_Click()
    
    If Arr_Localidades Is Nothing Then
        
        CmdLocalidades_Click
        Exit Sub
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_Localidades
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "Lista de Localidades: " & GetLines(2) & ListaStr & "", _
            2500, 4000, txtLocalidades, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "8", True), , , 200
            
        End If
        
    End If
    
End Sub

Private Sub txtLocalidades_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Dim SQL As String
            
            SQL = "SELECT c_Codigo, c_Descripcion FROM MA_SUCURSALES "
            
            With FrmAppLink.GetFrmSuperConsultas
                
                .Inicializar SQL, StellarMensaje(1251), Ent.BDD, , True
                
                .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2000, 0
                .Add_ItemLabels StellarMensaje(143), "c_Descripcion", 9000, 0
                
                .Add_ItemSearching StellarMensaje(143), "c_Descripcion"
                .Add_ItemSearching StellarMensaje(142), "c_Codigo"
                
                .StrOrderBy = "c_Descripcion"
                
                .BusquedaInstantanea = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .ModoDespliegueInstantaneo = True
                
                .Show vbModal
                
                Set Arr_Localidades = .ArrResultado
                
                If Arr_Localidades Is Nothing Then
                    
                    txtLocalidades.Text = Empty
                    txtLocalidades.Tag = Empty
                    
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_Localidades
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txtLocalidades.Text = ListaStr
                    
                    txtLocalidades_Click
                    
                    lblBuscarSolicitudes_Click
                    
                End If
                
            End With
            
            Set Frm_Super_Consultas = Nothing
            
        Case vbKeyDelete, vbKeyBack
            
            txtLocalidades.Text = Empty
            txtLocalidades.Tag = Empty
            
            Set Arr_Localidades = Nothing
            
    End Select
    
End Sub

Private Sub CmdLocalidades_Click()
    Call txtLocalidades_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtLote_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyBack _
    Or KeyCode = vbKeyDelete _
    Or KeyCode = vbKeySpace Then
        
        txtLote.Text = Empty
        
    ElseIf KeyCode = vbKeyF2 Then
        
        btnLote_Click
        
    End If
    
End Sub

Private Sub btnLote_Click()
    
    Dim mResul As Variant
    Dim mConsulta As String
    
    mEstCerrado = "Finalizado"
    mEstPreCierre = "Por aprobar (Listo para Transferir)"
    mEstPacking = "En proceso de Empacado"
    mEstPicking = "En proceso de Recolecci�n"
    
    mConsulta = "SELECT TOP (100) cs_Corrida, d_Fecha, b_Finalizada AS Finalizada, " & _
    "b_PackingFinalizado, b_PickingFinalizado, CASE WHEN b_Finalizada = 1 THEN '" & mEstCerrado & "' " & _
    "WHEN b_PackingFinalizado = 1 THEN '" & mEstPreCierre & "' " & _
    "WHEN b_PickingFinalizado = 1 THEN '" & mEstPacking & "' " & _
    "ELSE '" & mEstPicking & "' END AS Estado FROM MA_LOTE_GESTION_TRANSFERENCIA "
    
    Dim mFrm_Super_Consultas As Object
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With mFrm_Super_Consultas
        
        .Inicializar mConsulta, "�ltimos 100 Lotes de Pedidos.", Ent.BDD
        
        .Add_ItemLabels "Lote", "cs_Corrida", 1545, 0
        .Add_ItemLabels "Fecha", "d_Fecha", 3495, 0, , Array("Fecha", "VbGeneralDate")
        .Add_ItemLabels "Estado", "Estado", 6060, 0
        .Add_ItemLabels "Finalizada", "Finalizada", 0, 0
        .Add_ItemLabels "Packing", "b_PackingFinalizado", 0, 0
        .Add_ItemLabels "Picking", "b_PickingFinalizado", 0, 0
        
        .Add_ItemSearching "N� Lote", "cs_Corrida"
        .Add_ItemSearching "Fecha", "CONVERT(NVARCHAR(MAX), d_Fecha, 103)"
        
        .txtDato.Text = "%"
        .StrOrderBy = "cs_Corrida DESC"
        
        .Show vbModal
        
        mResul = .ArrResultado()
        
        If Not IsEmpty(mResul) Then
            
            If Trim(mResul(0)) <> Empty Then
                
                Me.txtLote.Text = CStr(mResul(0))
                
                If mResul(3) Then
                    fCls.FasePickingPacking = 3
                ElseIf mResul(4) Then
                    fCls.FasePickingPacking = 2
                ElseIf mResul(5) Then
                    fCls.FasePickingPacking = 1
                Else
                    fCls.FasePickingPacking = 0
                End If
                
            End If
            
        Else
            
            Me.txtLote.Text = Empty
            DatosLote = Empty
            
        End If
    
    End With
    
    Set mFrm_Super_Consultas = Nothing
    
End Sub

Private Sub CmbClasificacion_Click()
    
    If Not EventoProgramado Then
        
        Static PosAnterior As Long
        
        If (PosAnterior <> CmbClasificacion.ListIndex _
        And CmbClasificacion.ListIndex = CmbClasificacion.ListCount - 1) _
        Or CmbClasificacion.ListCount = 1 Then
            PosAnterior = CmbClasificacion.ListIndex
            EventoProgramado = True
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CmbClasificacion
            mClsGrupos.CargarComboGrupos Ent.BDD, CmbClasificacion
            FrmAppLink.ListRemoveItems CmbClasificacion, "Ninguno" ' Quitar dichos elementos unicamente aqu� sin alterar la funci�n anterior.
            EventoProgramado = False
        ElseIf PosAnterior <> CmbClasificacion.ListIndex _
        And CmbClasificacion.ListIndex < CmbClasificacion.ListCount - 1 Then
            PosAnterior = CmbClasificacion.ListIndex
        End If
        
    End If
    
End Sub

' --------------------------------------------

Private Sub cmdTransporte_Click()
    
    Dim mCls As recsun.obj_busqueda
    Dim mBus As Variant
    Dim mResul As Variant
    
    mConsulta = _
    "Select c_CodTransporte, c_Descripcion, c_Marca, c_Modelo " & _
    "FROM MA_TRANSPORTE"
    
    Dim mFrm_Super_Consultas As Object
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With mFrm_Super_Consultas
        
        .Inicializar mConsulta, "Transporte", Ent.BDD
        
        .Add_ItemLabels "CODIGO", "c_CodTransporte", 1935, 0
        .Add_ItemLabels "DESCRIPCION", "c_Descripcion", 6000, 0
        .Add_ItemLabels "Marca", "c_Marca", 1500, 0
        .Add_ItemLabels "Modelo", "c_Modelo", 1500, 0
        
        .Add_ItemSearching "CODIGO", "c_CodTransporte"
        .Add_ItemSearching "DESCRIPCION", "c_Descripcion"
        .Add_ItemSearching "Marca", "c_Marca"
        .Add_ItemSearching "Modelo", "c_Modelo"
        
        .txtDato.Text = "%"
        .StrOrderBy = "c_Descripcion"
        
        .Show vbModal
        
        mResul = .ArrResultado()
        
        If Not IsEmpty(mResul) Then
            If Trim(mResul(0)) <> Empty Then
                Me.txtTransporte.Text = CStr(mResul(0))
                txtTransporte_LostFocus
            End If
        Else
            Me.txtTransporte.Text = Empty
            Me.lblResponsable = Empty
        End If
        
    End With
    
    Set mFrm_Super_Consultas = Nothing
    
End Sub

Public Function BuscarTransporte(ByVal pCodigo As String) As Boolean
    Set mTransporte = New ClsTransporte
    BuscarTransporte = mTransporte.BuscarTransporte(Ent.BDD, pCodigo)
End Function

Private Sub txtTransporte_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF2
            cmdTransporte_Click
        Case vbKeyBack, vbKeyDelete, vbKeySpace
            LimpiarControlesT
    End Select
End Sub

Private Sub txtTransporte_KeyPress(KeyAscii As Integer)
    KeyAscii = IIf(KeyAscii = Asc("'"), 0, KeyAscii)
End Sub

Private Sub txtTransporte_LostFocus()
    If BuscarTransporte(txtTransporte.Text) Then
        LlenarDatosTransporte
    Else
        LimpiarControlesT
    End If
End Sub

Private Sub LlenarDatosTransporte()
    With mTransporte
        txtTransporte.Text = .Codigo
        lblDescri.Caption = .Descripcion
        'lblMarca.Caption = .Marca
        'lblModelo.Caption = .Modelo
        'lblPlaca.Caption = .Placa
        lblResponsable.Caption = .Responsable
        'CalculoTransporte
    End With
End Sub

Private Sub LimpiarControlesT()
    txtTransporte.Text = Empty
    lblDescri.Caption = Empty
    'lblMarca.Caption = Empty
    'lblModelo.Caption = Empty
    'lblPlaca.Caption = Empty
    lblResponsable.Caption = Empty
    'lblPesoV.Caption = "Peso: "
    'lblvolumenV.Caption = "Volumen: "
    'lblPesoD.Caption = "Peso: "
    'lblvolumenD.Caption = "Volumen: "
    'lblPesoP.Caption = "Peso: "
    'lblvolumenP.Caption = "Volumen: "
End Sub
