VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsTransporte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarCodigo                                                                  As String
Private mvarResponsable                                                             As String
Private mvarDescripcion                                                             As String
Private mvarPlaca                                                                   As String
Private mvarMarca                                                                   As String
Private mvarModelo                                                                  As String
Private mvarCapacidadPeso                                                           As Double
Private mvarCapacidadVolumen                                                        As Double

Property Get Codigo() As String
    Codigo = mvarCodigo
End Property

Property Get Responsable() As String
    Responsable = mvarResponsable
End Property

Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Property Get Placa() As String
    Placa = mvarPlaca
End Property

Property Get Marca() As String
    Marca = mvarMarca
End Property

Property Get Modelo() As String
    Modelo = mvarModelo
End Property

Property Get CapacidadPeso() As Double
    CapacidadPeso = mvarCapacidadPeso
End Property

Property Get CapacidadVolumen() As Double
    CapacidadVolumen = mvarCapacidadVolumen
End Property

Function BuscarTransporte(pCn As ADODB.Connection, pCodigo As String) As Boolean
    
    Dim mRs As New ADODB.Recordset
    Dim mSQl As String
    
    IniciarProperty
    
    mSQl = "SELECT * FROM MA_TRANSPORTE " & _
    "WHERE c_CodTransporte = '" & pCodigo & "' "
    
    mRs.Open mSQl, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarTransporte = AsignarPropertyRs(mRs)
    End If
    
    mRs.Close
    
End Function

Private Sub IniciarProperty()
    mvarCodigo = Empty
    mvarResponsable = Empty
    mvarDescripcion = Empty
    mvarPlaca = Empty
    mvarMarca = Empty
    mvarModelo = Empty
    mvarCapacidadPeso = 0
    mvarCapacidadVolumen = 0
End Sub

Private Function AsignarPropertyRs(pRs As ADODB.Recordset) As Boolean
    mvarCodigo = pRs!c_CodTransporte
    mvarResponsable = pRs!c_Responsable
    mvarDescripcion = pRs!c_Descripcion
    mvarPlaca = pRs!c_Placa
    mvarMarca = pRs!c_Marca
    mvarModelo = pRs!c_Modelo
    mvarCapacidadPeso = pRs!n_Peso
    mvarCapacidadVolumen = pRs!n_Volumen
    AsignarPropertyRs = True
End Function
