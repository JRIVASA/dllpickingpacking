VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmTransferencia_EntregaPedido 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtLote 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   495
      Left            =   2520
      Locked          =   -1  'True
      TabIndex        =   21
      Top             =   960
      Width           =   3375
   End
   Begin VB.TextBox txtLocalidad 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   495
      Left            =   2520
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   20
      Top             =   1800
      Width           =   3375
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2655
      Left            =   120
      TabIndex        =   12
      Top             =   720
      Width           =   6975
      Begin VB.TextBox txtPedido 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   1920
         Width           =   3375
      End
      Begin VB.CommandButton CmdPedido 
         Height          =   615
         Left            =   6000
         Picture         =   "FrmTransferencia_EntregaPedido.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   1880
         Width           =   615
      End
      Begin VB.CommandButton CmdLocalidad 
         Height          =   615
         Left            =   6000
         Picture         =   "FrmTransferencia_EntregaPedido.frx":0802
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   1040
         Width           =   615
      End
      Begin VB.CommandButton CmdLote 
         Height          =   615
         Left            =   6000
         Picture         =   "FrmTransferencia_EntregaPedido.frx":1004
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   200
         Width           =   615
      End
      Begin VB.Label lblPedido 
         BackStyle       =   0  'Transparent
         Caption         =   "Solicitud No."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   19
         Top             =   2040
         Width           =   2055
      End
      Begin VB.Label lblTransferencia 
         BackStyle       =   0  'Transparent
         Caption         =   "Localidad Destino"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   16
         Top             =   1200
         Width           =   1935
      End
      Begin VB.Label lblLote 
         BackStyle       =   0  'Transparent
         Caption         =   "Lote"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   14
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.Frame Frame7 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      ForeColor       =   &H80000008&
      Height          =   1455
      Left            =   7200
      TabIndex        =   6
      Top             =   720
      Width           =   5295
      Begin VB.TextBox TextFechaDesde 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   2295
      End
      Begin VB.TextBox TextFechaHasta 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   2640
         TabIndex        =   7
         Top             =   720
         Width           =   2415
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   2640
         TabIndex        =   10
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha de Transferencia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   120
         Width           =   4695
      End
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "Buscar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   8760
      Picture         =   "FrmTransferencia_EntregaPedido.frx":1806
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2400
      Width           =   2055
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   19335
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12600
         TabIndex        =   4
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmTransferencia_EntregaPedido.frx":3588
         Top             =   0
         Width           =   480
      End
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Consola de Despacho"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   120
         Width           =   11535
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   7005
      LargeChange     =   10
      Left            =   14505
      TabIndex        =   0
      Top             =   3720
      Width           =   674
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   7020
      Left            =   150
      TabIndex        =   1
      Top             =   3720
      Width           =   15045
      _ExtentX        =   26538
      _ExtentY        =   12383
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16777215
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image btnInfo 
      Height          =   300
      Left            =   0
      Picture         =   "FrmTransferencia_EntregaPedido.frx":530A
      Top             =   0
      Width           =   900
   End
   Begin VB.Image Image1 
      Height          =   300
      Left            =   0
      Picture         =   "FrmTransferencia_EntregaPedido.frx":5663
      Top             =   0
      Width           =   900
   End
   Begin VB.Image CmdSelect 
      Height          =   300
      Left            =   0
      Picture         =   "FrmTransferencia_EntregaPedido.frx":59BC
      Top             =   0
      Width           =   900
   End
End
Attribute VB_Name = "FrmTransferencia_EntregaPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public DocumentoPublic As String
Public BarBool As Boolean

Private Enum GrdDoc
    ColNull
    ColLote
    ColSolicitud
    ColLocalidadSolicitud
    ColTransferencia
    ColFecha
    ColDestino
    ColCantidadArticulos
    ColInfo
    ColCodLocalidadOrigen
    ColCodLocalidadDestino
    ColCount
End Enum

Private AnchoCampoDescripcion As Long

Private Consulta As String
Private Where As String
Private CantFilas As Long

Private Sub btnInfo_Click()
    
    On Error GoTo Error1
    
    Dim Rs As New ADODB.Recordset
    Dim SQL As String
    
    FrmTransferencia_PackingDocInfo.Pedido = Grid.TextMatrix(Grid.Row, ColSolicitud) 'Pedido
    FrmTransferencia_PackingDocInfo.LocalidadSolicitud = Grid.TextMatrix(Grid.Row, ColLocalidadSolicitud) 'Pedido
    FrmTransferencia_PackingDocInfo.Lote = Grid.TextMatrix(Grid.Row, ColLote)
    
    FrmTransferencia_PackingDocInfo.PostPacking = True
    
    FrmTransferencia_PackingDocInfo.Show vbModal
    
    Set FrmTransferencia_PackingDocInfo = Nothing
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al consultar los detalles de la solicitud, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    'Call PrepararGrid
    'Call PrepararDatos
    
End Sub

Private Sub CmdBuscar_Click()
    
    On Error GoTo Error1
    
    Dim Where As String
    
    Where = Empty
    
    If txtLote.Text <> Empty Then
        Where = Where & "AND TR.cs_Corrida = '" & FixTSQL(txtLote.Text) & "' "
    End If
    
    If txtLocalidad.Text <> Empty Then
        Where = Where & "AND TR.c_CodLocalidad_Destino = '" & FixTSQL(txtLocalidad.Tag) & "' "
    End If
    
    If txtPedido.Text <> Empty Then
        Where = Where & "AND TR.c_Documento = '" & FixTSQL(txtPedido.Text) & "' "
    End If
    
    If TextFechaDesde.Text <> Empty _
    And TextFechaHasta.Text <> Empty Then
        Where = Where & "AND CAST(MA.d_FECHA AS DATE) BETWEEN " & _
        "'" & FechaBD(CDate(TextFechaDesde.Tag)) & "' AND '" & FechaBD(CDate(TextFechaHasta.Tag)) & "' "
    End If
    
    Consulta = ";WITH Base AS (" & _
    "SELECT TR.cs_Corrida, TR.c_Documento, TR.c_Documento_Trans, TR.cs_CodLocalidad, " & _
    "TR.c_CodLocalidad_Destino, isNULL(LCD.c_Descripcion, 'N/A') AS LocalidadDestino, " & _
    "MA.d_Fecha, TR.c_Documento_Nota, TR.cs_CodLocalidad_Solicitud " & _
    "FROM TR_LOTE_GESTION_TRANSFERENCIA TR " & _
    "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA MA " & _
    "ON TR.cs_Corrida = MA.cs_Corrida " & _
    "LEFT JOIN MA_ENTREGA_TRANSFERENCIA ENT " & _
    "ON TR.c_Documento_Trans = ENT.Documento " & _
    "AND TR.cs_CodLocalidad = ENT.cs_CodLocalidad " & _
    "LEFT JOIN MA_SUCURSALES LCD " & _
    "ON TR.c_CodLocalidad_Destino = LCD.c_Codigo " & _
    "WHERE MA.b_Anulada = 0 " & _
    "AND MA.b_Finalizada = 1 " & _
    "AND ENT.Documento IS NULL " & _
    Where
    
    Consulta = Consulta & _
    "), SumItemsDoc AS ( " & _
    "SELECT ITM.c_Documento, ITM.cs_CodLocalidad_Solicitud, ITM.cs_CodLocalidad, " & _
    "SUM(ITM.n_Cantidad) AS CantEmpacada " & _
    "FROM BASE " & _
    "INNER JOIN TR_PACKING_TRANSFERENCIA ITM " & _
    "ON BASE.c_Documento_Nota = ITM.c_Documento " & _
    "AND BASE.cs_CodLocalidad = ITM.cs_CodLocalidad " & _
    "GROUP BY ITM.c_Documento, ITM.cs_CodLocalidad_Solicitud, ITM.cs_CodLocalidad " & _
    ") SELECT BASE.*, DET.CantEmpacada " & _
    "FROM BASE " & _
    "INNER JOIN SumItemsDoc DET " & _
    "ON BASE.c_Documento_Nota = DET.c_Documento " & _
    "AND BASE.cs_CodLocalidad = DET.cs_CodLocalidad " & _
    "ORDER BY BASE.d_Fecha ASC, BASE.c_Documento ASC; "
    
    Dim Rs As New ADODB.Recordset
    
    Rs.Open Consulta, Ent.BDD, adOpenStatic, adLockOptimistic
    
    Call PrepararGrid
    
    If Rs.RecordCount > 0 Then
        
        Rs.MoveFirst
        
'        Select Case True
'            Case OptPedido.Value
'                Rs.Sort = "Pedido"
'            Case OptCliente.Value
'                Rs.Sort = "Nombre"
'            Case OptFecha.Value
'                Rs.Sort = "Fecha"
'            Case OptDeposito.Value
'                Rs.Sort = "Deposito"
'            Case OptRIF.Value
'                Rs.Sort = "RIF"
'            Case OptTotal.Value
'                Rs.Sort = "Total"
'        End Select
        
        Call PrepararDatos(Rs)
        
    Else
        
        Mensaje True, "No hay solicitudes de transferencia pendientes por entregar con los criterios seleccionados."
        
    End If
    
    'Call LimpiarControlesBusqueda
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al realizar la consulta de los datos, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Private Sub Grid_EnterCell()
    
    If Grid.Rows = 1 Then
        'FrameSelect.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = 600
            End If
            
            Grid.RowHeight(Grid.Row) = 720
            Fila = Grid.Row
            
        End If
        
        MostrarEditorTexto2 Me, Grid, btnInfo
        'Grid.ColSel = 0
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        
    Else
        Band = False
    End If
    
End Sub

Public Sub MostrarEditorTexto2(pFrm As Form, pGrd As Object, ByRef txteditor As Object)
    
    On Error Resume Next
    
    With Grid

        .RowSel = .Row
        
        If .Col <> ColCantidadArticulos Then Band = True
        '.Col = ColCantidadArticulos
        
        If BarBool Then
            FrameSelect.BackColor = pGrd.BackColorSel
            FrameSelect.Move .Left + (13635 - 1220), .Top + .CellTop, 1290, .CellHeight
            btnInfo.Move ((FrameSelect.Width / 2) - (btnInfo.Width / 2)), ((FrameSelect.Height / 2) - (btnInfo.Height / 2))
            FrameSelect.Visible = True: btnInfo.Visible = True
            FrameSelect.ZOrder
        Else
            FrameSelect.BackColor = pGrd.BackColorSel
            FrameSelect.Move .Left + (13635 - 560), .Top + .CellTop, 1290, .CellHeight
            btnInfo.Move ((FrameSelect.Width / 2) - (btnInfo.Width / 2)), ((FrameSelect.Height / 2) - (btnInfo.Height / 2))
            FrameSelect.Visible = True: btnInfo.Visible = True
            FrameSelect.ZOrder
        End If
        
        cellRow = .Row
        cellCol = .Col
        
     End With
     
End Sub

Private Sub Form_Load()
    
    AjustarPantalla Me
    
    Call PrepararGrid
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = 0
    ScrollGrid.Value = 0
    
    Grid.ScrollBars = flexScrollBarNone
    
    ScrollGrid.Visible = False
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionFree
                     
        .Rows = 2
             
        .FixedCols = 0
        .FixedRows = 1
                
        .Rows = 1
        .Cols = ColCount
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        .Row = 0
        
        AnchoCampoDescripcion = 4000
        
        .Col = ColNull
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColLote
        .ColWidth(.Col) = 2000
        .Text = "Lote"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColSolicitud
        .ColWidth(.Col) = 2000
        .Text = "Solicitud"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColLocalidadSolicitud
        .ColWidth(.Col) = 0
        .Text = "Solicitud"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColTransferencia
        .ColWidth(.Col) = 2000
        .Text = "Transferencia"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColFecha
        .ColWidth(.Col) = 1600
        .Text = "Fecha"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDestino
        .ColWidth(.Col) = AnchoCampoDescripcion
        .Text = "Destino"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantidadArticulos
        .ColWidth(.Col) = 1500
        .Text = "Cant. Art."
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColInfo
        .ColWidth(.Col) = 1300
        .Text = Empty
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodLocalidadOrigen
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColCodLocalidadDestino
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Width = 15050
        
        .ScrollTrack = True
        
        Fila = .Row
        
        .Col = ColCodLocalidadDestino
        .ColSel = ColCodLocalidadDestino
        
    End With
    
    'For i = 0 To Grid.Cols - 1
        'Grid.Col = i
        'Grid.CellAlignment = flexAlignCenterCenter
    'Next
    
End Sub

Private Sub PrepararDatos(Rs As ADODB.Recordset)
    
    On Error GoTo Error1
    
    Grid.Visible = False
    
    If Not Rs.EOF Then
        
        ScrollGrid.Min = 0
        ScrollGrid.Max = Rs.RecordCount
        ScrollGrid.Value = 0
        
        CantFilas = Rs.RecordCount
        
        BarBool = False
        
        If CantFilas > 10 Then
            BarBool = True
        End If
        
        While Not Rs.EOF
            
            Grid.Rows = Grid.Rows + 1
            Linea = Grid.Rows - 1
            
            Grid.RowData(Linea) = Grid.RowHeight(Linea)
            
            Grid.Row = Linea
            
            Grid.TextMatrix(Linea, ColNull) = Empty
        
            Grid.TextMatrix(Linea, ColLote) = Rs!cs_Corrida
            Grid.TextMatrix(Linea, ColSolicitud) = Rs!c_Documento
            Grid.TextMatrix(Linea, ColLocalidadSolicitud) = Rs!cs_CodLocalidad_Solicitud
            Grid.TextMatrix(Linea, ColTransferencia) = Rs!c_Documento_Trans
            Grid.TextMatrix(Linea, ColFecha) = SDate(Rs!d_Fecha)
            Grid.TextMatrix(Linea, ColDestino) = Rs!LocalidadDestino
            Grid.TextMatrix(Linea, ColCantidadArticulos) = FormatoDecimalesDinamicos(Rs!CantEmpacada, 0, 4)
            Grid.TextMatrix(Linea, ColInfo) = Empty
            Grid.TextMatrix(Linea, ColCodLocalidadOrigen) = Rs!cs_CodLocalidad
            Grid.TextMatrix(Linea, ColCodLocalidadDestino) = Rs!c_CodLocalidad_Destino
            
            Grid.Col = ColInfo
            
            Set Grid.CellPicture = Me.btnInfo.Picture
            Grid.CellPictureAlignment = flexAlignCenterCenter
            
            Rs.MoveNext
            
        Wend
        
        Rs.Close
        
        Grid.Col = ColLote
        Grid.Row = 1
        
        If Grid.Rows > 10 Then
            ScrollGrid.Visible = True
            Grid.ScrollBars = flexScrollBarBoth
            Grid.ColWidth(ColDestino) = AnchoCampoDescripcion - 560
        Else
            Grid.ScrollBars = flexScrollBarNone
            ScrollGrid.Visible = False
        End If
        
    End If
    
Finally:
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Call LimpiarControlesBusqueda
    
    GoTo Finally
    
End Sub

Private Sub LimpiarControlesBusqueda()
    txtLote.Text = Empty
    txtLocalidad.Text = Empty
    txtLocalidad.Tag = Empty
    txtPedido.Text = Empty
    TextFechaDesde.Text = Empty
    TextFechaDesde.Tag = Empty
    TextFechaHasta.Text = Empty
    TextFechaHasta.Tag = Empty
End Sub

Private Sub Grid_Click()
    If Grid.CellPicture = Me.btnInfo.Picture Then
        btnInfo_Click
    End If
End Sub

Private Sub Grid_DblClick()
    
    On Error GoTo Error1
    
    If Grid.Row > 0 _
    And (Grid.Col = ColLote _
    Or Grid.Col = ColSolicitud _
    Or Grid.Col = ColTransferencia) Then
        
        If Grid.Col = ColLote Then
            
            If Mensaje(False, "Est� seguro de que desea marcar todas las transferencias " & _
            "del Lote [" & Grid.TextMatrix(Grid.Row, ColLote) & "] como entregadas? ") Then
                
                mObservacion = Left(Trim(QuickInputRequest( _
                "(Opcional) Indique alguna observaci�n acerca de " & _
                "la entrega del Lote si la hay, en caso contrario deje vac�o " & _
                "y pulse Enter", True, "[**CANCEL**]", , "Escriba aqu� (MAX 2000 Caracteres)", , _
                , , , , , True, , GetFont("Tahoma", 9))), 2000)
                
                If UCase(mObservacion) = "[**CANCEL**]" Then
                    Exit Sub
                End If
                
                Ent.BDD.Execute _
                "INSERT INTO MA_ENTREGA_TRANSFERENCIA " & _
                "([Documento], [cs_CodLocalidad], [CodLocalidadDestino], [Fecha], [Relacion], [Observacion]) " & _
                "SELECT " & _
                "TR.c_Documento_Trans, TR.cs_CodLocalidad, TR.c_CodLocalidad_Destino, " & _
                "GetDate() AS Fecha, 'SOLTRA ' + TR.c_Documento AS Relacion, " & _
                "'" & FixTSQL(mObservacion) & "' AS Observacion " & _
                "FROM TR_LOTE_GESTION_TRANSFERENCIA TR " & _
                "LEFT JOIN MA_ENTREGA_TRANSFERENCIA ENT " & _
                "ON TR.c_Documento_Trans = ENT.Documento " & _
                "AND TR.cs_CodLocalidad = ENT.cs_CodLocalidad " & _
                "WHERE TR.cs_Corrida = '" & Grid.TextMatrix(Grid.Row, ColLote) & "' " & _
                "AND ENT.Documento IS NULL ", mRowsAffected
                
                If mRowsAffected >= 1 Then
                    
                    Mensaje True, "Lote Finalizado con �xito."
                    Call LimpiarControlesBusqueda
                    CmdBuscar_Click
                    
                End If
                
            End If
            
        Else
            
            mObservacion = Left(Trim(QuickInputRequest( _
            "(Opcional) Indique alguna observaci�n acerca de " & _
            "la entrega del pedido si la hay, en caso contrario deje vac�o " & _
            "y pulse Enter", True, "[**CANCEL**]", , "Escriba aqu� (MAX 2000 Caracteres)", , _
            , , , , , True, , GetFont("Tahoma", 9))), 2000)
            
            If UCase(mObservacion) = "[**CANCEL**]" Then
                Exit Sub
            End If
            
            Ent.BDD.Execute _
            "INSERT INTO MA_ENTREGA_TRANSFERENCIA " & _
            "([Documento], [cs_CodLocalidad], [CodLocalidadDestino], [Fecha], [Relacion], [Observacion]) " & _
            "SELECT " & _
            "'" & FixTSQL(Grid.TextMatrix(Grid.Row, ColTransferencia)) & "', " & _
            "'" & FixTSQL(Grid.TextMatrix(Grid.Row, ColCodLocalidadOrigen)) & "', " & _
            "'" & FixTSQL(Grid.TextMatrix(Grid.Row, ColCodLocalidadDestino)) & "', " & _
            "GetDate(), 'SOLTRA " & FixTSQL(Grid.TextMatrix(Grid.Row, ColSolicitud)) & "', " & _
            "'" & FixTSQL(mObservacion) & "' ", mRowsAffected
            
            'If mRowsAffected >= 1 Then
                Call LimpiarControlesBusqueda
                CmdBuscar_Click
            'End If
            
        End If
        
    Else
        
        Mensaje True, "Seleccione una Transferencia o un Lote, " & _
        "ubicandose en la columna que corresponda e intente de nuevo. "
        
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If mErrorNumber = -2147217873 Then ' Primary Key
        Mensaje True, "El pedido ya fue marcado como entregado por otro operador desde otra estaci�n."
    Else
        Mensaje True, "Error al finalizar la entrega, " & _
        "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    End If
    
    Call LimpiarControlesBusqueda
    Call PrepararGrid
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call Grid_DblClick
    End If
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub TextFechaDesde_Click()
    
    Dim mCls As Object 'As clsFechaSeleccion
    
    Set mCls = FrmAppLink.GetClassFechaSeleccion 'New clsFechaSeleccion
    
    mCls.MostrarInterfazFechaHora DateTypes.JustDate
    
    If mCls.Selecciono Then
        TextFechaDesde.Text = mCls.FECHA
        TextFechaDesde.Tag = CDec(mCls.FECHA)
    End If
    
End Sub

Private Sub TextFechaHasta_Click()
    
    Dim mCls As Object 'As clsFechaSeleccion
    
    Set mCls = FrmAppLink.GetClassFechaSeleccion 'New clsFechaSeleccion
    
    mCls.MostrarInterfazFechaHora DateTypes.JustDate
    
    If mCls.Selecciono Then
        TextFechaHasta.Text = mCls.FECHA
        TextFechaHasta.Tag = CDec(mCls.FECHA)
    End If
    
End Sub

Private Sub txtLote_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
       CmdLote_Click
    End If
End Sub

Private Sub txtLote_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
    CmdLote_Click
End Sub

Private Sub CmdLote_Click()
    
    Dim SQL As String
    Dim mResul As Variant
    
    SQL = "SELECT TOP (200) cs_Corrida, d_Fecha, " & _
    "c_Observacion, c_Clasificacion " & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA " & _
    "WHERE b_Finalizada = 1 "
    
    mTitulo = "Picking > Packing - Top 200 Lotes Finalizados"
    
    Dim mFrm_Super_Consultas As Object
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With mFrm_Super_Consultas
        
        .Inicializar SQL, mTitulo, Ent.BDD
        
        .Add_ItemLabels "Lote", "cs_Corrida", 2200, 0
        .Add_ItemLabels "Fecha", "d_Fecha", 3700, 0, , Array("Fecha", "VbGeneralDate")
        .Add_ItemLabels "Observaci�n", "c_Observacion", 5130, 0
        .Add_ItemLabels "Clasificacion", "c_Clasificacion", 0, 0
        
        .Add_ItemSearching "N� Lote", "cs_Corrida"
        .Add_ItemSearching "Fecha", "CONVERT(NVARCHAR(MAX), d_Fecha, 103)"
        .Add_ItemSearching "Observaci�n", "c_Observacion"
        .Add_ItemSearching "Clasificaci�n", "c_Clasificacion"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = True
        .StrOrderBy = "cs_Corrida DESC"
        
        .Show vbModal
        
        mResul = .ArrResultado()
        
    End With
    
    Set mFrm_Super_Consultas = Nothing
    
    txtLote.Text = Empty ' Borrar si no selecciona.
    
    If Not IsEmpty(mResul) Then
        If Trim(mResul(0)) <> Empty Then
            txtLote.Text = mResul(0)
        End If
    End If
    
End Sub

Private Sub txtLocalidad_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
       CmdLocalidad_Click
    End If
End Sub

Private Sub txtLocalidad_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
    CmdLocalidad_Click
End Sub

Private Sub CmdLocalidad_Click()
    
    Dim mResul As Variant
    
    mResul = FrmAppLink.Buscar_Localidad
    
    txtLocalidad.Text = Empty
    txtLocalidad.Tag = Empty
    
    If Not IsEmpty(mResul) Then
        If Trim(mResul(0)) <> Empty Then
            txtLocalidad.Text = mResul(1)
            txtLocalidad.Tag = mResul(0)
        End If
    End If
       
End Sub

Private Sub txtPedido_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
       CmdPedido_Click
    End If
End Sub

Private Sub txtOrdenCompra_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
    CmdPedido_Click
End Sub

Private Sub CmdPedido_Click()
    
    Dim mResul As Variant
    Dim SQL As String
    
    SQL = "SELECT TOP 500 TR.cs_Corrida, TR.c_Documento, " & _
    "TR.c_CodLocalidad_Destino, isNULL(LCD.c_Descripcion, 'N/A') AS LocalidadDestino, " & _
    "TR.n_TotalUnidades, TR.n_TotalPeso, TR.n_TotalVolumen, " & _
    "DOC.ds_Fecha, DOC.d_Fecha_Recepcion, DOC.cs_Observacion, " & _
    "CONVERT(NVARCHAR(MAX), DOC.ds_Fecha, 112) AS ds_Fecha_YYYYMMDD, " & _
    "CONVERT(NVARCHAR(MAX), DOC.d_Fecha_Recepcion, 112) AS d_Fecha_Recepcion_YYYYMMDD " & _
    "FROM TR_LOTE_GESTION_TRANSFERENCIA TR " & _
    "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA MA " & _
    "ON TR.cs_Corrida = MA.cs_Corrida " & _
    "INNER JOIN MA_REQUISICIONES DOC " & _
    "ON TR.c_Documento = DOC.cs_Documento " & _
    "AND TR.cs_CodLocalidad_Solicitud = DOC.cs_CodLocalidad " & _
    "LEFT JOIN MA_SUCURSALES LCD " & _
    "ON TR.c_CodLocalidad_Destino = LCD.c_Codigo " & _
    "WHERE MA.b_Anulada = 0 " & _
    "AND MA.b_Finalizada = 1 " & _
    " " 'ORDER BY DOC.ds_Fecha DESC
    
    Dim mFrm_Super_Consultas As Object
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With mFrm_Super_Consultas
        
        .Inicializar SQL, mTitulo, Ent.BDD
        
        .Add_ItemLabels "Lote", "cs_Corrida", 2000, 0
        .Add_ItemLabels "Solicitud", "c_Documento", 2000, 0
        .Add_ItemLabels "Fecha", "ds_Fecha", 1725, 0, , Array("Fecha", "VbGeneralDate")
        .Add_ItemLabels "Destino", "LocalidadDestino", 3420, 0
        .Add_ItemLabels "Cant. Sol.", "n_TotalUnidades", 1785, 0, , Array("Numerico", 0, 3)
        .Add_ItemLabels "Peso", "n_TotalPeso", 1500, 0, , Array("Numerico", 0, 3)
        .Add_ItemLabels "Volumen", "n_TotalVolumen", 1500, 0, , Array("Numerico", 0, 3)
        .Add_ItemLabels "Fecha Rec.", "d_Fecha_Recepcion", 3585, 0, , Array("Fecha", "VbGeneralDate")
        .Add_ItemLabels "Observaci�n", "cs_Observacion", 8955, 0
        
        .Add_ItemSearching "Lote", "TR.cs_Corrida"
        .Add_ItemSearching "Solicitud", "TR.c_Documento"
        .Add_ItemSearching "Fecha YYYYMMDD", "CONVERT(NVARCHAR(MAX), DOC.ds_Fecha, 112)"
        .Add_ItemSearching "Fecha Rec. YYYYMMDD", "CONVERT(NVARCHAR(MAX), DOC.d_Fecha_Recepcion, 112)"
        .Add_ItemSearching "Destino", "isNULL(LCD.c_Descripcion, 'N/A')"
        .Add_ItemSearching "Cod. Localidad", "TR.c_CodLocalidad_Destino"
        .Add_ItemSearching "Observaci�n", "DOC.cs_Observacion"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = True
        .StrOrderBy = "DOC.ds_Fecha DESC"
        
        .Show vbModal
        
        mResul = .ArrResultado()
        
    End With
    
    Set mFrm_Super_Consultas = Nothing
    
    txtPedido.Text = Empty ' Borrar si no selecciona.
    
    If Not IsEmpty(mResul) Then
        
        If Trim(mResul(0)) <> Empty Then
            
            LimpiarControlesBusqueda
            
            txtPedido.Text = mResul(1)
            
            CmdBuscar_Click
            
        End If
        
    End If
    
End Sub
