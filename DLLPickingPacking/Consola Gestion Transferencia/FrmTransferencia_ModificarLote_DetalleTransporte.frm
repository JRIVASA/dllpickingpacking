VERSION 5.00
Begin VB.Form FrmTransferencia_ModificarLote_DetalleTransporte 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9405
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9405
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FrameTransporte 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   6975
      Left            =   240
      TabIndex        =   4
      Top             =   840
      Width           =   14775
      Begin VB.CommandButton cmdTransporte 
         Height          =   390
         Left            =   2340
         Picture         =   "FrmTransferencia_ModificarLote_DetalleTransporte.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   480
         Width           =   390
      End
      Begin VB.TextBox txtTransporte 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   480
         Locked          =   -1  'True
         TabIndex        =   25
         Top             =   480
         Width           =   1815
      End
      Begin VB.Frame Frame3 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Capacidades Vehiculo"
         ForeColor       =   &H80000008&
         Height          =   2055
         Left            =   360
         TabIndex        =   17
         Top             =   2760
         Width           =   6250
         Begin VB.Label lblvolumenV 
            BackStyle       =   0  'Transparent
            Caption         =   "Volumen"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   24
            Top             =   1080
            Width           =   2655
         End
         Begin VB.Label lblPesoV 
            BackStyle       =   0  'Transparent
            Caption         =   "Peso"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   23
            Top             =   600
            Width           =   2655
         End
         Begin VB.Label lblPlaca 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   22
            Top             =   1440
            Width           =   3135
         End
         Begin VB.Label Label9 
            BackStyle       =   0  'Transparent
            Caption         =   "Placa"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   21
            Top             =   1560
            Width           =   2655
         End
         Begin VB.Label lblPlacaPesoVeh 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   20
            Top             =   480
            Width           =   3135
         End
         Begin VB.Label lblPlacaVolumenVeh 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   19
            Top             =   960
            Width           =   3135
         End
         Begin VB.Label Label19 
            BackStyle       =   0  'Transparent
            Caption         =   "Datos del Vehiculo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   255
            Left            =   120
            TabIndex        =   18
            Top             =   120
            Width           =   2055
         End
      End
      Begin VB.Frame Frame4 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Totales Pedido"
         ForeColor       =   &H80000008&
         Height          =   3855
         Left            =   7200
         TabIndex        =   11
         Top             =   2880
         Width           =   6615
         Begin VB.Label lblFilasSel 
            BackStyle       =   0  'Transparent
            Caption         =   "No. Filas Seleccionadas"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   45
            Top             =   3360
            Width           =   2415
         End
         Begin VB.Label lblValorFilasSel 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   44
            Top             =   3240
            Width           =   3135
         End
         Begin VB.Label lblUnidadesTotales 
            BackStyle       =   0  'Transparent
            Caption         =   "Unidades Totales"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   43
            Top             =   2880
            Width           =   1815
         End
         Begin VB.Label lblValorUnidadesTotales 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   42
            Top             =   2760
            Width           =   3135
         End
         Begin VB.Label lblEmpaques 
            BackStyle       =   0  'Transparent
            Caption         =   "Empaques"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   41
            Top             =   2400
            Width           =   1815
         End
         Begin VB.Label lblValorEmpaques 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   40
            Top             =   2280
            Width           =   3135
         End
         Begin VB.Label lblUnidades 
            BackStyle       =   0  'Transparent
            Caption         =   "Unidades "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   39
            Top             =   1920
            Width           =   1815
         End
         Begin VB.Label lblValorUnidades 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   38
            Top             =   1800
            Width           =   3135
         End
         Begin VB.Label lblCantProductos 
            BackStyle       =   0  'Transparent
            Caption         =   "Cant. Productos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   37
            Top             =   1440
            Width           =   2415
         End
         Begin VB.Label lblValorCantProductos 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   36
            Top             =   1320
            Width           =   3135
         End
         Begin VB.Label lblPesoP 
            BackStyle       =   0  'Transparent
            Caption         =   "Peso"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   16
            Top             =   480
            Width           =   2295
         End
         Begin VB.Label lblvolumenP 
            BackStyle       =   0  'Transparent
            Caption         =   "Volumen"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   15
            Top             =   960
            Width           =   1815
         End
         Begin VB.Label lblPesoPedido 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   14
            Top             =   360
            Width           =   3135
         End
         Begin VB.Label lblvolumenPedido 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   13
            Top             =   840
            Width           =   3135
         End
         Begin VB.Label Label20 
            BackStyle       =   0  'Transparent
            Caption         =   "Totales Selecci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   0
            Width           =   2055
         End
      End
      Begin VB.Frame Frame5 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Diferencias"
         ForeColor       =   &H80000008&
         Height          =   1815
         Left            =   360
         TabIndex        =   5
         Top             =   5040
         Width           =   6250
         Begin VB.Label lblvolumenD 
            BackStyle       =   0  'Transparent
            Caption         =   "Volumen"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   10
            Top             =   1080
            Width           =   1695
         End
         Begin VB.Label lblPesoD 
            BackStyle       =   0  'Transparent
            Caption         =   "Peso"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   120
            TabIndex        =   9
            Top             =   600
            Width           =   1695
         End
         Begin VB.Label lblPesoDespacho 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   8
            Top             =   480
            Width           =   3135
         End
         Begin VB.Label lblvolumenDespacho 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3120
            TabIndex        =   7
            Top             =   1080
            Width           =   3135
         End
         Begin VB.Label Label21 
            BackStyle       =   0  'Transparent
            Caption         =   "Diferencias"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   120
            Width           =   2055
         End
      End
      Begin VB.Label lblDescri 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   480
         TabIndex        =   35
         Top             =   1320
         Width           =   6135
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Descripcion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   480
         TabIndex        =   34
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label lblModelo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   3600
         TabIndex        =   33
         Top             =   2160
         Width           =   3015
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Modelo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   3600
         TabIndex        =   32
         Top             =   1800
         Width           =   1695
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Responsable"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   2760
         TabIndex        =   31
         Top             =   120
         Width           =   1695
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Marca"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   480
         TabIndex        =   30
         Top             =   1800
         Width           =   1695
      End
      Begin VB.Label lblMarca 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   480
         TabIndex        =   29
         Top             =   2160
         Width           =   2895
      End
      Begin VB.Label lblResponsable 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   2760
         TabIndex        =   28
         Top             =   480
         Width           =   3855
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Transporte"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   480
         TabIndex        =   27
         Top             =   120
         Width           =   1455
      End
   End
   Begin VB.CommandButton cmd_salir 
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   13800
      Picture         =   "FrmTransferencia_ModificarLote_DetalleTransporte.frx":0802
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   8085
      Width           =   1215
   End
   Begin VB.Frame FrameTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   19320
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Pedido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   480
         TabIndex        =   3
         Top             =   120
         Width           =   7575
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12480
         TabIndex        =   2
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14520
         Picture         =   "FrmTransferencia_ModificarLote_DetalleTransporte.frx":2584
         Top             =   0
         Width           =   480
      End
   End
End
Attribute VB_Name = "FrmTransferencia_ModificarLote_DetalleTransporte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public CodigoTransporte As String

Public CantFilasSeleccionadas As Variant ' Decimal
Public CantProductos As Variant ' Decimal
Public CantEmpaques As Variant ' Decimal
Public CantUnidades As Variant ' Decimal
Public CantTotalUnidades As Variant ' Decimal
Public VolumenAsignado As Variant ' Decimal
Public PesoAsignado As Variant ' Decimal

Private mTransporte As ClsTransporte

Public CodTransporteFinal As String

Private FormaCargada As Boolean
Private Salir As Variant

Private Sub cmd_salir_Click()
    Exit_Click
End Sub

Public Function BuscarTransporte(ByVal pCodigo As String) As Boolean
    Set mTransporte = New ClsTransporte
    BuscarTransporte = mTransporte.BuscarTransporte(Ent.BDD, pCodigo)
End Function

Private Sub cmdTransporte_Click()
    
    Dim mCls As recsun.obj_busqueda
    Dim mBus As Variant
    Dim mResul As Variant
    
    mConsulta = _
    "Select c_CodTransporte, c_Descripcion, c_Marca, c_Modelo " & _
    "FROM MA_TRANSPORTE"
    
    Dim mFrm_Super_Consultas As Object
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With mFrm_Super_Consultas
        
        .Inicializar mConsulta, "Transporte", Ent.BDD
        
        .Add_ItemLabels "CODIGO", "c_CodTransporte", 1935, 0
        .Add_ItemLabels "DESCRIPCION", "c_Descripcion", 6000, 0
        .Add_ItemLabels "Marca", "c_Marca", 1500, 0
        .Add_ItemLabels "Modelo", "c_Modelo", 1500, 0
        
        .Add_ItemSearching "CODIGO", "c_CodTransporte"
        .Add_ItemSearching "DESCRIPCION", "c_Descripcion"
        .Add_ItemSearching "Marca", "c_Marca"
        .Add_ItemSearching "Modelo", "c_Modelo"
        
        .txtDato.Text = "%"
        .StrOrderBy = "c_Descripcion"
        
        .Show vbModal
        
        mResul = .ArrResultado()
        
        If Not IsEmpty(mResul) Then
            If Trim(mResul(0)) <> Empty Then
                Me.txtTransporte.Text = CStr(mResul(0))
                txtTransporte_LostFocus
            End If
        Else
            Me.txtTransporte.Text = Empty
            Me.lblResponsable = Empty
        End If
        
    End With
    
    Set mFrm_Super_Consultas = Nothing
    
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        FormaCargada = True
        txtTransporte.Text = CodigoTransporte
        txtTransporte_LostFocus
    End If
End Sub

Private Sub txtTransporte_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF2
            cmdTransporte_Click
        Case vbKeyBack, vbKeyDelete, vbKeySpace
            LimpiarControlesT
    End Select
End Sub

Private Sub txtTransporte_KeyPress(KeyAscii As Integer)
    KeyAscii = IIf(KeyAscii = Asc("'"), 0, KeyAscii)
End Sub

Private Sub txtTransporte_LostFocus()
    If BuscarTransporte(txtTransporte.Text) Then
        LlenarDatosTransporte
    Else
        LimpiarControlesT
    End If
End Sub

Private Sub LlenarDatosTransporte()
    With mTransporte
        txtTransporte.Text = .Codigo
        lblDescri.Caption = .Descripcion
        lblMarca.Caption = .Marca
        lblModelo.Caption = .Modelo
        lblPlaca.Caption = .Placa
        lblResponsable.Caption = .Responsable
        CalculoTransporte
    End With
End Sub

Private Sub LimpiarControlesT()
    txtTransporte.Text = Empty
    lblDescri.Caption = Empty
    lblMarca.Caption = Empty
    lblModelo.Caption = Empty
    lblPlaca.Caption = Empty
    lblResponsable.Caption = Empty
    lblPesoV.Caption = "Peso: "
    lblvolumenV.Caption = "Volumen: "
    lblPesoD.Caption = "Peso: "
    lblvolumenD.Caption = "Volumen: "
    lblPesoP.Caption = "Peso: "
    lblvolumenP.Caption = "Volumen: "
End Sub

Private Sub CalculoTransporte()
    
    lblValorFilasSel = FormatoDecimalesDinamicos(CantFilasSeleccionadas, 2, 4)
    lblValorCantProductos = FormatoDecimalesDinamicos(CantProductos, 2, 4)
    lblValorUnidades = FormatoDecimalesDinamicos(CantUnidades, 2, 4)
    lblValorEmpaques = FormatoDecimalesDinamicos(CantEmpaques, 2, 4)
    lblValorUnidadesTotales = FormatoDecimalesDinamicos(CantTotalUnidades, 2, 4)
    lblPesoPedido.Caption = FrmAppLink.FormatoDecimalesDinamicos(PesoAsignado, 2, 4)
    lblvolumenPedido.Caption = FrmAppLink.FormatoDecimalesDinamicos(VolumenAsignado, 2, 4)
    
    lblPlacaPesoVeh.Caption = FrmAppLink.FormatoDecimalesDinamicos(mTransporte.CapacidadPeso, 2, 4)
    lblPlacaVolumenVeh.Caption = FrmAppLink.FormatoDecimalesDinamicos(mTransporte.CapacidadVolumen, 2, 4)
    lblPesoPedido.Caption = FrmAppLink.FormatoDecimalesDinamicos(PesoAsignado, 2, 4)
    lblvolumenPedido.Caption = FrmAppLink.FormatoDecimalesDinamicos(VolumenAsignado, 2, 4)
    lblPesoDespacho.Caption = FrmAppLink.FormatoDecimalesDinamicos(mTransporte.CapacidadPeso - PesoAsignado, 2, 4)
    lblvolumenDespacho.Caption = FrmAppLink.FormatoDecimalesDinamicos(mTransporte.CapacidadVolumen - VolumenAsignado, 2, 4)
    
End Sub

Private Sub Form_Load()
    
    AjustarPantalla Me
    
    FormaCargada = False
    Salir = False
    
End Sub

Private Sub Exit_Click()
    
    CodTransporteFinal = txtTransporte.Text
    Salir = True
    
    Unload Me
    
End Sub
