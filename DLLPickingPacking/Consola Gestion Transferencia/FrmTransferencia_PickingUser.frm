VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmTransferencia_PickingUser 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Chk_Volumen 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Volumen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   300
      Left            =   2640
      TabIndex        =   20
      Top             =   10440
      Width           =   1785
   End
   Begin VB.Frame FramePickAll 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      ForeColor       =   &H0000C000&
      Height          =   615
      Left            =   13260
      TabIndex        =   18
      Top             =   9180
      Width           =   1755
      Begin VB.Label CmdPickAll 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "     Recoger    Todo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   555
         Left            =   60
         TabIndex        =   19
         Top             =   20
         Width           =   1635
      End
   End
   Begin VB.Frame FrameCheckAll 
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   13260
      TabIndex        =   16
      Top             =   10080
      Width           =   1755
      Begin VB.Label CmdCheckAll 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "     Finalizar    Todo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   555
         Left            =   60
         TabIndex        =   17
         Top             =   30
         Width           =   1635
      End
   End
   Begin VB.Frame buscar 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   " C�digo a Buscar: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   1275
      Left            =   5040
      TabIndex        =   15
      Top             =   9300
      Width           =   6165
      Begin VB.TextBox Cod_Buscar 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   120
         MaxLength       =   15
         TabIndex        =   1
         Top             =   480
         Width           =   5805
      End
   End
   Begin VB.CommandButton ButtonLeyenda 
      Caption         =   "Leyenda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   11520
      Picture         =   "FrmTransferencia_PickingUser.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   14
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9060
      Width           =   1455
   End
   Begin VB.CheckBox Chk_Peso 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Peso"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   300
      Left            =   2640
      TabIndex        =   13
      Top             =   10050
      Width           =   1785
   End
   Begin VB.CheckBox Chk_NombreProducto 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Art�culo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   300
      Left            =   2640
      TabIndex        =   11
      Top             =   9650
      Width           =   1785
   End
   Begin VB.CheckBox Chk_Ubicacion 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Ubicaci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   300
      Left            =   600
      TabIndex        =   10
      Top             =   9650
      Value           =   1  'Checked
      Width           =   1785
   End
   Begin VB.CheckBox Chk_Cantidad 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Cantidad"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   300
      Left            =   600
      TabIndex        =   12
      Top             =   10050
      Width           =   1785
   End
   Begin VB.Frame FrameSelect 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   13680
      TabIndex        =   7
      Top             =   7860
      Visible         =   0   'False
      Width           =   720
      Begin VB.Image CmdSelect 
         Height          =   480
         Left            =   120
         Picture         =   "FrmTransferencia_PickingUser.frx":0CCA
         Top             =   120
         Width           =   480
      End
   End
   Begin VB.CommandButton ButtonActualizar 
      Caption         =   "Actualizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   11520
      Picture         =   "FrmTransferencia_PickingUser.frx":2A4C
      Style           =   1  'Graphical
      TabIndex        =   6
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   10020
      Width           =   1455
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   8370
      LargeChange     =   10
      Left            =   14530
      TabIndex        =   3
      Top             =   600
      Width           =   674
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   8370
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   14764
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   19335
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Consola de Recolecci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   360
         TabIndex        =   5
         Top             =   120
         Width           =   11535
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmTransferencia_PickingUser.frx":47CE
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12600
         TabIndex        =   2
         Top             =   120
         Width           =   1815
      End
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00AE5B00&
      X1              =   3360
      X2              =   4750
      Y1              =   9475
      Y2              =   9475
   End
   Begin VB.Label C�digo 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Criterios de Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Index           =   71
      Left            =   480
      TabIndex        =   8
      Top             =   9300
      Width           =   2805
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   1605
      Index           =   1
      Left            =   360
      TabIndex        =   9
      Top             =   9180
      Width           =   4455
   End
End
Attribute VB_Name = "FrmTransferencia_PickingUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Band As Boolean
Private OrderBy As String
Private Fila As Integer
Private Salir As Variant
Private CheckAll As Boolean

Private CodDepositoOrigen As String

Private Enum GrdPick
    ColNull
    ColLote
    ColNumDoc ' Solicitud
    ColCodigoAlterno
    ColDesPro
    ColUbicacion
    ColCantSolicitada
    ColCantRecolectada
    ColPeso
    ColVolumen
    ColIconoPick
    ColCodPro
    ColDocCount ' En cuantos pedidos est� solicitado el art�culo.
    ColDecimales
    ColUltimoIDxProducto
    ColCount
End Enum

Private AnchoCampoDescripcion As Long

Private Sub Form_Activate()
    
    If Salir Then
        
        Mensaje True, "No hay productos pendientes por recolectar."
        
        Unload Me
        
        Exit Sub
        
    End If
    
    SafeFocus Cod_Buscar
    
End Sub

Private Sub Form_Load()
    
    CodDepositoOrigen = BuscarReglaNegocioStr("TRA_Consola_CodDepositoOrigen", Empty)
    
    If Trim(mvarCodigoDepositoOrigen) = Empty Then
        CodDepositoOrigen = FrmAppLink.GetCodDepositoPredeterminado
    End If
    
    AjustarPantalla Me
    
   'OrderBy = "order by isNull(MA_UBICACIONxPRODUCTO.CU_MASCARA,'N/A')"
    OrderBy = "ORDER BY isNULL(MAX(Ubicacion), 'N/A')"
    
    Salir = False
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionByRow
        
        .Rows = 2
        
        .FixedCols = 0
        .FixedRows = 1
        
        '.AllowUserResizing = flexResizeColumns
        
        .Rows = 1
        .Cols = GrdPick.ColCount
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        AnchoCampoDescripcion = 4860 '6790
        
        .Row = 0
        
        .Col = ColNull
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColLote
        .ColWidth(.Col) = 0
        .Text = "Lote"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColNumDoc
        .ColWidth(.Col) = 0
        .Text = "Solicitud"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColCodigoAlterno
        .ColWidth(.Col) = 2050
        .Text = "Art�culo"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesPro
        .ColWidth(.Col) = AnchoCampoDescripcion
        .Text = "Nombre del Producto"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColUbicacion
        .ColWidth(.Col) = 1860
        .Text = "Ubicaci�n"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantSolicitada
        .ColWidth(.Col) = 1300
        .Text = "Cant. Sol"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColPeso
        .ColWidth(.Col) = 1300
        .Text = "Peso"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColVolumen
        .ColWidth(.Col) = 1300
        .Text = "Vol."
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantRecolectada
        .ColWidth(.Col) = 1300
        .Text = "Cant. Rec"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColIconoPick
        .ColWidth(.Col) = 1000
        .Text = Empty
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodPro
        .ColWidth(.Col) = 0
        .Text = "C�digo Principal"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColDocCount
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColDecimales
        .ColWidth(.Col) = 0
        .Text = "Decimales"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColUltimoIDxProducto
        .ColWidth(.Col) = 0
        .Text = "PP_ID"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Width = 15050
        
        .ScrollTrack = True
        
        Fila = 0
        .Row = Fila
        
        .Col = ColDecimales
        .ColSel = ColDocCount
        
    End With
    
    'For I = 0 To Grid.Cols - 1
        'Grid.Col = I
        'Grid.CellAlignment = flexAlignCenterCenter
    'Next
    
End Sub

Private Sub PrepararDatos()
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset, I As Integer
    
    Grid.Visible = False
    
    mCampoUbicaciones = "REPLACE(isNULL(REVERSE(SUBSTRING(REVERSE((SELECT UBC2.cu_Mascara + '[VBNEWLINE]' " & _
    "FROM MA_UBICACIONxPRODUCTO UBC2 " & _
    "WHERE UBC2.cu_Deposito + UBC2.cu_Producto = " & _
    "'" & FixTSQL(CodDepositoOrigen) & "' + CodProducto " & _
    "FOR XML PATH(''))), 1 + LEN('[VBNEWLINE]'), 9999)), 'N/A'), '&#x20', '') AS Ubicacion"
    
    SQL = "SELECT MAX(CodEdi) AS CodEdi, CodProducto, MAX(Nombre) AS Nombre, " & _
    "MAX(Ubicacion) AS Ubicacion, MAX(CodLote) AS CodLote, SUM(CantSolicitada) AS CantSolicitada, " & vbNewLine & _
    "SUM(CantRecolectada) as CantRecolectada, SUM(CantPedidos) AS CantPedidos, CantDecimales, " & vbNewLine & _
    "Ult_PP_ID, n_Peso, n_Volumen, n_PesoBul, n_VolBul FROM (" & vbNewLine & _
    "SELECT CodEDI, CodProducto, Nombre, " & mCampoUbicaciones & ", " & _
    "CodLote, Ult_PP_ID, CantSolicitada, CantRecolectada, CantPedidos, CantDecimales, " & _
    "n_Peso, n_Volumen, n_PesoBul, n_VolBul FROM (" & vbNewLine & _
    "SELECT isNULL(COD.C_Codigo, PRO.C_Codigo) AS CodEDI, " & vbNewLine & _
    "PP.CodProducto, PRO.c_Descri AS Nombre, " & vbNewLine & _
    "PP.CodLote, MAX(PP.ID) AS Ult_PP_ID, SUM(PP.CantSolicitada) AS CantSolicitada, " & vbNewLine & _
    "SUM(PP.CantRecolectada) as CantRecolectada, 0 AS CantPedidos, " & vbNewLine & _
    "PRO.Cant_Decimales AS CantDecimales, '' AS c_CodDeposito, " & vbNewLine & _
    "PRO.n_Peso, PRO.n_Volumen, PRO.n_PesoBul, PRO.n_VolBul " & vbNewLine & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & _
    "INNER JOIN MA_REQUISICIONES DOC " & vbNewLine
    
    SQL = SQL & _
    "ON PP.CodPedido = DOC.cs_Documento " & vbNewLine & _
    "AND PP.cs_CodLocalidad_Solicitud = DOC.cs_CodLocalidad " & vbNewLine & _
    "INNER JOIN MA_PRODUCTOS PRO " & _
    "ON PRO.c_Codigo = PP.CodProducto " & vbNewLine & _
    "LEFT JOIN MA_CODIGOS COD " & _
    "ON COD.c_CodNasa = PRO.c_Codigo " & _
    "AND COD.nu_Intercambio = 1 " & _
    "WHERE PP.CodRecolector = '" & LcCodUsuario & "' " & vbNewLine & _
    "AND PP.Picking = 0 " & vbNewLine & _
    "GROUP BY PRO.c_Codigo, COD.c_Codigo, PP.CodProducto, PRO.C_DESCRI, " & _
    "PP.CodLote, PP.CodRecolector, PP.Picking, PRO.Cant_Decimales, " & vbNewLine & _
    "PRO.n_Peso, PRO.n_Volumen, PRO.n_PesoBul, PRO.n_VolBul " & vbNewLine & _
    ") AS Todo " & _
    "GROUP BY Nombre, CodEDI, CantSolicitada, CantRecolectada, CodProducto, " & _
    "CodLote, Ult_PP_ID, CantPedidos, CantDecimales, c_CodDeposito, " & vbNewLine & _
    "n_Peso, n_Volumen, n_PesoBul, n_VolBul " & vbNewLine
    
    SQL = SQL & _
    "UNION ALL " & vbNewLine & _
    "(SELECT '' AS CodEdi, CodProducto, '' AS Nombre, '' AS Ubicacion, " & _
    "'' AS CodLote, MAX(M.ID) AS Ult_PP_ID, 0 AS CantSolicitada, 0 AS CantRecolectada, " & vbNewLine & _
    "COUNT(*) AS CantPedidos, P.Cant_Decimales AS CantDecimales, " & _
    "P.n_Peso, P.n_Volumen, P.n_PesoBul, P.n_VolBul " & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING M " & _
    "INNER JOIN MA_PRODUCTOS P " & _
    "ON M.CodProducto = P.c_Codigo " & _
    "WHERE M.CodRecolector = '" & LcCodUsuario & "' " & vbNewLine & _
    "AND M.Picking = 0 " & _
    "GROUP BY M.CodProducto, P.Cant_Decimales, " & _
    "P.n_Peso, P.n_Volumen, P.n_PesoBul, P.n_VolBul " & _
    ")) AS TB GROUP BY Ult_PP_ID, CodProducto, CantDecimales, " & vbNewLine & _
    "n_Peso, n_Volumen, n_PesoBul, n_VolBul " & _
    OrderBy
    
    ' Actualizacion 2023-06-21: Traerse el MAX ID hasta el momento. _
    Para que a la hora de procesar el query no vaya a marcar como recolectados _
    posibles productos recien asignados a este usuario.
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        Salir = True
    End If
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    While Not Rs.EOF
        
        Grid.Rows = Grid.Rows + 1
        Linea = Grid.Rows - 1
        
        'GRID.TextMatrix(Linea, ColLote) = Rs!CodLote
        'GRID.TextMatrix(Linea, ColNumDoc) = Rs!CodPedido
        
        Grid.TextMatrix(Linea, ColCodigoAlterno) = Rs!CodEDI
        Grid.TextMatrix(Linea, ColDesPro) = Rs!Nombre
        
        TmpUbc = Rs!Ubicacion
        Ubc = vbNullString
        
        Do While (TmpUbc Like "*[[]VBNEWLINE[]]*")
            
            TmpNumChars = InStr(1, TmpUbc, "[VBNEWLINE]")
            TmpNumChars = TmpNumChars - 1
            TmpItmUbc = Mid(TmpUbc, 1, TmpNumChars)
            TmpUbc = Mid(TmpUbc, TmpNumChars + Len("[VBNEWLINE]") + 1)
            
            TmpMaxLen = 12
            
            If Len(TmpItmUbc) <= TmpMaxLen Then
                RemChar = TmpMaxLen - Len(TmpItmUbc)
                TmpItmUbc = String(Int(RemChar / 2), " ") & TmpItmUbc & String(Int(RemChar / 2), " ")
                'TmpItmUbc = 'Rellenar_SpaceR(TmpItmUbc, 15, " ")
            End If
            
            Ubc = Ubc & TmpItmUbc
            
        Loop
        
        Ubc = Ubc & TmpUbc
        
        TmpCantLn = (Len(Rs!Ubicacion) - Len(Replace(Rs!Ubicacion, _
        "[VBNEWLINE]", vbNullString))) / Len("[VBNEWLINE]")
        
        If TmpCantLn >= 2 Then
            Grid.RowHeight(Linea) = (Grid.RowHeight(Linea) * ((TmpCantLn + 1) / 2))
        End If
        
        Grid.TextMatrix(Linea, ColUbicacion) = Ubc 'Rs!Ubicacion
        
        Grid.RowData(Linea) = Grid.RowHeight(Linea)
        
        Grid.TextMatrix(Linea, ColCantSolicitada) = FormatNumber(Rs!CantSolicitada, Rs!CantDecimales)
        Grid.TextMatrix(Linea, ColPeso) = FormatoDecimalesDinamicos(Rs!CantSolicitada * Rs!n_Peso, 0, 4)
        Grid.TextMatrix(Linea, ColVolumen) = FormatoDecimalesDinamicos(Rs!CantSolicitada * Rs!n_Volumen, 0, 4)
        Grid.TextMatrix(Linea, ColCantRecolectada) = FormatNumber(Rs!CantRecolectada, Rs!CantDecimales)
        Grid.TextMatrix(Linea, ColCodPro) = Rs!CodProducto
        Grid.TextMatrix(Linea, ColDocCount) = FormatNumber(Rs!CantPedidos, 0)
        Grid.TextMatrix(Linea, ColDecimales) = CDec(Rs!CantDecimales)
        Grid.TextMatrix(Linea, ColUltimoIDxProducto) = CDec(Rs!Ult_PP_ID)
        
        If Rs!CantRecolectada > 0 Then
            For I = 0 To Grid.Cols - 1
                Grid.Row = Grid.Rows - 1
                Grid.Col = I
                Grid.CellBackColor = -2147483624 'Amarillo
            Next I
        End If
        
        Rs.MoveNext
        
    Wend
    
    Rs.Close
    
    If Grid.Row > 1 Then
        Grid.Row = 1
        Fila = 1 ' Para que se cambie el tama�o en el EnterCell
        Grid.ColSel = ColCantRecolectada
    End If
    
    If Grid.Rows > 13 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(ColDesPro) = AnchoCampoDescripcion - 675
    Else
        Grid.ScrollBars = flexScrollBarNone
        ScrollGrid.Visible = False
    End If
    
    SQL = "SELECT Descripcion " & _
    "FROM MA_USUARIOS " & _
    "WHERE CodUsuario = '" & LcCodUsuario & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    LabelTitulo.Caption = "Consola de Recolecci�n de Transferencia - Usuario: " & Rs!Descripcion
    
    Grid_EnterCell
    
    SafeFocus Cod_Buscar
    
Finally:
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub FramePickAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    CmdPickAll_MouseUp Button, Shift, X, Y
End Sub

Private Sub CmdPickAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_KeyDown vbKeyF5, 0
End Sub

Private Sub FrameCheckAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    CmdCheckAll_MouseUp Button, Shift, X, Y
End Sub

Private Sub CmdCheckAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_KeyDown vbKeyF6, 0
End Sub

Private Sub Grid_DblClick()
    
    On Error GoTo Error1
    
    Dim Resultado As Variant, NewCant As Variant
    
    Dim CantSolicitada As Variant, CantRecolectada As Variant
    
    CantSolicitada = CDec(Grid.TextMatrix(Grid.RowSel, ColCantSolicitada))
    CantRecolectada = CDec(Grid.TextMatrix(Grid.RowSel, ColCantRecolectada))
    
    If CheckAll Then
        NewCant = CantSolicitada
    Else
        FrmNumPadGeneral.ValorOriginal = CantRecolectada
        FrmNumPadGeneral.Show vbModal
        NewCant = CDec(FrmNumPadGeneral.ValorNumerico)
        Set FrmNumPadGeneral = Nothing
    End If
    
    'If NewCant = 0 Then ' Comentado... y si en verdad no hay...
        'Mensaje True, "La cantidad recolectada no puede ser cero (0)."
        'Exit Sub
    'End If
    
    'If NewCant > 0 Then
    
        Dim SQL As String
        Dim CantDisponible As Double
        
        Dim CantDecimalesProducto As Long
        
        CantDecimalesProducto = SDec(Grid.TextMatrix(Grid.RowSel, ColDecimales))
        
        CantDisponible = RoundDecimalUp(NewCant, CantDecimalesProducto)
        
        Dim SQLRecolector As String
        Dim Rs As Recordset
        Dim AffectedRecords
        
        'Validaciones
        
        If NewCant > CantSolicitada Then
            
            If Not CheckAll Then
                Mensaje True, "La cantidad recolectada no puede ser mayor a la cantidad solicitada."
            End If
            
            Exit Sub
            
        End If
        
        If CantRecolectada > NewCant _
        And Not CheckAll Then
            
            Mensaje False, "La cantidad actual de productos recolectados es de " & _
            CantRecolectada & " �Seguro que desea disminuirla a " & CantDisponible & "?"
            
            If Not Retorno Then
                Exit Sub
            End If
            
        End If
        
        'Grabar
        
        Dim Trans As Boolean
        
        Ent.BDD.BeginTrans
        Trans = True
        
        Set Rs = New Recordset
        
        SQLRecolector = _
        "SELECT * FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
        "WHERE CodProducto = '" & FixTSQL(Grid.TextMatrix(Grid.Row, ColCodPro)) & "' " & vbNewLine & _
        "AND CodRecolector = '" & LcCodUsuario & "' " & _
        "AND Picking = 0 " & _
        "AND ID <= " & SDec(Grid.TextMatrix(Grid.Row, ColUltimoIDxProducto)) & " " & _
        "ORDER BY FechaAsignacion ASC "
        
        'consulta
        
        Rs.Open SQLRecolector, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        Do While Not Rs.EOF
            
            'If (CantDisponible = 0) Then ' Comentado en caso de que haya que cambiar la cantidad hacia abajo...
                'Exit Do
            'End If
            
            If (CDec(CantDisponible) >= CDec(Rs!CantSolicitada)) Then
                
                SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                "CantRecolectada = " & CDec(Rs!CantSolicitada) & " " & vbNewLine & _
                "WHERE ID = " & CDec(Rs!ID) & " " & _
                "AND CodProducto = '" & FixTSQL(Grid.TextMatrix(Grid.RowSel, ColCodPro)) & "' "
                
                Ent.BDD.Execute SQL, AffectedRecords
                
                CantDisponible = RoundUp((CantDisponible - Rs!CantSolicitada), CantDecimalesProducto)
                
            Else
                
                SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                "CantRecolectada = " & CantDisponible & " " & vbNewLine & _
                "WHERE ID = " & CDec(Rs!ID) & " " & _
                "AND CodProducto = '" & FixTSQL(Grid.TextMatrix(Grid.RowSel, ColCodPro)) & "' "
                
                Ent.BDD.Execute SQL, AffectedRecords
                
                CantDisponible = 0
                
            End If
            
            Rs.MoveNext
            
        Loop
        
        Rs.Close
        
        If Trans Then
            Ent.BDD.CommitTrans
            Trans = False
        End If
        
        If Not CheckAll Then
            Call ButtonActualizar_Click
        End If
        
    'End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If Trans Then
        Ent.BDD.RollbackTrans
        Trans = False
    End If
    
    If Not CheckAll Then
        
        Mensaje True, "Error al actualizar cantidad recolectada, " & _
        "Descripci�n " & mErrorDesc & " (" & mErrorNumber & ")"
        
        Call PrepararGrid
        Call PrepararDatos
        
    End If
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Grid_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyF5 And Shift = 0 Then
        
        If Grid.Rows > 1 Then
            
            Mensaje False, "Atenci�n! �Est� seguro de establecer la cantidad " & _
            "m�xima como recolectada para todos los productos?" & GetLines & _
            "En tal caso, presione Aceptar para continuar"
            
            If Retorno Then
                
                CheckAll = True
                
                For I = 1 To Grid.Rows - 1
                    Grid.Row = I
                    Grid.RowSel = Grid.Row
                    Grid_DblClick
                Next I
                
                CheckAll = False
                
                ButtonActualizar_Click
                
            End If
            
        End If
        
    ElseIf KeyCode = vbKeyF6 And Shift = 0 Then
        
        If Grid.Rows > 1 Then
            
            Mensaje False, "Atenci�n! �Est� seguro de finalizar la " & _
            "recolecci�n de todos los productos? En tal caso, " & _
            "presione Aceptar para continuar o Cancelar si desea verificar los datos."
            
            If Retorno Then
                
                CheckAll = True
                
                For I = 1 To Grid.Rows - 1
                    Grid.Row = I
                    Grid.RowSel = Grid.Row
                    CmdSelect_Click
                Next I
                
                CheckAll = False
                
                ButtonActualizar_Click
                
            End If
            
        End If
        
    End If
    
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call Grid_DblClick
    End If
End Sub

Private Sub ButtonActualizar_Click()
    Call PrepararGrid
    Call PrepararDatos
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub Grid_EnterCell()
        
    If Grid.Rows = 1 Then
        FrameSelect.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = Grid.RowData(Fila) '600
            End If
            
            Grid.RowHeight(Grid.Row) = Grid.RowData(Grid.Row) + 300 '900
            Fila = Grid.Row
            
        End If
        
        MostrarEditorTexto2 Me, Grid, CmdSelect
        Grid.ColSel = ColNull
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        
    Else
        Band = False
    End If
    
End Sub

Public Sub MostrarEditorTexto2(pFrm As Form, pGrd As Object, ByRef txteditor As Object)
    
    On Error Resume Next
    
    With Grid
        
        .RowSel = .Row
        
        If .Col <> ColIconoPick Then Band = True
        .Col = ColIconoPick
        
        FrameSelect.BackColor = pGrd.BackColorSel
        FrameSelect.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), _
        ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
        FrameSelect.Visible = True: CmdSelect.Visible = True
        FrameSelect.ZOrder
        
        cellRow = .Row
        cellCol = .Col
        
     End With
     
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Private Sub CmdSelect_Click()
    
    On Error GoTo Error1
    
    Dim Cant As Variant ' Decimal
    Dim FinalizarCantidadCero As Boolean
    Dim CantRecolectada As Variant ' Decimal
    Dim CantSolicitada As Variant ' Decimal
    Dim Trans As Boolean, AffectedRecords
    
    Dim CantDecimalesProducto As Long
    
    CantDecimalesProducto = SDec(Grid.TextMatrix(Grid.RowSel, ColDecimales))
    
    'CantRecolectada = GRID.TextMatrix(GRID.RowSel, ColDocCount)
    CantRecolectada = SDec(Grid.TextMatrix(Grid.RowSel, ColCantRecolectada))
    
    CantSolicitada = SDec(Grid.TextMatrix(Grid.RowSel, ColCantSolicitada))
    
    Cant = RoundDecimalUp((CantSolicitada - CantRecolectada), CantDecimalesProducto)
    
    If CantRecolectada = 0 Then
        
        If CheckAll Then
            Retorno = True
        Else
            Mensaje False, "�Est� seguro de finalizar la recolecci�n " & _
            "de este producto con una cantidad igual a cero (0)?"
        End If
        
        If Retorno Then
            FinalizarCantidadCero = True
        End If
        
    End If
    
    If (CantRecolectada < CantSolicitada) _
    And Not FinalizarCantidadCero Then
        
        If CheckAll Then
            Retorno = True
        Else
            Mensaje False, "�Est� seguro de finalizar la recolecci�n " & _
            "de este producto con una cantidad faltante de productos " & _
            "(Falta(n) " & FormatoDecimalesDinamicos(Cant) & ") ?"
        End If
        
        If Not Retorno Then
            Exit Sub
        End If
        
    End If
    
    Ent.BDD.BeginTrans
    Trans = True
    
    Dim mRs As Recordset
    
    Set mRs = New Recordset
    
    SQL = "SELECT * FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
    "WHERE CodProducto = '" & FixTSQL(Grid.TextMatrix(Grid.RowSel, ColCodPro)) & "' " & vbNewLine & _
    "AND CodRecolector = '" & LcCodUsuario & "' " & _
    "AND Picking = 0 " & _
    "AND ID <= " & SDec(Grid.TextMatrix(Grid.RowSel, ColUltimoIDxProducto)) & " " & _
    "ORDER BY FechaAsignacion ASC "
    
    mRs.CursorLocation = adUseClient
    mRs.Open SQL, Ent.BDD, adOpenStatic, adLockOptimistic
    
    Set mRs.ActiveConnection = Nothing
    
    ' 2023-06-21 Lo siguiente se comenta, no ten�a sentido.
    
    'If Not FinalizarCantidadCero Then
        
        'mRs.Filter = "CantRecolectada = 0"
        
        'If Not mRs.EOF Then
            
            'While Not mRs.EOF
                
                'mRs.Delete
                'mRs.MoveNext
                
            'Wend
            
        'End If
        
    'End If
    
    mRs.Filter = "" 'vbNullString
    
    If mRs.RecordCount > 0 Then
        mRs.MoveFirst
    End If
    
    While Not mRs.EOF
        
        If CantRecolectada = 0 Then
            
            ' Si estoy confirmando recoleccion cero.
            ' Limpiamos todos los registros asociados al recolector.
            
            SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
            "CantRecolectada = 0 " & vbNewLine & _
            "WHERE ID = " & CDec(mRs!ID) & " " & _
            "AND CodProducto = '" & FixTSQL(Grid.TextMatrix(Grid.RowSel, ColCodPro)) & "' " & _
            "AND CodRecolector = '" & LcCodUsuario & "' "
            
            Ent.BDD.Execute SQL, AffectedRecords
            'Ent.BDD.RollbackTrans
            'Exit Sub
            
        End If
        
        ' Confirmamos el estatus recolectado de todas las l�neas de este producto asociadas _
        al recolector.
        
        SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
        "Picking = 1 " & vbNewLine & _
        "WHERE ID = " & CDec(mRs!ID) & " " & _
        "AND CodProducto = '" & FixTSQL(Grid.TextMatrix(Grid.RowSel, ColCodPro)) & "' " & _
        "AND CodRecolector = '" & LcCodUsuario & "' "
        
        ' Considerar aqui el Max ID que puedo afectar de este producto.
        
        Ent.BDD.Execute SQL, AffectedRecords
        
        SQL = "IF NOT EXISTS( " & _
        "SELECT * FROM MA_DEPOPROD " & _
        "WHERE c_CodDeposito = '" & CodDepositoOrigen & "' " & _
        "AND c_CodArticulo = '" & FixTSQL(Grid.TextMatrix(Grid.RowSel, ColCodPro)) & "' " & _
        ") " & _
        "INSERT INTO MA_DEPOPROD (c_CodDeposito, c_CodArticulo, " & _
        "c_Descripcion, n_Cantidad, n_Cant_Ordenada, n_Cant_Comprometida) " & _
        "SELECT " & _
        "'" & CodDepositoOrigen & "', '" & FixTSQL(Grid.TextMatrix(Grid.RowSel, ColCodPro)) & "', " & _
        "'', 0, 0, 0"
        
        Ent.BDD.Execute SQL, AffectedRecords
        
        SQL = "UPDATE MA_DEPOPROD SET " & _
        "n_Cant_Comprometida = ROUND(n_Cant_Comprometida + (" & CDec(CantRecolectada) & "), 8, 0) " & _
        "WHERE c_CodDeposito = '" & CodDepositoOrigen & "' " & _
        "AND c_CodArticulo = '" & FixTSQL(Grid.TextMatrix(Grid.RowSel, ColCodPro)) & "' "
        
        Ent.BDD.Execute SQL, AffectedRecords
        
        mRs.MoveNext
        
    Wend
    
    mRs.Close
    
    If Trans Then
        Ent.BDD.CommitTrans
        Trans = False
    End If
    
    If Not CheckAll Then
        Call ButtonActualizar_Click
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If Trans Then
        Ent.BDD.RollbackTrans
        Trans = False
    End If
    
    If Not CheckAll Then
        
        Mensaje True, "Error al finalizar esta recolecci�n, " & _
        "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
        
        Call PrepararGrid
        Call PrepararDatos
        
    End If
    
End Sub

Private Sub Chk_Ubicacion_Click()
    If Chk_Ubicacion.Value = vbChecked Then
        OrderBy = "ORDER BY isNULL(MAX(Ubicacion), 'N/A') "
        Chk_Peso.Value = vbUnchecked
        Chk_Cantidad.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Chk_Volumen.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Cantidad_Click()
    If Chk_Cantidad.Value = vbChecked Then
        OrderBy = "ORDER BY CantSolicitada DESC "
        Chk_Peso.Value = vbUnchecked
        Chk_Ubicacion.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Chk_Volumen.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_NombreProducto_Click()
    If Chk_NombreProducto.Value = vbChecked Then
        OrderBy = "ORDER BY isNULL(MAX(Nombre), 'N/A') "
        Chk_Peso.Value = vbUnchecked
        Chk_Ubicacion.Value = vbUnchecked
        Chk_Cantidad.Value = vbUnchecked
        Chk_Volumen.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Peso_Click()
    If Chk_Peso.Value = vbChecked Then
        OrderBy = "ORDER BY (SUM(CantSolicitada) * n_Peso) DESC "
        Chk_Cantidad.Value = vbUnchecked
        Chk_Ubicacion.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Chk_Volumen.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Chk_Volumen_Click()
    If Chk_Volumen.Value = vbChecked Then
        OrderBy = "ORDER BY (SUM(CantSolicitada) * n_Volumen) DESC "
        Chk_Cantidad.Value = vbUnchecked
        Chk_Ubicacion.Value = vbUnchecked
        Chk_NombreProducto.Value = vbUnchecked
        Chk_Peso.Value = vbUnchecked
        Call ButtonActualizar_Click
    End If
End Sub

Private Sub Cod_Buscar_GotFocus()
    Cod_Buscar.SelStart = 0
    Cod_Buscar.SelLength = Len(Cod_Buscar)
End Sub

Private Sub Cod_Buscar_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        
        Dim SQL As String, Rs As New ADODB.Recordset, I As Integer, Band As Integer
        
        SQL = "SELECT c_CodNasa " & _
        "FROM MA_CODIGOS " & _
        "WHERE c_Codigo = '" & FixTSQL(Trim(Cod_Buscar.Text)) & "' "
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        If Not Rs.EOF Then
            
            Cod_Buscar = Rs!c_CodNasa
            
            Band = 0
            
            For I = 1 To Grid.Rows - 1
                
                If UCase(Grid.TextMatrix(I, ColCodPro)) = UCase(Cod_Buscar) Then
                    
                    Band = I
                    
                    If CDec(Grid.TextMatrix(I, ColCantSolicitada)) _
                    <> CDec(Grid.TextMatrix(I, ColCantRecolectada)) Then
                        
                        Grid.Row = Band
                        
                        Cod_Buscar = Empty
                        
                        Call Grid_DblClick
                        
                        Exit Sub
                        
                    End If
                    
                End If
                
            Next I
            
            If Band > 0 Then
                Call Mensaje(True, "El producto ya fue recolectado.")
            Else
                Call Mensaje(True, "El producto no se encontr�.")
            End If
            
        Else
            
            Call Mensaje(True, "C�digo no encontrado, verifique el c�digo a buscar.")
            
            SafeFocus Cod_Buscar
            
            Exit Sub
            
        End If
        
        Cod_Buscar = Empty
        
    End If
    
End Sub

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute( _
    "SELECT c_Proceso " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE c_Proceso = '008'")
    
    If Not TempRs.EOF Then
        frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    End If
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub
