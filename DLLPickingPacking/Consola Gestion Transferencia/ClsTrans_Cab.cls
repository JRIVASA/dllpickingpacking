VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsTrans_Cab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarDocumento                                                       As String
Private mvarDocumentoTrans                                                  As String
Private mvarConcepto                                                        As String
Private mvarEstatus                                                         As String
Private mvarFechaEmision                                                    As Date
Private mVarFechaDespacho                                                   As Date
Private mVarFechaMostrar                                                    As Date
Private mvarDepositoOrigen                                                  As String
Private mvarDepositoDestino                                                 As String
Private mvarNomDepositoOrigen                                               As String
Private mvarCodLocalidadDestino                                             As String
Private mvarNomLocalidadDestino                                             As String
Private mvarCodLocalidadOrigen                                              As String
Private mvarCodLocalidadDondeFueHechaLaSolicitud                            As String
'Distincion aca: mvarCodLocalidadDondeFueHechaLaSolicitud no es lo mismo que _
mvarCodLocalidadOrigen. La primera es en que localidad fue hecho el documento, _
y la segunda es de donde va a salir la mercanc�a (a quien va dirigida la solicitud). _
y mvarCodLocalidadDestino vendr�a siendo la localidad que pidi� la mercanc�a. _
Por ende estan los siguientes escenarios: _
' Yo, Localidad X, hago una solicitud desde X hacia Y, para reponer mi inventario de X. _
X es Destino, Y Origen, y X a su vez es DondeFueHechaLaSolicitud. (Solo 2 partes involucradas) _
' o tambi�n podr�a existir: _
Yo, Localidad X, hago una solicitud desde X haciar Y, para reponer inventario de Z. _
Z es Destino, Y Origen, y X es DondeFueHechaLaSolicitud (todas son distintas, hay origen destino e intermediario)
Private mvarObservacion                                                     As String
Private mvarMoneda                                                          As String
Private mvarFactor                                                          As Double
Private mvarSeleccionado                                                    As Boolean
Private mvarAutorizadoPor                                                   As String
Private mLineasDocumento                                                    As New Collection

Property Get Documento() As String
    Documento = mvarDocumento
End Property

Property Get DocumentoTrans() As String
    DocumentoTrans = mvarDocumentoTrans
End Property

Property Let DocumentoTrans(pValor As String)
    mvarDocumentoTrans = pValor
End Property

Property Get Concepto() As String
    Concepto = mvarConcepto
End Property

Property Get Estatus() As String
    Estatus = mvarEstatus
End Property

Property Get FechaEmision() As Date
    FechaEmision = mvarFechaEmision
End Property

Property Get FechaDespacho() As Date
    FechaDespacho = mVarFechaDespacho
End Property

Property Get FechaMostrar() As Date
    FechaMostrar = mVarFechaMostrar
End Property

Property Get CodDepositoOrigen() As String
    CodDepositoOrigen = mvarDepositoOrigen
End Property

Property Get CodDepositoDestino() As String
    CodDepositoDestino = mvarDepositoDestino
End Property

Property Get LocalidadDestino() As String
    LocalidadDestino = mvarCodLocalidadDestino
End Property

Property Get LocalidadOrigen() As String
    LocalidadOrigen = mvarCodLocalidadOrigen
End Property

Property Get LocalidadDondeFueHechaLaSolicitud() As String
    LocalidadDondeFueHechaLaSolicitud = mvarCodLocalidadDondeFueHechaLaSolicitud
End Property

Property Get NombreDepositoOrigen() As String
    NombreDepositoOrigen = mvarNomDepositoOrigen
End Property

Property Get NombreLocalidadDestino() As String
    NombreLocalidadDestino = mvarNomLocalidadDestino
End Property

Property Get Observacion() As String
    Observacion = mvarObservacion
End Property

Property Get Moneda() As String
    Moneda = mvarMoneda
End Property
                                                        
Property Get Factor() As Double
    Factor = mvarFactor
End Property

' Property detalle

Property Get LineasDocumento() As Collection
    Set LineasDocumento = mLineasDocumento
End Property

Property Get ObtenerItem(pIndice) As ClsTrans_Det
    Set ObtenerItem = mLineasDocumento(pIndice)
End Property

Property Get NumeroItems() As Long
   On Error GoTo Errores
   NumeroItems = mLineasDocumento.Count
Errores:
   Err.Clear
End Property

Property Get Seleccionado() As Boolean
    Seleccionado = mvarSeleccionado
End Property

Property Let Seleccionado(pValor As Boolean)
    mvarSeleccionado = pValor
End Property

Property Get AutorizadoPor() As String
    AutorizadoPor = mvarAutorizadoPor
End Property

Property Let AutorizadoPor(pValor As String)
    mvarAutorizadoPor = pValor
End Property

Property Let Moneda(pValor As String)
    mvarMoneda = pValor
End Property

Property Let Factor(pValor As Double)
    mvarFactor = pValor
End Property

Property Let Concepto(pValor As String)
    mvarConcepto = pValor
End Property

Property Let CodDepositoDestino(pValor As String)
    mvarDepositoDestino = pValor
End Property

'******************************************* Constructor *****************************************************************

Function AgregarDocumento(pCn As ADODB.Connection, pRs As ADODB.Recordset) As Boolean
    
    mvarDocumento = pRs!c_Documento
    mvarDocumentoTrans = pRs!cs_Documento_Trans
    'mvarConcepto = pRs!Concepto
    mvarFechaEmision = pRs!d_Fecha
    mVarFechaDespacho = pRs!FechaDespacho
    mVarFechaMostrar = pRs!FechaMostrar
    mvarCodLocalidadOrigen = pRs!CodLocalidadOrigen
    mvarDepositoOrigen = pRs!CodDepositoOrigen
    mvarNomDepositoOrigen = pRs!NombreDepositoOrigen
    mvarCodLocalidadDestino = pRs!CodLocalidadDestino
    mvarNomLocalidadDestino = pRs!LocalidadDestino
    'mvarDepositoDestino = pRs!CodDepositoDestino ' Aqui todav�a no se sabe. Se recupera en el detalle.
    mvarCodLocalidadDondeFueHechaLaSolicitud = pRs!CodLocalidadDondeFueHechaLaSolicitud
    mvarObservacion = pRs!c_Observacion
    mvarMoneda = pRs!c_CodMoneda
    mvarFactor = pRs!n_FactorCambio
    
    If ExisteCampoTabla("c_Status", pRs) Then
        mvarEstatus = pRs!c_Status
    Else
        mvarEstatus = "DPE"
    End If
    
    AgregarDocumento = True
    
End Function

Function AgregarDetalleDocumento(pCn As ADODB.Connection, ByRef pInv As Collection, _
Optional ByVal pTransferencia As Boolean = False, _
Optional ByVal PosibleTransferencia As Boolean = False, _
Optional ByVal pForzarVerDatosDeSolicitud As Boolean = False _
) As Boolean
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    Dim mLinea As ClsTrans_Det
    Dim mCont As Long
    
    If Not pTransferencia Or pForzarVerDatosDeSolicitud Then
        
        If PosibleTransferencia Then ' Para buscar detalles de una NDT.
            
            mvarConcepto = "NDT" 'NOTA DE TRANSFERENCIA (PACKING)
            'A DIFERENCIA DE PICKING Y PACKING DE PEDIDOS AQUI NO SE DESCARGA INVENTARIO HASTA
            'QUE SE PROCESE EL LOTE, POR ESO SE USO UNA NUEVA TABLA EN VEZ DE UN MOVIMIENTO DE INVENTARIO.
            
            mSQL = "SELECT TR.c_Documento AS c_Documento, TR.c_CodArticulo AS c_CodArticulo, " & _
            "TR.n_Cantidad AS Cantidad, TR.n_Costo AS n_CostoDoc, TR.ns_CantidadEmpaque AS n_CantiBul_Doc, " & _
            "P.c_Descri, P.c_CodMoneda AS CodMonProd, P.n_CostoAct, P.n_CostoAnt, " & _
            "P.n_CostoPro, P.n_CostoRep, P.n_Precio1, P.n_CantiBul, P.n_TipoPeso, " & _
            "P.Cant_Decimales, P.n_Impuesto1 AS Impuesto1, P.n_Volumen, P.n_VolBul, " & _
            "P.n_Peso, P.n_PesoBul, (TR.n_Cantidad * TR.n_Costo) AS Subtotal, " & _
            "MA.c_CodMoneda AS c_CodMonedaDoc, MA.n_FactorCambio AS n_FactorCambioDoc, " & _
            "'" & mvarConcepto & "' AS c_Concepto, MA.ID AS ID, " & _
            "MA.c_CodDeposito_Destino AS CodDepositoDestino " & _
            "FROM TR_PACKING_TRANSFERENCIA TR " & _
            "INNER JOIN MA_PACKING_TRANSFERENCIA MA " & _
            "ON TR.c_Documento = MA.c_Documento " & _
            "AND TR.cs_CodLocalidad = MA.cs_CodLocalidad " & _
            "LEFT JOIN MA_REQUISICIONES V " & _
            "ON MA.c_Relacion = 'SOL_TRA ' + V.cs_Documento " & _
            "AND MA.cs_CodLocalidad_Relacion = V.cs_CodLocalidad " & _
            "LEFT JOIN MA_PRODUCTOS P " & _
            "ON TR.c_CodArticulo = P.c_Codigo " & _
            "WHERE V.cs_Documento = '" & mvarDocumento & "' " & _
            "AND V.cs_Estado NOT IN ('DWT') "
            
            mSQL = mSQL & "ORDER BY TR.c_CodArticulo, TR.n_Linea "
            
        Else ' Para buscar detalles de una solicitud.
            
            If pForzarVerDatosDeSolicitud Then
                mCantidad = "T.ns_Cantidad AS Cantidad"
                mCantidadRestante = vbNullString
            Else
                mCantidad = "T.ns_Cantidad AS Cantidad"
                mCantidadRestante = "AND T.ns_Cantidad > 0 "
            End If
            
            mSQL = "SELECT T.cs_Documento AS c_Documento, T.cs_CodArticulo AS c_CodArticulo, " & mCantidad & ", " & _
            "T.ns_Costo AS n_CostoDoc, T.ns_CantidadEmpaque AS n_CantiBul_Doc, " & _
            "P.c_Descri, P.c_CodMoneda AS CodMonProd, P.n_CostoAct, " & _
            "P.n_CostoAnt, P.n_CostoPro, P.n_CostoRep, P.n_Precio1, P.n_CantiBul, P.n_TipoPeso, " & _
            "P.Cant_Decimales, P.n_Impuesto1 AS Impuesto1, P.n_Volumen, P.n_VolBul, " & _
            "P.n_Peso, P.n_PesoBul, (T.ns_Cantidad * T.ns_Costo) AS Subtotal, " & _
            "V.cs_CodMoneda AS c_CodMonedaDoc, V.ns_Factor_Cambio AS n_FactorCambioDoc, " & _
            "'SOL_TRA' AS c_Concepto, 0 AS ID, '' AS CodDepositoDestino " & _
            "FROM TR_REQUISICIONES T " & _
            "INNER JOIN MA_REQUISICIONES V " & _
            "ON T.cs_Documento = V.cs_Documento " & _
            "AND T.cs_CodLocalidad = V.cs_CodLocalidad " & _
            "LEFT JOIN MA_PRODUCTOS P " & _
            "ON T.cs_CodArticulo = P.c_Codigo " & _
            "WHERE T.cs_Documento = '" & mvarDocumento & "' " & _
            "AND V.cs_Estado NOT IN ('DWT', 'ANU') " & _
            mCantidadRestante
            
            mSQL = mSQL & "ORDER BY T.cs_CodArticulo, T.ns_Linea "
            
        End If
        
    Else
        
        mSQL = "SELECT T.c_Documento, T.c_CodArticulo, T.n_Cantidad AS Cantidad, " & _
        "T.n_Costo AS n_CostoDoc, T.ns_CantidadEmpaque AS n_CantiBul_Doc, P.c_Descri, " & _
        "P.c_CodMoneda AS CodMonProd, P.n_CostoAct, P.n_CostoAnt, P.n_CostoPro, " & _
        "P.n_CostoRep, P.n_Precio1, P.n_CantiBul, P.n_TipoPeso, P.Cant_Decimales, " & _
        "P.n_Impuesto1 AS Impuesto1, P.n_Volumen, P.n_VolBul, P.n_Peso, P.n_PesoBul, " & _
        "T.n_Subtotal AS Subtotal, V.c_CodMoneda AS c_CodMonedaDoc, " & _
        "V.n_FactorCambio AS n_FactorCambioDoc, T.c_Concepto, T.ID, " & _
        "V.c_Dep_Dest AS CodDepositoDestino " & _
        "FROM TR_INVENTARIO T " & _
        "INNER JOIN MA_INVENTARIO V " & _
        "ON T.c_Documento = V.c_Documento " & _
        "AND T.c_Concepto = V.c_Concepto " & _
        "AND T.c_CodLocalidad = V.c_CodLocalidad " & _
        "INNER JOIN MA_PRODUCTOS P " & _
        "ON T.c_CodArticulo = P.c_Codigo " & _
        "WHERE T.c_Documento = '" & mvarDocumentoTrans & "' " & _
        "AND T.c_Concepto = 'TRA' " & _
        "AND T.c_TipoMov = 'Descargo' "
        
        mSQL = mSQL & "ORDER BY T.c_Linea, T.ID, T.c_CodArticulo "
        
    End If
    
    mRs.CursorLocation = adUseClient
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    'mRs.Sort = "c_CodArticulo" ' Dejar que venga ordenado por linea / orden de entrada
    'Para que tambien la factura se genere como en el orden del pedido / nde
    
    If Not mRs.EOF Then
        
        mRs.Filter = "c_Descri = NULL" ' Para validar que no hayan inconsistencias _
        de productos no creados cuando se hace el Left Join. Si hay inconsistencia no cargar pedido.
        
        If Not mRs.EOF Then
            Exit Function
        End If
        
        mRs.Filter = vbNullString
        
        Set MonedaDoc = FrmAppLink.ClaseMonedaDocumento
        
        'Debug.Assert Not UCase(mRs!c_CodMonedaDoc) <> UCase(CodMonPred)
        MonedaDoc.BuscarMonedas , mRs!c_CodMonedaDoc
        
        Me.Moneda = mRs!c_CodMonedaDoc
        
        'primer cambio para mantener el documento en la moneda original se hace para guardar el factor actual
        'y no del documento origen
        'If mRs!n_FactorCambioDoc <> MonedaDoc.FacMoneda Then
            'Me.Factor = MonedaDoc.FacMoneda
        'Else
            Me.Factor = mRs!n_FactorCambioDoc
        'End If
        
        Me.CodDepositoDestino = mRs!CodDepositoDestino
        
        Do While Not mRs.EOF
            
            Set mLinea = New ClsTrans_Det
            
            mCont = mCont + 1
            
            'Debug.Assert Not mRs!c_CodArticulo = "012562"
            
            If mLinea.AgregarLineaItemDocumento(mRs, mCont, Me) Then
                
                Me.Concepto = mLinea.TipoDocumentoRel
                
                If pTransferencia Or PosibleTransferencia Or pForzarVerDatosDeSolicitud Then
                    mLinea.CantidadInvDisponible = mLinea.CantidadSolicitadaItem
                    mLinea.CantidadAsignadaItem = mLinea.CantidadSolicitadaItem
                Else
                    VerificarExistencia pCn, mLinea, pInv
                End If
                
                mLineasDocumento.Add mLinea
                
            End If
            
            mRs.MoveNext
            
        Loop
        
    Else
        
        If Not PosibleTransferencia Then
            Exit Function
        End If
        
    End If
    
    AgregarDetalleDocumento = True
    
End Function

Public Function TotalizarFac(Optional pSubtotal As Boolean = False) As Double
    
    Dim mLin As ClsTrans_Det
    
    For Each mLin In mLineasDocumento
        If pSubtotal Then
            mTotal = mTotal + mLin.SubtotalItemFac
        Else
            mTotal = mTotal + mLin.TotalItemFac
        End If
    Next
    
    TotalizarFac = mTotal
    
End Function

Public Function Totalizar(Optional pSubtotal As Boolean = False) As Double
    
    Dim mLin As ClsTrans_Det
    Dim mTotal As Double
    
    For Each mLin In mLineasDocumento
        If pSubtotal Then
            mTotal = mTotal + mLin.SubtotalItem
        Else
            mTotal = mTotal + mLin.TotalItem
        End If
    Next
    
    Totalizar = mTotal
    
End Function

Public Function TotalizarVolumen(ByVal mFormaDeTrabajo) As Double
    
    Dim mLin As ClsTrans_Det
    Dim mTotal As Double
    
    For Each mLin In mLineasDocumento
        If mFormaDeTrabajo = 0 Then
            mTotal = mTotal + mLin.VolumenUnitarioItemAsignado
        Else
            mTotal = mTotal + mLin.VolumenItemAsignado
        End If
    Next
    
    TotalizarVolumen = mTotal
    
End Function

Public Function TotalizarPeso(ByVal mFormaDeTrabajo) As Double
    
    Dim mLin As ClsTrans_Det
    Dim mTotal As Double
    
    For Each mLin In mLineasDocumento
        If mFormaDeTrabajo = 0 Then
            mTotal = mTotal + mLin.PesoUnitarioItemAsignado
        Else
            mTotal = mTotal + mLin.PesoItemAsignado
        End If
    Next
    
    TotalizarPeso = mTotal
    
End Function

Public Function TotalizarEmpaquesGrid(ByVal mFormaDeTrabajo) As Double
    
    Dim mLin As ClsTrans_Det
    Dim mTotal As Double
    
    For Each mLin In mLineasDocumento
        
        If mFormaDeTrabajo = 0 Then ' Solo Unidades
            mTotal = 0
            Exit For
        Else ' Empaques y / o Unidades
            mTotal = mTotal + mLin.CantidadEmpaquesGrid
        End If
        
    Next
    
    TotalizarEmpaquesGrid = mTotal
    
End Function

Public Function TotalizarUnidadesGrid(ByVal mFormaDeTrabajo) As Double
    
    Dim mLin As ClsTrans_Det
    Dim mTotal As Double
    
    For Each mLin In mLineasDocumento
        If mFormaDeTrabajo = 0 Then ' Solo Unidades
            mTotal = mTotal + mLin.CantidadSolicitadaItem
        Else ' Empaques y / o Unidades
            mTotal = mTotal + mLin.CantidadUnidadesGrid
        End If
    Next
    
    TotalizarUnidadesGrid = mTotal
    
End Function

Property Get TotalizarImpuestos( _
Optional pEsBase As Boolean = False) As Double
    
    Dim mItm As ClsTrans_Det
    Dim mTotal As Double
    
    For Each mItm In mLineasDocumento
        
        If mItm.ImpuestoItem > 0 Then
            
            If pEsBase Then
                mTotal = mTotal + mItm.SubtotalItem
            Else
                mTotal = mTotal + mItm.ImpuestoItem
            End If
            
        End If
        
    Next
    
    TotalizarImpuestos = mTotal
    
End Property

Private Sub VerificarExistencia(pCn As ADODB.Connection, _
pItm As ClsTrans_Det, pInv As Collection)
    
    Dim mInv As ClsTrans_Det_Inv, mCantidad As Double
    
    'Set mInv = ExisteArticuloInvLoc(pItm.CodigoItem, mvarCodLocalidadDestino, pInv)
    Set mInv = ExisteArticuloInv(pItm.CodigoItem, mvarDepositoOrigen, pInv)
    
    If mInv Is Nothing Then
        
        Set mInv = New ClsTrans_Det_Inv
        
        'If piTm.TipoProducto <> IntTipoInformativo And piTm.TipoProducto <> intTipoCarExt Then 'Modificado
        If pItm.TipoProducto <> 4 Then
            
            VerificarExistenciaProdExt = BuscarReglaNegocioBoolean("TRA_Consola_VerificarExistenciaProdExt", "1")
            
            If VerificarExistenciaProdExt Then
                
                If pItm.TipoProducto = 3 Then 'intTipoCarExt Then
                    
                    'mCantidad = BuscarCantidadProdExtxLocalidad(pCn, pItm.CodigoItem, mvarCodLocalidadDestino) + _
                    BuscarCantidadPickingLoc(pCn, pItm.CodigoItem, mvarCodLocalidadDestino)
                    
                    mCantidad = BuscarCantidadDepoProdExt(pCn, pItm.CodigoItem, mvarDepositoOrigen) - _
                    BuscarCantidadPicking(pCn, pItm.CodigoItem)
                    
                Else
                    
                    'mCantidad = BuscarCantidadProdxLocalidad(pCn, pItm.CodigoItem, mvarCodLocalidadDestino) + _
                    BuscarCantidadPickingLoc(pCn, pItm.CodigoItem, mvarCodLocalidadDestino)
                    
                    mCantidad = BuscarCantidadDepoProd(pCn, pItm.CodigoItem, mvarDepositoOrigen) - _
                    BuscarCantidadPicking(pCn, pItm.CodigoItem)
                    
                End If
                
            Else
                
                'mCantidad = BuscarCantidadProdxLocalidad(pCn, pItm.CodigoItem, mvarCodLocalidadDestino) + _
                BuscarCantidadPickingLoc(pCn, pItm.CodigoItem, mvarCodLocalidadDestino)
                
                mCantidad = BuscarCantidadDepoProd(pCn, pItm.CodigoItem, mvarDepositoOrigen) - _
                BuscarCantidadPicking(pCn, pItm.CodigoItem)
                
            End If
            
            mCantidad = RoundUp(mCantidad, pItm.NumeroDecimales)
            
        Else
            
            mCantidad = pItm.CantidadSolicitadaItem
            
        End If
        
        'pInv.Add mInv.AddProductoxLocalidad(pItm.CodigoItem, mvarCodLocalidadDestino, mCantidad)
        pInv.Add mInv.AddProductoxDeposito(pItm.CodigoItem, mvarDepositoOrigen, mCantidad)
        
    Else
        
        If pItm.TipoProducto = 4 Then 'IntTipoInformativo Then
            
            mCantidad = pItm.CantidadSolicitadaItem
            
            'mInv.AddProductoxLocalidad pItm.CodigoItem, mvarCodLocalidadDestino, (mInv.Cantidad + mCantidad)
            mInv.AddProductoxDeposito pItm.CodigoItem, mvarDepositoOrigen, (mInv.Cantidad + mCantidad)
            
        End If
        
    End If
    
    'pItm.CantidadInvDisponible = mInv.CantidadDisponible
    'mInv.AsignarCantidadDisponibleLoc mvarDocumento, pItm.CantidadSolicitadaItem
    'pItm.CantidadAsignadaItem = mInv.UltimaCantidadSolicitud
    'pItm.PoseeInv = (pItm.CantidadAsignadaItem > 0)
    
    pItm.CantidadInvDisponible = mInv.CantidadDisponible
    mInv.AsignarCantidadDisponible mvarDocumento, pItm.CantidadSolicitadaItem
    pItm.CantidadAsignadaItem = mInv.UltimaCantidadSolicitud
    pItm.PoseeInv = (pItm.CantidadAsignadaItem > 0)
    
End Sub

Private Function ExisteArticuloInv(pCodigo As String, _
pDeposito As String, pInv As Collection) As ClsTrans_Det_Inv
    
    Dim mDoc As ClsTrans_Det_Inv
    
    For Each mDoc In pInv
        
        If UCase(mDoc.Codigo) = UCase(pCodigo) _
        And UCase(mDoc.DepositoOrigen) = UCase(pDeposito) Then
            
            Set ExisteArticuloInv = mDoc
            Exit Function
            
        End If
        
    Next
    
End Function

Private Function ExisteArticuloInvLoc(pCodigo As String, _
pLocalidad As String, pInv As Collection) As ClsTrans_Det_Inv
    
    Dim mDoc As ClsTrans_Det_Inv
    
    For Each mDoc In pInv
        
        If UCase(mDoc.Codigo) = UCase(pCodigo) _
        And UCase(mDoc.LocalidadDestino) = UCase(pLocalidad) Then
            
            Set ExisteArticuloInvLoc = mDoc
            Exit Function
            
        End If
        
    Next
    
End Function

Private Function BuscarCantidadDepoProd(pCn As ADODB.Connection, _
ByVal pCodigo As String, ByVal pDeposito As String) As Double
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "SELECT * FROM MA_DEPOPROD " & _
    "WHERE c_CodArticulo = '" & pCodigo & "' " & _
    "AND c_CodDeposito = '" & pDeposito & "' "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarCantidadDepoProd = mRs!n_Cantidad
    End If
    
    mRs.Close
    
End Function

Private Function BuscarCantidadProdxLocalidad(pCn As ADODB.Connection, _
ByVal pCodigo As String, ByVal pLocalidad As String) As Double
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "SELECT * FROM MA_DEPOPROD " & _
    "WHERE c_CodArticulo = '" & FixTSQL(pCodigo) & "' " & _
    "AND c_CodDeposito IN (SELECT c_CodDeposito FROM MA_DEPOSITO " & _
    "WHERE c_CodLocalidad = '" & FixTSQL(pLocalidad) & "') "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarCantidadProdxLocalidad = mRs!n_Cantidad
    End If
    
    mRs.Close
    
End Function

Private Function BuscarCantidadDepoProdExt(pCn As ADODB.Connection, _
ByVal pCodigo As String, ByVal pDeposito As String) As Double
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "SELECT isNULL(SUM(n_Cantidad), 0) AS n_Cantidad " & _
    "FROM MA_DEPOPROD " & _
    "WHERE c_CodArticulo IN (SELECT c_Codigo FROM MA_PRODUCTOS " & _
    "WHERE c_Codigo_Base = '" & FixTSQL(pCodigo) & "') " & _
    "AND c_CodDeposito = '" & FixTSQL(pDeposito) & "' "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarCantidadDepoProdExt = mRs!n_Cantidad
    End If
    
    mRs.Close
    
End Function

Private Function BuscarCantidadProdExtxLocalidad(pCn As ADODB.Connection, _
ByVal pCodigo As String, ByVal pLocalidad As String) As Double
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "SELECT isNULL(SUM(n_Cantidad), 0) AS n_Cantidad " & _
    "FROM MA_DEPOPROD " & _
    "WHERE c_CodArticulo IN (SELECT c_Codigo FROM MA_PRODUCTOS " & _
    "WHERE c_Codigo_Base = '" & FixTSQL(pCodigo) & "') " & _
    "AND c_CodDeposito IN (SELECT c_CodDeposito FROM MA_DEPOSITO " & _
    "WHERE c_CodLocalidad = '" & FixTSQL(pDeposito) & "') "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarCantidadProdExtxLocalidad = mRs!n_Cantidad
    End If
    
    mRs.Close
    
End Function

Private Function BuscarCantidadPicking(pCn As ADODB.Connection, _
ByVal pCodigo As String) As Double
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    '"AND TR.b_PackingFinalizado = 0 " & _
    "AND TR.c_Documento_Nota = '' " &
    
    ' Cambiado criterio anterior por
    '"AND MP.b_Finalizada = 0 " & _
    "AND MP.b_Anulada = 0 " &
    
    ' Ya que a diferencia del Packing de Pedidos de Venta, el Packing de la Transferencia _
    No esta registrando un movimiento que descargue inventario de manera anticipada _
    Sino cuando se cierra el lote, ejecutando las transferencias (Cargos y Descargos). _
    Ahi es cuando se debe dejar de considerar la cantidad en transito como CantidadPicking.
    
    mSQL = "SELECT isNULL(SUM(ns_Cantidad), 0) AS CantEnProceso " & vbNewLine & _
    "FROM (" & vbNewLine & _
    "SELECT DET.cs_CodArticulo, DET.ns_Cantidad " & _
    "FROM TR_LOTE_GESTION_TRANSFERENCIA TP " & _
    "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA MP " & _
    "ON TP.cs_Corrida = MP.cs_Corrida " & _
    "AND MP.b_Finalizada = 0 " & _
    "AND MP.b_Anulada = 0 " & _
    "INNER JOIN MA_REQUISICIONES CAB " & _
    "ON TP.c_Documento = CAB.cs_Documento " & _
    "AND TP.cs_CodLocalidad_Solicitud = CAB.cs_CodLocalidad " & _
    "INNER JOIN TR_REQUISICIONES DET " & _
    "ON DET.cs_Documento = CAB.cs_Documento " & _
    "AND DET.cs_CodLocalidad = CAB.cs_CodLocalidad " & _
    "AND DET.cs_CodArticulo = '" & FixTSQL(pCodigo) & "' " & vbNewLine & _
    ") TB "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarCantidadPicking = isDBNull(mRs!CantEnProceso, 0)
    End If
    
    mRs.Close
    
End Function

Private Function BuscarCantidadPickingLoc(pCn As ADODB.Connection, _
ByVal pCodigo As String, ByVal pLocalidadDestino As String) As Double
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "SELECT isNULL(SUM(ns_Cantidad), 0) AS CantEnProceso " & vbNewLine & _
    "FROM (" & vbNewLine & _
    "SELECT DET.cs_CodArticulo, DET.ns_Cantidad " & _
    "FROM TR_LOTE_GESTION_TRANSFERENCIA TP " & _
    "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA MP " & _
    "ON TP.cs_Corrida = MP.cs_Corrida " & _
    "AND b_PackingFinalizado = 0 " & _
    "AND c_Documento_Nota = '' " & _
    "INNER JOIN MA_REQUISICIONES CAB " & _
    "ON TP.c_Documento = CAB.cs_Documento " & _
    "AND TP.cs_CodLocalidad_Solicitud = CAB.cs_CodLocalidad " & _
    "INNER JOIN TR_REQUISICIONES DET " & _
    "ON DET.cs_Documento = CAB.cs_Documento " & _
    "AND DET.cs_CodLocalidad = CAB.cs_CodLocalidad " & _
    "AND DET.cs_CodArticulo = '" & FixTSQL(pCodigo) & "' " & _
    "AND CAB.cs_Deposito_Despachar = '" & FixTSQL(pLocalidadDestino) & "' " & vbNewLine & _
    ") TB "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarCantidadPickingLoc = isDBNull(mRs!CantEnProceso, 0)
    End If
    
    mRs.Close
    
End Function
