VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmTransferencia_ConsolaGestion_Detalle 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   60
   ClientWidth     =   15330
   ControlBox      =   0   'False
   Icon            =   "FrmTransferencia_ConsolaGestion_Detalle.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton CmdDatosPedido 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Left            =   120
      Picture         =   "FrmTransferencia_ConsolaGestion_Detalle.frx":74F2
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   600
      Width           =   1575
   End
   Begin VB.Frame Frame6 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmTransferencia_ConsolaGestion_Detalle.frx":9274
         Top             =   -40
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Detalle"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12675
         TabIndex        =   5
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      ScaleHeight     =   225
      ScaleWidth      =   225
      TabIndex        =   1
      Top             =   10080
      Width           =   255
   End
   Begin MSFlexGridLib.MSFlexGrid grd 
      Height          =   7935
      Left            =   120
      TabIndex        =   0
      Top             =   1920
      Width           =   15060
      _ExtentX        =   26564
      _ExtentY        =   13996
      _Version        =   393216
      FixedCols       =   0
      RowHeightMin    =   400
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      Caption         =   "Presione F2 para ver  detalle del Inventario"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5880
      TabIndex        =   3
      Top             =   10560
      Width           =   5535
   End
   Begin VB.Label Label1 
      Caption         =   "Sin Existencia"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   720
      TabIndex        =   2
      Top             =   10080
      Width           =   1815
   End
   Begin VB.Menu mnu2 
      Caption         =   "menu"
      Visible         =   0   'False
      Begin VB.Menu disponible 
         Caption         =   "Ver Disponibilidad"
         Shortcut        =   {F2}
      End
   End
End
Attribute VB_Name = "FrmTransferencia_ConsolaGestion_Detalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As ClsTrans_Gestion
Dim mCargo As Boolean

Private Sub CmdDatosPedido_Click()
    
    Dim Texto As String, TmpRs As Object
    
    Texto = vbNewLine ' Empty
    
    Texto = Texto & "No. Solicitud: " & GetTab(2) & fCls.DocumentoSeleccionado.Documento
    Texto = Texto & GetLines(2)
    
    Texto = Texto & "Localidad Destino: " & GetTab(2) & _
    fCls.DocumentoSeleccionado.NombreLocalidadDestino & _
    " [" & fCls.DocumentoSeleccionado.LocalidadDestino & "]"
    Texto = Texto & GetLines(2)
    
    Texto = Texto & "Fecha de Emisi�n: " & GetTab(3) & _
    GDate(fCls.DocumentoSeleccionado.FechaEmision)
    Texto = Texto & GetLines(2)
    
    Texto = Texto & "Fecha de Entrega Solicitada: " & GetTab(3) & _
    GDate(fCls.DocumentoSeleccionado.FechaDespacho)
    Texto = Texto & GetLines(2)
    
    'Texto = Texto & "Monto total (Disponible): " & GetTab(1) & FormatNumber(fCls.DocumentoSeleccionado.Totalizar, Std_Decm)
    'Texto = Texto & GetLines(2)
    
    Texto = Texto & "Empaques: " & GetTab(2) & _
    FrmAppLink.FormatoDecimalesDinamicos( _
    fCls.DocumentoSeleccionado.TotalizarEmpaquesGrid(fCls.FormaTrabajoTransferencia))
    Texto = Texto & GetLines(2)
    
    Texto = Texto & "Unidades: " & GetTab(2) & _
    FrmAppLink.FormatoDecimalesDinamicos( _
    fCls.DocumentoSeleccionado.TotalizarUnidadesGrid(fCls.FormaTrabajoTransferencia))
    Texto = Texto & GetLines(2)
    
    Texto = Texto & "Cant. Total: " & GetTab(2) & _
    FrmAppLink.FormatoDecimalesDinamicos( _
    fCls.DocumentoSeleccionado.TotalizarUnidadesGrid(0))
    Texto = Texto & GetLines(2)
    
    Texto = Texto & "Total Peso: " & GetTab(2) & _
    FrmAppLink.FormatoDecimalesDinamicos(fCls.DocumentoSeleccionado.TotalizarPeso(mvarFormaTrabajoTransferencias))
    Texto = Texto & GetLines(2)
    
    Texto = Texto & "Total Volumen: " & GetTab(2) & _
    FrmAppLink.FormatoDecimalesDinamicos(fCls.DocumentoSeleccionado.TotalizarVolumen(mvarFormaTrabajoTransferencias))
    Texto = Texto & GetLines(2)
    
    Texto = Texto & "Observaciones: " & GetTab(2)
    
    mCadena = Split(SplitStringIntoFixedLengthLines(75, _
    fCls.DocumentoSeleccionado.Observacion & " ", False), vbNewLine)
    
    For i = 0 To UBound(mCadena)
        
        If i > 0 Then Texto = Texto & GetTab(3)
        Texto = Texto & mCadena(i) & vbNewLine
        
    Next i
    
    Texto = Texto & GetLines(1)
    
    Set FrmMostrarXML = FrmAppLink.GetFrmMostrarXML
    
    FrmMostrarXML.Caption = "Informaci�n del Pedido"
    FrmMostrarXML.TextArea.Text = Texto
    Set FrmMostrarXML.TmpAlternateFont = FrmAppLink.GetFont("Console", "13")
    FrmMostrarXML.Show vbModal
    
    Set FrmMostrarXML = Nothing
    
End Sub

Private Sub disponible_Click()
    
    Dim mItm As ClsTrans_Det
    
    On Error GoTo Errores
    
    Set mItm = fCls.DocumentoSeleccionado.LineasDocumento(grd.Row)
    
    fCls.VerDetalleInventario mItm
    
Errores:
    
    Err.Clear
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    If Not mCargo Then
        mCargo = True
        'If fCls.NumDocPendientes <= 0 And fCls.TipoGrid = eGrdDocumentosCxc Then
            'Mensaje True, "No posee documentos pendientes, el cliente no posee Cr�dito."
            'Unload Me
        'End If
    End If
End Sub

Private Sub Form_Load()
    
    Me.Picture1.BackColor = fCls.ColorSinExistencia
    
    fCls.IniciarGrid grd
    
    If fCls.TipoGrid = eGrdDocumentosDet Then
        fCls.LlenarDocumentosGrdDet grd
        Me.lbl_Organizacion = "Detalle de Solicitud (" & fCls.DocumentoSeleccionado.Documento & ")"
    End If
    
    CmdDatosPedido.Caption = "Datos del Pedido"
    CmdDatosPedido.Enabled = True
    
End Sub

Private Sub grd_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim mItm As ClsTrans_Det
    
    On Error GoTo Errores
    
    If grd.Row > 0 Then
        Select Case KeyCode
            Case vbKeyF2
                If fCls.TipoGrid = eGrdDocumentosDet Then
                    Set mItm = fCls.DocumentoSeleccionado.LineasDocumento(grd.Row)
                    disponible_Click
                End If
        End Select
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
End Sub

Private Sub grd_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    On Error GoTo Errores
    
    If Button = 2 And grd.Row > 0 And fCls.TipoGrid = eGrdDocumentosDet Then
        'fCls.EstablecerVentana Me.hWnd
        Me.PopupMenu Me.mnu2
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
End Sub

Private Sub Label2_Click()
    If fCls.TipoGrid = eGrdDocumentosDet Then
        disponible_Click
    End If
End Sub
