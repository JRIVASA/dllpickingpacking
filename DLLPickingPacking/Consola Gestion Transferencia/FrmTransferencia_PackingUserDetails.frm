VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmTransferencia_PackingUserDetails 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Chk_Volumen 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Volumen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   300
      Left            =   2880
      TabIndex        =   10
      Top             =   10480
      Width           =   1785
   End
   Begin VB.CheckBox Chk_Peso 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Peso"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   300
      Left            =   480
      TabIndex        =   9
      Top             =   10480
      Width           =   1785
   End
   Begin VB.Frame FramePickAll 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      BorderStyle     =   0  'None
      ForeColor       =   &H0000C000&
      Height          =   855
      Left            =   13560
      TabIndex        =   15
      Top             =   9780
      Width           =   1635
      Begin VB.Label CmdPickAll 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "   Empacar   Todo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   555
         Left            =   0
         TabIndex        =   16
         Top             =   165
         Width           =   1635
      End
   End
   Begin VB.CommandButton Cmd_Listo 
      Caption         =   "Finalizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   12360
      Picture         =   "FrmTransferencia_PackingUserDetails.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   9780
      Width           =   1095
   End
   Begin VB.Frame buscar 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   " C�digo a Buscar: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   915
      Left            =   6360
      TabIndex        =   12
      Top             =   9720
      Width           =   4605
      Begin VB.TextBox Cod_Buscar 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   240
         MaxLength       =   15
         TabIndex        =   13
         Top             =   240
         Width           =   4125
      End
   End
   Begin VB.CommandButton ButtonLeyenda 
      Caption         =   "Leyenda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   11100
      Picture         =   "FrmTransferencia_PackingUserDetails.frx":1D82
      Style           =   1  'Graphical
      TabIndex        =   11
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9780
      Width           =   1095
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   19335
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12480
         TabIndex        =   5
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14520
         Picture         =   "FrmTransferencia_PackingUserDetails.frx":2A4C
         Top             =   0
         Width           =   480
      End
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Pedido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   120
         Width           =   10695
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   8850
      LargeChange     =   10
      Left            =   14480
      TabIndex        =   1
      Top             =   600
      Width           =   674
   End
   Begin VB.CheckBox Chk_NombreProducto 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Art�culo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   300
      Left            =   480
      TabIndex        =   0
      Top             =   10110
      Width           =   1785
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   8865
      Left            =   160
      TabIndex        =   2
      Top             =   600
      Width           =   15000
      _ExtentX        =   26458
      _ExtentY        =   15637
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox Chk_Cantidad 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Cantidad Solicitada"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   300
      Left            =   2880
      TabIndex        =   8
      Top             =   10110
      Width           =   2745
   End
   Begin VB.Label C�digo 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Criterios de Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Index           =   71
      Left            =   360
      TabIndex        =   6
      Top             =   9720
      Width           =   3045
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00AE5B00&
      X1              =   3480
      X2              =   6000
      Y1              =   9870
      Y2              =   9870
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1215
      Index           =   1
      Left            =   180
      TabIndex        =   7
      Top             =   9600
      Width           =   6015
   End
End
Attribute VB_Name = "FrmTransferencia_PackingUserDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Pedido As String
Public Lote As String
Public LocalidadSolicitud As String

Private Enum GrdPack
    ColNull
    ColCodigoAlterno
    ColDesPro
    ColCantSolicitada
    ColCantRecolectada
    ColCantEmpacada
    ColPesoRec
    ColVolumenRec
    ColCodPro
    ColDecimales
    ColCount
End Enum

'Const ConceptoPacking = "TRS" ' Crear un Traslado temporal como Gu�a de Despacho.

Private AnchoCampoDescripcion As Long

Private OrderBy As String
Private Salir As Variant
Private CambioCantidad As Boolean

Private CodDepositoOrigen As String
Private CodLocalidadOrigen As String
Private CodDepositoTransito As String

Public DocumentoNDT As String
Public PedidoWeb As String
Public EmpaquesMobile As Long
Public CodEmpacadorMobile As String

Private FormaCargada As Boolean
Private CheckAll As Boolean

Private Sub Form_Activate()
    
    If Salir Then
        
        Mensaje True, "No hay art�culos pendientes por empacar."
        
        Unload Me
        
        Exit Sub
        
    End If
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        CodDepositoOrigen = BuscarReglaNegocioStr( _
        "TRA_Consola_CodDepositoOrigen", Empty)
        
        If Trim(CodDepositoOrigen) = Empty Then
            CodDepositoOrigen = FrmAppLink.GetCodDepositoPredeterminado
        End If
        
        CodLocalidadOrigen = BuscarValorBD("c_CodLocalidad", _
        "SELECT c_CodLocalidad " & _
        "FROM MA_DEPOSITO " & _
        "WHERE c_CodDeposito = '" & CodDepositoOrigen & "' ", Empty, Ent.BDD)
        
        If Trim(CodLocalidadOrigen) = Empty Then
            Mensaje True, "La localidad o el dep�sito origen " & _
            "establecido no existe o los datos son inv�lidos."
            Unload Me
            Exit Sub
        End If
        
        CodLocalidadDestino = BuscarValorBD("CodLocalidad", _
        "SELECT cs_Deposito_Despachar AS CodLocalidad " & _
        "FROM MA_REQUISICIONES " & _
        "WHERE cs_Documento = '" & FixTSQL(Me.Pedido) & "' " & _
        "AND cs_CodLocalidad = '" & FixTSQL(LocalidadSolicitud) & "' ", Empty, Ent.BDD)
        
        If Trim(CodLocalidadDestino) = Empty Then
            Mensaje True, "No se pudo recuperar la localidad destino de la Solicitud."
            Unload Me
            Exit Sub
        End If
        
        CodDepositoTransito = BuscarReglaNegocioStr( _
        "TRA_DEPOSITO_TRANSITO_[" & CodLocalidadDestino & "]", Empty)
        
        If Trim(CodDepositoTransito) = Empty Then
            Mensaje True, "El dep�sito transito no est� establecido " & _
            "para la localidad del pedido (" & CodLocalidadDestino & "). "
            Unload Me
            Exit Sub
        End If
        
        TR_INV_n_FactorMonedaProd = ExisteCampoTabla(Empty, , _
        "SELECT n_FactorMonedaProd FROM TR_INVENTARIO WHERE 1 = 2", , Ent.BDD)
        
        Moneda_Dec = FrmAppLink.DecMonedaPref 'BuscarValorBD("Cant_Decimales", _
        "SELECT Cant_Decimales FROM MA_MONEDAS WHERE b_Preferencia = 1", "2", Ent.BDD)
        
    End If
    
    If gRutinas.PuedeObtenerFoco(Cod_Buscar) Then
        Cod_Buscar.SetFocus
    End If
    
End Sub

Private Sub Form_Load()
    
    AjustarPantalla Me
    
    If Chk_NombreProducto.Value = vbUnchecked Then
        Chk_NombreProducto.Value = vbChecked
    Else
        Chk_NombreProducto_Click
    End If
    
    Salir = False
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionByRow
        
        .Rows = 2
        
        .FixedCols = 0
        .FixedRows = 1
        
        .Rows = 1
        .Cols = ColCount
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        AnchoCampoDescripcion = 5500
        
        .Row = 0
        
        .Col = ColNull
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColCodigoAlterno
        .ColWidth(.Col) = 2500
        .Text = "Art�culo"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesPro
        .ColWidth(.Col) = AnchoCampoDescripcion
        .Text = "Nombre del Producto"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantSolicitada
        .ColWidth(.Col) = 1500
        .Text = "Cant. Sol"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantRecolectada
        .ColWidth(.Col) = 1500
        .Text = "Cant. Rec"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantEmpacada
        .ColWidth(.Col) = 1500
        .Text = "Cant. Emp"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColPesoRec
        .ColWidth(.Col) = 1250
        .Text = "Peso Rec."
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColVolumenRec
        .ColWidth(.Col) = 1250
        .Text = "Vol. Rec."
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodPro
        .ColWidth(.Col) = 0
        .Text = "C�digo Principal"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColDecimales
        .ColWidth(.Col) = 0
        .Text = "Cant. Dec."
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Width = 15000
        
        .ScrollTrack = True
        
        .Col = ColDecimales
        .ColSel = ColCodPro
        
    End With
    
    'For I = 0 To Grid.Cols - 1
        'Grid.Col = I
        'Grid.CellAlignment = flexAlignCenterCenter
    'Next
    
End Sub

Private Sub PrepararDatos()
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset, I As Integer
    
    SQL = "SELECT PP.CodProducto, PRO.c_Descri AS Descri, " & vbNewLine & _
    "PP.CantSolicitada, PP.CantEmpacada, " & vbNewLine & _
    "PP.CantRecolectada, isNULL(COD.c_Codigo, PRO.c_Codigo) AS CodEDI, " & _
    "PRO.Cant_Decimales AS CantDecimales, PRO.n_Peso, PRO.n_Volumen, PRO.n_PesoBul, PRO.n_VolBul " & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & _
    "INNER JOIN MA_PRODUCTOS PRO " & _
    "ON PRO.c_Codigo = PP.CodProducto " & vbNewLine & _
    "LEFT JOIN MA_CODIGOS COD " & _
    "ON COD.c_CodNasa = PRO.c_Codigo " & _
    "AND COD.nu_Intercambio = 1 " & _
    "WHERE PP.CodEmpacador = '" & IIf(FrmPackingUser.PackingMobile, _
    CodEmpacadorMobile, LcCodUsuario) & "' " & _
    "AND PP.CodLote = '" & Lote & "' " & _
    "AND PP.Picking = 1 " & vbNewLine & _
    "AND PP.CodPedido = '" & Pedido & "' " & _
    "AND PP.cs_CodLocalidad_Solicitud = '" & LocalidadSolicitud & "' " & _
    "-- AND PP.CantRecolectada > 0 " & vbNewLine & _
    OrderBy
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    If Rs.EOF Then
        Salir = True
    End If
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    Grid.Visible = False
    
    While Not Rs.EOF
        
        Grid.Rows = Grid.Rows + 1
        Linea = Grid.Rows - 1
        
        Grid.RowData(Linea) = Grid.RowHeight(Linea)
        
        Grid.TextMatrix(Linea, ColNull) = Empty
        
        Grid.TextMatrix(Linea, ColCodigoAlterno) = Rs!CodEDI
        Grid.TextMatrix(Linea, ColDesPro) = Rs!Descri
        
        Grid.TextMatrix(Linea, ColCantSolicitada) = FormatNumber(Rs!CantSolicitada, Rs!CantDecimales)
        Grid.TextMatrix(Linea, ColCantRecolectada) = FormatNumber(Rs!CantRecolectada, Rs!CantDecimales)
        Grid.TextMatrix(Linea, ColCantEmpacada) = FormatNumber(Rs!CantEmpacada, Rs!CantDecimales)
        
        Grid.TextMatrix(Linea, ColPesoRec) = FormatoDecimalesDinamicos( _
        (Rs!CantRecolectada * Rs!n_Peso), 0, 4)
        Grid.TextMatrix(Linea, ColVolumenRec) = FormatoDecimalesDinamicos( _
        (Rs!CantRecolectada * Rs!n_Volumen), 0, 4)
        
        Grid.TextMatrix(Linea, ColCodPro) = Rs!CodProducto
        Grid.TextMatrix(Linea, ColDecimales) = Rs!CantDecimales
        
        If Rs!CantEmpacada > 0 Then
            
            If CDec(Rs!CantRecolectada) = CDec(Rs!CantEmpacada) Then
                For I = 0 To Grid.Cols - 1
                    Grid.Row = Grid.Rows - 1
                    Grid.Col = I
                    Grid.CellBackColor = &HB4EDB9 ' Verde.
                Next I
            Else
                For I = 0 To Grid.Cols - 1
                    Grid.Row = Grid.Rows - 1
                    Grid.Col = I
                    Grid.CellBackColor = -2147483624 ' Amarillo.
                Next I
            End If
            
        End If
        
        Rs.MoveNext
        
    Wend
    
    Rs.Close
    
    If Grid.Rows > 14 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(ColDesPro) = AnchoCampoDescripcion - 675
    Else
        Grid.ScrollBars = flexScrollBarNone
        ScrollGrid.Visible = False
    End If
    
Finally:
    
    Grid.Visible = True
    
    LabelTitulo.Caption = "Detalles de la Solicitud de Transferencia " & Pedido
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub Grid_DblClick()
    
    On Error GoTo Error1
    
    Dim ActiveTrans As Boolean
    
    If BuscarReglaNegocioBoolean("TRA_Consola_DesglosarProductoPadre", 1) Then
        DesglosarProductoPadre = True
    Else
        DesglosarProductoPadre = False
    End If
    
    ProductoNuevo = False
    
    If Grid.TextMatrix(Grid.Row, ColCodPro) <> Empty Then
        
        Dim Resultado As Variant, NewCant As Double
        Dim CantSolicitada As Double, CantRecolectada As Double, _
        CantEmpacada As Double, CantDecimalesProducto As Long
        
        Retorno = True
        
        CantSolicitada = CDec(Grid.TextMatrix(Grid.Row, ColCantSolicitada))
        CantRecolectada = CDec(Grid.TextMatrix(Grid.Row, ColCantRecolectada))
        CantEmpacada = CDec(Grid.TextMatrix(Grid.Row, ColCantEmpacada))
        CantDecimalesProducto = SDec(Grid.TextMatrix(Grid.Row, ColDecimales))
        
        If CantRecolectada = CantEmpacada Then
            If CheckAll Then
                Retorno = True
            Else
                Retorno = Mensaje(False, "Ha empacado la cantidad recolectada " & _
                "para este producto �Seguro que desea cambiarla?")
            End If
        End If
        
        If Retorno Then
            
            If CheckAll Then
                NewCant = CantRecolectada
            Else
                FrmNumPadGeneral.ValorOriginal = CantEmpacada
                FrmNumPadGeneral.Show vbModal
                NewCant = Round(FrmNumPadGeneral.ValorNumerico, CantDecimalesProducto)
                Set FrmNumPadGeneral = Nothing
            End If
            
            If NewCant >= 0 Then
                
                Dim SQL As String, Rs As New ADODB.Recordset
                
                'DesglosarProductoPadre = True ' Debug
                
                If DesglosarProductoPadre Then
                    
                    Dim CodProducto As String
                    
                    Dim cCodPlan As String
                    Dim cCodProExt As String
                    
                    Dim RsProductos As New ADODB.Recordset
                    
                    CodProducto = Grid.TextMatrix(Grid.Row, ColCodPro)
                    
                    Call Apertura_RecordsetC(RsProductos)
                    
                    RsProductos.Open _
                    "SELECT * FROM MA_PRODUCTOS " & _
                    "WHERE c_Codigo = '" & FixTSQL(CodProducto) & "' ", _
                    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                    
                    Set RsProductos.ActiveConnection = Nothing
                    
                    cCodPlan = RsProductos!c_Cod_Plantilla
                    cCodProExt = RsProductos!c_Codigo
                    
                    If Trim(cCodPlan) <> Empty Then
                        
                        If NewCant = 0 Then Exit Sub
                        
                        Dim Query As String
                        Dim RsTemp As ADODB.Recordset
                        
                        Dim Deposito As String
                        
                        Deposito = CodDepositoOrigen
                        
                        RsTemp.Close
                        
                        FrmExtendidasProductos.CodPlantilla = cCodPlan
                        FrmExtendidasProductos.CodProducto = cCodProExt
                        'FrmExtendidasProductos.TipoPrecio = 3
                        FrmExtendidasProductos.CodDeposito = Deposito
                        FrmExtendidasProductos.Forma = "Packing"
                        
                        FrmExtendidasProductos.Show vbModal
                        
                        If Trim(FrmExtendidasProductos.Codigo) <> Empty Then
                            
                            Dim RsTrVentas As New ADODB.Recordset
                            Dim SqlTrVen As String
                            
                            Call Apertura_RecordsetC(RsTrVentas)
                            
                            SqlTrVen = "SELECT * FROM TR_REQUISICIONES " & _
                            "WHERE cs_Documento = '" & Pedido & "' " & _
                            "AND cs_CodLocalidad = '" & LocalidadSolicitud & "' " & _
                            "AND cs_CodArticulo = '" & CodProducto & "' "
                            
                            RsTrVentas.Open SqlTrVen, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                            
                            RsTrVentas.ActiveConnection = Nothing
                            
                            Ent.BDD.BeginTrans
                            ActiveTrans = True
                            
                            Dim RsTrVentasNew As New ADODB.Recordset
                            
                            RsTrVentasNew.Open "SELECT * FROM TR_REQUISICIONES WHERE 1 = 2 ", _
                            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                            
                            RsTrVentasNew.AddNew
                            
                            RsTrVentasNew!cs_Documento = RsTrVentas!cs_Documento
                            RsTrVentasNew!cs_Estado = RsTrVentas!cs_Estado
                            RsTrVentasNew!ns_Linea = BuscarValorBD("Linea", _
                            "SELECT (isNULL(MAX(ns_Linea), 0) + 1) AS Linea " & _
                            "FROM TR_REQUISICIONES " & _
                            "WHERE cs_Documento = '" & RsTrVentas!cs_Documento & "' ", 1, Ent.BDD)
                            RsTrVentasNew!cs_CodArticulo = FrmExtendidasProductos.Codigo
                            RsTrVentasNew!cs_CodEdi = RsTrVentasNew!cs_CodArticulo
                            RsTrVentasNew!ns_Cantidad = NewCant
                            'RsTrVentasNew!ns_Cant_Recibida = NewCant
                            RsTrVentasNew!ns_Decimales = RsTrVentas!ns_Decimales
                            RsTrVentasNew!cs_CodMoneda = RsTrVentas!cs_CodMoneda
                            RsTrVentasNew!ns_Factor_Cambio = RsTrVentas!ns_Factor_Cambio
                            RsTrVentasNew!ns_Costo = RsTrVentas!ns_Costo
                            RsTrVentasNew!cs_Descripcion = RsTrVentas!cs_Descripcion
                            RsTrVentasNew!ns_Prod_Ext = RsTrVentas!ns_Prod_Ext
                            RsTrVentasNew!ns_CantidadEmpaque = RsTrVentas!ns_CantidadEmpaque
                            RsTrVentasNew!cs_CodLocalidad = RsTrVentas!cs_CodLocalidad
                            
                            RsTrVentasNew.UpdateBatch
                            
                            RsTrVentasNew.Close
                            
                            Dim RsPedRutaPick As New ADODB.Recordset
                            Dim Sql2 As String
                            
                            Sql2 = "SELECT * FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & vbNewLine & _
                            "WHERE CodLote = '" & Lote & "' " & _
                            "AND CodPedido = '" & Pedido & "' " & _
                            "AND cs_CodLocalidad_Solicitud = '" & LocalidadSolicitud & "' " & _
                            "AND CodProducto = '" & FixTSQL(Grid.TextMatrix(Grid.Row, ColCodPro)) & "' "
                            
                            Call Apertura_RecordsetC(RsPedRutaPick)
                            
                            RsPedRutaPick.Open Sql2, Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
                            
                            RsPedRutaPick.ActiveConnection = Nothing
                            
                            Dim RsPedRutaPickNew As New ADODB.Recordset
                            
                            RsPedRutaPickNew.Open _
                            "SELECT * FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING WHERE 1 = 2 ", _
                            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                            
                            RsPedRutaPickNew.AddNew
                            
                            RsPedRutaPickNew!CodLote = RsPedRutaPick!CodLote
                            RsPedRutaPickNew!CodPedido = RsPedRutaPick!CodPedido
                            RsPedRutaPickNew!CodProducto = FrmExtendidasProductos.Codigo
                            RsPedRutaPickNew!CodRecolector = RsPedRutaPick!CodRecolector
                            RsPedRutaPickNew!CodSupervisorPicking = RsPedRutaPick!CodSupervisorPicking
                            RsPedRutaPickNew!CodEmpacador = RsPedRutaPick!CodEmpacador
                            RsPedRutaPickNew!CodSupervisorPacking = RsPedRutaPick!CodSupervisorPacking
                            RsPedRutaPickNew!CantSolicitada = NewCant
                            RsPedRutaPickNew!CantRecolectada = NewCant
                            RsPedRutaPickNew!CantEmpacada = NewCant
                            RsPedRutaPickNew!Picking = RsPedRutaPick!Picking
                            RsPedRutaPickNew!Packing = RsPedRutaPick!Packing
                            RsPedRutaPickNew!FechaAsignacion = RsPedRutaPick!FechaAsignacion
                            RsPedRutaPickNew!PackingMobile_CantEmpaques = RsPedRutaPick!PackingMobile_CantEmpaques
                            RsPedRutaPickNew!cs_CodLocalidad_Solicitud = RsPedRutaPick!cs_CodLocalidad_Solicitud
                            RsPedRutaPickNew!CostoLinea = RsPedRutaPick!CostoLinea
                            
                            RsPedRutaPickNew.UpdateBatch
                            RsPedRutaPickNew.Close
                            
                            ProductoNuevo = True
                            
                        Else
                            
                            Exit Sub
                            
                        End If
                        
                    Else
                        
                        GoTo ProductoNormal1
                        
                    End If
                    
                Else
                    
ProductoNormal1:
                    
                    SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                    "CantEmpacada = " & CDec(NewCant) & " " & vbNewLine & _
                    "WHERE CodLote = '" & Lote & "' " & _
                    "AND CodPedido = '" & Pedido & "' " & _
                    "AND cs_CodLocalidad_Solicitud = '" & LocalidadSolicitud & "' " & _
                    "AND CodProducto = '" & Grid.TextMatrix(Grid.Row, ColCodPro) & "' "
                    
                End If
                
                If CantEmpacada > NewCant Then
                    
                    Retorno = True
                    
                    If Not CheckAll Then
                        Retorno = Mensaje(False, "La cantidad actual de productos empacados es de " & _
                        CantEmpacada & " �Seguro que desea disminuirla a " & _
                        FormatoDecimalesDinamicos(NewCant) & "?")
                    End If
                    
                    If Retorno Then
                        
                        If Not ProductoNuevo Then
                            Ent.BDD.Execute SQL
                        Else
                            Ent.BDD.CommitTrans
                            ActiveTrans = False
                        End If
                        
                        CambioCantidad = True
                        
                        If Not CheckAll Then
                            Call PrepararGrid
                            Call PrepararDatos
                        End If
                        
                    End If
                    
                ElseIf CantRecolectada = NewCant Then
                    
                    If Not ProductoNuevo Then
                        Ent.BDD.Execute SQL
                    Else
                        Ent.BDD.CommitTrans
                        ActiveTrans = False
                    End If
                    
                    CambioCantidad = True
                    
                    If Not CheckAll Then
                        Call PrepararGrid
                        Call PrepararDatos
                    End If
                    
                ElseIf CantEmpacada < NewCant Then
                    
                    If NewCant > CantRecolectada Then
                        Mensaje True, "La cantidad empacada no puede ser mayor a la cantidad recolectada."
                    Else
                        
                        If Not ProductoNuevo Then
                            Ent.BDD.Execute SQL
                        Else
                            Ent.BDD.CommitTrans
                            ActiveTrans = False
                        End If
                        
                        CambioCantidad = True
                        
                        If Not CheckAll Then
                            Call PrepararGrid
                            Call PrepararDatos
                        End If
                        
                    End If
                    
                End If
                
            End If
            
        End If
        
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If ActiveTrans Then
        Ent.BDD.RollbackTrans
        ActiveTrans = False
    End If
    
    If Not CheckAll Then
        
        Mensaje True, "Error al actualizar cantidad empacada, " & _
        "Informaci�n: " & mErrorDesc & " (" & mErrorNumber & ")."
        
        Call PrepararGrid
        Call PrepararDatos
        
    End If
    
End Sub

Private Sub FramePickAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    CmdPickAll_MouseUp Button, Shift, X, Y
End Sub

Private Sub CmdPickAll_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_KeyDown vbKeyF6, 0
End Sub

Private Function AnularPedido(pCn As ADODB.Connection, _
ByVal pLote As String, ByVal pPedido As String, _
ByVal pLocalidadSolicitud As String, _
Optional ByVal pTransInterna As Boolean = True) As Boolean
    
    On Error GoTo Error
    
    Dim SQL As String
    Dim Rs As ADODB.Recordset, RowsAffected
    
    Dim Deposito As String
    Dim Localidad As String
    Dim CodCliente As String
    
    If pTransInterna Then
        pCn.BeginTrans
        Trans = True
    End If
    
    ' Finalizar el Packing del pedido.
    
    If Not MarcarPedido(pCn, pLote, pPedido, pLocalidadSolicitud, False) Then
        pCn.RollbackTrans
        Trans = False
        Exit Function
    End If
    
    ' NOTA: Ahora con el nuevo desarrollo de Dividir Lotes, ser� posible que un mismo
    ' Pedido exista en 2 lotes. En caso de ser asi, solo podremos anular el pedido,
    ' Siempre y cuando este mismo pedido no este presente en otro lote no cerrado.
    ' Agregar validaci�n nueva ac�.
    
    ' Actualizacion: Se deja de llamar este metodo. Ya no es valido en estas circunstancias
    ' anular solicitudes.
    
    'Colocar el pedido con status = ANU
    
    SQL = "UPDATE MA_REQUISICIONES WITH (ROWLOCK) SET " & _
    "cs_Estado = 'ANU' " & _
    "WHERE cs_Documento = '" & pPedido & "' " & _
    "AND cs_CodLocalidad = '" & pLocalidadSolicitud & "' "
    
    pCn.Execute SQL, RowsAffected
    
    If pTransInterna Then
        pCn.CommitTrans
        Trans = False
    End If
    
    AnularPedido = True
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If pTransInterna Then
        If Trans Then
            pCn.RollbackTrans
            Trans = False
        End If
    End If
    
    MsjErrorRapido mErrorDesc & " (" & mErrorNumber & ")", _
    "Ocurri� un error al anular la solicitud, reporte lo siguiente: "
    
End Function

Private Function MarcarPedido(pCn As ADODB.Connection, _
ByVal pLote As String, ByVal pPedido As String, _
ByVal pLocalidadSolicitud As String, _
Optional ByVal pTransInterna As Boolean = True) As Boolean
    
    On Error GoTo Error
    
    Dim SQL As String
    Dim Rs As ADODB.Recordset, RowsAffected
    
    If pTransInterna Then
        pCn.BeginTrans
        Trans = True
    End If
    
    ' Finalizar el Packing del pedido.
    
    SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING WITH (ROWLOCK) SET " & _
    "Packing = 1 " & vbNewLine & _
    "WHERE CodLote = '" & pLote & "' " & _
    "AND CodPedido = '" & pPedido & "' " & _
    "AND cs_CodLocalidad_Solicitud = '" & pLocalidadSolicitud & "' "
    
    pCn.Execute SQL, RowsAffected
    
    If pTransInterna Then
        pCn.CommitTrans
        Trans = False
    End If
    
    MarcarPedido = True
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If pTransInterna Then
        If Trans Then
            pCn.RollbackTrans
            Trans = False
        End If
    End If
    
    MsjErrorRapido mErrorDesc & " (" & mErrorNumber & ")", _
    "Ocurri� un error al marcar la solicitud como procesada, reporte lo siguiente: "
    
End Function

Private Function PrevenirDuplicados(pCn As Object) As Boolean
    
    Dim SQL As String, ExisteDoc As Boolean, ExistePacking As Boolean
    
    SQL = "SELECT TOP 1 c_Documento " & vbNewLine & _
    "FROM MA_PICKING_TRANSFERENCIA " & vbNewLine & _
    "WHERE 1 = 1 " & vbNewLine & _
    "AND c_Relacion = 'SOL_TRA " & Pedido & "' " & vbNewLine
    
    ExisteDoc = (UCase(BuscarValorBD("c_Documento", SQL, "NULL", pCn)) <> UCase("NULL"))
    
    SQL = "SELECT TOP 1 CodPedido " & vbNewLine & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA_PACKING " & vbNewLine & _
    "WHERE CodLote = '" & Lote & "' " & vbNewLine & _
    "AND CodPedido = '" & Pedido & "' " & vbNewLine & _
    "AND cs_CodLocalidad_Solicitud = '" & LocalidadSolicitud & "' " & vbNewLine
    
    ExistePacking = (UCase(BuscarValorBD("CodPedido", SQL, "NULL", pCn)) <> UCase("NULL"))
    
    PrevenirDuplicados = (ExisteDoc And ExistePacking)
    
    ' Si las 2 condiciones se cumplen entonces hay que _
    evitar grabar pues se crear�a un duplicado.
    ' NOTA: Ahora con el nuevo desarrollo de Dividir Lotes, ser� posible que un mismo
    ' Pedido exista en 2 lotes. En caso de ser asi, ExisteDoc Podria ser True, pero
    ' ExistePacking se refiere a que si el pedido registro ya esta hecho para este lote
    ' Asi que no importa que el pedido ya este empacado siempre y cuando sea en otro lote.
    
End Function

' Empacar Pedido

Private Sub Cmd_Listo_Click()
    
    On Error GoTo Error1
    
    Dim Trans As Boolean, SumCantRec As Variant, SumCantEmp As Variant, Anular As Boolean
    Dim I As Integer, Resultado As Variant, CantEmpaques As Long
    
    Dim mCampoMonedaProd As String, mFactorMonedaProd As Variant, mCodMonedaProd As String
    Dim CostoUsar As Double, PrecioUsar As Double, TmpPrecioDoc As Double
    
    Dim mMantenerMonedaOriginal As Boolean
    Dim mMantenerTasaOrigen As Boolean
    
    'aca buscar la regla de negocio para mantener el documento en moneda original
    mMantenerMonedaOriginal = False 'BuscarReglaNegocioBoolean("Packing_MantenerMonedaOrigen", 1)
    mMantenerTasaOrigen = False 'BuscarReglaNegocioBoolean("Packing_MantenerTasaOrigen", 1)
    
    If PrevenirDuplicados(Ent.BDD) Then
        Mensaje True, "Atenci�n, esta solicitud ya ha sido empacada, probablemente " & _
        "desde otro equipo. Verifique con los otros operadores por que ya la Nota " & _
        "de Empacado ha sido generada."
        Exit Sub
    End If
    
    Resultado = False
    
    'Verificar Que todos los articulos recolectados sean iguales a los empacados.
    
    SumCantRec = CDec(0)
    SumCantEmp = CDec(0)
    
    For I = 1 To Grid.Rows - 1
        
        If CDbl(Grid.TextMatrix(I, ColCantRecolectada)) _
        <> CDbl(Grid.TextMatrix(I, ColCantEmpacada)) Then
            Resultado = True
        End If
        
        SumCantRec = SumCantRec + CDec(Grid.TextMatrix(I, ColCantRecolectada))
        SumCantEmp = SumCantEmp + CDec(Grid.TextMatrix(I, ColCantEmpacada))
        
    Next I
    
    If Resultado _
    And IIf(FrmPackingUser.PackingMobile, CambioCantidad, True) Then
        Resultado = Mensaje(False, _
        "�Est� Seguro que desea finalizar el empacado con productos faltantes?")
    Else
        Resultado = True
    End If
    
    If Not Resultado Then
        Exit Sub
    End If
    
    If SumCantRec = 0 Or SumCantEmp = 0 Then
        
        'Mensaje False, "No se ha establecido ninguna cantidad para los productos, " & _
        "en ese caso el pedido ser� anulado, dicha acci�n no se podr� reversar. " & _
        "�Est� realmente seguro de anular el pedido? "
        
        ' Ya no anulamos. Leer comentario de referencia mas abajo.
        
        Mensaje False, "No se ha establecido ninguna cantidad para los productos, " & _
        "en ese caso el pedido ser� marcado como procesado y faltante. " & _
        "�Est� seguro de proceder de esta forma? "
        
        If Not Retorno Then
            Exit Sub
        End If
        
        Anular = True
        
    End If
    
    Dim Rs As New ADODB.Recordset, SQL As String, _
    Deposito As String, LocalidadDestino As String, _
    LocalidadOrigen As String, LocalidadDeDondeViene As String
    
    If Anular Then
        
        ' Ya no anularemos, por que las solicitudes pueden ser divididas en varios lotes
        ' Y pueden ser procesadas "Estilo BackOrder" por medio de la funcionalidad de
        ' Transformar Lotes. Entonces no podemos anular un documento que puede estar en
        ' Proceso en otro lote.
        
        ' Incluso, el documento puede pertenecer a un solo lote, pero igual no lo podemos
        ' anular desde ac� ya que con la funcionalidad de reversar una fase, ya podemos
        ' reversar todo lo que se esta haciendo en esta ventana (Empacado, Constancia de Empaque)
        ' y no tiene sentido anular una solicitud que puede ser reempacada.
        
        ' Esto significa que ya no hay anulaci�n en este punto. Debemos tratar las solicitudes
        ' Como que son documentos backOrder hasta que ya no quede ninguna parte de ellas en
        ' ning�n lote abierto (Solo entonces en teor�a podriamos anular sin riesgos de integridad).
        
        'If AnularPedido(Ent.BDD, Lote, Pedido, LocalidadSolicitud) Then
        If MarcarPedido(Ent.BDD, Lote, Pedido, LocalidadSolicitud) Then
            Unload Me
        End If
        
        Exit Sub
        
    End If
    
    If Resultado Then
        
        If Not FrmTransferencia_PackingUser.PackingMobile Then
            Mensaje True, "Ingrese la cantidad de empaques usados para este pedido."
            Retorno = False
        Else
            Mensaje False, "El empacador indic� " & EmpaquesMobile & " empaques para este pedido. " & _
            "Si es correcto presione aceptar, o cancelar para cambiar la cantidad de empaques."
        End If
        
        If Not Retorno Then
            
            FrmNumPadGeneral.ValorOriginal = 1
            FrmNumPadGeneral.Show vbModal
            CantEmpaques = FrmNumPadGeneral.ValorNumerico
            Set FrmNumPadGeneral = Nothing
            
        Else
            CantEmpaques = EmpaquesMobile
        End If
        
        Retorno = False
        
        If CantEmpaques > 0 Then
            
            Uno = False
            
            FrmMensajeAlerta.Mensaje.Text = _
            "La cantidad de empaques indicada para este " & _
            "pedido es: " & FormatoDecimalesDinamicos(CantEmpaques) & ", " & _
            "por favor confirme la finalizaci�n " & _
            "del proceso de empacado para este pedido."
            
            FrmMensajeAlerta.Show vbModal
            
            frm_Mensajeria.aceptar.Enabled = False
            
            Set frm_Mensajeria = Nothing
            
        End If
        
        If Retorno Then
            
            Dim Cont As Long
            
            CampoC = Ent.BDD.Execute( _
            "SELECT Estimacion_INV " & _
            "FROM REGLAS_COMERCIALES")!Estimacion_Inv
            
            Select Case CampoC
                Case 0
TipoCostoDefault:
                    CampoC = "n_CostoAct"
                Case 1
                    CampoC = "n_CostoAnt"
                Case 2
                    CampoC = "n_CostoPro"
                'Case 3
                    'CampoC = "n_CostoRep"
                Case Else
                    GoTo TipoCostoDefault
            End Select
            
            Ent.BDD.BeginTrans
            Trans = True
            
            SQL = "SELECT cs_Deposito_Despachar AS Localidad, cs_CodLocalidad, " & _
            "cs_CodMoneda, ns_Factor_Cambio " & _
            "FROM MA_REQUISICIONES WITH (NOLOCK) " & _
            "WHERE cs_Documento = '" & Pedido & "' " & _
            "AND cs_CodLocalidad = '" & LocalidadSolicitud & "' "
            
            Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
            
            Deposito = CodDepositoOrigen
            LocalidadDestino = Rs!Localidad
            CodMonedaDoc = Rs!cs_CodMoneda
            
            'posiblemente buscar aca el factor de la cabecera
            Set MonedaTmp = FrmAppLink.ClaseMonedaDocumento
            
            If Not MonedaTmp.BuscarMonedas(, CodMonedaDoc) Then
                FrmAppLink.ValidarConexion Ent.BDD, , True
                If Not MonedaTmp.BuscarMonedas(, CodMonedaDoc) Then
                    Err.Raise 999, , "No se pudo cargar datos de la moneda del documento. "
                End If
            End If
            
            Dim FacMonedaDoc As Double
            Dim DecMonedaDoc As Integer
            
            If mMantenerMonedaOriginal Then
                If Not mMantenerTasaOrigen Then
                    FacMonedaDoc = MonedaTmp.FacMoneda
                Else
                    FacMonedaDoc = Rs!ns_Factor_Cambio
                End If
                
                DecMonedaDoc = MonedaTmp.DecMoneda
            Else
                FacMonedaDoc = 1
                DecMonedaDoc = FrmAppLink.DecMonedaPref
            End If
            
            Rs.Close
            
            SQL = "DELETE FROM MA_LOTE_GESTION_TRANSFERENCIA_PACKING " & vbNewLine & _
            "WHERE CodLote = '" & Lote & "' " & _
            "AND CodPedido = '" & Pedido & "' " & _
            "AND cs_CodLocalidad_Solicitud = '" & LocalidadSolicitud & "' " & vbNewLine
            
            Ent.BDD.Execute SQL, RowsAffected
            
            ''Insertar el Nuevo Pedido al Packing. Despues de emitir la Nota
            
            'SQL = "INSERT MA_LOTE_GESTION_TRANSFERENCIA_PACKING " & _
            "(CodLote, CodPedido, CantEmpaques) " & GetLines & _
            "VALUES ('" & Lote & "', '" & Pedido & "', (" & CantEmpaques & ")) "
            
            'Ent.BDD.Execute SQL, RowsAffected
            
            ' Finalizar el Packing del pedido.
            
            SQL = "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING WITH (ROWLOCK) SET " & _
            "Packing = 1 " & vbNewLine & _
            "WHERE CodLote = '" & Lote & "' " & _
            "AND CodPedido = '" & Pedido & "' " & _
            "AND cs_CodLocalidad_Solicitud = '" & LocalidadSolicitud & "' "
            
            Ent.BDD.Execute SQL, RowsAffected
            
            DocumentoNDT = Format(NO_CONSECUTIVO("Packing_Transferencia", True), "00000000#")
            
            Cont = 1 ' Control del campo C_Linea
            
            SQL = "SELECT * " & _
            "FROM TR_REQUISICIONES WITH (NOLOCK) " & _
            "WHERE cs_Documento = '" & Pedido & "' " & _
            "AND cs_CodLocalidad = '" & LocalidadSolicitud & "' " & _
            "ORDER BY ns_Linea "
            
            Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
            
            Dim RsProducto As Recordset
            
            Faltantes = False 'Bandera usada para saber La cantidad de productos empacados es diferente a los solicitados.
            
            While Not Rs.EOF
                
                For I = 1 To Grid.Rows - 1
                    
                    If UCase(Grid.TextMatrix(I, ColCodPro)) = UCase(Rs!cs_CodArticulo) Then
                        
                        Set RsProducto = New Recordset
                        
                        RsProducto.Open _
                        "SELECT * FROM MA_PRODUCTOS " & _
                        "WHERE c_Codigo = '" & Rs!cs_CodArticulo & "' ", _
                        Ent.BDD, adOpenKeyset, adLockReadOnly, adCmdText
                        
                        If RsProducto.EOF Then
                            Err.Raise 999, , "El Producto [" & Rs!cs_CodArticulo & "] no existe."
                        End If
                        
                        mCodMonedaProd = RsProducto!c_CodMoneda
                        
                        Set MonedaProd = FrmAppLink.ClaseMonedaProducto
                        
                        If Not MonedaProd.BuscarMonedas(, mCodMonedaProd) Then
                            FrmAppLink.ValidarConexion Ent.BDD, , True
                            If Not MonedaProd.BuscarMonedas(, mCodMonedaProd) Then
                                Err.Raise 999, , "No se pudo cargar datos de la moneda para el producto " & Rs!cs_CodArticulo & ""
                            End If
                        End If
                        
                        mFactorMonedaProd = MonedaProd.FacMoneda 'FactorMonedaProducto(RsProducto!c_Codigo, Ent.BDD) 'Mejor no, Existe el riesgo de mezclar datos de ficha de VAD10 con VAD20 en la transacci�n.
                        
                        CostoUsar = RsProducto.Fields(CampoC).Value
                        
                        If mMantenerMonedaOriginal Then
                            CostoUsar = Round(CostoUsar * (mFactorMonedaProd / FacMonedaDoc), DecMonedaDoc) 'FrmAppLink.DecMonedaPref)    'CMM1N
                        Else
                            'CostoUsar = Round(CostoUsar * mFactorMonedaProd, DecMonedaDoc) 'CMM1N
                            CostoUsar = Rs!ns_Costo
                        End If
                        
                        Dim Cantidad As Variant, Subtotal As Double
                        
                        Cantidad = CDec(Grid.TextMatrix(I, ColCantEmpacada)) ' Cantidad Restante.
                        
                        If Cantidad >= CDec(Rs!ns_Cantidad) Then ' Si hay suficiente, se toma la del pedido.
                            
                            Grid.TextMatrix(I, ColCantEmpacada) = Round((Cantidad - CDec(Rs!ns_Cantidad)), _
                            RsProducto!Cant_Decimales)
                            
                            Cantidad = Rs!ns_Cantidad
                            Resultado = 0
                            
                        Else ' En caso contrario, se da la restante...
                            
                            Faltantes = True
                            
                            Resultado = Round((CDec(Rs!ns_Cantidad) - Cantidad), _
                            RsProducto!Cant_Decimales)
                            
                        End If
                        
                        ' Calcular con la informaci�n del Pedido y no de la
                        ' Ficha del producto, donde la informaci�n var�a.
                        
                        Subtotal = (Cantidad * CostoUsar)
                        
                        If Cantidad > 0 Then
                            
                            If TR_INV_n_FactorMonedaProd Then
                                mCampoMonedaProd = ", n_FactorMonedaProd"
                                mFactorMonedaProd = ", " & mFactorMonedaProd & ""
                            Else
                                mCampoMonedaProd = Empty
                                mFactorMonedaProd = Empty
                            End If
                            
                            'SQL = "INSERT INTO TR_INVENTARIO " & _
                            "(c_Linea, c_Concepto, c_Documento, c_Deposito, c_CodArticulo, " & _
                            "n_Cantidad, n_Costo, n_Subtotal, n_Impuesto, n_Total, " & _
                            "c_TipoMov, n_Cant_Teorica, n_Cant_Diferencia, n_Precio, " & _
                            "n_Precio_Original, f_Fecha, c_CodLocalidad, n_FactorCambio, " & _
                            "c_Descripcion, c_Compuesto, CodConcepto, n_DescuentoGeneral, " & _
                            "n_DescuentoEspecifico, c_Documento_Origen, c_TipoDoc_Origen, " & _
                            "n_CantidadFac, ns_Descuento, cs_ComprobanteContable, cs_CodLocalidad, " & _
                            "ns_CantidadEmpaque, Impuesto" & mCampoMonedaProd & ") " & vbNewLine & _
                            "VALUES ( " & _
                            "" & Cont & ", '" & ConceptoPacking & "', '" & DocumentoNDT & "', '" & Deposito & "', " & _
                            "'" & Rs!cs_CodArticulo & "', " & Cantidad & ", " & CostoUsar & ", " & _
                            "" & Subtotal & ", 0, " & Subtotal & ", 'Descargo', " & _
                            "" & Cantidad & ", 0, " & CostoUsar & ", " & CostoUsar & ", " & _
                            "CAST(GETDATE() AS DATE), '" & CodLocalidadOrigen & "', " & FacMonedaDoc & ", " & _
                            "'" & Rs!cs_Descripcion & "', '', 0, " & 0 & ", " & 0 & ", '" & Pedido & "', " & _
                            "'SOL_TRA', 0, 0, '', '" & CodLocalidadOrigen & "', " & Rs!ns_CantidadEmpaque & ", " & _
                            "0" & mFactorMonedaProd & ") "
                            
                            SQL = "INSERT INTO TR_PACKING_TRANSFERENCIA " & _
                            "([c_Documento], [n_Linea], [c_CodArticulo], [c_Descripcion], " & _
                            "[n_Cantidad], [n_Costo], [ns_CantidadEmpaque], [cs_CodLocalidad]) " & _
                            "SELECT " & _
                            "'" & DocumentoNDT & "', " & CDec(Cont) & ", '" & Rs!cs_CodArticulo & "', " & _
                            "'" & Rs!cs_Descripcion & "', " & CDec(Cantidad) & ", " & CDec(CostoUsar) & ", " & _
                            "" & CDec(Rs!ns_CantidadEmpaque) & ", '" & CodLocalidadOrigen & "' "
                            
                            Ent.BDD.Execute SQL, RowsAffected
                            
                        End If
                        
                        Exit For
                        
                    End If
                    
                Next I
                
                Cont = Cont + 1
                Rs.MoveNext
                
            Wend
            
            Rs.Close
            
Totalizar:
            
            SQL = "SELECT COUNT(*) AS CantProd, SUM(n_Cantidad * n_Costo) AS Base " & _
            "FROM TR_PACKING_TRANSFERENCIA WITH (NOLOCK) " & _
            "WHERE c_Documento = '" & DocumentoNDT & "' " & _
            "AND cs_CodLocalidad = '" & CodLocalidadOrigen & "' "
            
            Rs.Open SQL, Ent.BDD, adOpenKeyset, adLockReadOnly, adCmdText
            
            If Rs.EOF Then
                
                Ent.BDD.RollbackTrans
                Trans = False
                
                'If AnularPedido(Ent.BDD, Lote, Pedido, LocalidadSolicitud) Then
                If MarcarPedido(Ent.BDD, Lote, Pedido, LocalidadSolicitud) Then
                    Unload Me
                End If
                
                Exit Sub
                
            End If
            
            Subtotal = 0
            
            While Not Rs.EOF
                
                Subtotal = (Subtotal + Rs!Base)
                
                Rs.MoveNext
                
            Wend
            
            Rs.Close
            
            If PrevenirDuplicados(Ent.BDD) Then
                
                Mensaje True, "Atenci�n, esta solicitud ya ha sido empacada, probablemente " & _
                "desde otro equipo. Verifique con los otros operadores por que ya la nota " & _
                "de Empacado ha sido generada."
                
                Ent.BDD.RollbackTrans
                Trans = False
                
                Exit Sub
                
            End If
            
            Dim CodDepositoDestino As String
            
            CodDepositoDestino = CodDepositoTransito '"01TS01"
            
            SQL = "INSERT INTO MA_PACKING_TRANSFERENCIA " & _
            "([c_Documento], [d_Fecha], [c_Status], [c_CodLocalidad_Destino], " & _
            "[c_CodDeposito_Destino], [cs_Observacion], [c_Relacion], [cs_CodLocalidad_Relacion], " & _
            "[c_Usuario], [n_CantEmpaques], [n_Subtotal], [c_CodMoneda], [n_FactorCambio], " & _
            "[cs_CodLocalidad], [cs_Numero_Transferencia], [cs_NumTransf_Destino]) " & _
            "SELECT " & _
            "'" & DocumentoNDT & "', GETDATE(), 'DPE', '" & LocalidadDestino & "', " & _
            "'" & CodDepositoDestino & "', 'Desde Consola de Gestion de Transferencia, " & _
            "Solicitud [" & Pedido & "] ', 'SOL_TRA " & Pedido & "', '" & LocalidadSolicitud & "', " & _
            "'" & FrmAppLink.GetCodUsuario & "', " & CDec(CantEmpaques) & ", " & _
            "" & CDec(Subtotal) & ", '" & CodMonedaDoc & "', " & CDec(FacMonedaDoc) & ", " & _
            "'" & FrmAppLink.GetCodLocalidadSistema & "', '', '' "
            
            Ent.BDD.Execute SQL, RowsAffected
            
            'If Faltantes Then 'Si es true eso quiere decir que el pedido NO fue totalmente completado c_status = DPE
                
                'Colocar el pedido con status = DPE
                'SQL = "UPDATE MA_VENTAS WITH (ROWLOCK) SET c_Status = 'DPE' WHERE c_Documento = '" & Pedido & "' AND c_Concepto = 'PED'"
                'Ent.BDD.Execute SQL
                
            'Else 'Si es false eso quiere decir que el pedido fue totalmente completado c_status = DCO
                
                'Finalizar el pedido, status = DCO
                
                'SQL = "UPDATE MA_REQUISICIONES WITH (ROWLOCK) " & _
                "SET cs_Estado = 'DCO' " & _
                "WHERE cs_Documento = '" & Pedido & "' "
                
                'Ent.BDD.Execute SQL
                
                ' Mejor para estos pedidos de Picking & Packing, colocar siempre DCO.
                ' Sin embargo, si hubo faltantes, hay que liberar la cantidad restante de la
                ' cantidad comprometida al instante. De esta manera esas cantidades ya no quedan
                ' en el aire, y no hay que preocuparse de que el cliente (la organizaci�n)
                ' tenga que anular el pedido manualmente o que al hacer la facturaci�n masiva
                ' del lote haya que liberar las cantidades en ese momento.
                
                'If Faltantes Then
                    'RestaurarComprometidos Ent.BDD, Pedido, Deposito
                'End If
                
            'End If
            
            'Insertar el Nuevo Pedido al Packing
            
            SQL = "INSERT MA_LOTE_GESTION_TRANSFERENCIA_PACKING " & _
            "(CodLote, CodPedido, cs_CodLocalidad_Solicitud, CantEmpaques, DocPacking) " & GetLines & _
            "VALUES ('" & Lote & "', '" & Pedido & "', '" & LocalidadSolicitud & "', " & _
            "(" & CantEmpaques & "), '" & DocumentoNDT & "') "
            
            Ent.BDD.Execute SQL, RowsAffected
            
            Ent.BDD.CommitTrans
            Trans = False
            
            Call ImprimirNotaDeEntrega(CantEmpaques)
            
            Unload Me
            
            Exit Sub
            
        End If
        
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If Trans Then
        Ent.BDD.RollbackTrans
        Trans = False
    End If
    
    If mErrorNumber <> 0 Then
        Mensaje True, "Error al finalizar el empaque del pedido, " & _
        "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    End If
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Public Sub ImprimirNotaDeEntrega(ByVal CantEmpaques As Long, _
Optional ByVal pShowRTF As Boolean = False)
    If CantEmpaques = 1 Then
        Call LlenarRTF(1, CantEmpaques, pShowRTF)
        'Call RPT_DOC_VENTAS(lcConsecu, Find_Concept, Find_Status, IIf(mImpres = 0, 5, Index), TipoImpresion)
    Else
        Dim I As Long
        For I = 1 To CantEmpaques
            Call LlenarRTF(I, CantEmpaques, pShowRTF)
        Next I
    End If
End Sub

Private Sub LlenarRTF(NumTique As Long, TotalTique As Long, _
Optional ByVal pShowRTF As Boolean = False)
    
    On Error GoTo Error1
    
    Dim Rs As New ADODB.Recordset, Rtf As recsun.Obj_rtf, SQL As String
    
    Dim H101 As String, H102 As String, H1017(3) As String, H1001 As String, _
    H1002(6) As String, H1003 As String, H1018 As String, H1015 As String, _
    H1005 As String, H1005P As String, H1006(1) As String, H1007 As String, _
    H1008 As String, H1009 As String, H1004 As String, H1016 As String, _
    H1019 As String, H1020(6) As String, H1021 As String, H1022 As String, H1023 As String, _
    H1010 As String, H1011 As String, H1012 As String, H1013 As String, H1014 As String
    
    Dim mTotalPeso As Double, mTotalVolumen As Double, mTotalArticulos As Double, _
    mTotalEmpaquesTransaccion As Double, mTotalUnidadesTransaccion As Double, _
    mTotalEmpaquesFicha As Double, mTotalUnidadesFicha As Double

    Dim mDetalles As Collection, mItem As Variant ' Dictionary
    
    'Cantidad de Empaques y fecha
    
    SQL = "SELECT CantEmpaques, FechaEmpacado " & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA_PACKING " & _
    "WHERE CodPedido = '" & Pedido & "' " & _
    "AND cs_CodLocalidad_Solicitud = '" & LocalidadSolicitud & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    H101 = CStr(NumTique)
    H102 = CStr(TotalTique)
    H1017(0) = SDate(Rs!FechaEmpacado)
    H1017(1) = GDate(Rs!FechaEmpacado)
    H1017(2) = GTime(Rs!FechaEmpacado)
    H1017(3) = STime(Rs!FechaEmpacado)
    
    Rs.Close
    
    'Datos de la Empresa
    
    SQL = "SELECT Nom_Org AS Nombre, Dir_Org AS Direccion, " & _
    "Tlf_Org AS Telefono, Rif_Org AS RIF " & _
    "FROM ESTRUC_SIS"
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    H1001 = Trim(Rs!Nombre)
    H1002(0) = Trim(Rs!Direccion)
    
    mLongitud = SVal(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "50"))
    
    If mLongitud = 0 Then mLongitud = 50
    
    mCadenaAux = Rs!Direccion & " "
    
    mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, mCadenaAux, False), vbNewLine)
    
    For I = 1 To UBound(H1002)
        
        If (I - 1) <= UBound(mCadena) Then
            H1002(I) = CStr(mCadena((I - 1)))
        Else
            H1002(I) = Empty
        End If
        
    Next I
    
    H1003 = Trim(Rs!Telefono)
    H1018 = Trim(Rs!Rif)
    
    Rs.Close
    
    'Nombre de Usuario
    
    SQL = "SELECT USR.Descripcion AS Nombre " & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & _
    "INNER JOIN MA_USUARIOS USR " & vbNewLine & _
    "ON PP.CodEmpacador = USR.CodUsuario " & vbNewLine & _
    "WHERE PP.CodPedido =  '" & Pedido & "' " & _
    "AND PP.cs_CodLocalidad_Solicitud = '" & LocalidadSolicitud & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    H1015 = Trim(Rs!Nombre)
    
    Rs.Close
    
    'C�digo de Documento
    
    H1005 = Trim(DocumentoNDT) ' Constancia de Empaque
    H1005P = Trim(Pedido)
    
    'Datos de la NDT
    
    Dim Relacion As String, CodLocalidadDestino As String
    
    SQL = "SELECT PACK.n_Subtotal AS mSubtotal, * " & vbNewLine & _
    "FROM MA_PACKING_TRANSFERENCIA PACK " & _
    "WHERE PACK.c_Documento = '" & DocumentoNDT & "' " & _
    "AND PACK.cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    H1007 = FormatNumber(Rs!mSubtotal, 2)
    
    CodLocalidadOrigen = Rs!cs_CodLocalidad
    
    H1006(0) = CodLocalidadOrigen
    H1006(1) = FrmAppLink.BuscarNombreSucursal(CodLocalidadOrigen)
    
    Rs.Close
    
    SQL = "SELECT c_CodLocalidad_Destino " & _
    "FROM TR_LOTE_GESTION_TRANSFERENCIA " & _
    "WHERE c_Documento = '" & Pedido & "' " & _
    "AND cs_CodLocalidad_Solicitud = '" & LocalidadSolicitud & "' " & _
    "AND cs_Corrida = '" & Lote & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    CodLocalidadDestino = Rs!c_CodLocalidad_Destino
    
    Rs.Close
    
    'Datos del Cliente
    
    SQL = "SELECT * FROM MA_SUCURSALES " & _
    "WHERE c_Codigo = '" & CodLocalidadDestino & "' "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    H1004 = Trim(Rs!c_Descripcion)
    H1016 = Trim(Rs!c_Nombre_Cheque)
    H1019 = Trim(Rs!c_Rif)
    H1020(0) = Rs!c_Direccion
    
    mLongitud = SVal(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "50"))
    
    If mLongitud = 0 Then mLongitud = 50
    
    mCadenaAux = Rs!c_Direccion & " "
    
    mCadena = Split(FrmAppLink.SplitStringIntoFixedLengthLines( _
    mLongitud, mCadenaAux, False), vbNewLine)
    
    For I = 1 To UBound(H1020)
        
        If (I - 1) <= UBound(mCadena) Then
            H1020(I) = CStr(mCadena((I - 1)))
        Else
            H1020(I) = Empty
        End If
        
    Next I
    
    H1021 = Rs!c_Telefono
    H1022 = Rs!c_Gerente
    H1023 = Rs!c_Ciudad
    
    Rs.Close
    
    'LLenando el Tr Con la Informacion de los productos empacados
    SQL = "SELECT PRO.c_Codigo AS CodProducto, DET.n_Cantidad AS Cantidad, " & vbNewLine & _
    "PRO.c_Descri AS Descripcion, DET.n_Costo AS CostoUnitario, " & vbNewLine & _
    "(DET.n_Cantidad * DET.n_Costo) AS Subtotal, " & vbNewLine & _
    "isNULL(COD.c_Codigo, 'N/A') AS CodEDI, " & vbNewLine & _
    "DET.c_Descripcion AS DescriProDoc, DET.ns_CantidadEmpaque, " & _
    "PRO.n_Peso, PRO.n_Volumen, PRO.n_PesoBul, PRO.n_VolBul, " & _
    "PRO.Cant_Decimales, PRO.n_CantiBul, PRO.c_Presenta AS UndMedida " & _
    "FROM TR_PACKING_TRANSFERENCIA DET " & vbNewLine & _
    "INNER JOIN MA_PRODUCTOS PRO " & vbNewLine & _
    "ON DET.c_CodArticulo = PRO.c_Codigo " & vbNewLine & _
    "LEFT JOIN MA_CODIGOS COD " & vbNewLine & _
    "ON DET.c_CodArticulo = COD.c_CodNasa " & vbNewLine & _
    "AND COD.nu_Intercambio = 1 " & vbNewLine & _
    "WHERE DET.c_Documento = '" & DocumentoNDT & "' " & vbNewLine & _
    "AND DET.cs_CodLocalidad = '" & FrmAppLink.GetCodLocalidadSistema & "' " & vbNewLine & _
    "ORDER BY DET.n_Linea, DET.ID "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    mTotalPeso = 0
    mTotalVolumen = 0
    mTotalArticulos = 0
    mTotalEmpaquesTransaccion = 0
    mTotalUnidadesTransaccion = 0
    mTotalEmpaquesFicha = 0
    mTotalEmpaquesFicha = 0
    
    Dim mCantiBulHistorico As Double, mCantiBulFicha As Double
    
    Set mDetalles = New Collection
    
    While Not Rs.EOF
        
        Set mItem = New Dictionary
        
        mItem("D1001") = Rs!CodProducto
        
        mCantLinea = Rs!Cantidad
        mTotalArticulos = mTotalArticulos + mCantLinea
        
        mItem("D1002") = FormatoDecimalesDinamicos(mCantLinea, 0, Rs!Cant_Decimales)
        mItem("D1003") = Rs!Descripcion
        mItem("D1004") = Rs!DescriProDoc
        mItem("D1005") = FormatNumber(Rs!CostoUnitario, 2)
        mItem("D1006") = FormatNumber(Rs!Subtotal, 2)
        mItem("D1007") = FormatoDecimalesDinamicos(Rs!n_Peso, 0, 4)
        mItem("D1008") = FormatoDecimalesDinamicos(Rs!n_Volumen, 0, 4)
        mItem("D1009") = FormatoDecimalesDinamicos(Rs!n_PesoBul, 0, 4)
        mItem("D1010") = FormatoDecimalesDinamicos(Rs!n_VolBul, 0, 4)
        
        mPesoLinea = (Rs!Cantidad * Rs!n_Peso)
        mVolumenLinea = (Rs!Cantidad * Rs!n_Volumen)
        
        mTotalPeso = mTotalPeso + mPesoLinea
        mTotalVolumen = mTotalVolumen + mVolumenLinea
        
        mItem("D1011") = FormatoDecimalesDinamicos(mPesoLinea, 0, 4)
        mItem("D1012") = FormatoDecimalesDinamicos(mVolumenLinea, 0, 4)
        
        mCantiBulHistorico = IIf(Rs!ns_CantidadEmpaque <= 0, 1, Rs!ns_CantidadEmpaque)
        mCantiBulFicha = IIf(Rs!n_CantiBul <= 0, 1, Rs!n_CantiBul)
        
        mItem("D1013") = FormatoDecimalesDinamicos(mCantiBulHistorico)
        mItem("D1014") = FormatoDecimalesDinamicos(mCantiBulFicha)
        
        mEmpaquesLineaTransaccion = Fix(mCantLinea / mCantiBulHistorico)
        mUnidadesLineaTransaccion = (CDec(mCantLinea) - _
        CDec(Fix(mCantLinea / mCantiBulHistorico) * mCantiBulHistorico))
        
        mTotalEmpaquesTransaccion = mTotalEmpaquesTransaccion + mEmpaquesLineaTransaccion
        mTotalUnidadesTransaccion = mTotalUnidadesTransaccion + mUnidadesLineaTransaccion
        
        mItem("D1015") = FormatoDecimalesDinamicos(mEmpaquesLineaTransaccion)
        mItem("D1016") = FormatoDecimalesDinamicos(mUnidadesLineaTransaccion, 0, Rs!Cant_Decimales)
        
        mEmpaquesLineaFicha = Fix(mCantLinea / mCantiBulFicha)
        mUnidadesLineaFicha = (CDec(mCantLinea) - _
        CDec(Fix(mCantLinea / mCantiBulFicha) * mCantiBulFicha))
        
        mTotalEmpaquesFicha = mTotalEmpaquesFicha + mEmpaquesLineaFicha
        mTotalUnidadesFicha = mTotalUnidadesFicha + mUnidadesLineaFicha
        
        mItem("D1017") = FormatoDecimalesDinamicos(mEmpaquesLineaFicha)
        mItem("D1018") = FormatoDecimalesDinamicos(mUnidadesLineaFicha, 0, Rs!Cant_Decimales)
        
        mItem("D1019") = Rs!CodEDI
        
        mDetalles.Add mItem
        
        Rs.MoveNext
        
    Wend
    
    H1008 = FormatoDecimalesDinamicos(mTotalPeso, 0, 4)
    H1009 = FormatoDecimalesDinamicos(mTotalVolumen, 0, 4)
    H1010 = FormatoDecimalesDinamicos(mTotalArticulos, 0, 4)
    H1011 = FormatoDecimalesDinamicos(mTotalEmpaquesTransaccion, 0, 4)
    H1012 = FormatoDecimalesDinamicos(mTotalUnidadesTransaccion, 0, 4)
    H1013 = FormatoDecimalesDinamicos(mTotalEmpaquesFicha, 0, 4)
    H1014 = FormatoDecimalesDinamicos(mTotalUnidadesFicha, 0, 4)
    
    Rs.Close
    
    If PathExists(NDEArchivo) Then
        
        Set Rtf = New recsun.Obj_rtf
        
        With Rtf
            
            .RutayArchivo = NDEArchivo
            
            .IdentifidicadorMA = "H1"
            .IdentifidicadorTR = "D1"

            Call .LLenarCamposMA("H101", H101)
            Call .LLenarCamposMA("H102", H102)
            Call .LLenarCamposMA("H1017", H1017(0))
            Call .LLenarCamposMA("H1017-1", H1017(1))
            Call .LLenarCamposMA("H1017-2", H1017(2))
            Call .LLenarCamposMA("H1017-3", H1017(3))
            
            Call .LLenarCamposMA("H1001", H1001)
            Call .LLenarCamposMA("H1002", H1002(0))
            
            For I = 1 To UBound(H1002)
                
                mVariable = "H1002-" & Format(I, "#")
                
                Call .LLenarCamposMA(CStr(mVariable), H1002(I))
                
            Next I
            
            Call .LLenarCamposMA("H1003", H1003)
            Call .LLenarCamposMA("H1018", H1018)
            
            Call .LLenarCamposMA("H1015", H1015)
            
            Call .LLenarCamposMA("H1005", H1005)
            Call .LLenarCamposMA("H1005P", H1005P) 'Pedido
            
            Call .LLenarCamposMA("H1006", H1006(0))
            Call .LLenarCamposMA("H1006-1", H1006(1))
            
            Call .LLenarCamposMA("H1007", H1007)
            Call .LLenarCamposMA("H1008", H1008)
            Call .LLenarCamposMA("H1009", H1009)
            Call .LLenarCamposMA("H1010", H1010)
            Call .LLenarCamposMA("H1011", H1011)
            Call .LLenarCamposMA("H1012", H1012)
            Call .LLenarCamposMA("H1013", H1013)
            Call .LLenarCamposMA("H1014", H1014)
            
            Call .LLenarCamposMA("H1004", H1004)
            Call .LLenarCamposMA("H1016", H1016)
            Call .LLenarCamposMA("H1019", H1019)
            Call .LLenarCamposMA("H1020", H1020(0))
            
            For I = 1 To UBound(H1020)
                
                mVariable = "H1020-" & Format(I, "#")
                
                Call .LLenarCamposMA(CStr(mVariable), H1020(I))
                
            Next I
            
            Call .LLenarCamposMA("H1021", H1021)
            Call .LLenarCamposMA("H1022", H1022)
            Call .LLenarCamposMA("H1023", H1023)
            
            For Each mItem In mDetalles
                
                .LLenarCamposTr_add
                
                .LLenarCamposTr "D1001", CStr(mItem("D1001"))
                .LLenarCamposTr "D1002", CStr(mItem("D1002"))
                .LLenarCamposTr "D1003", CStr(mItem("D1003"))
                .LLenarCamposTr "D1004", CStr(mItem("D1004"))
                .LLenarCamposTr "D1005", CStr(mItem("D1005"))
                .LLenarCamposTr "D1006", CStr(mItem("D1006"))
                .LLenarCamposTr "D1007", CStr(mItem("D1007"))
                .LLenarCamposTr "D1008", CStr(mItem("D1008"))
                .LLenarCamposTr "D1009", CStr(mItem("D1009"))
                .LLenarCamposTr "D1010", CStr(mItem("D1010"))
                .LLenarCamposTr "D1011", CStr(mItem("D1011"))
                .LLenarCamposTr "D1012", CStr(mItem("D1012"))
                .LLenarCamposTr "D1013", CStr(mItem("D1013"))
                .LLenarCamposTr "D1014", CStr(mItem("D1014"))
                .LLenarCamposTr "D1015", CStr(mItem("D1015"))
                .LLenarCamposTr "D1016", CStr(mItem("D1016"))
                .LLenarCamposTr "D1017", CStr(mItem("D1017"))
                .LLenarCamposTr "D1018", CStr(mItem("D1018"))
                .LLenarCamposTr "D1019", CStr(mItem("D1019"))
                
            Next
            
            If (Trim(mvarNDEPrinter) <> Empty) Then
                .Impresora = mvarNDEPrinter
            End If
            
            .MostrarSelecionImpresora = pShowRTF ' para imprimir directo con la impresora por defecto
            .ModoImpresion = IIf(pShowRTF, s_RtfPantalla, s_RtfImpresora) ' para imprimir directo con la impresora por defecto
            
            .Ejecutar
            
        End With
        
    End If
    
    If PathExists(NDEArchivoEtiqueta) Then
        
        Dim mContenidoArchivo As String, mContenidoLineas As Variant
        Dim mLineaResuelta As String, mLineaBase As String
        Dim InicioDetalle As Boolean
        
        mContenidoArchivo = LoadTextFile(NDEArchivoEtiqueta)
        
        mContenidoLineas = Split(mContenidoArchivo, vbNewLine, , vbTextCompare)
        
        mContenidoArchivo = Empty
        
        For I = 0 To UBound(mContenidoLineas)
            
            If InStr(1, mContenidoLineas(I), "|*D*|", vbTextCompare) > 0 Then
                InicioDetalle = True
                mContenidoLineas(I) = Replace(mContenidoLineas(I), "|*D*|", Empty, 1, 1, vbTextCompare)
            End If
            
            mLineaResuelta = mContenidoLineas(I)
            
            mLineaResuelta = Replace(mLineaResuelta, "|H101|", H101, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H102|", H102, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1017|", H1017(0), , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1017-1|", H1017(1), , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1017-2|", H1017(2), , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1017-3|", H1017(3), , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1001|", H1001, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1002|", H1002(0), , , vbTextCompare)
            For K = 1 To UBound(H1002)
                mLineaResuelta = Replace(mLineaResuelta, "|H1002-" & Format(K, "#") & "|", H1002(K), , , vbTextCompare)
            Next K
            mLineaResuelta = Replace(mLineaResuelta, "|H1003|", H1003, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1018|", H1018, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1015|", H1015, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1005|", H1005, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1005P|", H1005P, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1006|", H1006(0), , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1006-1|", H1006(1), , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1007|", H1007, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1008|", H1008, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1009|", H1009, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1010|", H1010, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1011|", H1011, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1012|", H1012, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1013|", H1013, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1014|", H1014, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1004|", H1004, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1016|", H1016, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1019|", H1019, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1020|", H1020(0), , , vbTextCompare)
            For K = 1 To UBound(H1020)
                mLineaResuelta = Replace(mLineaResuelta, "|H1020-" & Format(K, "#") & "|", H1020(K), , , vbTextCompare)
            Next K
            mLineaResuelta = Replace(mLineaResuelta, "|H1021|", H1021, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1022|", H1022, , , vbTextCompare)
            mLineaResuelta = Replace(mLineaResuelta, "|H1023|", H1023, , , vbTextCompare)
            
            If InicioDetalle Then
                
                InicioDetalle = False
                
                mLineaBase = mLineaResuelta
                
                mLineaResuelta = Empty
                
                mCont = 0
                
                For Each mItem In mDetalles
                    
                    mCont = mCont + 1
                    
                    mLineaDetalle = mLineaBase
                    mLineaDetalle = Replace(mLineaDetalle, "|D1001|", CStr(mItem("D1001")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1002|", CStr(mItem("D1002")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1003|", CStr(mItem("D1003")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1004|", CStr(mItem("D1004")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1005|", CStr(mItem("D1005")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1006|", CStr(mItem("D1006")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1007|", CStr(mItem("D1007")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1008|", CStr(mItem("D1008")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1009|", CStr(mItem("D1009")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1010|", CStr(mItem("D1010")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1011|", CStr(mItem("D1011")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1012|", CStr(mItem("D1012")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1013|", CStr(mItem("D1013")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1014|", CStr(mItem("D1014")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1015|", CStr(mItem("D1015")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1016|", CStr(mItem("D1016")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1017|", CStr(mItem("D1017")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1018|", CStr(mItem("D1018")), , , vbTextCompare)
                    mLineaDetalle = Replace(mLineaDetalle, "|D1019|", CStr(mItem("D1019")), , , vbTextCompare)
                    
                    mLineaResuelta = mLineaResuelta & mLineaDetalle & _
                    IIf(mCont < mDetalles.Count, vbNewLine, Empty)
                    
                Next
                
            End If
            
            If InStr(1, mLineaResuelta, "**[DELETE_LINE]**", vbTextCompare) <= 0 _
            And Not Left(mLineaResuelta, 5) = "/*-*/" Then
                mContenidoArchivo = mContenidoArchivo & _
                IIf(I > 0, vbNewLine, Empty) & mLineaResuelta
            End If
            
        Next I
        
        mContenidoLineas = Split(mContenidoArchivo, vbNewLine, , vbTextCompare)
        
        mUltimaImpresora = Printer.DeviceName
        
        If (Trim(mvarNDELabelPrinter) <> Empty) Then
            Call BuscarImpresora(mvarNDELabelPrinter)
        End If
        
        For mCampo = 0 To UBound(mContenidoLineas)
            Printer.Print mContenidoLineas(mCampo)
        Next
        
        Printer.EndDoc
        
        If (Trim(mvarNDELabelPrinter) <> Empty) Then
            Call BuscarImpresora(mUltimaImpresora)
        End If
        
        ' ...
        
    End If
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If (Trim(mUltimaImpresora) <> Empty) Then
        Call BuscarImpresora(mUltimaImpresora)
    End If
    
    Mensaje True, "Error al generar el RTF de la Nota de Empacado, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    PrinterSecureKillDoc
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Grid_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyF6 And Shift = 0 Then
        
        If Grid.Rows > 1 Then
            
            Mensaje False, "Atenci�n! �Est� seguro de que no hay " & _
            "cantidad faltante de ning�n producto en este pedido? " & _
            GetLines & _
            "En tal caso, presione Aceptar para continuar"
            
            If Retorno Then
                
                CheckAll = True
                
                For I = 1 To Grid.Rows - 1
                    Grid.Row = I
                    Grid.RowSel = Grid.Row
                    Grid_DblClick
                Next I
                
                CheckAll = False
                
                PrepararGrid
                PrepararDatos
                
            End If
            
        End If
        
    End If
    
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call Grid_DblClick
    End If
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub Chk_Cantidad_Click()
    If Chk_Cantidad.Value = vbChecked Then
        OrderBy = "ORDER BY PP.CantSolicitada DESC "
        Chk_NombreProducto.Value = vbUnchecked
        Chk_Peso.Value = vbUnchecked
        Chk_Volumen.Value = vbUnchecked
        Call PrepararGrid
        Call PrepararDatos
    End If
End Sub

Private Sub Chk_NombreProducto_Click()
    If Chk_NombreProducto.Value = vbChecked Then
        OrderBy = "ORDER BY PRO.c_Descri "
        Chk_Cantidad.Value = vbUnchecked
        Chk_Peso.Value = vbUnchecked
        Chk_Volumen.Value = vbUnchecked
        Call PrepararGrid
        Call PrepararDatos
    End If
End Sub

Private Sub Chk_Peso_Click()
    If Chk_Peso.Value = vbChecked Then
        OrderBy = "ORDER BY (PP.CantRecolectada * PRO.n_Peso) DESC "
        Chk_NombreProducto.Value = vbUnchecked
        Chk_Cantidad.Value = vbUnchecked
        Chk_Volumen.Value = vbUnchecked
        Call PrepararGrid
        Call PrepararDatos
    End If
End Sub

Private Sub Chk_Volumen_Click()
    If Chk_Volumen.Value = vbChecked Then
        OrderBy = "ORDER BY (PP.CantRecolectada * PRO.n_Volumen) DESC "
        Chk_NombreProducto.Value = vbUnchecked
        Chk_Peso.Value = vbUnchecked
        Chk_Cantidad.Value = vbUnchecked
        Call PrepararGrid
        Call PrepararDatos
    End If
End Sub

Private Sub Cod_Buscar_GotFocus()
    Cod_Buscar.SelStart = 0
    Cod_Buscar.SelLength = Len(Cod_Buscar)
End Sub

Private Sub Cod_Buscar_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        
        Dim SQL As String, Rs As New ADODB.Recordset, Band As Integer
        
        SQL = "SELECT c_CodNasa " & _
        "FROM MA_CODIGOS " & _
        "WHERE c_Codigo = '" & FixTSQL(Trim(Cod_Buscar.Text)) & "' "
        
        Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
        
        If Not Rs.EOF Then
            
            Cod_Buscar = Rs!c_CodNasa
            
            Band = 0
            
            For I = 1 To Grid.Rows - 1
                
                If UCase(Grid.TextMatrix(I, ColCodPro)) = UCase(Cod_Buscar) Then
                    
                    Band = I
                    
                    If CDec(Grid.TextMatrix(I, ColCantSolicitada)) _
                    <> CDec(Grid.TextMatrix(I, ColCantRecolectada)) Then
                        
                        Grid.Row = I
                        
                        Call Grid_DblClick
                        
                        Cod_Buscar = Empty
                        
                        Exit Sub
                        
                    End If
                    
                End If
                
            Next I
            
            If Band > 0 Then
                Call Mensaje(True, "El producto ya fue recolectado.")
            Else
                Call Mensaje(True, "El producto no esta dentro de su pedido.")
            End If
            
        Else
            
            Call Mensaje(True, "C�digo no encontrado, verifique el c�digo a buscar.")
            
            If gRutinas.PuedeObtenerFoco(Cod_Buscar) Then Cod_Buscar.SetFocus
            
            Exit Sub
            
        End If
        
        Cod_Buscar = Empty
        
    End If
    
End Sub

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute( _
    "SELECT c_Proceso " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE c_Proceso = '009' ")
    
    If Not TempRs.EOF Then
        frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    End If
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub
