VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsTrans_Det_Inv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarCodigo                                          As String
Private mvarLocalidadDestino                                As String
Private mvarDepositoOrigen                                  As String
Private mvarCantidad                                        As Double
Private mvarCantidadDisponible                              As Double
Private mvarCantUltimaSolicitud                             As Double
Private mDocumentoInv                                       As Collection

Property Get Codigo() As String
    Codigo = mvarCodigo
End Property

Property Get LocalidadDestino() As String
    LocalidadDestino = mvarLocalidadDestino
End Property

Property Get DepositoOrigen() As String
    DepositoOrigen = mvarDepositoOrigen
End Property

Property Get Cantidad() As Double
    Cantidad = mvarCantidad
End Property

Property Get CantidadDisponible() As Double
    CantidadDisponible = mvarCantidadDisponible
End Property

Property Get UltimaCantidadSolicitud() As Double
    UltimaCantidadSolicitud = mvarCantUltimaSolicitud
End Property

Property Get DocumentosInv() As Collection
    Set DocumentosInv = mDocumentoInv
End Property

Public Sub AsignarCantidadDisponibleLoc(pDoc As String, pCantidad As Double)
    
    Dim mCls As New ClsTrans_Det_InvxDoc
    
    'If mvarCantidadDisponible > 0 Then
        
        mvarCantUltimaSolicitud = pCantidad
        
        mDocumentoInv.Add mCls.AddDocumentoInvxDoc(pDoc, mvarCantidadDisponible, mvarCantUltimaSolicitud)
        
        mvarCantidadDisponible = mvarCantidadDisponible + mvarCantUltimaSolicitud
        
    'Else
        'mvarCantUltimaSolicitud = 0
    'End If
    
End Sub

Public Function AddProductoxLocalidad(pCodigo As String, _
pLocalidad As String, pCantidad As Double) As ClsTrans_Det_Inv
    
    mvarCodigo = pCodigo
    mvarLocalidadDestino = pLocalidad
    mvarCantidad = pCantidad
    mvarCantidadDisponible = pCantidad
    
    Set mDocumentoInv = New Collection
    Set AddProductoxLocalidad = Me
    
End Function

Public Function AddProductoxDeposito(pCodigo As String, _
pDeposito As String, pCantidad As Double) As ClsTrans_Det_Inv
    
    mvarCodigo = pCodigo
    mvarDepositoOrigen = pDeposito
    mvarCantidad = pCantidad
    mvarCantidadDisponible = pCantidad
    
    Set mDocumentoInv = New Collection
    Set AddProductoxDeposito = Me
    
End Function

Public Sub AsignarCantidadDisponible(pDoc As String, pCantidad As Double)
    
    Dim mCls As New ClsTrans_Det_InvxDoc
    
    If mvarCantidadDisponible > 0 Then
        
        mvarCantUltimaSolicitud = IIf(mvarCantidadDisponible >= pCantidad, pCantidad, mvarCantidadDisponible)
        mDocumentoInv.Add mCls.AddDocumentoInvxDoc(pDoc, mvarCantidadDisponible, mvarCantUltimaSolicitud)
        
        If mvarCantidadDisponible > pCantidad Then
            mvarCantidadDisponible = (CDec(mvarCantidadDisponible) - CDec(pCantidad))
        Else
            mvarCantidadDisponible = 0
        End If
        
    Else
        mvarCantUltimaSolicitud = 0
    End If
    
End Sub

Public Sub IniciarDocumentos()
    
    Set mDocumentoInv = New Collection
    
    mvarCantidadDisponible = mvarCantidad
    
End Sub

