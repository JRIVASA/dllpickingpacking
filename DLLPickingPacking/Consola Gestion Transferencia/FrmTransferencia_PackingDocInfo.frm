VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmTransferencia_PackingDocInfo 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton ButtonLeyenda 
      Caption         =   "Leyenda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   13920
      Picture         =   "FrmTransferencia_PackingDocInfo.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   5
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9650
      Width           =   1215
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   8850
      LargeChange     =   10
      Left            =   14480
      TabIndex        =   3
      Top             =   600
      Width           =   674
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   19320
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Pedido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   480
         TabIndex        =   2
         Top             =   120
         Width           =   7575
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12480
         TabIndex        =   1
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14520
         Picture         =   "FrmTransferencia_PackingDocInfo.frx":0CCA
         Top             =   0
         Width           =   480
      End
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   8865
      Left            =   160
      TabIndex        =   4
      Top             =   600
      Width           =   15000
      _ExtentX        =   26458
      _ExtentY        =   15637
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblValorCantEnEmpaques 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   6840
      TabIndex        =   13
      Top             =   9840
      Width           =   1575
   End
   Begin VB.Label lblCantEnEmpaques 
      BackStyle       =   0  'Transparent
      Caption         =   "En Empaques:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   4680
      TabIndex        =   12
      Top             =   9840
      Width           =   2175
   End
   Begin VB.Label lblValorTotalVolumenRec 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   6840
      TabIndex        =   11
      Top             =   10320
      Width           =   1575
   End
   Begin VB.Label lblTotalVolumenRec 
      BackStyle       =   0  'Transparent
      Caption         =   "Total Volumen Rec.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   4680
      TabIndex        =   10
      Top             =   10320
      Width           =   2175
   End
   Begin VB.Label lblValorTotalPesoRec 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   2400
      TabIndex        =   9
      Top             =   10320
      Width           =   1575
   End
   Begin VB.Label lblTotalPesoRec 
      BackStyle       =   0  'Transparent
      Caption         =   "Total Peso Rec.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   10320
      Width           =   2175
   End
   Begin VB.Label lblValorTotalCantRec 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   2400
      TabIndex        =   7
      Top             =   9840
      Width           =   1575
   End
   Begin VB.Label lblTotalCantRec 
      BackStyle       =   0  'Transparent
      Caption         =   "Total Art�culos Rec.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   9840
      Width           =   2175
   End
End
Attribute VB_Name = "FrmTransferencia_PackingDocInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Lote As String
Public Pedido As String
Public LocalidadSolicitud As String

Private Enum GrdPack
    ColNull
    ColCodigoAlterno
    ColDesPro
    ColCantSolicitada
    ColCantRecolectada
    ColCantEmpacada
    ColPesoRec
    ColVolumenRec
    ColCantiBul
    ColHidden1
    ColCount
End Enum

Private AnchoCampoDescripcion As Long

Private Band As Boolean
Private Fila As Integer

Private Salir As Variant

Private mPostPacking As Boolean

Property Let PostPacking(ByVal pValor As Boolean)
    mPostPacking = pValor
End Property

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute( _
    "SELECT c_Proceso " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE c_Proceso = '007' ")
    
    If Not TempRs.EOF Then frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub

Private Sub Form_Activate()
    
    If Salir Then
        
        Mensaje True, "No hay productos para mostrar."
        
        Unload Me
        
        Exit Sub
        
    End If
    
    SafeFocus Grid
    
End Sub

Private Sub Form_Load()
    
    If mPostPacking Then
        lblTotalCantRec.Caption = "Total Art�culos Emp.:"
        lblCantEnEmpaques.Caption = "En Empaques:"
        lblTotalPesoRec.Caption = "Total Peso Emp.:"
        lblTotalVolumenRec.Caption = "Total Volumen Emp.:"
    End If
    
    AjustarPantalla Me
    
    Salir = False
    
    Call PrepararGrid
    Call PrepararDatos
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionByRow
        
        .FixedCols = 0
        .FixedRows = 1
        
        .Rows = 1
        .Cols = ColCount
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        AnchoCampoDescripcion = 5750
        
        .Row = 0
        
        .Col = ColNull
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColCodigoAlterno
        .ColWidth(.Col) = 2250
        .Text = "Art�culo"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesPro
        .ColWidth(.Col) = AnchoCampoDescripcion
        .Text = "Nombre del Producto"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantSolicitada
        .ColWidth(.Col) = 1500
        .Text = "Cant. Sol"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantRecolectada
        .ColWidth(.Col) = 1500
        .Text = "Cant. Rec"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantEmpacada
        .ColWidth(.Col) = 1500
        .Text = "Cant. Emp"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColPesoRec
        .ColWidth(.Col) = 1250
        If mPostPacking Then
            .Text = "Peso Emp."
        Else
            .Text = "Peso Rec"
        End If
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColVolumenRec
        .ColWidth(.Col) = 1250
        If mPostPacking Then
            .Text = "Vol. Emp."
        Else
            .Text = "Vol. Rec"
        End If
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantiBul
        .ColWidth(.Col) = 0
        .Text = "Uni. x Emp."
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColHidden1
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Width = 15000
        
        .ScrollTrack = True
        
        Fila = .Row
        
        .Col = ColHidden1
        .ColSel = ColVolumenRec
        
    End With
    
    'For I = 0 To Grid.Cols - 1
        'Grid.Col = I
        'Grid.CellAlignment = flexAlignCenterCenter
    'Next
    
End Sub

Private Sub PrepararDatos()
    
    On Error GoTo Error1
    
    Dim SQL As String, Rs As New ADODB.Recordset, I As Integer, Deposito As String
    
    Dim TotalCantRec, TotalPesoRec, TotalVolumenRec, TotalEmpaquesRec
    
    SQL = "SELECT PRO.c_Descri AS Nombre, PP.CantSolicitada, " & vbNewLine & _
    "PP.CantRecolectada, isNULL(COD.c_Codigo, PRO.c_Codigo) AS CodEDI, " & vbNewLine & _
    "PP.CantEmpacada, PP.Packing AS Packing, PP.Picking AS Picking, " & vbNewLine & _
    "PRO.Cant_Decimales, PRO.n_Peso, PRO.n_Volumen, PRO.n_PesoBul, PRO.n_VolBul, " & vbNewLine & _
    "PRO.n_CantiBul " & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & _
    "INNER JOIN MA_PRODUCTOS PRO " & _
    "ON PRO.c_Codigo = PP.CodProducto " & _
    "LEFT JOIN MA_CODIGOS COD " & _
    "ON COD.c_CodNasa = PRO.c_Codigo " & _
    "AND COD.nu_Intercambio = 1 " & vbNewLine & _
    "WHERE PP.CodLote = '" & Lote & "' " & vbNewLine & _
    "AND PP.CodPedido = '" & Pedido & "' " & vbNewLine & _
    "AND PP.cs_CodLocalidad_Solicitud = '" & LocalidadSolicitud & "' "
    
    'TEST GRAN CANTIDAD DE FILAS
    ''SQL = SQL & " UNION ALL " & SQL & " UNION ALL " & SQL & " UNION ALL " & SQL & " UNION ALL " & SQL & " UNION ALL " & SQL
    ''SQL = SQL & "ORDER BY c_Descri "
    
    SQL = SQL & "ORDER BY PRO.c_Descri "
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Rs.EOF Then
        Salir = True
    End If
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    Grid.Visible = False
    
    TotalCantRec = CDec(0)
    TotalEmpaquesRec = CDec(0)
    TotalPesoRec = CDec(0)
    TotalVolumenRec = CDec(0)
    
    While Not Rs.EOF
        
        Grid.Rows = Grid.Rows + 1
        Linea = Grid.Rows - 1
        
        Grid.RowData(Linea) = Grid.RowHeight(Linea)
        
        Grid.TextMatrix(Linea, ColNull) = Empty
        
        Grid.TextMatrix(Linea, ColCodigoAlterno) = Rs!CodEDI
        Grid.TextMatrix(Linea, ColDesPro) = Rs!Nombre
        Grid.TextMatrix(Linea, ColCantiBul) = CDec(IIf(Rs!n_CantiBul <= 0, 1, Rs!n_CantiBul))
        
        Grid.TextMatrix(Linea, ColCantSolicitada) = FormatNumber(Rs!CantSolicitada, Rs!Cant_Decimales)
        Grid.TextMatrix(Linea, ColCantRecolectada) = FormatNumber(Rs!CantRecolectada, Rs!Cant_Decimales)
        Grid.TextMatrix(Linea, ColCantEmpacada) = FormatNumber(Rs!CantEmpacada, Rs!Cant_Decimales)
        
        If mPostPacking Then
            
            Grid.TextMatrix(Linea, ColPesoRec) = FormatoDecimalesDinamicos( _
            (Rs!CantEmpacada * Rs!n_Peso), 0, 4)
            Grid.TextMatrix(Linea, ColVolumenRec) = FormatoDecimalesDinamicos( _
            (Rs!CantEmpacada * Rs!n_Volumen), 0, 4)
            
        Else
            
            Grid.TextMatrix(Linea, ColPesoRec) = FormatoDecimalesDinamicos( _
            (Rs!CantRecolectada * Rs!n_Peso), 0, 4)
            Grid.TextMatrix(Linea, ColVolumenRec) = FormatoDecimalesDinamicos( _
            (Rs!CantRecolectada * Rs!n_Volumen), 0, 4)
            
        End If
        
        If mPostPacking Then
            
            TotalCantRec = TotalCantRec + SDec(Grid.TextMatrix(Linea, ColCantEmpacada))
            TotalPesoRec = TotalPesoRec + SDec(Grid.TextMatrix(Linea, ColPesoRec))
            TotalVolumenRec = TotalVolumenRec + SDec(Grid.TextMatrix(Linea, ColVolumenRec))
            TotalEmpaquesRec = TotalEmpaquesRec + _
            Fix(SDec(Grid.TextMatrix(Linea, ColCantEmpacada)) / SDec(Grid.TextMatrix(Linea, ColCantiBul)))
            
        Else
            
            TotalCantRec = TotalCantRec + SDec(Grid.TextMatrix(Linea, ColCantEmpacada))
            TotalPesoRec = TotalPesoRec + SDec(Grid.TextMatrix(Linea, ColPesoRec))
            TotalVolumenRec = TotalVolumenRec + SDec(Grid.TextMatrix(Linea, ColVolumenRec))
            TotalEmpaquesRec = TotalEmpaquesRec + _
            Fix(SDec(Grid.TextMatrix(Linea, ColCantEmpacada)) / SDec(Grid.TextMatrix(Linea, ColCantiBul)))
            
        End If
        
        If Rs!Packing And Rs!Picking Then
            
            If CDec(Rs!CantSolicitada) = CDec(Rs!CantEmpacada) Then
                For I = 0 To Grid.Cols - 1
                    Grid.Row = Grid.Rows - 1
                    Grid.Col = I
                    Grid.CellBackColor = &HB4EDB9 'verde
                Next I
            Else
                For I = 0 To Grid.Cols - 1
                    Grid.Row = Grid.Rows - 1
                    Grid.Col = I
                    Grid.CellBackColor = 12632319 'rojo
                Next I
            End If
            
        ElseIf (Rs!CantEmpacada > 0) Then
            For I = 0 To Grid.Cols - 1
                Grid.Row = Grid.Rows - 1
                Grid.Col = I
                Grid.CellBackColor = -2147483624 'Amarillo
            Next I
        End If
        
        Rs.MoveNext
        
    Wend
    
    lblValorTotalCantRec = FormatoDecimalesDinamicos(TotalCantRec)
    lblValorCantEnEmpaques = FormatoDecimalesDinamicos(TotalEmpaquesRec)
    lblValorTotalPesoRec = FormatoDecimalesDinamicos(TotalPesoRec)
    lblValorTotalVolumenRec = FormatoDecimalesDinamicos(TotalVolumenRec)
    
    Rs.Close
    
    If Grid.Row > 1 Then
        Grid.Row = 1
        Fila = 1 ' Para que se cambie el tama�o en el EnterCell
        Grid.ColSel = ColVolumenRec
    End If
    
    If Grid.Rows > 14 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(ColDesPro) = AnchoCampoDescripcion - 675
    Else
        Grid.ScrollBars = flexScrollBarNone
        ScrollGrid.Visible = False
    End If
    
    Grid_EnterCell
    
Finally:
    
    LabelTitulo.Caption = "Detalles de la Solicitud de Transferenica " & Pedido
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, " & _
    "Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub Grid_EnterCell()
        
    If Grid.Rows = 1 Then
        'FrameSelect.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = Grid.RowData(Fila) '600
            End If
            
            Grid.RowHeight(Grid.Row) = Grid.RowData(Grid.Row) + 300 '900
            Fila = Grid.Row
            
        End If
        
        'MostrarEditorTexto2 Me, Grid, CmdSelect
        Grid.ColSel = ColNull
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        
    Else
        Band = False
    End If
    
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub
