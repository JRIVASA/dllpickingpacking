VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsTrans_Det"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarCodigoItem                                                      As String
Private mvarDescripcionItem                                                 As String
Private mvarPresentacionItem                                                As Double

Private mvarCantidadSolicitada                                              As Double
Private mvarCantidadAsignada                                                As Double
Private mvarCantidadInv                                                     As Double

Private mvarMonedaItem                                                      As String
Private mvarFactorMonedaItem                                                As Double
Private mvarPrecio                                                          As Double
Private mvarCosto                                                           As Double

Private mvarCostoDoc                                                        As Double
Private mvarCantibulDoc                                                     As Double

Private mvarTipoProducto                                                    As Integer
Private mvarPorcImpuestoItem                                                As Double
Private mvarDocumentoRel                                                    As String
Private mvarTipoDocumentoRel                                                As String
Private mvarLineaDocumentoRel                                               As Long
Private mvarNumeroDecimales                                                 As Integer

Private mvarIdItem                                                          As Long
Private mvarPoseeInv                                                        As Boolean
Private mvarVolumen                                                         As Double
Private mvarVolumenEmp                                                      As Double
Private mvarPeso                                                            As Double
Private mvarPesoEmp                                                         As Double

'************************************************** Property Gets **********************************************************************

Property Get CodigoItem() As String
    CodigoItem = mvarCodigoItem
End Property

Property Get DescripcionItem() As String
    DescripcionItem = mvarDescripcionItem
End Property

Property Get PresentacionItem() As Double
    PresentacionItem = mvarPresentacionItem
End Property

Property Get UniXEmp_Documento() As Double
    UniXEmp_Documento = mvarCantibulDoc
End Property

Property Get PorcImpuestoItem() As Double
    PorcImpuestoItem = mvarPorcImpuestoItem
End Property

Property Get MonedaItem() As String
    MonedaItem = mvarMonedaItem
End Property

Property Get FactorMonedaItem() As Double
    FactorMonedaItem = mvarFactorMonedaItem
End Property

Property Get CostoItem() As Double
    CostoItem = mvarCosto
End Property

Property Get CostoItemDoc() As Double
    CostoItemDoc = mvarCostoDoc
End Property

Property Get PrecioItem() As Double
    PrecioItem = mvarPrecio
End Property

Property Get TipoProducto() As Integer
    TipoProducto = mvarTipoProducto
End Property

Property Get NumeroDecimales() As Integer
    NumeroDecimales = mvarNumeroDecimales
End Property

Property Get PoseeInv() As Boolean
    PoseeInv = mvarPoseeInv
End Property

Property Let PoseeInv(pValor As Boolean)
    mvarPoseeInv = pValor
End Property

Property Get DiferenciaEnCantidades() As Boolean
    DiferenciaEnCantidades = (mvarCantidadSolicitada <> mvarCantidadAsignada)
End Property

'************************************************** Cantidades Item *****************************************************************************

Property Get CantidadSolicitadaUnidades() As Double
    'If mvarNumeroDecimales = 0 Then
        'CantidadSolicitadaUnidades = mvarCantidadSolicitada Mod mvarCantibulDoc
    'Else
        'CantidadSolicitadaUnidades = (mvarCantidadSolicitada / mvarCantibulDoc) _
        - Fix(mvarCantidad / mvarCantibulDoc)
        CantidadSolicitadaUnidades = (CDec(mvarCantidadSolicitada) - _
        CDec(Fix(mvarCantidadSolicitada / mvarCantibulDoc) * mvarCantibulDoc))
    'End If
End Property

Property Get CantidadSolicitadaEmpaques() As Double
    CantidadSolicitadaEmpaques = Fix(mvarCantidadSolicitada / mvarCantibulDoc)
End Property

Property Get CantidadSolicitadaItem() As Double
    CantidadSolicitadaItem = mvarCantidadSolicitada
End Property

Property Get CantidadAsignadaItem() As Double
    CantidadAsignadaItem = mvarCantidadAsignada
End Property

Property Let CantidadAsignadaItem(pValor As Double)
    mvarCantidadAsignada = pValor
End Property

Property Get CantidadAsignadaUnidades() As Double
    'If mvarNumeroDecimales = 0 Then
        'CantidadAsignadaUnidades = mvarCantidadAsignada Mod mvarCantibulDoc
    'Else
        CantidadAsignadaUnidades = (CDec(mvarCantidadAsignada) - _
        CDec(Fix(mvarCantidadAsignada / mvarCantibulDoc) * mvarCantibulDoc))
    'End If
End Property

Property Get CantidadAsignadaEmpaques() As Double
    CantidadAsignadaEmpaques = Fix(mvarCantidadAsignada / mvarCantibulDoc)
End Property

Property Get CantidadEmpaquesGrid() As Double
    'If mvarCantibulDoc <> 1 Then
        CantidadEmpaquesGrid = Fix(mvarCantidadSolicitada / mvarCantibulDoc)
    'Else
        'CantidadEmpaquesGrid = 0
    'End If
End Property

Property Get CantidadUnidadesGrid() As Double
    'If mvarCantibulDoc <> 1 Then
        CantidadUnidadesGrid = CantidadSolicitadaUnidades
    'Else
        'CantidadUnidadesGrid = CantidadSolicitadaItem
    'End If
End Property

Property Get CantidadInvDisponible() As Double
    CantidadInvDisponible = mvarCantidadInv
End Property

Property Let CantidadInvDisponible(pValor As Double)
    mvarCantidadInv = pValor
End Property

'************ Montos x Precio ****************************************

Property Get SubtotalItemFac() As Double
    SubtotalItemFac = mvarPrecio * mvarCantidad
End Property

Property Get ImpuestoItemFac() As Double
    ImpuestoItemFac = SubtotalItemFac * (mvarPorcImpuestoItem / 100)
End Property

Property Get TotalItemFac() As Double
    TotalItemFac = SubtotalItemFac + ImpuestoItemFac
End Property

Property Get SubtotalItem() As Double
    SubtotalItem = mvarCostoDoc * mvarCantidadAsignada
End Property

Property Get ImpuestoItem() As Double
    ImpuestoItem = SubtotalItem * (mvarPorcImpuestoItem / 100)
End Property

Property Get TotalItem() As Double
    TotalItem = SubtotalItem + ImpuestoItem
End Property

'********************************************************* Documento Relacion *******************************************************************************

Property Get LineaDocumentoRel() As Long
    LineaDocumentoRel = mvarLineaDocumentoRel
End Property

Property Get TipoDocumentoRel() As String
    TipoDocumentoRel = mvarTipoDocumentoRel
End Property

Property Get DocumentoRel() As String
    DocumentoRel = mvarDocumentoRel
End Property

Property Get IdItemPedido() As Long
    IdItemPedido = mvarIdItem
End Property

'*********************************************************** Volumen y Peso ******************************************************************************************

Property Get VolumenUnidad() As Double
    VolumenUnidad = mvarVolumen
End Property

Property Get VolumenEmpaque() As Double
    VolumenEmpaque = mvarVolumenEmp
End Property

Property Get VolumenUnitarioItemSolicitud() As Double
    VolumenUnitarioItemSolicitud = (mvarVolumen * CantidadSolicitadaItem)
End Property

Property Get VolumenItemSolicitud() As Double
    VolumenItemSolicitud = (mvarVolumen * CantidadSolicitadaUnidades) + (mvarVolumenEmp * CantidadSolicitadaEmpaques)
End Property

Property Get VolumenUnitarioItemAsignado() As Double
    VolumenUnitarioItemAsignado = (mvarVolumen * CantidadAsignadaItem)
End Property

Property Get VolumenItemAsignado() As Double
    VolumenItemAsignado = (mvarVolumen * CantidadAsignadaUnidades) + (mvarVolumenEmp * CantidadAsignadaEmpaques)
End Property

Property Get PesoUnidad() As Double
    PesoUnidad = mvarPeso
End Property

Property Get PesoEmpaque() As Double
    PesoEmpaque = mvarPesoEmp
End Property

Property Get PesoUnitarioItemSolicitud() As Double
    PesoUnitarioItemSolicitud = (mvarPeso * CantidadSolicitadaItem)
End Property

Property Get PesoItemSolicitud() As Double
    PesoItemSolicitud = (mvarPeso * CantidadSolicitadaUnidades) + (mvarPesoEmp * CantidadSolicitadaEmpaques)
End Property

Property Get PesoUnitarioItemAsignado() As Double
    PesoUnitarioItemAsignado = (mvarPeso * CantidadAsignadaItem)
End Property

Property Get PesoItemAsignado() As Double
    PesoItemAsignado = (mvarPeso * CantidadAsignadaUnidades) + (mvarPesoEmp * CantidadAsignadaEmpaques)
End Property

'*********************************** Constructor *************************************************************************

Public Function AgregarLineaItemDocumento(pRsPed As ADODB.Recordset, _
pLin As Long, pCab As ClsTrans_Cab) As Boolean
    
    'On Error GoTo 0
    
    Dim mDesc As Double
    
    ' Propiedades del producto
    
    mvarCodigoItem = pRsPed!c_CodArticulo
    mvarDescripcionItem = pRsPed!c_Descri
    mvarPresentacionItem = IIf(pRsPed!n_CantiBul > 0, pRsPed!n_CantiBul, 1)
    mvarPorcImpuestoItem = pRsPed!Impuesto1
    mvarNumeroDecimales = pRsPed!Cant_Decimales
    
    mvarMonedaItem = pRsPed!CodMonProd ' Aplicando la misma l�gica que en factura.
    
    Set MonedaProd = FrmAppLink.ClaseMonedaProducto
    
    If Not MonedaProd.BuscarMonedas(, mvarMonedaItem) Then
        FrmAppLink.ValidarConexion Ent.BDD
        If Not MonedaProd.BuscarMonedas(, mvarMonedaItem) Then
            Err.Raise 999, , "No se pudo cargar datos de la moneda para el producto " & mvarCodigoItem & ""
        End If
    End If
    
    mvarFactorMonedaItem = MonedaProd.FacMoneda
    
    mvarCostoDoc = pRsPed!n_CostoDoc
    mvarCantibulDoc = IIf(pRsPed!n_CantiBul_Doc > 0, pRsPed!n_CantiBul_Doc, 1)
    
    'Select Case Ent.rsReglasComerciales!Estimacion_PV
    Select Case FrmAppLink.RsReglasComerciales!Estimacion_Inv
        Case 0
TipoCostoDefault:
            CampoC = "n_CostoAct"
        Case 1
            CampoC = "n_CostoAnt"
        Case 2
            CampoC = "n_CostoPro"
        'Case 3
            'CampoC = "n_CostoRep"
        Case Else
            GoTo TipoCostoDefault
    End Select
    
    mvarCosto = (pRsPed.Fields(CampoC).Value * pCab.Factor)
    mvarPrecio = (pRsPed!n_Precio1 * pCab.Factor) 'MonedaDoc.FacMoneda
    mvarCantidadSolicitada = pRsPed!Cantidad
    mvarTipoProducto = pRsPed!n_TipoPeso
    
    ' Propiedades documento relacion
    
    mvarDocumentoRel = pRsPed!c_Documento
    mvarTipoDocumentoRel = pRsPed!c_Concepto
    mvarLineaDocumentoRel = pLin
    mvarIdItem = pRsPed!ID
    mvarPoseeInv = False
    
    mvarVolumen = pRsPed!n_Volumen
    mvarVolumenEmp = pRsPed!n_VolBul
    mvarPeso = pRsPed!n_Peso
    mvarPesoEmp = pRsPed!n_PesoBul
    
    AgregarLineaItemDocumento = True
    
End Function
