VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmTransferencia_ConsolaGestion_Inv 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6840
   ClientLeft      =   15
   ClientTop       =   60
   ClientWidth     =   9465
   ControlBox      =   0   'False
   Icon            =   "FrmTransferencia_ConsolaGestion_Inv.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6840
   ScaleWidth      =   9465
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_salir 
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   4800
      Picture         =   "FrmTransferencia_ConsolaGestion_Inv.frx":74F2
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   5520
      Width           =   1215
   End
   Begin VB.CommandButton CmdUbicaciones 
      Appearance      =   0  'Flat
      Caption         =   "Ubicaci�n"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   3360
      Picture         =   "FrmTransferencia_ConsolaGestion_Inv.frx":9274
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Importa los productos a su planilla de trabajo"
      Top             =   5520
      Width           =   1215
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   11955
         TabIndex        =   6
         Top             =   120
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Disponibilidad Inventario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   75
         Width           =   6015
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   8640
         Picture         =   "FrmTransferencia_ConsolaGestion_Inv.frx":AFF6
         Top             =   0
         Width           =   480
      End
   End
   Begin MSComctlLib.ListView lv 
      Height          =   3735
      Left            =   195
      TabIndex        =   0
      Top             =   1560
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   6588
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   4210752
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Documento"
         Object.Width           =   4057
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   1
         Text            =   "Disponible"
         Object.Width           =   3704
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Text            =   "Cantidad"
         Object.Width           =   3704
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Text            =   "Saldo"
         Object.Width           =   3704
      EndProperty
   End
   Begin VB.Label lblDisp 
      Caption         =   "Stock Actual: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   255
      Left            =   6000
      TabIndex        =   3
      Top             =   1080
      Width           =   3135
   End
   Begin VB.Label lblDep 
      Caption         =   "Dep�sito: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   1080
      Width           =   5535
   End
   Begin VB.Label lblPro 
      Caption         =   "Producto: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   600
      Width           =   8895
   End
End
Attribute VB_Name = "FrmTransferencia_ConsolaGestion_Inv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As ClsTrans_Gestion

Private Declare Function SendMessage Lib "user32" _
Alias "SendMessageA" (ByVal hWnd As Long, _
ByVal wMsg As Long, _
ByVal wParam As Long, _
lParam As Any) As Long

Private Declare Sub ReleaseCapture Lib "user32" ()
Const WM_NCLBUTTONDOWN = &HA1
Const HTCAPTION = 2

Private Sub cmd_salir_Click()
    Exit_Click
End Sub

Private Sub CmdUbicaciones_Click()
    
    If Not lv.SelectedItem Is Nothing Then
        
        Dim mMask As Object 'cls_MascarasUbicacion
        
        Set mMask = FrmAppLink.GetClassMascaraUbicacion 'New cls_MascarasUbicacion
        
        mMask.ConsultaUbicacion = True
        mMask.UbicxProducto = True
        mMask.CodigoUbicar = Me.fCls.ItemSeleccionado.CodigoItem
        mMask.DepositoFiltrar = Me.fCls.DocumentoSeleccionado.CodDepositoOrigen
        
        Call mMask.UbicacionInterfaz(Ent.BDD)
        
        Set mMask = Nothing
        
    End If
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Frame5_MouseMove(Button As Integer, Shift As Integer, _
X As Single, Y As Single)
    
    On Error Resume Next
    
    Dim lngReturnValue As Long
    
    If Button = 1 Then
        Call ReleaseCapture
        lngReturnValue = SendMessage( _
        Me.hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0&)
    End If
    
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Frame5_MouseMove Button, Shift, X, Y
End Sub

Private Sub Form_Load()
    fCls.LlenarDocumentosDetInv lv, lblPro, lblDep, lblDisp
End Sub
