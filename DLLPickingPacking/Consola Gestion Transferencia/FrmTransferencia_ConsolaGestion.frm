VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form FrmTransferencia_ConsolaGestion 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   120
   ClientTop       =   60
   ClientWidth     =   15330
   ControlBox      =   0   'False
   Icon            =   "FrmTransferencia_ConsolaGestion.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   Begin VB.Frame FrameTransferencias 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8655
      Left            =   120
      TabIndex        =   37
      Top             =   2160
      Width           =   15135
      Begin VB.Frame FrameAccionesGrid 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Frame7"
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   0
         TabIndex        =   75
         Top             =   8040
         Width           =   15015
         Begin VB.CheckBox CmdAccion5 
            BackColor       =   &H00AE5B00&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   630
            Left            =   11880
            Style           =   1  'Graphical
            TabIndex        =   13
            Top             =   0
            Width           =   2750
         End
         Begin VB.CheckBox CmdAccion4 
            BackColor       =   &H00AE5B00&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   630
            Left            =   9000
            Style           =   1  'Graphical
            TabIndex        =   12
            Top             =   0
            Width           =   2750
         End
         Begin VB.CheckBox CmdAccion3 
            BackColor       =   &H00AE5B00&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   630
            Left            =   6120
            Style           =   1  'Graphical
            TabIndex        =   11
            Top             =   0
            Width           =   2750
         End
         Begin VB.CheckBox CmdAccion2 
            BackColor       =   &H00AE5B00&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   630
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   10
            Top             =   0
            Width           =   2750
         End
         Begin VB.CheckBox CmdAccion1 
            BackColor       =   &H00AE5B00&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   630
            Left            =   360
            Style           =   1  'Graphical
            TabIndex        =   9
            Top             =   0
            Width           =   2750
         End
      End
      Begin VB.Frame FrameDatosTransferencias 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Frame7"
         ForeColor       =   &H80000008&
         Height          =   855
         Left            =   0
         TabIndex        =   47
         Top             =   1560
         Width           =   15015
         Begin VB.CommandButton CmdLocalidades 
            CausesValidation=   0   'False
            Height          =   480
            Left            =   14400
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":74F2
            Style           =   1  'Graphical
            TabIndex        =   80
            Top             =   240
            Width           =   480
         End
         Begin VB.TextBox txtLocalidades 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   10920
            Locked          =   -1  'True
            TabIndex        =   7
            Top             =   240
            Width           =   3375
         End
         Begin VB.OptionButton OptFechaVer 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "F. Despacho"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   1
            Left            =   3360
            TabIndex        =   6
            Top             =   480
            Width           =   1380
         End
         Begin VB.OptionButton OptFechaVer 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "F. Emisi�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   0
            Left            =   3360
            TabIndex        =   5
            Top             =   120
            Value           =   -1  'True
            Width           =   1380
         End
         Begin VB.Frame FrameBuscar 
            Appearance      =   0  'Flat
            BackColor       =   &H00AE5B00&
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            ForeColor       =   &H0000C000&
            Height          =   495
            Left            =   5040
            TabIndex        =   49
            Top             =   120
            Width           =   4455
            Begin VB.Label lblBuscarSolicitudes 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "Buscar Solicitudes (Alt - B)"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FAFAFA&
               Height          =   255
               Left            =   240
               TabIndex        =   50
               Top             =   120
               Width           =   3975
            End
         End
         Begin VB.TextBox txtMaxRegistros 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   510
            Left            =   990
            TabIndex        =   4
            Top             =   120
            Width           =   900
         End
         Begin VB.Label lblLocalidad 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Localidades"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   9600
            TabIndex        =   79
            Top             =   285
            Width           =   1215
         End
         Begin VB.Label lblFechaMostrar 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Mostrar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   2040
            TabIndex        =   78
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label lblMax 
            BackStyle       =   0  'Transparent
            Caption         =   "Top"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   480
            TabIndex        =   48
            Top             =   225
            Width           =   615
         End
      End
      Begin VB.Frame FrameFechas 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Frame7"
         ForeColor       =   &H80000008&
         Height          =   1575
         Left            =   2040
         TabIndex        =   38
         Top             =   0
         Width           =   10695
         Begin VB.TextBox TextFechaDesde 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   480
            TabIndex        =   0
            Top             =   960
            Width           =   2055
         End
         Begin VB.TextBox TextFechaHasta 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   2760
            TabIndex        =   1
            Top             =   960
            Width           =   2175
         End
         Begin VB.TextBox TextFechaDesdeRes 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   5280
            TabIndex        =   2
            Top             =   960
            Width           =   2415
         End
         Begin VB.TextBox TextFechaHastaRes 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   7920
            TabIndex        =   3
            Top             =   960
            Width           =   2535
         End
         Begin MSComCtl2.DTPicker desde 
            Height          =   315
            Left            =   480
            TabIndex        =   39
            Top             =   960
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarForeColor=   5790296
            CalendarTitleForeColor=   5790296
            Format          =   134152193
            CurrentDate     =   40709
         End
         Begin MSComCtl2.DTPicker hasta 
            Height          =   315
            Left            =   2760
            TabIndex        =   40
            Top             =   960
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarForeColor=   5790296
            CalendarTitleForeColor=   5790296
            Format          =   134152193
            CurrentDate     =   40709
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Desde"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   240
            TabIndex        =   46
            Top             =   600
            Width           =   2295
         End
         Begin VB.Label Label2 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Hasta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   2760
            TabIndex        =   45
            Top             =   600
            Width           =   2295
         End
         Begin VB.Label Label11 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Desde"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   5520
            TabIndex        =   44
            Top             =   600
            Width           =   2295
         End
         Begin VB.Label Label12 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Hasta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   8040
            TabIndex        =   43
            Top             =   600
            Width           =   2415
         End
         Begin VB.Label Label13 
            BackStyle       =   0  'Transparent
            Caption         =   "Fecha de Emision"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   255
            Left            =   1680
            TabIndex        =   42
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label Label14 
            BackStyle       =   0  'Transparent
            Caption         =   "Fecha de Despacho"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   255
            Left            =   6960
            TabIndex        =   41
            Top             =   240
            Width           =   1935
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grd 
         Height          =   5415
         Left            =   0
         TabIndex        =   8
         Top             =   2520
         Width           =   15015
         _ExtentX        =   26485
         _ExtentY        =   9551
         _Version        =   393216
         FixedCols       =   0
         RowHeightMin    =   400
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         WordWrap        =   -1  'True
         AllowBigSelection=   0   'False
         GridLinesFixed  =   0
         SelectionMode   =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Timer TmrHideActions 
      Interval        =   350
      Left            =   9720
      Top             =   1440
   End
   Begin VB.Frame TabDatosLote 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   3840
      TabIndex        =   73
      Top             =   1560
      Width           =   3150
      Begin VB.Label lblTabDatosLote 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Datos del Lote (Alt - L)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   120
         TabIndex        =   74
         Top             =   90
         Width           =   2895
      End
   End
   Begin VB.Frame TabTransferencias 
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   120
      TabIndex        =   71
      Top             =   1560
      Width           =   3150
      Begin VB.Label lblTabTransferencias 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Transferencias (Alt - P)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   120
         TabIndex        =   72
         Top             =   90
         Width           =   2900
      End
   End
   Begin VB.Frame Frame6 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   34
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmTransferencia_ConsolaGestion.frx":7CF4
         Top             =   -40
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12675
         TabIndex        =   36
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   35
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   15705
      TabIndex        =   32
      Top             =   360
      Width           =   15735
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   0
         TabIndex        =   33
         Top             =   120
         Width           =   14955
         _ExtentX        =   26379
         _ExtentY        =   1429
         ButtonWidth     =   2011
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   10
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Buscar"
               Key             =   "Buscar"
               Object.ToolTipText     =   "F2 Buscar"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Grabar"
               Key             =   "Grabar"
               Object.ToolTipText     =   "F4 Grabar"
               ImageIndex      =   2
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   1
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "gfactura"
                     Text            =   "Ejecutar"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Anular"
               Key             =   "anular"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Cancelar"
               Key             =   "Cancelar"
               Object.ToolTipText     =   "F7 Cancelar"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Reimprimir"
               Key             =   "Reimprimir"
               Object.ToolTipText     =   "F8 Reimprimir"
               ImageIndex      =   5
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   6
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "RFac"
                     Text            =   "Lote de Solicitudes - Resumido"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "RDet"
                     Text            =   "Lote de Solicitudes - Detallado"
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "RPicking"
                     Text            =   "Picking Acumulado de Productos"
                  EndProperty
                  BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PickingResFac"
                     Text            =   "Lote de Transferencias - Resumido"
                  EndProperty
                  BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PickingDetFac"
                     Text            =   "Lote de Transferencias - Detallado"
                  EndProperty
                  BeginProperty ButtonMenu6 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "LT_CON_TRA_X_LOC"
                     Text            =   "Lote de Transferencias Agrupado"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "F12 Salir"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Leyenda"
               Key             =   "Leyenda"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Object.Visible         =   0   'False
               Caption         =   "Separar P."
               Key             =   "Separar"
               ImageIndex      =   9
            EndProperty
         EndProperty
      End
   End
   Begin VB.CommandButton cmd 
      Height          =   340
      Left            =   0
      Picture         =   "FrmTransferencia_ConsolaGestion.frx":9A76
      Style           =   1  'Graphical
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   1440
      Visible         =   0   'False
      Width           =   360
   End
   Begin VB.PictureBox Picture2 
      Height          =   255
      Left            =   0
      Picture         =   "FrmTransferencia_ConsolaGestion.frx":9E00
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   30
      Top             =   10080
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture1 
      Height          =   255
      Left            =   0
      Picture         =   "FrmTransferencia_ConsolaGestion.frx":AACA
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   29
      Top             =   9840
      Visible         =   0   'False
      Width           =   255
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   -240
      Top             =   1800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":B794
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":D526
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":F2B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":1104A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":12DDC
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":14B6E
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":16900
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":1C902
            Key             =   "Info"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":1D5DC
            Key             =   "Separar"
         EndProperty
      EndProperty
   End
   Begin VB.Frame FrameDatosLote 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8415
      Left            =   240
      TabIndex        =   51
      Top             =   2280
      Width           =   15015
      Begin VB.Frame FrameTransporte 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   8175
         Left            =   120
         TabIndex        =   52
         Top             =   120
         Width           =   14775
         Begin VB.ComboBox CmbClasificacion 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8400
            Style           =   2  'Dropdown List
            TabIndex        =   27
            Top             =   5520
            Width           =   3615
         End
         Begin VB.TextBox txtObs 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   900
            Left            =   6960
            MultiLine       =   -1  'True
            TabIndex        =   28
            Top             =   6585
            Width           =   7545
         End
         Begin VB.Frame Frame5 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            BorderStyle     =   0  'None
            Caption         =   "Diferencias"
            ForeColor       =   &H80000008&
            Height          =   1695
            Left            =   360
            TabIndex        =   67
            Top             =   6360
            Width           =   6250
            Begin VB.Label Label21 
               BackStyle       =   0  'Transparent
               Caption         =   "Diferencias"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00AE5B00&
               Height          =   255
               Left            =   120
               TabIndex        =   70
               Top             =   0
               Width           =   2055
            End
            Begin VB.Label lblvolumenDespacho 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   390
               Left            =   3120
               TabIndex        =   26
               Top             =   720
               Width           =   3130
            End
            Begin VB.Label lblPesoDespacho 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   390
               Left            =   3120
               TabIndex        =   25
               Top             =   240
               Width           =   3130
            End
            Begin VB.Label lblPesoD 
               BackStyle       =   0  'Transparent
               Caption         =   "Peso"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   375
               Left            =   120
               TabIndex        =   69
               Top             =   360
               Width           =   1695
            End
            Begin VB.Label lblvolumenD 
               BackStyle       =   0  'Transparent
               Caption         =   "Volumen"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   375
               Left            =   120
               TabIndex        =   68
               Top             =   840
               Width           =   1695
            End
         End
         Begin VB.Frame Frame4 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            BorderStyle     =   0  'None
            Caption         =   "Totales Pedido"
            ForeColor       =   &H80000008&
            Height          =   1695
            Left            =   360
            TabIndex        =   63
            Top             =   4800
            Width           =   6250
            Begin VB.Label Label20 
               BackStyle       =   0  'Transparent
               Caption         =   "Totales Pedido"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00AE5B00&
               Height          =   255
               Left            =   120
               TabIndex        =   66
               Top             =   0
               Width           =   2055
            End
            Begin VB.Label lblvolumenPedido 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   390
               Left            =   3120
               TabIndex        =   24
               Top             =   720
               Width           =   3130
            End
            Begin VB.Label lblPesoPedido 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   390
               Left            =   3120
               TabIndex        =   23
               Top             =   240
               Width           =   3130
            End
            Begin VB.Label lblvolumenP 
               BackStyle       =   0  'Transparent
               Caption         =   "Volumen"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   375
               Left            =   120
               TabIndex        =   65
               Top             =   840
               Width           =   1815
            End
            Begin VB.Label lblPesoP 
               BackStyle       =   0  'Transparent
               Caption         =   "Peso"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   375
               Left            =   120
               TabIndex        =   64
               Top             =   360
               Width           =   2295
            End
         End
         Begin VB.Frame Frame3 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            BorderStyle     =   0  'None
            Caption         =   "Capacidades Vehiculo"
            ForeColor       =   &H80000008&
            Height          =   1695
            Left            =   360
            TabIndex        =   58
            Top             =   2760
            Width           =   6250
            Begin VB.Label Label19 
               BackStyle       =   0  'Transparent
               Caption         =   "Datos del Vehiculo"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00AE5B00&
               Height          =   255
               Left            =   120
               TabIndex        =   62
               Top             =   0
               Width           =   2055
            End
            Begin VB.Label lblPlacaVolumenVeh 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   390
               Left            =   3120
               TabIndex        =   21
               Top             =   720
               Width           =   3130
            End
            Begin VB.Label lblPlacaPesoVeh 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   390
               Left            =   3120
               TabIndex        =   20
               Top             =   240
               Width           =   3130
            End
            Begin VB.Label Label9 
               BackStyle       =   0  'Transparent
               Caption         =   "Placa"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   375
               Left            =   120
               TabIndex        =   61
               Top             =   1320
               Width           =   2655
            End
            Begin VB.Label lblPlaca 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   390
               Left            =   3120
               TabIndex        =   22
               Top             =   1200
               Width           =   3130
            End
            Begin VB.Label lblPesoV 
               BackStyle       =   0  'Transparent
               Caption         =   "Peso"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   375
               Left            =   120
               TabIndex        =   60
               Top             =   360
               Width           =   2655
            End
            Begin VB.Label lblvolumenV 
               BackStyle       =   0  'Transparent
               Caption         =   "Volumen"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   375
               Left            =   120
               TabIndex        =   59
               Top             =   840
               Width           =   2655
            End
         End
         Begin VB.TextBox txtTransporte 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   480
            TabIndex        =   14
            Top             =   480
            Width           =   1815
         End
         Begin VB.CommandButton cmdTransporte 
            Height          =   390
            Left            =   2340
            Picture         =   "FrmTransferencia_ConsolaGestion.frx":1F36E
            Style           =   1  'Graphical
            TabIndex        =   15
            Top             =   480
            Width           =   390
         End
         Begin VB.Label lblClasificacion 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Clasificaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   6960
            TabIndex        =   77
            Top             =   5550
            Width           =   1170
         End
         Begin VB.Label lblObs 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Observaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Left            =   6960
            TabIndex        =   76
            Top             =   6240
            Width           =   1290
         End
         Begin VB.Line LnObs 
            BorderColor     =   &H00AE5B00&
            X1              =   8400
            X2              =   14500
            Y1              =   6360
            Y2              =   6360
         End
         Begin VB.Label Label5 
            BackStyle       =   0  'Transparent
            Caption         =   "Transporte"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   480
            TabIndex        =   57
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblResponsable 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   2760
            TabIndex        =   16
            Top             =   480
            Width           =   3855
         End
         Begin VB.Label lblMarca 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   480
            TabIndex        =   18
            Top             =   2160
            Width           =   2895
         End
         Begin VB.Label Label6 
            BackStyle       =   0  'Transparent
            Caption         =   "Marca"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   480
            TabIndex        =   56
            Top             =   1800
            Width           =   1695
         End
         Begin VB.Label Label7 
            BackStyle       =   0  'Transparent
            Caption         =   "Responsable"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   2760
            TabIndex        =   55
            Top             =   120
            Width           =   1695
         End
         Begin VB.Label Label8 
            BackStyle       =   0  'Transparent
            Caption         =   "Modelo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   3600
            TabIndex        =   54
            Top             =   1800
            Width           =   1695
         End
         Begin VB.Label lblModelo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   3600
            TabIndex        =   19
            Top             =   2160
            Width           =   3015
         End
         Begin VB.Label Label10 
            BackStyle       =   0  'Transparent
            Caption         =   "Descripcion"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   480
            TabIndex        =   53
            Top             =   960
            Width           =   1455
         End
         Begin VB.Label lblDescri 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   480
            TabIndex        =   17
            Top             =   1320
            Width           =   6135
         End
      End
   End
   Begin VB.Menu pmenu 
      Caption         =   "menu"
      Visible         =   0   'False
      Begin VB.Menu docpen 
         Caption         =   "Documentos Pendientes"
         Shortcut        =   ^P
         Visible         =   0   'False
      End
      Begin VB.Menu detalleped 
         Caption         =   "Ver Detalle"
         Shortcut        =   ^D
      End
      Begin VB.Menu auto 
         Caption         =   "Autorizar"
         Shortcut        =   ^A
      End
      Begin VB.Menu CompletarPre 
         Caption         =   "Completar Pago"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
      Begin VB.Menu RechazarPed 
         Caption         =   "Rechazar"
      End
      Begin VB.Menu VerObs 
         Caption         =   "Ver Observaci�n"
      End
      Begin VB.Menu NuevoLotePedidosListos 
         Caption         =   "Separar Pedidos Listos"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
      Begin VB.Menu NuevoLotePedidosPendientes 
         Caption         =   "Separar Pedidos con Pago Pendiente"
         Enabled         =   0   'False
         Shortcut        =   ^S
         Visible         =   0   'False
      End
   End
End
Attribute VB_Name = "FrmTransferencia_ConsolaGestion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As ClsTrans_Gestion
Public CantTransacciones As Long

Private WithEvents TooltipContainer                  As PictureBox
Attribute TooltipContainer.VB_VarHelpID = -1
Private WithEvents TooltipLabel                      As Label
Attribute TooltipLabel.VB_VarHelpID = -1

Private Arr_DirEntrega As Object
Private Arr_DirPickup As Object

Private Const mColorTabSel = &HC000&
Private Const mColorTab = &H808080

Private EventoProgramado As Boolean

Dim mClsGrupos As Object 'New cls_grupos

Private Arr_Localidades As Object

Public Property Let TooltipContainerObj(pValue As Object)
    Set TooltipContainer = pValue
End Property

Public Property Let TooltipLabelObj(pValue As Object)
    Set TooltipLabel = pValue
End Property

Private Sub CmdAccion1_Click()
    If CmdAccion1.Value = vbUnchecked Then Exit Sub
    SafeFocus grd
    TmrHideActions.Enabled = False
    EventoProgramado = True
    grd_MouseDown vbRightButton, 0, 0, 0
    CmdAccion1.Value = vbUnchecked
End Sub

Private Sub CmdAccion2_Click()
    If CmdAccion2.Value = vbUnchecked Then Exit Sub
    SafeFocus grd
    TmrHideActions.Enabled = False
    EventoProgramado = True
    grd_MouseDown vbRightButton, 0, 0, 0
    If detalleped.Enabled Then detalleped_Click
    CmdAccion2.Value = vbUnchecked
End Sub

Private Sub CmdAccion3_Click()
    If CmdAccion3.Value = vbUnchecked Then Exit Sub
    SafeFocus grd
    TmrHideActions.Enabled = False
    EventoProgramado = True
    grd_MouseDown vbRightButton, 0, 0, 0
    If Toolbar1.Buttons(3).ButtonMenus(1).Enabled Then
        On Error Resume Next
        Dim Exist As Boolean
        Exist = TooltipContainer.Visible
        If Exist Then
            FrmAppLink.ShowTooltip Empty, 10, 10, grd
            'TooltipContainer_MouseUp vbRightButton, 0, 0, 0
        Else
            If VerObs.Enabled Then VerObs_Click
        End If
    Else
        If auto.Enabled Then Auto_Click
    End If
    CmdAccion3.Value = vbUnchecked
End Sub

Private Sub CmdAccion4_Click()
    If CmdAccion4.Value = vbUnchecked Then Exit Sub
    SafeFocus grd
    TmrHideActions.Enabled = False
    EventoProgramado = True
    grd_MouseDown vbRightButton, 0, 0, 0
    If Toolbar1.Buttons(3).ButtonMenus(1).Enabled Then
        If CompletarPre.Enabled Then CompletarPre_Click
    Else
        On Error Resume Next
        Dim Exist As Boolean
        Exist = TooltipContainer.Visible
        If Exist Then
            FrmAppLink.ShowTooltip Empty, 10, 10, grd
            'TooltipContainer_MouseUp vbRightButton, 0, 0, 0
        Else
            If VerObs.Enabled Then VerObs_Click
        End If
    End If
    CmdAccion4.Value = vbUnchecked
End Sub

Private Sub CmdAccion5_Click()
    If CmdAccion5.Value = vbUnchecked Then Exit Sub
    SafeFocus grd
    TmrHideActions.Enabled = False
    EventoProgramado = True
    'If Toolbar1.Buttons(3).ButtonMenus(1).Enabled Then
    'Else
        If RechazarPed.Visible And RechazarPed.Enabled Then
            RechazarPed_Click
        End If
    'End If
    CmdAccion5.Value = vbUnchecked
End Sub

Private Sub grd_Click()
    
    If grd.MouseRow = 0 Then
        
        Select Case grd.MouseCol
            
            Case eColGridSolicitudTrans.eColTransFechaMostrar, _
            eColGridSolicitudTrans.eColTransLocalidadDestino, _
            eColGridSolicitudTrans.eColTransNumDoc, _
            eColGridSolicitudTrans.eColTransTotalEmp, _
            eColGridSolicitudTrans.eColTransTotalUni, _
            eColGridSolicitudTrans.eColTransTotalVol, _
            eColGridSolicitudTrans.eColTransTotalPeso
            
            TmpCol = grd.MouseCol
            
            If TmpCol <> fCls.mOrderByGrid_ColAct Then
                fCls.mOrderByGrid_Mode = 0
            Else
                fCls.mOrderByGrid_Mode = IIf(fCls.mOrderByGrid_Mode = 0, 1, 0)
            End If
            
            fCls.mOrderByGrid_ColAnt = fCls.mOrderByGrid_ColAct
            fCls.mOrderByGrid_ColAct = TmpCol
            
            grd.TextMatrix(0, fCls.mOrderByGrid_ColAct) = fCls.eColGridTrans_Nombre(fCls.mOrderByGrid_ColAct) & _
            IIf(fCls.mOrderByGrid_Mode = 0, " (�)", " (�)")
            
            If fCls.mOrderByGrid_ColAct <> fCls.mOrderByGrid_ColAnt Then
                grd.TextMatrix(0, fCls.mOrderByGrid_ColAnt) = fCls.eColGridTrans_Nombre(fCls.mOrderByGrid_ColAnt)
                'grd.Col = fCls.mOrderByGrid_ColAct
            End If
            
            lblBuscarSolicitudes_Click
            
        End Select
        
        Exit Sub
        
    End If
    
End Sub

Private Sub grd_EnterCell()
    
    Dim mDoc As ClsTrans_Cab
    
    On Error GoTo Errores
    
    If grd.Row > 0 Then
        
        If grd.TextMatrix(grd.Row, eColGridSolicitudTrans.eColTransNumDoc) = Empty Then
            GoTo EmptyGrid
        End If
        
        Set mDoc = fCls.Documento(grd.Row)
        
        CmdAccion1.Enabled = False 'Not mDoc.PoseeCredito
        CmdAccion1.Caption = "Doc. Pendientes"
        CmdAccion1.Visible = False
        
        If Toolbar1.Buttons(3).ButtonMenus(1).Enabled Then
            CmdAccion3.Enabled = False 'Not mDoc.Seleccionado ' And Not mDoc.PoseeCredito
            auto.Tag = 1
        Else
            CmdAccion3.Enabled = True
            auto.Tag = IIf(mDoc.Seleccionado, 1, 0)
        End If
        
        If Val(auto.Tag) = 1 Then
            CmdAccion3.Caption = "Desmarcar"
        Else
            CmdAccion3.Caption = "Autorizar"
        End If
        
        CmdAccion2.Enabled = True
        CmdAccion2.Caption = "Ver Detalle"
        CmdAccion2.Visible = True
        
        If Toolbar1.Buttons(3).ButtonMenus(1).Enabled Then
            
            CmdAccion3.Caption = "Observaci�n"
            CmdAccion3.Enabled = True
            CmdAccion3.Visible = True
            
            CmdAccion4.Caption = "Completar Pre."
            
            'If Not mDoc.PagoCompletado Then
                'CmdAccion4.Enabled = True
                'RechazarPed.Enabled = True
                'NuevoLotePedidosPendientes.Visible = True: NuevoLotePedidosPendientes.Enabled = True
            'Else
                CmdAccion4.Enabled = False
                RechazarPed.Enabled = False
                NuevoLotePedidosPendientes.Visible = False: NuevoLotePedidosPendientes.Enabled = False
            'End If
            
            Toolbar1.Buttons("Separar").Enabled = NuevoLotePedidosPendientes.Visible
            
            CmdAccion4.Visible = CmdAccion4.Enabled
            
            CmdAccion5.Enabled = RechazarPed.Enabled
            CmdAccion5.Visible = CmdAccion5.Enabled
            CmdAccion5.Caption = "Rechazar Ped."
            
            NuevoLotePedidosListos.Visible = False: NuevoLotePedidosListos.Enabled = False ' TO DO ...
            
        Else
            
            CmdAccion3.Visible = True
            
            CmdAccion4.Caption = "Observaci�n"
            CmdAccion4.Enabled = True
            CmdAccion4.Visible = True
            
            CmdAccion5.Enabled = RechazarPed.Visible
            CmdAccion5.Visible = CmdAccion5.Enabled
            CmdAccion5.Caption = "Rechazar Ped."
            
        End If
        
        If mDoc.LineasDocumento.Count = 0 Then
EmptyGrid:
            CmdAccion1.Enabled = False
            CmdAccion2.Enabled = False
            CmdAccion3.Enabled = False
            CmdAccion4.Enabled = False
            CmdAccion5.Enabled = False
        End If
        
        'If Not CmdAccion1.Enabled Then CmdAccion1.Caption = Empty
        'If Not CmdAccion2.Enabled Then CmdAccion2.Caption = Empty
        'If Not CmdAccion3.Enabled Then CmdAccion3.Caption = Empty
        'If Not CmdAccion4.Enabled Then CmdAccion4.Caption = Empty
        'If Not CmdAccion5.Enabled Then CmdAccion5.Caption = Empty
        
        If Not CmdAccion1.Enabled Then CmdAccion1.Visible = False
        If Not CmdAccion2.Enabled Then CmdAccion2.Visible = False
        If Not CmdAccion3.Enabled Then CmdAccion3.Visible = False
        If Not CmdAccion4.Enabled Then CmdAccion4.Visible = False
        If Not CmdAccion5.Enabled Then CmdAccion5.Visible = False
        
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
End Sub

Private Sub grd_GotFocus()
    MostrarFrameAccionesGrid
End Sub

Private Sub grd_LostFocus()
    TmrHideActions.Enabled = True
End Sub

Private Sub lblTabDatosLote_Click()
    FrameDatosLote.Visible = True
    TabDatosLote.BackColor = mColorTabSel
    FrameTransferencias.Visible = False
    TabTransferencias.BackColor = mColorTab
    FrameDatosLote.ZOrder
End Sub

Private Sub lblTabTransferencias_Click()
    FrameTransferencias.Visible = True
    TabTransferencias.BackColor = mColorTabSel
    FrameDatosLote.Visible = False
    TabDatosLote.BackColor = mColorTab
    FrameTransferencias.ZOrder
End Sub

Private Sub TabDatosLote_Click()
    lblTabDatosLote_Click
End Sub

Private Sub TabTransferencias_Click()
    lblTabTransferencias_Click
End Sub

Private Sub TextFechaDesde_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        TextFechaDesde_Click
    ElseIf KeyCode = vbKeyDelete Or KeyCode = vbKeyBack Or KeyCode = vbKeyEscape Then
        TextFechaDesde.Text = Empty
        TextFechaDesde.Tag = TextFechaDesde.Text
    End If
End Sub

Private Sub TextFechaDesdeRes_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        TextFechaDesdeRes_Click
    End If
End Sub

Private Sub TextFechaHasta_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        TextFechaHasta_Click
    End If
End Sub

Private Sub TextFechaHastaRes_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        TextFechaHastaRes_Click
    End If
End Sub

Private Sub TmrHideActions_Timer()
    If TmrHideActions.Enabled Then
        TmrHideActions.Enabled = False
        OcultarFrameAccionesGrid
    End If
End Sub

' Si se arrastra, el usuario decidir� cuando desaparecerla.

Private Sub TooltipContainer_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
    If TooltipContainer.Tag <> "[HIDING]" Then
        If Source Is TooltipContainer Then
            If GlobalEventTimer.Enabled Then GlobalEventTimer.Enabled = False
        End If
    End If
    Debug.Print X, Y
End Sub

' Al Click Derecho, desaparecer el Tooltip immediatamente.

Private Sub TooltipContainer_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error Resume Next
    If TooltipContainer.Tag <> "[HIDING]" Then
        If Button = vbRightButton Then
            GlobalEventTimer.Enabled = False
            GlobalEventTimer.Interval = 100
            GlobalEventTimer.Enabled = True
        End If
    End If
End Sub

Private Sub TooltipLabel_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If TooltipContainer.Tag <> "[HIDING]" Then
        TooltipContainer_MouseUp Button, Shift, X, Y
    End If
End Sub

' Doble Click: Copiar el Texto del Tooltip.

Private Sub TooltipContainer_DblClick()
    If TooltipContainer.Tag <> "[HIDING]" Then
        CtrlC TooltipContainer.Container.Controls("TooltipLabel").Caption
        TooltipContainer.Container.Controls("TooltipLabel").Caption = "Copiado"
    End If
End Sub

Private Sub TooltipLabel_DblClick()
    If TooltipContainer.Tag <> "[HIDING]" Then
        TooltipContainer_DblClick
    End If
End Sub

Private Sub Auto_Click()
    
    If Val(auto.Tag) = 1 Then
        
        grd_KeyDown vbKeyDelete, 0
        
    Else
        
        Dim mNivel As Long
        Dim mFila As Long
        
        mNivel = Val(BuscarReglaNegocioStr("TRA_Consola_NivelAutorizarPicking", "5"))
        
        If mNivel > FrmAppLink.GetNivelUsuario Then
            If Not gRutinas.ShowAutorizaciones(CInt(mNivel), fCls.Conexion) Then
                Mensaje True, FrmAppLink.Stellar_Mensaje(11010) 'Usted no posee el nivel de usuario permitido para ejecutar esta Operaci�n.
                Exit Sub
            End If
        End If
        
        fCls.SeleccionarDocumento grd.Row, True, gRutinas.UsuarioAutorizo
        
        mFila = grd.Row
        
        Me.grd.Visible = False
        FrameAccionesGrid.Visible = False
        fCls.LlenarDocumentosGrd grd
        
        'Call AjustarColumnasDelGrid
        
        Me.grd.Visible = True
        FrameAccionesGrid.Visible = True
        
        DoEvents
        
        'grd_GotFocus
        
        SeleccionarFila mFila
        
        grd.Col = 0
        grd.ColSel = grd.Cols - 1
        
        CalculoTransporte
        
        gRutinas.UsuarioAutorizo = Empty
        gRutinas.DescriUsuarioAutorizo = Empty
        
        DoEvents
        EventoProgramado = True
        SafeFocus grd
        
        TmrHideActions.Enabled = False
        'MostrarFrameAccionesGrid
        FrameAccionesGrid.Top = 8040
        EventoProgramado = False
        
    End If
    
End Sub

Private Sub CmdBuscar_Click()
    
    On Error GoTo ErrorAlBuscar
    
    If Trim(fCls.Corrida) <> Empty Then
        Form_KeyDown vbKeyF7, 0
    End If
    
    Me.grd.Visible = False
    FrameAccionesGrid.Visible = False
    fCls.IniciarGrid grd
    
    Dim TipoOrigenDoc As Integer
    
    'Select Case True
        'Case OptWeb.Value
            'TipoOrigenDoc = 1
        'Case OptVentas.Value
            'TipoOrigenDoc = 2
        'Case OptAmbos.Value
            'TipoOrigenDoc = 0
    'End Select
    
    Dim pOpcionFechaMostrar As Integer
    
    If OptFechaVer(0).Value Then
        pOpcionFechaMostrar = 0
    ElseIf OptFechaVer(1).Value Then
        pOpcionFechaMostrar = 1
    End If
    
    Dim MaxReg As Long
    Dim MaxLong As Long: MaxLong = ((2 ^ 32) / 2) - 1
    
    MaxReg = ValidarNumeroIntervalo(SVal(txtMaxRegistros.Text), MaxLong, 0)
    
    Select Case ValidarFechasEnviar( _
    TextFechaDesde.Text, TextFechaHasta.Text, _
    TextFechaDesdeRes.Text, TextFechaHastaRes.Text)
        
        Case 0
            
            If fCls.BuscarSolicitudes(TipoOrigenDoc, MaxReg, , , , , _
            pOpcionFechaMostrar, Arr_Localidades) Then
                If fCls.NumeroDocumentos > 0 Then
                    fCls.LlenarDocumentosGrd grd
                    EstadoForm eEstadoEditar
                Else
                    Mensaje True, "No se encontr� informaci�n."
                End If
            End If
            
        Case 1
            
            If fCls.BuscarSolicitudes(TipoOrigenDoc, MaxReg, _
            CDate(TextFechaDesde.Tag), CDate(TextFechaHasta.Tag), , , _
            pOpcionFechaMostrar, Arr_Localidades) Then
                If fCls.NumeroDocumentos > 0 Then
                    fCls.LlenarDocumentosGrd grd
                    EstadoForm eEstadoEditar
                Else
                    Mensaje True, "No se encontr� informaci�n."
                End If
            End If
            
        Case 2
            
            If fCls.BuscarSolicitudes(TipoOrigenDoc, MaxReg, , , _
            CDate(TextFechaDesdeRes.Tag), CDate(TextFechaHastaRes.Tag), _
            pOpcionFechaMostrar, Arr_Localidades) Then
                If fCls.NumeroDocumentos > 0 Then
                    fCls.LlenarDocumentosGrd grd
                    EstadoForm eEstadoEditar
                Else
                    Mensaje True, "No se encontr� informaci�n."
                End If
            End If
            
        Case 3
            
            If fCls.BuscarSolicitudes(TipoOrigenDoc, MaxReg, _
            CDate(TextFechaDesde.Tag), CDate(TextFechaHasta.Tag), _
            CDate(TextFechaDesdeRes.Tag), CDate(TextFechaHastaRes.Tag), _
            pOpcionFechaMostrar, Arr_Localidades) Then
                If fCls.NumeroDocumentos > 0 Then
                    fCls.LlenarDocumentosGrd grd
                    EstadoForm eEstadoEditar
                Else
                    Mensaje True, "No se encontr� informaci�n."
                End If
            End If
            
    End Select
    
    CalculoTransporte
    
    'Call AjustarColumnasDelGrid
    
    Me.grd.Visible = True
    FrameAccionesGrid.Visible = True
    
    If grd.Rows > 1 Then
        
        grd.Row = 1
        
        SafeFocus grd
        
        DoEvents
        
        TmrHideActions.Enabled = False
        
        grd_GotFocus
        
        TmrHideActions.Enabled = False
        
    End If
    
    RechazarPed.Enabled = CBool(Val(BuscarReglaNegocioStr("TRA_Consola_PermiteAnularSolicitud", "0")))
    RechazarPed.Visible = RechazarPed.Enabled
    
    Exit Sub
    
ErrorAlBuscar:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Buscar_Click)"
    
End Sub

Private Sub cmdTransporte_Click()
    
    Dim mCls As recsun.obj_busqueda
    Dim mBus As Variant
    Dim mResul As Variant
    
    mConsulta = _
    "Select c_CodTransporte, c_Descripcion, c_Marca, c_Modelo " & _
    "FROM MA_TRANSPORTE"
    
    Dim mFrm_Super_Consultas As Object
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With mFrm_Super_Consultas
        
        .Inicializar mConsulta, "Transporte", Ent.BDD
        
        .Add_ItemLabels "CODIGO", "c_CodTransporte", 1935, 0
        .Add_ItemLabels "DESCRIPCION", "c_Descripcion", 6000, 0
        .Add_ItemLabels "Marca", "c_Marca", 1500, 0
        .Add_ItemLabels "Modelo", "c_Modelo", 1500, 0
        
        .Add_ItemSearching "CODIGO", "c_CodTransporte"
        .Add_ItemSearching "DESCRIPCION", "c_Descripcion"
        .Add_ItemSearching "Marca", "c_Marca"
        .Add_ItemSearching "Modelo", "c_Modelo"
        
        .txtDato.Text = "%"
        .StrOrderBy = "c_Descripcion"
        
        .Show vbModal
        
        mResul = .ArrResultado()
        
        If Not IsEmpty(mResul) Then
            If Trim(mResul(0)) <> Empty Then
                Me.txtTransporte.Text = CStr(mResul(0))
                txtTransporte_LostFocus
            End If
        Else
            Me.txtTransporte.Text = Empty
            Me.lblResponsable = Empty
        End If
        
    End With
    
    Set mFrm_Super_Consultas = Nothing
    
End Sub

'Private Sub txtLocalidades_Click()
'
'    Dim mResult As Variant
'    Dim mLimpiar As Boolean
'
'    mResult = FrmAppLink.Buscar_Localidad
'    mLimpiar = True
'
'    If Not IsEmpty(mResult) Then
'        If Trim(mResult(0)) <> Empty Then
'            mLimpiar = False
'            txtLocalidades.Tag = mResult(0)
'            txtLocalidades.Text = mResult(1)
'        End If
'    End If
'
'    If mLimpiar Then
'        txtLocalidades.Tag = Empty
'        txtLocalidades.Text = Empty
'    End If
'
'End Sub

'Private Sub txtLocalidades_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyReturn Or KeyCode = vbKeyF2 Then
'        txtLocalidades_Click
'    ElseIf KeyCode = vbKeyDelete Or KeyCode = vbKeyBack Then
'        txtLocalidades.Text = Empty
'        txtLocalidades.Tag = Empty
'        Set Arr_Localidades = Nothing
'    End If
'End Sub

Private Sub txtLocalidades_Click()
    
    If Arr_Localidades Is Nothing Then
        
        CmdLocalidades_Click
        Exit Sub
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_Localidades
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "Lista de Localidades: " & GetLines(2) & ListaStr & "", 2500, 4000, txtLocalidades, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "8", True), , , 200
            
        End If
        
    End If
    
End Sub

Private Sub txtLocalidades_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
                
            Dim SQL As String
            
            SQL = "SELECT c_Codigo, c_Descripcion FROM MA_SUCURSALES "
            
            With FrmAppLink.GetFrmSuperConsultas
                
                .Inicializar SQL, StellarMensaje(1251), Ent.BDD, , True
                
                .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2000, 0
                .Add_ItemLabels StellarMensaje(143), "c_Descripcion", 9000, 0
                
                .Add_ItemSearching StellarMensaje(143), "c_Descripcion"
                .Add_ItemSearching StellarMensaje(142), "c_Codigo"
                
                .StrOrderBy = "c_Descripcion"
                
                .BusquedaInstantanea = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .ModoDespliegueInstantaneo = True
                
                .Show vbModal
                
                Set Arr_Localidades = .ArrResultado
                
                If Arr_Localidades Is Nothing Then
                    
                    txtLocalidades.Text = Empty
                    txtLocalidades.Tag = Empty
                    
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_Localidades
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txtLocalidades.Text = ListaStr
                    
                    txtLocalidades_Click
                    
                    lblBuscarSolicitudes_Click
                    
                End If
                
            End With
            
            Set Frm_Super_Consultas = Nothing
            
        Case vbKeyDelete, vbKeyBack
            
            txtLocalidades.Text = Empty
            txtLocalidades.Tag = Empty
            
            Set Arr_Localidades = Nothing
            
    End Select
    
End Sub

Private Sub CmdLocalidades_Click()
    Call txtLocalidades_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtMaxRegistros_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        lblBuscarSolicitudes_Click
    End If
End Sub

Private Sub VerObs_Click()
    
    On Error Resume Next
    
    Dim mDoc As ClsTrans_Cab
    
    Set mDoc = fCls.Documento(grd.Row)
    
    If mDoc Is Nothing Then
        Exit Sub
    End If
    
    FrmAppLink.ShowTooltip mDoc.Observacion, (grd.Width / 1.75), 10000, grd, 4, , _
    grd.ForeColor, , FrmAppLink.GetFont("Tahoma", 14)
    
    'FrmAppLink.ShowToast "Click Derecho para ocultar:", (grd.Width / 3), 2500, TooltipContainer, 1, , grd.ForeColor, , GetFont("Tahoma", 14), , -200
    
End Sub

Private Sub CompletarPre_Click()
    'N/A
End Sub

Private Sub NuevoLotePedidosPendientes_Click()
    'N/A
End Sub

Private Sub detalleped_Click()
    grd_KeyDown vbKeyD, vbCtrlMask
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    Toolbar1.Buttons("Reimprimir").ButtonMenus("RFac").Text = FrmAppLink.StellarMensaje(4057) ' Lote de Solicitudes Resumido
    Toolbar1.Buttons("Reimprimir").ButtonMenus("RDet").Text = FrmAppLink.StellarMensaje(4058) ' Lote de Solicitudes Detallado
    Toolbar1.Buttons("Reimprimir").ButtonMenus("RPicking").Text = FrmAppLink.StellarMensaje(411) ' Picking Acumulado por Producto
    Toolbar1.Buttons("Reimprimir").ButtonMenus("PickingResFac").Text = FrmAppLink.StellarMensaje(4059) ' Lote de Transferencias Resumido
    Toolbar1.Buttons("Reimprimir").ButtonMenus("PickingDetFac").Text = FrmAppLink.StellarMensaje(4060) ' Lote de Transferencias Detallado
    Toolbar1.Buttons("Reimprimir").ButtonMenus("LT_CON_TRA_X_LOC").Text = StellarMensaje(5572) '"Lote de Transferencias Agrupado"
    
    lblTabTransferencias.Caption = FrmAppLink.StellarMensaje(4035) & " (Alt - T)" ' Transferencias
    lblTabDatosLote.Caption = FrmAppLink.StellarMensaje(3166) & " (Alt - L)" ' Datos del Lote
    lblBuscarSolicitudes.Caption = StellarMensaje(8048) & " (Alt - B)" ' Buscar Solicitudes
    
    txtLocalidades.Enabled = True
    
    If txtLocalidades.Enabled Then
        'txtLocalidades.Text = Sucursal
        'Set Arr_Localidades = New Collection
        'Arr_Localidades.Add Array(Sucursal, BuscarNombreSucursal(Sucursal))
    End If
    
    Me.Toolbar1.Buttons(1).Caption = FrmAppLink.StellarMensaje(419) ' "Lotes"
    Me.lblFechaMostrar.Caption = FrmAppLink.StellarMensaje(10088) ' Mostrar
    
    Me.lbl_Organizacion.Caption = FrmAppLink.StellarMensaje(4052) '"C�nsola Gesti�n de Transferencias"
    lblLocalidad.Caption = StellarMensaje(8024) 'Localidades
    
    IniciarForm
    
    Call AjustarPantalla(Me)
    
    lblClasificacion.Caption = FrmAppLink.StellarMensaje(3168) 'Clasificacion
    lblObs.Caption = Replace(FrmAppLink.StellarMensaje(137), ":", Empty) ' Observacion
    
    EventoProgramado = True
    Set mClsGrupos = FrmAppLink.GetClassGrupo
    mClsGrupos.cTipoGrupo = "TRA_PICKING_CLS"
    mClsGrupos.CargarComboGrupos Ent.BDD, CmbClasificacion
    FrmAppLink.ListRemoveItems CmbClasificacion, "Ninguno" ' Quitar dichos elementos unicamente aqu� sin alterar la funci�n anterior.
    EventoProgramado = False
    
    lblTabTransferencias_Click
    
    SafeFocus TextFechaDesde
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case Shift
        
        Case vbAltMask
            
            Select Case KeyCode
                
                Case vbKeyB
                    
                    lblBuscarSolicitudes_Click
                    
                Case vbKeyL
                    
                    lblTabDatosLote_Click
                    
                Case vbKeyP
                    
                    lblTabTransferencias_Click
                    
                Case vbKeyS
                    
                    If NuevoLotePedidosPendientes.Enabled Then
                        NuevoLotePedidosPendientes_Click
                    End If
                    
            End Select
            
        Case Else
            
            Select Case KeyCode
                
                Case vbKeyF2
                    
                    If Toolbar1.Buttons(1).Enabled Then
                        
                        If Not Me.ActiveControl Is Nothing Then
                            
                            If Me.ActiveControl.Name = txtTransporte.Name Or _
                            Me.ActiveControl.Name = txtMaxRegistros.Name Then
                                Exit Sub
                            End If
                            
                        End If
                        
                        Me.grd.Visible = False
                        
                        buscar
                        
                        Me.grd.Visible = True
                        
                        If grd.Rows > 1 Then
                            grd.Row = 1
                            SafeFocus grd
                        End If
                        
                    End If
                    
                Case vbKeyF4
                    
                    Me.grd.Visible = False
                    
                    If Toolbar1.Buttons(3).ButtonMenus(1).Enabled Then
                        GrabarPicking True
                    Else
                        GrabarPicking False
                    End If
                    
                    'Call AjustarColumnasDelGrid
                    Me.grd.Visible = True
                    
                Case vbKeyF6
                    
                    If Toolbar1.Buttons(5).Enabled Then
                        Anular
                    End If
                    
                Case vbKeyF7
                    
                    IniciarForm
                    'Call AjustarColumnasDelGrid
                    
                Case vbKeyF8
                    
                    Reimprimir False, False
                    
                Case vbKeyF12
                    
                    Unload Me
                    
                Case vbKeyReturn
                    
                    SendKeys Chr(vbKeyTab)
                    
            End Select
            
    End Select
    
End Sub

Private Sub IniciarForm()
    
    desde.Value = Date
    hasta.Value = Date
    
    'txtRuta.Text = Empty
    'txtVendedor.Text = Empty
    'txtZona.Text = Empty
    txtMaxRegistros.Text = Empty
    
    ActivarControlesBusqueda True
    LimpiarControlesT
    
    fCls.IniciarGrid grd
    'Call AjustarColumnasDelGrid
    
    fCls.EstadoToolbar Toolbar1, eEstadoCancelar
    
    fCls.IniciarPropiedadesClase
    
    Me.TextFechaDesde.Text = Empty
    Me.TextFechaDesdeRes.Text = Empty
    Me.TextFechaHasta.Text = Empty
    Me.TextFechaHastaRes = Empty
    
    txtLocalidades.Tag = Empty
    txtLocalidades.Text = Empty
    Set Arr_Localidades = Nothing
    
    TabTransferencias_Click
    
    NuevoLotePedidosPendientes.Visible = False: NuevoLotePedidosPendientes.Enabled = False
    Toolbar1.Buttons("Separar").Enabled = False
    
    txtObs.Text = Empty
    FrmAppLink.ListSafeIndexSelection CmbClasificacion, 0
    
    SafeFocus Me
    
End Sub

Private Sub FrameBuscar_Click()
    Call CmdBuscar_Click
End Sub

Private Sub grd_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim mDoc As ClsTrans_Cab
    Dim mFila As Long
    
    On Error GoTo Errores
    
    If grd.Row > 0 Then
        
        Set mDoc = fCls.Documento(grd.Row)
        
        Select Case Shift
            Case vbAltMask
            Case vbShiftMask
            Case vbCtrlMask
                Select Case KeyCode
                    Case vbKeyA
                        If Toolbar1.Buttons(3).ButtonMenus(1).Enabled = False Then
                            'If Not mDoc.Seleccionado Then 'And Not mDoc.PoseeCredito Then
                                Auto_Click
                            'End If
                        End If
                    Case vbKeyD
                        MostrarInterfaz grd.Row, 1
                    Case vbKeyP
                        'If Not mDoc.PoseeCredito Then
                            'MostrarInterfaz grd.Row, 0
                        'End If
                End Select
            Case Else
                
                Select Case KeyCode
                    
                    Case vbKeyDelete
                        
                        If Toolbar1.Buttons(3).ButtonMenus(1).Enabled = False Then
                            
                            If mDoc.Seleccionado Then
                                
                                fCls.SeleccionarDocumento grd.Row, False
                                
                                mFila = grd.Row
                                grd.Visible = False
                                FrameAccionesGrid.Visible = False
                                
                                fCls.LlenarDocumentosGrd grd
                                
                                CalculoTransporte
                                
                                'AjustarColumnasDelGrid
                                
                                SeleccionarFila mFila
                                
                                grd.Col = 0
                                grd.ColSel = grd.Cols - 1
                                
                                DoEvents
                                
                                grd.Visible = True
                                TmrHideActions.Enabled = False
                                FrameAccionesGrid.Visible = True
                                
                                EventoProgramado = True
                                
                                SafeFocus grd
                                'grd_GotFocus
                                
                                DoEvents
                                
                                'MostrarFrameAccionesGrid
                                FrameAccionesGrid.Top = 8040
                                
                                EventoProgramado = False
                                
                            Else
                                
                                If RechazarPed.Visible And RechazarPed.Enabled Then
                                    RechazarPed_Click
                                End If
                                
                            End If
                            
                        End If
                        
                    Case vbKeySpace
                        
                        If Toolbar1.Buttons(3).ButtonMenus(1).Enabled = False Then
                            
                            'If Not mDoc.Seleccionado Then ' And mDoc.PoseeCredito Then
                                
                                'fCls.SeleccionarDocumento grd.Row, True, gRutinas.UsuarioAutorizo
                                
                                'mFila = grd.Row
                                'grd.Visible = False
                                
                                'fCls.LlenarDocumentosGrd grd
                                
                                'CalculoTransporte
                                
                                ''AjustarColumnasDelGrid
                                
                                'SeleccionarFila mFila
                                'grd.Visible = True
                                
                                'SafeFocus grd
                                
                            'ElseIf (mDoc.Seleccionado) Or (Not mDoc.Seleccionado) Then 'Not mDoc.Seleccionado And Not mDoc.PoseeCredito Then
                                
                                Auto_Click
                                
                            'End If
                            
                        End If
                        
                    Case 93, vbKeyMenu
                        
                        grd_MouseDown vbRightButton, 0, 0, 0
                        
                        PopupMenu pmenu
                        
                End Select
                
        End Select
        
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
    grd.Visible = True
    
End Sub

Private Sub SeleccionarFila(pFila As Long)
    
    If Not grd.RowIsVisible(pFila) Then
        grd.TopRow = pFila
    End If
    
    grd.Row = pFila
    grd.RowSel = pFila
    
End Sub

Private Sub grd_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Dim mDoc As ClsTrans_Cab
    
    On Error GoTo Errores
    
    If Button = vbRightButton And grd.Row > 0 Then
        
        Set mDoc = fCls.Documento(grd.Row)
        
        'docpen.Enabled = Not mDoc.PoseeCredito
        
        If Toolbar1.Buttons(3).ButtonMenus(1).Enabled Then
            auto.Enabled = Not mDoc.Seleccionado ' And Not mDoc.PoseeCredito
            auto.Tag = 1
        Else
            auto.Tag = IIf(mDoc.Seleccionado, 1, 0)
            auto.Enabled = True
        End If
        
        If Val(auto.Tag) = 1 Then
            auto.Caption = "Desmarcar"
        Else
            auto.Caption = "Autorizar"
        End If
        
        detalleped.Enabled = True
        
        If Toolbar1.Buttons(3).ButtonMenus(1).Enabled Then
            
            'If Not mDoc.PagoCompletado Then
                'CompletarPre.Visible = True: CompletarPre.Enabled = True
                'RechazarPed.Enabled = True
                'NuevoLotePedidosPendientes.Visible = True: NuevoLotePedidosPendientes.Enabled = True
            'Else
                CompletarPre.Visible = False: CompletarPre.Enabled = False
                RechazarPed.Enabled = Not mDoc.Seleccionado
                NuevoLotePedidosPendientes.Visible = False: NuevoLotePedidosPendientes.Enabled = False
            'End If
            
            NuevoLotePedidosListos.Visible = False: NuevoLotePedidosListos.Enabled = False ' TO DO ...
            
            Toolbar1.Buttons("Separar").Enabled = NuevoLotePedidosPendientes.Visible
            
        Else
            CompletarPre.Visible = False: CompletarPre.Enabled = False
            RechazarPed.Enabled = Not mDoc.Seleccionado
            NuevoLotePedidosListos.Visible = False: NuevoLotePedidosListos.Enabled = False
            NuevoLotePedidosPendientes.Visible = False: NuevoLotePedidosPendientes.Enabled = False
        End If
        
        If mDoc.LineasDocumento.Count = 0 Then
            RechazarPed.Enabled = False
            detalleped.Enabled = False
            auto.Enabled = False
            CompletarPre.Enabled = False
        End If
        
        'pmenu.Caption = mDoc.Documento & " - " & mDoc.Fecha
        If Not EventoProgramado Then
            Me.PopupMenu Me.pmenu
        Else
            EventoProgramado = False
        End If
        
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    
End Sub

Public Sub AnularPedido(ByVal pCorrelativo As String, Optional pEstatus = "DPE")
    
    Set VIEW_DOCUMENTOS = FrmAppLink.Preparar_MAKE_DOC( _
    "MA_REQUISICIONES", "cs_Documento", "cs_Deposito_Solicitud", "ds_Fecha", _
    UCase(FrmAppLink.StellarMensaje(1219)), Me, "REQUISICION", "REQ", "DPE", _
    True, False)
    
    On Error GoTo ErrorAnular
    
    If Not VIEW_DOCUMENTOS Is Nothing Then
        
        VIEW_DOCUMENTOS.txt_dato.Text = pCorrelativo
        VIEW_DOCUMENTOS.opt_cod = True
        
        VIEW_DOCUMENTOS.txt_dato_KeyPress vbKeyReturn
        
        If VIEW_DOCUMENTOS.Grid.ListItems.Count > 0 Then
            VIEW_DOCUMENTOS.Grid.ListItems(1).Selected = True
            VIEW_DOCUMENTOS.Grid.ListItems(1).Checked = True
            VIEW_DOCUMENTOS.anular_Click
        End If
        
        Set VIEW_DOCUMENTOS = Nothing
        
    End If
    
    FrmAppLink.Finalizar_MAKE_DOC
    
    Exit Sub
    
ErrorAnular:
    
    Set VIEW_DOCUMENTOS = Nothing
    FrmAppLink.Finalizar_MAKE_DOC
    
End Sub

Public Sub AnularNDE(ByVal pCorrelativo As String, Optional pEstatus = "DPE")
'
'    On Error GoTo ErrorAnular
'
'    Anulacion = True
'    Imprime = False
'
'    Find_Concept = "NDE"
'    'Find_Status = pEstatus
'
'    Set Forma2 = Me
'    Forma2.Tag = "PEDIDO"
'
'    Titulo = "Pedidos"
'    TablaDoc = "MA_VENTAS"
'
'    Campo_Doc1 = "c_Documento"
'    Campo_Doc2 = "c_Descripcion"
'    Campo_Doc3 = "d_Fecha"
'
'    BypassAgenteSync = True: Find_Status = "DPE', 'DCO"
'
'    Unload VIEW_DOCUMENTOS
'
'    VIEW_DOCUMENTOS.esbkodc = False
'    VIEW_DOCUMENTOS.pPrechequeo = False
'
'    VIEW_DOCUMENTOS.txt_dato.Text = pCorrelativo
'    VIEW_DOCUMENTOS.opt_cod = True
'
'    VIEW_DOCUMENTOS.txt_dato_KeyPress vbKeyReturn
'
'    If VIEW_DOCUMENTOS.Grid.ListItems.Count > 0 Then
'        VIEW_DOCUMENTOS.Grid.ListItems(1).Selected = True
'        VIEW_DOCUMENTOS.Grid.ListItems(1).Checked = True
'        VIEW_DOCUMENTOS.anular_Click
'    End If
'
'    Set VIEW_DOCUMENTOS = Nothing
'    BypassAgenteSync = False
'    Find_Status = pEstatus
'
'    Exit Sub
'
'ErrorAnular:
'
'    Set VIEW_DOCUMENTOS = Nothing
'    BypassAgenteSync = False
'    Find_Status = pEstatus
'
End Sub

Private Sub lblBuscarSolicitudes_Click()
    Call CmdBuscar_Click
End Sub

Private Sub RechazarPed_Click()
    
    Dim mNivel As Long
    Dim mFila As Long
    Dim mDoc As ClsTrans_Cab
    
    mFila = grd.Row
    
    PedidoTmp = grd.TextMatrix(mFila, eColGridSolicitudTrans.eColTransNumDoc)
    
    Mensaje False, "�Est� seguro que desea rechazar / anular la solicitud N� [" & PedidoTmp & "]?"
    
    If Retorno Then
        
        mNivel = Val(BuscarReglaNegocioStr("TRA_Consola_NivelRechazarSolicitud", "5"))
        
        If mNivel > FrmAppLink.GetNivelUsuario Then
            If Not gRutinas.ShowAutorizaciones(CInt(mNivel), fCls.Conexion) Then
                Mensaje True, FrmAppLink.Stellar_Mensaje(11010) 'Usted no posee el nivel de usuario permitido para ejecutar esta Operaci�n.
                Exit Sub
            End If
        End If
        
        Set mDoc = fCls.Documento(mFila)
        
        If Toolbar1.Buttons(3).ButtonMenus(1).Enabled Then
            
            If mDoc.LineasDocumento.Count > 0 Then
                
                'AnularNDE fCls.Documento(grd.Row).LineasDocumento(1).DocumentoRel
                AnularPedido PedidoTmp, fCls.Documento(grd.Row).Estatus
                
                DoEvents
                
                buscar fCls.Corrida
                
            End If
            
        Else
            
            AnularPedido PedidoTmp, mDoc.Estatus
            
            CmdBuscar_Click
            
        End If
        
    End If
    
End Sub

Private Sub TextFechaDesde_Click()
    
    Dim mCls As Object 'clsFechaSeleccionclsFechaSeleccionclsFechaSeleccion
    
    Set mCls = FrmAppLink.GetClassFechaSeleccion 'New clsFechaSeleccion
    
    mCls.MostrarInterfazFechaHora DateTypes.JustDate
    
    If mCls.Selecciono Then
        'If ValidarItems(mCls.FECHA) Then
            TextFechaDesde.Text = mCls.FECHA
            TextFechaDesde.Tag = CDbl(mCls.FECHA)
        'End If
    End If
    
End Sub

Private Sub TextFechaDesdeRes_Click()
    
    Dim mCls As Object 'clsFechaSeleccion
    
    Set mCls = FrmAppLink.GetClassFechaSeleccion 'New clsFechaSeleccion
    
    mCls.MostrarInterfazFechaHora DateTypes.Full
    
    If mCls.Selecciono Then
        'If ValidarItems(mCls.FECHA) Then
            TextFechaDesdeRes.Text = mCls.FECHA
            TextFechaDesdeRes.Tag = CDbl(mCls.FECHA)
        'End If
    End If
    
End Sub

Private Sub TextFechaHasta_Click()
    
    Dim mCls As Object 'clsFechaSeleccionclsFechaSeleccionclsFechaSeleccion
    
    Set mCls = FrmAppLink.GetClassFechaSeleccion 'New clsFechaSeleccion
    
    mCls.MostrarInterfazFechaHora DateTypes.JustDate
    
    If mCls.Selecciono Then
        'If ValidarItems(mCls.FECHA) Then
            TextFechaHasta.Text = mCls.FECHA
            TextFechaHasta.Tag = CDbl(mCls.FECHA)
        'End If
    End If
    
End Sub

Private Sub TextFechaHastaRes_Click()
    
    Dim mCls As Object 'clsFechaSeleccionclsFechaSeleccion
    
    Set mCls = FrmAppLink.GetClassFechaSeleccion 'New clsFechaSeleccion
    
    mCls.MostrarInterfazFechaHora DateTypes.Full
    
    If mCls.Selecciono Then
        'If ValidarItems(mCls.FECHA) Then
            TextFechaHastaRes.Text = mCls.FECHA
            TextFechaHastaRes.Tag = CDbl(mCls.FECHA)
        'End If
    End If
    
End Sub

Public Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case LCase(Button.Key)
        Case "anular"
            Form_KeyDown vbKeyF6, 0
        Case "buscar"
            Form_KeyDown vbKeyF2, 0
        Case "cancelar"
            Form_KeyDown vbKeyF7, 0
        Case "grabar"
            Form_KeyDown vbKeyF4, 0
        Case "reimprimir"
            Form_KeyDown vbKeyF8, 0
        Case "salir"
            Form_KeyDown vbKeyF12, 0
        Case LCase("Leyenda")
            
            On Error Resume Next
            
            Dim TempRs As ADODB.Recordset
            
            If Trim(fCls.Corrida) <> Empty Then
                
                Set TempRs = Ent.BDD.Execute("SELECT * FROM MA_ALERTAGRID_PROCESOS WHERE c_Descripcion LIKE 'Transferencia - Finalizar Picking'")
                If Not TempRs.EOF Then frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
                frmLeyendaGridStellar.Show vbModal
                
            Else
                
                Set TempRs = Ent.BDD.Execute("SELECT * FROM MA_ALERTAGRID_PROCESOS WHERE c_Descripcion LIKE 'Transferencia - Iniciar Picking'")
                If Not TempRs.EOF Then frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
                frmLeyendaGridStellar.Show vbModal
                
            End If
            
    End Select
    
End Sub

Public Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    Select Case UCase(ButtonMenu.Key)
        Case UCase("GFactura")
             GrabarPicking True
        Case UCase("RFac")
            Reimprimir False, False, True
        Case UCase("RDet")
            Reimprimir False, True, True
        Case UCase("RPicking")
            Reimprimir True, False, True
        Case UCase("PickingResFac")
            Reimprimir False, False, False
        Case UCase("PickingDetFac")
            Reimprimir False, True, False
        Case UCase("LT_CON_TRA_X_LOC")
            ReimprimirLoteTransferenciaAgrupado
    End Select
End Sub

Public Sub ReimprimirLoteTransferenciaAgrupado()
    
    Dim mSQL As String, mResult As Variant
    
    mSQL = ";WITH Lote AS ( " & vbNewLine & _
    "SELECT MA.* " & vbNewLine & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA MA " & vbNewLine & _
    "WHERE 1 = 1 " & vbNewLine & _
    "AND MA.b_Finalizada = 1 " & vbNewLine & _
    "), Base AS ( " & vbNewLine & _
    "SELECT MA.*, '___' AS TmpSplit1, TR.c_Documento, " & _
    "TR.c_Documento_Nota, TR.c_Documento_Trans, TR.c_CodLocalidad_Destino, " & vbNewLine & _
    "TR.cs_CodLocalidad , TR.cs_CodLocalidad_Solicitud " & vbNewLine & _
    "FROM Lote MA " & vbNewLine & _
    "INNER JOIN TR_LOTE_GESTION_TRANSFERENCIA TR " & vbNewLine & _
    "ON MA.cs_Corrida = TR.cs_Corrida " & vbNewLine & _
    "), LoteXLoc AS ( " & vbNewLine & _
    "SELECT MAX(d_FechaCorrida) AS d_FechaCorrida, MAX(c_Transporte) AS c_Transporte, " & vbNewLine & _
    "MAX(c_Observacion) AS c_Observacion, MAX(c_Clasificacion) AS c_Clasificacion, " & vbNewLine & _
    "cs_Corrida, MAX(cs_CodLocalidad) AS cs_CodLocalidad, " & vbNewLine & _
    "c_CodLocalidad_Destino, COUNT(c_Documento) AS n_Solicitudes, " & vbNewLine & _
    "isNULL(LCD.c_Descripcion, 'N/A') AS LocalidadDestino " & vbNewLine & _
    "FROM Base " & vbNewLine & _
    "LEFT JOIN MA_SUCURSALES LCD " & vbNewLine & _
    "ON Base.c_CodLocalidad_Destino = LCD.c_Codigo " & vbNewLine & _
    "GROUP BY cs_Corrida, c_CodLocalidad_Destino, LCD.c_Descripcion " & vbNewLine
    
    mSQL = mSQL & _
    ") SELECT  * FROM LoteXLoc " & vbNewLine & _
    "WHERE 1 = 1 "
    
    Dim mFrm_Super_Consultas As Object
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With mFrm_Super_Consultas
        
        '"Lote de Transferencias Agrupado"
        .Inicializar mSQL, StellarMensaje(5572), Ent.BDD
        
        .Add_ItemLabels "Lote", "cs_Corrida", 2200, 0
        .Add_ItemLabels "Fecha", "d_FechaCorrida", 3585, 0, , Array("Fecha", "VbGeneralDate")
        .Add_ItemLabels "Destino", "LocalidadDestino", 3330, 0
        .Add_ItemLabels "# Solicitudes", "n_Solicitudes", 2235, 0
        .Add_ItemLabels "Observaci�n", "c_Observacion", 7130, 0
        .Add_ItemLabels "Clasificacion", "c_Clasificacion", 0, 0
        .Add_ItemLabels "Origen", "cs_CodLocalidad", 0, 0
        .Add_ItemLabels "CodLocalidadDestino", "c_CodLocalidad_Destino", 0, 0
        
        .Add_ItemSearching "N� Lote", "cs_Corrida"
        .Add_ItemSearching "Fecha", "CONVERT(NVARCHAR(MAX), d_FechaCorrida, 103)"
        .Add_ItemSearching "Destino", "LocalidadDestino"
        .Add_ItemSearching "Observaci�n", "c_Observacion"
        .Add_ItemSearching "Clasificaci�n", "c_Clasificacion"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = False
        .StrOrderBy = "d_FechaCorrida DESC;"
        
        .Show vbModal
        
        mResult = .ArrResultado()
        
        If Not IsEmpty(mResult) Then
            If Trim(mResult(0)) <> Empty Then
                
                mTmpLote = mResult(0)
                mTmpLocDestino = mResult(7)
                
                Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
                
'                ' 0 PANTALLA, 1 IMPRESORA, 2 XML, 3 ETIQUETAS
'                mSQL = _
'                "SELECT 0 AS NumOpcion, '" & FrmAppLink.StellarMensaje(10013) & "' AS Opcion " & _
'                "UNION ALL " & _
'                "SELECT 1 AS NumOpcion, '" & FrmAppLink.StellarMensaje(10014) & "' AS Opcion " & _
'                "UNION ALL " & _
'                "SELECT 2 AS NumOpcion, 'XML' AS Opcion " & _
'                "UNION ALL " & _
'                "SELECT 3 AS NumOpcion, '" & FrmAppLink.StellarMensaje(273) & "' AS Opcion "
                
                ' 0 PANTALLA, 1 IMPRESORA, 3 ETIQUETAS
                mSQL = _
                "SELECT 0 AS NumOpcion, '" & FrmAppLink.StellarMensaje(10013) & "' AS Opcion " & _
                "UNION ALL " & _
                "SELECT 1 AS NumOpcion, '" & FrmAppLink.StellarMensaje(10014) & "' AS Opcion " & _
                "UNION ALL " & _
                "SELECT 3 AS NumOpcion, '" & FrmAppLink.StellarMensaje(273) & "' AS Opcion "
                
                mSQL = "SELECT * FROM (" & vbNewLine & mSQL & vbNewLine & ") TB WHERE 1 = 1 "
                
                'SELECCIONE EL TIPO DE IMPRESION
                
                .Inicializar mSQL, FrmAppLink.StellarMensaje(16400), Ent.BDD
                
                .Add_ItemLabels FrmAppLink.StellarMensaje(108) & " #", "NumOpcion", 0, 0
                .Add_ItemLabels FrmAppLink.StellarMensaje(108), "Opcion", 11340, 0
                
                .Add_ItemSearching FrmAppLink.StellarMensaje(108), "Opcion"
                
                .txtDato.Text = "%"
                .CustomFontSize = 24
                .BusquedaInstantanea = True
                .StrOrderBy = "NumOpcion ASC"
                
                .Show vbModal
                
                mResult = .ArrResultado()
                
                If Not IsEmpty(mResult) Then
                    
                    If Trim(mResult(0)) <> Empty Then
                        
                        TmpNumOpc = CInt(mResult(0))
                        
                        Dim TipoImpresion As String
                        
                        TipoImpresion = UCase(FrmAppLink.StellarMensaje(445)) '"REIMPRESI�N"
                        
                        Imprimir_Lote_Transferencia_Agrupado mTmpLote, _
                        FrmAppLink.GetCodLocalidadSistema, mTmpLocDestino, _
                        TmpNumOpc, TipoImpresion
                        
                    End If
                    
                End If
                
            End If
            
        End If
        
    End With
    
    Set mFrm_Super_Consultas = Nothing
    
End Sub

Private Sub txtMaxRegistros_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim Res As Double
    gRutinas.LlamarTecladoNumerico Res, 0
    txtMaxRegistros.Text = CDec(Res)
End Sub

Private Sub txtTransporte_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
       cmdTransporte_Click
    End If
End Sub

Private Sub txtTransporte_KeyPress(KeyAscii As Integer)
    KeyAscii = IIf(KeyAscii = Asc("'"), 0, KeyAscii)
End Sub

Private Sub txtTransporte_LostFocus()
    If fCls.BuscarTransporte(txtTransporte.Text) Then
        LlenarDatosTransporte
    Else
        LimpiarControlesT
    End If
End Sub

Private Sub LlenarDatosTransporte()
    With fCls.Transporte
        txtTransporte.Text = .Codigo
        lblDescri.Caption = .Descripcion
        lblMarca.Caption = .Marca
        lblModelo.Caption = .Modelo
        lblPlaca.Caption = .Placa
        lblResponsable.Caption = .Responsable
        CalculoTransporte
    End With
End Sub

Private Sub LimpiarControlesT()
    txtTransporte.Text = Empty
    lblDescri.Caption = Empty
    lblMarca.Caption = Empty
    lblModelo.Caption = Empty
    lblPlaca.Caption = Empty
    lblResponsable.Caption = Empty
    lblPesoV.Caption = "Peso: "
    lblvolumenV.Caption = "Volumen: "
    lblPesoD.Caption = "Peso: "
    lblvolumenD.Caption = "Volumen: "
    lblPesoP.Caption = "Peso: "
    lblvolumenP.Caption = "Volumen: "
End Sub

Private Function InterfazBusqueda(pCriterio As Integer)
    
    Dim mResul As Variant
    
    Select Case pCriterio
        
        Case 0 ' Sin criterio / todas
            
            mConsulta = "SELECT TOP (100) cs_Corrida, d_Fecha, " & _
            "c_Observacion, c_Clasificacion " & _
            "FROM MA_LOTE_GESTION_TRANSFERENCIA "
            
            mTitulo = "Top 100 Lotes Picking"
            
        Case 2
            
            mConsulta = "SELECT TOP (100) cs_Corrida, d_Fecha, " & _
            "c_Observacion, c_Clasificacion " & _
            "FROM MA_LOTE_GESTION_TRANSFERENCIA " & _
            "WHERE (b_Finalizada = 0 AND b_Anulada = 0) AND b_PackingFinalizado = 1 "
            
            mTitulo = "Picking > Packing - Top 100 Lotes por Aprobar - Listos para Transferir"
            
        Case 1
            
            mConsulta = "SELECT TOP (100) cs_Corrida, d_Fecha, " & _
            "c_Observacion, c_Clasificacion " & _
            "FROM MA_LOTE_GESTION_TRANSFERENCIA " & _
            "WHERE b_Finalizada = 1 "
            
            mTitulo = "Picking > Packing - Top 100 Lotes Finalizados"
            
    End Select
    
    Dim mFrm_Super_Consultas As Object
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With mFrm_Super_Consultas
        
        .Inicializar mConsulta, mTitulo, Ent.BDD
        
        .Add_ItemLabels "Lote", "cs_Corrida", 2200, 0
        .Add_ItemLabels "Fecha", "d_Fecha", 3700, 0, , Array("Fecha", "VbGeneralDate")
        .Add_ItemLabels "Observaci�n", "c_Observacion", 5130, 0
        .Add_ItemLabels "Clasificacion", "c_Clasificacion", 0, 0
        
        .Add_ItemSearching "N� Lote", "cs_Corrida"
        .Add_ItemSearching "Fecha", "CONVERT(NVARCHAR(MAX), d_Fecha, 103)"
        .Add_ItemSearching "Observaci�n", "c_Observacion"
        .Add_ItemSearching "Clasificaci�n", "c_Clasificacion"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = True
        .StrOrderBy = "cs_Corrida DESC"
        
        .Show vbModal
        
        InterfazBusqueda = .ArrResultado()
        
        If Not IsEmpty(mResul) Then
            If Trim(mResul(0)) <> Empty Then
                Me.txtTransporte.Text = CStr(mResul(0))
                txtTransporte_LostFocus
            End If
        Else
            Me.txtTransporte.Text = Empty
            Me.lblResponsable = Empty
        End If
        
    End With
    
    Set mFrm_Super_Consultas = Nothing
    
End Function

Private Sub buscar(Optional pCorrida As String)
    
    Dim mBus As Variant
    
    If pCorrida = Empty Then
        
        mBus = InterfazBusqueda(2)
        
        If Not IsEmpty(mBus) Then
            pCorrida = mBus(0)
        End If
        
    End If
    
    If Len(pCorrida) > 0 Then
        
        If fCls.BuscarLoteGestionTransferencia(pCorrida) Then
            
            txtTransporte.Text = fCls.Transporte.Codigo
            txtTransporte_LostFocus
            
            fCls.LlenarDocumentosGrd grd
            
            txtObs.Text = fCls.Observacion
            FrmAppLink.ListSafeItemSelection CmbClasificacion, fCls.Clasificacion
            
            EstadoForm eEstadoBuscar
            'AjustarColumnasDelGrid
            
        End If
        
    End If
    
End Sub

Private Sub EstadoForm(pEstado As eEstadoBarraDoc)
    Select Case pEstado
        Case eEstadoBuscar
            ActivarControlesBusqueda False
            fCls.EstadoToolbar Toolbar1, eEstadoEditar
        Case eEstadoCancelar
            IniciarForm
        Case Else
            fCls.EstadoToolbar Toolbar1, eEstadoEditar
    End Select
End Sub

Private Sub GrabarPicking(pCerrarLote As Boolean)
    
    Dim I As Long
    
    If ValidarDatos(pCerrarLote) Then
        
        If fCls.Grabar(pCerrarLote, CantTransacciones) Then
            
            If pCerrarLote Then
            
                Find_Concept = "TRA"
                Find_Status = "DCO"
                
                fCls.ImprimirDocumento fCls.Corrida, True, False, True
                
            Else
                
                fCls.ImprimirDocumento fCls.Corrida, False, False, , True
                
            End If
            
            EstadoForm eEstadoCancelar
            
        End If
        
    End If
    
End Sub

Private Sub Reimprimir( _
Optional pXCliente As Boolean = False, _
Optional pDetallado As Boolean = False, _
Optional pSolicitudes As Boolean = False)
    
    Dim mBus As Variant
    
    If pXCliente Then
        
        Set Rep_Picking_Transferencia.fCls = fCls
        Rep_Picking_Transferencia.Show vbModal
        
    Else
        
        mBus = InterfazBusqueda(IIf(pSolicitudes, 0, 1))
        
        'ValidarImpresoraPredeterminadaDelSistemaOperativo
        
        If Not IsEmpty(mBus) Then
            If Trim(mBus(0)) <> Empty Then
                fCls.ImprimirDocumento CStr(mBus(0)), pXCliente, pDetallado, , pSolicitudes
            End If
        End If
        
    End If
    
End Sub

Private Sub CalculoTransporte()
    
    Dim mPeso As Double, mVol As Double
    
    mPeso = fCls.Totalizar(0)
    mVol = fCls.Totalizar(1)
    
    'lblPesoV.Caption = "Peso: " & Format(fCls.Transporte.CapacidadPeso, "Standard")
    'lblvolumenV.Caption = "Volumen: " & Format(fCls.Transporte.CapacidadVolumen, "Standard")
    'lblPesoP.Caption = "Peso: " & Format(mPeso, "Standard")
    'lblvolumenP.Caption = "Volumen: " & Format(mVol, "Standard")
    'lblPesoD.Caption = "Peso: " & Format(fCls.Transporte.CapacidadPeso - mPeso, "Standard")
    'lblvolumenD.Caption = "Volumen: " & Format(fCls.Transporte.CapacidadVolumen - mVol, "Standard")
    
    lblPlacaPesoVeh.Caption = FrmAppLink.FormatoDecimalesDinamicos(fCls.Transporte.CapacidadPeso, 2, 4)
    lblPlacaVolumenVeh.Caption = FrmAppLink.FormatoDecimalesDinamicos(fCls.Transporte.CapacidadVolumen, 2, 4)
    lblPesoPedido.Caption = FrmAppLink.FormatoDecimalesDinamicos(mPeso, 2, 4)
    lblvolumenPedido.Caption = FrmAppLink.FormatoDecimalesDinamicos(mVol, 2, 4)
    lblPesoDespacho.Caption = FrmAppLink.FormatoDecimalesDinamicos(fCls.Transporte.CapacidadPeso - mPeso, 2, 4)
    lblvolumenDespacho.Caption = FrmAppLink.FormatoDecimalesDinamicos(fCls.Transporte.CapacidadVolumen - mVol, 2, 4)
    
End Sub

Private Sub ActivarControlesBusqueda(pActivar As Boolean)
    desde.Enabled = pActivar
    hasta.Enabled = pActivar
    'txtRuta.Enabled = pActivar
    'cmdRuta.Enabled = pActivar
    'txtZona.Enabled = pActivar
    'cmdZona.Enabled = pActivar
    'txtVendedor.Enabled = pActivar
    'cmdVendedor.Enabled = pActivar
End Sub

Private Function ValidarDatos(pCerrarLote As Boolean) As Boolean
    
    Dim mDoc As ClsTrans_Cab
    
    'Solo pedir el transporte al final.
    
    If Trim(fCls.Corrida) <> Empty _
    And (Trim(fCls.Transporte.Codigo) = Empty _
    Or Trim(Me.txtTransporte.Text) = Empty) Then
        
        Mensaje True, "Debe seleccionar un Transporte."
        
        If mColorTab = TabDatosLote.BackColor Then
            TabDatosLote_Click
            DoEvents
            cmdTransporte_Click
        End If
        
        Exit Function
        
    End If
    
    If fCls.NumeroDocumentos > 0 Then
        For Each mDoc In fCls.Documento
            If mDoc.Seleccionado Then
                If pCerrarLote Then
                    'If Not mDoc.PagoCompletado Then
                        'Mensaje True, _
                        "Existen pedidos con pagos no autorizados. " & _
                        "Se deben completar dichos pagos o en " & _
                        "�ltima instancia anular los pedidos antes de proceder con el cierre del lote."
                        'Exit Function
                    'End If
                End If
            End If
        Next
    Else
        Call Mensaje(True, "No hay Documentos Seleccionados.")
        Exit Function
    End If
    
    If mColorTab = TabDatosLote.BackColor Then
        TabDatosLote_Click
        FrmAppLink.ShowToast "Verifique los datos del Lote y pulse " & _
        "grabar nuevamente en esta pesta�a.", 3000, 2000, Toolbar1, , , , , , , , 4000
        Exit Function
    End If
    
    fCls.Observacion = txtObs.Text
    
    If CmbClasificacion.ListCount > 1 Then
        fCls.Clasificacion = CmbClasificacion.Text
    End If
    
    ValidarDatos = True
    
End Function

Private Sub Anular()
    
    Mensaje False, "�Esta seguro de Anular el Picking?"
    
    If Retorno Then
        If fCls.Anular Then
            IniciarForm
        End If
    End If
    
End Sub

Private Sub MostrarInterfaz(pFila As Integer, pTipoInterfaz As Integer)
    
    SendKeys "{ESC}"
    fCls.EstablecerVentana Me.hWnd
    
    Select Case pTipoInterfaz
        Case 1
            fCls.VerDetalleDocumento pFila
    End Select
    
End Sub

Public Sub AjustarColumnasDelGrid()
    
    'Esto lo que hace es que recorre todas las columnas del grid y quita el texto
    'si la columna no tiene un width para que no se vean los cachos.
    
'    For i = 0 To Me.grd.Cols - 1
'        grd.Row = 0
'        If grd.ColWidth(i) = 0 Then
'            grd.Col = i
'            grd.Text = Empty
'        Else
'            ' para saber el numero de la columna que se muestra es util, por la multiples formas de trabajo
'            Debug.Print i
'        End If
'    Next
'
'    If Picking_TipoGrid = 1 Then
'
'        grd.ColWidth(0) = 400 ' LN
'        grd.ColWidth(1) = 2000 ' DOCUMENTO
'        grd.ColWidth(2) = 1450 ' FECHA
'        grd.ColWidth(3) = 3500 ' CLIENTE
'        grd.ColWidth(4) = 1600 ' SUBTOTAL
'        grd.ColWidth(5) = 1600 ' IMPUESTO
'        grd.ColWidth(6) = 2000 ' TOTAL
'        grd.ColWidth(7) = 900 ' PESO
'        grd.ColWidth(8) = 900 ' VOLUMEN
'
'    ElseIf Picking_TipoGrid = 2 Then
'
'        grd.ColWidth(0) = 400 ' LN
'        grd.ColWidth(1) = 2000 ' DOCUMENTO
'        grd.ColWidth(2) = 1450 ' FECHA
'        grd.ColWidth(3) = 4200 ' CLIENTE
'        grd.ColWidth(4) = 2025 ' SUBTOTAL
'        grd.ColWidth(5) = 2025 ' IMPUESTO
'        grd.ColWidth(6) = 2325 ' TOTAL
'
'        grd.Col = 7
'        grd.ColWidth(grd.Col) = 0 ' PESO
'        grd.ColAlignment(grd.Col) = flexAlignRightCenter
'        grd.CellAlignment = flexAlignRightCenter
'
'        grd.Col = 8
'        grd.ColWidth(grd.Col) = 0 ' PESO
'        grd.ColAlignment(grd.Col) = flexAlignRightCenter
'        grd.CellAlignment = flexAlignRightCenter
'
'    End If
    
End Sub

Private Function ValidarFechasEnviar(pDesde, pHasta, dDesde, dHasta) As String
    
    If (pDesde = Empty Or pHasta = Empty) And (dDesde = Empty Or dHasta = Empty) Then
        ValidarFechasEnviar = 0
    End If
    
    If (pDesde <> Empty And pHasta <> Empty) And (dDesde = Empty Or dHasta = Empty) Then
        ValidarFechasEnviar = 1
    End If
    
    If (pDesde = Empty Or pHasta = Empty) And (dDesde <> Empty And dHasta <> Empty) Then
        ValidarFechasEnviar = 2
    End If
    
    If (pDesde <> Empty And pHasta <> Empty) And (dDesde <> Empty And dHasta <> Empty) Then
        ValidarFechasEnviar = 3
    End If
    
End Function

Private Sub MostrarFrameAccionesGrid()
    
    If EventoProgramado Then
        Exit Sub
    End If
    
    Dim I
    
    grd_EnterCell
    
    FrameAccionesGrid.Visible = True
    
    For I = Me.Height To 8040 Step -5
        DoEvents
        FrameAccionesGrid.Top = I
    Next I
    
End Sub

Private Sub OcultarFrameAccionesGrid()
    
    Dim I
    
    FrameAccionesGrid.Top = 15000
    
    For I = 8040 To Me.Height Step 5
        DoEvents
        FrameAccionesGrid.Top = I
    Next I
    
    'FrameAccionesGrid.Visible = False
    
End Sub

Private Sub CmbClasificacion_Click()
    
    If Not EventoProgramado Then
        
        Static PosAnterior As Long
        
        If (PosAnterior <> CmbClasificacion.ListIndex _
        And CmbClasificacion.ListIndex = CmbClasificacion.ListCount - 1) _
        Or CmbClasificacion.ListCount = 1 Then
            PosAnterior = CmbClasificacion.ListIndex
            EventoProgramado = True
            mClsGrupos.AgregarModificarGrupo Ent.BDD, CmbClasificacion
            mClsGrupos.CargarComboGrupos Ent.BDD, CmbClasificacion
            FrmAppLink.ListRemoveItems CmbClasificacion, "Ninguno" ' Quitar dichos elementos unicamente aqu� sin alterar la funci�n anterior.
            EventoProgramado = False
        ElseIf PosAnterior <> CmbClasificacion.ListIndex _
        And CmbClasificacion.ListIndex < CmbClasificacion.ListCount - 1 Then
            PosAnterior = CmbClasificacion.ListIndex
        End If
        
    End If
    
End Sub

Public Sub Imprimir_Lote_Transferencia_Agrupado( _
ByVal Documento As String, ByVal CodLocalidadOrigen As String, _
ByVal CodLocalidadDestino As String, ByVal Dispositivo As Integer, _
Optional ByVal TipoImpresion As String)
    
    On Error GoTo Error
    
    Dim RsEmpresa As New ADODB.Recordset
    Dim RsCompras As New ADODB.Recordset
    
    Dim mRsMaRequisicion As New ADODB.Recordset, _
    mRsTrRequisicion As New ADODB.Recordset, _
    RsDocumento As New ADODB.Recordset
    
    Dim mClsRutinas As New cls_Rutinas
    
    'Dim fSeguridadReglasdeNegocio As New cls_Seg_RegdeNegocio
    
    Dim mFormaTrabajo As Integer
    
    mFormaTrabajo = SVal(BuscarReglaNegocioStr("Trf_FormaDeTrabajo")) 'fSeguridadReglasdeNegocio.Req_FormadeTrabajo
    
    Dim mSQL As String
    
    Call mClsRutinas.Apertura_Recordset(mRsMaRequisicion, adUseClient)
    Call mClsRutinas.Apertura_Recordset(mRsTrRequisicion, adUseClient)
    
    mSQL = ";WITH Lote AS ( " & vbNewLine & _
    "SELECT MA.* " & vbNewLine & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA MA " & vbNewLine & _
    "WHERE MA.cs_Corrida = '" & Documento & "' " & vbNewLine & _
    "AND MA.b_Finalizada = 1 " & vbNewLine & _
    "), Base AS ( " & vbNewLine & _
    "SELECT MA.*, '___' AS TmpSplit1, TR.c_Documento, " & _
    "TR.c_Documento_Nota, TR.c_Documento_Trans, TR.c_CodLocalidad_Destino, " & vbNewLine & _
    "TR.cs_CodLocalidad , TR.cs_CodLocalidad_Solicitud " & vbNewLine & _
    "FROM Lote MA " & vbNewLine & _
    "INNER JOIN TR_LOTE_GESTION_TRANSFERENCIA TR " & vbNewLine & _
    "ON MA.cs_Corrida = TR.cs_Corrida " & vbNewLine
    
    mSQL = mSQL & _
    "), LoteXLoc AS ( " & vbNewLine & _
    "SELECT MAX(d_FechaCorrida) AS d_FechaCorrida, MAX(c_Transporte) AS c_Transporte, " & vbNewLine & _
    "MAX(c_Observacion) AS c_Observacion, MAX(c_Clasificacion) AS c_Clasificacion, " & vbNewLine & _
    "MAX(cs_CodLocalidad) AS cs_CodLocalidad, cs_Corrida, " & vbNewLine & _
    "c_CodLocalidad_Destino, COUNT(c_Documento) AS n_Solicitudes " & vbNewLine & _
    "FROM Base " & vbNewLine & _
    "GROUP BY cs_Corrida, c_CodLocalidad_Destino " & vbNewLine & _
    "), TMP_MA_LOTE_GESTION_TRANSFERENCIA_DESTINO AS ( " & vbNewLine & _
    "SELECT [cs_CodLocalidad], [cs_Corrida], [d_FechaCorrida], " & vbNewLine & _
    "[c_Transporte], [c_Observacion], [c_Clasificacion], " & vbNewLine & _
    "'DPE' AS [c_Status], c_CodLocalidad_Destino " & vbNewLine & _
    "FROM LoteXLoc " & vbNewLine & _
    "WHERE c_CodLocalidad_Destino = '" & CodLocalidadDestino & "' " & vbNewLine & _
    ") " & vbNewLine
    
    mSQL = mSQL & _
    "SELECT MA.*, isNULL(ORIGEN.c_Descripcion, 'N/A') AS OrigenDes, " & _
    "isNULL(DESTINO.c_Descripcion, 'N/A') AS DestinoDes, " & _
    "isNULL(DESTINO.c_Direccion, '') AS DireccionDes, " & _
    "isNULL(DESTINO.c_Telefono, '') AS TelefonoDes, " & _
    "isNULL(DESTINO.c_Ciudad, '') AS CiudadDes, " & _
    "isNULL(DESTINO.c_Rif, '') AS RifDes, " & _
    "isNULL(DESTINO.c_Nombre_Cheque, '') AS RazonSocialDes, " & _
    "isNULL(DESTINO.c_Gerente, '') AS GerenteDes, " & _
    "isNULL(DESTINO.c_SubGerebte, '') AS SubGerenteDes, " & _
    "isNULL(TRP.c_Descripcion, 'N/A') AS Transporte " & _
    "FROM TMP_MA_LOTE_GESTION_TRANSFERENCIA_DESTINO MA " & _
    "LEFT JOIN MA_SUCURSALES AS ORIGEN " & _
    "ON ORIGEN.c_Codigo = MA.cs_CodLocalidad " & _
    "LEFT JOIN MA_SUCURSALES AS DESTINO " & _
    "ON DESTINO.c_Codigo = MA.c_CodLocalidad_Destino " & _
    "LEFT JOIN MA_TRANSPORTE TRP " & _
    "ON TRP.c_CodTransporte = MA.c_Transporte " & _
    "WHERE MA.cs_Corrida = '" & Documento & "' " & _
    "AND MA.cs_CodLocalidad = '" & CodLocalidadOrigen & "' " & _
    "; "
    
    mRsMaRequisicion.Open mSQL, Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
    
    ' Detalle
    
    mSQL = ";WITH Lote AS ( " & vbNewLine & _
    "SELECT MA.* " & vbNewLine & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA MA " & vbNewLine & _
    "WHERE MA.cs_Corrida = '" & Documento & "' " & vbNewLine & _
    "AND MA.b_Finalizada = 1 " & vbNewLine & _
    "), Base AS ( " & vbNewLine & _
    "SELECT MA.*, '___' AS TmpSplit1, TR.c_Documento, " & _
    "TR.c_Documento_Nota, TR.c_Documento_Trans, TR.c_CodLocalidad_Destino, " & vbNewLine & _
    "TR.cs_CodLocalidad , TR.cs_CodLocalidad_Solicitud " & vbNewLine & _
    "FROM Lote MA " & vbNewLine & _
    "INNER JOIN TR_LOTE_GESTION_TRANSFERENCIA TR " & vbNewLine & _
    "ON MA.cs_Corrida = TR.cs_Corrida " & vbNewLine & _
    "), LoteXLoc AS ( " & vbNewLine & _
    "SELECT MAX(d_FechaCorrida) AS d_FechaCorrida, MAX(c_Transporte) AS c_Transporte, " & vbNewLine & _
    "MAX(c_Observacion) AS c_Observacion, MAX(c_Clasificacion) AS c_Clasificacion, " & vbNewLine & _
    "MAX(cs_CodLocalidad) AS cs_CodLocalidad, cs_Corrida, " & vbNewLine & _
    "c_CodLocalidad_Destino, COUNT(c_Documento) AS n_Solicitudes " & vbNewLine & _
    "FROM Base " & vbNewLine & _
    "GROUP BY cs_Corrida, c_CodLocalidad_Destino " & vbNewLine & _
    "), ProXLoc_Solicitud AS ( " & vbNewLine & _
    "SELECT BASE.cs_CodLocalidad, BASE.cs_Corrida, BASE.c_CodLocalidad_Destino, " & vbNewLine & _
    "DET.cs_CodArticulo, SUM(DET.ns_Cantidad) AS ns_Cantidad, " & vbNewLine & _
    "MIN(DET.ns_Linea) AS ns_Linea, MAX(DET.ns_Decimales) AS ns_Decimales, " & vbNewLine & _
    "CASE WHEN MAX(DET.ns_CantidadEmpaque) > 0 THEN MAX(DET.ns_CantidadEmpaque) " & vbNewLine & _
    "ELSE 1 END AS ns_CantidadEmpaque " & vbNewLine
    
    mSQL = mSQL & _
    "FROM BASE " & vbNewLine & _
    "INNER JOIN MA_REQUISICIONES CAB " & vbNewLine & _
    "ON BASE.c_Documento = CAB.cs_Documento " & vbNewLine & _
    "AND BASE.cs_CodLocalidad_Solicitud = CAB.cs_CodLocalidad " & vbNewLine & _
    "INNER JOIN TR_REQUISICIONES DET " & vbNewLine & _
    "ON BASE.c_Documento = DET.cs_Documento " & vbNewLine & _
    "AND BASE.cs_CodLocalidad_Solicitud = DET.cs_CodLocalidad " & vbNewLine & _
    "GROUP BY BASE.cs_CodLocalidad, BASE.cs_Corrida, BASE.c_CodLocalidad_Destino, DET.cs_CodArticulo " & vbNewLine & _
    "), ProXLoc_Solicitud2 AS ( " & vbNewLine & _
    "SELECT *, ROUND(ns_Cantidad / ns_CantidadEmpaque, 0, 1) AS Emp, " & vbNewLine & _
    "ROUND((ns_Cantidad - (ROUND(ns_Cantidad / ns_CantidadEmpaque, 0, 1) * ns_CantidadEmpaque)), ns_Decimales, 0) AS Und, " & vbNewLine & _
    "ROW_NUMBER() OVER (ORDER BY ns_Linea) AS NewRowID " & vbNewLine & _
    "FROM ProXLoc_Solicitud " & vbNewLine & _
    "), ProXLoc_Trans AS ( " & vbNewLine & _
    "SELECT BASE.cs_CodLocalidad, BASE.cs_Corrida, BASE.c_CodLocalidad_Destino, " & vbNewLine & _
    "DET.c_CodArticulo, SUM(DET.n_Cantidad) AS ns_Cantidad, MIN(DET.c_Linea) AS c_Linea, " & vbNewLine & _
    "CASE WHEN MAX(DET.ns_CantidadEmpaque) > 0 THEN MAX(DET.ns_CantidadEmpaque) " & vbNewLine & _
    "ELSE 1 END AS ns_CantidadEmpaque " & vbNewLine
    
    mSQL = mSQL & _
    "FROM BASE " & vbNewLine & _
    "INNER JOIN MA_INVENTARIO CAB " & vbNewLine & _
    "ON BASE.c_Documento_Trans = CAB.c_Documento " & vbNewLine & _
    "AND CAB.c_Concepto = 'TRA' " & vbNewLine & _
    "AND BASE.cs_CodLocalidad = CAB.c_CodLocalidad " & vbNewLine & _
    "INNER JOIN TR_INVENTARIO DET " & vbNewLine & _
    "ON BASE.c_Documento_Trans = DET.c_Documento " & vbNewLine & _
    "AND DET.c_Concepto = 'TRA' " & vbNewLine & _
    "AND DET.c_TipoMov = 'Cargo' " & vbNewLine & _
    "AND BASE.cs_CodLocalidad = DET.c_CodLocalidad " & vbNewLine & _
    "GROUP BY BASE.cs_CodLocalidad, BASE.cs_Corrida, BASE.c_CodLocalidad_Destino, DET.c_CodArticulo " & vbNewLine & _
    "), ProXLoc_Trans2 AS ( " & vbNewLine & _
    "SELECT *, ROUND(ns_Cantidad / ns_CantidadEmpaque, 0, 1) AS Emp, " & vbNewLine & _
    "ROUND((ns_Cantidad - (ROUND(ns_Cantidad / ns_CantidadEmpaque, 0, 1) * ns_CantidadEmpaque)), 8, 0) AS Und, " & vbNewLine & _
    "ROW_NUMBER() OVER (PARTITION BY cs_Corrida, c_CodLocalidad_Destino ORDER BY c_Linea) AS NewRowID " & vbNewLine & _
    "FROM ProXLoc_Trans " & vbNewLine
    
    mSQL = mSQL & _
    "), ProxLoc AS ( " & vbNewLine & _
    "SELECT SOL.cs_CodLocalidad, SOL.cs_Corrida, SOL.c_CodLocalidad_Destino, SOL.cs_CodArticulo AS CodProducto, " & vbNewLine & _
    "SOL.ns_Cantidad AS Cant_Sol, 0 AS Cant_Tra, 0 AS Emp_Tra, 0 AS Und_Tra, 0 AS ns_CantidadEmpaque, 0 AS NewRowID " & vbNewLine & _
    "FROM ProXLoc_Solicitud2 SOL " & vbNewLine & _
    "UNION ALL " & vbNewLine & _
    "SELECT TRA.cs_CodLocalidad, TRA.cs_Corrida, TRA.c_CodLocalidad_Destino, TRA.c_CodArticulo AS CodProducto, " & vbNewLine & _
    "0 AS Cant_Sol, TRA.ns_Cantidad AS Cant_Tra, TRA.Emp AS Emp_Tra, TRA.Und AS Und_Tra, TRA.ns_CantidadEmpaque, TRA.NewRowID AS NewRowID " & vbNewLine & _
    "FROM ProXLoc_Trans2 TRA " & vbNewLine & _
    "), ProXLoc2 AS ( " & vbNewLine & _
    "SELECT cs_CodLocalidad, cs_Corrida, c_CodLocalidad_Destino, CodProducto, SUM(Cant_Sol) AS CantSolicitada, " & vbNewLine & _
    "SUM(Cant_Tra) AS CantTransferencia, SUM(ns_CantidadEmpaque) AS CantxEmp, " & vbNewLine & _
    "SUM(Emp_Tra) AS EmpaquesTransferencia, SUM(Und_Tra) AS UnidadesTransferencia, SUM(NewRowID) AS Linea " & vbNewLine & _
    "FROM ProxLoc " & vbNewLine & _
    "GROUP BY cs_CodLocalidad, cs_Corrida, c_CodLocalidad_Destino, CodProducto " & vbNewLine & _
    "), TMP_TR_LOTE_GESTION_TRANSFERENCIA_DESTINO_DETALLE AS ( " & vbNewLine & _
    "SELECT * FROM ProXLoc2 " & vbNewLine & _
    "WHERE c_CodLocalidad_Destino = '" & CodLocalidadDestino & "' " & vbNewLine & _
    ") "
    
    Dim mCampoCantiBul As String
    
    mCampoCantiBul = "(CASE WHEN TR.CantXEmp > 0 THEN TR.CantXEmp " & _
    "ELSE CASE WHEN PRO.n_CantiBul > 0 THEN PRO.n_CantiBul ELSE 1 END END)"
    
    mSQL = mSQL & "SELECT " & mCampoCantiBul & " AS CantiBul, " & _
    "ROUND(TR.CantSolicitada / " & mCampoCantiBul & ", 0, 1) AS EmpSol, " & _
    "ROUND(((TR.CantSolicitada / " & mCampoCantiBul & ") - " & _
    "ROUND(TR.CantSolicitada / " & mCampoCantiBul & ", 0, 1)) * " & mCampoCantiBul & ", PRO.Cant_Decimales, 0) AS UndSol, " & _
    "ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1) AS EmpTra, " & _
    "ROUND(((TR.CantTransferencia / " & mCampoCantiBul & ") - " & _
    "ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1)) * " & mCampoCantiBul & ", PRO.Cant_Decimales, 0) AS UndTra, " & _
    "TR.*, PRO.Cant_Decimales, PRO.n_Peso, PRO.n_Volumen, PRO.n_PesoBul, PRO.n_VolBul, " & _
    "PRO.n_CostoAct, PRO.n_CostoRep, PRO.n_CostoPro, PRO.n_CostoAnt, " & _
    "((ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1) * PRO.n_PesoBul) + " & _
    "((ROUND(((TR.CantTransferencia / " & mCampoCantiBul & ") - " & _
    "ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1)) * " & mCampoCantiBul & ", PRO.Cant_Decimales, 0)) * PRO.n_Peso)) AS Peso, " & _
    "((ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1) * PRO.n_VolBul) + " & _
    "((ROUND(((TR.CantTransferencia / " & mCampoCantiBul & ") - " & _
    "ROUND(TR.CantTransferencia / " & mCampoCantiBul & ", 0, 1)) * " & mCampoCantiBul & ", PRO.Cant_Decimales, 0)) * PRO.n_Volumen)) AS Volumen, " & _
    "(TR.CantTransferencia * PRO.n_CostoAct * MON.n_Factor) AS Total, " & _
    "(PRO.n_CostoAct * MON.n_Factor) As CostoActFactorizado, " & _
    "PRO.c_Modelo AS c_Modelo, PRO.c_Marca AS c_Marca, PRO.c_Descri, " & _
    "isNULL(EDI.c_Codigo, TR.CodProducto) AS cs_CodEDI "
    
    mSQL = mSQL & _
    "FROM TMP_TR_LOTE_GESTION_TRANSFERENCIA_DESTINO_DETALLE TR " & _
    "INNER JOIN MA_PRODUCTOS PRO " & _
    "ON PRO.c_Codigo = TR.CodProducto " & _
    "LEFT JOIN MA_MONEDAS MON " & _
    "ON PRO.c_CodMoneda = MON.c_CodMoneda " & _
    "LEFT JOIN MA_CODIGOS EDI " & _
    "ON EDI.c_CodNasa = TR.CodProducto " & _
    "AND EDI.nu_Intercambio = 1 " & _
    "WHERE TR.cs_Corrida = '" & Documento & "' " & _
    "AND TR.cs_CodLocalidad = '" & CodLocalidadOrigen & "' " & _
    "ORDER BY TR.Linea "
    
    mRsTrRequisicion.Open mSQL, Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
    
    'Debug.Print mRsTrRequisicion.Source
    
    If mRsMaRequisicion.EOF Then
        mRsMaRequisicion.Close
        Exit Sub
    End If
    
    'Nombre_Usuario = FrmAppLink.Buscar_Usuario(mRsMaRequisicion!c_Usuario)
    
    'Titulo = "* R E Q U I S I C I O N *"
    Titulo = FrmAppLink.StellarMensaje(4065)
    
    SQL = "SELECT Clave, Texto, Ruta, SeImprime " & _
    "FROM ESTRUC_REPORT WHERE Clave = 'LT_TRA' "
    
    Call Apertura_RecordsetC(RsDocumento)
    RsDocumento.Open SQL, Ent.BDD, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not RsDocumento.EOF Then
        ImpXPlantilla = RsDocumento.Fields!SeImprime
    Else
        ImpXPlantilla = False
    End If
    
    If Dispositivo = 2 Then ' -- ES XML, NO ES PLANTILLA, AHORRARSE EL RTF
        ImpXPlantilla = False
    ElseIf Dispositivo = 3 Then ' -- ES ETIQUETA - RECOGER VARIABLES
        ImpXPlantilla = True
    End If
    
    Dim Rep As Object
    
    If ImpXPlantilla Then
        Set Rep = FrmAppLink.ClaseReporteRTF 'New Reporte
        Rep.Configurar_Documentos "LT_CON_TRA"
        Rep.AddNewMaster
    Else
        AnuladaRegladeJuego = FrmAppLink.RsReglasComerciales!Factura_Encabezado
    End If
    
    If Not mRsMaRequisicion.EOF Then
        
        '*************************************
        '*** DATOS DE LA EMPRESA
        '*************************************
        
        Call Apertura_Recordset(RsEmpresa)
        
        RsEmpresa.Open "ESTRUC_SIS", Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdTable
        
        If ImpXPlantilla Then
            
            Dim H1077 As String, H1001 As String, H1002 As String, H1003 As String, _
            H1010 As String, H1011(2) As String, H1012(4) As String, H1013(2) As String
            
            Dim H1014(5) As String, H1015(5) As String, H1016(5) As String, _
            H1017(9) As String, H1018(6) As String
            
            Dim H1020 As String, H1021 As String, H1022 As String, H1023 As String, _
            H1024 As String, H1025 As String, H1026 As String, H1027 As String, _
            H1028 As String, H1029 As String, H1030 As String, H1031 As String, _
            H1032 As String
            
            Dim D1001 As String, D1002 As String, D1003 As String, D1004 As String, _
            D1005 As String, D1006 As String, D1007 As String, D1008 As String, _
            D1009 As String, D1010 As String, D1011 As String, D1012 As String, _
            D1013 As String, D1014 As String, D1015 As String, D1016 As String, _
            D1017 As String, D1018 As String, D1019 As String, D1020 As String, _
            D1021 As String, D1022 As String, D1023 As String, D1024 As String, _
            D1025 As String, D1026 As String
            
            H1077 = TipoImpresion
            H1001 = IIf(Len(RsEmpresa!nom_org) > 1, RsEmpresa!nom_org, Empty)
            H1002 = IIf(Len(RsEmpresa!dir_org) > 1, RsEmpresa!dir_org, Empty)
            H1003 = IIf(Len(RsEmpresa!tlf_org) > 1, RsEmpresa!tlf_org, Empty)
            
            Rep.FillMaster "H1077", H1077
            
            Rep.FillMaster "H1001", H1001
            Rep.FillMaster "H1002", H1002
            Rep.FillMaster "H1003", H1003
            
        Else
            
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("lblTipoImpresion").Caption = TipoImpresion
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_EMPRESA").Caption = RsEmpresa!nom_org
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_DIRECCION").Caption = RsEmpresa!dir_org
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_TELEFONOS").Caption = "TELEFONOS:" & RsEmpresa!tlf_org
            
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("Etiqueta1").Caption = "LOCALIDAD ORIGEN: "
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("Etiqueta4").Caption = "LOCALIDAD A DESPACHAR: "
            
            '"LOTE DE CONSOLA DE TRANSFERENCIAS No "
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("QUE_DOCUMENTO").Caption = FrmAppLink.StellarMensaje(4065) & " No. "
            Documento_Lote_Consola_Transferencia.Caption = FrmAppLink.StellarMensaje(4065)
            
        End If

        Call Cerrar_Recordset(RsEmpresa)
        
        Call Apertura_RecordsetC(RsCompras)
        
        mSQL = ";WITH Lote AS ( " & vbNewLine & _
        "SELECT MA.* " & vbNewLine & _
        "FROM MA_LOTE_GESTION_TRANSFERENCIA MA " & vbNewLine & _
        "WHERE MA.cs_Corrida = '" & Documento & "' " & vbNewLine & _
        "AND MA.b_Finalizada = 1 " & vbNewLine & _
        "), Base AS ( " & vbNewLine & _
        "SELECT MA.*, '___' AS TmpSplit1, TR.c_Documento AS cs_Solicitud, " & _
        "TR.c_Documento_Nota, TR.c_Documento_Trans, TR.c_CodLocalidad_Destino, " & vbNewLine & _
        "TR.cs_CodLocalidad , TR.cs_CodLocalidad_Solicitud " & vbNewLine & _
        "FROM Lote MA " & vbNewLine & _
        "INNER JOIN TR_LOTE_GESTION_TRANSFERENCIA TR " & vbNewLine & _
        "ON MA.cs_Corrida = TR.cs_Corrida " & vbNewLine & _
        "), TMP_TR_LOTE_GESTION_TRANSFERENCIA_DESTINO AS ( " & vbNewLine & _
        "SELECT [cs_CodLocalidad], [cs_Corrida], [cs_Solicitud] " & vbNewLine & _
        "FROM Base " & vbNewLine & _
        "WHERE c_CodLocalidad_Destino = '" & CodLocalidadDestino & "' " & vbNewLine & _
        ") "
        
        mSQL = mSQL & _
        "SELECT * FROM TMP_TR_LOTE_GESTION_TRANSFERENCIA_DESTINO " & _
        "WHERE cs_Corrida = '" & Documento & "' " & _
        "AND cs_CodLocalidad = '" & CodLocalidadOrigen & "'; "
        
        RsCompras.Open mSQL, Ent.BDD, adOpenStatic, adLockReadOnly, adCmdText
        
        Dim mSolicitudes As String
        
        mSolicitudes = Empty
        
        Do While Not RsCompras.EOF
            
            mSolicitudes = mSolicitudes & ", " & RsCompras!cs_Solicitud
            
            RsCompras.MoveNext
            
        Loop
        
        Call Cerrar_Recordset(RsCompras)
        
        mSolicitudes = Mid(mSolicitudes, 3)
        
        '*************************************
        '*** DATOS DEL DOCUMENTO
        '*************************************
        
        If ImpXPlantilla Then
            
            H1010 = mRsMaRequisicion!cs_Corrida
            H1011(1) = mRsMaRequisicion!cs_CodLocalidad
            H1011(2) = mRsMaRequisicion!OrigenDes
            H1012(1) = GDate(mRsMaRequisicion!d_FechaCorrida)
            H1012(2) = SDate(mRsMaRequisicion!d_FechaCorrida)
            H1012(3) = GTime(mRsMaRequisicion!d_FechaCorrida)
            H1012(4) = STime(mRsMaRequisicion!d_FechaCorrida)
            H1013(1) = mRsMaRequisicion!c_Transporte
            H1013(2) = mRsMaRequisicion!Transporte
            
            Rep.FillMaster "H1010", H1010
            Rep.FillMaster "H1011-1", H1011(1)
            Rep.FillMaster "H1011-2", H1011(2)
            Rep.FillMaster "H1012-1", H1012(1)
            Rep.FillMaster "H1012-2", H1012(2)
            Rep.FillMaster "H1012-3", H1012(3)
            Rep.FillMaster "H1012-4", H1012(4)
            Rep.FillMaster "H1013-1", H1013(1)
            Rep.FillMaster "H1013-2", H1013(2)
            
            H1014(0) = mRsMaRequisicion!c_Observacion
            Rep.FillMaster "H1014", H1014(0)
            
            mLongitud = 0 'Val(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "40"))
            If mLongitud = 0 Then mLongitud = 40
            
            mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, _
            mRsMaRequisicion!c_Observacion & " ", False), vbNewLine)
            
            For I = 0 To 4
                
                mVariable = "H1014-" & Format(I + 1, "#")
                
                If I <= UBound(mCadena) Then
                    H1014(I + 1) = mCadena(I)
                    Rep.FillMaster CStr(mVariable), H1014(I + 1)
                Else
                    H1014(I + 1) = Empty
                    Rep.FillMaster CStr(mVariable), H1014(I + 1)
                End If
                
            Next I
            
            H1015(0) = mRsMaRequisicion!c_Clasificacion
            Rep.FillMaster "H1015", H1015(0)
            
            mLongitud = 0 'Val(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "40"))
            If mLongitud = 0 Then mLongitud = 40
            
            mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, _
            mRsMaRequisicion!c_Clasificacion & " ", False), vbNewLine)
            
            For I = 0 To 4
                
                mVariable = "H1015-" & Format(I + 1, "#")
                
                If I <= UBound(mCadena) Then
                    H1015(I + 1) = mCadena(I)
                    Rep.FillMaster CStr(mVariable), CStr(mCadena(I))
                Else
                    H1015(I + 1) = Empty
                    Rep.FillMaster CStr(mVariable), H1015(I + 1)
                End If
                
            Next I
            
            H1016(0) = mSolicitudes
            Rep.FillMaster "H1016", mSolicitudes
            
            mLongitud = 0 'Val(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "40"))
            If mLongitud = 0 Then mLongitud = 40
            
            mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, _
            mSolicitudes & " ", False), vbNewLine)
            
            For I = 0 To 4
                
                mVariable = "H1016-" & Format(I + 1, "#")
                
                If I <= UBound(mCadena) Then
                    H1016(I + 1) = mCadena(I)
                    Rep.FillMaster CStr(mVariable), H1016(I + 1)
                Else
                    H1016(I + 1) = Empty
                    Rep.FillMaster CStr(mVariable), H1016(I + 1)
                End If
                
            Next I
            
            H1017(1) = mRsMaRequisicion!c_CodLocalidad_Destino
            H1017(2) = mRsMaRequisicion!DestinoDes
            H1017(3) = mRsMaRequisicion!DireccionDes
            H1017(4) = mRsMaRequisicion!TelefonoDes
            H1017(5) = mRsMaRequisicion!CiudadDes
            H1017(6) = mRsMaRequisicion!RifDes
            H1017(7) = mRsMaRequisicion!RazonSocialDes
            H1017(8) = mRsMaRequisicion!GerenteDes
            H1017(9) = mRsMaRequisicion!SubGerenteDes
            
            Rep.FillMaster "H1017-1", H1017(1)
            Rep.FillMaster "H1017-2", H1017(2)
            Rep.FillMaster "H1017-3", H1017(3)
            Rep.FillMaster "H1017-4", H1017(4)
            Rep.FillMaster "H1017-5", H1017(5)
            Rep.FillMaster "H1017-6", H1017(6)
            Rep.FillMaster "H1017-7", H1017(7)
            Rep.FillMaster "H1017-8", H1017(8)
            Rep.FillMaster "H1017-9", H1017(9)
            
            ' H1018
            
            mLongitud = 0 'Val(BuscarReglaNegocioStr("Ventas_LongitudDireccion", "40"))
            If mLongitud = 0 Then mLongitud = 40
            
            mCadena = Split(SplitStringIntoFixedLengthLines(mLongitud, _
            H1017(3) & " ", False), vbNewLine)
            
            For I = 0 To 5
                
                mVariable = "H1018-" & Format(I + 1, "#")
                
                If I <= UBound(mCadena) Then
                    H1018(I + 1) = mCadena(I)
                    Rep.FillMaster CStr(mVariable), H1018(I + 1)
                Else
                    H1018(I + 1) = Empty
                    Rep.FillMaster CStr(mVariable), H1018(I + 1)
                End If
                
            Next I
            
        Else
            
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_DOCUMENTO").Caption = Trim(mRsMaRequisicion!cs_Corrida)
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_FECHA").Caption = SDate(mRsMaRequisicion!d_FechaCorrida)
            
            '*************************************
            '*** DATOS DE LA LOCALIDAD
            '*************************************
            
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_DIRECCION_ORIGEN").Caption = mRsMaRequisicion!OrigenDes
            Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("LBL_DIRECCION_DESTINO").Caption = mRsMaRequisicion!DestinoDes
            
            '*************************************
            '*** DATOS DEL PIE DE PAGINA
            '*************************************
            
            Documento_Lote_Consola_Transferencia.Sections("PIE_INF").Controls("lbl_Solicitudes").Caption = mSolicitudes
            Documento_Lote_Consola_Transferencia.Sections("PIE_INF").Controls("LBL_OBSERVACION").Caption = _
            IIf(IsNull(mRsMaRequisicion!c_Observacion), Empty, mRsMaRequisicion!c_Observacion)
            
            '********** usuario *******************
            Documento_Lote_Consola_Transferencia.Sections("PIE_PAG").Controls("usuario").Caption = Empty 'Nombre_Usuario
            '********** ***************************
            
        End If
        
'        Select Case mFormaTrabajo
'            Case 0
'                Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("lblCantibul").Visible = False
'                Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("lblEmpq").Visible = False
'                Documento_Lote_Consola_Transferencia.Sections("ENC_PAG").Controls("lblDescri").Width = 4150
'                Documento_Lote_Consola_Transferencia.Sections("DETALLE").Controls("txtCantibul").Visible = False
'                Documento_Lote_Consola_Transferencia.Sections("DETALLE").Controls("txtEmpq").Visible = False
'                Documento_Lote_Consola_Transferencia.Sections("DETALLE").Controls("txtUnid").DataField = "ns_Cantidad"
'        End Select
        
        '*************************************
        '*** DETALLE DE LA ORDEN
        '*************************************
        
        Dim Total_CantSol As Double, Total_CantTra As Double, Total_DifCant As Double
        Dim Total_EmpSol As Double, Total_UndSol As Double, Total_EmpTra As Double, _
        Total_UndTra As Double, Total_DifEmp As Double, Total_DifUnd As Double
        Dim Total_Peso As Double, Total_Volumen As Double, Total_Costo As Double
        
        Dim mDetalles As Collection, mItem As Variant ' Dictionary
        
        Set mDetalles = New Collection
        
        If Not mRsTrRequisicion.EOF Then
            
            If ImpXPlantilla Then
                
                While Not mRsTrRequisicion.EOF
                    
                    If ImpXPlantilla Then
                        
                        Rep.AddNewDetail
                        
                        Set mItem = New Dictionary
                        
                        Dim CantSol As Double, CantTra As Double, DifCant As Double
                        Dim EmpSol As Double, UndSol As Double, EmpTra As Double, _
                        UndTra As Double, DifEmp As Double, DifUnd As Double
                        Dim Peso As Double, Volumen As Double, Costo As Double
                        
                        CantSol = mRsTrRequisicion!CantSolicitada
                        CantTra = mRsTrRequisicion!CantTransferencia
                        DifCant = CDec(CantTra) - CDec(CantSol)
                        EmpSol = mRsTrRequisicion!EmpSol
                        UndSol = mRsTrRequisicion!UndSol
                        EmpTra = mRsTrRequisicion!EmpTra
                        UndTra = mRsTrRequisicion!UndTra
                        DifEmp = CDec(EmpTra) - CDec(EmpSol)
                        DifUnd = CDec(UndTra) - CDec(UndSol)
                        Peso = mRsTrRequisicion!Peso
                        Volumen = mRsTrRequisicion!Volumen
                        Costo = mRsTrRequisicion!Total
                        
                        Total_CantSol = Total_CantSol + CantSol
                        Total_CantTra = Total_CantTra + CantTra
                        Total_DifCant = CDec(Total_DifCant) + CDec(DifCant)
                        Total_EmpSol = Total_EmpSol + EmpSol
                        Total_UndSol = Total_UndSol + UndSol
                        Total_EmpTra = Total_EmpTra + EmpTra
                        Total_UndTra = Total_UndTra + UndTra
                        Total_DifEmp = CDec(Total_DifEmp) + CDec(DifEmp)
                        Total_DifUnd = CDec(Total_DifUnd) + CDec(DifUnd)
                        Total_Peso = Total_Peso + Peso
                        Total_Volumen = Total_Volumen + Volumen
                        Total_Costo = Total_Costo + Costo
                        
                        mItem("D1001") = mRsTrRequisicion!CodProducto
                        mItem("D1002") = mRsTrRequisicion!c_Descri
                        mItem("D1003") = FormatoDecimalesDinamicos(CantSol)
                        mItem("D1004") = FormatoDecimalesDinamicos(CantTra)
                        mItem("D1005") = FormatoDecimalesDinamicos(DifCant)
                        mItem("D1006") = mRsTrRequisicion!c_Marca
                        mItem("D1007") = mRsTrRequisicion!c_Modelo
                        mItem("D1008") = mRsTrRequisicion!cs_CodEdi
                        mItem("D1009") = FormatoDecimalesDinamicos(EmpSol)
                        mItem("D1010") = FormatoDecimalesDinamicos(UndSol)
                        mItem("D1011") = FormatoDecimalesDinamicos(EmpTra)
                        mItem("D1012") = FormatoDecimalesDinamicos(UndTra)
                        mItem("D1013") = mRsTrRequisicion!CantiBul
                        mItem("D1014") = FormatoDecimalesDinamicos(DifEmp)
                        mItem("D1015") = FormatoDecimalesDinamicos(DifUnd)
                        mItem("D1016") = FormatoDecimalesDinamicos(Peso)
                        mItem("D1017") = FormatoDecimalesDinamicos(Volumen)
                        mItem("D1018") = FormatoDecimalesDinamicos(mRsTrRequisicion!n_Peso)
                        mItem("D1019") = FormatoDecimalesDinamicos(mRsTrRequisicion!n_Volumen)
                        mItem("D1020") = FormatoDecimalesDinamicos(mRsTrRequisicion!n_PesoBul)
                        mItem("D1021") = FormatoDecimalesDinamicos(mRsTrRequisicion!n_VolBul)
                        mItem("D1022") = FormatoDecimalesDinamicos(mRsTrRequisicion!Cant_Decimales)
                        mItem("D1023") = FormatoDecimalesDinamicos(mRsTrRequisicion!CostoActFactorizado)
                        mItem("D1024") = FormatoDecimalesDinamicos(Costo)
                        mItem("D1025") = IIf(DifCant <> 0, "(**)", Empty) ' Indicador de Diferencia
                        mItem("D1026") = FormatoDecimalesDinamicos(mDetalles.Count + 1)
                        
                        Rep.FillDetails "D1001", mItem("D1001")
                        Rep.FillDetails "D1002", mItem("D1002")
                        Rep.FillDetails "D1003", mItem("D1003")
                        Rep.FillDetails "D1004", mItem("D1004")
                        Rep.FillDetails "D1005", mItem("D1005")
                        Rep.FillDetails "D1006", mItem("D1006")
                        Rep.FillDetails "D1007", mItem("D1007")
                        Rep.FillDetails "D1008", mItem("D1008")
                        Rep.FillDetails "D1009", mItem("D1009")
                        Rep.FillDetails "D1010", mItem("D1010")
                        Rep.FillDetails "D1011", mItem("D1011")
                        Rep.FillDetails "D1012", mItem("D1012")
                        Rep.FillDetails "D1013", mItem("D1013")
                        Rep.FillDetails "D1014", mItem("D1014")
                        Rep.FillDetails "D1015", mItem("D1015")
                        Rep.FillDetails "D1016", mItem("D1016")
                        Rep.FillDetails "D1017", mItem("D1017")
                        Rep.FillDetails "D1018", mItem("D1018")
                        Rep.FillDetails "D1019", mItem("D1019")
                        Rep.FillDetails "D1020", mItem("D1020")
                        Rep.FillDetails "D1021", mItem("D1021")
                        Rep.FillDetails "D1022", mItem("D1022")
                        Rep.FillDetails "D1023", mItem("D1023")
                        Rep.FillDetails "D1024", mItem("D1024")
                        Rep.FillDetails "D1025", mItem("D1025")
                        Rep.FillDetails "D1026", mItem("D1026")
                        
                        mDetalles.Add mItem
                        
                    End If
                    
                    mRsTrRequisicion.MoveNext
                    
                Wend
                
                H1020 = FormatoDecimalesDinamicos(Total_CantSol, 0, 4)
                H1021 = FormatoDecimalesDinamicos(Total_CantTra, 0, 4)
                H1022 = FormatoDecimalesDinamicos(Total_DifCant, 0, 4)
                
                H1023 = FormatoDecimalesDinamicos(Total_EmpSol, 0, 4)
                H1024 = FormatoDecimalesDinamicos(Total_UndSol, 0, 4)
                H1025 = FormatoDecimalesDinamicos(Total_EmpTra, 0, 4)
                H1026 = FormatoDecimalesDinamicos(Total_UndTra, 0, 4)
                
                H1027 = FormatoDecimalesDinamicos(Total_DifEmp, 0, 4)
                H1028 = FormatoDecimalesDinamicos(Total_DifUnd, 0, 4)
                
                H1029 = FormatoDecimalesDinamicos(Total_Peso, 0, 4)
                H1030 = FormatoDecimalesDinamicos(Total_Volumen, 0, 4)
                H1031 = FormatoDecimalesDinamicos(Total_Costo, 0, 2)
                
                H1032 = FormatoDecimalesDinamicos(mDetalles.Count)
                
                Rep.FillMaster "H1020", H1020
                Rep.FillMaster "H1021", H1021
                Rep.FillMaster "H1022", H1022
                
                Rep.FillMaster "H1023", H1023
                Rep.FillMaster "H1024", H1024
                Rep.FillMaster "H1025", H1025
                Rep.FillMaster "H1026", H1026
                
                Rep.FillMaster "H1027", H1027
                Rep.FillMaster "H1028", H1028
                
                Rep.FillMaster "H1029", H1029
                Rep.FillMaster "H1030", H1030
                Rep.FillMaster "H1031", H1031
                
                Rep.FillMaster "H1032", H1032
                
            End If
            
            If ImpXPlantilla Then
                
                Set ShowRTF = FrmAppLink.GetFrmShowRTF
                FrmAppLink.SetRepObjRTF Rep
                
                If Dispositivo = 3 Then ' -- ES ETIQUETA
                    
                    Dim TmpImpresoraEtiqueta As String, TmpArchivoEtiqueta As String
                    
                    TmpImpresoraEtiqueta = FrmAppLink.sGetIni(FrmAppLink.GetSetup, _
                    "OPCION_IMP", "ImpresoraEtiquetaLoteTransferencia", Empty)
                    TmpArchivoEtiqueta = FrmAppLink.sGetIni(FrmAppLink.GetSetup, _
                    "OPCION_IMP", "RutaEtiquetaLoteTransferencia", Empty)
                    
                    If TmpImpresoraEtiqueta = Empty Then
                        TmpImpresoraEtiqueta = BuscarReglaNegocioStr( _
                        "TRA_ImpresoraEtiquetaDeLote", Empty)
                    End If
                    
                    If TmpArchivoEtiqueta = Empty Then
                        TmpArchivoEtiqueta = BuscarReglaNegocioStr( _
                        "TRA_RutaEtiquetaDeLote", "C:\RTF\EtiquetaLoteTransferencia.txt")
                    End If
                    
                    If PathExists(TmpArchivoEtiqueta) Then
                        
                        Dim mContenidoArchivo As String, mContenidoLineas As Variant
                        Dim mLineaResuelta As String, mLineaBase As String, TmpArrD As Variant
                        Dim InicioDetalle As Boolean
                        
                        Dim TmpDetallePorLinea As Boolean
                        Dim TmpLineaDetalleActual As Long
                        
                        mContenidoArchivo = LoadTextFile(TmpArchivoEtiqueta)
                        
                        mContenidoLineas = Split(mContenidoArchivo, vbNewLine, , vbTextCompare)
                        
                        mContenidoArchivo = Empty
                        
                        For I = 0 To UBound(mContenidoLineas)
                            
                            If InStr(1, mContenidoLineas(I), "||D||", vbTextCompare) > 0 Then
                                TmpDetallePorLinea = True
                            ElseIf InStr(1, mContenidoLineas(I), "|*D*|", vbTextCompare) > 0 Then
                                InicioDetalle = True
                                mContenidoLineas(I) = Replace(mContenidoLineas(I), "|*D*|", Empty, 1, 1, vbTextCompare)
                            End If
                            
                            mLineaResuelta = mContenidoLineas(I)
                            
                            mLineaResuelta = Replace(mLineaResuelta, "|H1001|", H1001, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1002|", H1002, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1003|", H1003, , , vbTextCompare)
                            
                            mLineaResuelta = Replace(mLineaResuelta, "|H1010|", H1010, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1011-1|", H1011(1), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1011-2|", H1011(2), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1012-1|", H1012(1), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1012-2|", H1012(2), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1012-3|", H1012(3), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1012-4|", H1012(4), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1013-1|", H1013(1), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1013-2|", H1013(2), , , vbTextCompare)
                            
                            mLineaResuelta = Replace(mLineaResuelta, "|H1014|", H1014(0), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1014-1|", H1014(1), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1014-2|", H1014(2), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1014-3|", H1014(3), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1014-4|", H1014(4), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1014-5|", H1014(5), , , vbTextCompare)
                            
                            mLineaResuelta = Replace(mLineaResuelta, "|H1015|", H1015(0), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1015-1|", H1015(1), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1015-2|", H1015(2), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1015-3|", H1015(3), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1015-4|", H1015(4), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1015-5|", H1015(5), , , vbTextCompare)
                            
                            mLineaResuelta = Replace(mLineaResuelta, "|H1016|", H1016(0), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1016-1|", H1016(1), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1016-2|", H1016(2), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1016-3|", H1016(3), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1016-4|", H1016(4), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1016-5|", H1016(5), , , vbTextCompare)
                            
                            mLineaResuelta = Replace(mLineaResuelta, "|H1017-1|", H1017(1), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1017-2|", H1017(2), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1017-3|", H1017(3), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1017-4|", H1017(4), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1017-5|", H1017(5), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1017-6|", H1017(6), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1017-7|", H1017(7), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1017-8|", H1017(8), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1017-9|", H1017(9), , , vbTextCompare)
                            
                            'mLineaResuelta = Replace(mLineaResuelta, "|H1018|", H1018(0), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1018-1|", H1018(1), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1018-2|", H1018(2), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1018-3|", H1018(3), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1018-4|", H1018(4), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1018-5|", H1018(5), , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1018-6|", H1018(6), , , vbTextCompare)
                            
                            mLineaResuelta = Replace(mLineaResuelta, "|H1020|", H1020, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1021|", H1021, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1022|", H1022, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1023|", H1023, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1024|", H1024, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1025|", H1025, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1026|", H1026, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1027|", H1027, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1028|", H1028, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1029|", H1029, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1030|", H1030, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1031|", H1031, , , vbTextCompare)
                            mLineaResuelta = Replace(mLineaResuelta, "|H1032|", H1032, , , vbTextCompare)
                            
                            mLineaResuelta = Replace(mLineaResuelta, "|H1077|", H1077, , , vbTextCompare)
                            
                            If InicioDetalle Then
                                
                                InicioDetalle = False
                                
                                mLineaBase = mLineaResuelta
                                
                                mLineaResuelta = Empty
                                
                                mCont = 0
                                
                                For Each mItem In mDetalles
                                    
                                    mCont = mCont + 1
                                    
InicioReemplazarDetalle:
                                    
                                    mLineaDetalle = mLineaBase
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1001|", CStr(mItem("D1001")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1002|", CStr(mItem("D1002")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1003|", CStr(mItem("D1003")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1004|", CStr(mItem("D1004")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1005|", CStr(mItem("D1005")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1006|", CStr(mItem("D1006")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1007|", CStr(mItem("D1007")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1008|", CStr(mItem("D1008")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1009|", CStr(mItem("D1009")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1010|", CStr(mItem("D1010")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1011|", CStr(mItem("D1011")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1012|", CStr(mItem("D1012")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1013|", CStr(mItem("D1013")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1014|", CStr(mItem("D1014")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1015|", CStr(mItem("D1015")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1016|", CStr(mItem("D1016")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1017|", CStr(mItem("D1017")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1018|", CStr(mItem("D1018")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1019|", CStr(mItem("D1019")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1020|", CStr(mItem("D1020")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1021|", CStr(mItem("D1021")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1022|", CStr(mItem("D1022")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1023|", CStr(mItem("D1023")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1024|", CStr(mItem("D1024")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1025|", CStr(mItem("D1025")), , , vbTextCompare)
                                    mLineaDetalle = Replace(mLineaDetalle, "|D1026|", CStr(mItem("D1026")), , , vbTextCompare)
                                    
FinReemplazarDetalle:
                                    
                                    If TmpDetallePorLinea Then
                                        TmpDetallePorLinea = False
                                        GoTo ContinuarTmpDetallePorLinea
                                    End If
                                    
                                    mLineaResuelta = mLineaResuelta & mLineaDetalle & _
                                    IIf(mCont < mDetalles.Count, vbNewLine, Empty)
                                    
                                Next
                                
                            ElseIf TmpDetallePorLinea Then
                                
                                TmpDetallePorLinea = False
                                
                                mLineaBase = mLineaResuelta
                                
                                mLineaResuelta = Empty
                                
                                TmpArrD = Split(mLineaBase, "||D||", , vbTextCompare)
                                
                                For K = 0 To UBound(TmpArrD)
                                    
                                    If K > 0 Then
                                        TmpLineaDetalleActual = TmpLineaDetalleActual + 1
                                    Else
                                        mLineaResuelta = mLineaResuelta & TmpArrD(0)
                                    End If
                                    
                                    mLineaDetalle = Empty
                                    
                                    TmpDetallePorLinea = True
                                    
                                    If K > 0 And TmpLineaDetalleActual <= mDetalles.Count Then
                                        
                                        Set mItem = mDetalles(TmpLineaDetalleActual)
                                        
                                        mLineaBase = TmpArrD(K)
                                        
                                        GoTo InicioReemplazarDetalle
                                        
ContinuarTmpDetallePorLinea:
                                        
                                        mLineaResuelta = mLineaResuelta & mLineaDetalle
                                        
                                    End If
                                    
                                Next
                                
                                If mLineaResuelta <> Empty Then
                                    mLineaResuelta = mLineaResuelta & vbNewLine
                                Else
                                    mLineaResuelta = "**[DELETE_LINE]**"
                                End If
                                
                                TmpDetallePorLinea = False
                                
                            End If
                            
                            If InStr(1, mLineaResuelta, "**[DELETE_LINE]**", vbTextCompare) <= 0 _
                            And Not Left(mLineaResuelta, 5) = "/*-*/" Then
                                mContenidoArchivo = mContenidoArchivo & _
                                IIf(I > 0, vbNewLine, Empty) & mLineaResuelta
                            End If
                            
                        Next I
                        
                        mContenidoLineas = Split(mContenidoArchivo, vbNewLine, , vbTextCompare)
                        
                        mUltimaImpresora = Printer.DeviceName
                        
                        If (Trim(TmpImpresoraEtiqueta) <> Empty) Then
                            Call BuscarImpresora(TmpImpresoraEtiqueta)
                        End If
                        
                        For mCampo = 0 To UBound(mContenidoLineas)
                            Printer.Print mContenidoLineas(mCampo)
                        Next
                        
                        Printer.EndDoc
                        
                        If (Trim(TmpImpresoraEtiqueta) <> Empty) Then
                            Call BuscarImpresora(mUltimaImpresora)
                        End If
                        
                        ' ...
                        
                    Else
                        
                        Mensaje True, FrmAppLink.StellarMensaje(294) & vbNewLine & TmpArchivoEtiqueta
                        
                    End If
                    
                ElseIf Dispositivo = 0 Then ' VER RTF PANTALLA
                    
                    ShowRTF.LoadFile RsDocumento.Fields!Ruta
                    ShowRTF.Show vbModal
                    
                Else ' IMPRIMIR RTF
                    
                    ShowRTF.LoadFile RsDocumento.Fields!Ruta
                    ShowRTF.PrintAllPage
                    
                End If
                
                FrmAppLink.SetRepObjRTF FrmAppLink.ClaseReporteRTF
                Set Rep = Nothing
                Set ShowRTF = Nothing
                
            Else
                
                If Dispositivo = 2 Then ' ES XML
                    
                    ' ESTUVO EN IDEA MAS NO HA SIDO SOPORTADO AUN
                    ' ESTO IMPLICARIA CONSTRUIR UN RECORDSET ANIDADO _
                    CON CABECERO Y DETALLE, Y REALMENTE LA GANANCIA NO _
                    ES SIGNIFICATIVA, ASI QUE POR LOS MOMENTOS NO SE HARA.
                    
                Else
                    
                    Set Documento_Lote_Consola_Transferencia.DataSource = mRsTrRequisicion
                    
                    'If SolicitudTransferencia_ReporteCodigoEDI Then
                        Documento_Lote_Consola_Transferencia.Sections("Detalle").Controls("Texto2").DataField = "cs_CodEdi"
                    'End If
                    
                    If Dispositivo = 0 Then
                        Documento_Lote_Consola_Transferencia.Show vbModal
                    Else
                        Documento_Lote_Consola_Transferencia.PrintReport True
                    End If
                    
                End If
                
                Set Documento_Lote_Consola_Transferencia = Nothing
                
            End If
            
        Else
            Call Mensaje(True, "No existen datos relacionados en el sistema.")
        End If
        
    Else
        Call Cerrar_Recordset(mRsMaRequisicion)
    End If
    
    Call Cerrar_Recordset(mRsTrRequisicion)
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Imprimir_Lote_Transferencia_Agrupado)"
    
    FrmAppLink.SetRepObjRTF FrmAppLink.ClaseReporteRTF
    Set Rep = Nothing
    Set ShowRTF = Nothing
    
    Set Documento_Lote_Consola_Transferencia = Nothing
    
    PrinterSecureKillDoc
    
End Sub
