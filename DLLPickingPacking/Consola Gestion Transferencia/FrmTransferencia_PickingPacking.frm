VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form FrmTransferencia_PickingPacking 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdGestionarLote 
      Caption         =   "Gestionar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   13920
      Picture         =   "FrmTransferencia_PickingPacking.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   18
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9600
      Width           =   1215
   End
   Begin VB.OptionButton OptOrden 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Volumen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   6
      Left            =   5160
      TabIndex        =   14
      Top             =   10320
      Width           =   1260
   End
   Begin VB.OptionButton OptOrden 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Destinos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   3720
      TabIndex        =   10
      Top             =   9960
      Width           =   1500
   End
   Begin VB.OptionButton OptOrden 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Peso"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   3720
      TabIndex        =   13
      Top             =   10320
      Width           =   1260
   End
   Begin VB.OptionButton OptOrden 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Art�culos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   2040
      TabIndex        =   12
      Top             =   10320
      Width           =   1500
   End
   Begin VB.OptionButton OptOrden 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Solicitudes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   2040
      TabIndex        =   9
      Top             =   9960
      Width           =   1500
   End
   Begin VB.OptionButton OptOrden 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Procesado"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   360
      TabIndex        =   11
      Top             =   10320
      Width           =   1500
   End
   Begin VB.OptionButton OptOrden 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Lote"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   360
      TabIndex        =   8
      Top             =   9960
      Value           =   -1  'True
      Width           =   1500
   End
   Begin VB.Timer TimerAlert 
      Interval        =   1000
      Left            =   13800
      Top             =   1200
   End
   Begin VB.Timer GlobalHotKeyTimer 
      Interval        =   25
      Left            =   0
      Top             =   1200
   End
   Begin VB.Frame FrameBusqueda 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   480
      TabIndex        =   31
      Top             =   580
      Width           =   8895
      Begin VB.CommandButton CmdTopRegistros 
         Caption         =   "Top 100"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   885
         Left            =   7680
         Picture         =   "FrmTransferencia_PickingPacking.frx":0CCA
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   120
         Width           =   1100
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   885
         Left            =   6480
         Picture         =   "FrmTransferencia_PickingPacking.frx":2A4C
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   120
         Width           =   1095
      End
      Begin VB.TextBox TextFechaDesde 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   120
         TabIndex        =   0
         Top             =   600
         Width           =   3000
      End
      Begin VB.TextBox TextFechaHasta 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   3360
         TabIndex        =   1
         Top             =   600
         Width           =   3000
      End
      Begin MSComCtl2.DTPicker desde 
         Height          =   315
         Left            =   120
         TabIndex        =   32
         Top             =   600
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarForeColor=   5790296
         CalendarTitleForeColor=   5790296
         Format          =   132841473
         CurrentDate     =   40709
      End
      Begin MSComCtl2.DTPicker hasta 
         Height          =   315
         Left            =   3360
         TabIndex        =   33
         Top             =   600
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarForeColor=   5790296
         CalendarTitleForeColor=   5790296
         Format          =   132841473
         CurrentDate     =   40709
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   260
         Width           =   1215
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   3360
         TabIndex        =   35
         Top             =   255
         Width           =   1455
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha de Creaci�n de Lote."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   0
         Width           =   4935
      End
   End
   Begin VB.Frame TipoPedido 
      Caption         =   "Tipo de Lote"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   9480
      TabIndex        =   30
      Top             =   480
      Width           =   4335
      Begin VB.OptionButton OptPendXCerrar 
         Caption         =   "Por Finalizar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   38
         Top             =   720
         Width           =   1815
      End
      Begin VB.OptionButton OptEnproceso 
         Caption         =   "En Proceso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   4
         Top             =   720
         Value           =   -1  'True
         Width           =   1575
      End
      Begin VB.OptionButton OptProcesados 
         Caption         =   "Procesados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   5
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.CommandButton ButtonLeyenda 
      Caption         =   "Leyenda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   11400
      Picture         =   "FrmTransferencia_PickingPacking.frx":47CE
      Style           =   1  'Graphical
      TabIndex        =   16
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9600
      Width           =   1095
   End
   Begin VB.Timer Timer1 
      Interval        =   500
      Left            =   6120
      Top             =   9480
   End
   Begin VB.TextBox txtminutos 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   9360
      TabIndex        =   15
      Text            =   "1"
      Top             =   10200
      Width           =   375
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   15360
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Consola Asignaci�n para Recolecci�n y Empacado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   120
         Width           =   5415
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmTransferencia_PickingPacking.frx":5498
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12600
         TabIndex        =   20
         Top             =   120
         Width           =   1815
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   7650
      LargeChange     =   10
      Left            =   14520
      TabIndex        =   7
      Top             =   1800
      Width           =   675
   End
   Begin VB.CommandButton ButtonActualizar 
      Caption         =   "Actualizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   12600
      Picture         =   "FrmTransferencia_PickingPacking.frx":721A
      Style           =   1  'Graphical
      TabIndex        =   17
      Tag             =   "Actualizar"
      ToolTipText     =   "Actualizar Movimientos"
      Top             =   9600
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   7665
      Left            =   210
      TabIndex        =   6
      Top             =   1800
      Width           =   15000
      _ExtentX        =   26458
      _ExtentY        =   13520
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ProgressBar bar 
      Height          =   255
      Left            =   9240
      TabIndex        =   24
      Top             =   9600
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.Label lblPackingMobile 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Mobile"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H002F39C0&
      Height          =   240
      Left            =   14040
      TabIndex        =   37
      Top             =   1200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Image ImgAlertaPackingMobile 
      Height          =   480
      Left            =   14280
      Picture         =   "FrmTransferencia_PickingPacking.frx":8F9C
      Top             =   720
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label LabelTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FAFAFA&
      Height          =   255
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   5295
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Pr�xima actualizaci�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   6840
      TabIndex        =   25
      Top             =   9600
      Width           =   2415
   End
   Begin VB.Shape Shape1 
      Height          =   240
      Index           =   4
      Left            =   6720
      Top             =   9600
      Visible         =   0   'False
      Width           =   4575
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Intervalo de actualizar: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   6840
      TabIndex        =   22
      Top             =   10200
      Width           =   2535
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "minutos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9840
      TabIndex        =   21
      Top             =   10200
      Width           =   975
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   735
      Index           =   0
      Left            =   6720
      TabIndex        =   23
      Top             =   10020
      Width           =   4575
   End
   Begin VB.Label C�digo 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Criterios de Ordenamiento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   285
      Index           =   71
      Left            =   480
      TabIndex        =   28
      Top             =   9600
      Width           =   2805
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00AE5B00&
      X1              =   3480
      X2              =   6120
      Y1              =   9720
      Y2              =   9720
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      ForeColor       =   &H80000008&
      Height          =   1335
      Index           =   1
      Left            =   240
      TabIndex        =   29
      Top             =   9480
      Width           =   6375
   End
   Begin VB.Menu TrayMenu 
      Caption         =   "Opciones de Bandeja"
      Visible         =   0   'False
      Begin VB.Menu mShow 
         Caption         =   "Mostrar"
      End
      Begin VB.Menu mExit 
         Caption         =   "Cerrar"
      End
   End
End
Attribute VB_Name = "FrmTransferencia_PickingPacking"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private OrderBy As String
Private SQL As String
Private Proceso As Boolean

Private Enum GridPPT
    ColNull
    ColLote
    ColFecha
    ColNumDoc
    ColDestinos
    ColPeso
    ColVolumen
    ColCantSolicitada
    ColCantRecolectada
    ColCantEmpacada
    ColProcesado
    ColHidden1
    ColFaseLote
    ColCount
End Enum

Private OrdenLabel() As String
Private OrdenCampo() As String

Private CodDepositoOrigen As String

Private Sub ButtonLeyenda_Click()
    
    On Error Resume Next
    
    Dim TempRs As ADODB.Recordset
    
    Set TempRs = Ent.BDD.Execute( _
    "SELECT c_Proceso " & _
    "FROM MA_ALERTAGRID_PROCESOS " & _
    "WHERE c_Proceso = '005' ")
    
    If Not TempRs.EOF Then frmLeyendaGridStellar.CodigoProcesoInicial = TempRs!c_Proceso
    
    frmLeyendaGridStellar.Show vbModal
    
    Set frmLeyendaGridStellar = Nothing
    
End Sub

Private Sub CmdBuscar_Click()
    
    If TextFechaDesde <> Empty And TextFechaHasta <> Empty Then
        
        SQL = QueryProcesados(Empty, True)
        
        Call PrepararGrid
        Call PrepararDatos(SQL)
        
    End If
    
End Sub

Private Sub CmdGestionarLote_Click()
    
    On Error GoTo Error
    
    Dim mNivel As Long
    Dim mFila As Long
    
    Dim ActiveTrans As Boolean
    
    mFila = Grid.Row
    
    If mFila < 1 Then
        Mensaje True, "Debe seleccionar una fila."
        Exit Sub
    End If
    
    If OptProcesados.Value Then
        
        Mensaje True, "Estas opciones solo est�n disponibles para lotes no procesados."
        
    Else
        
        FrmTransferencia_OpcionesLote.Show vbModal
        
        Select Case FrmTransferencia_OpcionesLote.ValorCaption
            
            Case "A" ' Cambiar Fase
                
                Set FrmTransferencia_OpcionesLote = Nothing
                
                Select Case Grid.TextMatrix(mFila, ColFaseLote)
                    
                    Case "3"
                        
                        Mensaje True, "Este lote ya esta cerrado. Acci�n no permitida."
                        
                    Case "2"
                        
                        ActivarMensajeGrande 80
                        
                        If Mensaje(False, "�Est� seguro de querer retroceder la fase " & _
                        "y volver a la Fase de Empacado? " & vbNewLine & vbNewLine & _
                        "Tome en cuenta que esto anular� cualquier Constancia de Empaque " & _
                        "que haya realizado y el empacado deber� iniciar desde cero. " & vbNewLine & vbNewLine & _
                        "Si est� seguro presione Aceptar") Then
                            
                            mNivel = Val(BuscarReglaNegocioStr("TRA_Consola_NivelReiniciarPacking", "9"))
                            
                            If mNivel > FrmAppLink.GetNivelUsuario Then
                                If Not gRutinas.ShowAutorizaciones(CInt(mNivel), Ent.BDD) Then
                                    Mensaje True, FrmAppLink.Stellar_Mensaje(11010) 'Usted no posee el nivel de usuario permitido para ejecutar esta Operaci�n.
                                    Set FrmTransferencia_ModificarLote = Nothing
                                    Exit Sub
                                End If
                            End If
                            
                            Ent.BDD.BeginTrans
                            ActiveTrans = True
                            
                            ' Borrar el detalle de cualquier constancia de empaque asociada al lote.
                            
                            Ent.BDD.Execute _
                            "DELETE DET " & _
                            "FROM MA_LOTE_GESTION_TRANSFERENCIA_PACKING MA " & _
                            "INNER JOIN MA_PACKING_TRANSFERENCIA PACK " & _
                            "ON MA.DocPacking = PACK.c_Documento " & _
                            "AND MA.cs_CodLocalidad_Solicitud = PACK.cs_CodLocalidad_Relacion " & _
                            "AND 'SOL_TRA' + ' ' + MA.CodPedido = PACK.c_Relacion " & _
                            "INNER JOIN TR_PACKING_TRANSFERENCIA DET " & _
                            "ON PACK.c_Documento = DET.c_Documento " & _
                            "AND PACK.cs_CodLocalidad = DET.cs_CodLocalidad " & _
                            "WHERE MA.CodLote = '" & Grid.TextMatrix(mFila, ColLote) & "' ", TmpRows
                            
                            ' Borrar el cabecero de cualquier constancia de empaque asociada al lote.
                            
                            Ent.BDD.Execute _
                            "DELETE PACK " & _
                            "FROM MA_LOTE_GESTION_TRANSFERENCIA_PACKING MA " & _
                            "INNER JOIN MA_PACKING_TRANSFERENCIA PACK " & _
                            "ON MA.DocPacking = PACK.c_Documento " & _
                            "AND MA.cs_CodLocalidad_Solicitud = PACK.cs_CodLocalidad_Relacion " & _
                            "AND 'SOL_TRA' + ' ' + MA.CodPedido = PACK.c_Relacion " & _
                            "WHERE MA.CodLote = '" & Grid.TextMatrix(mFila, ColLote) & "' ", TmpRows
                            
                            ' Borrar la relacion de lote y constancias de empaque eliminadas.
                            
                            Ent.BDD.Execute _
                            "DELETE MA " & _
                            "FROM MA_LOTE_GESTION_TRANSFERENCIA_PACKING MA " & _
                            "WHERE MA.CodLote = '" & Grid.TextMatrix(mFila, ColLote) & "' ", TmpRows
                            
                            ' Devolver todo a Sin Empacar
                            
                            Ent.BDD.Execute _
                            "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                            "Packing = 0 " & _
                            "WHERE CodLote = '" & Grid.TextMatrix(mFila, ColLote) & "' ", TmpRows
                            
                            ' Desmarcar Marca de Finalizacion del Proceso de Packing
                            
                            Ent.BDD.Execute _
                            "UPDATE MA_LOTE_GESTION_TRANSFERENCIA SET " & _
                            "b_PackingFinalizado = 0 " & _
                            "WHERE cs_Corrida = '" & Grid.TextMatrix(mFila, ColLote) & "' ", TmpRows
                            
                            Ent.BDD.CommitTrans
                            ActiveTrans = False
                            
                            If TmpRows > 0 Then
                                Mensaje True, "Se reinici� exitosamente la Fase de Empacado para este lote."
                                ButtonActualizar_Click
                            End If
                            
                        End If
                        
                    Case "1"
                        
                        If Mensaje(False, "�Est� seguro de querer retroceder la fase " & _
                        "y volver a la Fase de Recolecci�n? Si est� seguro presione Aceptar") Then
                            
                            mNivel = Val(BuscarReglaNegocioStr("TRA_Consola_NivelReiniciarPicking", "9"))
                            
                            If mNivel > FrmAppLink.GetNivelUsuario Then
                                If Not gRutinas.ShowAutorizaciones(CInt(mNivel), Ent.BDD) Then
                                    Mensaje True, FrmAppLink.Stellar_Mensaje(11010) 'Usted no posee el nivel de usuario permitido para ejecutar esta Operaci�n.
                                    Set FrmTransferencia_ModificarLote = Nothing
                                    Exit Sub
                                End If
                            End If
                            
                            Ent.BDD.BeginTrans
                            ActiveTrans = True
                            
                            ' Borrar el detalle de cualquier constancia de empaque asociada al lote.
                            
                            Ent.BDD.Execute _
                            "DELETE DET " & _
                            "FROM MA_LOTE_GESTION_TRANSFERENCIA_PACKING MA " & _
                            "INNER JOIN MA_PACKING_TRANSFERENCIA PACK " & _
                            "ON MA.DocPacking = PACK.c_Documento " & _
                            "AND MA.cs_CodLocalidad_Solicitud = PACK.cs_CodLocalidad_Relacion " & _
                            "AND 'SOL_TRA' + ' ' + MA.CodPedido = PACK.c_Relacion " & _
                            "INNER JOIN TR_PACKING_TRANSFERENCIA DET " & _
                            "ON PACK.c_Documento = DET.c_Documento " & _
                            "AND PACK.cs_CodLocalidad = DET.cs_CodLocalidad " & _
                            "WHERE MA.CodLote = '" & Grid.TextMatrix(mFila, ColLote) & "' ", TmpRows
                            
                            ' Borrar el cabecero de cualquier constancia de empaque asociada al lote.
                            
                            Ent.BDD.Execute _
                            "DELETE PACK " & _
                            "FROM MA_LOTE_GESTION_TRANSFERENCIA_PACKING MA " & _
                            "INNER JOIN MA_PACKING_TRANSFERENCIA PACK " & _
                            "ON MA.DocPacking = PACK.c_Documento " & _
                            "AND MA.cs_CodLocalidad_Solicitud = PACK.cs_CodLocalidad_Relacion " & _
                            "AND 'SOL_TRA' + ' ' + MA.CodPedido = PACK.c_Relacion " & _
                            "WHERE MA.CodLote = '" & Grid.TextMatrix(mFila, ColLote) & "' ", TmpRows
                            
                            ' Borrar la relacion de lote y constancias de empaque eliminadas.
                            
                            Ent.BDD.Execute _
                            "DELETE MA " & _
                            "FROM MA_LOTE_GESTION_TRANSFERENCIA_PACKING MA " & _
                            "WHERE MA.CodLote = '" & Grid.TextMatrix(mFila, ColLote) & "' ", TmpRows
                            
                            ' NUEVO: DESCOMPROMETER EL DEPOPROD
                            
                            SQLBase = _
                            "SELECT '" & CodDepositoOrigen & "' AS CodDeposito, " & _
                            "PP.CodProducto, SUM(PP.CantRecolectada) AS CantRecolectada " & _
                            "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & _
                            "WHERE PP.CodLote = '" & Grid.TextMatrix(mFila, ColLote) & "' " & _
                            "AND PP.Picking = 1 " & _
                            "GROUP BY PP.CodProducto "
                            
                            SQL = _
                            ";WITH RebuildData AS ( " & vbNewLine & _
                            SQLBase & vbNewLine & _
                            ") " & vbNewLine & _
                            "INSERT INTO MA_DEPOPROD (c_CodDeposito, c_CodArticulo, c_Descripcion, " & vbNewLine & _
                            "n_Cantidad, n_Cant_Comprometida, n_Cant_Ordenada) " & vbNewLine & _
                            "SELECT RD.CodDeposito AS c_CodDeposito, RD.CodProducto AS c_CodArticulo, " & vbNewLine & _
                            "'' AS c_Descripcion, 0 AS n_Cantidad, " & vbNewLine & _
                            "0 AS n_Cant_Comprometida, 0 AS n_Cant_Ordenada " & vbNewLine & _
                            "FROM RebuildData RD " & _
                            "LEFT JOIN MA_DEPOPROD DP " & vbNewLine & _
                            "ON RD.CodDeposito COLLATE MODERN_SPANISH_CI_AS = DP.c_CodDeposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
                            "AND RD.CodProducto COLLATE MODERN_SPANISH_CI_AS = DP.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
                            "WHERE DP.n_Cantidad IS NULL " & vbNewLine
                            
                            Ent.BDD.Execute SQL, TmpRows ' Ingresados
                            
                            SQL = _
                            ";WITH RebuildData AS ( " & vbNewLine & _
                            SQLBase & vbNewLine & _
                            ") " & vbNewLine & _
                            "UPDATE MA_DEPOPROD SET " & vbNewLine & _
                            "n_Cant_Comprometida = ROUND(n_Cant_Comprometida - RD.CantRecolectada, 8, 0) " & vbNewLine & _
                            "FROM RebuildData RD " & _
                            "INNER JOIN MA_DEPOPROD DP " & vbNewLine & _
                            "ON RD.CodDeposito COLLATE MODERN_SPANISH_CI_AS = DP.c_CodDeposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
                            "AND RD.CodProducto COLLATE MODERN_SPANISH_CI_AS = DP.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine
                            
                            Ent.BDD.Execute SQL, TmpRows ' Actualizados
                            
                            ' Devolver todo a Sin Recolectar
                            
                            Ent.BDD.Execute _
                            "UPDATE MA_LOTE_GESTION_TRANSFERENCIA_PICKING SET " & _
                            "Picking = 0, Packing = 0 " & _
                            "WHERE CodLote = '" & Grid.TextMatrix(mFila, ColLote) & "' ", TmpRows
                            
                            ' Desmarcar Marca de Finalizacion del Proceso de Picking / Packing
                            
                            Ent.BDD.Execute _
                            "UPDATE MA_LOTE_GESTION_TRANSFERENCIA SET " & _
                            "b_PickingFinalizado = 0, b_PackingFinalizado = 0 " & _
                            "WHERE cs_Corrida = '" & Grid.TextMatrix(mFila, ColLote) & "' ", TmpRows
                            
                            Ent.BDD.CommitTrans
                            ActiveTrans = False
                            
                            If TmpRows > 0 Then
                                Mensaje True, "Se reinici� exitosamente la Fase de Recolecci�n para este lote."
                                ButtonActualizar_Click
                            End If
                            
                        End If
                        
                    Case "0"
                        
                        ' N / A
                        
                        Mensaje True, "Este lote ya se encuentra en la fase inicial (Recolecci�n)."
                        
                End Select
                
            Case "B" ' Modificar / Crear Nuevo Lote
                
                FrmTransferencia_ModificarLote.Show vbModal
                
                If FrmTransferencia_ModificarLote.GeneroLote Then
                    Set FrmTransferencia_ModificarLote = Nothing
                    ButtonActualizar_Click
                Else
                    Set FrmTransferencia_ModificarLote = Nothing
                End If
                
            Case "C" ' Ver Lote
                
                Set FrmTransferencia_OpcionesLote = Nothing
                
                Grid_DblClick
                
            Case Else 'Empty
                
                ' Salir
                
                Set FrmTransferencia_OpcionesLote = Nothing
                
        End Select
        
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If ActiveTrans Then
        Ent.BDD.RollbackTrans
        ActiveTrans = False
    End If
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(GestionarLote)"
    
End Sub

Private Sub CmdTopRegistros_Click()
    
    SQL = QueryProcesados("TOP 100")
    
    Call PrepararGrid
    Call PrepararDatos(SQL)
    
End Sub

Private Sub Form_Load()
    
    CodDepositoOrigen = BuscarReglaNegocioStr("TRA_Consola_CodDepositoOrigen", Empty)
    
    If Trim(mvarCodigoDepositoOrigen) = Empty Then
        CodDepositoOrigen = FrmAppLink.GetCodDepositoPredeterminado
    End If
    
    ReDim OrdenLabel(OptOrden.UBound) As String
    ReDim OrdenCampo(OptOrden.UBound) As String
    
    OrdenLabel(0) = "Lote"
    OrdenLabel(1) = "Procesado"
    OrdenLabel(2) = "Solicitudes"
    OrdenLabel(3) = "Art�culos"
    OrdenLabel(4) = "Destinos"
    OrdenLabel(5) = "Peso"
    OrdenLabel(6) = "Volumen"
    
    OrdenCampo(0) = "Lote"
    OrdenCampo(1) = "Proceso"
    OrdenCampo(2) = "Pedidos"
    OrdenCampo(3) = "CantProductos"
    OrdenCampo(4) = "Destinos"
    OrdenCampo(5) = "Peso"
    OrdenCampo(6) = "Vol"
    
    OptOrden(0).Caption = OrdenLabel(0)
    OptOrden(1).Caption = OrdenLabel(1)
    OptOrden(2).Caption = OrdenLabel(2)
    OptOrden(3).Caption = OrdenLabel(3)
    OptOrden(4).Caption = OrdenLabel(4)
    OptOrden(5).Caption = OrdenLabel(5)
    OptOrden(6).Caption = OrdenLabel(6)
    
    OptOrden(0).Tag = "0"
    OptOrden(1).Tag = "1"
    OptOrden(2).Tag = "1"
    OptOrden(3).Tag = "1"
    OptOrden(4).Tag = "0"
    OptOrden(5).Tag = "1"
    OptOrden(6).Tag = "1"
    
    AjustarPantalla Me
    
    FrameBusqueda.Enabled = False
    
    OptOrden(0).Tag = "1"
    OptOrden_MouseUp 0, vbRightButton, 0, 0, 0
    OptOrden_Click 0
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .Clear
        
        .SelectionMode = flexSelectionByRow
        
        .FixedCols = 0
        
        .Rows = 1
        .Cols = GridPPT.ColCount
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        '.AllowUserResizing = flexResizeColumns
        
        .Row = 0
        
        .Col = ColNull
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColLote
        .ColWidth(.Col) = 1700
        .Text = "Lote"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColFecha
        .ColWidth(.Col) = 1700
        .Text = "Fecha"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColNumDoc
        .ColWidth(.Col) = 950
        .Text = "# Solic."
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDestinos
        .ColWidth(.Col) = 3160
        .Text = "Destinos"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColPeso
        .ColWidth(.Col) = 1300
        .Text = "Peso"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColVolumen
        .ColWidth(.Col) = 1300
        .Text = "Vol."
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantSolicitada
        .ColWidth(.Col) = 1300
        .Text = "Art. Sol"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantRecolectada
        .ColWidth(.Col) = 1300
        .Text = "Art. Rec"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCantEmpacada
        .ColWidth(.Col) = 1300
        .Text = "Art. Emp"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColProcesado
        .ColWidth(.Col) = 925
        .Text = "Proc."
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColHidden1
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColFaseLote
        .ColWidth(.Col) = 0
        .Text = Empty
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Width = 15000
        
        .ScrollTrack = True
        
        .Col = ColHidden1
        .ColSel = ColHidden1
        
    End With
    
    'For i = 0 To Grid.Cols - 1
        'Grid.Col = i
        'Grid.CellAlignment = flexAlignCenterCenter
    'Next
    
End Sub

Private Sub PrepararDatos(SQL As String)
    
    On Error GoTo Error1
    
    Dim Rs As New ADODB.Recordset
    
    Rs.Open SQL, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = Rs.RecordCount
    ScrollGrid.Value = 0
    
    Grid.Visible = False
    
    While Not Rs.EOF
    With Grid
        
        .Rows = .Rows + 1
        Linea = .Rows - 1
        
        .TextMatrix(Linea, ColNull) = Empty
        .TextMatrix(Linea, ColLote) = Rs!Lote
        .TextMatrix(Linea, ColFecha) = Rs!FECHA
        .TextMatrix(Linea, ColNumDoc) = FormatNumber(Rs!Pedidos, 0)
        .TextMatrix(Linea, ColDestinos) = Rs!Destinos
        .TextMatrix(Linea, ColPeso) = FormatoDecimalesDinamicos(Rs!Peso, 0, 4)
        .TextMatrix(Linea, ColVolumen) = FormatoDecimalesDinamicos(Rs!VOL, 0, 4)
        .TextMatrix(Linea, ColCantSolicitada) = FormatoDecimalesDinamicos(Rs!CantProductos, 0, 6)
        .TextMatrix(Linea, ColCantRecolectada) = FormatoDecimalesDinamicos(Rs!CantRecolectada, 0, 6)
        .TextMatrix(Linea, ColCantEmpacada) = FormatoDecimalesDinamicos(Rs!CantEmpacada, 0, 6)
        .TextMatrix(Linea, ColProcesado) = RoundUp(Rs!Proceso, 2) & "%"
        .TextMatrix(Linea, ColHidden1) = Empty
        
        If Proceso Then
            
            If Rs!Picking Then
                
                .TextMatrix(Linea, ColHidden1) = "1"
                
                If Rs!Fase = 2 Then
                    For I = 0 To Grid.Cols - 1
                        .Col = I
                        .CellBackColor = &HB4EDB9    'verde 11857337
                    Next I
                Else
                    For I = 0 To .Cols - 1
                        .Col = I
                        .CellBackColor = &HC0E0FF      'naranja 12640511
                    Next I
                End If
                
            Else
                
                .TextMatrix(Linea, ColHidden1) = "0"
                
                If CDec(Rs!CantRecolectada) = CDec(Rs!CantProductos) Then
                    For I = 0 To Grid.Cols - 1
                        .Col = I
                        .CellBackColor = &HB4EDB9    'verde 11857337
                    Next I
                ElseIf CDec(Rs!CantRecolectada) > 0 Then
                    For I = 0 To .Cols - 1
                        .Col = I
                        .CellBackColor = -2147483624   'Amarillo
                    Next I
                End If
                
            End If
            
        End If
        
        .TextMatrix(Linea, ColFaseLote) = Rs!Fase
        
        Rs.MoveNext
        
    End With
    Wend
    
    Rs.Close
    
    If Grid.Rows > 12 Then
        ScrollGrid.Visible = True
        Grid.ScrollBars = flexScrollBarBoth
        Grid.ColWidth(ColDestinos) = 3160 - 675
    Else
        Grid.ColWidth(ColDestinos) = 3160
        Grid.ScrollBars = flexScrollBarNone
        ScrollGrid.Visible = False
    End If
    
    ImgAlertaPackingMobile.Visible = False
    lblPackingMobile.Visible = False
    
    TimerAlert.Enabled = (CDec( _
    BuscarValorBD("PackingMobile", _
    "SELECT isNULL(COUNT(*), 0) AS PackingMobile " & _
    "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING " & _
    "WHERE Picking = 1 " & _
    "AND (PackingMobile_CantEmpaques > 0) " & _
    "AND Packing = 0", "0", _
    Ent.BDD)) > 0)
    
Finally:
    
    Grid.Visible = True
    
    Exit Sub
    
Error1:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    Mensaje True, "Error al preparar los datos, Descripci�n: " & mErrorDesc & " (" & mErrorNumber & ")"
    
    Salir = True
    
    GoTo Finally
    
End Sub

Private Sub GlobalHotKeyTimer_Timer()
    If GetAsyncKeyState(VK_CONTROL) < 0 Then
        If GetAsyncKeyState(VK_SHIFT) < 0 Then
            If GetAsyncKeyState(VK_DOWN) < 0 Then
                GoTo ToTheTray
            ElseIf GetAsyncKeyState(VK_UP) < 0 Then
                GoTo RestoreFromTray
            End If
        End If
        If GetAsyncKeyState(VK_SHIFT) < 0 Then
            If GetAsyncKeyState(VK_F10) < 0 Then
ToTheTray:
                mExit.Enabled = (Screen.ActiveForm Is Me _
                Or Screen.ActiveForm Is FrmTransferencia_AsignarPickingLote _
                Or Screen.ActiveForm Is FrmTransferencia_AsignarPackingLote)
                TrayApp Me
            End If
            If GetAsyncKeyState(VK_F11) < 0 Then
RestoreFromTray:
                UnTrayApp
            End If
        End If
    End If
End Sub

Private Sub Grid_DblClick()
    
    If Proceso Then
        
        If Grid.TextMatrix(Grid.RowSel, ColLote) <> Empty Then
            
            Select Case Grid.TextMatrix(Grid.RowSel, ColFaseLote)
                
                Case "2"
                    
                    Mensaje True, "Este lote ya esta listo para enviar, pendiente por finalizar. " & _
                    "Si desea modificar el Lote debe reabrir la fase anterior."
                    
                Case "1"
                    
                    FrmTransferencia_AsignarPackingLote.Lote = Grid.TextMatrix(Grid.RowSel, ColLote)
                    FrmTransferencia_AsignarPackingLote.Show vbModal
                    Set FrmTransferencia_AsignarPackingLote = Nothing
                    
                    Call ButtonActualizar_Click
                    
                Case Else '"0"
                    
                    FrmTransferencia_AsignarPickingLote.Lote = Grid.TextMatrix(Grid.RowSel, ColLote)
                    FrmTransferencia_AsignarPickingLote.Show vbModal
                    Set FrmTransferencia_AsignarPickingLote = Nothing
                    
                    Call ButtonActualizar_Click
                    
            End Select
            
        Else
            Mensaje True, "El Lote Esta Vac�o."
        End If
        
    End If
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Function QueryProcesados( _
Optional ByVal pTop As String, _
Optional ByVal pFechas As Boolean = False) As String
    
    Dim SQL As String
    
    Dim mTop As String, mFechas As String
    
    If Len(pTop) > 0 Then
        mTop = pTop & " "
    End If
    
    If pFechas Then
        mFechas = "HAVING CAST(MAX(PP.FechaAsignacion) AS DATE) " & _
        "BETWEEN '" & FechaBD(TextFechaDesde.Text) & "' " & _
        "AND '" & FechaBD(TextFechaHasta.Text) & "' " & vbNewLine
    End If
    
    SQL = ";WITH Base AS ( " & vbNewLine & _
    "SELECT " & mTop & "PP.CodLote as Lote, COUNT(DISTINCT(PP.CodPedido)) AS Pedidos, " & vbNewLine & _
    "SUM(PP.CantSolicitada) AS CantProductos, MAX(PP.FechaAsignacion) AS Fecha, " & vbNewLine & _
    "SUM(PP.CantEmpacada) AS CantEmpacada, SUM(PP.CantRecolectada) as CantRecolectada, " & vbNewLine & _
    "((SUM(PP.CantEmpacada) / (SUM(PP.CantSolicitada))) * 100) AS Proceso, " & vbNewLine & _
    "SUM(TR.n_TotalPeso) AS Peso, SUM(TR.n_TotalVolumen) AS Vol, TR.c_CodLocalidad_Destino " & vbNewLine & _
    "FROM TR_LOTE_GESTION_TRANSFERENCIA TR " & vbNewLine & _
    "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & vbNewLine & _
    "ON TR.cs_Corrida = PP.CodLote " & vbNewLine & _
    "WHERE TR.c_Documento_Nota <> '' " & vbNewLine & _
    "OR TR.c_Documento_Trans <> '' " & vbNewLine & _
    "GROUP BY PP.CodLote, TR.c_CodLocalidad_Destino " & vbNewLine & _
    mFechas & _
    ")"
    
    SQL = SQL & _
    ", Datos  AS ( " & vbNewLine & _
    "SELECT " & mTop & "PP.CodLote as Lote, COUNT(DISTINCT(PP.CodPedido)) AS Pedidos, " & vbNewLine & _
    "SUM(PP.CantSolicitada) AS CantProductos, MAX(PP.FechaAsignacion) AS Fecha, " & vbNewLine & _
    "SUM(PP.CantEmpacada) AS CantEmpacada, SUM(PP.CantRecolectada) as CantRecolectada, " & vbNewLine & _
    "((SUM(PP.CantEmpacada) / (SUM(PP.CantSolicitada))) * 100) AS Proceso, " & vbNewLine & _
    "SUM(TR.n_TotalPeso) AS Peso, SUM(TR.n_TotalVolumen) AS Vol, " & vbNewLine & _
    "3 AS Fase " & vbNewLine & _
    "FROM TR_LOTE_GESTION_TRANSFERENCIA TR " & vbNewLine & _
    "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & vbNewLine & _
    "ON TR.cs_Corrida = PP.CodLote " & vbNewLine & _
    "LEFT JOIN MA_LOTE_GESTION_TRANSFERENCIA MA " & vbNewLine & _
    "ON PP.CodLote = MA.cs_Corrida " & vbNewLine & _
    "WHERE TR.c_Documento_Nota <> '' " & vbNewLine & _
    "OR TR.c_Documento_Trans <> '' " & vbNewLine & _
    "GROUP BY PP.CodLote, MA.cs_Corrida " & vbNewLine & _
    mFechas & _
    ")"
    
    SQL = SQL & _
    ", GroupByDestinos AS ( " & vbNewLine & _
    "    SELECT BASE.Lote, BASE.c_CodLocalidad_Destino, " & vbNewLine & _
    "    isNULL(SUC.c_Descripcion, 'N/A') AS Localidad, COUNT(*) AS CantDestinos " & vbNewLine & _
    "    FROM BASE" & vbNewLine & _
    "    LEFT JOIN MA_SUCURSALES SUC " & vbNewLine & _
    "    ON BASE.c_CodLocalidad_Destino = SUC.c_Codigo " & vbNewLine & _
    "    GROUP BY BASE.Lote, BASE.c_CodLocalidad_Destino, isNULL(SUC.c_Descripcion, 'N/A') "
    
    SQL = SQL & _
    "), DestinosXLote aS ( " & vbNewLine & _
    "SELECT T0.Lote, STUFF(( " & vbNewLine & _
    "    SELECT ', ' + T1.Localidad " & vbNewLine & _
    "    FROM GroupByDestinos T1 " & vbNewLine & _
    "    WHERE T1.Lote = T0.Lote " & vbNewLine & _
    "    --ORDER BY T1.Localidad " & vbNewLine & _
    "    FOR XML PATH(''), TYPE).value('(./text())[1]','VARCHAR(MAX)'), 1, LEN(','), '') AS Destinos " & vbNewLine & _
    "FROM GroupByDestinos T0 " & vbNewLine & _
    "GROUP BY T0.Lote " & vbNewLine
    
    SQL = SQL & _
    "), Final AS ( " & vbNewLine & _
    "SELECT TB.*, isNULL(DLT.Destinos, 'N/A') AS Destinos " & vbNewLine & _
    "FROM Datos TB " & vbNewLine & _
    "LEFT JOIN DestinosXLote DLT " & vbNewLine & _
    "ON TB.Lote = DLT.Lote " & vbNewLine & _
    ") SELECT * FROM Final " & vbNewLine & _
    OrderBy & ";"
    
    QueryProcesados = SQL
    
End Function

Private Sub ButtonActualizar_Click()
    
    Dim mCriterioEstatus As String
    
    Select Case True
        
        Case OptEnproceso.Value, OptPendXCerrar.Value
            
            If OptEnproceso.Value Then
                mCriterioEstatus = "MA.b_PackingFinalizado = 0 " & _
                "AND MA.b_Finalizada = 0 " & _
                "AND MA.b_Anulada = 0 "
            ElseIf OptPendXCerrar.Value Then
                mCriterioEstatus = "MA.b_PickingFinalizado = 1 " & _
                "AND MA.b_PackingFinalizado = 1 " & _
                "AND MA.b_Finalizada = 0 " & _
                "AND MA.b_Anulada = 0 "
            End If
            
            SQL = ";WITH Base AS ( " & vbNewLine & _
            "SELECT TR.cs_Corrida AS Lote, TR.c_Documento AS Pedido, DET.cs_CodArticulo AS Art, " & vbNewLine & _
            "SUM(DET.ns_Cantidad) AS Articulos, SUM(TR.n_TotalPeso) AS Peso, SUM(TR.n_TotalVolumen) AS Vol, " & vbNewLine & _
            "MA.d_FechaCorrida AS Fecha, MA.b_PickingFinalizado AS Picking, TR.c_CodLocalidad_Destino, " & vbNewLine & _
            "TR.cs_CodLocalidad_Solicitud " & vbNewLine & _
            "FROM TR_LOTE_GESTION_TRANSFERENCIA TR " & vbNewLine & _
            "INNER JOIN TR_REQUISICIONES DET " & vbNewLine & _
            "ON TR.c_Documento = DET.cs_Documento " & vbNewLine & _
            "AND TR.cs_CodLocalidad_Solicitud = DET.cs_CodLocalidad " & vbNewLine & _
            "INNER JOIN MA_REQUISICIONES DOC " & vbNewLine & _
            "ON TR.c_Documento = DOC.cs_Documento " & vbNewLine & _
            "AND TR.cs_CodLocalidad_Solicitud = DOC.cs_CodLocalidad " & vbNewLine & _
            "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA MA " & vbNewLine & _
            "ON TR.cs_Corrida = MA.cs_Corrida " & vbNewLine & _
            "WHERE " & mCriterioEstatus & " " & vbNewLine & _
            "GROUP BY TR.cs_Corrida, TR.c_Documento, DET.cs_CodArticulo, " & vbNewLine & _
            "MA.d_FechaCorrida, MA.b_PackingFinalizado, MA.b_PickingFinalizado, " & _
            "TR.c_CodLocalidad_Destino, TR.cs_CodLocalidad_Solicitud " & vbNewLine & _
            ")"
            
            SQL = SQL & _
            ", Datos AS ( " & vbNewLine & _
            "SELECT TR.cs_Corrida AS Lote, TR.c_Documento AS Pedido, DET.cs_CodArticulo AS Art, " & vbNewLine & _
            "SUM(DET.ns_Cantidad) AS Articulos, SUM(TR.n_TotalPeso) AS Peso, SUM(TR.n_TotalVolumen) AS Vol, " & vbNewLine & _
            "TR.cs_CodLocalidad_Solicitud, MA.d_FechaCorrida AS Fecha, MA.b_PickingFinalizado AS Picking, " & _
            "CASE WHEN MA.b_PackingFinalizado = 1 THEN 2 WHEN MA.b_PickingFinalizado = 1 THEN 1 ELSE 0 END AS Fase " & vbNewLine & _
            "FROM TR_LOTE_GESTION_TRANSFERENCIA TR " & vbNewLine & _
            "INNER JOIN TR_REQUISICIONES DET " & vbNewLine & _
            "ON TR.c_Documento = DET.cs_Documento " & vbNewLine & _
            "AND TR.cs_CodLocalidad_Solicitud = DET.cs_CodLocalidad " & vbNewLine & _
            "INNER JOIN MA_REQUISICIONES DOC " & vbNewLine & _
            "ON TR.c_Documento = DOC.cs_Documento " & vbNewLine & _
            "AND TR.cs_CodLocalidad_Solicitud = DOC.cs_CodLocalidad " & vbNewLine & _
            "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA MA " & vbNewLine & _
            "ON TR.cs_Corrida = MA.cs_Corrida " & vbNewLine & _
            "WHERE " & mCriterioEstatus & " " & vbNewLine & _
            "GROUP BY TR.cs_Corrida, TR.c_Documento, DET.cs_CodArticulo, " & vbNewLine & _
            "MA.d_FechaCorrida, MA.b_PackingFinalizado, MA.b_PickingFinalizado, " & vbNewLine & _
            "TR.cs_CodLocalidad_Solicitud " & vbNewLine & _
            ")"
            
            SQL = SQL & _
            ", " & vbNewLine & _
            "GroupByDestinos AS ( " & vbNewLine & _
            "    SELECT BASE.Lote, BASE.c_CodLocalidad_Destino, " & vbNewLine & _
            "    isNULL(SUC.c_Descripcion, 'N/A') AS Localidad, COUNT(*) AS CantDestinos " & vbNewLine & _
            "    FROM BASE" & vbNewLine & _
            "    LEFT JOIN MA_SUCURSALES SUC " & vbNewLine & _
            "    ON BASE.c_CodLocalidad_Destino = SUC.c_Codigo " & vbNewLine & _
            "    GROUP BY BASE.Lote, BASE.c_CodLocalidad_Destino, isNULL(SUC.c_Descripcion, 'N/A') "
            
            SQL = SQL & _
            "), DestinosXLote aS ( " & vbNewLine & _
            "SELECT T0.Lote, STUFF(( " & vbNewLine & _
            "    SELECT ', ' + T1.Localidad " & vbNewLine & _
            "    FROM GroupByDestinos T1 " & vbNewLine & _
            "    WHERE T1.Lote = T0.Lote " & vbNewLine & _
            "    --ORDER BY T1.Localidad " & vbNewLine & _
            "    FOR XML PATH(''), TYPE).value('(./text())[1]', " & _
            "'VARCHAR(MAX)'), 1, LEN(','), '') AS Destinos " & vbNewLine & _
            "FROM GroupByDestinos T0 " & vbNewLine & _
            "GROUP BY T0.Lote " & vbNewLine
            
            SQL = SQL & _
            "), " & vbNewLine & _
            "Final AS ( " & vbNewLine & _
            "SELECT TB.Lote, MIN(TB.Fecha) AS Fecha, COUNT(DISTINCT(TB.Pedido)) AS Pedidos, " & vbNewLine & _
            "SUM(TB.Articulos) AS CantProductos, TB.Picking, SUM(TB.Peso) AS Peso, SUM(TB.Vol) AS Vol, " & vbNewLine & _
            "isNULL(DLT.Destinos, 'N/A') AS Destinos, " & vbNewLine & _
            "ROUND(isNULL(SUM(PP.CantRecolectada), 0), MAX(Pro.Cant_Decimales), 0) AS CantRecolectada, " & vbNewLine & _
            "ROUND(isNULL(SUM(PP.CantEmpacada), 0), MAX(Pro.Cant_Decimales), 0) AS CantEmpacada, " & vbNewLine & _
            "((ROUND(isNULL(SUM(PP.CantRecolectada), 0), MAX(Pro.Cant_Decimales), 0) + " & vbNewLine & _
            "ROUND(isNULL(SUM(PP.CantEmpacada), 0), MAX(Pro.Cant_Decimales), 0)) " & vbNewLine & _
            "/ CONVERT(Real, SUM(TB.Articulos) * 2) * 100) AS Proceso, TB.Fase " & vbNewLine & _
            "FROM Datos TB " & vbNewLine & _
            "INNER JOIN MA_PRODUCTOS PRO " & vbNewLine & _
            "ON TB.Art = Pro.c_Codigo " & vbNewLine & _
            "LEFT JOIN MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & vbNewLine & _
            "ON PP.CodLote = TB.Lote " & vbNewLine & _
            "AND PP.CodPedido = TB.Pedido " & vbNewLine & _
            "AND PP.cs_CodLocalidad_Solicitud = TB.cs_CodLocalidad_Solicitud " & vbNewLine & _
            "AND PP.CodProducto = TB.Art " & vbNewLine & _
            "LEFT JOIN DestinosXLote DLT " & vbNewLine & _
            "ON TB.Lote = DLT.Lote " & vbNewLine & _
            "GROUP BY TB.Lote, TB.Picking, TB.Fase, isNULL(DLT.Destinos, 'N/A') " & vbNewLine & _
            ") SELECT * FROM Final " & vbNewLine & _
            OrderBy & ";"
            
            Proceso = True
            FrameBusqueda.Enabled = False
            
        Case OptProcesados.Value
            
            SQL = QueryProcesados("TOP 40")
            
            Proceso = False
            FrameBusqueda.Enabled = True
            
    End Select
    
    Call PrepararGrid
    Call PrepararDatos(SQL)
    
End Sub

Private Sub ShowPackingMobile()
    
    FrmTransferencia_PackingUser.PackingMobile = True
    FrmTransferencia_PackingUser.Show vbModal
    
    Set FrmTransferencia_PackingUser = Nothing
    
    ButtonActualizar_Click
    
End Sub

Private Sub ImgAlertaPackingMobile_Click()
    ShowPackingMobile
End Sub

Private Sub lblPackingMobile_Click()
    ShowPackingMobile
End Sub

Private Sub OptEnproceso_Click()
    
    TextFechaDesde.Text = Empty
    TextFechaHasta.Text = Empty
    
    Call ButtonActualizar_Click
    
End Sub

Private Sub OptPendXCerrar_Click()
    
    TextFechaDesde.Text = Empty
    TextFechaHasta.Text = Empty
    
    Call ButtonActualizar_Click
    
End Sub

Private Sub OptOrden_Click(Index As Integer)
    
    For I = OptOrden.LBound To OptOrden.UBound
        
        If I <> Index Then
            
            OptOrden(I).Caption = OrdenLabel(I)
            OptOrden(I).Font.Bold = False
            
        Else
            
            If OptOrden(I).Tag = "0" Then
                OptOrden(I).Caption = OrdenLabel(I) & " �"
            Else
                OptOrden(I).Caption = OrdenLabel(I) & " �"
            End If
            
            OptOrden(I).Font.Bold = True
            
        End If
        
    Next
    
    OrderBy = "ORDER BY " & OrdenCampo(Index) & " " & IIf(OptOrden(Index).Tag = "1", "DESC", "ASC")
    
    ButtonActualizar_Click
    
End Sub

Private Sub OptOrden_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button <> vbLeftButton Then
        OptOrden(Index).Tag = IIf(OptOrden(Index).Tag = "1", "0", "1")
        If OptOrden(Index).Value Then
            OptOrden_Click Index
        Else
            OptOrden(Index).Value = True
        End If
    Else
        OptOrden_Click Index
    End If
End Sub

Private Sub OptProcesados_Click()
    Call ButtonActualizar_Click
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub TextFechaDesde_Click()
    
    Dim mCls As clsFechaSeleccion
    
    Set mCls = New clsFechaSeleccion
    mCls.MostrarInterfazFechaHora DateTypes.JustDate
    
    If mCls.Selecciono Then
        TextFechaDesde.Text = mCls.FECHA
        TextFechaDesde.Tag = CDbl(mCls.FECHA)
    End If
    
End Sub

Private Sub TextFechaHasta_Click()
    
    Dim mCls As clsFechaSeleccion
    
    Set mCls = New clsFechaSeleccion
    mCls.MostrarInterfazFechaHora DateTypes.JustDate
    
    If mCls.Selecciono Then
        TextFechaHasta.Text = mCls.FECHA
        TextFechaHasta.Tag = CDbl(mCls.FECHA)
    End If

End Sub

Private Sub Timer1_Timer()
    DoEvents
    bar.Value = bar.Value + 1
    If bar.Value = bar.Max Then
        Call ButtonActualizar_Click
        Call txtminutos_LostFocus
    End If
End Sub

Private Sub TimerAlert_Timer()
    ImgAlertaPackingMobile.Visible = Not ImgAlertaPackingMobile.Visible
    lblPackingMobile.Visible = ImgAlertaPackingMobile.Visible
End Sub

Private Sub txtminutos_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyReturn
            Call txtminutos_LostFocus
    End Select
End Sub

Private Sub txtminutos_LostFocus()
    
    If Not IsNumeric(Me.txtminutos) Then
        
        Me.txtminutos.Text = "1"
        
        bar.Max = 1 * 100
        bar.Min = 0
        bar.Value = 0
        
        Timer1.Enabled = False
        Timer1.Enabled = True
        
    Else
        
        If txtminutos < 1 Then
            txtminutos = "1"
        End If
        
        bar.Max = Me.txtminutos * 100
        bar.Min = 0
        bar.Value = 0
        
        Timer1.Enabled = False
        Timer1.Enabled = True
        
    End If
    
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Dim Msg As Long
    
    Msg = X / Screen.TwipsPerPixelX
    
    Select Case Msg
        
        Case WM_LBUTTONDOWN
        Case WM_LBUTTONUP
        Case WM_LBUTTONDBLCLK
            
            UnTrayApp
            
        Case WM_RBUTTONDOWN
            
            PopupMenu TrayMenu, , , , mShow
            
        Case WM_RBUTTONUP
        Case WM_RBUTTONDBLCLK
        
    End Select
    
End Sub

Private Sub mExit_Click()
    mShow_Click
    CerrarDesdeBandeja = True
    ForcedExit
End Sub

Private Sub mShow_Click()
    Form_MouseMove vbLeftButton, 0, _
    WM_LBUTTONDBLCLK * Screen.TwipsPerPixelX, 0
End Sub
