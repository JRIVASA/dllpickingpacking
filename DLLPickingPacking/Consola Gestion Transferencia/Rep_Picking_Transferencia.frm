VERSION 5.00
Begin VB.Form Rep_Picking_Transferencia 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4965
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   9600
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4965
   ScaleMode       =   0  'User
   ScaleWidth      =   12631.58
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   3030
      Left            =   120
      TabIndex        =   6
      Top             =   580
      Width           =   9345
      Begin VB.TextBox Departamento 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2250
         MaxLength       =   10
         TabIndex        =   20
         ToolTipText     =   "Ingrese el Departamento donde se encuentra el producto"
         Top             =   1440
         Width           =   1755
      End
      Begin VB.TextBox Grupo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2250
         MaxLength       =   10
         TabIndex        =   19
         ToolTipText     =   "Ingrese el Grupo al que Pertenece el producto"
         Top             =   1845
         Width           =   1755
      End
      Begin VB.TextBox Subgrupo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2250
         MaxLength       =   10
         TabIndex        =   18
         ToolTipText     =   "Ingrese el Subgrupo al que Pertenece el producto"
         Top             =   2250
         Width           =   1755
      End
      Begin VB.CommandButton CmdDepartamento 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   345
         Left            =   4050
         Picture         =   "Rep_Picking_Transferencia.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   1440
         Width           =   315
      End
      Begin VB.CommandButton CmdSubgrupo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   345
         Left            =   4050
         Picture         =   "Rep_Picking_Transferencia.frx":0802
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   2250
         Width           =   315
      End
      Begin VB.CommandButton CmdGrupo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   345
         Left            =   4050
         Picture         =   "Rep_Picking_Transferencia.frx":1004
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   1845
         Width           =   315
      End
      Begin VB.TextBox txtRecolector 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2250
         Locked          =   -1  'True
         TabIndex        =   10
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   765
         Width           =   1755
      End
      Begin VB.CommandButton btnRecolector 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   315
         Left            =   4050
         Picture         =   "Rep_Picking_Transferencia.frx":1806
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   765
         Width           =   315
      End
      Begin VB.CommandButton btnLote 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   315
         Left            =   4050
         Picture         =   "Rep_Picking_Transferencia.frx":2008
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   360
         Width           =   315
      End
      Begin VB.TextBox txtLote 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2250
         Locked          =   -1  'True
         TabIndex        =   7
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   360
         Width           =   1755
      End
      Begin VB.Label lblDepartamento 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   26
         Top             =   1440
         Width           =   1530
      End
      Begin VB.Label lblGrupo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   25
         Top             =   1845
         Width           =   825
      End
      Begin VB.Label lblSubgrupo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Subgrupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   24
         Top             =   2250
         Width           =   1140
      End
      Begin VB.Label lbl_Departamento 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4410
         TabIndex        =   23
         Top             =   1440
         Width           =   4650
      End
      Begin VB.Label lbl_Grupo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4410
         TabIndex        =   22
         Top             =   1845
         Width           =   4650
      End
      Begin VB.Label lbl_Subgrupo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4410
         TabIndex        =   21
         Top             =   2250
         Width           =   4650
      End
      Begin VB.Label lblRecolector 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Recolector:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   150
         TabIndex        =   14
         Top             =   765
         Width           =   975
      End
      Begin VB.Label lbl_descRecolector 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4410
         TabIndex        =   13
         Top             =   765
         Width           =   4650
      End
      Begin VB.Label lblLote 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Lote:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   150
         TabIndex        =   12
         Top             =   360
         Width           =   435
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00AE5B00&
         X1              =   2040
         X2              =   9100
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios de Busqueda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Index           =   71
         Left            =   0
         TabIndex        =   11
         Top             =   0
         Width           =   1875
      End
   End
   Begin VB.CommandButton cmd_salir 
      Appearance      =   0  'Flat
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   8385
      Picture         =   "Rep_Picking_Transferencia.frx":280A
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Salir del Reporte (F3)"
      Top             =   3825
      Width           =   1095
   End
   Begin VB.CommandButton aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Impresora"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   0
      Left            =   7185
      Picture         =   "Rep_Picking_Transferencia.frx":458C
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Imprimir Reporte (F8)"
      Top             =   3825
      Width           =   1095
   End
   Begin VB.CommandButton aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Pantalla"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   1
      Left            =   5985
      Picture         =   "Rep_Picking_Transferencia.frx":630E
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Vista Preliminar (F2)"
      Top             =   3825
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10800
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Reporte de Picking"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7395
         TabIndex        =   1
         Top             =   75
         Width           =   1815
      End
   End
End
Attribute VB_Name = "Rep_Picking_Transferencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As ClsTrans_Gestion
Public LastButton As Integer

Private Sub aceptar_Click(Index As Integer)
    
    'ValidarImpresoraPredeterminadaDelSistemaOperativo
    
    Dim Tmp As String
    
    'Tmp = Me.txtLote.Text
    
    LastButton = Index
    
    'If Me.txtRecolector <> Empty Then
        'Tmp = Tmp & "|" & Me.txtRecolector.Text
    'End If
    
    pDatosRect = txtRecolector.Text & _
    IIf(Len(lbl_descRecolector.Caption) > 0, "|" & lbl_descRecolector.Caption, vbNullString)
    
    pDatosDept = Departamento.Text & _
    IIf(Len(lbl_Departamento.Caption) > 0, "|" & lbl_Departamento.Caption, vbNullString)
    
    pDatosGrup = Grupo.Text & _
    IIf(Len(lbl_Grupo.Caption) > 0, "|" & lbl_Grupo.Caption, vbNullString)
    
    pDatosSubg = Subgrupo.Text & _
    IIf(Len(lbl_Subgrupo.Caption) > 0, "|" & lbl_Subgrupo.Caption, vbNullString)
    
    'If Not IsEmpty(Tmp) Then
        'fCls.ImprimirDocumento Tmp, True, False ' Demasiado embasurado...
        fCls.Corrida = txtLote.Text
        fCls.ReporteDeRecoleccion fCls.Corrida, pDatosRect, pDatosDept, pDatosGrup, pDatosSubg
    'End If
    
End Sub

Private Sub btnLote_Click()
    
    Dim mResul As Variant
    Dim mConsulta As String
    
    mEstCerrado = "Finalizado"
    mEstPreCierre = "Por aprobar (Listo para Transferir)"
    mEstPacking = "En proceso de Empacado"
    mEstPicking = "En proceso de Recolecci�n"
    
    mConsulta = "SELECT TOP (100) cs_Corrida, d_Fecha, b_Finalizada AS Finalizada, " & _
    "b_PackingFinalizado, b_PickingFinalizado, CASE WHEN b_Finalizada = 1 THEN '" & mEstCerrado & "' " & _
    "WHEN b_PackingFinalizado = 1 THEN '" & mEstPreCierre & "' " & _
    "WHEN b_PickingFinalizado = 1 THEN '" & mEstPacking & "' " & _
    "ELSE '" & mEstPicking & "' END AS Estado FROM MA_LOTE_GESTION_TRANSFERENCIA "
    
    Dim mFrm_Super_Consultas As Object
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With mFrm_Super_Consultas
        
        .Inicializar mConsulta, "�ltimos 100 Lotes de Pedidos.", Ent.BDD
        
        .Add_ItemLabels "Lote", "cs_Corrida", 1545, 0
        .Add_ItemLabels "Fecha", "d_Fecha", 3495, 0, , Array("Fecha", "VbGeneralDate")
        .Add_ItemLabels "Estado", "Estado", 6060, 0
        .Add_ItemLabels "Finalizada", "Finalizada", 0, 0
        .Add_ItemLabels "Packing", "b_PackingFinalizado", 0, 0
        .Add_ItemLabels "Picking", "b_PickingFinalizado", 0, 0
        
        .Add_ItemSearching "N� Lote", "cs_Corrida"
        .Add_ItemSearching "Fecha", "CONVERT(NVARCHAR(MAX), d_Fecha, 103)"
        
        .txtDato.Text = "%"
        .StrOrderBy = "cs_Corrida DESC"
        
        .Show vbModal
        
        mResul = .ArrResultado()
        
        If Not IsEmpty(mResul) Then
            
            If Trim(mResul(0)) <> Empty Then
                
                Me.txtLote.Text = CStr(mResul(0))
                
                If mResul(3) Then
                    fCls.FasePickingPacking = 3
                ElseIf mResul(4) Then
                    fCls.FasePickingPacking = 2
                ElseIf mResul(5) Then
                    fCls.FasePickingPacking = 1
                Else
                    fCls.FasePickingPacking = 0
                End If
                
            End If
            
        Else
            
            Me.txtLote.Text = Empty
            DatosLote = Empty
            
        End If
    
    End With
    
    Set mFrm_Super_Consultas = Nothing
    
End Sub

Private Sub btnRecolector_Click()
    
    Dim mResul As Variant
    Dim mConsulta As String
    
    mConsulta = "SELECT *, SUBSTRING(cu_Vendedor_Cod, 5, 9999) AS CodUsuario " & _
    "FROM MA_VENDEDORES " & _
    "WHERE MA_VENDEDORES.cs_Tipo = 'PIC' "
    
    Dim mFrm_Super_Consultas As Object
    
    Set mFrm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With mFrm_Super_Consultas
        
        .Inicializar mConsulta, "Transporte", Ent.BDD
        
        .Add_ItemLabels "Cod. Usuario", "CodUsuario", 2550, 0
        .Add_ItemLabels "Usuario", "cu_Vendedor_Des", 8475, 0
        
        .Add_ItemSearching "N� Lote", "cu_Vendedor_Des"
        '.Add_ItemSearching "Fecha", "CONVERT(NVARCHAR(MAX), d_Fecha, 103)"
        
        .txtDato.Text = "%"
        .StrOrderBy = "cu_Vendedor_Des DESC"
        
        .Show vbModal
        
        mResul = .ArrResultado()
        
        If Not IsEmpty(mResul) Then
            If Trim(mResul(0)) <> Empty Then
                Me.txtRecolector.Text = CStr(mResul(0))
                Me.lbl_descRecolector.Caption = CStr(mResul(1))
            End If
        Else
            Me.txtRecolector.Text = Empty
            Me.lbl_descRecolector.Caption = Empty
        End If
        
    End With
    
    Set mFrm_Super_Consultas = Nothing
    
End Sub

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub Departamento_Change()
    If Departamento.Text = vbNullString Then
        Grupo.Text = vbNullString
        Grupo.Enabled = False
        Subgrupo.Text = vbNullString
        Subgrupo.Enabled = False
    Else
        Grupo.Enabled = True
    End If
End Sub

Private Sub Departamento_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Tecla_Pulsada = True
            
            Set Forma = Me
            Tabla = "MA_DEPARTAMENTOS"
            Titulo = UCase(FrmAppLink.StellarMensaje(361)) '"D E P A R T A M E N T O S"
            Call DGS(Forma, Titulo, Tabla)
            
            Tecla_Pulsada = False
            
        Case Is = vbKeyDelete
            
            LcDepartamento = vbNullString
            Departamento.Text = vbNullString
            lbl_Departamento.Caption = vbNullString
            LcGrupo = vbNullString
            Grupo.Text = vbNullString
            lbl_Grupo.Caption = vbNullString
            LcSubGrupo = vbNullString
            Subgrupo.Text = vbNullString
            lbl_Subgrupo.Caption = vbNullString
            
    End Select
    
End Sub

Private Sub Departamento_LostFocus()
    If Departamento.Text <> Empty Then
        Set Forma = Me
        Tabla = "MA_DEPARTAMENTOS"
        Titulo = UCase(FrmAppLink.StellarMensaje(361)) '"D E P A R T A M E N T O S"
        Call PrepararDGS(Forma, Titulo, Tabla)
        Call Carga_DGS(Departamento, "MA_DEPARTAMENTOS", 0)
    Else
        Call Departamento_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub Form_Load()
    
    Departamento.MaxLength = CampoLength("c_Departamento", "MA_PRODUCTOS", Ent.BDD, Alfanumerico, 10)
    Grupo.MaxLength = CampoLength("c_Grupo", "MA_PRODUCTOS", Ent.BDD, Alfanumerico, 10)
    Subgrupo.MaxLength = CampoLength("c_Subgrupo", "MA_PRODUCTOS", Ent.BDD, Alfanumerico, 10)
    
End Sub

Private Sub Grupo_Change()
    If Grupo.Text = Empty Then
        Subgrupo.Text = Empty
        Subgrupo.Enabled = False
    Else
        Subgrupo.Enabled = True
    End If
End Sub

Private Sub Grupo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Set Forma = Me
            Tabla = "MA_GRUPOS"
            Titulo = UCase(FrmAppLink.StellarMensaje(362))
            
            Call DGS(Forma, Titulo, Tabla)
            
        Case Is = vbKeyDelete
            
            LcGrupo = vbNullString
            Grupo.Text = vbNullString
            lbl_Grupo = vbNullString
            Subgrupo.Text = vbNullString
            LcSubGrupo = vbNullString
            lbl_Subgrupo = vbNullString
            
    End Select
    
End Sub

Private Sub Grupo_LostFocus()
    If Grupo.Text <> Empty Then
        Set Forma = Me
        Tabla = "MA_GRUPOS"
        Titulo = UCase(FrmAppLink.StellarMensaje(362)) 'G R U P O S"
        Call PrepararDGS(Forma, Titulo, Tabla)
        Call Carga_DGS(Grupo, "MA_GRUPOS", 1)
    Else
        Call Grupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub Subgrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Set Forma = Me
            Tabla = "MA_SUBGRUPOS"
            Titulo = UCase(FrmAppLink.StellarMensaje(363))
            Call DGS(Forma, Titulo, Tabla)
            
        Case Is = vbKeyDelete
            
            LcSubGrupo = vbNullString
            Subgrupo.Text = vbNullString
            lbl_Subgrupo = vbNullString
            
    End Select
    
End Sub

Private Sub Subgrupo_LostFocus()
    If Subgrupo.Text <> Empty Then
        Set Forma = Me
        Tabla = "MA_SUBGRUPOS"
        Titulo = UCase(FrmAppLink.StellarMensaje(363)) 'S U B - G R U P O S"
        Call PrepararDGS(Forma, Titulo, Tabla)
        Call Carga_DGS(Subgrupo, "MA_SUBGRUPOS", 2)
    Else
        Call Subgrupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub CmdDepartamento_Click()
    Departamento.SetFocus
    Call Departamento_KeyDown(vbKeyF2, 0)
End Sub

Private Sub CmdGrupo_Click()
    If Grupo.Enabled Then
        Grupo.SetFocus
        Call Grupo_KeyDown(vbKeyF2, 0)
    End If
End Sub

Private Sub CmdSubgrupo_Click()
    If Subgrupo.Enabled Then
        Subgrupo.SetFocus
        Call Subgrupo_KeyDown(vbKeyF2, 0)
    End If
End Sub

Private Sub txtLote_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyBack _
    Or KeyCode = vbKeyDelete _
    Or KeyCode = vbKeySpace Then
        
        txtLote.Text = Empty
        
    ElseIf KeyCode = vbKeyF2 Then
        
        btnLote_Click
        
    End If
    
End Sub

Private Sub txtRecolector_Change()
    
    If KeyCode = vbKeyBack _
    Or KeyCode = vbKeyDelete _
    Or KeyCode = vbKeySpace Then
        
        txtRecolector.Text = Empty
        lbl_descRecolector.Caption = Empty
        
    ElseIf KeyCode = vbKeyF2 Then
        
        btnRecolector_Click
        
    End If
    
End Sub
